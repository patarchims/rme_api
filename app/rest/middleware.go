package rest

import (
	"fmt"
	"hms_api/modules/user"
	"hms_api/pkg/helper"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gofiber/fiber/v2"
	jwtMiddleware "github.com/gofiber/jwt/v2"
	"github.com/golang-jwt/jwt/v5"
	"github.com/sirupsen/logrus"
)

type Middleware interface {
	GenerateToken(users user.ApiUser) (map[string]string, error)
	JwtVerify() gin.Context
	ParseToken(tokenString string) jwt.MapClaims
}

type jwtMidleware struct {
}

func NewMiddleware() *jwtMidleware {
	return &jwtMidleware{}
}

func CORSMiddleware() gin.HandlerFunc {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
		if c.Request.Method == "OPTIONS" {
			c.Writer.Write([]byte("allowed"))
			return
		}

		c.Next()
	}
}

var SecretKey = os.Getenv("JWT_SECRET_KEY")

func GlobalErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				response := helper.APIResponseFailure("Internal Server Error", http.StatusInternalServerError)
				c.JSON(http.StatusInternalServerError, response)
			}
		}()
		c.Next()
	}
}

// GenerateTokenPair
func GenerateTokenPair(users user.ApiUser) (map[string]string, error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	// This is the information which frontend can use
	// The backend can also decode the token and get admin etc.
	claims := token.Claims.(jwt.MapClaims)
	// ENDCODE STRING ID USER
	// var encodedString = base64.StdEncoding.EncodeToString([]byte(users.ID))
	claims["user_id"] = users.ID
	claims["username"] = users.Username
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	// Generate encoded token and send it as response.
	// The signing string should be secret (a generated UUID works too)
	t, err := token.SignedString([]byte(SecretKey))
	if err != nil {
		return nil, err
	}

	// REFRESH TOKEN NOT USE
	refreshToken := jwt.New(jwt.SigningMethodHS256)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["username"] = users.Username
	rtClaims["user_id"] = users.ID
	rtClaims["exp"] = time.Now().Add(time.Hour * 360).Unix()

	resf := ""
	resf, err = refreshToken.SignedString([]byte(SecretKey))
	if err != nil {
		return nil, err
	}

	return map[string]string{
		"token":         t,
		"refresh_token": resf,
	}, nil
}

func GenerateTokenUser(users user.Users) (map[string]string, error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	// This is the information which frontend can use
	// The backend can also decode the token and get admin etc.
	claims := token.Claims.(jwt.MapClaims)
	// ENDCODE STRING ID USER
	// var encodedString = base64.StdEncoding.EncodeToString([]byte(users.ID))
	// claims["user_id"] = users.IdUser
	claims["id"] = users.UserId
	claims["kd_modul"] = users.KodeModul
	claims["person"] = users.KeteranganPerson
	claims["exp"] = time.Now().Add(time.Hour * 120).Unix()

	// Generate encoded token and send it as response.
	// The signing string should be secret (a generated UUID works too)
	t, err := token.SignedString([]byte(SecretKey))
	if err != nil {
		return nil, err
	}

	// REFRESH TOKEN NOT USE
	refreshToken := jwt.New(jwt.SigningMethodHS256)
	rtClaims := refreshToken.Claims.(jwt.MapClaims)
	rtClaims["id"] = users.UserId
	rtClaims["kd_modul"] = users.KodeModul
	rtClaims["person"] = users.KeteranganPerson
	rtClaims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	resf := ""
	resf, err = refreshToken.SignedString([]byte(SecretKey))

	if err != nil {
		return nil, err
	}

	return map[string]string{
		"token":         t,
		"refresh_token": resf,
	}, nil
}

func JwtVerify(Logging *logrus.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		type requestHeader struct {
			Token string `header:"x-token" binding:"required"`
		}

		r := new(requestHeader)
		c.ShouldBindHeader(&r)

		data, err := jwt.Parse(r.Token, func(token *jwt.Token) (interface{}, error) {
			return []byte(SecretKey), nil
		})

		if err != nil {
			Logging.Error("Token Expired")
			response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
			c.AbortWithStatusJSON(http.StatusCreated, response)
			return
		}

		claim, ok := data.Claims.(jwt.MapClaims)

		if !ok || !data.Valid {
			Logging.Error("Token Expired")
			response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
			c.AbortWithStatusJSON(http.StatusCreated, response)
			return
		}

		// SET USER ID
		userID := string(claim["id"].(string))
		modul := string(claim["kd_modul"].(string))
		person := string(claim["person"].(string))

		c.Set("userID", userID)
		c.Set("modulID", modul)
		c.Set("person", person)

		c.Next()
	}
}

func ParseToken(tokenString string) jwt.MapClaims {
	//Parse token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return nil
	}
	return token.Claims.(jwt.MapClaims)
}

func FiberJwtVerify(Logging *logrus.Logger) func(c *fiber.Ctx) error {
	return nil
}

func ProtectedHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("log middleware")
		next.ServeHTTP(w, r)
	})
}

// JWTProtected func for specify routes group with JWT authentication.
// See: https://github.com/gofiber/jwt
func JWTProtected() func(*fiber.Ctx) error {

	config := jwtMiddleware.Config{

		SigningKey:     []byte(os.Getenv("JWT_SECRET_KEY")),
		ErrorHandler:   jwtError,
		SuccessHandler: jwtSuccess,
	}

	return jwtMiddleware.New(config)
}

func jwtError(c *fiber.Ctx, err error) error {
	// Return status 401 and failed authentication error.
	if err.Error() == "Missing or malformed JWT" {
		response := helper.APIResponseFailure("Token Failed", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// Return status 401 and failed authentication error.
	fmt.Println("Token Invalid")
	return c.Status(fiber.StatusCreated).JSON(helper.APIResponseFailure("Token invalid", http.StatusCreated))
}

func jwtSuccess(c *fiber.Ctx) error {
	authHeader := c.Get("Authorization")

	if !strings.Contains(authHeader, "Bearer") {
		response := helper.APIResponseFailure("Unauthorized", http.StatusUnauthorized)
		return c.Status(fiber.StatusUnauthorized).JSON(response)
	}

	tokenString := ""

	arrayToken := strings.Split(authHeader, " ")
	if len(arrayToken) == 2 {
		tokenString = arrayToken[1]
	}

	data, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})

	if err != nil {
		response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	claim, ok := data.Claims.(jwt.MapClaims)

	if !ok || !data.Valid {
		response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// SET USER ID
	// modul := string(claim["kd_modul"].(string))
	userID := string(claim["id"].(string))
	modul := string(claim["kd_modul"].(string))
	person := string(claim["person"].(string))

	if err != nil {
		panic(err)
	}

	c.Locals("userID", userID)
	c.Locals("modulID", modul)
	c.Locals("person", person)

	return c.Next()
}

func JWTVeifyHandler(Logging *logrus.Logger) fiber.Handler {

	return func(c *fiber.Ctx) error {

		var token = c.Get("x-token")

		if token == "" {
			Logging.Error("Token Expired")
			response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		data, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
			return []byte(SecretKey), nil
		})

		if err != nil {
			Logging.Error("Token Expired")
			response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		claim, ok := data.Claims.(jwt.MapClaims)

		if !ok || !data.Valid {
			Logging.Error("Token Expired")
			response := helper.APIResponseFailure("Token Expired", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		// SET USER ID
		userID := string(claim["id"].(string))
		modul := string(claim["kd_modul"].(string))

		c.Locals("userID", userID)
		c.Locals("modulID", modul)

		return c.Next()
	}

}
