server {
        listen 8282 default_server;
        listen [::]:8282 default_server;
        root /usr/local/bin/web-app/dist;
        index index.html index.htm index.nginx-debian.html;
        location / {
            try_files $uri $uri / =404;
        }
}