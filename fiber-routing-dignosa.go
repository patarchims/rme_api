package main

import (
	"hms_api/app/rest"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

func FiberRoutingDiagnosaAndListenV2(Logging *logrus.Logger, Store *session.Store, Router fiber.Router, Services *Service) {
	apiV4 := Router.Group("/app/v4/")
	apiV4.Get("diagnosa-banding", rest.JWTProtected(), Services.DiagnosaHandler.OnGetDiagnosaBandingFiberHandler)
	apiV4.Post("diagnosa-banding", rest.JWTProtected(), Services.DiagnosaHandler.OnSaveDiagnosaBandingFiberHandler)
	apiV4.Delete("diagnosa-banding", rest.JWTProtected(), Services.DiagnosaHandler.OnDeleteDiagnosaBandingFiberHandler)
}
