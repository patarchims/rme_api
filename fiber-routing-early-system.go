package main

import (
	"hms_api/app/rest"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

func FiberRoutingEarlySystem(Logging *logrus.Logger, Store *session.Store, Router fiber.Router, Services *Service) {
	apiV2 := Router.Group("/app/v2/")
	apiV2.Post("kontrol-pasien", rest.JWTProtected(), Services.EarlyHandler.OnSaveKontrolPasienFiberHandler)
	apiV2.Get("kontrol-pasien", rest.JWTProtected(), Services.EarlyHandler.OnGetKontrolPasienFiberHandler)

}
