package main

import (
	"hms_api/app/rest"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

func FiberRoutingIGDAndListenV2(Logging *logrus.Logger, Store *session.Store, Router fiber.Router, Services *Service) {
	apiV2 := Router.Group("/app/v2/")
	apiV2.Get("pengkajian-persistem-igd", rest.JWTProtected(), Services.IGDHandler.OnGetPengkajianPersistemIGDFiberHandler)
	apiV2.Post("pengkajian-persistem-igd", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianPersistemIGDHandler)

	apiV2.Patch("report-pengkajian-keperawatan-anak-igd", Services.IGDHandler.OnGetPengkajianAnakIGDFiberHandler)
	apiV2.Patch("report-pengkajian-keperawatan-dewasa-igd", Services.IGDHandler.OnGetReportAsesmenKeperawatanDeasaIGDFiberHandler)

	// REPORT PENGKAJIAN KEPERAWATAN DEWASA ANAK IGD
	apiV2.Patch("report-pengkajian-keperawatan-dewasa-igd", Services.IGDHandler.OnReportPengkajianKeperawatanDewasaIGDFiberHandler)

	// PENGKAJIAN IGD DEWASA
	apiV2.Get("pengobatan-dirumah-igd-anak", Services.IGDHandler.OnGetPengkajianPengobatanDirumahIGdAnakFiberHandler)
	apiV2.Post("pengobatan-dirumah-igd-anak", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianRiwayatPengobatanDirumahAnakFiberHandler)

	// === PENGKAJIAN NYERI ================ //
	apiV2.Post("pengkajian-nyeri-igd-anak", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianNyeriKeperawatanANAKIGD)
	apiV2.Get("pengkajian-nyeri-igd-anak", Services.IGDHandler.OnGetPengkajianNyeriFiberHandler)

	// PENGKAJIAN NUTRISI IGD ==>
	apiV2.Get("pengkajian-nutrisi-igd-anak", Services.IGDHandler.OnGetPengkajianNutrisiIGDFiberHandler)
	apiV2.Post("pengkajian-nutrisi-igd-anak", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianNutrisiFiberHandler)

	// ON SAVE RIWAYAT KEHAMILAN PADA ASESEMEN ANAK
	apiV2.Post("pengkajian-kehamilan-igd-anak", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianAwalIGDRiwayatKehamilanFiberHandler)

	// ======================= GENERAL CONSENT ==============
	apiV2.Get("general-consent", Services.IGDHandler.OnGetGeneralConsentIGDFiberHandler)
	apiV2.Get("general-consent-ranap", Services.IGDHandler.OnGetGeneralConsentIGDFiberHandlerRANAP)
	apiV2.Post("general-consent-ranap", rest.JWTProtected(), Services.IGDHandler.OnSaveGeneralConsentIGDRANAPFiberHandler)

	apiV2.Post("general-consent", rest.JWTProtected(), Services.IGDHandler.OnSaveGeneralConsentIGDFiberHandler)
	apiV2.Patch("general-consent", Services.IGDHandler.OnReportGeneralConsentIGDFiberHandler)

	// RIWAYAT ALERGI
	apiV2.Post("riwayat-alergi-igd", rest.JWTProtected(), Services.IGDHandler.OnSaveRiwayatAlergiIGDFiberHandler)
	apiV2.Get("riwayat-alergi-igd", Services.IGDHandler.OnGetRiwayatAlergiIGDFiberHandler)

	// TINDAK LANJUT IGD RAWAT JALAN
	apiV2.Get("tindak-lanjut-igd", Services.IGDHandler.OnGetKondisiTindakLanjutIGDFiberHandler)
	apiV2.Post("tindak-lanjut-igd", rest.JWTProtected(), Services.IGDHandler.OnSaveKondisiTindakLanjutIGDFiberHandler)

	// GET ASESMEN KEPERAWATAN ANAK
	apiV2.Get("asesmen-keperawatan-igd", Services.IGDHandler.OnGetAsesmenKeperawatanIGDFiberHandler)
	apiV2.Post("asesmen-keperawatan-igd", rest.JWTProtected(), Services.IGDHandler.OnSaveAsesmenKeperawatanIGDFiberHandler)

	// GET PENGKAJIAN NUTIRSI DEWASA
	apiV2.Get("pengkajian-nutrisi-igd-dewasa", Services.IGDHandler.OnGetPengkajianNutrisiDewasaIGDFiberHandler)
	apiV2.Post("pengkajian-nutrisi-igd-dewasa", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianNutrisiDewasaFiberHandlerIGD)

	// RESIKO JATUH PASIEN DEWASA
	apiV2.Get("pengkajian-resiko-jatuh-igd-dewasa", Services.IGDHandler.OnGetPengkajianResikoJatuhPasienDewasaIGDFiberHandler)
	apiV2.Post("pengkajian-resiko-jatuh-igd-dewasa", rest.JWTProtected(), Services.IGDHandler.OnSavePengkajianResikoJatuhPasienDewasaIGDFiberHandler)

	// EARLY WARNING SYSTEM
	apiV2.Get("early-warning-system", Services.IGDHandler.OnGetPengkajianResikoJatuhPasienDewasaIGDFiberHandler)
	apiV2.Post("early-warning-system", Services.IGDHandler.OnGetPengkajianResikoJatuhPasienDewasaIGDFiberHandler)

	// ON GET ASESMENT KEPERAWATAN RAWAT INAP
	apiV2.Get("asesmen-keperawatan-inap", Services.IGDHandler.OnGetAsesmenKeperawatanRANAPFiberHandler)
	apiV2.Post("asesmen-keperawatan-inap", rest.JWTProtected(), Services.IGDHandler.OnSaveAsesmenKeperawatanRANAPFiberHandler)

	// REPORT ASESEMEN KEPERAWATAN RAWAT INAP
	apiV2.Get("report-asesmen-keperawatan-inap", Services.IGDHandler.OnGetReportAsesmenKeperawatanRANAPFiberHandler)

	// ASESMEN KEPERATAN ANAK
	apiV2.Get("asesmen-keperawatan-inap-anak", Services.IGDHandler.OnGetAsesmenKeperawatanANAKFiberHandler)
	apiV2.Post("asesmen-keperawatan-inap-anak", rest.JWTProtected(), Services.IGDHandler.OnSaveAsesmenKeperawatanANAKFiberHandler)
	// REPORT
	apiV2.Get("report-asesmen-keperawatan-inap-anak", Services.IGDHandler.OnReportAsesmenKeperawatanANAKFiberHandler)

}
