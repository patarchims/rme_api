package main

import (
	"hms_api/app/rest"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

func FiberRoutingAndListenV2(Logging *logrus.Logger, Store *session.Store, Router fiber.Router, Services *Service) {
	apiV4 := Router.Group("/app/v4/")
	apiV4.Get("edukasi-terintegrasi", Services.RMEHandler.OnGetDataEdukasiTerintegrasiFiberHandler)
	apiV4.Post("edukasi-terintegrasi", rest.JWTProtected(), Services.RMEHandler.OnSaveDataEdukasiTerintegrasiFiberHandler)
	apiV4.Delete("edukasi-terintegrasi", rest.JWTProtected(), Services.RMEHandler.OnDeleteDataEdukasiTerintegrasiFiberHandler)
	apiV4.Put("edukasi-terintegrasi", Services.RMEHandler.OnGetReportEdukasiTerintegrasiFiberHandler)
	apiV4.Patch("edukasi-terintegrasi", rest.JWTProtected(), Services.RMEHandler.OnUpdateEdukasiTerintegrasiFiberHandler)
	apiV4.Get("pemberi-edukasi-terintegrasi", Services.UserHandler.OnGetDataPengawaiFiberHandler)
	apiV4.Put("ringkasan-pulang-igd", Services.ReportHandler.OnGetRingksanPulangIGDFiberHandler)

	// ROUTING TRANSFUSI DARAH
	apiV4.Post("transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnSaveTransfusiDarahFiberHandler)
	apiV4.Delete("transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnDeleteTransfusiDarahFiberHandler)
	apiV4.Get("transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnGetTransfusiDarahFiberHandler)
	apiV4.Put("transfusi-darah", Services.RMEHandler.OnChangedTransfusiDarahFiberHandler)

	// REAKSI TRANSFUSI DARAH
	apiV4.Post("reaksi-transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnSaveReaksiTransfusiDarahFiberHandler)
	apiV4.Delete("reaksi-transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnDeleteReaksiTransfusiDarahFiberHandler)
	apiV4.Get("reaksi-transfusi-darah", Services.RMEHandler.OnViewReaksiTranfusiDarahByNoTranfusiFiberHandler)
	apiV4.Post("verify-reaksi-transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnVerifyReaksiTransfusiDarahFiberHandler)
	apiV4.Get("verify-reaksi-transfusi-darah", rest.JWTProtected(), Services.RMEHandler.OnGetVerifyReaksiTransfusiDarahFiberHandler)

	// ASESMEN ULANG NYERI
	apiV4.Post("asesmen-ulang-nyeri", rest.JWTProtected(), Services.RMEHandler.OnSaveAssesmenNyeriFiberHandler)
	apiV4.Get("asesmen-ulang-nyeri", Services.RMEHandler.OnGetAsesmenUlangNyeriFiberHandler)
	apiV4.Delete("asesmen-ulang-nyeri", Services.RMEHandler.OnDeleteAssemenUlangNyeriFiberHandler)
	apiV4.Put("asesmen-ulang-nyeri", Services.RMEHandler.OnGetAsesmenUlangNyeriFiberHandler)
	apiV4.Patch("asesmen-ulang-nyeri", Services.RMEHandler.OnReportAsesmenUlangNyeriFiberHandler)

	// apiV4.Patch("report-pengkajian-awal-keperawatan-dewasa-ranap", rest.JWTProtected(), Services.SoapHandler.OnReportPengkajianAwalKeperawatanRANAPFiberHandler)
	// REPORT PENGKAJIAN AWAL DOKTER
	apiV4.Put("pengkajian-awal-pasien-dewasa-medis", Services.ReportHandler.GetReasesmenResikoJatuhAnakHandler)
	apiV4.Put("pengkajian-awal-keperawatan-dewasa-ranap", Services.RMEHandler.PengkajianKeperawatanBangsalPasienFiberHandler)

	// REPORT PENGKAJIAN AWAL RAWAT INAP ANAK
	apiV4.Put("pengkajian-awal-rawat-inap-anak", Services.ReportHandler.OnGetReportPengkajianRawatInapAnakFiberHandler)

	// DATA PEMBERIAN CAIRAN INFUSE
	apiV4.Post("pemberian-terapi-cairan-infuse", rest.JWTProtected(), Services.RMEHandler.OnSavePemberianTerapiCairanFiberHandler)
	apiV4.Patch("pemberian-terapi-cairan-infuse", Services.RMEHandler.OnGetReportPemberianTerapiCairanInfuseFiberHandler)

	// CAIRAN INTAKE
	apiV4.Post("cairan-intake", rest.JWTProtected(), Services.RMEHandler.OnSaveCairanIntakeFiberHandler)
	apiV4.Post("cairan-out-put", rest.JWTProtected(), Services.RMEHandler.OnSaveCairanOutPutFiberHandler)
	apiV4.Delete("cairan-intake", rest.JWTProtected(), Services.RMEHandler.OnDeleteCairanIntakeFiberHandler)
	apiV4.Delete("cairan-out-put", rest.JWTProtected(), Services.RMEHandler.OnDeleteCairanOutPutFiberHandler)

	//======================//
	apiV4.Get("cairan-intake", Services.RMEHandler.OnGetCairanIntakeFiberHandler)
	apiV4.Get("cairan-out-put", Services.RMEHandler.OnGetCairanOutPutFiberHandler)
	apiV4.Get("monitoring-cairan", Services.RMEHandler.OnGetMonitoringCairanFiberHandler)

	// === //
	apiV4.Get("persistem-ranap", Services.RMEHandler.OnGetDataPengkajianPersistemDewasaRANAPFiberHandler)
	apiV4.Post("persistem-ranap", rest.JWTProtected(), Services.RMEHandler.OnSavePengkajianPersistemDewasaRANAPFiberHandler)

	apiV4.Post("deteksi-resiko-jatuh-igd", Services.IGDHandler.OnDeteksiResikoJatuhFiberHandler)
	apiV4.Get("data-karyawan", Services.UserHandler.OnGetDataPengawaiFiberHandler)
}
