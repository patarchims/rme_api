package main

import (
	"hms_api/app/rest"
	"hms_api/config"
	"hms_api/exception"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"

	"github.com/gofiber/fiber/v2/middleware/favicon"
	"github.com/gofiber/fiber/v2/middleware/logger"

	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

func (s *Service) FiberRoutingAndListen(Logging *logrus.Logger, Store *session.Store) {
	app := fiber.New(config.NewFiberConfig())

	app.Get("/dashboard", monitor.New())

	app.Use(logger.New())
	app.Use(recover.New())
	app.Use(favicon.New(favicon.Config{
		File: "./favicon.ico",
		URL:  "/favicon.ico",
	}))
	app.Use(cors.New())
	app.Use(prometheusMiddleware())

	app.Static("/app/images/anatomi/", "./images/anatomi/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/public/", "./images/public/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/anatomi", "./images/anatomi/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Get("/metrics", func(c *fiber.Ctx) error {
		handler := fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler())
		handler(c.Context())
		return nil
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Static("/app/images/odontogram/", "./images/odontogram/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/odontogram", "./images/odontogram/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/lokalis/", "./images/lokalis/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/lokalis", "./images/lokalis/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/users", "./images/users/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	app.Static("/app/images/identitas-bayi", "./images/identitas-bayi/", fiber.Static{
		Compress:      true,
		ByteRange:     true,
		Browse:        true,
		CacheDuration: 10 * time.Second,
		MaxAge:        3600,
	})

	api := app.Group("/app/v1/")
	apiV2 := app.Group("/app/v2/")
	apiV3 := app.Group("/app/v3/")
	apiV4 := app.Group("/app/v4/")

	api.Get("root", s.UserHandler.RootHandlerV2)
	api.Get("cari-karyawan", s.UserHandler.CariKaryawanV2)

	api.Get("login", s.UserHandler.SignInWithIDAndPassword)
	api.Post("decrypt-password", s.UserHandler.DecryptPasswordUserHandler)
	api.Get("session", rest.JWTProtected(), s.UserHandler.SessionV3)
	api.Get("verifikasi-user", s.UserHandler.ActivatedCodeV2)
	api.Get("send-otp", s.UserHandler.RegisterUserV2)
	api.Get("verify-otp", s.UserHandler.VerifyOTPV2)
	api.Get("register-user", s.UserHandler.CreateAccountUserV2)

	// CHANGE USER LOGIN IGD TO PONEK
	api.Get("igd-to-ponek", s.UserHandler.ChangedIGDToPonekFiberHandler)

	// =================================== LIBRARY  =============================== //
	api.Get("lib/:title", s.LibHandler.GetLibraryV2)
	api.Get("lib-bankdata/:kategori", s.LibHandler.GetBankDataV2)
	api.Get("lib-icd/", s.LibHandler.GetICDV2)
	api.Get("lib-icd-all", s.LibHandler.GetICDAllV2)
	api.Post("insert-icd10", rest.JWTProtected(), s.LibHandler.InsertICDV2)

	// VERSION
	api.Get("version", s.LibHandler.GetVersionFiberHandler)

	api.Get("soap-pasien", s.LibHandler.GetSoapPasienV2)
	api.Get("jadwal-dokter", s.LibHandler.GetJadwalDokterV2)
	api.Get("kemasan", s.LibHandler.GetKemasanV2)
	api.Get("komposisi", s.LibHandler.GetKomposisiV2)
	api.Get("penggunaan", s.LibHandler.PenggunaanV2)
	api.Get("satuan", s.LibHandler.GetSatuanV2)
	api.Get("suku", s.LibHandler.GetSukuV2)
	api.Get("getRuangKasus", s.LibHandler.GetRuangKasusV2)
	api.Get("getKRuangPerawatan", s.LibHandler.GetKRuangPerawatV2)
	api.Get("inventory", s.LibHandler.GetInventoryV2)
	api.Get("kprocedure/:kelompok", s.LibHandler.GetProcedurByBagianV2)

	//  ====== //
	// GET FISIOTERAPIs
	api.Get("procedure/:kelompok", s.LibHandler.GetDesProcedureV2)
	api.Post("detail-rad", s.LibHandler.GetDetailRadiologiV2)

	// API PUBLIC KECAMATAN, KELURAHAN
	api.Get("provinsi", s.RegHandler.GetAllProvinsiV2)
	api.Get("prov-kab/:namaProv", s.RegHandler.GetProvAndKabV2)
	api.Get("kecamatan-kab/:namaKab", s.RegHandler.GetKabAndKecV2)
	api.Get("kelurahan-kec/:namaKec", s.RegHandler.GetKelurahanByKecV2)

	api.Get("list-antrean-pasien", rest.JWTProtected(), s.AntreanHandler.GetAntreanPasienV3)
	apiV2.Get("list-antrean-pasien", rest.JWTProtected(), s.AntreanHandler.GetAntreanPasienV4)
	apiV3.Get("list-antrean-pasien", rest.JWTProtected(), s.AntreanHandler.GetAntreanPasienV5)
	api.Get("list-antrean-pasien-diproses", rest.JWTProtected(), s.AntreanHandler.GetAntreanYangTelahDiprosesHandler)

	// TAMBAHKAN LIST ANTRIAN VERSI TERBARU
	apiV4.Get("list-antrean-pasien", rest.JWTProtected(), s.AntreanHandler.GetAntreanPasienNewVersion)
	api.Post("detail-pasien", rest.JWTProtected(), s.AntreanHandler.GetDetailPasienV3)
	api.Get("anteranResep", s.AntreanHandler.GetAntreanResepV2)
	api.Get("bookKamar", s.AntreanHandler.BookKamarV2)

	// SOAP
	api.Post("save-anatomi", rest.JWTProtected(), s.SoapHandler.SaveAnatomiFiberHandler)
	api.Post("get-skrining", rest.JWTProtected(), s.SoapHandler.GetSkriningFiberHandler)
	api.Post("save-skrining", rest.JWTProtected(), s.SoapHandler.SaveSkriningFiberHandler)

	// UPLOAD GAMBAR ODONTOGRAM DI LUAR DARI APLIKASI MOBILE
	api.Post("upload-odontogram", rest.JWTProtected(), s.SoapHandler.UploadImageOdontogramFiberHandler)
	api.Post("publish-odontogram", rest.JWTProtected(), s.SoapHandler.PublisImageOdontogramV2)

	// UPLOAD GAMBAR ODONTOGRAM
	api.Post("save-odontogram", rest.JWTProtected(), s.SoapHandler.SaveSingleOdontogramFiberHandler)
	api.Post("odontogram", rest.JWTProtected(), s.SoapHandler.UploadImageOdontogramFiberHandler)

	api.Delete("delete-odontogram", rest.JWTProtected(), s.SoapHandler.DeleteOdontogramV2)
	api.Post("list-odontogram", rest.JWTProtected(), s.SoapHandler.ListOdontogramFiberHandler)
	api.Post("insert-odontogram", rest.JWTProtected(), s.SoapHandler.InsertOdontogramFiberHandler)

	// ASSEMENT RAWAT JALAN DOKTER
	api.Post("getrawatjalan-dokter", rest.JWTProtected(), s.SoapHandler.GetAssesRawatJalanDokterFiberHandler)
	api.Post("saveRawatjalan-dokter", rest.JWTProtected(), s.SoapHandler.SaveAssmentRawatJalanDokterFiberHandler)

	// ANAMNESA
	api.Post("get-anamnesa", rest.JWTProtected(), s.SoapHandler.GetAnamnesaFiberHandler)
	api.Post("save-anamnesa", rest.JWTProtected(), s.SoapHandler.SaveAnamnesaFiberHandler)

	// ASSEMSENT RAWAT JALAN PERAWAT
	api.Post("save-asses-rawatJalan-perawat", rest.JWTProtected(), s.SoapHandler.SaveAssesmentRawatJalanPerawatFiberHandler)
	api.Post("get-asses-rawatJalan-perawat", rest.JWTProtected(), s.SoapHandler.GetRawatJalanPerawatFiberHandler)
	api.Post("insert-diagnosa", rest.JWTProtected(), s.SoapHandler.InsertDiagnosaFiberHandler)
	api.Post("get-kebutuhanEdukasi", rest.JWTProtected(), s.SoapHandler.GetAssesKebEdukasiFiberHandler)
	api.Post("save-kebutuhanEdukasi", rest.JWTProtected(), s.SoapHandler.SaveAssesKebEdukasiFiberHandler)

	// NEW UPDATE API // BUAT API GET ANAMNESA
	api.Post("hasil-penunjang-medik", rest.JWTProtected(), s.SoapHandler.HasilPenunjangMedikFiberHandler)
	api.Get("diagnosa-icd10", rest.JWTProtected(), s.SoapHandler.HasilPenunjangMedikFiberHandler)
	api.Post("diagnosa-icd10", rest.JWTProtected(), s.SoapHandler.HasilPenunjangMedikFiberHandler)

	// DIAGNOSA
	api.Post("diagnosa", rest.JWTProtected(), s.SoapHandler.InputSingleDiagnosaFiberHandler)
	api.Get("diagnosa", rest.JWTProtected(), s.SoapHandler.GetDiagnosaFiberHandler)
	api.Get("diagnosa-banding", rest.JWTProtected(), s.SoapHandler.GetDiagnoabandingFiberHandler)
	api.Delete("diagnosa", rest.JWTProtected(), s.SoapHandler.DeleteDiagnosaFiberHandler)

	// INFORMASI MEDIS
	api.Get("informasi-medis", rest.JWTProtected(), s.SoapHandler.GetInformasiMedisFiberHandler)
	api.Post("informasi-medis", rest.JWTProtected(), s.SoapHandler.SaveInformasiMedisFiberHandler)

	// ANAMNESA
	api.Get("anamnesa", rest.JWTProtected(), s.SoapHandler.GetAsesmedAnamnesaFiberHandler)
	api.Post("anamnesa", rest.JWTProtected(), s.SoapHandler.SaveAsesmedAnamnesaFiberHandler)

	// ANAMNESA IGD
	api.Get("anamnesa-igd", rest.JWTProtected(), s.SoapHandler.GetAnamnesaIGDFiberHandler)
	api.Post("anamnesa-igd", rest.JWTProtected(), s.SoapHandler.SaveAnamnesaIGDFiberHandler)

	// Data Medik yang diperhatikan
	api.Get("data-medik", rest.JWTProtected(), s.SoapHandler.GetDataMedikYangDiperlukanFiberHandler)
	api.Post("data-medik", rest.JWTProtected(), s.SoapHandler.SaveDataMedikYangDiperlukanV2)

	// Data Intra Oral
	api.Get("data-intra-oral", rest.JWTProtected(), s.SoapHandler.GetDataIntraOralFiberHandler)
	api.Post("data-intra-oral", rest.JWTProtected(), s.SoapHandler.SaveIntraOralFiberHandler)

	// VICORE_HIS API
	// AMBIL DATA DARI DABATASE LAMA
	api.Post("hasil-pemeriksaan-labor-old", rest.JWTProtected(), s.HisHandler.HistoryLaborV3)
	api.Post("hasil-pemeriksaan-radiologi-old", rest.JWTProtected(), s.HisHandler.HistoryRadiologiOld)
	api.Post("hasil-pemeriksaan-fisioterapi-old", rest.JWTProtected(), s.HisHandler.HistoryFisioterapiOld)
	api.Post("hasil-pemeriksaan-gizi-old", rest.JWTProtected(), s.HisHandler.HistoryGiziOld)

	// AMBIL HASIL PEMERIKSAAN LABOR PADA TABLE BARU
	api.Post("hasil-pemeriksaan-labor", rest.JWTProtected(), s.HisHandler.HistoryLaborV2)
	api.Post("hasil-pemeriksaan-labor-pasien", rest.JWTProtected(), s.HisHandler.HistoryLaborPasien)
	api.Post("hasil-pemeriksaan-labor-v2", rest.JWTProtected(), s.HisHandler.HistoryLaborV2)

	api.Post("hasil-penmed", rest.JWTProtected(), s.HisHandler.HistoryPenMedV2)
	api.Post("history-pasien", rest.JWTProtected(), s.HisHandler.HistoryPasienV2)

	// DHASIL PENUNJANG
	api.Post("hasil-radiologi", rest.JWTProtected(), s.HisHandler.HistoryDHasilPenunjang)
	api.Post("hasil-fisioterapi", rest.JWTProtected(), s.HisHandler.HasilPemeriksaanFisioterapi)
	api.Post("hasil-gizi", rest.JWTProtected(), s.HisHandler.HasilPemeriksaanGizi)

	// PASCA OPERASI
	api.Post("pasca-operasi", rest.JWTProtected(), s.SoapHandler.SavePascaOperasiFiberHandler)
	api.Get("pasca-operasi", rest.JWTProtected(), s.SoapHandler.GetRawatJalanPerawatFiberHandler)

	api.Get("intra-operasi", rest.JWTProtected(), s.SoapHandler.GetIntraOperasiFiberHandler)
	api.Post("intra-operasi", rest.JWTProtected(), s.SoapHandler.SaveIntraOperasiFiberHandler)

	// SAVE TRIASE
	api.Get("triase", rest.JWTProtected(), s.SoapHandler.GetTriaseFiberHandler)
	api.Post("triase", rest.JWTProtected(), s.SoapHandler.SaveTriaseFiberHandler)

	// TRIASE - RIWAYAT ALERGI
	// KELUHAN UTAMA ===
	api.Get("keluhan-utama-igd", rest.JWTProtected(), s.SoapHandler.GetKeluhanUtamaFiberHandler)
	api.Post("keluhan-utama-igd", rest.JWTProtected(), s.SoapHandler.SaveKeluhanUtamaFiberHandler)

	api.Get("riwayat-alergi", rest.JWTProtected(), s.SoapHandler.GetRiwayatAlergiFiberHandler)
	api.Post("riwayat-alergi", rest.JWTProtected(), s.SoapHandler.SaveRiwayatAlergiFiberHandler)

	// LOKALIS - IMAGES // LOKALIS IMAGES
	api.Post("lokalis-image-private", rest.JWTProtected(), s.SoapHandler.UploadImageLokalisFiberHandlerPrivate)
	api.Post("lokalis-image", rest.JWTProtected(), s.SoapHandler.UploadImageLokalisFiberHandlerPrivate)
	api.Get("lokalis-image", rest.JWTProtected(), s.SoapHandler.GetImageLokalisFiberHandler)
	api.Get("lokalis-image-mata", rest.JWTProtected(), s.SoapHandler.OngetLokalisImageMataFiberHandler)

	// UPLOAD LOKALIS DI LUAR DARI APLIKASI MOBILE

	// UPLOAD LOKALIS PUBLIC
	api.Post("upload-lokalis", s.SoapHandler.UploadImageLokalisPublicFiberHandler)
	api.Post("upload-odontogram", s.SoapHandler.UploadImageOdontogramFiberHandler)
	api.Post("upload-anatomi", s.SoapHandler.SaveAnatomiPublicUploadFiberHandler)

	// ============================ DOKTER SPESIALIS
	api.Get("dokter-spesialis", rest.JWTProtected(), s.LibHandler.GetDokterSpesialisFiberHandler)

	//============PEMERIKSAAN FISIK  ==================
	api.Post("pemeriksaan-fisik", rest.JWTProtected(), s.SoapHandler.SimpanPemeriksaanFisikFiberHandler)
	api.Get("pemeriksaan-fisik", rest.JWTProtected(), s.SoapHandler.GetPemeriksaanFisikFiberHandler)

	// PILIH PEMERIKSAAN LABOR
	api.Post("pemeriksaan-labor", rest.JWTProtected(), s.SoapHandler.PilihPemeriksaanLaborFiberHandler)
	api.Post("detail-pemeriksaan-labor", rest.JWTProtected(), s.SoapHandler.DetailPemeriksaanLaborFiberHandler)

	// RENCANA TINDAK LANJUT
	api.Post("rencana-tindak-lanjut", rest.JWTProtected(), s.SoapHandler.InsertRencanaTindakLanjutFiberHandler)
	api.Get("rencana-tindak-lanjut", rest.JWTProtected(), s.SoapHandler.GetRencanaTindakLanjutFiberHandler)

	// INSERT PENMED
	api.Post("input-pemeriksaan-labor", rest.JWTProtected(), s.HisHandler.InputPemeriksaanLaborFiberHandler)
	api.Post("input-pemeriksaan-penunjang", rest.JWTProtected(), s.HisHandler.InputPemeriksaaanRadiologiFiberHandler)

	// ===================== SDKI  ================================================== //
	api.Get("sdki/:judul", s.RMEHandler.GetDaftarSDKIFiberHandler)
	api.Post("intervensi", rest.JWTProtected(), s.RMEHandler.GetIntervensiFiberHandler)

	// ===================== CARI INTERVENSI  CARI SIKI ======== // GET SIKI
	api.Post("siki", rest.JWTProtected(), s.RMEHandler.GetSIKIFiberHandler)
	api.Post("save-asesmen-keperawatan", rest.JWTProtected(), s.RMEHandler.SaveAsesmenKeperawatanFiberHandler)
	api.Get("get-asesmen-keperawatan", rest.JWTProtected(), s.RMEHandler.GetAsesmenKeperawatanFiberHandler)

	// ASESMEN KEPERAWATAN BIDAN
	api.Post("asesmen-keperawatan-bidan", rest.JWTProtected(), s.SoapHandler.SaveAsesmedKeperawatanBidanFiberHandler)
	api.Get("asesmen-keperawatan-bidan", rest.JWTProtected(), s.SoapHandler.GetAsesmedKeperawatanBidanFiberHandler)

	// ================================== DPPCP SOAP
	api.Get("cppt-pasien", rest.JWTProtected(), s.RMEHandler.CPPTPasien)
	api.Delete("cppt-pasien", rest.JWTProtected(), s.RMEHandler.OnDeleteCPPTPasienHandler)
	api.Post("cppt-pasien", rest.JWTProtected(), s.RMEHandler.InsertCPPTPasien)
	api.Patch("cppt-pasien", rest.JWTProtected(), s.RMEHandler.UpdateCPPTFiberHandler)
	// CPPT PASIEN
	api.Put("cppt-pasien", rest.JWTProtected(), s.ReportHandler.GetReportCpptPasienHandler)
	apiV2.Put("cppt-pasien", rest.JWTProtected(), s.ReportHandler.GetReportCpptPasienHandlerV2)

	// ================================= ASESMEN DOKTER ================================= //
	api.Post("asesmen-rawat-jalan-dokter", rest.JWTProtected(), s.RMEHandler.CariDeskripsiSiki)
	api.Post("riwayat-penyakit", rest.JWTProtected(), s.RMEHandler.CariDeskripsiSiki)

	// PEMERIKSAAN  ============= FISIK ================IGD
	api.Post("asesmen-pemeriksaan-fisik", rest.JWTProtected(), s.RMEHandler.SaveAsesmenPemeriksaanFisikIGD)
	api.Get("asesmen-pemeriksaan-fisik", rest.JWTProtected(), s.RMEHandler.GetAsesmenPemeriksaanFisikIGD)

	// PEMERIKSAAN FISIK BANGSAL
	api.Post("asesmen-pemeriksaan-fisik-bangsal", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikBangsal)
	api.Get("asesmen-pemeriksaan-fisik-bangsal", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikBangsal)
	api.Get("asesmen-pemeriksaan-fisik-bangsal-dokter", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikBangsalDokter)

	//============PEMERIKSAAN FISIK ANAK  ================== //
	api.Post("pemeriksaan-fisik-anak", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikAnak)
	api.Get("pemeriksaan-fisik-anak", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanAnakHandler)

	// VITAL SIGN TIDAK DI PAKAI, MENGGUNAKAN YANG BARU
	api.Post("dvital-sign", rest.JWTProtected(), s.RMEHandler.SaveVitalSignBangsal)
	api.Get("dvital-sign", rest.JWTProtected(), s.RMEHandler.GetVitalSignBangsal)

	// VITAL SIGN GANGGUAN PERILAKU
	// VITAL SIGN GANGGUAN PERILAKU
	api.Post("vital-sign-gangguan-perilaku", rest.JWTProtected(), s.RMEHandler.SaveGangguanPerilaku)
	api.Get("vital-sign-gangguan-perilaku", rest.JWTProtected(), s.RMEHandler.GetGangguanPerilakuFiberHandler)

	// VITAL SIGN DOKTER IGD
	api.Get("vital-sign-igd", rest.JWTProtected(), s.RMEHandler.GetVitalSignIGDHandler)
	api.Post("vital-sign-igd", rest.JWTProtected(), s.RMEHandler.SaveVitalSignDokter)

	// ============================== SKALA NYERI ================================== //
	api.Post("skala-nyeri", rest.JWTProtected(), s.RMEHandler.SaveTriaseSkalaNyeriHandler)
	api.Get("skala-nyeri", rest.JWTProtected(), s.RMEHandler.GetTriaseSkalaNyeriHandler)

	// SKALA TRIASE IGD
	api.Post("skala-nyeri-igd", rest.JWTProtected(), s.RMEHandler.SaveTriaseSkalaNyeriHandler)

	// ================================== ASESMEN AWAL KEPERAWATAN / BIDAN DI RUANGAN GAWAT DARURAT
	api.Post("asesmen-info-keluhan-igd", rest.JWTProtected(), s.SoapHandler.SaveAsesmenInfoKeluhanIGdHandler)
	api.Get("asesmen-info-keluhan-igd", rest.JWTProtected(), s.SoapHandler.GetAsesmenInfoKeluhanIGdHandler)

	// ================================= GET ASESMEN AWAL KEPERAWATAN IGD // ================ //
	api.Get("asesmen-awal-igd", rest.JWTProtected(), s.SoapHandler.OnGetAsesmenAwalIGDHangler)
	api.Post("asesmen-awal-igd", rest.JWTProtected(), s.SoapHandler.OnSaveAsesmenAwalIGDFiberHandler)

	// RESIKO JATUH GET UP AND GO TEST
	api.Get("resiko-jatuh-getup-igd", rest.JWTProtected(), s.SoapHandler.GetAsesmenResikoJatuhGoUpAndGoTestFiberHandler)
	api.Post("resiko-jatuh-getup-igd", rest.JWTProtected(), s.SoapHandler.OnSaveAsesmenResikoJatuhGoUpAndGoTestFiberHandler)

	// ================================= ASESMEN //
	api.Post("skrining-resiko-dekubitus", rest.JWTProtected(), s.SoapHandler.SaveSkriningResikoDekubituIGDHandler)
	api.Get("skrining-resiko-dekubitus", rest.JWTProtected(), s.SoapHandler.GetSkriningResikoDekubitusHandler)

	// =================================
	api.Get("riwayat-kehamilan", rest.JWTProtected(), s.SoapHandler.GetRiwayatKehamilanHandler)
	api.Post("riwayat-kehamilan", rest.JWTProtected(), s.SoapHandler.SaveRiwayatKehamilanHandler)

	// ==================================== SKRINING NYERI ========================== //
	api.Get("skrining-nyeri", rest.JWTProtected(), s.SoapHandler.GetSkriningNyeriHandler)
	api.Post("skrining-nyeri", rest.JWTProtected(), s.SoapHandler.SaveSkriningNyeriHandler)

	// ============================== TINDAK LANJUT ============================== //
	api.Get("tindak-lanjut", rest.JWTProtected(), s.SoapHandler.GetTindakLanjutHandler)
	api.Post("tindak-lanjut", rest.JWTProtected(), s.SoapHandler.SaveTindakLanjuthandler)

	// RIWAYAT PENGGAJIAN IGD
	api.Get("riwayat-asesmen-igd", rest.JWTProtected(), s.RMEHandler.CPPTPasien)

	// SIMPAN DATA ASESMEN RAWAT INAP

	api.Post("keadaan-umum-bangsal", rest.JWTProtected(), s.SoapHandler.SaveKeadaanUmumBangsalFiberHandler)
	api.Get("keadaan-umum-bangsal", rest.JWTProtected(), s.SoapHandler.GetKeadaanUmumBangsalFiberHandler)

	// =========================== ASESMEN DOKTER ========================================= //
	api.Get("asesmen-dokter", rest.JWTProtected(), s.SoapHandler.GetAsesmenDokterFiberHandler)
	api.Post("asesmen-dokter", rest.JWTProtected(), s.SoapHandler.SaveAsesmenDokterFiberHandler)
	// FIND ASESMEN FROM DOKTER TO PERAWAT
	api.Get("asesmen-dokter-perawat", rest.JWTProtected(), s.SoapHandler.FindAsesmenDokterToPerawatFiberHandler)

	// =========================== ASESMEN PERAWAT ======================================== //

	// =============================== RIWAYAT KELUHAN UTAMA PASIEN ====================== //
	api.Get("history-asesmen-pasien", rest.JWTProtected(), s.SoapHandler.HistoryAsesmenPasienFiberHandler)

	// RISIKO JATU PASIEN // INTERVENSI RESIKO JATUH
	// DETEKSI RESIKO JATUH
	api.Get("resiko-jatuh", s.LibHandler.GetResikoJatuhPasienFiberHandler)
	api.Post("resiko-jatuh", rest.JWTProtected(), s.RMEHandler.SaveResikoJatuhPasienFiberHandler)
	api.Get("reasesmen-resiko-jatuh-anak", s.LibHandler.GetReasesmenResikoJatuhAnakFiberHandler)

	api.Post("deteksi-resiko-jatuh", rest.JWTProtected(), s.RMEHandler.DeteksiResikoJatuhPasienFiberHandler)

	// INTERVENSI RESIKO JATUH PASIEN
	api.Post("intervensi-resiko-jatuh", rest.JWTProtected(), s.RMEHandler.SaveIntervensiResikoJatuhFiberHandler)
	api.Get("intervensi-resiko-jatuh", rest.JWTProtected(), s.RMEHandler.SaveIntervensiResikoJatuhFiberHandler)

	// LAKUKAN GENERATE INTERVENSI PADA DATA SIKI
	api.Get("generate-siki", s.RMEHandler.GenerateDataSIKIFiberHandler)
	api.Get("generate-kriteria-slki", s.RMEHandler.GenerateKriteriaSLKIFIberHandler)
	api.Get("generate-kriteria-sdki", s.RMEHandler.GenerateSDKIFiberHandler)
	api.Get("generate-kode-sdki", s.RMEHandler.GenerateKodeSDKIFiberHandler)

	// GET DESKRIPSI SIKI
	api.Post("search-deskripsi-siki", rest.JWTProtected(), s.RMEHandler.CariDeskripsiSiki)

	// LUARAH SLKI SIMPAN-DESKRIPSI-SLKI
	api.Post("deskripsi-luaran-slki", s.RMEHandler.GetDeskripsiLuaranSDKIFiberHandler)
	api.Post("save-deskripsi-luaran-slki", rest.JWTProtected(), s.RMEHandler.SaveDeskripsiLuaranSDKIFiberHandler)
	api.Post("save-tindakan-slki", rest.JWTProtected(), s.RMEHandler.OnSaveImplementasiTindakanSKLIFiberHandler)

	// GET DATA DESKRIPSI LUARAN SLKI // ON SAVE DATA INTERVENSI
	api.Get("asuhan-keperawatan", rest.JWTProtected(), s.RMEHandler.GetDeskripsiAsuhanKeperawatanFiberHandler)
	api.Delete("asuhan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnDeleteAsuhanKeperawatanFiberhandler)
	apiV2.Get("asuhan-keperawatan", rest.JWTProtected(), s.RMEHandler.GetDeskripsiAsuhanKeperawatanFiberHandlerV2)

	apiV3.Get("asuhan-keperawatan", rest.JWTProtected(), s.RMEHandler.GetDeskripsiAsuhanKeperawatanFiberHandlerV3)

	api.Post("daskep-slki", rest.JWTProtected(), s.RMEHandler.OnSaveDaskepSLKIFiberHandler)
	api.Post("daskep-slki-all", rest.JWTProtected(), s.RMEHandler.OnSaveDataDaskepAllFiberHandler)
	api.Post("closed-daskep-slki", rest.JWTProtected(), s.RMEHandler.OnClosedDaskepSLKIFiberHandler)

	// ============================ ALERGI OBAT ==================================================== //
	// TAMPILKAN ALERGI OBAT
	api.Post("alergi", rest.JWTProtected(), s.RMEHandler.OnSaveAlergiObatFiberHandler)
	api.Get("alergi", rest.JWTProtected(), s.RMEHandler.OnGetAlergiObatFiberHandler)
	api.Delete("alergi", rest.JWTProtected(), s.RMEHandler.OnDeleteAlergiObatFiberHandler)

	// REPORT APP
	// GET DATA OBAT // TAMBAH RIWAYAT ALERGI KELUARGA
	api.Get("inventory-obat", s.LibHandler.GetAllKInventoryObatFiberHandler)
	api.Post("alergi-keluarga", rest.JWTProtected(), s.RMEHandler.OnSaveRiwayatPenyakitKeluargahandler)
	api.Delete("alergi-keluarga", rest.JWTProtected(), s.RMEHandler.OnDeletePenyakitKeluargaHandler)
	api.Get("alergi-keluarga", rest.JWTProtected(), s.RMEHandler.OnGetRiwayatPenyakitKeluargaHandler)

	// =============================== GAMBAR IMAGE LOKALIS UNTUK PABLIC ================================== //
	api.Get("triase-report", rest.JWTProtected(), s.ReportHandler.ReportTriaseIGDFiberHandler)
	api.Get("ringkasan-pulang", rest.JWTProtected(), s.ReportHandler.ReportRingkasanPulangIGDFiberhandler)

	//  REPORT
	api.Get("pengkajian-rawat-inap-anak", rest.JWTProtected(), s.ReportHandler.ReportPengkajianRawatInapAnak)
	api.Post("report-intervensi-resiko-jatuh", rest.JWTProtected(), s.ReportHandler.ReportIntervensiResikoJatuhHandler)
	api.Post("report-resiko-jatuh-anak", rest.JWTProtected(), s.ReportHandler.GetResikoJatuhAnakHandler)
	api.Post("report-resiko-reasesmen-jatuh-anak", rest.JWTProtected(), s.ReportHandler.GetResikoJatuhAnakHandler)
	api.Post("report-resiko-jatuh-morse", rest.JWTProtected(), s.ReportHandler.GetResikoJatuhMorseHandler)
	//====//
	api.Post("report-resiko-jatuh-reasesmen-anak", rest.JWTProtected(), s.ReportHandler.GetReasesmenResikoJatuhAnakHandler)

	api.Post("report-resiko-jatuh-dewasa", rest.JWTProtected(), s.ReportHandler.GetResikoJatuhDewasaHandler)
	api.Get("report-perkembangan-pasien", rest.JWTProtected(), s.ReportHandler.GetReportPelaksanaanPasienHandler)
	api.Get("report-intervensi", rest.JWTProtected(), s.ReportHandler.GetViewIntervensiPasienHandler)
	// TODO : PASIEN
	apiV2.Get("report-intervensi", s.ReportHandler.GetViewIntervensiPasienHandlerV2)

	// BHP HD
	api.Get("bhp-dializer", rest.JWTProtected(), s.RMEHandler.GetBHPHDHandler)

	// DIAGNOSA KEBIDANAN
	api.Get("diagnosa-kebidanan", rest.JWTProtected(), s.KebidananHandler.GetVitalSignBangsalBidan)
	api.Post("diagnosa-kebidanan", rest.JWTProtected(), s.RMEHandler.GetBHPHDHandler)
	api.Delete("diagnosa-kebidanan", rest.JWTProtected(), s.RMEHandler.GetBHPHDHandler)

	// ======================================= RIWAYAT PENGOBATAN DI RUMAH
	api.Post("riwayat-pengobatan-dirumah", rest.JWTProtected(), s.KebidananHandler.InsertRiwayatPengobatanDirumahFiberHandler)
	api.Get("riwayat-pengobatan-dirumah", rest.JWTProtected(), s.KebidananHandler.OnGetRiwayatPengobatanDirumahFiberHandler)
	api.Delete("riwayat-pengobatan-dirumah", rest.JWTProtected(), s.KebidananHandler.OnDeleteRiwayatPengobatanDirumahFiberHandler)
	api.Put("riwayat-pengobatan-dirumah", rest.JWTProtected(), s.KebidananHandler.OnUpdateRiwayatPengobatanDirumahFiberHandler)
	// ======================================= RIWAYAT PENGOBATAN DI RUMAH
	api.Get("riwayat-kehamilan-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetRiwayatKehamilanFiberHandler)
	api.Post("riwayat-kehamilan-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSaveRiwayatKehamilanFiberHandler)
	api.Delete("riwayat-kehamilan-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnDeleteRiwayatKehamilanFiberHandler)

	// ======================== KEBIDANAN =====
	api.Get("resiko-jatuh-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetResikoJatuhKebidananFiberHandler)
	api.Post("resiko-jatuh-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSaveResikoJatuhFiberhandler)

	api.Post("dvital-sign-bidan", rest.JWTProtected(), s.KebidananHandler.OnSaveVitalSignBangsalBidan)
	api.Get("dvital-sign-bidan", rest.JWTProtected(), s.KebidananHandler.GetVitalSignBangsalBidan)

	//GET FUNGSIONAL //
	api.Get("pengkajian-fungsional", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianFungsionalFiberHandler)
	api.Post("pengkajian-fungsional", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianFungsionalFiberHandler)

	// ==== //
	api.Get("pengkajian-nutrisi", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianNutrisiFiberHandler)
	api.Post("pengkajian-nutrisi", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianNutrisiFiberHandler)

	// ================================ //
	api.Get("pemeriksaan-fisik-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetPemeriksaanFisikKebidananFiberHandler)
	api.Post("pemeriksaan-fisik-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSavePemeriksaanFisikKebidahanFiberHandler)

	// DIAGNOSA KEBIDANAN
	api.Put("input-all-diagnosa-kebidanan", s.KebidananHandler.InputAllDiagnosaKebidananHandler)

	api.Get("data-diagnosa-kebidanan", s.KebidananHandler.GetAllDiagnosaKebidananHandler)
	api.Post("data-diagnosa-kebidanan", rest.JWTProtected(), s.KebidananHandler.SaveDiagnosaKebidananHandler)
	api.Patch("data-diagnosa-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnPatchDataDiagnosaKebidananHandler)
	api.Delete("data-diagnosa-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnDeleteDataDiagnosaKebidananhandler)

	// REPORT IMPLEMENTASI KEBIDANAN
	api.Patch("report-data-diagnosa-kebidanan", s.KebidananHandler.OnGetReportDiagnoasaKebidananFiberHandler)

	// PEMERIKSAAN FISIK KEBIDANAN
	api.Get("pengkajian-fisik-kebidanan", rest.JWTProtected(), s.KebidananHandler.GetPengkajianFisikKebidananHandler)

	// RESEP OBAT
	api.Get("resep-obat", rest.JWTProtected(), s.HisHandler.GetKtaripPersediaanObatFiberHandler)
	api.Post("resep-obat", rest.JWTProtected(), s.HisHandler.SaveKtaripResepObatFiberHandler)
	api.Post("resep-obat-manual", rest.JWTProtected(), s.HisHandler.SaveKtaripResepObatFiberHandler)
	apiV2.Post("resep-obat", rest.JWTProtected(), s.HisHandler.SaveResepObatV2FiberHandler)
	// ==== RIWAYAT RESEP
	api.Get("history-resep-obat", rest.JWTProtected(), s.HisHandler.OnGetHistoryResepObatFiberHandler)
	api.Post("simpan-resep-manual", rest.JWTProtected(), s.HisHandler.OnSaveResepManualFiberHandler)
	api.Get("history-resep-manual", rest.JWTProtected(), s.HisHandler.OnGetHistoryResepManualFiberHandler)

	// IGD LOGIN DOKTER
	//====================== KELUHAN UTAMA DOKTER UNTUK RAWAT INAP DAN RAWAT JALAN
	api.Get("keluhan-utama-dokter-igd", rest.JWTProtected(), s.RMEHandler.KeluhanUtamaIGDFiberHandler)
	api.Post("keluhan-utama-dokter-igd", rest.JWTProtected(), s.RMEHandler.SaveKeluhanUtamaFiberHandler)
	api.Get("riwayat-keluhan-utama-dokter-igd", rest.JWTProtected(), s.RMEHandler.OnGetRiwayatKeluhanUtamaIGDDokterFiberHandler)

	// ASESMEN IGD BANGSAL RAWAT INAP
	api.Get("keluhan-utama-dokter-bangsal", rest.JWTProtected(), s.RMEHandler.OnGetKeluhanUtamaRANAP_FIBER_HANDLER)

	api.Post("riwayat-penyakit-keluarga-dokter-igd", rest.JWTProtected(), s.RMEHandler.SaveRiwayatPenyakitKeluargaFiberHandler)
	api.Put("report-asesmen-awal-medis-dokter-igd", s.RMEHandler.OnGetReportAsesmenAwalMedisFiberHandler)
	api.Put("report-asesmen-awal-medis-dokter-igd-antonio", s.RMEHandler.OnGetAsesmenAwalMedisDokterFiberAntonioHandler)

	// REPORT ASESMEN AWAL DOKTER IGD HARAPAN
	api.Put("report-asesmen-awal-medis-dokter-igd-harapan", s.RMEHandler.OnGetReportAsesmenAwalHarapanMedisFiberHandler)

	// =================================== IGD ======================================================
	api.Post("pemeriksaan-fisik-dokter-igd", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikIGDDokterFiberHandler)
	api.Get("pemeriksaan-fisik-dokter-igd", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikIGDDokterFiberHandler)
	api.Get("pemeriksaan-fisik-perawat-igd", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikIGDPerawatFiberHandler)

	// =================================== IGD-METHODIST ======================================================
	api.Get("pemeriksaan-fisik-dokter-igd-methodist", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikIGDDokterMethodistFiberHandler)
	api.Post("pemeriksaan-fisik-dokter-igd-methodist", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikIGDDokterMetodistFiberHandler)
	api.Get("pemeriksaan-fisik-dokter-antonio", rest.JWTProtected(), s.RMEHandler.OnGetPemeriksaanFisikDokterAntonioFiberHandler)
	api.Post("pemeriksaan-fisik-dokter-antonio", rest.JWTProtected(), s.RMEHandler.OnSavePemeriksaanFisikDokterAntonioFiberHandler)

	// ================ PEMeRIKSAAN FISIK IGD DOKTER
	api.Get("pemeriksaan-fisik-dokter-igd-antonio", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikIGDDokterAntonioFiberHandler)
	api.Post("pemeriksaan-fisik-dokter-igd-antonio", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikIGDDokterMetodistFiberHandler)

	// ================= PEMERIKSAAN FISIK RAWAT INAP
	api.Get("pemeriksaan-fisik-dokter-inap-antonio", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikIGDDokterAntonioFiberHandler)

	api.Post("rencana-tindak-lanjut-igd-dokter", rest.JWTProtected(), s.SoapHandler.InsertRencanaTindakLanjutFiberHandler)
	api.Get("rencana-tindak-lanjut-igd-dokter", rest.JWTProtected(), s.SoapHandler.GetRencanaTindakLanjutFiberHandler)
	api.Get("tanda-vital-igd-dokter", rest.JWTProtected(), s.SoapHandler.GetTandaVitalIGDDokterFiberHandler)

	// TANDA VITAL
	api.Get("tanda-vital-igd-perawat", rest.JWTProtected(), s.SoapHandler.GetTandaVitalPerawatIGDFiberHandler)
	api.Post("tanda-vital-igd-dokter", rest.JWTProtected(), s.SoapHandler.OnSaveTandaVitalIGDDokterFiberHandler)

	// ======================== KEPERAWATAN BANGSAL API ==========================================
	api.Post("pengkajian-awal-perawat", rest.JWTProtected(), s.SoapHandler.OnSavePengkajianAwalPerawatFiberHandler)
	api.Get("pengkajian-awal-perawat", rest.JWTProtected(), s.SoapHandler.GetPengkajianPerawatFiberHandler)

	api.Post("tanda-vital-bangsal-keperawatan", rest.JWTProtected(), s.SoapHandler.OnSaveTandaKeperawatanBangsalFiberHandler)
	api.Get("tanda-vital-bangsal-keperawatan", rest.JWTProtected(), s.SoapHandler.GetTandaVitalKeperawatanBangsalFiberHandler)

	// =========================== KEBIDANAN BANGSAL API ======================================
	api.Post("pengkajian-awal-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSaveAsesmenKebidananHandler)
	api.Get("pengkajian-awal-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetAsesmenKebidahanHandler)

	//=================== PENGKAJIAN AWAL KEBIDANAN RS ANTONIO

	// TODO: PENGKAJIAN KEBIDANAN
	api.Get("pengkajian-persistem-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianPersistemHandler)
	api.Post("pengkajian-persistem-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianPerSistemKebidananHandler)

	api.Post("pengkajian-pebidanan", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianPerSistemHandler)
	api.Get("pengkajian-pebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianKebidananHandler)

	// DIAGNOSA BANDING IGD DOKTER
	api.Get("diagnosa-banding-dokter", rest.JWTProtected(), s.KebidananHandler.OnGetDiagnosaBandingHandler)
	api.Post("diagnosa-banding-dokter", rest.JWTProtected(), s.KebidananHandler.OnSaveDiagnosaBandingHandler)

	// CPPT SBAR
	api.Get("cppt-sbar-bangsal", rest.JWTProtected(), s.RMEHandler.GetCPPTSBARPerawatFiberHandler)
	api.Post("cppt-sbar-bangsal", rest.JWTProtected(), s.RMEHandler.OnSaveSBARFiberHandler)
	api.Put("cppt-sbar-bangsal", rest.JWTProtected(), s.RMEHandler.OnUpdateCPPTSBARFiberHandler)
	api.Delete("cppt-sbar-bangsal", rest.JWTProtected(), s.RMEHandler.OnDeleteCpptSBARFiberHandler)

	// SIMPAN DATA RIWAYAT PENYAKIT KELUARGA
	api.Post("riwayat-penyakit-keluarga-bangsal", rest.JWTProtected(), s.RMEHandler.OnSaveRiwayatPenyakitKeluargaFiberHandler)

	// ===================== PENGKAJIAN PERSISTEM KEPERWATAN ============================ //
	api.Get("pengkajian-persistem-keperawatan", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianPersistemKeperawatanHandler)
	api.Post("pengkajian-persistem-keperawatan", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianPersistemKeperawatanFiberHandler)

	// ============ PENGKAJIAN PERSISTEM ANAK
	api.Get("pengkajian-persistem-anak-keperawatan", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianPersistemAnakKeperawatanFiberHandler)
	api.Post("pengkajian-persistem-anak-keperawatan", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianPersistemAnakKeperawatanFiberHandler)

	// ASESMEN KEBIDANAN
	api.Get("asesmen-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnGetAsesmenFiberHandler)
	api.Post("asesmen-kebidanan", rest.JWTProtected(), s.KebidananHandler.OnSaveAsesmenKebidananFiberHandler)

	// REPORT PENGKAJIAN AWAL KEBIDANAN
	api.Put("report-pengkajian-kebidanan", rest.JWTProtected(), s.KebidananHandler.ReportAsesmenPengkajianKebidananFiberHandler)
	api.Put("report-pengkajian-kebidanan-antonio", rest.JWTProtected(), s.KebidananHandler.ReportAsesmenPengkajianKebidananAntonioFiberHandler)

	// TRIASE IGD ===================== //
	api.Get("triase-igd-dokter", rest.JWTProtected(), s.KebidananHandler.TriaseIGDDokterKebidananFiberHandler)
	api.Post("triase-igd-dokter", rest.JWTProtected(), s.KebidananHandler.OnSaveTriaseIGDDokterKebidananFiberHandler)
	api.Put("triase-igd-dokter", rest.JWTProtected(), s.KebidananHandler.OnReportTriaseIGDDokterFiberHandler)
	api.Put("triase-ponek", rest.JWTProtected(), s.KebidananHandler.OnReportTriasePonekFiberHandler)

	// REPORT PENGKAJIAN AWAL DOKTER
	api.Put("pengkajian-awal-dokter", rest.JWTProtected(), s.KebidananHandler.OnReportPengkajianAwalMedisHandler)
	api.Put("report-pengkajian-awal-perawat", rest.JWTProtected(), s.KebidananHandler.OnReportPengkajianAwalKeperawatanFiberHandler)

	api.Post("pemeriksaan-fisik-bangsal", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikBangsalFiberHandler)
	api.Get("pemeriksaan-fisik-bangsal", rest.JWTProtected(), s.RMEHandler.OnGetPemeriksaanFisikBangsal)

	// ASESMEN PERINA BAYI
	api.Post("riwayat-kelahiran-lalu", rest.JWTProtected(), s.RMEHandler.SaveRiwayatKelahiranLaluPerinaFiberHandler)
	api.Put("riwayat-kelahiran-lalu", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikBangsalFiberHandler)
	api.Delete("riwayat-kelahiran-lalu", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikBangsalFiberHandler)

	// GET ASESMEN BAYI ========================= ASESMEN PERINA
	// ===== FITAL SIGN PERINA
	api.Get("asesmen-bayi", rest.JWTProtected(), s.RMEHandler.OnGetAsesmenBayiBangsalFiberHandler)
	api.Post("asesmen-bayi", rest.JWTProtected(), s.RMEHandler.OnSaveAsesmenBayiBangsalFiberHandler)
	api.Post("riwayat-kelahiran-lalu-perina", rest.JWTProtected(), s.RMEHandler.OnSaveRiwayatKelahiranYangLaluPerinaFiberHandler)
	api.Delete("riwayat-kelahiran-lalu-perina", rest.JWTProtected(), s.RMEHandler.OnDeleteRiwayatKelahiranYangLaluPerinaFiberHandler)

	api.Post("pemeriksaan-fisik-perina", rest.JWTProtected(), s.RMEHandler.SavePemeriksaanFisikPerinaFiberHandler)
	api.Get("pemeriksaan-fisik-perina", rest.JWTProtected(), s.RMEHandler.OnGetPemeriksaanFisikPerinaFiberHandler)

	api.Post("dvital-sign-perina", rest.JWTProtected(), s.RMEHandler.OnSaveDVitalSignPerinaFiberHandler)
	api.Get("dvital-sign-perina", rest.JWTProtected(), s.RMEHandler.OnGetDVitalSignFiberHandler)

	// DOWN-SCORE PADA NEONATUS
	api.Get("ddown_score_neonatus-perina", rest.JWTProtected(), s.RMEHandler.OnGetDownScoreNeoNatusFiberHandler)
	api.Post("ddown_score_neonatus-perina", rest.JWTProtected(), s.RMEHandler.OnSaveDownScoreNeoNatusFiberHandler)

	api.Post("move-to-pengajar", s.UserHandler.MoveDokterToPengajar)
	api.Get("dapgar-score-perina", rest.JWTProtected(), s.RMEHandler.OnGetDataApgarScoreNeoNatusFiberHandler)
	api.Post("dapgar-score-perina", rest.JWTProtected(), s.RMEHandler.OnSaveDataApgarScoreFiberHandler)

	// REPORT ASESMEN KEPERWATAN BAYI BARU LAHIR
	api.Put("report-asesmen-bayi", rest.JWTProtected(), s.RMEHandler.OnReportFormulirBayiFiberHandler)
	api.Get("diagnosa-keperawatan", rest.JWTProtected(), s.RMEHandler.OnGetAllDiagnosaKeperawatan)

	// ==== ANALISA DATA
	api.Post("analisa-data", rest.JWTProtected(), s.RMEHandler.OnSaveAnalisaDataFiberHandler)
	api.Get("analisa-data", rest.JWTProtected(), s.RMEHandler.OnGetAnalisaDataFiberHandler)
	api.Delete("analisa-data", rest.JWTProtected(), s.RMEHandler.OnDeleteAnalisaDataFiberHandler)

	// ==== VALIDASI ANALISA DATA
	api.Post("validasi-analisa-data", rest.JWTProtected(), s.RMEHandler.OnValidaasiAnalisaDataFiberHandler)
	api.Post("validasi-analisa-data-enum", rest.JWTProtected(), s.RMEHandler.OnValidasiAnalisaDataEnumFiberHandler)
	api.Put("report-analisa-data", rest.JWTProtected(), s.RMEHandler.OnReportAnalisaDataFiberHandler)

	api.Get("tindak-lanjut-perina", rest.JWTProtected(), s.RMEHandler.OnGetTindakLanjutPerinaFiberHandler)
	api.Post("tindak-lanjut-perina", rest.JWTProtected(), s.RMEHandler.OnSaveTindakLanjutPerinatologiFiberHandler)
	api.Put("report-resume-medis-perinatologi", rest.JWTProtected(), s.RMEHandler.OnGetReportResumeMedisPerinatologiFiberHandler)
	api.Put("report-identias-bayi-perinatologi", rest.JWTProtected(), s.RMEHandler.OnGetReportIdentitasBayiPerinatologiFiberHandler)

	// TODO : ASESMEN SKALA NYERI DEWASA
	api.Get("asesmen-skala-nyeri", rest.JWTProtected(), s.RMEHandler.OnGetAsesmenNyeriDewasaFiberHandler)
	api.Post("asesmen-skala-nyeri", rest.JWTProtected(), s.RMEHandler.OnSaveAsesmenSkalaNyeriFiberHandler)
	api.Put("report-asesmen-skala-nyeri", rest.JWTProtected(), s.RMEHandler.OnReportAsesmenNyeriFiberHandler)
	// ASESMEN SKALA NYERI BIDAN
	api.Get("asesmen-skala-nyeri-bidan", rest.JWTProtected(), s.RMEHandler.OnGetAsesmenSkalaNyeriBidanFiberHandler)

	// IDENTITAS BAYI
	api.Post("upload-identitas-bayi", rest.JWTProtected(), s.RMEHandler.UploadIdentitasBayiFiberHandler)
	api.Get("identitas-bayi", rest.JWTProtected(), s.RMEHandler.OnGetIdentitasBayiBayiHandler)
	api.Post("identitas-bayi", rest.JWTProtected(), s.RMEHandler.OnSaveIndentitasBayiHandler)

	// RINGKASAN MASUK DAN KELUAR REPORT API
	api.Put("report-ringkasan-masuk-dan-keluar", rest.JWTProtected(), s.RMEHandler.OnReportRingkasanMasukDanKeluarFiberHandler)

	// ADD CATATAN KEPERAWATAN
	api.Post("catatan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnSaveCatatanKeperawatanHandler)
	api.Delete("catatan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnDeleteCatatanKeperawatanFiberHandler)
	api.Patch("catatan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnUpdateCatatanKeperawatanFiberHandler)
	api.Put("catatan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnGetReportCatatanKeperawtanFiberHandler)

	// EARLY WARNING SYSTEM
	api.Post("early-warning-system", rest.JWTProtected(), s.RMEHandler.OnSaveEarlyWarningSystemHandler)
	api.Get("early-warning-system", rest.JWTProtected(), s.RMEHandler.OnGetEarlyWarningSystemHandler)
	api.Put("early-warning-system", rest.JWTProtected(), s.RMEHandler.OnReportEarlyWarningSystemHandler)
	api.Delete("early-warning-system", rest.JWTProtected(), s.RMEHandler.OnDeleteEarlyWarningSystemHandler)

	// ON SAVE EARLY WARNIG SYSTEM ANAK
	api.Post("early-warning-system-anak", rest.JWTProtected(), s.RMEHandler.OnSaveEarlyWarningSystemHandler)

	// =================================== PEMERIKSAAN FISIK ICU ======================================================
	api.Post("pemeriksaan-fisik-icu", rest.JWTProtected(), s.RMEHandler.OnSavePemeriksaanFisikICUFiberHandler)
	api.Get("pemeriksaan-fisik-icu", rest.JWTProtected(), s.RMEHandler.GetPemeriksaanFisikICUFiberHandler)

	// VITAL SIGN ICU
	api.Get("vital-sign-icu", rest.JWTProtected(), s.RMEHandler.OnGetVitalSignICUHandler)
	api.Post("vital-sign-icu", rest.JWTProtected(), s.RMEHandler.OnSaveVitalSignICUHandler)

	// PENGKAJIAN PERSISTEM ICU
	api.Get("pengkajian-persistem-icu", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianPersistemICUFiberHandler)
	api.Post("pengkajian-persistem-icu", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianPersistemICUFiberHandler)

	// GET ASESMEN KEPERAWATAN INTENSIVE
	api.Get("asesmen-intensive-icu", rest.JWTProtected(), s.KebidananHandler.OnGetAsesmenUlangKeperatanIntenveFiberHandler)
	api.Post("asesmen-intensive-icu", rest.JWTProtected(), s.KebidananHandler.OnSaveAsesmenIntensiveFiberHandler)
	// VITAL SIGN
	api.Post("tanda-vital-bangsal-anak", rest.JWTProtected(), s.SoapHandler.OnSaveTandaKeperawatanBangsalFiberHandler)
	api.Get("tanda-vital-bangsal-anak", rest.JWTProtected(), s.SoapHandler.GetTandaVitalSignBangsalAnakFiberHandler)

	api.Post("pengkajian-awal-perawat-anak", rest.JWTProtected(), s.SoapHandler.OnSavePengkajianAwalPerawatFiberHandler)
	api.Get("pengkajian-awal-perawat-anak", rest.JWTProtected(), s.SoapHandler.OnGetPengkajianKeperawatanAnakFiberHandler)

	// ==== //PENGKAJIAN NUTRISI ANAK
	api.Get("pengkajian-nutrisi-anak", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianNutrisiAnakFiberHandler)
	api.Post("pengkajian-nutrisi-anak", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianNutrisiAnakFiberHandler)

	// TODO: PENGKAJIAN NYERI ANAK
	api.Get("pengkajian-nyeri-anak", rest.JWTProtected(), s.KebidananHandler.OnGetPengkajianNyeriNipsAnakFiberHandler)
	api.Post("pengkajian-nyeri-anak", rest.JWTProtected(), s.KebidananHandler.OnSavePengkajianNyeriNipsAnakFiberHandler)

	// ASESMEN NYERI ICU
	api.Get("asesmen-nyeri-icu", rest.JWTProtected(), s.KebidananHandler.OnGetAsesmenNyeriICUFiberHandler)
	api.Post("asesmen-nyeri-icu", rest.JWTProtected(), s.KebidananHandler.OnSaveAsesmenNyeriICUFiberHandler)
	api.Put("asesmen-perawatan-intensive", rest.JWTProtected(), s.KebidananHandler.OnReportAsesmenIntensiveFiberHandler)
	// REPORT ASESMEN ULANG PERAWATAN INTENSIVE ICU

	// PEMERIKSAAN FISIK
	api.Get("get-pemeriksaan-fisik-anak", rest.JWTProtected(), s.RMEHandler.OnGetPemeriksaanFisikAnakFiberHandler)
	api.Post("save-pemeriksaan-fisik-anak", rest.JWTProtected(), s.RMEHandler.OnSavePemeriksaanFisikAnakFiberHandler)

	// PENGKAJIAN FISIK ANAK
	api.Post("asesmen-awal-anak", rest.JWTProtected(), s.SoapHandler.OnSavePengkajianAwalAnakFiberHandler)
	api.Get("asesmen-awal-anak", rest.JWTProtected(), s.SoapHandler.OnGetAsesmenAnakFiberHandler)

	// ===== //
	api.Get("report-pengkajian-anak", rest.JWTProtected(), s.SoapHandler.OnReportAssesmenAnakFiberHandler)

	// FORMULIR DOUBLE CHECK
	api.Get("double-check", rest.JWTProtected(), s.SoapHandler.OnGetDoubleCheckFiberHandler)
	api.Post("double-check", rest.JWTProtected(), s.SoapHandler.OnSaveDoubleCheckFiberHandler)
	api.Delete("double-check", rest.JWTProtected(), s.SoapHandler.OnDeleteCheckFiberHandler)
	api.Post("double-check-verify", rest.JWTProtected(), s.SoapHandler.OnSaveDoubleCheckVerifyFiberHandler)
	apiV2.Post("double-check-verify", rest.JWTProtected(), s.SoapHandler.OnSaveDoubleCheckVerifyV2FiberHandler)
	api.Put("double-check", s.SoapHandler.OnGetReportDoubleCheckFiberHandler)

	// KARTU OBSERASI
	api.Post("kartu-observasi", rest.JWTProtected(), s.RMEHandler.OnSaveKartuObservasiFiberHandler)
	api.Get("kartu-observasi", rest.JWTProtected(), s.RMEHandler.OnGetKartuObservasiFiberHandler)
	api.Delete("kartu-observasi", rest.JWTProtected(), s.RMEHandler.OnDeleteKartuObservasiFiberHandler)
	api.Put("kartu-observasi", rest.JWTProtected(), s.RMEHandler.OnUpdateKartuObservasiFiberHandler)

	// KARTU CAIRAN
	api.Post("kartu-cairan", rest.JWTProtected(), s.RMEHandler.OnSaveKartuCairanFiberHandler)
	api.Get("kartu-cairan", rest.JWTProtected(), s.RMEHandler.OnGetKartuCairanFiberHandler)
	api.Delete("kartu-cairan", rest.JWTProtected(), s.RMEHandler.OnDeleteKartuCairanFiberHandler)
	api.Put("kartu-cairan", rest.JWTProtected(), s.RMEHandler.OnUpdateKartuCairanFiberhandler)

	// ===== //INSERT DESKRIPSI ACTION // ASUHAN KEPERAWATAN BIDAN
	api.Post("asuhan-keperawatan-bidan", rest.JWTProtected(), s.RMEHandler.SaveAsuhanKeperawatanBidanFiberHandler)
	api.Get("asuhan-keperawatan-bidan", rest.JWTProtected(), s.RMEHandler.GetAsuhanKeperawatanFiberHandler)

	// INSERT DESKRIPSI
	api.Post("insert-deskripsi", rest.JWTProtected(), s.RMEHandler.InsertDeskripsiSIKIFiberHandler)
	api.Post("deskripsi-siki", rest.JWTProtected(), s.RMEHandler.DeskripsiSikiFiberHandler)

	api.Post("add-action", rest.JWTProtected(), s.RMEHandler.OnSaveImplementasiCPPTFiberHandler)
	api.Post("implementasi-tindakan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnSaveImplementasiTindakanSKLIFiberHandler)
	api.Get("tindakan-keperawatan", rest.JWTProtected(), s.RMEHandler.ImplementasiTindakanAsuhanKeperawatanFiberHandler)
	api.Delete("tindakan-keperawatan", rest.JWTProtected(), s.RMEHandler.OnDeleteImplementasiTindakanAsuhanKeperawatanFiberHandler)

	// ========================== PENGKAJIAN PERSISTEM ============================= //
	// api.Get("pengkajian-persistem-igd", rest.JWTProtected(), s.IGDHandler.OnGetAsesmenIGDFiberHandler)
	api.Post("pengkajian-sistem-igd", rest.JWTProtected(), s.RMEHandler.OnSavePengkajianPersistemIGDFiberHandler)
	api.Get("pengkajian-persistem-igd", rest.JWTProtected(), s.RMEHandler.OnGetPengkajianPersistemIGDFiberHandler)

	// ===================================== REPORT ASESMEN AWAL KEPERAWATAN
	api.Put("report-asesmen-awal-keperawatan-igd", s.ReportHandler.OnGetReportAsesmenAwalBatuRajaKeperawatanHandler)
	api.Put("report-asesmen-awal-kebianan-antonio", s.ReportHandler.ReportAsesmenAwalKebidananAntonioFiberHandler)
	api.Put("report-asesmen-awal-keperawatan-igd-harapan", s.ReportHandler.OnGetReportAsesmenAwalHarapanKeperawatanHandler)

	// ===================================== IMAGE ROUTING===============================//
	api.Get("report-pengkajian-awal-keperawatan-anak", rest.JWTProtected(), s.SoapHandler.OnGetPengkajianKeperawatanAnakFiberHandler)
	api.Get("report-pengkajian-awal-keperawatan-dewasa", rest.JWTProtected(), s.SoapHandler.OnReportPengkajianAwalKeperawatanFiberHandler)

	// IMPLEMENTASI KEPERAWATAN
	api.Put("report-implementasi-keperawatan", s.ReportHandler.OnReportDImplementasiKeperawatanAntonioFiberHandler)
	api.Put("update-implementasi-keperawatan", s.ReportHandler.OnUpdateImplementasiKeperawatanFiberHandler)

	api.Put("report-asesmen-dokter-antonio", s.ReportHandler.ReportAsesmenAwalKebidananAntonioFiberHandler)

	api.Get("icd-9", s.LibHandler.OnGetAllKICD9FiberHandler)
	api.Post("tindakan-icd9", rest.JWTProtected(), s.SoapHandler.InputSingleDiagnosaFiberHandler)
	api.Get("tindakan-icd9", rest.JWTProtected(), s.SoapHandler.GetTindakanICD9FiberHandler)
	api.Delete("tindakan-icd9", rest.JWTProtected(), s.SoapHandler.OnDeleteTindakanICD9FiberHandler)
	api.Put("tindakan-icd9", rest.JWTProtected(), s.SoapHandler.OnAddTindakanICD9FiberHandler)

	// ==================== //
	api.Get("report-asesmen-rawat-inap-antonio", rest.JWTProtected(), s.SoapHandler.OnGetAsesmenReportAwalRawatInalDokterFiberHandler)
	api.Get("riwayat-kunjungan", s.HisHandler.RiwayatKunjunganFiberHandler)
	api.Get("riwayat-perawatan-pasien", s.HisHandler.RiwayaatPerawatanPasienFiberHandler)

	// REPORT PENGKAJIAN RAWAT INAP ANAK
	api.Get("report-pengkajian-rawat-inap-anak", s.HisHandler.RiwayaatPerawatanPasienFiberHandler)
	api.Get("instruksi-medis-farmakologi", rest.JWTProtected(), s.HisHandler.InStruksiMedisFarmatologiFiberHandler)
	api.Post("instruksi-medis-farmakologi", rest.JWTProtected(), s.HisHandler.OnSaveInstruksiMedisFarmakologiFiberHandler)
	api.Put("instruksi-medis-farmakologi", rest.JWTProtected(), s.HisHandler.OnGetReportInstruksiMedisFarmakologiFiberHandler)
	api.Get("pemberi-instruksi-medis-farmakologi", rest.JWTProtected(), s.HisHandler.OnGetPemberiInstruksiFarmakologiFiberHandler)

	api.Get("view-obat-double-check", rest.JWTProtected(), s.HisHandler.OnViewObatDoubleCheckFiberHandler)

	FiberRoutingAndListenV2(Logging, Store, app, s)
	FiberRoutingDiagnosaAndListenV2(Logging, Store, app, s)
	FiberRoutingIGDAndListenV2(Logging, Store, app, s)
	FiberRoutingEarlySystem(Logging, Store, app, s)

	err := app.Listen(os.Getenv("DEPLOY_PORT"))

	exception.PanicIfNeeded(err)
}

func prometheusMiddleware() fiber.Handler {
	return func(c *fiber.Ctx) error {
		start := time.Now()
		method := c.Method()
		path := c.Path()

		err := c.Next()

		status := c.Response().StatusCode()
		duration := time.Since(start)

		httpRequestsTotal.WithLabelValues(method, path, string(status)).Inc()
		httpRequestDuration.WithLabelValues(method, path).Observe(duration.Seconds())

		return err
	}
}
