package main

import (
	"hms_api/config"
	"hms_api/pkg/logs"
	"os"

	_ "hms_api/docs"

	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/joho/godotenv"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"

	// RME
	handlerRME "hms_api/modules/rme/handler"
	rmeMapper "hms_api/modules/rme/mapper"
	repositoryRME "hms_api/modules/rme/repository"
	rmeUseCase "hms_api/modules/rme/usecase"

	handlerHis "hms_api/modules/his/handler"
	hisMapper "hms_api/modules/his/mapper"
	repositoryHis "hms_api/modules/his/repository"

	hisUseCase "hms_api/modules/his/usecase"

	// REPORT
	handlerReport "hms_api/modules/report/handler"
	reportMapper "hms_api/modules/report/mapper"
	repositoryReport "hms_api/modules/report/repository"
	reportUsecase "hms_api/modules/report/usecase"

	// ANTREAN
	handlerAntrean "hms_api/modules/antrean/handler"
	antreanMapper "hms_api/modules/antrean/mapper"
	repositoryAntrean "hms_api/modules/antrean/repository"
	antreanUseCase "hms_api/modules/antrean/usecase"

	// KEBIDANAN
	handlerKebidanan "hms_api/modules/kebidanan/handler"
	mapperKebidanan "hms_api/modules/kebidanan/mapper"
	repoKebidanan "hms_api/modules/kebidanan/repository"
	kebidananUsecase "hms_api/modules/kebidanan/usecase"

	//SOAP
	handlerSoap "hms_api/modules/soap/handler"
	soapMapper "hms_api/modules/soap/mapper"
	repositorySoap "hms_api/modules/soap/repository"
	soapUseCase "hms_api/modules/soap/usecase"

	// USER
	handlerUser "hms_api/modules/user/handler"
	userMapper "hms_api/modules/user/mapper"
	repositoryUser "hms_api/modules/user/repository"
	userUseCase "hms_api/modules/user/usecase"

	// LIBRARY
	handlerLib "hms_api/modules/lib/handler"
	libMapper "hms_api/modules/lib/mapper"
	repositoryLib "hms_api/modules/lib/repository"
	libUseCase "hms_api/modules/lib/usecase"

	// REGIONAL
	handlerReg "hms_api/modules/regional/handler"
	repoReg "hms_api/modules/regional/repository"

	handlerVersion "hms_api/modules/version/handler"

	// DIAGNOSA HANDLER
	handlerDiagnosa "hms_api/modules/diagnosa/handler"
	mapperDiagnosa "hms_api/modules/diagnosa/mapper"
	repoDiagnosa "hms_api/modules/diagnosa/repository"
	diagnosaUsecase "hms_api/modules/diagnosa/usecase"

	// IGD
	handlerIGD "hms_api/modules/igd/handler"
	mapperIGD "hms_api/modules/igd/mapper"
	repoIGD "hms_api/modules/igd/repository"
	usecaseIGD "hms_api/modules/igd/usecase"

	// NYERI
	nyeriMapper "hms_api/modules/nyeri/mapper"
	nyeriRepo "hms_api/modules/nyeri/repository"

	fileWebHandler "hms_api/web/handler"
	fileMapper "hms_api/web/mapper"

	// TAMBAKAN EARLY WANINGG SYSTEM HANDLER
	earlyHandler "hms_api/modules/early-warning-system/handler"
	earlyMapper "hms_api/modules/early-warning-system/mapper"
	earlyRepo "hms_api/modules/early-warning-system/repository"
	earlyUseCase "hms_api/modules/early-warning-system/usecase"
)

type Service struct {
	UserHandler      *handlerUser.UserHandler
	LibHandler       *handlerLib.LibHandler
	AntreanHandler   *handlerAntrean.AntreanHandler
	SoapHandler      *handlerSoap.SoapHandler
	RegHandler       *handlerReg.RegionalHandler
	RMEHandler       *handlerRME.RMEHandler
	FileWebHandler   *fileWebHandler.FileWebHandler
	HisHandler       *handlerHis.HisHandler
	ReportHandler    *handlerReport.ReportHandler
	KebidananHandler *handlerKebidanan.KebidananHandler
	VersionHandler   *handlerVersion.VersionHandler
	DiagnosaHandler  *handlerDiagnosa.DiagnosaHandler
	IGDHandler       *handlerIGD.IGDHandler
	EarlyHandler     *earlyHandler.EarlyHandler
}

var (
	httpRequestsTotal = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of HTTP requests",
		},
		[]string{"method", "endpoint", "status"},
	)

	httpRequestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_duration_seconds",
			Help:    "Duration of HTTP requests in seconds",
			Buckets: prometheus.DefBuckets,
		},
		[]string{"method", "endpoint"},
	)

	activeConnections = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "http_active_connections",
			Help: "Number of active HTTP connections",
		},
	)
)

func init() {
	prometheus.MustRegister(httpRequestsTotal)
	prometheus.MustRegister(httpRequestDuration)
	prometheus.MustRegister(activeConnections)
}

func main() {
	store := session.New()

	logging := logs.NewLogger()
	err := godotenv.Load(".env")

	if err != nil {
		logging.Error(err)
		log.Println(".env is not loaded properly")
		os.Exit(1)
	}

	db := config.InitMysqlDB()

	// MAPPER
	um := userMapper.NewUserMapperImple()
	am := antreanMapper.NewAntreanMapperImple()
	lm := libMapper.NewLibMapperImple()
	mr := reportMapper.NewReportImple()
	km := mapperKebidanan.NewKebidananMapperImple()
	dm := mapperDiagnosa.NewDiagnosaMapperImple()
	ym := nyeriMapper.NewNyeriImple()
	me := earlyMapper.NewEarlyMapperImple()
	rm := rmeMapper.NewLibMapperImple(ym)
	im := mapperIGD.NewIGDMapperImple(rm)
	hm := hisMapper.NewHisMapperImple(um)
	sm := soapMapper.NewSoapMapperImple(rmeMapper.RMEMapperImpl{})

	// NOTE REPOSITORY
	repoUser := repositoryUser.NewUserRepository(db)
	repoLib := repositoryLib.NewLibRepository(db, logging)
	repoAntrean := repositoryAntrean.NewLibRepository(db)
	repoSoap := repositorySoap.NewSoapRepository(db, logging, sm)
	repoReg := repoReg.NewRegionalRepository(db)
	repoRME := repositoryRME.NewRMERepository(db, logging, rm)
	repoHis := repositoryHis.NewHisRepository(db, logging)
	reportRepository := repositoryReport.NewReortRespository(db, logging)
	repoKebidanan := repoKebidanan.NewKebidananRepository(db, logging, km)
	repoDiagnosa := repoDiagnosa.NewDiagnosaRepository(db, logging)
	igdRepo := repoIGD.NewIgdRepository(db, logging, im)
	nyeriREPO := nyeriRepo.NewRegionalRepository(db)
	repoEarly := earlyRepo.NewEarlyRepository(db, logging)

	// USECASE
	uu := userUseCase.NewUserUseCase(repoUser, logging)
	ua := antreanUseCase.NewAntreanUseCase(repoAntrean, am, im, igdRepo, logging)
	lu := libUseCase.NewLibUseCase(repoLib)
	uh := hisUseCase.NewHisUseCase(repoHis, logging, hm, repoRME, repoSoap, sm)
	su := soapUseCase.NewAntreanUseCase(repoSoap, sm, logging, repoRME, repoKebidanan, km, uh)
	igdUsecase := usecaseIGD.NewIgdUseCase(igdRepo, logging, im, repoRME, nyeriREPO, ym, repoUser, um, repoLib, repoSoap, su, rm, repoKebidanan)
	ru := rmeUseCase.NewRMEUseCase(repoRME, rm, logging, su, repoSoap, uh, repoUser, reportRepository, um, repoDiagnosa, repoKebidanan, igdRepo, nyeriREPO, ym, igdUsecase)
	ku := kebidananUsecase.NewKebidananUseCase(repoKebidanan, logging, km, repoSoap, repoRME, repoUser, su, rm, uh)
	reportUsecase := reportUsecase.NewReportUsecase(reportRepository, logging, mr, repoRME, rm, repoSoap, sm, repoUser, um, repoLib)
	du := diagnosaUsecase.NewDiagnosaUseCase(repoDiagnosa, logging, dm)
	usecaseEarly := earlyUseCase.NewHisUseCase(repoEarly, logging, me)

	// HANDLER
	userHandler := handlerUser.UserHandler{UserUseCase: uu, UserMapperImpl: um, UserRepository: repoUser, Logging: logging, Store: store}
	libHandler := handlerLib.LibHandler{LibUseCase: lu, LibRepository: repoLib, LibMapper: lm, Logging: logging}
	antreanHandler := handlerAntrean.AntreanHandler{AntreanUseCase: ua, AntreanRepository: repoAntrean, Logging: logging, AntreanMapper: am}
	soapHandler := handlerSoap.SoapHandler{SoapUseCase: su, SoapRepository: repoSoap, SoapMapper: sm, Logging: logging, UserRepository: repoUser, RMEReporitory: repoRME, RMEMapper: rm, KebidananRepository: repoKebidanan, KebidananMaper: km, HisUsecase: uh, IGDRepository: igdRepo}
	regionalHandler := handlerReg.RegionalHandler{RegionalRepo: repoReg, Logging: logging}
	rmeHandler := handlerRME.RMEHandler{RMERepository: repoRME, Logging: logging, RMEMapper: rm, RMEUsecase: ru, SoapUsecase: su, UserRepository: repoUser, ReporRepository: reportRepository, SoapRepository: repoSoap, HisUseCase: uh, UserMapper: um, DiagnosaRepository: repoDiagnosa}
	fileWebHandler := fileWebHandler.FileWebHandler{WebMapper: fileMapper.WebMapper{}, Logging: logging}
	hisHandler := handlerHis.HisHandler{HisRepository: repoHis, HisUsecase: uh, Logging: logging, HisMapper: hm, RMERepository: repoRME, UserRepository: repoUser}
	reportHandler := handlerReport.ReportHandler{ReportRepository: reportRepository, Logging: logging, ReportMapper: mr, UserRepository: repoUser, SoapRepository: repoSoap, RMERepository: repoRME, ReportUsecase: reportUsecase, HisRepository: repoHis, UserMapper: um, LibRepository: repoLib}
	kebidananHandler := handlerKebidanan.KebidananHandler{Logging: logging, KebidananRepository: repoKebidanan, KebidananUseCase: ku, KebidananMapper: km, RMERepository: repoRME}
	diagnosaHandler := handlerDiagnosa.DiagnosaHandler{DiagnosaRepository: repoDiagnosa, DiagnosaUseCase: du, DiagnosaMapper: dm, Logging: logging}
	igdHandler := handlerIGD.IGDHandler{IGDMapper: im, IGDUseCase: igdUsecase, IGDRepository: igdRepo, Logging: logging, SoapUseCase: su}
	earlyHanler := earlyHandler.EarlyHandler{EarlyRepository: repoEarly, EarlyUseCase: usecaseEarly, EarlyMapper: me, Logging: logging}

	service := &Service{
		UserHandler:      &userHandler,
		LibHandler:       &libHandler,
		AntreanHandler:   &antreanHandler,
		SoapHandler:      &soapHandler,
		RegHandler:       &regionalHandler,
		RMEHandler:       &rmeHandler,
		FileWebHandler:   &fileWebHandler,
		HisHandler:       &hisHandler,
		ReportHandler:    &reportHandler,
		KebidananHandler: &kebidananHandler,
		DiagnosaHandler:  &diagnosaHandler,
		IGDHandler:       &igdHandler,
		EarlyHandler:     &earlyHanler,
	}

	service.FiberRoutingAndListen(logging, store)
}
