package antrean

import "time"

type (
	TempObatResep struct {
		Signa    string `json:"signa"`
		Nomor    string `json:"nomor"`
		KdDokter string `json:"kdDokter"`
		KdBagian string `json:"kdBagian"`
		Tglinput string `json:"tglInput"`
		Id       string `json:"id"`
		Noreg    string `json:"noreg"`
		Kodeobat string `json:"kodeObat"`
		Namaobat string `json:"namaObat"`
		Satuan   string `json:"satuan"`
		Jumlah   string `json:"jumlah"`
		Dosis    string `json:"dosis"`
		Cara     string `json:"cara"`
		Racik    string `json:"racik"`
		Alergi   string `json:"alergi"`
	}

	BookKamar struct {
		Status       string `json:"no"`
		ProsesDttm   string `json:"prosesDttm"`
		ProsesByUser string `json:"prosesByUser"`
		InsertDttm   string `json:"insertDttm"`
		UserBy       string `json:"userBy"`
		KdBooking    string `json:"kdBooking"`
		Nik          string `json:"nik"`
		Nama         string `json:"nama"`
		Alamat       string `json:"alamat"`
		Nohp         string `json:"nohp"`
		Kamar        string `json:"kamar"`
	}

	AntreanResep struct {
		No             int       `json:"no"`
		KodeBookingRef string    `json:"kodeBookingRef"`
		Cdttm          time.Time `json:"cdttm"`
		JenisAtrean    string    `json:"jenisAntrean"`
		JenisPasien    string    `json:"jenisPasien"`
		Tanggal        time.Time `json:"tanggal"`
		Jam            string    `json:"jam"`
		TimeEslapsed   time.Time `json:"timeEslapsed"`
		NoAntrean      string    `json:"noAntrean"`
		KodeBooking    string    `json:"kodeBooking"`
		Racikan        string    `json:"racikan"`
		Dilayani       string    `json:"dilayani"`
		UpdDttm        time.Time `json:"updDttm"`
	}

	AntrianPasien struct {
		Tgllahir     string `json:"tglLahir"`
		NoAntrian    string `json:"noAntrean"`
		Jeniskelamin string `json:"jenisKelamin"`
		Debitur      string `json:"debitur"`
		KdDebitur    string `json:"kdDebitur"`
		Noreg        string `json:"noreg"`
		RegType      string `json:"regType"`
		Mrn          string `json:"mrn"`
		Status       string `json:"status"`
		Nampas       string `json:"namaPasien"`
		KdBagian     string `json:"kd_bagian"`
		KdDokter     string `json:"kd_dokter"`
		Bagian       string `json:"bagian"`
		Pelayanan    string `json:"pelayanan"`
		NamaDokter   string `json:"namaDokter"`
		KdKelas      string `json:"kdKelas"`
		Umur         string `json:"umur"`
	}

	AntrianPoliIGD struct {
		Nama         string
		Kodedr       string
		Dokter       string
		Id           string
		Noreg        string
		NoBook       string
		NoAntrian    string
		Jeniskelamin string
		Umurth       int
		RegType      string
		Status       string
		Tgllahir     string
	}

	PenMedikModel struct {
		Kamar        string
		Kasur        string
		Namapenmedik string
		Id           string
		Noreg        string
		Dokter       string
		Nama         string
		NamaAsal     string
		Jeniskelamin string
		Kodepenmedik string
	}

	KbangsalKasur struct {
		Kodebangsal string
		Kamar       string
		Kasur       string
		Id          string
		Noreg       string
		Kodedr      string
		Nama        string
		Sex         string
		Umur        string
		Tgllahir    string
		Ket         string
		Dokter      string
		Kelas       string
	}

	SoapPasien struct {
		Tgllahir     string     `json:"tglLahir"`
		NoAntrean    string     `json:"noAntrean"`
		JenisKelamin string     `json:"jenisKelamin"`
		Debitur      string     `json:"debitur"`
		KodeDebitur  string     `json:"kdDebitur"`
		Noreg        string     `json:"noreg"`
		RegType      string     `json:"regType"`
		Mrn          string     `json:"mrn"`
		Status       string     `json:"status"`
		Keterangan   string     `json:"keterangan"`
		Proses       string     `json:"proses"`
		NamaPasien   string     `json:"namaPasien"`
		KdBagian     string     `json:"kd_bagian"`
		NamaDokter   string     `json:"namaDokter"`
		KPelayanan   KPelayanan `gorm:"foreignKey:KdBagian" json:"kd_pelayanan"`
	}

	RiwayatPasien struct {
		Noreg         string `json:"noreg"`
		RegInsertDttm string `json:"dttm"`
		Dokter        string `json:"dokter"`
		Bagian        string `json:"bagian"`
		Umur          string `json:"umur"`
		Debitur       string `json:"debitur"`
		NoSep         string `json:"noSEP"`
	}

	DprofilePasien struct {
		ID           string `json:"id"`
		Nik          string `json:"nik"`
		Nokapst      string `json:"nokapst"`
		Nama         string `json:"nama"`
		Tempatlahir  string `json:"tempatlahir"`
		HAR          string `json:"har"`
		BUL          string `json:"bul"`
		TAH          string `json:"tah"`
		Jeniskelamin string `json:"jeniskelamin"`
		Status       string `json:"status"`
		Negara       string `json:"negara"`
		Propinsi     string `json:"provinsi"`
		Kabupaten    string `json:"kabupaten"`
		Kecamatan    string `json:"kecamatan"`
		Kelurahan    string `json:"kelurahan"`
		Rtrw         string `json:"rtrw"`
		Alamat       string `json:"alamat"`
		Alamat2      string `json:"alamat2"`
		Hp           string `json:"hp"`
		CPN          string `json:"cpn"`
		Agama        string `json:"agama"`
		Suku         string `json:"suku"`
		Pendidikan   string `json:"pendidikan"`
		Pekerjaan    string `json:"pekerjaan"`
		CpName       string `json:"cpName"`
		CpNumber     string `json:"cpNumber"`
		CpRelasi     string `json:"cpRelasi"`
		Tahun        int    `json:"tahun"`
		Bulan        int    `json:"bulan"`
		Hari         int    `json:"hari"`
	}

	KPelayanan struct {
		KdBag     string `json:"kd_bagian"`
		Bagian    string `json:"bagian"`
		Pelayanan string `json:"pelayanan"`
	}

	// Antrian Pasien
	AntrianPasienTmp struct {
		RecieveDttm string
		Noreg       string
		Antrian     AntrianOL
		KdTujun     string
		NoOrder     string
		Notif       string
	}

	// Antrian OL
	AntrianOL struct {
		Id          string
		Noreg       string
		Nik         string
		Noka        string
		NoHp        string
		Nama        string
		Gender      string
		Alamat      string
		KodeTujuan  string
		Tujuan      string
		KdDokter    string
		Dokter      string
		KodeDebitur string
		Debitur     string
		NoBook      string
	}

	// =======//
	Dpoliugd6 struct {
		RiB1         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b1"`
		RiB2         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b2"`
		RiB3         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b3"`
		RiB4         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b4"`
		RiB5         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b5"`
		RiB6         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b6"`
		RiB7         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b7"`
		RiB8         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b8"`
		RiB9         string `gorm:"type:varchar(1);not null;default:''" json:"ri_b9"`
		RiB10        string `gorm:"type:varchar(1);not null;default:''" json:"ri_b10"`
		RiB11        string `gorm:"type:varchar(1);not null;default:''" json:"ri_b11"`
		RiB12        string `gorm:"type:varchar(1);not null;default:''" json:"ri_b12"`
		Bpjs         string `gorm:"type:varchar(6);not null;default:'false'" json:"bpjs"`
		Kasus        string `gorm:"type:varchar(4);not null;default:'LAMA'" json:"kasus"`
		Pelayanan    string `gorm:"type:varchar(100);not null;default:''" json:"pelayanan"`
		Bagian       string `gorm:"type:varchar(30);not null;default:''" json:"bagian"`
		User         string `gorm:"type:varchar(100);not null;default:''" json:"user"`
		Periode      string `gorm:"type:varchar(100);not null;default:''" json:"periode"`
		SexL         string `gorm:"type:varchar(1);not null;default:''" json:"sex_l"`
		SexP         string `gorm:"type:varchar(1);not null;default:''" json:"sex_p"`
		UmurTh       string `gorm:"type:varchar(3);not null;default:''" json:"umur_th"`
		UmurBl       string `gorm:"type:varchar(2);not null;default:''" json:"umur_bl"`
		UmurHr       string `gorm:"type:varchar(2);not null;default:''" json:"umur_hr"`
		Dokter       string `gorm:"type:varchar(100);not null;default:''" json:"dokter"`
		Tanggal      string `gorm:"type:date;not null;default:'0000-00-00'" json:"tanggal"`
		Jam          string `gorm:"type:time;not null;default:'00:00:00'" json:"jam"`
		Shift        string `gorm:"type:varchar(100);not null;default:''" json:"shift"`
		Minggu       string `gorm:"type:varchar(100);not null;default:''" json:"minggu"`
		ID           string `gorm:"type:varchar(100);not null;default:''" json:"id"`
		Noreg        string `gorm:"type:varchar(100);not null;default:''" json:"noreg"`
		Nama         string `gorm:"type:varchar(24);not null;default:''" json:"nama"`
		Sex          string `gorm:"type:varchar(100);not null;default:''" json:"sex"`
		Umur         string `gorm:"type:varchar(100);not null;default:''" json:"umur"`
		TdLambat     string `gorm:"type:varchar(100);not null;default:''" json:"td_lambat"`
		Lambat       string `gorm:"type:varchar(100);not null;default:''" json:"lambat"`
		CaraKrujuk   string `gorm:"type:varchar(100);not null;default:''" json:"cara_krujuk"`
		CaraKnon     string `gorm:"type:varchar(100);not null;default:''" json:"cara_knon"`
		JenisKbaru   string `gorm:"type:varchar(100);not null;default:''" json:"jenis_kbaru"`
		JenisKlama   string `gorm:"type:varchar(100);not null;default:''" json:"jenis_klama"`
		TlDoa        string `gorm:"type:varchar(100);not null;default:''" json:"tl_doa"`
		TlDoe        string `gorm:"type:varchar(100);not null;default:''" json:"tl_doe"`
		TlPul        string `gorm:"type:varchar(100);not null;default:''" json:"tl_pul"`
		TlRaw        string `gorm:"type:varchar(100);not null;default:''" json:"tl_raw"`
		TlRuj        string `gorm:"type:varchar(100);not null;default:''" json:"tl_ruj"`
		Diagnosa     string `gorm:"type:varchar(100);not null;default:''" json:"diagnosa"`
		Keadaan      string `gorm:"type:varchar(100);not null;default:''" json:"keadaan"`
		AlatEkg      string `gorm:"type:varchar(100);not null;default:''" json:"alat_ekg"`
		AlatO2       string `gorm:"type:varchar(100);not null;default:''" json:"alat_o2"`
		AlatMonitor  string `gorm:"type:varchar(100);not null;default:''" json:"alat_monitor"`
		AlatNebul    string `gorm:"type:varchar(100);not null;default:''" json:"alat_nebul"`
		AlatSuction  string `gorm:"type:varchar(100);not null;default:''" json:"alat_suction"`
		AlatDc       string `gorm:"type:varchar(100);not null;default:''" json:"alat_dc"`
		AlatRad      string `gorm:"type:varchar(100);not null;default:''" json:"alat_rad"`
		AlatLab      string `gorm:"type:varchar(100);not null;default:''" json:"alat_lab"`
		TriaseMerah  string `gorm:"type:varchar(100);not null;default:''" json:"triase_merah"`
		TriaseKuning string `gorm:"type:varchar(100);not null;default:''" json:"triase_kuning"`
		TriaseHijau  string `gorm:"type:varchar(100);not null;default:''" json:"triase_hijau"`
		TriaseHitam  string `gorm:"type:varchar(100);not null;default:''" json:"triase_hitam"`
		GolInter     string `gorm:"type:varchar(100);not null;default:''" json:"gol_inter"`
		GolAnak      string `gorm:"type:varchar(100);not null;default:''" json:"gol_anak"`
		GolBedah     string `gorm:"type:varchar(100);not null;default:''" json:"gol_bedah"`
		GolObgyn     string `gorm:"type:varchar(100);not null;default:''" json:"gol_obgyn"`
		GolLain      string `gorm:"type:varchar(100);not null;default:''" json:"gol_lain"`
		TruEmer      string `gorm:"type:varchar(100);not null;default:''" json:"tru_emer"`
		FalEmer      string `gorm:"type:varchar(100);not null;default:''" json:"fal_emer"`
		Visum        string `gorm:"type:varchar(100);not null;default:''" json:"visum"`
		Penggolongan string `gorm:"type:varchar(100);not null;default:''" json:"penggolongan"`
	}
)

func (KPelayanan) TableName() string {
	return "vicore_lib.kpelayanan"
}

func (Dpoliugd6) TableName() string {
	return "his.dpoliugd6"
}

func (PenMedikModel) TableName() string {
	return "his.antrianpenmedik"
}

func (KbangsalKasur) TableName() string {
	return "his.kbangsalkasur"
}

func (SoapPasien) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AntreanResep) TableName() string {
	return "vicore_posfar.antrean_resep"
}

func (BookKamar) TableName() string {
	return "vicore_tmp.book_kamar"
}

func (TempObatResep) TableName() string {
	return "vicore_tmp.tempobatresep"
}
