package dto

type (
	RequestNorm struct {
		NoRM string `json:"no_rm"  binding:"required"`
	}

	RequestNormV2 struct {
		NoRM string `json:"no_rm"  validate:"required"`
	}

	// TODO : ANTRIAN PASIEN
	AntrianPasien struct {
		Tgllahir       string `json:"tglLahir"`
		Usia           int    `json:"usia"`
		NoAntrean      string `json:"noAntrean"`
		JenisKelamin   string `json:"jenisKelamin"`
		Debitur        string `json:"debitur"`
		KodeDebitur    string `json:"kdDebitur"`
		Noreg          string `json:"noreg"`
		RegType        string `json:"regType"`
		Mrn            string `json:"mrn"`
		Status         string `json:"status"`
		Keterangan     string `json:"keterangan"`
		Proses         string `json:"proses"`
		NamaPasien     string `json:"namaPasien"`
		KdBag          string `json:"kd_bagian"`
		Bagian         string `json:"bagian"`
		Pelayanan      string `json:"pelayanan"`
		NamaDokter     string `json:"namaDokter"`
		KdKelas        string `json:"kdKelas"`
		Kelas          string `json:"kelas"`
		Umur           string `json:"umur"`
		KdDokter       string `json:"kdDokter"`
		Kamar          string `json:"kamar"`
		Kasur          string `json:"kasur"`
		AsesmenDokter  string `json:"asesmen_dokter"`
		AsesmenPerawat string `json:"asesmen_perawat"`
	}
)
