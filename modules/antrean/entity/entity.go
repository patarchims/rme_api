package entity

import (
	"hms_api/modules/antrean"
	"hms_api/modules/antrean/dto"
	"hms_api/modules/user"
)

// UserUseCase
type AntreanUseCase interface {
	OnGetAntreanIGDTelahDiprosesUseCase(modulID string) (res []dto.AntrianPasien, message string, err error)
	OnGetAntrianIGDUseCase(modulID string, person string, userID string) (res []dto.AntrianPasien, message string, err error)
}

// UserRepository
type AntreanRepository interface {
	OnGetAntreanIGDYangTelahDiProsesRepsoitory() (res []antrean.Dpoliugd6, err error)
	//===//
	GetAntreanPasien(kodeDokter string, kdModul string) (res []antrean.AntrianPasien, err error)
	GetDetailPasien(noRm string) (res antrean.DprofilePasien, err error)
	GetRiwayatPasien(noRM string) (res []antrean.RiwayatPasien, err error)
	GetAntreanResep() (res []antrean.AntreanResep, err error)
	GetBookKamar() (res []antrean.BookKamar, err error)
	FindUserByID(userID string) (res user.Kemploye, err error)
	GetAntreanPasienForPerawat(kdModul string) (res []antrean.AntrianPasien, err error)
	GetAntreanIGD(kdModul string) (res []antrean.AntrianPasien, err error)

	// === ANTRIAN UGD
	GetAntrianUGD() (res []antrean.AntrianPoliIGD, err error)
	GetDetailPasienV3(noRm string) (res antrean.DprofilePasien, err error)
	GetPasienBangsal(kodeBangsal string) (res []antrean.KbangsalKasur, err error)
	GetAntrianPenmedikRepository(namaPenmedik string) (res []antrean.PenMedikModel, err error)

	// GET ANTRIAN PENMEDIK LAIN

	GetAntrianPoliGigi() (res []antrean.KbangsalKasur, err error)
	GetAntrianIGDDokterUmumRepository(KodeDokter string) (res []antrean.AntrianPoliIGD, err error)
	GetAntrianSpesialisasiRepository(KodeDokter string) (res []antrean.AntrianPoliIGD, err error)
	GetPasienBangsalForDokter(kodeBangsal string, kodeDokter string) (res []antrean.KbangsalKasur, err error)

	// GET PASIEN OK
	GetAntrianRuangOperasi() (res []antrean.KbangsalKasur, err error)
	GetAntrianPasienKamarOperasiRepository() (res []antrean.KbangsalKasur, err error)
}

// Mapper
type AntreanMapper interface {
	ToResponseAntreanMapper(data []antrean.AntrianPasien) (value []dto.AntrianPasien)
	ToAntreanPasienMapper(data []antrean.AntrianPoliIGD) (value []dto.AntrianPasien)
	ToBangsalPasienAntrean(data []antrean.KbangsalKasur) (value []dto.AntrianPasien)
	ToPenmedikPasienAntreanMapper(data []antrean.PenMedikModel) (value []dto.AntrianPasien)
}
