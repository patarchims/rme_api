package handler

import (
	"hms_api/modules/antrean/dto"
	"hms_api/modules/antrean/entity"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"github.com/sirupsen/logrus"
)

type AntreanHandler struct {
	AntreanUseCase    entity.AntreanUseCase
	AntreanRepository entity.AntreanRepository
	AntreanMapper     entity.AntreanMapper
	Logging           *logrus.Logger
}

func (ah *AntreanHandler) BookKamar(c *gin.Context) {
	value, err := ah.AntreanRepository.GetBookKamar()

	if err != nil || len(value) == 0 {
		log.Error().Msg(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	m := map[string]any{}
	if len(value) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	m["list"] = value
	response := helper.APIResponse("Ok", http.StatusOK, m)
	c.JSON(http.StatusOK, response)
}

func (ah *AntreanHandler) BookKamarV2(c *fiber.Ctx) error {
	value, err := ah.AntreanRepository.GetBookKamar()

	if err != nil || len(value) == 0 {
		ah.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	m := map[string]any{}
	if len(value) == 0 {
		ah.Logging.Error("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	m["list"] = value
	response := helper.APIResponse("Ok", http.StatusOK, m)
	ah.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ah *AntreanHandler) GetAntreanResep(c *gin.Context) {
	value, err := ah.AntreanRepository.GetAntreanResep()

	if err != nil || len(value) == 0 {
		log.Error().Msg(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	m := map[string]any{}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	m["list"] = value
	response := helper.APIResponse("OK", http.StatusOK, m)
	c.JSON(http.StatusOK, response)
}

func (ah *AntreanHandler) GetAntreanResepV2(c *fiber.Ctx) error {
	value, err := ah.AntreanRepository.GetAntreanResep()

	if err != nil || len(value) == 0 {
		log.Error().Msg(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	m := map[string]any{}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	m["list"] = value
	response := helper.APIResponse("Ok", http.StatusOK, m)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ah *AntreanHandler) GetAntreanPasien(c *gin.Context) {
	userID := c.MustGet("userID").(string)
	modulID := c.MustGet("modulID").(string)

	// CARI APAKAH YANG LOGIN DOKTER ATAU PERAWAT
	userd, err := ah.AntreanRepository.FindUserByID(userID)

	if err != nil {
		ah.Logging.Error("Find User By ID" + err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if modulID != "IGD001" {
		if userd.KeteranganPerson == "dokter" {
			value, err := ah.AntreanRepository.GetAntreanPasien(userID, modulID)

			ah.Logging.Info(value)

			if err != nil {
				ah.Logging.Error("GetAntreanPasien" + err.Error())
				response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
				c.JSON(http.StatusCreated, response)
				return
			}

			if len(value) == 0 {
				ah.Logging.Info("Data Kosong")
				response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
				c.JSON(http.StatusAccepted, response)
				return
			} else {
				m := map[string]any{}
				mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				c.JSON(http.StatusOK, response)
			}
		}

		if userd.KeteranganPerson == "perawat" || userd.KeteranganPerson == "non_dokter" {
			value, err := ah.AntreanRepository.GetAntreanPasienForPerawat(modulID)

			ah.Logging.Info(value)

			if err != nil || len(value) == 0 {
				log.Error().Msg(err.Error())
				response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
				c.JSON(http.StatusCreated, response)
				return
			}

			if len(value) == 0 {
				response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
				c.JSON(http.StatusAccepted, response)
				return
			} else {
				m := map[string]any{}
				mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				c.JSON(http.StatusOK, response)
			}

		}
	}

	if modulID == "IGD001" {
		value, ers := ah.AntreanRepository.GetAntreanIGD(modulID)

		ah.Logging.Info("ANTRIAN PASIEN ")
		ah.Logging.Info(value)

		if ers != nil {
			log.Error().Msg(ers.Error())
			response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(value) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		} else {
			ah.Logging.Info("PRINT IGD ")
			ah.Logging.Info(value)
			ah.Logging.Info(len(value))

			m := map[string]any{}
			mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			c.JSON(http.StatusOK, response)
			return
		}
	}
}

func (ah *AntreanHandler) GetAntreanPasienV2(c *fiber.Ctx) error {
	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	// CARI APAKAH YANG LOGIN DOKTER ATAU PERAWAT
	userd, err := ah.AntreanRepository.FindUserByID(userID)

	if err != nil {
		ah.Logging.Error("Find User By ID" + err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.JSON(response)
	}

	if modulID != "IGD001" {
		// KETERANGAN PERSON
		if userd.KeteranganPerson == "dokter" {
			value, errs := ah.AntreanRepository.GetAntreanPasien(userID, modulID)

			ah.Logging.Info(value)

			if errs != nil {
				ah.Logging.Error("GetAntreanPasien" + errs.Error())
				response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(value) == 0 {
				ah.Logging.Info("Data Kosong")
				response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
				return c.Status(fiber.StatusAccepted).JSON(response)
			} else {
				m := map[string]any{}
				mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}
		}

		if userd.KeteranganPerson == "perawat" || userd.KeteranganPerson == "non_dokter" {
			value, err := ah.AntreanRepository.GetAntreanPasienForPerawat(modulID)

			ah.Logging.Info(value)

			if err != nil || len(value) == 0 {
				log.Error().Msg(err.Error())
				response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(value) == 0 {
				response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
				return c.JSON(response)
			} else {
				m := map[string]any{}
				mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}
		}
	}

	if modulID == "IGD001" {
		value, ers := ah.AntreanRepository.GetAntreanIGD(modulID)

		ah.Logging.Info("ANTRIAN PASIEN ")
		ah.Logging.Info(value)

		if ers != nil {
			log.Error().Msg(ers.Error())
			response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(value) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)
		} else {
			ah.Logging.Info("PRINT IGD ")
			ah.Logging.Info(value)
			ah.Logging.Info(len(value))

			m := map[string]any{}
			mapper := ah.AntreanMapper.ToResponseAntreanMapper(value)

			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
	}

	ah.Logging.Info("Data Tidak ditemukan")
	response := helper.APIResponseFailure("Data Tidak ditemukan", http.StatusAccepted)
	return c.Status(fiber.StatusAccepted).JSON(response)
}

func (ah *AntreanHandler) GetAntreanPasienV3(c *fiber.Ctx) error {
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)
	userID := c.Locals("userID").(string)

	// JIKA  MODUL IGD
	switch modulID {
	case "NST3":
		ah.Logging.Info("ANTRIAN NURSE 3")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NST3", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("NST3")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEMP":
		ah.Logging.Info("ANTRIAN NURSE CEMPAKA")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")

			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				// antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEMP", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEMP", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEMP")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ELI1":
		ah.Logging.Info("ANTRIAN NURSE ELI ")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")

			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				// antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ELI1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ELI1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("ELI1")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ELI2":
		ah.Logging.Info("ANTRIAN NURSE ELI ")
		if person == "dokter" {
			spesial := strings.Split(userID, "_")

			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ELI2", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ELI2", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("ELI2")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "FRA1":
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")

			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				// antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("FRA1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("FRA1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("FRA1")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "FRA2":
		ah.Logging.Info("ANTRIAN FRANSISKUS 2 ")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")

			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				// antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("FRA2", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("FRA1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("FRA2")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NST2":
		if person == "dokter" {
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NST2", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NST2", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("NST2")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "THER":
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("THER", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("THER", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("THER")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NST1":
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NST1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NST1", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("NST1")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "VK0001":
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("VK0001", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("VK0001", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PONEK":
		ah.Logging.Info("ANTRIAN IGD")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("PONEK", userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "IGD001":
		ah.Logging.Info("ANTRIAN IGD")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				// antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					log.Info().Msg(err.Error())
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				if len(mapper) == 0 {
					m := map[string]any{}
					m["list"] = make([]string, 0)

					response := helper.APIResponse("Ok", http.StatusOK, m)
					ah.Logging.Info(response)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MARI":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MARI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ESTE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ESTE", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ESTE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIDY":
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIDY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIDY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NAOM":
		ah.Logging.Info("ANTRIAN NAOM") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NAOM", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NAOM")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "REBE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("REBE", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("REBE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SARA":
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("SARA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LUKA":
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LUKA", userID)

			ah.Logging.Debug("Antrian dokter")
			ah.Logging.Debug("DOKTER")
			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		ah.Logging.Info("PERAWAT")
		ah.Logging.Info(person)

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LUKA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MEIN":
		ah.Logging.Info("ANTRIAN MEIN")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MEIN", userID)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MEIN")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("MART")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MART":
		ah.Logging.Info("ANTRIAN MARTINA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MART", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MART")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper
		ah.Logging.Info(mapper)
		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ANGG":
		ah.Logging.Info("ANTRIAN ANGGREEK")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ANGG", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ANGG")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("OKsds")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MELA":
		ah.Logging.Info("ANTRIAN MELA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MELA", userID)

			ah.Logging.Info("AMBIL MELATI")

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MELA")

		ah.Logging.Info("AMBIL MELATI")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "RUBY":
		ah.Logging.Info("ANTRIAN RUBY")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RUBY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("RUBY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAHO":
		ah.Logging.Info("ANTRIAN MAHONI")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAHO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAHO")

		ah.Logging.Info("AMBIL ANTRIAN MAHO")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTRIAN MAHONI DI AMBIL")
		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "POL037":
		ah.Logging.Info("ANTRIAN POL037")
		// GET ANTRIAN POLI GIGI
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEDR":
		ah.Logging.Info("ANTRIAN CEDR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEDR", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEDR")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEND":
		ah.Logging.Info("ANTRIAN CEND")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEND", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEND")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "EMER":
		ah.Logging.Info("ANTRIAN EMERALD")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("EMER", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("EMER")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ICU":
		ah.Logging.Info("ICU")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ICU", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ICU")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAWA":
		ah.Logging.Info("MAWAR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAWA", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAWA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NEON":
		ah.Logging.Info("NEON")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NEON", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NEON")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PERI":
		ah.Logging.Info("PERI")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("PERI", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("PERI")

		ah.Logging.Info("ANTRIAN PERI")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIEB":
		ah.Logging.Info("LIEB")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIEB", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIEB")

		ah.Logging.Info("ANTRIAN LIEB")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "ISOL":
		ah.Logging.Info("ISOL")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ISOL", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("ISOL")

		ah.Logging.Info("ANTRIAN ISOL")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "VINC":
		ah.Logging.Info("VINC")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("VINC", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("VINC")

		ah.Logging.Info("ANTRIAN VINC")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "RECO":
		ah.Logging.Info("RECO")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RECO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("RECO")

		ah.Logging.Info("ANTRIAN RECO")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SAFI":
		ah.Logging.Info("SARIR")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("SAFI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("SAFI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "OK0001":
		ah.Logging.Info("KAMAR OPERASI")

		if person == "dokter" {

			antrian, err := ah.AntreanRepository.GetAntrianRuangOperasi()

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetAntrianRuangOperasi()

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "HAED":
		ah.Logging.Info("ANTRIAN HAEMODALISA")
		antrian, err := ah.AntreanRepository.GetAntrianPenmedikRepository("cpenm007")

		ah.Logging.Debug("HAED")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(antrian) > 0 {
			mapper := ah.AntreanMapper.ToPenmedikPasienAntreanMapper(antrian)
			ah.Logging.Info("ANTRIAN HAEMODALISA")
			ah.Logging.Info(mapper)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}
		ah.Logging.Info("ANTRIAN KOSONG")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)

	default:
		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	}
}

func (ah *AntreanHandler) GetAntreanYangTelahDiprosesHandler(c *fiber.Ctx) error {
	modulID := c.Locals("modulID").(string)
	data, message, er11 := ah.AntreanUseCase.OnGetAntreanIGDTelahDiprosesUseCase(modulID)

	if er11 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ah *AntreanHandler) GetAntreanPasienV4(c *fiber.Ctx) error {
	// CEK APAKAH PERSON DOKTER ATAU PERAWAT
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)
	userID := c.Locals("userID").(string)

	switch modulID {
	case "VK0001":
		if person == "dokter" {
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PONEK":
		ah.Logging.Info("ANTRIAN IGD")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "IGD001":
		ah.Logging.Info("ANTRIAN IGD")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				if len(mapper) == 0 {
					m := map[string]any{}
					m["list"] = make([]string, 0)

					response := helper.APIResponse("OK", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MARI":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MARI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ESTE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ESTE", userID)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ESTE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIDY":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIDY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("OK", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIDY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NAOM":
		ah.Logging.Info("ANTRIAN NAOM") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NAOM", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NAOM")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "REBE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("REBE", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("REBE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SARA":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("SARA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LUKA":
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LUKA", userID)

			ah.Logging.Debug("Antrian dokter")
			ah.Logging.Debug("DOKTER")
			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		ah.Logging.Info("PERAWAT")
		ah.Logging.Info(person)

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LUKA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MEIN":
		ah.Logging.Info("ANTRIAN MEIN")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MEIN", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MEIN")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("MART")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MART":
		ah.Logging.Info("ANTRIAN MARTINA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MART", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MART")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper
		ah.Logging.Info(mapper)
		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ANGG":
		ah.Logging.Info("ANTRIAN ANGGREEK")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ANGG", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ANGG")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("OKsds")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MELA":
		ah.Logging.Info("ANTRIAN MELA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MELA", userID)

			ah.Logging.Info("AMBIL MELATI")

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MELA")

		ah.Logging.Info("AMBIL MELATI")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "RUBY":
		ah.Logging.Info("ANTRIAN RUBY")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RUBY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("RUBY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAHO":
		ah.Logging.Info("ANTRIAN MAHONI")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAHO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAHO")

		ah.Logging.Info("AMBIL ANTRIAN MAHO")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTRIAN MAHONI DI AMBIL")
		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "POL037":
		ah.Logging.Info("ANTRIAN POL037")
		// GET ANTRIAN POLI GIGI
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEDR":
		ah.Logging.Info("ANTRIAN CEDR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEDR", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEDR")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEND":
		ah.Logging.Info("ANTRIAN CEND")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEND", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEND")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "EMER":
		ah.Logging.Info("ANTRIAN EMERALD")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("EMER", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("EMER")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ICU":
		ah.Logging.Info("ICU")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ICU", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ICU")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAWA":
		ah.Logging.Info("MAWAR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAWA", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAWA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NEON":
		ah.Logging.Info("NEON")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NEON", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NEON")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PERI":
		ah.Logging.Info("PERI")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("PERI", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("PERI")

		ah.Logging.Info("ANTRIAN PERI")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIEB":
		ah.Logging.Info("LIEB")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIEB", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIEB")

		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "ISOL":
		ah.Logging.Info("ISOL")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ISOL", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("ISOL")

		ah.Logging.Info("ANTRIAN ISOL")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "VINC":
		ah.Logging.Info("VINC")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("VINC", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("VINC")

		ah.Logging.Info("ANTRIAN VINC")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "RECO":
		ah.Logging.Info("RECO")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RECO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("RECO")

		ah.Logging.Info("ANTRIAN RECO")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SAFI":
		ah.Logging.Info("SARIR")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("SAFI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("SAFI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			log.Info().Msg(err.Error())
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "HAED":
		ah.Logging.Info("ANTRIAN HAEMODALISA")
		antrian, err := ah.AntreanRepository.GetAntrianPenmedikRepository("cpenm007")

		ah.Logging.Debug("HAED")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(antrian) > 0 {
			mapper := ah.AntreanMapper.ToPenmedikPasienAntreanMapper(antrian)
			ah.Logging.Info("ANTRIAN HAEMODALISA")
			ah.Logging.Info(mapper)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}
		ah.Logging.Info("ANTRIAN KOSONG")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)

	default:
		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	}
}

func (ah *AntreanHandler) GetAntreanPasienNewVersion(c *fiber.Ctx) error {
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)
	userID := c.Locals("userID").(string)

	data, message, er11 := ah.AntreanUseCase.OnGetAntrianIGDUseCase(modulID, person, userID)

	if er11 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ah *AntreanHandler) GetAntreanPasienV5(c *fiber.Ctx) error {
	// CEK APAKAH PERSON DOKTER ATAU PERAWAT
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)
	userID := c.Locals("userID").(string)

	switch modulID {
	case "VK0001":
		if person == "dokter" {
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PONEK":
		ah.Logging.Info("ANTRIAN IGD")
		if person == "dokter" {
			// CEK APAKAH DOKTER UMUM
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}
		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "IGD001":
		if person == "dokter" {
			spesial := strings.Split(userID, "_")
			ah.Logging.Info("DOKTER SPESIALISASI")
			if spesial[0] == "DU" {
				ah.Logging.Info("DOKTER UMUM")
				antrian, err := ah.AntreanRepository.GetAntrianIGDDokterUmumRepository(userID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				if len(mapper) == 0 {
					m := map[string]any{}
					m["list"] = make([]string, 0)

					response := helper.APIResponse("OK", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

			if spesial[0] == "DS" {
				ah.Logging.Info("DOKTER SPESIALISASI")
				antrian, err := ah.AntreanRepository.GetAntrianSpesialisasiRepository(modulID)

				if err != nil {
					response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
					return c.Status(fiber.StatusCreated).JSON(response)
				}

				// m := map[string]any{}
				// m["list"] = []string{}

				mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)
				ah.Logging.Info(mapper)

				if len(mapper) != 0 {
					m := map[string]any{}
					m["list"] = mapper

					response := helper.APIResponse("Ok", http.StatusOK, m)
					return c.Status(fiber.StatusOK).JSON(response)
				}

				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("OK", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)

			}

		}

		antrian, err := ah.AntreanRepository.GetAntrianUGD()

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToAntreanPasienMapper(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MARI":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MARI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ESTE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ESTE", userID)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ESTE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("OK", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("OK", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIDY":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIDY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("OK", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIDY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NAOM":
		ah.Logging.Info("ANTRIAN NAOM") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NAOM", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NAOM")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "REBE":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("REBE", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("REBE")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SARA":
		ah.Logging.Info("ANTRIAN MARIA") // CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MARI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if antrian == nil || len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("SARA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)
		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "LUKA":
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LUKA", userID)

			ah.Logging.Debug("Antrian dokter")
			ah.Logging.Debug("DOKTER")
			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			if len(antrian) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		ah.Logging.Info("PERAWAT")
		ah.Logging.Info(person)

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LUKA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MEIN":
		ah.Logging.Info("ANTRIAN MEIN")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MEIN", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MEIN")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("MART")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MART":
		ah.Logging.Info("ANTRIAN MARTINA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MART", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("MART")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper
		ah.Logging.Info(mapper)
		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ANGG":
		ah.Logging.Info("ANTRIAN ANGGREEK")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ANGG", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ANGG")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		log.Info().Msg("OKsds")
		return c.Status(fiber.StatusOK).JSON(response)
	case "MELA":
		ah.Logging.Info("ANTRIAN MELA")
		// CHECK APAKAH DOKTER
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MELA", userID)

			ah.Logging.Info("AMBIL MELATI")

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = []string{}

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MELA")

		ah.Logging.Info("AMBIL MELATI")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTIRAN MELATI DIAMBIL")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "RUBY":
		ah.Logging.Info("ANTRIAN RUBY")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RUBY", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("RUBY")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAHO":
		ah.Logging.Info("ANTRIAN MAHONI")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAHO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAHO")

		ah.Logging.Info("AMBIL ANTRIAN MAHO")

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		ah.Logging.Info("ANTRIAN MAHONI DI AMBIL")
		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "POL037":
		ah.Logging.Info("ANTRIAN POL037")
		// GET ANTRIAN POLI GIGI
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetAntrianPoliGigi()

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEDR":
		ah.Logging.Info("ANTRIAN CEDR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEDR", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEDR")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "CEND":
		ah.Logging.Info("ANTRIAN CEND")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("CEND", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("CEND")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "EMER":
		ah.Logging.Info("ANTRIAN EMERALD")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("EMER", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("EMER")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "ICU":
		ah.Logging.Info("ICU")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ICU", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("ICU")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "MAWA":
		ah.Logging.Info("MAWAR")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("MAWA", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("MAWA")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "NEON":
		ah.Logging.Info("NEON")
		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("NEON", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}
		antrian, err := ah.AntreanRepository.GetPasienBangsal("NEON")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "PERI":
		ah.Logging.Info("PERI")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("PERI", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("PERI")

		ah.Logging.Info("ANTRIAN PERI")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "LIEB":
		ah.Logging.Info("LIEB")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("LIEB", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("LIEB")

		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "ISOL":
		ah.Logging.Info("ISOL")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("ISOL", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("ISOL")

		ah.Logging.Info("ANTRIAN ISOL")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "VINC":
		ah.Logging.Info("VINC")

		if person == "dokter" {
			ah.Logging.Info("PERSON DOKTER")
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("VINC", userID)

			ah.Logging.Info(antrian)
			ah.Logging.Info(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			if len(mapper) == 0 {
				m := map[string]any{}
				m["list"] = make([]string, 0)

				response := helper.APIResponse("Ok", http.StatusOK, m)
				ah.Logging.Info(response)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("VINC")

		ah.Logging.Info("ANTRIAN VINC")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		ah.Logging.Info(c.Status(fiber.StatusOK).JSON(response))
		return c.Status(fiber.StatusOK).JSON(response)
	case "RECO":
		ah.Logging.Info("RECO")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("RECO", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("RECO")

		ah.Logging.Info("ANTRIAN RECO")
		ah.Logging.Info(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "SAFI":
		ah.Logging.Info("SARIR")

		if person == "dokter" {
			antrian, err := ah.AntreanRepository.GetPasienBangsalForDokter("SAFI", userID)

			ah.Logging.Debug(antrian)

			if err != nil {
				response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}

			mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

			if len(mapper) != 0 {
				m := map[string]any{}
				m["list"] = mapper

				response := helper.APIResponse("Ok", http.StatusOK, m)
				return c.Status(fiber.StatusOK).JSON(response)
			}

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		antrian, err := ah.AntreanRepository.GetPasienBangsal("SAFI")

		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			log.Info().Msg(err.Error())
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		mapper := ah.AntreanMapper.ToBangsalPasienAntrean(antrian)

		if len(mapper) != 0 {
			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		if len(mapper) == 0 {
			m := map[string]any{}
			m["list"] = make([]string, 0)

			response := helper.APIResponse("Ok", http.StatusOK, m)
			ah.Logging.Info(response)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = mapper

		response := helper.APIResponse("OK", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	case "HAED":
		ah.Logging.Info("ANTRIAN HAEMODALISA")
		antrian, err := ah.AntreanRepository.GetAntrianPenmedikRepository("cpenm007")

		ah.Logging.Debug("HAED")
		ah.Logging.Debug(antrian)

		if err != nil {
			response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(antrian) > 0 {
			mapper := ah.AntreanMapper.ToPenmedikPasienAntreanMapper(antrian)
			ah.Logging.Info("ANTRIAN HAEMODALISA")
			ah.Logging.Info(mapper)

			m := map[string]any{}
			m["list"] = mapper

			response := helper.APIResponse("Ok", http.StatusOK, m)
			return c.Status(fiber.StatusOK).JSON(response)
		}

		m := map[string]any{}
		m["list"] = []string{}
		ah.Logging.Info("ANTRIAN KOSONG")

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)

	default:
		m := map[string]any{}
		m["list"] = []string{}

		response := helper.APIResponse("Ok", http.StatusOK, m)
		return c.Status(fiber.StatusOK).JSON(response)
	}
}

func (ah *AntreanHandler) GetDetailPasien(c *gin.Context) {
	payload := new(dto.RequestNorm)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		log.Error().Msg(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Detail pasien", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	m := map[string]any{}

	value, err := ah.AntreanRepository.GetDetailPasien(payload.NoRM)

	if err != nil {
		log.Error().Msg(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}
	riwayat, errs := ah.AntreanRepository.GetRiwayatPasien(payload.NoRM)

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(riwayat) > 0 {
		log.Info().Msg("Riwayat kosong")
		m["riwayat"] = riwayat
	} else {
		m["riwayat"] = []string{}
	}

	m["pasien"] = value

	response := helper.APIResponse("Ok", http.StatusOK, m)
	c.JSON(http.StatusOK, response)
}

func (ah *AntreanHandler) GetDetailPasienV2(c *fiber.Ctx) error {
	payload := new(dto.RequestNormV2)
	errs := c.BodyParser(&payload)

	ah.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		ah.Logging.Info("Data tidak dapat diproses")
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	m := map[string]any{}

	value, errs := ah.AntreanRepository.GetDetailPasien(payload.NoRM)

	if errs != nil {
		log.Error().Msg(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		ah.Logging.Info(errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	riwayat, errss := ah.AntreanRepository.GetRiwayatPasien(payload.NoRM)

	if errss != nil {
		log.Error().Msg(errss.Error())
		response := helper.APIResponseFailure(errss.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(riwayat) > 0 {
		m["riwayat"] = riwayat
	} else {
		log.Info().Msg("Riwayat kosong")
		m["riwayat"] = []string{}
	}

	m["pasien"] = value

	response := helper.APIResponse("Ok", http.StatusOK, m)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ah *AntreanHandler) GetDetailPasienV3(c *fiber.Ctx) error {
	payload := new(dto.RequestNormV2)
	errs := c.BodyParser(&payload)

	ah.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		ah.Logging.Info("Data tidak dapat diproses")
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		ah.Logging.Info("Data tidak dapat diproses")
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	m := map[string]any{}

	value, errs := ah.AntreanRepository.GetDetailPasienV3(payload.NoRM)

	if errs != nil {
		log.Error().Msg(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		ah.Logging.Info(errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	riwayat, errss := ah.AntreanRepository.GetRiwayatPasien(payload.NoRM)

	if errss != nil {
		response := helper.APIResponseFailure(errss.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(riwayat) > 0 {
		m["riwayat"] = riwayat
	} else {
		log.Info().Msg("Riwayat kosong")
		m["riwayat"] = []string{}
	}

	m["pasien"] = value

	response := helper.APIResponse("Ok", http.StatusOK, m)
	return c.Status(fiber.StatusOK).JSON(response)
}
