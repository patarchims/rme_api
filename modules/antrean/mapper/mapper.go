package mapper

import (
	"hms_api/modules/antrean"
	"hms_api/modules/antrean/dto"
	"hms_api/modules/antrean/entity"
	"time"
)

type AntreanImpl struct{}

func NewAntreanMapperImple() entity.AntreanMapper {
	return &AntreanImpl{}
}

func (am *AntreanImpl) ToResponseAntreanMapper(data []antrean.AntrianPasien) (value []dto.AntrianPasien) {

	for _, V := range data {

		tgl, _ := time.Parse("2006-01-02", (V.Tgllahir[0:10]))
		birthDate := time.Date(tgl.Year(), tgl.Month(), tgl.Day(), 0, 0, 0, 0, time.UTC)
		currentDate := time.Now()
		ageDuration := currentDate.Sub(birthDate)
		ageInYears := ageDuration.Hours() / 24 / 365

		value = append(value, dto.AntrianPasien{
			Keterangan:   "-",
			Proses:       "-",
			KdKelas:      V.KdKelas,
			Umur:         V.Umur,
			Tgllahir:     (V.Tgllahir[0:10]),
			Usia:         int(ageInYears),
			NoAntrean:    V.NoAntrian,
			JenisKelamin: V.Jeniskelamin,
			Debitur:      V.Debitur,
			KodeDebitur:  V.KdDebitur,
			Noreg:        V.Noreg,
			RegType:      V.RegType,
			Mrn:          V.Mrn,
			Status:       V.Status,
			NamaPasien:   V.Nampas,
			KdBag:        V.KdBagian,
			Bagian:       V.Bagian,
			Pelayanan:    V.Pelayanan,
			NamaDokter:   V.NamaDokter,
			KdDokter:     V.KdDokter,
		})

	}
	return value
}

func (am *AntreanImpl) ToAntreanPasienMapper(data []antrean.AntrianPoliIGD) (value []dto.AntrianPasien) {
	// var nama = ""

	for _, V := range data {

		value = append(value, dto.AntrianPasien{
			Keterangan:   "-",
			Proses:       "-",
			KdKelas:      "",
			Umur:         "",
			Tgllahir:     V.Tgllahir,
			Usia:         V.Umurth,
			NoAntrean:    V.NoAntrian,
			JenisKelamin: V.Jeniskelamin,
			Debitur:      "",
			KodeDebitur:  "",
			Noreg:        V.Noreg,
			RegType:      V.RegType,
			Mrn:          V.Id,
			Status:       V.Status,
			NamaPasien:   V.Nama,
			KdBag:        "",
			Bagian:       "",
			Pelayanan:    "IGD",
			NamaDokter:   V.Dokter,
			KdDokter:     V.Kodedr,
		})
	}

	return value
}

func (am *AntreanImpl) ToBangsalPasienAntrean(data []antrean.KbangsalKasur) (value []dto.AntrianPasien) {

	for _, V := range data {
		value = append(value, dto.AntrianPasien{
			Keterangan:   "-",
			Proses:       "-",
			KdKelas:      V.Kelas,
			Umur:         "",
			NoAntrean:    V.Kamar,
			JenisKelamin: V.Sex,
			Debitur:      V.Ket,
			KodeDebitur:  "",
			Noreg:        V.Noreg,
			Mrn:          V.Id,
			NamaPasien:   V.Nama,
			KdBag:        "",
			Bagian:       "",
			Tgllahir:     V.Tgllahir,
			Pelayanan:    V.Kodebangsal,
			NamaDokter:   V.Dokter,
			KdDokter:     V.Kodedr,
			Kelas:        V.Kelas,
			Kamar:        V.Kamar,
			Kasur:        V.Kasur,
		})
	}

	return value
}

func (am *AntreanImpl) ToPenmedikPasienAntreanMapper(data []antrean.PenMedikModel) (value []dto.AntrianPasien) {

	for _, V := range data {

		value = append(value, dto.AntrianPasien{
			Keterangan:   "-",
			Proses:       "-",
			KdKelas:      "",
			Umur:         "",
			NoAntrean:    V.Kamar,
			JenisKelamin: V.Jeniskelamin,
			Debitur:      "",
			KodeDebitur:  "",
			Noreg:        V.Noreg,
			Mrn:          V.Id,
			NamaPasien:   V.Nama,
			KdBag:        "",
			Bagian:       "",
			Pelayanan:    "Penmedik",
			NamaDokter:   V.Dokter,
		})
	}

	return value
}
