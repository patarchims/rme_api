package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/antrean"
	"hms_api/modules/antrean/entity"
	"hms_api/modules/user"
	"time"

	"gorm.io/gorm"
)

type antreanRepository struct {
	DB *gorm.DB
}

func NewLibRepository(db *gorm.DB) entity.AntreanRepository {
	return &antreanRepository{
		DB: db,
	}
}

func (lu *antreanRepository) GetAntreanResep() (res []antrean.AntreanResep, err error) {
	var results []antrean.AntreanResep

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *antreanRepository) GetAntreanPasienForPerawat(kdModul string) (res []antrean.AntrianPasien, err error) {
	query := "SELECT dr.kd_kelas , dr.umur, dp.tgllahir,   ao.no_antrian , dp.jeniskelamin, ao.debitur,    dr.kd_debitur,   ap.noreg, ao.reg_type,  dr.id AS mrn, ao.`status`,  dp.nama AS nampas,  ap.kd_tujuan AS kd_bagian, ap.kd_dokter, kp.bagian, kp.pelayanan, ke.nama AS nama_dokter FROM vicore_tmp.antrian_pasien AS ap LEFT JOIN vicore_his.dregister AS dr ON ap.noreg=dr.noreg LEFT JOIN vicore_his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN vicore_lib.kpelayanan AS kp ON kp.kd_bag=ap.kd_tujuan LEFT JOIN vicore_hrd.kemployee AS ke ON ap.kd_dokter=ke.idk LEFT JOIN vicore_tmp.antrian_ol AS ao ON ap.noreg=ao.noreg WHERE ap.kd_tujuan=?;"
	result := lu.DB.Raw(query, kdModul).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntrianIGDDokterUmumRepository(KodeDokter string) (res []antrean.AntrianPoliIGD, err error) {
	query := "SELECT nama, kodedr, a.id AS id, noreg, no_book, a.reg_type no_antrian, umurth, status, tgllahir, b.jeniskelamin, c.namadokter AS dokter from his.antrianpoliugd AS a LEFT JOIN his.dprofilpasien AS b ON  a.id=b.id LEFT JOIN his.ktaripdokter AS c ON c.iddokter=a.kodedr WHERE a.kodedr=? OR a.kodedr='' OR a.kodedr='' OR a.kodedr='NONE'"

	result := lu.DB.Raw(query, KodeDokter).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *antreanRepository) GetAntrianSpesialisasiRepository(KodeDokter string) (res []antrean.AntrianPoliIGD, err error) {
	query := "SELECT nama, kodedr, a.id AS id, noreg, no_book, a.reg_type no_antrian, umurth, status, tgllahir, b.jeniskelamin, c.namadokter AS dokter from his.antrianpoliugd AS a LEFT JOIN his.dprofilpasien AS b ON  a.id=b.id LEFT JOIN his.ktaripdokter AS c ON c.iddokter=a.kodedr WHERE a.kodedr=?"

	result := lu.DB.Raw(query, KodeDokter).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *antreanRepository) GetAntrianUGD() (res []antrean.AntrianPoliIGD, err error) {
	query := "SELECT nama, kodedr, a.id AS id, noreg, no_book, a.reg_type no_antrian, umurth, status, tgllahir, b.jeniskelamin, c.namadokter AS dokter from his.antrianpoliugd AS a LEFT JOIN his.dprofilpasien AS b ON  a.id=b.id LEFT JOIN his.ktaripdokter AS c ON c.iddokter=a.kodedr"

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetPasienBangsal(kodeBangsal string) (res []antrean.KbangsalKasur, err error) {
	query := `SELECT a.kodebangsal, a.kamar, a.kasur, b.tgllahir AS tgllahir, a.id, a.noreg, a.kodedr, b.firstname as nama, b.jeniskelamin AS sex, a.ket, ds.namadokter AS dokter FROM his.kbangsalkasur AS a INNER JOIN his.dprofilpasien AS b ON b.id=a.id LEFT JOIN his.ktaripdokter AS ds ON ds.iddokter=a.kodedr WHERE kodebangsal=? AND a.id !=""`

	result := lu.DB.Raw(query, kodeBangsal).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntrianRuangOperasi() (res []antrean.KbangsalKasur, err error) {
	query := `SELECT an.id,an.noreg,an.tgldaftar,an.kelas, dp.firstname as nama, dp.lastname, dp.tgllahir FROM his.antriankmoperasi AS an , his.dprofilpasien AS dp WHERE an.id=dp.id ORDER BY an.noreg ASC`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetPasienBangsalForDokter(kodeBangsal string, kodeDokter string) (res []antrean.KbangsalKasur, err error) {
	query := `SELECT a.kodebangsal, a.kamar, a.kasur, a.id, b.tgllahir AS tgllahir, a.noreg, a.kodedr, b.firstname as nama, a.sex, a.ket, ds.namadokter AS dokter,  a.kasur AS kasur, a.kamar AS kamar FROM his.kbangsalkasur AS a INNER JOIN his.dprofilpasien AS b ON b.id=a.id LEFT JOIN his.ktaripdokter AS ds ON ds.iddokter=a.kodedr WHERE kodebangsal=? AND a.id !=""`

	result := lu.DB.Raw(query, kodeBangsal).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntrianPasienKamarOperasiRepository() (res []antrean.KbangsalKasur, err error) {
	query := `SELECT ah.noreg, ah.id,  ah.kelas, ah.tgldaftar, ah.kelas, hd.nik , hd.tgllahir AS tgllahir, hd.firstname AS nama FROM his.antriankmoperasi AS ah INNER JOIN his.dprofilpasien  AS hd ON ah.id = hd.id`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntrianPenmedikRepository(namaPenmedik string) (res []antrean.PenMedikModel, err error) {

	errs := lu.DB.Where("kodepenmedik=?", namaPenmedik).Find(&res).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntrianPoliGigi() (res []antrean.KbangsalKasur, err error) {
	query := `SELECT  b.firstname as nama, kodedr, a.id AS id, noreg, no_book, a.reg_type no_antrian, umurth, status, tgllahir, b.jeniskelamin, c.namadokter AS dokter from his.antrianpoligigi AS a LEFT JOIN his.dprofilpasien AS b ON  a.id=b.id LEFT JOIN his.ktaripdokter AS c ON c.iddokter=a.kodedr`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntreanIGD(kdModul string) (res []antrean.AntrianPasien, err error) {

	query := "SELECT dr.kd_kelas, dr.umur, dp.tgllahir,   ao.no_antrian, dp.jeniskelamin, ao.debitur,    dr.kd_debitur,   ap.noreg, ao.reg_type,  dr.id AS mrn, ao.`status`,  dp.nama AS nampas,  ap.kd_tujuan AS kd_bagian, ap.kd_dokter, kp.bagian, kp.pelayanan, ke.nama AS nama_dokter FROM vicore_tmp.antrian_pasien AS ap LEFT JOIN vicore_his.dregister AS dr ON ap.noreg=dr.noreg LEFT JOIN vicore_his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN vicore_lib.kpelayanan AS kp ON kp.kd_bag=ap.kd_tujuan LEFT JOIN vicore_hrd.kemployee AS ke ON ap.kd_dokter=ke.idk LEFT JOIN vicore_tmp.antrian_ol AS ao ON ap.noreg=ao.noreg WHERE ap.kd_tujuan=?;"

	result := lu.DB.Raw(query, kdModul).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetAntreanPasien(kodeDokter string, kdModul string) (res []antrean.AntrianPasien, err error) {
	query := "SELECT dr.kd_kelas, dr.umur, dp.tgllahir,   ao.no_antrian , dp.jeniskelamin, ao.debitur,    dr.kd_debitur,   ap.noreg, ao.reg_type,  dr.id AS mrn, ao.`status`,  dp.nama AS nampas,  ap.kd_tujuan AS kd_bagian, ap.kd_dokter, kp.bagian, kp.pelayanan, ke.nama AS nama_dokter FROM vicore_tmp.antrian_pasien AS ap LEFT JOIN vicore_his.dregister AS dr ON ap.noreg=dr.noreg LEFT JOIN vicore_his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN vicore_lib.kpelayanan AS kp ON kp.kd_bag=ap.kd_tujuan LEFT JOIN vicore_hrd.kemployee AS ke ON ap.kd_dokter=ke.idk LEFT JOIN vicore_tmp.antrian_ol AS ao ON ap.noreg=ao.noreg WHERE ap.kd_tujuan=? AND ap.kd_dokter=?;"
	result := lu.DB.Raw(query, kdModul, kodeDokter).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetDetailPasien(noRm string) (res antrean.DprofilePasien, err error) {
	query := `SELECT hp, id,nik,nokapst,nama,tempatlahir, COALESCE(LPAD(DAY(tgllahir), 2, '0'),'01') HAR,COALESCE(MONTHNAME(tgllahir),'January') AS BUL,
	COALESCE(YEAR(tgllahir),'2000') AS TAH, jeniskelamin,status,negara,propinsi,kabupaten,kecamatan,kelurahan,rtrw,alamat,alamat2,
	COALESCE(hp,telp) AS CPN, agama,suku,pendidikan,pekerjaan,cp_name,cp_number,cp_relasi, TIMESTAMPDIFF(YEAR,tgllahir, CURDATE()) 
	AS tahun,(TIMESTAMPDIFF(MONTH,tgllahir, CURDATE())%12) AS bulan,((DATEDIFF(CURDATE(),tgllahir) % 365)%30) AS hari
	FROM vicore_his.dprofilpasien WHERE id=? LIMIT 1 `

	result := lu.DB.Raw(query, noRm).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetDetailPasienV3(noRm string) (res antrean.DprofilePasien, err error) {
	query := `SELECT hp, id,nik,nokapst,firstname AS nama,tempatlahir, COALESCE(LPAD(DAY(tgllahir), 2, '0'),'01') HAR,COALESCE(MONTHNAME(tgllahir),'January') AS BUL, COALESCE(YEAR(tgllahir),'2000') AS TAH, jeniskelamin,status,negara,propinsi,kabupaten,kecamatan,kelurahan,rtrw,alamat,alamat2,
	COALESCE(hp,telp) AS CPN, agama,suku,pendidikan,pekerjaan,cp_name,cp_number,cp_relasi, TIMESTAMPDIFF(YEAR,tgllahir, CURDATE()) 
	AS tahun,(TIMESTAMPDIFF(MONTH,tgllahir, CURDATE())%12) AS bulan,((DATEDIFF(CURDATE(),tgllahir) % 365)%30) AS hari
	FROM his.dprofilpasien WHERE id=? LIMIT 1 `

	result := lu.DB.Raw(query, noRm).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetRiwayatPasien(noRM string) (res []antrean.RiwayatPasien, err error) {
	query := `SELECT DISTINCT(vicore_usr.users.email) AS email, dregister.noreg,
	dregister.reg_insert_dttm, vicore_lib.kpelayanan.bagian,
	COALESCE(vicore_hrd.kemployee.nama,'None') AS dokter,
	dregister.umur, vicore_lib.kdebitur.debitur, COALESCE(vicore_jkn.dsep.no_sep,'-') AS no_sep FROM vicore_his.dregister LEFT JOIN vicore_jkn.dsep ON dregister.noreg=vicore_jkn.dsep.noreg LEFT JOIN vicore_lib.kpelayanan ON dregister.kd_tujuan=vicore_lib.kpelayanan.kd_bag LEFT JOIN vicore_hrd.kemployee ON dregister.kd_dokter=vicore_hrd.kemployee.idk LEFT JOIN vicore_lib.kdebitur ON dregister.kd_debitur=vicore_lib.kdebitur.kode LEFT JOIN vicore_usr.users ON dregister.reg_user_id=vicore_usr.users.user_id WHERE dregister.id=?  ORDER BY dregister.noreg DESC`

	result := lu.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *antreanRepository) GetBookKamar() (res []antrean.BookKamar, err error) {
	var results []antrean.BookKamar

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *antreanRepository) OnGetAntreanIGDYangTelahDiProsesRepsoitory() (res []antrean.Dpoliugd6, err error) {
	currentDate := time.Now().Format("2006-01-02")

	result := lu.DB.Where(antrean.Dpoliugd6{
		Tanggal: currentDate,
		Bagian:  "Poli UGD",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (ur *antreanRepository) FindUserByID(userID string) (res user.Kemploye, err error) {
	result := ur.DB.Where("idk = ?", userID).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
