package usecase

import (
	"fmt"
	"hms_api/modules/antrean/dto"
	"hms_api/modules/antrean/entity"
	igdEntity "hms_api/modules/igd/entity"
	"strings"

	"github.com/sirupsen/logrus"
)

type antreanUseCase struct {
	AntrianRepository entity.AntreanRepository
	AntrianMapper     entity.AntreanMapper
	IGDRepository     igdEntity.IGDRepository
	IGDMapper         igdEntity.IGDMapper
	Logging           *logrus.Logger
}

func NewAntreanUseCase(antrianRepo entity.AntreanRepository, antraianMapper entity.AntreanMapper, igdMapper igdEntity.IGDMapper, igdRepo igdEntity.IGDRepository, logging *logrus.Logger) entity.AntreanUseCase {
	return &antreanUseCase{
		AntrianRepository: antrianRepo,
		AntrianMapper:     antraianMapper,
		IGDRepository:     igdRepo,
		IGDMapper:         igdMapper,
	}
}

func (iu *antreanUseCase) OnGetAntrianIGDUseCase(modulID string, person string, userID string) (res []dto.AntrianPasien, message string, err error) {
	switch modulID {
	case "IGD001":
		if person == "dokter" {
			spesial := strings.Split(userID, "_")
			if spesial[0] == "DU" {

				var antrianpasien = []dto.AntrianPasien{}

				antrianDokterUmum, er12 := iu.AntrianRepository.GetAntrianIGDDokterUmumRepository(userID)

				if er12 != nil {
					return make([]dto.AntrianPasien, 0), "Data tidak ditemukan", er12
				}

				// JIKA ANTRIAN PASIEN KOSONG
				if len(antrianDokterUmum) == 0 {
					return make([]dto.AntrianPasien, 0), "Data kosong", nil
				}

				// LOOPING ANTRIAN DOKTER UMUM
				for i := 0; i <= len(antrianDokterUmum)-1; i++ {
					// GET ASESMEN PERAWAT & GET ASESMEN DOKTER
					asesmenPerawat, _ := iu.IGDRepository.OnGetPengkajianKeperawatanRepository(modulID, "RAJAL", antrianDokterUmum[i].Noreg)
					asesmenDokter, _ := iu.IGDRepository.OnGeAsesmenDokterRepository(antrianDokterUmum[i].Noreg, modulID)

					antrianpasien = append(antrianpasien, dto.AntrianPasien{
						Tgllahir:       antrianDokterUmum[i].Tgllahir,
						Usia:           antrianDokterUmum[i].Umurth,
						NoAntrean:      antrianDokterUmum[i].NoAntrian,
						JenisKelamin:   antrianDokterUmum[i].Jeniskelamin,
						Debitur:        "-",
						KodeDebitur:    "-",
						Noreg:          antrianDokterUmum[i].Noreg,
						RegType:        antrianDokterUmum[i].RegType,
						Mrn:            antrianDokterUmum[i].Id,
						Status:         antrianDokterUmum[i].Status,
						Keterangan:     "-",
						Proses:         "-",
						NamaPasien:     antrianDokterUmum[i].Nama,
						KdBag:          "-",
						Bagian:         "-",
						Pelayanan:      "IGD001",
						NamaDokter:     antrianDokterUmum[i].Dokter,
						KdKelas:        "-",
						Kelas:          "-",
						Umur:           fmt.Sprintf("%s ", antrianDokterUmum[i].Umurth),
						KdDokter:       antrianDokterUmum[i].Kodedr,
						Kamar:          "-",
						Kasur:          "-",
						AsesmenDokter:  asesmenDokter.Dokter.Namadokter,
						AsesmenPerawat: asesmenPerawat.Perawat.Namaperawat,
					})
				}

				return antrianpasien, "OK", nil
			}
		}

		antarinUGD, er12 := iu.AntrianRepository.GetAntrianUGD()

		if er12 != nil {
			return make([]dto.AntrianPasien, 0), er12.Error(), er12
		}

		var antrianpasien = []dto.AntrianPasien{}

		if len(antarinUGD) == 0 {
			return make([]dto.AntrianPasien, 0), "Data tidak ditemukan", nil
		}

		for i := 0; i <= len(antarinUGD)-1; i++ {
			asesmenPerawat, _ := iu.IGDRepository.OnGetPengkajianKeperawatanRepository(modulID, "RAJAL", antarinUGD[i].Noreg)
			asesmenDokter, _ := iu.IGDRepository.OnGeAsesmenDokterRepository(antarinUGD[i].Noreg, modulID)

			antrianpasien = append(antrianpasien, dto.AntrianPasien{
				Tgllahir:       antarinUGD[i].Tgllahir,
				Usia:           antarinUGD[i].Umurth,
				NoAntrean:      antarinUGD[i].NoAntrian,
				JenisKelamin:   antarinUGD[i].Jeniskelamin,
				Debitur:        "-",
				KodeDebitur:    "-",
				Noreg:          antarinUGD[i].Noreg,
				RegType:        antarinUGD[i].RegType,
				Mrn:            antarinUGD[i].Id,
				Status:         antarinUGD[i].Status,
				Keterangan:     "-",
				Proses:         "-",
				NamaPasien:     antarinUGD[i].Nama,
				KdBag:          "-",
				Bagian:         "-",
				Pelayanan:      "IGD001",
				NamaDokter:     antarinUGD[i].Dokter,
				KdKelas:        "-",
				Kelas:          "-",
				Umur:           fmt.Sprintf("%s ", antarinUGD[i].Umurth),
				KdDokter:       antarinUGD[i].Kodedr,
				Kamar:          "-",
				Kasur:          "-",
				AsesmenDokter:  asesmenDokter.Dokter.Namadokter,
				AsesmenPerawat: asesmenPerawat.Perawat.Namaperawat,
			})
		}

		return antrianpasien, "OK", nil
	default:
		if person == "dokter" {
			antrians, err := iu.AntrianRepository.GetPasienBangsalForDokter(modulID, userID)

			if err != nil {
				return make([]dto.AntrianPasien, 0), "Data tidak ditemukan", err
			}

			if len(antrians) == 0 {
				return make([]dto.AntrianPasien, 0), "Data kosong", nil
			}

			var antrianpasien = []dto.AntrianPasien{}

			// LOOPING DATA JIKA DITEMUKAN
			for i := 0; i <= len(antrians)-1; i++ {
				asesmenDokter, _ := iu.IGDRepository.OnGeAsesmenDokterRepository(antrians[i].Noreg, modulID)
				asesmenPerawat, _ := iu.IGDRepository.OnGetPengkajianKeperawatanRepository(modulID, "RANAP", antrians[i].Noreg)

				antrianpasien = append(antrianpasien, dto.AntrianPasien{
					Tgllahir:       antrians[i].Tgllahir,
					Usia:           0,
					NoAntrean:      "-",
					JenisKelamin:   antrians[i].Sex,
					Debitur:        "-",
					KodeDebitur:    "-",
					Noreg:          antrians[i].Noreg,
					RegType:        "-",
					Mrn:            antrians[i].Id,
					Status:         "-",
					Keterangan:     "-",
					Proses:         "-",
					NamaPasien:     antrians[i].Nama,
					KdBag:          "-",
					Bagian:         "-",
					Pelayanan:      "IGD001",
					NamaDokter:     antrians[i].Dokter,
					KdKelas:        "-",
					Kelas:          "-",
					Umur:           antrians[i].Umur,
					KdDokter:       antrians[i].Kodedr,
					Kamar:          "-",
					Kasur:          "-",
					AsesmenDokter:  asesmenDokter.Dokter.Namadokter,
					AsesmenPerawat: asesmenPerawat.Perawat.Namaperawat,
				})
			}

			return antrianpasien, "OK", nil
		}

		// JIKA TIDAK DOKTER
		antrianBangsal, er122 := iu.AntrianRepository.GetPasienBangsal(modulID)

		if er122 != nil {
			return make([]dto.AntrianPasien, 0), "Data tidak ditemukan", er122
		}

		if len(antrianBangsal) == 0 {
			return make([]dto.AntrianPasien, 0), "Data kosong", nil
		}

		var antrianpasien = []dto.AntrianPasien{}

		for i := 0; i <= len(antrianBangsal)-1; i++ {
			asesmenDokter, _ := iu.IGDRepository.OnGetAsesmenDokterRANAPRepository(antrianBangsal[i].Noreg, modulID)
			asesmenPerawat, _ := iu.IGDRepository.OnGetPengkajianKeperawatanRepository(modulID, "RANAP", antrianBangsal[i].Noreg)

			antrianpasien = append(antrianpasien, dto.AntrianPasien{
				Tgllahir:       antrianBangsal[i].Tgllahir,
				Usia:           0,
				NoAntrean:      "-",
				JenisKelamin:   antrianBangsal[i].Sex,
				Debitur:        "-",
				KodeDebitur:    "-",
				Noreg:          antrianBangsal[i].Noreg,
				RegType:        "-",
				Mrn:            antrianBangsal[i].Id,
				Status:         "-",
				Keterangan:     "-",
				Proses:         "-",
				NamaPasien:     antrianBangsal[i].Nama,
				KdBag:          "-",
				Bagian:         "-",
				Pelayanan:      "IGD001",
				NamaDokter:     antrianBangsal[i].Dokter,
				KdKelas:        "-",
				Kelas:          "-",
				Umur:           antrianBangsal[i].Umur,
				KdDokter:       antrianBangsal[i].Kodedr,
				Kamar:          antrianBangsal[i].Kamar,
				Kasur:          antrianBangsal[i].Kasur,
				AsesmenDokter:  asesmenDokter.Dokter.Namadokter,
				AsesmenPerawat: asesmenPerawat.Perawat.Namaperawat,
			})
		}

		return antrianpasien, "OK", nil
	}
}

func (iu *antreanUseCase) OnGetAntreanIGDTelahDiprosesUseCase(modulID string) (res []dto.AntrianPasien, message string, err error) {
	antrean, er12 := iu.AntrianRepository.OnGetAntreanIGDYangTelahDiProsesRepsoitory()

	if er12 != nil {
		return make([]dto.AntrianPasien, 0), "Data kosong", nil
	}

	// LAKUKAN CEK ASESMEN DOKTER DAN PERAWAT
	var antrianpasien = []dto.AntrianPasien{}

	if len(antrean) == 0 {
		return make([]dto.AntrianPasien, 0), "Data kosong", nil
	}

	for i := 0; i <= len(antrean)-1; i++ {
		asesmenPerawat, _ := iu.IGDRepository.OnGetPengkajianKeperawatanRepository(modulID, "RAJAL", antrean[i].Noreg)
		asesmenDokter, _ := iu.IGDRepository.OnGeAsesmenDokterRepository(antrean[i].Noreg, modulID)

		antrianpasien = append(antrianpasien, dto.AntrianPasien{
			Tgllahir:       antrean[i].Tanggal,
			Usia:           0,
			NoAntrean:      "-",
			Debitur:        "-",
			KodeDebitur:    "-",
			Noreg:          antrean[i].Noreg,
			RegType:        "-",
			Mrn:            antrean[i].ID,
			Status:         "-",
			Keterangan:     "-",
			Proses:         "-",
			NamaPasien:     antrean[i].Nama,
			KdBag:          "-",
			Bagian:         "-",
			Pelayanan:      "IGD001",
			NamaDokter:     antrean[i].Dokter,
			KdKelas:        "-",
			Kelas:          "-",
			Umur:           fmt.Sprintf("%s ", antrean[i].Umur),
			KdDokter:       antrean[i].Dokter,
			Kamar:          "-",
			Kasur:          "-",
			AsesmenDokter:  asesmenDokter.Dokter.Namadokter,
			AsesmenPerawat: asesmenPerawat.Perawat.Namaperawat,
		})

	}

	return antrianpasien, "OK", nil
}
