package diagnosa

import (
	"hms_api/modules/lib"
	"time"
)

type (

	// DIAGNOAS BANDING
	DDiagnosaBanding struct {
		IDDiagnosa int        `gorm:"column:id_diagnosa;primaryKey;autoIncrement" json:"id_diagnosa"`
		InsertDttm time.Time  `gorm:"column:insert_dttm;" json:"insert_dttm"`
		UserID     string     `gorm:"column:user_id;size:25" json:"user_id"`
		Noreg      string     `gorm:"column:noreg;size:25" json:"noreg"`
		KdBagian   string     `gorm:"column:kd_bagian;size:25" json:"kd_bagian"`
		Pelayanan  string     `gorm:"column:pelayanan;type:enum('RAJAL', 'RANAP');" json:"pelayanan"`
		Code       string     `gorm:"column:code;primaryKey:Code;size:25" json:"code"`
		Diagnosa   lib.KICD10 `gorm:"foreignKey:Code2;references:Code"  json:"diagnosa"`
	}
)

// TableName sets the table name for this struct type
func (DDiagnosaBanding) TableName() string {
	return "vicore_rme.ddiagnosa_banding"
}
