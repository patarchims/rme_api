package dto

type (

	// REQUEST ADD DIAGNOSA BANDING
	AddResponseDiagnosaBanding struct {
		NoReg string
		Code  string
	}

	OnGetDiagnosaBanding struct {
		Noreg     string `json:"noreg" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
	}

	OnSaveDiagnosaBanding struct {
		Noreg     string `json:"noreg" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
		Code      string `json:"code" binding:"required"`
	}

	OnDeleteDiagnosaBanding struct {
		ID int `json:"ID" binding:"required"`
	}
)
