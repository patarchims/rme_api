package entity

import (
	"hms_api/modules/diagnosa"
	"hms_api/modules/diagnosa/dto"
)

type DiagnosaRepository interface {
	OnDeleteDiagnosaBandingRepository(ID int) (res diagnosa.DDiagnosaBanding, err error)
	OnGetDianosaBandingRepository(noReg string, kdBagian string, pelayanan string) (res []diagnosa.DDiagnosaBanding, err error)
	OnSaveDiagnosaBandingRepository(kdBagian string, userID string, req dto.OnSaveDiagnosaBanding, pelayanan string) (res diagnosa.DDiagnosaBanding, err error)
}

type DiagnosaMapper interface {
}

// DIAGNOSA USECASE
type DiagnosaUseCase interface {
	OnDeleteDiagnosaBandingUsecase(ID int) (res diagnosa.DDiagnosaBanding, message string, err error)
	OnSaveDiagnosaUseCase(req dto.OnSaveDiagnosaBanding, kdBagian string, userID string, pelayanan string) (res diagnosa.DDiagnosaBanding, message string, err error)
	OnGetDiagnosaUseCase(noReg string, kdBagian string, pelayanan string) (res []diagnosa.DDiagnosaBanding, message string, err error)
}
