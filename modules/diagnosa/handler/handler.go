package handler

import (
	"fmt"
	"hms_api/modules/diagnosa/dto"
	"hms_api/modules/diagnosa/entity"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type DiagnosaHandler struct {
	DiagnosaRepository entity.DiagnosaRepository
	DiagnosaUseCase    entity.DiagnosaUseCase
	DiagnosaMapper     entity.DiagnosaMapper
	Logging            *logrus.Logger
}

func (dh *DiagnosaHandler) OnSaveDiagnosaBandingFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveDiagnosaBanding)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	dh.Logging.Info("PAYLOAD")
	dh.Logging.Info(payload)

	onSave, message, er12 := dh.DiagnosaUseCase.OnSaveDiagnosaUseCase(*payload, modulID, userID, payload.Pelayanan)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, onSave)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *DiagnosaHandler) OnDeleteDiagnosaBandingFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteDiagnosaBanding)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	dh.Logging.Info("PAYLOAD")
	dh.Logging.Info(payload)

	// userID := c.Locals("userID").(string)
	// modulID := c.Locals("modulID").(string)

	diagnosa, message, er12 := dh.DiagnosaUseCase.OnDeleteDiagnosaBandingUsecase(payload.ID)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, diagnosa)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *DiagnosaHandler) OnGetDiagnosaBandingFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetDiagnosaBanding)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	dh.Logging.Info("PAYLOAD")
	dh.Logging.Info(payload)

	// userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	diagnosa, message, er12 := dh.DiagnosaUseCase.OnGetDiagnosaUseCase(payload.Noreg, modulID, payload.Pelayanan)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, diagnosa)
	return c.Status(fiber.StatusOK).JSON(response)

}
