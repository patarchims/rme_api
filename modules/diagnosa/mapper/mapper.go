package mapper

import "hms_api/modules/diagnosa/entity"

type DiagnosaImple struct{}

func NewDiagnosaMapperImple() entity.DiagnosaMapper { return &DiagnosaImple{} }
