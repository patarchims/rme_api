package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/diagnosa"
	"hms_api/modules/diagnosa/dto"
	"hms_api/modules/diagnosa/entity"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type diagnosaRepository struct {
	DB      *gorm.DB
	Logging *logrus.Logger
}

func NewDiagnosaRepository(db *gorm.DB, logging *logrus.Logger) entity.DiagnosaRepository {
	return &diagnosaRepository{
		DB:      db,
		Logging: logging,
	}
}

func (lu *diagnosaRepository) OnSaveDiagnosaBandingRepository(kdBagian string, userID string, req dto.OnSaveDiagnosaBanding, pelayanan string) (res diagnosa.DDiagnosaBanding, err error) {

	data := diagnosa.DDiagnosaBanding{
		InsertDttm: time.Now(),
		KdBagian:   kdBagian,
		UserID:     userID,
		Noreg:      req.Noreg,
		Pelayanan:  req.Pelayanan,
		Code:       req.Code,
	}

	result := lu.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (lu *diagnosaRepository) OnGetDianosaBandingRepository(noReg string, kdBagian string, pelayanan string) (res []diagnosa.DDiagnosaBanding, err error) {
	result := lu.DB.Where(&diagnosa.DDiagnosaBanding{
		Noreg: noReg, KdBagian: kdBagian, Pelayanan: pelayanan,
	}).Preload("Diagnosa").Order("id_diagnosa ASC").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Data tidak ditemukan, Error %s,", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *diagnosaRepository) OnDeleteDiagnosaBandingRepository(ID int) (res diagnosa.DDiagnosaBanding, err error) {
	result := lu.DB.Where(&diagnosa.DDiagnosaBanding{IDDiagnosa: ID}).Delete(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Data gagal dihapus %s,", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}
