package usecase

import (
	"hms_api/modules/diagnosa"
	"hms_api/modules/diagnosa/dto"
	"hms_api/modules/diagnosa/entity"

	"github.com/sirupsen/logrus"
)

type diagnosaUsecase struct {
	logging            *logrus.Logger
	diagnosaMapper     entity.DiagnosaMapper
	diagnosaRepository entity.DiagnosaRepository
}

func NewDiagnosaUseCase(dRepository entity.DiagnosaRepository, logging *logrus.Logger, mapper entity.DiagnosaMapper) entity.DiagnosaUseCase {
	return &diagnosaUsecase{
		logging:            logging,
		diagnosaMapper:     mapper,
		diagnosaRepository: dRepository,
	}
}

func (du *diagnosaUsecase) OnSaveDiagnosaUseCase(req dto.OnSaveDiagnosaBanding, kdBagian string, userID string, pelayanan string) (res diagnosa.DDiagnosaBanding, message string, err error) {
	save, er11 := du.diagnosaRepository.OnSaveDiagnosaBandingRepository(kdBagian, userID, req, pelayanan)

	if er11 != nil {
		return res, "Data gagal disimpan", er11
	}

	return save, "Data berhasil disimpan", nil
}

func (du *diagnosaUsecase) OnDeleteDiagnosaBandingUsecase(ID int) (res diagnosa.DDiagnosaBanding, message string, err error) {
	data, er123 := du.diagnosaRepository.OnDeleteDiagnosaBandingRepository(ID)

	if er123 != nil {
		return res, "Data gagal dihapus", er123
	}
	return data, "Data berhasil dihapus", nil
}

// ON GET DIANODA BANDING USECASE
func (du *diagnosaUsecase) OnGetDiagnosaUseCase(noReg string, kdBagian string, pelayanan string) (res []diagnosa.DDiagnosaBanding, message string, err error) {

	getData, er123 := du.diagnosaRepository.OnGetDianosaBandingRepository(noReg, kdBagian, pelayanan)

	if er123 != nil {
		return make([]diagnosa.DDiagnosaBanding, 0), er123.Error(), er123
	}

	if len(getData) < 1 {
		return make([]diagnosa.DDiagnosaBanding, 0), "Data kosong", nil
	}

	return getData, "OK", nil
}
