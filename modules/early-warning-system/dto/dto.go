package dto

type (
	OnSaveKontrolPasien struct {
		Noreg        string  `json:"no_reg" binding:"required"`
		Kategori     string  `json:"kategori" binding:"required"`
		GCSE         int     `json:"gcs_e" binding:"required"`
		GCSV         int     `json:"gcs_v" binding:"required"`
		GCSM         int     `json:"gcs_m" binding:"required"`
		TDSistolik   int     `json:"sistolik" binding:"required"`
		TDDiastolik  int     `json:"diastolik" binding:"required"`
		Nadi         int     `json:"nadi" binding:"required"`
		Pernapasan   int     `json:"pernapasan" binding:"required"`
		ReaksiOtot   string  `json:"reaksi_otot" binding:"required"`
		Suhu         float32 `json:"suhu" binding:"required"`
		Spo2         int     `json:"spo2" binding:"required"`
		CRT          int     `json:"crt" binding:"required"`
		Obsigen      string  `json:"obsigen" binding:"required"`
		ProteinUrine int     `json:"protein_urine" binding:"required"`
	}

	OnGetKontrolPasien struct {
		Noreg    string `json:"no_reg"`
		Kategori string `json:"kategori"`
	}

	ResponseKontrolPasien struct {
		Noreg        string  `json:"no_reg"`
		Kategori     string  `json:"kategori"`
		GCSE         int     `json:"gcs_e"`
		GCSV         int     `json:"gcs_v"`
		GCSM         int     `json:"gcs_m"`
		TotalGCS     int     `json:"total_gcs"`
		Kesadaran    string  `json:"kesadaran"`
		TDSistolik   int     `json:"sistolik"`
		TDDiastolik  int     `json:"diastolik"`
		Nadi         int     `json:"nadi"`
		Pernapasan   int     `json:"pernapasan"`
		ReaksiOtot   string  `json:"reaksi_otot"`
		Suhu         float32 `json:"suhu"`
		Spo2         int     `json:"spo2"`
		CRT          int     `json:"crt"`
		Obsigen      string  `json:"obsigen"`
		ProteinUrine int     `json:"protein_urine"`
	}
)
