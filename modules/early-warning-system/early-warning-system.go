package earlywarningsystem

type (
	DKontrolPasien struct {
		IDKontrol    int     `gorm:"column:id_kontrol;primaryKey;autoIncrement"`
		Noreg        string  `gorm:"column:noreg;size:225;default:225"`
		KdBagian     string  `gorm:"column:kd_bagian;size:225"`
		UserID       string  `gorm:"column:user_id;size:225"`
		Kategori     string  `gorm:"column:kategori;type:enum('ANAK', 'DEWASA');default:'DEWASA'"`
		GCSE         int     `gorm:"column:gcs_e"`
		GCSV         int     `gorm:"column:gcs_v"`
		GCSM         int     `gorm:"column:gcs_m"`
		TDSistolik   int     `gorm:"column:td_sistolik"`
		TDDiastolik  int     `gorm:"column:td_diastolik"`
		Nadi         int     `gorm:"column:nadi"`
		Pernapasan   int     `gorm:"column:pernapasan"`
		ReaksiOtot   string  `gorm:"column:reaksi_otot;type:enum('BERAT', 'SEDANG');default:'SEDANG'"`
		Suhu         float32 `gorm:"column:suhu"`
		Spo2         int     `gorm:"column:spo2"`
		CRT          int     `gorm:"column:crt;"`
		Obsigen      string  `gorm:"column:obsigen;type:enum('YA', 'TIDAK')"`
		ProteinUrine int     `gorm:"column:protein_urine"`
	}
)

// TableName overrides the table name used by GORM
func (DKontrolPasien) TableName() string {
	return "vicore_rme.dkontrol_pasien"
}
