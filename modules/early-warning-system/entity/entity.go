package entity

import (
	earlywarningsystem "hms_api/modules/early-warning-system"
	"hms_api/modules/early-warning-system/dto"
)

type EarlyMapper interface {
	ToMappingEarlyWarningSystem(data []earlywarningsystem.DKontrolPasien) (res []dto.ResponseKontrolPasien)
}

type EarlyUseCase interface {
	OnGetKontrolPasienUseCase(req dto.OnGetKontrolPasien, kdBagian string) (res []dto.ResponseKontrolPasien, err error)
	OnSaveKontrolPasienUseCase(req dto.OnSaveKontrolPasien, userID string, kdBagian string) (message string, err error)
}

type EalryRepository interface {
	OnSaveKontrolPasienRepository(data earlywarningsystem.DKontrolPasien) (res earlywarningsystem.DKontrolPasien, err error)
	OnGetKontrolPasienRepository(req dto.OnGetKontrolPasien, kdBagian string) (res []earlywarningsystem.DKontrolPasien, err error)
}
