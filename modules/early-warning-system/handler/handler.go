package handler

import (
	"fmt"
	"hms_api/modules/early-warning-system/dto"
	"hms_api/modules/early-warning-system/entity"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type EarlyHandler struct {
	EarlyRepository entity.EalryRepository
	EarlyUseCase    entity.EarlyUseCase
	EarlyMapper     entity.EarlyMapper
	Logging         *logrus.Logger
}

func (hh *EarlyHandler) OnSaveKontrolPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveKontrolPasien)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Info(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if payload.GCSE == 0 {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("data tidak dapat disimpan, GCS-E tidak boleh %s ", payload.GCSE)
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if payload.GCSM == 0 {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("data tidak dapat disimpan, GCS-E tidak boleh %s", payload.GCSM)
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if payload.GCSV == 0 {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("data tidak dapat disimpan, GCS-E tidak boleh %s", payload.GCSV)
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	message, er11 := hh.EarlyUseCase.OnSaveKontrolPasienUseCase(*payload, userID, modulID)

	if er11 != nil {
		response := helper.APIResponseFailure(message, http.StatusAccepted)
		hh.Logging.Info(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *EarlyHandler) OnGetKontrolPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetKontrolPasien)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	data, er11 := hh.EarlyUseCase.OnGetKontrolPasienUseCase(*payload, modulID)

	if er11 != nil {
		response := helper.APIResponseFailure(er11.Error(), http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
