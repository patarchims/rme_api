package mapper

import (
	earlywarningsystem "hms_api/modules/early-warning-system"
	"hms_api/modules/early-warning-system/dto"
	"hms_api/modules/early-warning-system/entity"
)

type EarlyMapperImple struct{}

func NewEarlyMapperImple() entity.EarlyMapper {
	return &EarlyMapperImple{}
}

func (a *EarlyMapperImple) ToMappingEarlyWarningSystem(data []earlywarningsystem.DKontrolPasien) (res []dto.ResponseKontrolPasien) {
	for _, V := range data {
		res = append(res, dto.ResponseKontrolPasien{
			Noreg:        V.Noreg,
			Kategori:     V.Kategori,
			GCSE:         V.GCSE,
			GCSV:         V.GCSV,
			GCSM:         V.GCSM,
			TotalGCS:     V.GCSE + V.GCSV + V.GCSM,
			Kesadaran:    toKesadaran(V.GCSE + V.GCSV + V.GCSM),
			TDSistolik:   V.TDSistolik,
			TDDiastolik:  V.TDDiastolik,
			Nadi:         V.Nadi,
			Pernapasan:   V.Pernapasan,
			ReaksiOtot:   V.ReaksiOtot,
			Suhu:         V.Suhu,
			Spo2:         V.Spo2,
			CRT:          V.CRT,
			Obsigen:      V.Obsigen,
			ProteinUrine: V.ProteinUrine,
		})
	}

	return res
}

func toKesadaran(value int) string {
	switch {
	case value >= 15 && value <= 14:
		return "Composmentis"
	case value >= 13 && value <= 12:
		return "Apatis"
	case value >= 11 && value <= 10:
		return "Delirium"
	case value >= 9 && value <= 7:
		return "Somnolen"
	case value >= 6 && value <= 5:
		return "Sopor"
	case value == 4:
		return "Semi-coma"
	case value == 3:
		return "Coma"
	default:
		return "Unknown"
	}
}
