package repository

import (
	"errors"
	"fmt"
	earlywarningsystem "hms_api/modules/early-warning-system"
	"hms_api/modules/early-warning-system/dto"
	"hms_api/modules/early-warning-system/entity"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type earlyRepository struct {
	DB      *gorm.DB
	Logging *logrus.Logger
}

func NewEarlyRepository(db *gorm.DB, logging *logrus.Logger) entity.EalryRepository {
	return &earlyRepository{
		DB:      db,
		Logging: logging,
	}
}

func (lu *earlyRepository) OnSaveKontrolPasienRepository(data earlywarningsystem.DKontrolPasien) (res earlywarningsystem.DKontrolPasien, err error) {

	result := lu.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (lu *earlyRepository) OnGetKontrolPasienRepository(req dto.OnGetKontrolPasien, kdBagian string) (res []earlywarningsystem.DKontrolPasien, err error) {
	result := lu.DB.Where(&earlywarningsystem.DKontrolPasien{
		Noreg: req.Noreg, KdBagian: kdBagian, Kategori: req.Kategori,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}
