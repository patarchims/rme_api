package usecase

import (
	earlywarningsystem "hms_api/modules/early-warning-system"
	"hms_api/modules/early-warning-system/dto"
	"hms_api/modules/early-warning-system/entity"

	"github.com/sirupsen/logrus"
)

type earlyUseCase struct {
	logging         *logrus.Logger
	earlyRepository entity.EalryRepository
	earlyMapper     entity.EarlyMapper
}

func NewHisUseCase(earlyRepository entity.EalryRepository, logging *logrus.Logger, mapper entity.EarlyMapper) entity.EarlyUseCase {
	return &earlyUseCase{
		logging:         logging,
		earlyRepository: earlyRepository,
		earlyMapper:     mapper,
	}
}

func (eu *earlyUseCase) OnSaveKontrolPasienUseCase(req dto.OnSaveKontrolPasien, userID string, kdBagian string) (message string, err error) {
	// ON SAVE DATA
	data := earlywarningsystem.DKontrolPasien{
		Noreg:        req.Noreg,
		KdBagian:     kdBagian,
		Kategori:     req.Kategori,
		UserID:       userID,
		GCSE:         req.GCSE,
		GCSV:         req.GCSV,
		GCSM:         req.GCSM,
		TDSistolik:   req.TDSistolik,
		TDDiastolik:  req.TDDiastolik,
		Nadi:         req.Nadi,
		Pernapasan:   req.Pernapasan,
		ReaksiOtot:   req.ReaksiOtot,
		Suhu:         req.Suhu,
		Spo2:         req.Spo2,
		CRT:          req.CRT,
		Obsigen:      req.Obsigen,
		ProteinUrine: req.ProteinUrine,
	}

	_, er12 := eu.earlyRepository.OnSaveKontrolPasienRepository(data)

	if er12 != nil {
		return "Data gagal disimpan", er12
	}

	return "Data berhasil disimpan", nil
}

func (eu *earlyUseCase) OnGetKontrolPasienUseCase(req dto.OnGetKontrolPasien, kdBagian string) (res []dto.ResponseKontrolPasien, err error) {
	data, er11 := eu.earlyRepository.OnGetKontrolPasienRepository(req, kdBagian)

	if er11 != nil {
		return make([]dto.ResponseKontrolPasien, 0), er11
	}

	mapper := eu.earlyMapper.ToMappingEarlyWarningSystem(data)
	return mapper, nil
}
