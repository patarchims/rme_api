package dto

import (
	"hms_api/modules/lib"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	userDTO "hms_api/modules/user/dto"
	"time"
)

type (
	ReqNoreg struct {
		Noreg string `json:"no_reg" binding:"required"`
	}
	ReqNoregV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqNoRM struct {
		Norm string `json:"no_rm" binding:"required"`
	}

	ReqInsertDetailLabor struct {
		KodeDokter                  string                             `json:"kode_dokter" binding:"required"`
		KodeKelas                   string                             `json:"kode_kelas" binding:"required"`
		UmurPasien                  string                             `json:"umur_pasien" binding:"required"`
		KetPoli                     string                             `json:"ket_poli" binding:"required"`
		KodePoli                    string                             `json:"kd_poli" binding:"required"`
		Noreg                       string                             `json:"no_reg" binding:"required"`
		DeviceID                    string                             `json:"device_id" binding:"required"`
		DokterPengirim              string                             `json:"dokter_pengirim" binding:"required"`
		DetailPemeriksaanLaborModel []soap.DetailPemeriksaanLaborModel `json:"detail_labor"`
	}

	// REQUEST INPUT DETAIL RADIOLOGI
	ReqInsertDetailRadiologi struct {
		KodeDokter     string `json:"kode_dokter" binding:"required"`
		KodeKelas      string `json:"kode_kelas" binding:"required"`
		UmurPasien     string `json:"umur_pasien" binding:"required"`
		KetPoli        string `json:"ket_poli" binding:"required"`
		KodePoli       string `json:"kd_poli" binding:"required"`
		Noreg          string `json:"no_reg" binding:"required"`
		DeviceID       string `json:"device_id" binding:"required"`
		DokterPengirim string `json:"dokter_pengirim" binding:"required"`
		JenisPenunjang string `json:"jenis_penunjang" binding:"required"`
		// DETAIL RADIOLOGI
		DetailRadiologi []lib.DetailTaripRadioLogi `json:"detail_radiologi" binding:"required"`
	}

	// RESPONSE
	ResKTaripPersediaanObat struct {
		Kodeobat      string  `json:"kode_obat"`
		Namaobat      string  `json:"nama_obat"`
		Satuan        string  `json:"satuan"`
		Saldo         float32 `json:"saldo"`
		Aso           float32 `json:"aso"`
		Principal     string  `json:"principal"`
		Komposisi     string  `json:"komposisi"`
		Jumlah        int     `json:"jumlah"`
		Dosis         string  `json:"dosis"`
		Aturan        string  `json:"aturan"`
		Prescriptio   string  `json:"prescriptio"`
		JumlahRacikan int     `json:"jumlah_racikan"`
		Flag          string  `json:"flag"`
	}

	ReqGetDResepHistory struct {
		NoRm string `json:"no_rm"`
	}

	ReqOnSaveResepManual struct {
		NoRm         string `json:"no_rm" binding:"required"`
		Pelayanan    string `json:"pelayanan" binding:"required"`
		Noreg        string `json:"noreg" binding:"required"`
		CatatanResep string `json:"resep" binding:"required"`
		Kamar        string `json:"kamar" validate:"omitempty"`
		Kasur        string `json:"kasur" validate:"omitempty"`
		Kelas        string `json:"kelas" validate:"omitempty"`
		NamaUser     string `json:"nama_user" binding:"required"`
		NamaApotik   string `json:"nama_apotik" validate:"omitempty"`
		NamaPasien   string `json:"nama_pasien" binding:"required"`
	}

	RegOnSaveResepObat struct {
		Noreg      string                    `json:"noreg" binding:"required"`
		Norm       string                    `json:"no_rm" binding:"required"`
		Catatan    string                    `json:"catatan" binding:"required"`
		Keterangan string                    `json:"keterangan" binding:"required"`
		DeviceID   string                    `json:"device_id" binding:"required"`
		NamaUser   string                    `json:"nama_user" binding:"required"`
		NamaPasien string                    `json:"nama_pasien" binding:"required"`
		Resep      []ResKTaripPersediaanObat `json:"resep" binding:"required"`
		Pelayanan  string                    `json:"pelayanan" validate:"omitempty"`
		NamaApotik string                    `json:"nama_apotik" validate:"omitempty"`
		Kamar      string                    `json:"kamar" validate:"omitempty"`
		Kasur      string                    `json:"kasur" validate:"omitempty"`
		Kelas      string                    `json:"kelas" validate:"omitempty"`
	}

	ReqKunjungan struct {
		NoRM string `json:"no_rm" binding:"required"`
	}

	ReqInstruksiMedisFarmatologi struct {
		NoRM  string `json:"no_rm" binding:"required"`
		NoReg string `json:"no_reg" binding:"required"`
	}

	ReqReportInstruksiMedisFarmakologi struct {
		Noreg string `json:"no_reg" binding:"required"`
	}

	ReqGetPemberiObatMedisFarmakologi struct {
		Noreg    string `json:"no_reg" binding:"required"`
		KodeObat string `json:"kode_obat" binding:"required"`
	}

	ReqViewDoubleCheck struct {
		Noreg    string `json:"no_reg" binding:"required"`
		KodeObat string `json:"kode_obat" binding:"required"`
	}

	ReqOnSaveFarmakologi struct {
		Noreg         string `json:"no_reg" binding:"required"`
		NamaObat      string `json:"nama_obat" binding:"required"`
		Dosis         string `json:"dosis" binding:"required"`
		Frekuensi     int    `json:"frekuensi" binding:"required"`
		CaraPemberian string `json:"cara_pemberian" binding:"required"`
		// JUMLAH OBAT DI UBAH DENGAN SISA OBAT
		JumlahObat int    `json:"jumlah_obat" binding:"omitempty"`
		SiSaObat   string `json:"sisa_obat" binding:"omitempty"`
		KodeObat   string `json:"kode_obat" binding:"required"`
	}

	ReqRiwayat struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	ResponseInstruksiMedisFarmakologi struct {
		Id            int                  `json:"id"`
		InsertDttm    time.Time            `json:"insert_dttm"`
		KdBagian      string               `gorm:"primaryKey:KdBagian" json:"kode_bagian"`
		KPelayanan    lib.KPelayanan       `gorm:"foreignKey:KdBag" json:"kd_pelayanan"`
		Noreg         string               `json:"noreg"`
		NamaObat      string               `json:"nama_obat"`
		InsertUser    string               `json:"user_id"`
		Perawat       rme.UserPerawatModel `gorm:"foreignKey:InsertUser" json:"user"`
		Dosis         string               `json:"dosis"`
		Frekuensi     int                  `json:"frekuensi"`
		JamDiberi     time.Time            `json:"jam_diberi"`
		KodeObat      string               `json:"kode_obat"`
		JumlahObat    int                  `json:"jumlah_obat"`
		CaraPemberian string               `json:"cara_pemberian"`
	}

	ResponseViewDoubleCheck struct {
		DoubleChesk []soap.DDoubleCheck                    `json:"double_check"`
		Karyawan    []userDTO.ResponseDataPemberiInformasi `json:"karyawan"`
	}
)
