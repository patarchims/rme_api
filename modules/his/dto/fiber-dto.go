package dto

import (
	"hms_api/modules/lib"
	"hms_api/modules/soap"
)

type (
	ReqInsertDetailLaborV2 struct {
		KodeDokter                  string                             `json:"kode_dokter" validate:"required"`
		KodeKelas                   string                             `json:"kode_kelas" validate:"required"`
		UmurPasien                  string                             `json:"umur_pasien" validate:"required"`
		KetPoli                     string                             `json:"ket_poli" validate:"required"`
		KodePoli                    string                             `json:"kd_poli" validate:"required"`
		Noreg                       string                             `json:"no_reg" validate:"required"`
		DeviceID                    string                             `json:"device_id" validate:"required"`
		DokterPengirim              string                             `json:"dokter_pengirim" validate:"required"`
		DetailPemeriksaanLaborModel []soap.DetailPemeriksaanLaborModel `json:"detail_labor"`
	}

	ReqInsertDetailRadiologiV2 struct {
		KodeDokter     string `json:"kode_dokter" validate:"required"`
		KodeKelas      string `json:"kode_kelas" validate:"required"`
		UmurPasien     string `json:"umur_pasien" validate:"required"`
		KetPoli        string `json:"ket_poli" validate:"required"`
		KodePoli       string `json:"kd_poli" validate:"required"`
		Noreg          string `json:"no_reg" validate:"required"`
		DeviceID       string `json:"device_id" validate:"required"`
		DokterPengirim string `json:"dokter_pengirim" validate:"required"`
		JenisPenunjang string `json:"jenis_penunjang" validate:"required"`
		// DETAIL RADIOLOGI
		DetailRadiologi []lib.DetailTaripRadioLogi `json:"detail_radiologi" validate:"required"`
	}
)
