package entity

import (
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
)

type HisMapper interface {
	ToMappingKapersediaanObat(data []his.KTaripPersediaanObat) (res []dto.ResKTaripPersediaanObat)
	ToMappingDaftarObat(data []his.InstruksiMedisFarmakologi) (res []dto.ResponseInstruksiMedisFarmakologi)
	ToMappingDataDoubleCheck(data []soap.DDoubleCheck, pemberiInformasi []user.MutiaraPengajar) (res dto.ResponseViewDoubleCheck)
}

type HisRepository interface {
	// DOUBLE CHECK
	OnGetViewDoubleCheckRepository(noReg string, bagian string, kodeObat string) (res []soap.DDoubleCheck, err error)

	OnSaveInStruksiMedisFarmakologiRepository(noReg string, bagian string, userId string, req dto.ReqOnSaveFarmakologi) (res rme.DInstruksiMedisFarmakologi, err error)
	OnGetInstruksiMedisFarmataologiRepository(noRM string, noReg string) (res []his.DApotikKeluarObat1, err error)
	OnGetPemberiInstruksiMedisFarmakologiRepository(bagian string, noReg string, kodeObat string) (res []his.InstruksiMedisFarmakologi, err error)

	OnGetReportInstruksiMedisFarmakologiRepository(bagian string, noReg string) (res []his.InstruksiMedisFarmakologi, err error)
	OnGetRiwayatKunjunganPasienRepository(noRm string) (res []his.RiwayatKunjungan, err error)
	OnGetDResepOnlineHistory(noRM string) (res []his.DResepOnlineHistory, err error)
	OnGetDResepOnlineManualHistory(noRM string) (res []his.DResepOnlineHistory, err error)

	GetHasilLabRepository(noReg string) (res []his.DLab, err error)
	GetKelompokLab(noLab string, noReg string) (res []his.DKelompokLab, err error)
	GetKelompokLabV2(noLab string, noReg string) (res []his.DKelompokLab, err error)
	FindDApotikAmbilObat1Repository(kodeObat string) (res his.DapotikAmbilObat1, err error)
	CariPerjanjianKhususRepository(noReg string) (res his.HIsPerjanjianKhusus, err error)
	CariPresentaseObatRekananRepository(kode string, kelas string) (res his.HisProsentase, err error)
	CariKInventoryObatRepository(kodeObat string) (res his.HisKInventory, err error)

	// ================= GET HASIL LABOR
	GetHasilHisoryLabV2(noLab string, noReg string, nameGrup string) (res []his.DHasilLab, err error)
	GetPenLabKelompokTabelLamaRepository(jamInput string, noReg string) (res []his.DPenmedPemeriksaan, err error)
	// GetHasilLabRepositoryV2(noReg string) (res []his.DLab, err error)
	GetHasilHisoryLab(noLab string, noReg string, nameGrup string) (res []his.DHasilLab, err error)
	GetDetailHasilPenMed(noPenmed string, noReg string) (res []his.DetailHasilPenMedn, err error)
	GetHasilPenMed(noReg string) (res []his.DHasilPenMed, err error)
	GetDetailPasien(noRM string, kdBagian string) (res []his.RiwayatDetailPasien, err error)
	GetPenLabPemeriksaanTabelLamaRepository(jamInput string, noReg string, kelompok string) (res []his.DPemeriksaanLabor, err error)

	// ========================= INSERT LABOR
	// =========================
	GetPenlabTabelLamaRepository(noReg string) (res []his.DPenLab, err error)
	GetNomorLabor(noReg string) (res his.GenerateNomor, err error)
	GetNomorRadiologi(noReg string) (res his.GenerateNomor, err error)
	InsertLabor(labor his.DHasilLaborModel) (res his.DHasilLaborModel, err error)
	InsertRadiologi(radiologi his.DHasilPenunjang) (res his.DHasilPenunjang, err error)
	InsertAntrianPenmed(antrian his.AntrianPasienModel) (res his.AntrianPasienModel, err error)

	// GET RADIOLOGI
	GetHasilGiziRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error)
	GetHasilFisioterapiRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error)
	GetHasilDPenunjangRadiologiRepository(noReg string) (res []his.DHasilRadiologi, err error)
	GetHasilDPemeriksaanFisioterapi(noReg string) (res []his.DHasilRadiologi, err error)
	GetHasilRadiologiRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error)
	GetDetailHasilRadiologiOldDB(bagian string, noReg string, jamInput string) (res []his.DHasilRadiologiOldDB, err error)
	GetKTaripPersediaanObat() (res []his.KTaripPersediaanObat, err error)
	CekApotikKeluarObatRepository(kodeObat string, noReg string) (res his.JumlahKeluarObat, err error)
	GetNomorAntrianApotikRepository() (res his.AntrianApotik, err error)
	PerjanjianKhususResepOnlineReporitory(noReg string) (res his.PerjanjianKhusus, err error)
	InsertAntrianApotikRepository(data his.AntrianResepApotik) (res his.AntrianResepApotik, err error)
	InsertOnlineHistoryRepository(data his.DResepOnlineHistory) (res his.DResepOnlineHistory, err error)
	InsertAntrianApotik2Reporitory(data his.AntrianApotik2) (res his.AntrianApotik2, err error)

	// -=====-//
	GetAllKTaripDokterRepository() (res []his.KTaripDokterModel, err error)
	CariHisKInventory2PasienRekananRepository(kodeObat string, kodePerusahaan string) (res his.HisKInventory2, err error)
	InsertTempObatKeluarObatRepository(data his.TempTempaApotikKeluarObat) (res his.TempTempaApotikKeluarObat, err error)
}

type HisUsecase interface {
	SaveResepPasienManualUseCase(req dto.ReqOnSaveResepManual, userID string, bagian string) (message string, err error)

	SaveResepPasienUseCaseV2(req dto.RegOnSaveResepObat, userID string, bagian string) (message string, err error)
	HistoryLaboratoriumUsecase(noReg string) (res []his.ResHasilLabor, err error)
	HistoryLaboratoriumUsecaseV2(noReg string) (res []his.ResHasilLaborTableLama, err error)

	HistoryRadiologiUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error)
	HistoryFisioterapiUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error)

	HistoryPenMed(noReg string) (res []his.ResHasilDetailPenMed, err error)
	HistoryPasien(noRM string, kdBagian string) (res []his.ResRiwayatDetailPasien, err error)
	InputPemeriksaanLaborUsecase(userID string, req dto.ReqInsertDetailLabor) (res his.InputLabResponse, err error)
	HistoryGiziUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error)
	// INPUT PEMERIKSAAN
	HistoryDHasilPemeriksaanFisioterapi(noReg string) (res []his.ResHasilFisioterapiMapper, err error)
	HistoryDhasilPenunjangUsecase(noReg string) (res []his.ResHasilRadiologiMapper, err error)
	InputPemeriksaanRadiologiUseCase(userID string, req dto.ReqInsertDetailRadiologi) (res his.InputLabResponse, err error)
	InputPemeriksaanLaborUsecaseV2(userID string, req dto.ReqInsertDetailLaborV2) (res his.InputLabResponse, err error)
	InputPemeriksaanRadiologiUseCaseV2(userID string, req dto.ReqInsertDetailRadiologiV2) (res his.InputLabResponse, err error)
	HistoryDHasilPemeriksaanGizi(noReg string) (res []his.ResHasilFisioterapiMapper, err error)
	SaveResepPasienUsecase(req dto.RegOnSaveResepObat, userID string, bagian string) (message string, err error)
}
