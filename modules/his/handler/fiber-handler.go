package handler

import (
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (hh *HisHandler) HistoryLaborV2(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := hh.HisUsecase.HistoryLaboratoriumUsecase(payload.Noreg)

	hh.Logging.Info("Data usecase")
	hh.Logging.Info(data)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data == nil {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// HASIL PEMERIKSAAN LABOR PADA TABLE LAMA
func (hh *HisHandler) HistoryLaborV3(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN GET HASIL LABOR PADA TABLE LAMA
	data, errs := hh.HisUsecase.HistoryLaboratoriumUsecaseV2(payload.Noreg)

	hh.Logging.Info("Data usecase")
	hh.Logging.Info(data)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data == nil {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryRadiologiOld(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN GET HASIL LABOR PADA TABLE LAMA
	data, errs := hh.HisUsecase.HistoryRadiologiUsecaseV2(payload.Noreg)

	hh.Logging.Info("Data usecase")
	hh.Logging.Info(data)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data == nil {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryFisioterapiOld(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN GET HASIL LABOR PADA TABLE LAMA
	data, errs := hh.HisUsecase.HistoryFisioterapiUsecaseV2(payload.Noreg)

	hh.Logging.Info("Data usecase")
	hh.Logging.Info(data)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data == nil {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryGiziOld(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN GET HASIL LABOR PADA TABLE LAMA
	data, errs := hh.HisUsecase.HistoryGiziUsecaseV2(payload.Noreg)

	hh.Logging.Info("Data usecase")
	hh.Logging.Info(data)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data == nil {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryDHasilPenunjang(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := hh.HisUsecase.HistoryDhasilPenunjangUsecase(payload.Noreg)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *HisHandler) HasilPemeriksaanFisioterapi(c *fiber.Ctx) error {

	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := hh.HisUsecase.HistoryDHasilPemeriksaanFisioterapi(payload.Noreg)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HasilPemeriksaanGizi(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := hh.HisUsecase.HistoryDHasilPemeriksaanGizi(payload.Noreg)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		hh.Logging.Info("OK")
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryLaborPasien(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err := hh.HisUsecase.HistoryLaboratoriumUsecase(payload.Noreg)

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, data)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(data) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, data)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *HisHandler) HistoryPenMedV2(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err := hh.HisUsecase.HistoryPenMed(payload.Noreg)

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, err.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) HistoryPasienV2(c *fiber.Ctx) error {
	payload := new(dto.ReqNoRM)

	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	modulID := c.Locals("modulID").(string)

	data, err := hh.HisUsecase.HistoryPasien(payload.Norm, modulID)

	if err != nil {
		hh.Logging.Error("Data gagal diproses")
		response := helper.APIResponseFailure("Data gagal diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		hh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) InputPemeriksaanLaborFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInsertDetailLaborV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)

	// INPUT PEMERIKSAAN
	// LABOR USECASE
	data, errs := hh.HisUsecase.InputPemeriksaanLaborUsecaseV2(userID, *payload)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse(errs.Error(), http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *HisHandler) InputPemeriksaaanRadiologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInsertDetailRadiologiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)

	// INPUT PEMERIKSAAN
	// LABOR USECASE
	data, errs := hh.HisUsecase.InputPemeriksaanRadiologiUseCaseV2(userID, *payload)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse(errs.Error(), http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	hh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) GetKtaripPersediaanObatFiberHandler(c *fiber.Ctx) error {
	data, err := hh.HisRepository.GetKTaripPersediaanObat()

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		hh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	mapper := hh.HisMapper.ToMappingKapersediaanObat(data)

	response := helper.APIResponse("Ok", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) SaveKtaripResepObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegOnSaveResepObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	hh.Logging.Info(userID)

	message, err2 := hh.HisUsecase.SaveResepPasienUsecase(*payload, userID, modulID)

	if err2 != nil {
		response := helper.APIResponse(message, http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnGetHistoryResepObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetDResepHistory)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	res, er11 := hh.HisRepository.OnGetDResepOnlineHistory(payload.NoRm)

	if er11 != nil {
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	// hh.Logging.Info(res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnGetHistoryResepManualFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetDResepHistory)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	res, er11 := hh.HisRepository.OnGetDResepOnlineManualHistory(payload.NoRm)

	hh.Logging.Info(res)

	if er11 != nil {
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnSaveResepManualFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveResepManual)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	hh.Logging.Info(userID)

	message, err2 := hh.HisUsecase.SaveResepPasienManualUseCase(*payload, userID, modulID)

	if err2 != nil {
		response := helper.APIResponse(message, http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	hh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) SaveResepObatV2FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegOnSaveResepObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	hh.Logging.Info(userID)

	message, err2 := hh.HisUsecase.SaveResepPasienUseCaseV2(*payload, userID, modulID)

	hh.Logging.Info(message)

	if err2 != nil {
		response := helper.APIResponse(message, http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) SaveResepManualObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegOnSaveResepObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	// modulID := c.Locals("modulID").(string)
	hh.Logging.Info(userID)
	return nil
}

func (hh *HisHandler) RiwayatKunjunganFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqKunjungan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	riwayat, err2 := hh.HisRepository.OnGetRiwayatKunjunganPasienRepository(payload.NoRM)

	hh.Logging.Info("RIWAYAT KUNJUNGAN")
	hh.Logging.Info(riwayat)

	if err2 != nil {
		response := helper.APIResponse(err2.Error(), http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(riwayat) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, make([]his.RiwayatKunjungan, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, riwayat)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) RiwayaatPerawatanPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	riwayat, er12 := hh.RMERepository.GetHistoryRiwayatPenyakitRepository(payload.NoReg)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, riwayat)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) ReportPengkajianRawatInapAnakFiberHandler(c *fiber.Ctx) error {
	return nil
}

func (hh *HisHandler) InStruksiMedisFarmatologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInstruksiMedisFarmatologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	riwayat, err2 := hh.HisRepository.OnGetInstruksiMedisFarmataologiRepository(payload.NoRM, payload.NoReg)

	if err2 != nil {
		response := helper.APIResponse(err2.Error(), http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(riwayat) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, make([]his.DApotikKeluarObat1, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, riwayat)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *HisHandler) OnGetReportInstruksiMedisFarmakologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportInstruksiMedisFarmakologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	data, er12 := hh.HisRepository.OnGetReportInstruksiMedisFarmakologiRepository(modulID, payload.Noreg)

	hh.Logging.Info("DATA ")
	hh.Logging.Info(data)

	if er12 != nil {
		response := helper.APIResponse(er12.Error(), http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, make([]his.InstruksiMedisFarmakologi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	mapper := hh.HisMapper.ToMappingDaftarObat(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnViewObatDoubleCheckFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqViewDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, _ := hh.HisRepository.OnGetViewDoubleCheckRepository(payload.Noreg, modulID, payload.KodeObat)
	pengawai, _ := hh.UserRepository.OnGetAllMutiaraPengajarRepository()

	mapper := hh.HisMapper.ToMappingDataDoubleCheck(data, pengawai)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnGetPemberiInstruksiFarmakologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemberiObatMedisFarmakologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	data, er12 := hh.HisRepository.OnGetPemberiInstruksiMedisFarmakologiRepository(modulID, payload.Noreg, payload.KodeObat)

	hh.Logging.Info("DATA ")
	hh.Logging.Info(data)

	if er12 != nil {
		response := helper.APIResponse(er12.Error(), http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, make([]his.InstruksiMedisFarmakologi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *HisHandler) OnSaveInstruksiMedisFarmakologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveFarmakologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	hh.Logging.Info(payload)

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	_, er12 := hh.HisRepository.OnSaveInStruksiMedisFarmakologiRepository(payload.Noreg, modulID, userID, *payload)

	if er12 != nil {
		response := helper.APIResponse("Data gagal disimpan", http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}
