package handler

import (
	"hms_api/modules/his/dto"
	"hms_api/modules/his/entity"
	rmeEntity "hms_api/modules/rme/entity"
	userEntity "hms_api/modules/user/entity"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type HisHandler struct {
	HisRepository  entity.HisRepository
	HisUsecase     entity.HisUsecase
	HisMapper      entity.HisMapper
	Logging        *logrus.Logger
	RMERepository  rmeEntity.RMERepository
	UserRepository userEntity.UserRepository
}

func (hh *HisHandler) HistoryLabor(c *gin.Context) {
	payload := new(dto.ReqNoreg)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		hh.Logging.Error(errorMessage)
		response := helper.APIResponse("History Labor", http.StatusCreated, errorMessage)
		c.JSON(http.StatusCreated, response)
		return
	}

	data, errs := hh.HisUsecase.HistoryLaboratoriumUsecase(payload.Noreg)

	if errs != nil {
		hh.Logging.Error(errs.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (hh *HisHandler) InputPemeriksaanLabor(c *gin.Context) {
	payload := new(dto.ReqInsertDetailLabor)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		hh.Logging.Error(errorMessage)
		response := helper.APIResponse("Error", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}
	userID := c.MustGet("userID").(string)

	data, err := hh.HisUsecase.InputPemeriksaanLaborUsecase(userID, *payload)

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponse(err.Error(), http.StatusCreated, err.Error())
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	hh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (hh *HisHandler) InputPemeriksaaanRadiologi(c *gin.Context) {
	payload := new(dto.ReqInsertDetailRadiologi)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		hh.Logging.Error(errorMessage)
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errorMessage)
		c.JSON(http.StatusCreated, response)
		return
	}

	userID := c.MustGet("userID").(string)

	data, err := hh.HisUsecase.InputPemeriksaanRadiologiUseCase(userID, *payload)

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponse(err.Error(), http.StatusCreated, err.Error())
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	hh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (hh *HisHandler) InputPemeriksaanFisioterapi(c *gin.Context) {
	payload := new(dto.ReqInsertDetailRadiologi)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		hh.Logging.Error(errorMessage)
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errorMessage)
		c.JSON(http.StatusCreated, response)
		return
	}
}

func (hh *HisHandler) HistoryPenMed(c *gin.Context) {
	payload := new(dto.ReqNoreg)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Detail pasien", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	data, err := hh.HisUsecase.HistoryPenMed(payload.Noreg)

	if err != nil {
		hh.Logging.Error(err.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, err.Error())
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	hh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (hh *HisHandler) HistoryPasien(c *gin.Context) {
	payload := new(dto.ReqNoRM)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Detail pasien", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	modulID := c.MustGet("modulID").(string)

	data, err := hh.HisUsecase.HistoryPasien(payload.Norm, modulID)

	if err != nil {
		hh.Logging.Error("Data gagal diproses")
		response := helper.APIResponseFailure("Data gagal diproses", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(data) < 1 {
		hh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}
