package his

import (
	"hms_api/modules/lib"
	"hms_api/modules/rme"
	"time"
)

type (
	InputLabResponse struct {
		NoLab string `json:"no_lab"`
	}

	DHasilLabor struct {
		NoLab      string         `json:"no_lab"`
		KodeBagian string         `gorm:"primaryKey:KodeBagian" json:"kode_bagian"`
		KPelayanan lib.KPelayanan `gorm:"foreignKey:KdBag" json:"kd_pelayanan"`
		Noreg      string         `json:"no_reg"`
		Kelompok   string         `json:"kelompok"`
		NameGrup   string         `json:"name_grup"`
		Hasil      string         `json:"hasil"`
		Satuan     string         `json:"satuan"`
		Catatan    string         `json:"catatan"`
	}

	DLab struct {
		NoLab            string
		KdDrPengirim     string
		KetAsalPelayanan string
		Namadokter       string
	}

	DPenLab struct {
		Jaminput string
		Noreg    string
		Id       string
	}

	DHasilRadiologiV2 struct {
		Jaminput string
		Bagian   string
	}

	DPenmedPemeriksaan struct {
		NamaKelompok string
	}

	DHasilRadiologiOldDB struct {
		Pemeriksaan string `json:"pemeriksaan_deskripsi"`
		Uraian      string `json:"uraian"`
		Hasil       string `json:"hasil"`
	}

	DPemeriksaanLabor struct {
		Pemeriksaan string `json:"pemeriksaan_deskripsi"`
		Normal      string `json:"normal"`
		Satuan      string `json:"satuan"`
		Hasil       string `json:"hasil"`
	}

	DKelompokLab struct {
		Kel      string
		NameGrup string
	}

	DHasilLab struct {
		Kode      string `json:"kode"`
		Hasil     string `json:"hasil"`
		Satuan    string `json:"satuan"`
		Normal    string `json:"normal"`
		Deskripsi string `json:"deskripsi"`
	}

	DetailHasilPenMedn struct {
		Deskripsi string `json:"deskripsi"`
		Uraian    string `json:"uraian"`
		Hasil     string `json:"hasil"`
		Catatan   string `json:"catatan"`
	}

	DHasilPenMed struct {
		NoPenmed         string
		KdDokterPengirim string
		KetAsalPelayanan string
		Bagian           string
		Namadokter       string
	}

	ResHasilLabor struct {
		Dpjp           string      `json:"dpjp"`
		AsalPpelayanan string      `json:"asal_pelayanan"`
		NomorLab       string      `json:"nomor_lab"`
		Kelompok       string      `json:"kelompok"`
		Lab            []DHasilLab `json:"lab"`
	}

	ResHasilLaborTableLama struct {
		Tanggal           string              `json:"tanggal"`
		NamaKelompok      string              `json:"nama_kelompok"`
		DPemeriksaanLabor []DPemeriksaanLabor `json:"penlab"`
	}

	RegHasilRadiologiTabelLama struct {
		Tanggal              string                 `json:"tanggal"`
		NamaKelompok         string                 `json:"nama_kelompok"`
		DHasilRadiologiOldDB []DHasilRadiologiOldDB `json:"radiologi"`
	}

	ResHasilRadiologiMapper struct {
		Judul  string `json:"judul"`
		Hasil  string `json:"hasil"`
		Uraian string `json:"uraian"`
	}

	ResHasilFisioterapiMapper struct {
		Hasil  string `json:"hasil"`
		Uraian string `json:"uraian"`
	}

	ResHasilDetailPenMed struct {
		NoPenmed           string               `json:"no_penmed"`
		KdDokterPengirim   string               `json:"kd_dokter"`
		KetAsalPelayanan   string               `json:"ket_pelayanan"`
		Bagian             string               `json:"bagian"`
		Namadokter         string               `json:"nama_dokter"`
		DetailHasilPenMedn []DetailHasilPenMedn `json:"detail_penmed"`
	}

	RiwayatDetailPasien struct {
		TglMasuk               string `json:"tanggal"`
		Diagnosa               string `json:"diagnosa"`
		KeluhanUtama           string `json:"keluhan_utama"`
		RwtSekarang            string `json:"riwayat_sekarang"`
		AsesmedRwtPenyKlrg     string `json:"riwayat_penyakit_keluarga"`
		AsesmedRwtAlergiDetail string `json:"riwayat_alergi_detail"`
		AsesmedTerapi          string `json:"terapi"`
		Noreg                  string `json:"noreg"`
		Id                     string `json:"no_rm"`
		DeskDiagPrimer         string `json:"diag_primer"`
		Namadokter             string `json:"nama_dokter"`
	}

	GenerateNomor struct {
		Nomor string `json:"nomor"`
	}

	ResRiwayatDetailPasien struct {
		TglMasuk               string                 `json:"tanggal"`
		Diagnosa               string                 `json:"diagnosa"`
		KeluhanUtama           string                 `json:"keluhan_utama"`
		RwtSekarang            string                 `json:"riwayat_sekarang"`
		AsesmedRwtPenyKlrg     string                 `json:"riwayat_penyakit_keluarga"`
		AsesmedRwtAlergiDetail string                 `json:"riwayat_alergi_detail"`
		AsesmedTerapi          string                 `json:"terapi"`
		Noreg                  string                 `json:"noreg"`
		Id                     string                 `json:"no_rm"`
		ResHasilDetailPenMed   []ResHasilDetailPenMed `json:"detail_penmed"`
		ResHasilLabor          []ResHasilLabor        `json:"detail_labor"`
	}

	// INSERT INTO dhasil_laboratorium(tipe_pemeriksaan,insert_dttm,insert_user_id,insert_pc,no_lab,kd_dr_pengirim,jam_pengambilan_spesimen,jam_kirim_spesimen,kode_bagian,kode_kelas,noreg,umur_pasien,kelompok,name_grup,urut,kode,satuan,normal,ket_asal_pelayanan)
	// VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
	// ENUM TYPE PEMERIKSAAN
	TipePemeriksaan string

	DHasilLaborModel struct {
		//======================
		// AMBIL DARI DEVICE TABLET
		// Kode Dokter Pengirim
		// JAM PENGAMBILAN
		TransactionId          float64         `gorm:"primaryKey:transaction_id" json:"transaction_id"`
		TipePemeriksaan        TipePemeriksaan `json:"tipe_pemeriksaan"`
		InsertDttm             string          `json:"insert_dttm"`
		InsertUserId           string          `json:"user_id"`
		InsertPc               string          `json:"insert_pc"`
		NoLab                  string          `json:"no_lab"`
		KdDrPengirim           string          `json:"kd_pengirim"`
		JamPengambilanSpesimen string          `json:"spesimen"`
		JamKirimSpesimen       string          `json:"jam_spesimen"`
		KodeBagian             string          `json:"bagian"`
		KodeKelas              string          `json:"kode_kelas"`
		Noreg                  string          `json:"noreg"`
		UmurPasien             string          `json:"umur_pasien"`
		Kelompok               string          `json:"kelompok"`
		NameGrup               string          `json:"name_grup"`
		Urut                   int             `json:"urut"`
		Kode                   string          `json:"kode"`
		Satuan                 string          `json:"satuan"`
		Normal                 string          `json:"normal"`
		KetAsalPelayanan       string          `json:"asal_pelayanan"`
	}

	// DhasilPenunjang
	DHasilPenunjang struct {
		TipePemeriksaan  TipePemeriksaan `json:"tipe_pemeriksaan"`
		InsertDttm       string          `json:"insert_dttm"`
		InsertUserId     string          `json:"user_id"`
		NoPenmed         string          `json:"no_penmed"`
		KdDokterPengirim string          `json:"kd_dokter_pengirim"`
		KodeBagian       string          `json:"kode_bagian"`
		KodeKelas        string          `json:"kode_kelas"`
		KodePenunjang    string          `json:"kode_penunjang"`
		Noreg            string          `json:"noreg"`
		UmurPasien       string          `json:"umur_pasien"`
		Kode             string          `json:"kode"`
		KetAsalPelayanan string          `json:"ket_asal_pelayanan"`
		Uraian           string          `json:"uraian"`
		Hasil            string          `json:"hasil"`
		Catatan          string          `json:"catatan"`
	}

	DHasilRadiologi struct {
		Noreg         string `json:"noreg"`
		KodePenunjang string `json:"kode_penunjang"`
		Hasil         string `json:"hasil"`
		Uraian        string `json:"uraian"`
	}

	// INSERT ANTRIAN
	// antrian_pasien(mac,user_id,recieve_dttm,noreg,kd_tujuan,kd_dokter,no_order,code_from) values(?,?,?,?,?,?,?,?)
	AntrianPasienModel struct {
		Mac         string
		UserId      string
		RecieveDttm string
		Noreg       string
		KdTujuan    string
		KdDokter    string
		NoOrder     string
		CodeFrom    string
	}

	KTaripPersediaanObat struct {
		Kodeobat  string  `json:"kode_obat"`
		Namaobat  string  `json:"nama_obat"`
		Satuan    string  `json:"satuan"`
		Saldo     float32 `json:"saldo"`
		Aso       float32 `json:"aso"`
		Principal string  `json:"principal"`
		Komposisi string  `json:"komposisi"`
	}

	JumlahKeluarObat struct {
		Jumlah int
	}

	AntrianApotik struct {
		NoAntri string
	}

	PerjanjianKhusus struct {
		Noreg          string
		Kodeperusahaan string
		Perusahaan     string
	}

	DapotikKeluarObat struct {
		Pc           string
		User         string
		Jam          string
		Nama         string
		Dokter       string
		Statuspasien string
		Perusahaan   string
		Id           string
		Noreg        string
		Tglinput     string
		Tglkeluar    string
		Nokeluar     string
		Total        int
		Biayalain    int
		Ket          string
		Kelas        string
		Nilai        int
		Resep        string
		Bpjs         string
		Kodedr       string
	}

	AntrianResepApotik struct {
		Nama           string
		Nomor          string
		Jenis          string
		Resep          string
		Jaminput       string
		Kodeperusahaan string
		Kodedokter     string
		Id             string
		Noreg          string
		Kodeapotik     string
		Namaapotik     string
		Kodeasal       string
		Kamar          string
		Kasur          string
		Kelas          string
		Ket            string
		Tgldaftar      string
		Notif          string
	}

	DResepOnlineHistory struct {
		Num        int    `json:"num"`
		JenisResep string `json:"jenis_resep"`
		Catatan    string `json:"catatan"`
		Signa      string `json:"signa"`
		Nomor      string `json:"nomor"`
		KdDokter   string `json:"kd_dokter"`
		NmDokter   string `json:"nm_dokter"`
		Bagian     string `json:"bagian"`
		Tglinput   string `json:"tgl_input"`
		Id         string `json:"id"`
		Noreg      string `json:"noreg"`
		Kodeobat   string `json:"kode_obat"`
		Namaobat   string `json:"nama_obat"`
		Satuan     string `json:"satuan"`
		Jumlah     string `json:"jumlah"`
		Dosis      string `json:"dosis"`
		Cara       string `json:"cara"`
		Racik      string `json:"racik"`
		Alergi     string `json:"alergi"`
	}

	AntrianApotik2 struct {
		Noreg   string
		Jam     string
		NoAntri string
		Nomor   string
		Notif   string
	}

	// ================== KTARIP DOKTER
	KTaripDokterModel struct {
		Iddokter     string `gorm:"primaryKey;column:iddokter" json:"id_dokter"`
		Namadokter   string
		Alamat       string
		Jeniskelamin string
		Pendidikan   string
		Statusdokter string
		Spesialisasi string
	}

	// temp.tempapotikkeluarobat
	TempTempaApotikKeluarObat struct {
		Tanggung   string
		Nobatch    string
		Id         string
		Noreg      string
		Bagian     string
		Ket        string
		Tglinput   string
		Nokeluar   string
		Noambil    string
		Nomasuk    string
		Kodeobat   string
		Namaobat   string
		Satuan     string
		Hargajual  float64
		Hargabeli  float64
		Kadaluarsa string
		Saldo      float64
		Jumlah     float64
		R          float64
		Diskon     float64
		Total      float64
	}

	// his.dapotikambilobat1
	DapotikAmbilObat1 struct {
		Kunci      string
		Nobatch    string
		Gen        string
		Supplier   string
		Nomasuk    string
		Nokeluar   string
		Noambil    string
		Kodeobat   string
		Namaobat   string
		Kadaluarsa string
		Satuan     string
		Saldo      float64
		Pemakaian  float64
		Hargajual  float64
		Hargabeli  float64
		Jumlah     float32
	}

	// PERJANJIAN KHUSUS
	HIsPerjanjianKhusus struct {
		Noreg          string
		Kodeperusahaan string
		Perusahaan     string
	}

	// PRESENTASE OBAT
	HisProsentase struct {
		Kodeperusahaan string
		Kelas          string
		Nilai          float64
		Perusahaan     string
	}

	// KINVENTORY
	HisKInventory struct {
		Kodeobat  string
		Namaobat  string
		Hargajual float64
		Komposisi string
	}

	HisKInventory2 struct {
		Kodeperusahaan string
		Kodeobat       string
		Hargajual      float64
	}

	RiwayatKunjungan struct {
		Tanggal    string `json:"tanggal"`
		Noreg      string `json:"noreg"`
		Keterangan string `json:"keterangan"`
		Kunjungan  string `json:"kunjungan"`
		Id         string `json:"id"`
	}

	DApotikKeluarObat1 struct {
		No       int    `json:"no"`
		Nobatch  string `json:"no_batch"`
		Id       string `json:"id"`
		Noreg    string `json:"no_reg"`
		Bagian   string `json:"bagian"`
		Kodeobat string `json:"kode_obat"`
		Namaobat string `json:"nama_obat"`
		Satuan   string `json:"satuan"`
		Jumlah   int    `json:"jumlah"`
	}

	// FARMAKOLOGI
	InstruksiMedisFarmakologi struct {
		Id            int                  `json:"id"`
		InsertDttm    time.Time            `json:"insert_dttm"`
		KdBagian      string               `gorm:"primaryKey:KdBagian" json:"kode_bagian"`
		KPelayanan    lib.KPelayanan       `gorm:"foreignKey:KdBag" json:"kd_pelayanan"`
		Noreg         string               `json:"noreg"`
		NamaObat      string               `json:"nama_obat"`
		InsertUser    string               `json:"user_id"`
		Perawat       rme.UserPerawatModel `gorm:"foreignKey:InsertUser" json:"user"`
		Dosis         string               `json:"dosis"`
		Frekuensi     int                  `json:"frekuensi"`
		KodeObat      string               `json:"kode_obat"`
		JumlahObat    int                  `json:"jumlah_obat"`
		CaraPemberian string               `json:"cara_pemberian"`
	}
)

const (
	NonCito TipePemeriksaan = "non cito"
	Cito    TipePemeriksaan = "cito"
)

func (DHasilLabor) TableName() string {
	return "vicore_his.dhasil_laboratorium"
}

func (InstruksiMedisFarmakologi) TableName() string {
	return "vicore_rme.dinstruksi_medis_farmakologi"
}

func (DApotikKeluarObat1) TableName() string {
	return "his.dapotikkeluarobat1"
}

func (HisKInventory2) TableName() string {
	return "his.kinventory2"
}

func (HisKInventory) TableName() string {
	return "his.kinventory"
}

func (HisProsentase) TableName() string {
	return "his.prosentase"
}

func (HIsPerjanjianKhusus) TableName() string {
	return "his.perjanjiankhusus"
}

// tempapotikkeluarobat
func (TempTempaApotikKeluarObat) TableName() string {
	return "temp.tempapotikkeluarobat"
}

func (DapotikAmbilObat1) TableName() string {
	return "his.dapotikambilobat1"
}

func (KTaripDokterModel) TableName() string {
	return "his.ktaripdokter"
}

func (AntrianResepApotik) TableName() string {
	return "his.antrianapotik"
}
func (AntrianApotik2) TableName() string {
	return "his.antrianapotik2"
}

func (DResepOnlineHistory) TableName() string {
	return "posfar.dresep_online_history"
}

func (DHasilPenunjang) TableName() string {
	return "vicore_his.dhasil_penunjang"
}

func (AntrianPasienModel) TableName() string {
	return "vicore_tmp.antrian_pasien"
}

func (DHasilLaborModel) TableName() string {
	return "vicore_his.dhasil_laboratorium"
}
