package mapper

import (
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	"hms_api/modules/his/entity"
	"hms_api/modules/soap"
	"hms_api/modules/user"
	userEntyty "hms_api/modules/user/entity"
	"time"
)

type HisMapperImpl struct {
	userMapper userEntyty.IUserMapper
}

func NewHisMapperImple(userMapper userEntyty.IUserMapper) entity.HisMapper {
	return &HisMapperImpl{
		userMapper: userMapper,
	}
}

func (a *HisMapperImpl) ToMappingKapersediaanObat(data []his.KTaripPersediaanObat) (res []dto.ResKTaripPersediaanObat) {
	for _, V := range data {
		res = append(res, dto.ResKTaripPersediaanObat{
			Kodeobat:    V.Kodeobat,
			Namaobat:    V.Namaobat,
			Satuan:      V.Satuan,
			Saldo:       V.Saldo,
			Aso:         V.Aso,
			Principal:   V.Principal,
			Komposisi:   V.Komposisi,
			Jumlah:      0,
			Dosis:       "",
			Aturan:      "",
			Prescriptio: "",
			Flag:        "",
		})
	}

	return res
}

func (s *HisMapperImpl) ToMappingDaftarObat(data []his.InstruksiMedisFarmakologi) (res []dto.ResponseInstruksiMedisFarmakologi) {
	for _, V := range data {
		res = append(res, dto.ResponseInstruksiMedisFarmakologi{
			Id:            V.Id,
			InsertDttm:    V.InsertDttm,
			KdBagian:      V.KdBagian,
			KPelayanan:    V.KPelayanan,
			Noreg:         V.Noreg,
			NamaObat:      V.NamaObat,
			InsertUser:    V.InsertUser,
			Perawat:       V.Perawat,
			Dosis:         V.Dosis,
			Frekuensi:     V.Frekuensi,
			JamDiberi:     V.InsertDttm.Add(time.Duration(V.Frekuensi) * time.Hour),
			KodeObat:      V.KodeObat,
			JumlahObat:    V.JumlahObat,
			CaraPemberian: V.CaraPemberian,
		})
	}

	return res
}

// MAMPPING DATA
func (s *HisMapperImpl) ToMappingDataDoubleCheck(data []soap.DDoubleCheck, pemberiInformasi []user.MutiaraPengajar) (res dto.ResponseViewDoubleCheck) {
	var double = []soap.DDoubleCheck{}

	if len(data) > 0 {
		double = data
	} else {
		double = make([]soap.DDoubleCheck, 0)
	}

	return dto.ResponseViewDoubleCheck{
		DoubleChesk: double,
		Karyawan:    s.userMapper.ToMappingResponseDataPengajar(pemberiInformasi),
	}
}
