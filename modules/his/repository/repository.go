package respository

import (
	"errors"
	"fmt"
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	"hms_api/modules/his/entity"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type hisRepository struct {
	DB      *gorm.DB
	Logging *logrus.Logger
}

func NewHisRepository(db *gorm.DB, logging *logrus.Logger) entity.HisRepository {
	return &hisRepository{
		DB:      db,
		Logging: logging,
	}
}

func (lu *hisRepository) GetHasilLabRepository(noReg string) (res []his.DLab, err error) {
	query := `SELECT DISTINCT(dhl.no_lab),dhl.kd_dr_pengirim,dhl.ket_asal_pelayanan,ke.nama AS namadokter FROM vicore_his.dhasil_laboratorium AS dhl LEFT JOIN vicore_hrd.kemployee AS ke ON dhl.kd_dr_pengirim=ke.idk WHERE noreg=? ORDER BY no_lab DESC`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// Get Hasil Radiologi Old Table
func (lu *hisRepository) GetHasilRadiologiRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error) {
	query := `SELECT DISTINCT(jaminput), bagian FROM his.dpenmlablain WHERE noreg=? AND (bagian='Radiologi' OR bagian='Citiscan' OR bagian='Usg' OR bagian='Ekg')`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) GetHasilFisioterapiRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error) {
	query := `SELECT DISTINCT(jaminput), bagian FROM his.dpenmlablain WHERE noreg=? AND bagian='Fisiotherapy'`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) GetHasilGiziRepositoryV2(noReg string) (res []his.DHasilRadiologiV2, err error) {
	query := `SELECT DISTINCT(jaminput), bagian FROM his.dpenmlablain WHERE noreg=? AND bagian='Gizi'`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

// GET DETAIL HASIL RADIOLOGI
func (lu *hisRepository) GetDetailHasilRadiologiOldDB(bagian string, noReg string, jamInput string) (res []his.DHasilRadiologiOldDB, err error) {
	query := `SELECT pemeriksaan,uraian,hasil FROM his.dpenmlablain WHERE bagian=? AND noreg=? AND jaminput=?`

	result := lu.DB.Raw(query, bagian, noReg, jamInput).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) GetPenlabTabelLamaRepository(noReg string) (res []his.DPenLab, err error) {
	query := `SELECT DISTINCT(jaminput), noreg, id FROM his.dpenmlab WHERE noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) GetPenLabKelompokTabelLamaRepository(jamInput string, noReg string) (res []his.DPenmedPemeriksaan, err error) {

	query := `SELECT DISTINCT(kelompok) AS nama_kelompok FROM his.dpenmlab WHERE jaminput=? AND noreg=?;`

	result := lu.DB.Raw(query, jamInput, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

func (lu *hisRepository) GetPenLabPemeriksaanTabelLamaRepository(jamInput string, noReg string, kelompok string) (res []his.DPemeriksaanLabor, err error) {

	query := `SELECT dl.pemeriksaan, dl.normal, dl.satuan, dl.hasil FROM his.dpenmlab AS dl LEFT JOIN his.kpemeriksaanlaborat AS kp ON dl.pemeriksaan=kp.pemeriksaan WHERE dl.noreg=? AND dl.jaminput=? AND dl.kelompok=? ORDER BY kp.urut`

	result := lu.DB.Raw(query, noReg, jamInput, kelompok).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

func (lu *hisRepository) GetHasilDPenunjangRadiologiRepository(noReg string) (res []his.DHasilRadiologi, err error) {

	query := `SELECT noreg, kode_penunjang, hasil, uraian FROM vicore_his.dhasil_penunjang WHERE noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) GetHasilDPemeriksaanFisioterapi(noReg string) (res []his.DHasilRadiologi, err error) {

	query := `SELECT noreg, kode_penunjang, hasil, uraian FROM vicore_his.dhasil_penunjang WHERE noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// AMBIL DATA DARI DATABASE BARU
func (lu *hisRepository) GetKelompokLab(noLab string, noReg string) (res []his.DKelompokLab, err error) {
	query := `SELECT DISTINCT(kelompok) AS kel,name_grup FROM vicore_his.dhasil_laboratorium  
	WHERE no_lab=? AND noreg=? ORDER BY kelompok ASC`

	result := lu.DB.Raw(query, noLab, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// AMBIL DATA DARI DATABASE LAMA
func (lu *hisRepository) GetKelompokLabV2(noLab string, noReg string) (res []his.DKelompokLab, err error) {
	query := `SELECT DISTINCT(kelompok) AS kel,name_grup FROM his.dpenmlab
	WHERE no_lab=? AND noreg=? ORDER BY kelompok ASC`

	result := lu.DB.Raw(query, noLab, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) GetHasilHisoryLab(noLab string, noReg string, nameGrup string) (res []his.DHasilLab, err error) {
	query := `SELECT vicore_his.dhasil_laboratorium.kode,vicore_his.dhasil_laboratorium.hasil,vicore_his.dhasil_laboratorium.satuan,vicore_his.dhasil_laboratorium.normal, vicore_lib.kprocedure.deskripsi FROM vicore_his.dhasil_laboratorium  LEFT JOIN vicore_lib.kprocedure ON vicore_his.dhasil_laboratorium.kode=vicore_lib.kprocedure.kode AND vicore_lib.kprocedure.kd_bag='LAB001' WHERE vicore_his.dhasil_laboratorium.no_lab=? AND vicore_his.dhasil_laboratorium.noreg=? AND vicore_his.dhasil_laboratorium.name_grup=? ORDER BY vicore_his.dhasil_laboratorium.urut ASC`

	result := lu.DB.Raw(query, noLab, noReg, nameGrup).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// AMBIL DATA DARI HASI LABOR YANG BARU
func (lu *hisRepository) GetHasilHisoryLabV2(noLab string, noReg string, nameGrup string) (res []his.DHasilLab, err error) {
	query := `SELECT his.dpenmlab.kode,his.dpenmlab.hasil,his.dpenmlab.satuan,his.dpenmlab.normal, vicore_lib.kprocedure.deskripsi FROM his.dpenmlab  LEFT JOIN vicore_lib.kprocedure ON his.dpenmlab.kode=vicore_lib.kprocedure.kode AND vicore_lib.kprocedure.kd_bag='LAB001' WHERE his.dpenmlab.no_lab=? AND his.dpenmlab.noreg=? AND his.dpenmlab.name_grup=? ORDER BY his.dpenmlab.urut ASC`

	result := lu.DB.Raw(query, noLab, noReg, nameGrup).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) GetHasilPenMed(noReg string) (res []his.DHasilPenMed, err error) {
	query := `SELECT DISTINCT(dhl.no_penmed),dhl.kd_dokter_pengirim,dhl.ket_asal_pelayanan,kp.bagian,ke.nama AS namadokter FROM vicore_his.dhasil_penunjang AS dhl LEFT JOIN vicore_hrd.kemployee AS ke ON dhl.kd_dokter_pengirim=ke.idk LEFT JOIN vicore_lib.kpelayanan AS kp ON dhl.kode_penunjang=kp.kd_bag WHERE noreg=? ORDER BY no_penmed DESC`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) GetDetailHasilPenMed(noPenmed string, noReg string) (res []his.DetailHasilPenMedn, err error) {
	query := `SELECT kp.deskripsi,dp.uraian,dp.hasil,dp.catatan FROM vicore_his.dhasil_penunjang AS dp 
	LEFT JOIN vicore_lib.kprocedure AS kp ON dp.kode=kp.kode WHERE dp.no_penmed=? AND dp.noreg=?
	ORDER BY dp.no_penmed ASC`

	result := lu.DB.Raw(query, noPenmed, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) GetDetailPasien(noRM string, kdBagian string) (res []his.RiwayatDetailPasien, err error) {
	query := `SELECT dsp.tgl_masuk, dsp.asesmed_dokter_ttd_jam, dsp.asesmed_keluh_utama AS keluhan_utama, dsp.asesmed_rwyt_skrg AS rwt_skrg, dsp.asesmed_rwyt_peny_klrg, dsp.asesmed_rwyt_alergi_detail, dsp.asesmed_diagP, dsp.asesmed_terapi, dsp.noreg, dp.id, icd.description AS desk_diag_primer, ke2.nama AS namadokter FROM vicore_rme.dcppt_soap_pasien AS dsp LEFT JOIN vicore_his.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN vicore_his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN vicore_hrd.kemployee AS ke2 ON dsp.asesmed_dokter_ttd=ke2.idk LEFT JOIN vicore_lib.k_icd10 AS icd ON dsp.asesmed_diagP=icd.code2 WHERE dp.id=? AND dsp.kd_bagian=? ORDER BY dsp.insert_dttm DESC LIMIT 30`

	result := lu.DB.Raw(query, noRM, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// GET NOMOR LABOR
func (lu *hisRepository) GetNomorLabor(noReg string) (res his.GenerateNomor, err error) {
	query := `SELECT COALESCE(LPAD(CONVERT(SUBSTRING(@last_no_lab :=MAX(no_lab),18,3),SIGNED INTEGER)+1,3,0),'001') AS nomor FROM vicore_his.dhasil_laboratorium WHERE noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

// GENERATE NOMOR RADIOLOGI
func (lu *hisRepository) GetNomorRadiologi(noReg string) (res his.GenerateNomor, err error) {
	query := `SELECT COALESCE(LPAD(CONVERT(SUBSTRING(@last_no_penmed :=MAX(no_penmed),18,3),SIGNED INTEGER)+1,3,0),'001') AS nomor FROM vicore_his.dhasil_penunjang WHERE noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

func (lu *hisRepository) InsertLabor(labor his.DHasilLaborModel) (res his.DHasilLaborModel, err error) {

	result := lu.DB.Create(&labor)

	if result.Error != nil {
		return labor, result.Error
	}
	return labor, nil
}

func (lu *hisRepository) InsertAntrianPenmed(antrian his.AntrianPasienModel) (res his.AntrianPasienModel, err error) {

	result := lu.DB.Create(&antrian)

	if result.Error != nil {
		return antrian, result.Error
	}
	return antrian, nil
}

func (lu *hisRepository) InsertRadiologi(radiologi his.DHasilPenunjang) (res his.DHasilPenunjang, err error) {
	result := lu.DB.Create(&radiologi)

	if result.Error != nil {
		return radiologi, result.Error
	}

	return radiologi, nil
}

func (lu *hisRepository) GetKTaripPersediaanObat() (res []his.KTaripPersediaanObat, err error) {
	query := `SELECT his.kapotikpersediaan.kodeobat, his.kapotikpersediaan.namaobat, his.kapotikpersediaan.satuan, his.kapotikpersediaan.saldo, 
	his.kapotikpersediaan.aso, his.kinventory.principal,his.kinventory.komposisi FROM his.kapotikpersediaan	LEFT JOIN his.kinventory ON his.kapotikpersediaan.kodeobat=his.kinventory.kodeobat 	WHERE his.kapotikpersediaan.saldo>0`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

func (lu *hisRepository) CekApotikKeluarObatRepository(kodeObat string, noReg string) (res his.JumlahKeluarObat, err error) {
	query := `SELECT SUM(jumlah) AS jumlah FROM his.dapotikkeluarobat1 WHERE kodeobat=? AND noreg=?`

	result := lu.DB.Raw(query, kodeObat, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) GetNomorAntrianApotikRepository() (res his.AntrianApotik, err error) {
	query := `select no_antri from his.antrianapotik2 order by no_antri DESC LIMIT 1`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}
func (lu *hisRepository) InsertPostfarDResepManualHistory(bagian string, tglInput string, id string, noReg string, kdDokter string, nmDokter string, catatan string, jenisResep string, alergi string) (res his.AntrianApotik, err error) {
	query := `insert into posfar.dresep_online_history(bagian,tglinput,id,noreg,kd_dokter,nm_dokter,catatan,jenis_resep,alergi) values(?,?,?,?,?,?,?,?,?)`

	result := lu.DB.Raw(query, bagian, tglInput, id, noReg, kdDokter, nmDokter, catatan, jenisResep, alergi).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) PerjanjianKhususResepOnlineReporitory(noReg string) (res his.PerjanjianKhusus, err error) {
	query := `select * from his.perjanjiankhusus where noreg=?`

	result := lu.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (lu *hisRepository) InsertAntrianApotikRepository(data his.AntrianResepApotik) (res his.AntrianResepApotik, err error) {
	result := lu.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}
func (lu *hisRepository) InsertOnlineHistoryRepository(data his.DResepOnlineHistory) (res his.DResepOnlineHistory, err error) {
	result := lu.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}
func (lu *hisRepository) InsertAntrianApotik2Reporitory(data his.AntrianApotik2) (res his.AntrianApotik2, err error) {
	result := lu.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

// TempTempaApotikKeluarObat
func (lu *hisRepository) InsertTempObatKeluarObatRepository(data his.TempTempaApotikKeluarObat) (res his.TempTempaApotikKeluarObat, err error) {
	result := lu.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

// GET ALL KTARIP DOKTER
func (lu *hisRepository) GetAllKTaripDokterRepository() (res []his.KTaripDokterModel, err error) {
	query := `select * from his.ktaripdokter`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) OnGetDResepOnlineHistory(noRM string) (res []his.DResepOnlineHistory, err error) {

	query := `SELECT num, jenis_resep, catatan, signa, nomor, id, kd_dokter, bagian, tglinput, noreg, kodeobat, namaobat, satuan, jumlah, dosis, cara, racik, alergi FROM posfar.dresep_online_history  WHERE id=? AND kodeobat !='' ORDER BY num DESC LIMIT 20`

	// result := lu.DB.Find(&his.DResepOnlineHistory{}).Where("id=? AND kodeobat !=''", noRM).Limit(10).Order("num DESC").Scan(&res)

	result := lu.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) OnGetDResepOnlineManualHistory(noRM string) (res []his.DResepOnlineHistory, err error) {
	query := `SELECT num, jenis_resep, catatan, signa, nomor, id, kd_dokter, bagian, tglinput, noreg, kodeobat, namaobat, satuan, jumlah, dosis, cara, racik, alergi FROM posfar.dresep_online_history  WHERE id=? AND catatan !="" LIMIT 20`

	result := lu.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil

}

// FIND DAPOTIK AMBIL OBAT 1
func (lu *hisRepository) FindDApotikAmbilObat1Repository(kodeObat string) (res his.DapotikAmbilObat1, err error) {
	result := lu.DB.Where("saldo > 0 AND kodeobat=?", kodeObat).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// CARI PERJANJIAN KHUSUS
func (lu *hisRepository) CariPerjanjianKhususRepository(noReg string) (res his.HIsPerjanjianKhusus, err error) {
	result := lu.DB.Where("noreg=?", noReg).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// CARI PRESENTASE
func (lu *hisRepository) CariPresentaseObatRekananRepository(kode string, kelas string) (res his.HisProsentase, err error) {
	result := lu.DB.Where("kodeperusahaan=? AND kelas=?", kode, kelas).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) CariKInventoryObatRepository(kodeObat string) (res his.HisKInventory, err error) {
	result := lu.DB.Where("kodeobat=?", kodeObat).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) CariHisKInventory2PasienRekananRepository(kodeObat string, kodePerusahaan string) (res his.HisKInventory2, err error) {
	result := lu.DB.Where("kodeobat=? AND kodeperusahaan=?", kodeObat, kodePerusahaan).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) OnGetRiwayatKunjunganPasienRepository(noRm string) (res []his.RiwayatKunjungan, err error) {
	query := `SELECT tanggal, noreg, keterangan, kunjungan , id FROM rekam.dregister WHERE id=? ORDER BY tanggal DESC LIMIT 20 `

	result := lu.DB.Raw(query, noRm).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) OnGetInstruksiMedisFarmataologiRepository(noRM string, noReg string) (res []his.DApotikKeluarObat1, err error) {

	result := lu.DB.Where(&his.DApotikKeluarObat1{
		Id:    noRM,
		Noreg: noReg,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *hisRepository) OnGetReportInstruksiMedisFarmakologiRepository(bagian string, noReg string) (res []his.InstruksiMedisFarmakologi, err error) {

	errs := lu.DB.Where(his.InstruksiMedisFarmakologi{
		Noreg: noReg, KdBagian: bagian,
	}).Preload("Perawat").Preload("KPelayanan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (lu *hisRepository) OnGetViewDoubleCheckRepository(noReg string, bagian string, kodeObat string) (res []soap.DDoubleCheck, err error) {
	errs := lu.DB.Where(soap.DDoubleCheck{
		Noreg: noReg, KdBagian: bagian, KodeObat: kodeObat,
	}).Preload("Perawat").Find(&res)
	// }).Preload("Perawat").Preload("PerawatVerify").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (lu *hisRepository) OnGetPemberiInstruksiMedisFarmakologiRepository(bagian string, noReg string, kodeObat string) (res []his.InstruksiMedisFarmakologi, err error) {

	errs := lu.DB.Where(his.InstruksiMedisFarmakologi{
		Noreg: noReg, KdBagian: bagian, KodeObat: kodeObat,
	}).Preload("Perawat").Preload("KPelayanan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (lu *hisRepository) OnSaveInStruksiMedisFarmakologiRepository(noReg string, bagian string, userId string, req dto.ReqOnSaveFarmakologi) (res rme.DInstruksiMedisFarmakologi, err error) {

	times := time.Now()

	data := rme.DInstruksiMedisFarmakologi{
		InsertDttm:    times.Format("2006-01-02 15:04:05"),
		KdBagian:      bagian,
		NamaObat:      req.NamaObat,
		InsertUser:    userId,
		Dosis:         req.Dosis,
		Frekuensi:     req.Frekuensi,
		Noreg:         noReg,
		KodeObat:      req.KodeObat,
		JumlahObat:    req.JumlahObat,
		CaraPemberian: req.CaraPemberian,
		SisaObat:      req.SiSaObat,
	}

	result := lu.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}
