package usecase

import (
	"errors"
	"fmt"
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	soapDTO "hms_api/modules/soap/dto"
	"strconv"
	"strings"
	"time"
)

func (hu *hisUseCase) InputPemeriksaanLaborUsecaseV2(userID string, req dto.ReqInsertDetailLaborV2) (res his.InputLabResponse, err error) {
	// GET NOMOR LABOR
	labor, err := hu.hisRepository.GetNomorLabor(req.Noreg)
	times := time.Now()

	if err != nil {
		return res, errors.New(err.Error())
	}

	var total float64 = 0

	for i := 0; i <= len(req.DetailPemeriksaanLaborModel)-1; i++ {
		total = total + req.DetailPemeriksaanLaborModel[i].KtaripTotal.TaripKelas
	}

	if total > 0 {
		for i := 0; i <= len(req.DetailPemeriksaanLaborModel)-1; i++ {
			for _, V := range req.DetailPemeriksaanLaborModel[i].Pemeriksaan {
				// CREATE DATA
				dlabor := his.DHasilLaborModel{
					TipePemeriksaan:        his.NonCito,
					InsertDttm:             times.Format("2006-01-02 15:04:05"),
					InsertPc:               req.DeviceID,
					NoLab:                  "LAB" + req.Noreg + labor.Nomor,
					Noreg:                  req.Noreg,
					NameGrup:               V.NameGrup,
					Normal:                 V.Normal,
					Satuan:                 V.Satuan,
					Kode:                   V.Kode,
					Urut:                   V.Urut,
					InsertUserId:           userID,
					KdDrPengirim:           req.DokterPengirim,
					KetAsalPelayanan:       req.KetPoli,
					UmurPasien:             req.UmurPasien,
					Kelompok:               V.Kelompok,
					KodeBagian:             req.KodePoli,
					JamPengambilanSpesimen: times.Format("2006-01-02 15:04:05"),
					JamKirimSpesimen:       times.Format("2006-01-02 15:04:05"),
					KodeKelas:              req.KodeKelas,
				}

				_, err := hu.hisRepository.InsertLabor(dlabor)

				if err != nil {
					return res, errors.New(err.Error())
				}
			}
		}

		data := his.AntrianPasienModel{
			Mac:         req.DeviceID,
			UserId:      userID,
			Noreg:       req.Noreg,
			KdTujuan:    "LAB001",
			CodeFrom:    req.KodePoli,
			RecieveDttm: times.Format("2006-01-02 15:04:05"),
			KdDokter:    "",
			NoOrder:     "LAB" + req.Noreg + labor.Nomor,
		}

		// INPUT ANTRIAN LABOR
		_, err := hu.hisRepository.InsertAntrianPenmed(data)

		if err != nil {
			return res, errors.New(err.Error())
		}

		res.NoLab = "LAB" + req.Noreg + labor.Nomor

		return res, nil
	} else {
		return res, errors.New("tindakan tidak memiliki tarip")
	}

}

// GET HASIL LABORRATORIUM PADA TABLE LAMA
func (hu *hisUseCase) HistoryFisioterapiUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error) {
	// GET HASIL RADIOLOGI
	fisio, _ := hu.hisRepository.GetHasilFisioterapiRepositoryV2(noReg)

	hu.logging.Info(fisio)

	var data []his.RegHasilRadiologiTabelLama

	if len(fisio) > 0 {
		// eksekusi fisio
		for i := 0; i <= len(fisio)-1; i++ {
			detailRad, _ := hu.hisRepository.GetDetailHasilRadiologiOldDB(fisio[i].Bagian, noReg, fisio[i].Jaminput)

			data = append(data, his.RegHasilRadiologiTabelLama{
				Tanggal:              fisio[i].Jaminput,
				NamaKelompok:         fisio[i].Bagian,
				DHasilRadiologiOldDB: detailRad,
			})

		}
	}

	if len(fisio) < 1 {
		return data, nil
	}

	return data, nil
}

func (hu *hisUseCase) HistoryGiziUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error) {
	// GET HASIL RADIOLOGI
	fisio, _ := hu.hisRepository.GetHasilGiziRepositoryV2(noReg)

	hu.logging.Info(fisio)

	var data []his.RegHasilRadiologiTabelLama

	if len(fisio) > 0 {
		// eksekusi fisio
		for i := 0; i <= len(fisio)-1; i++ {
			hu.logging.Info("HASIL RADIOLOGI")
			hu.logging.Info(fisio[i].Bagian)
			detailRad, _ := hu.hisRepository.GetDetailHasilRadiologiOldDB(fisio[i].Bagian, noReg, fisio[i].Jaminput)

			hu.logging.Info("HASIL RADIOLOGI")
			hu.logging.Info(detailRad)

			data = append(data, his.RegHasilRadiologiTabelLama{
				Tanggal:              fisio[i].Jaminput,
				NamaKelompok:         fisio[i].Bagian,
				DHasilRadiologiOldDB: detailRad,
			})

		}
	}

	if len(fisio) < 1 {
		return data, nil
	}

	return data, nil
}

func (hu *hisUseCase) HistoryRadiologiUsecaseV2(noReg string) (res []his.RegHasilRadiologiTabelLama, err error) {
	// GET HASIL RADIOLOGI
	radiologi, _ := hu.hisRepository.GetHasilRadiologiRepositoryV2(noReg)

	hu.logging.Info(radiologi)

	var data []his.RegHasilRadiologiTabelLama

	if len(radiologi) > 0 {
		// eksekusi radiologi
		for i := 0; i <= len(radiologi)-1; i++ {
			detailRad, _ := hu.hisRepository.GetDetailHasilRadiologiOldDB(radiologi[i].Bagian, noReg, radiologi[i].Jaminput)

			data = append(data, his.RegHasilRadiologiTabelLama{
				Tanggal:              radiologi[i].Jaminput,
				NamaKelompok:         radiologi[i].Bagian,
				DHasilRadiologiOldDB: detailRad,
			})

		}
	}

	if len(radiologi) < 1 {
		return data, nil
	}

	return data, nil
}

// GET HASIL LABORRATORIUM PADA TABLE LAMA
func (hu *hisUseCase) HistoryLaboratoriumUsecaseV2(noReg string) (res []his.ResHasilLaborTableLama, err error) {

	// GET DATA DARI TABLE LAMA
	labor, _ := hu.hisRepository.GetPenlabTabelLamaRepository(noReg)

	var data []his.ResHasilLaborTableLama

	if len(labor) > 0 {
		// LAKUKAN QUERY PADA TABLE LABOR LAMA
		for i := 0; i <= len(labor)-1; i++ {
			kel, _ := hu.hisRepository.GetPenLabKelompokTabelLamaRepository(labor[i].Jaminput, labor[i].Noreg)
			// KETIKA KELOMPOK SUDAH DAPAT LAKUKAN QUERY PEMERIKSAAN
			for a := 0; a <= len(kel)-1; a++ {
				// LAKUKAN QUERY DAPATKAN DETAIL
				pemeriksaan, _ := hu.hisRepository.GetPenLabPemeriksaanTabelLamaRepository(labor[i].Jaminput, labor[i].Noreg, kel[a].NamaKelompok)

				data = append(data, his.ResHasilLaborTableLama{
					Tanggal:           labor[i].Jaminput,
					NamaKelompok:      kel[a].NamaKelompok,
					DPemeriksaanLabor: pemeriksaan,
				})

			}

		}
	}

	if len(labor) < 1 {
		return data, nil
	}

	return data, nil

}

func (hu *hisUseCase) InputPemeriksaanRadiologiUseCaseV2(userID string, req dto.ReqInsertDetailRadiologiV2) (res his.InputLabResponse, err error) {
	penmed, err := hu.hisRepository.GetNomorRadiologi(req.Noreg)

	times := time.Now()

	if err != nil {
		return res, errors.New(err.Error())
	}

	kdTujuan := []string{}
	penMedString := ""

	var total float64 = 0

	for i := 0; i <= len(req.DetailRadiologi)-1; i++ {
		// ==============================
		total = total + float64(req.DetailRadiologi[i].Tarip)

		// ==== FILTER LIST
		cari := searchInList(kdTujuan, req.DetailRadiologi[i].KdBag)

		if !cari {
			kdTujuan = append(kdTujuan, req.DetailRadiologi[i].KdBag)
		}

	}

	if total > 0 {

		if req.JenisPenunjang == "RADIOLOGI" {
			penMedString = "RAD" + req.Noreg + penmed.Nomor
		} else if req.JenisPenunjang == "GIZI" {
			penMedString = "GIZ" + req.Noreg + penmed.Nomor
		} else if req.JenisPenunjang == "FISIOTHERAPHY" {
			penMedString = "FIS" + req.Noreg + penmed.Nomor
		}

		// ================
		for i := 0; i <= len(req.DetailRadiologi)-1; i++ {
			dHasil := his.DHasilPenunjang{
				TipePemeriksaan:  his.NonCito,
				InsertDttm:       times.Format("2006-01-02 15:04:05"),
				InsertUserId:     userID,
				NoPenmed:         penMedString,
				KdDokterPengirim: req.DokterPengirim,
				KodePenunjang:    req.DetailRadiologi[i].KdBag,
				Kode:             req.DetailRadiologi[i].Kode,
				Noreg:            req.Noreg,
				KetAsalPelayanan: req.KetPoli,
				UmurPasien:       req.UmurPasien,
				KodeBagian:       req.KodePoli,
				KodeKelas:        req.KodeKelas,
			}

			_, err := hu.hisRepository.InsertRadiologi(dHasil)

			if err != nil {
				return res, errors.New(err.Error())
			}
		}

		for _, V := range kdTujuan {

			data := his.AntrianPasienModel{
				Mac:         req.DeviceID,
				UserId:      userID,
				Noreg:       req.Noreg,
				KdTujuan:    V,
				CodeFrom:    req.KodePoli,
				RecieveDttm: times.Format("2006-01-02 15:04:05"),
				KdDokter:    userID,
				NoOrder:     penMedString,
			}

			// INPUT ANTRIAN LABOR
			_, err := hu.hisRepository.InsertAntrianPenmed(data)

			if err != nil {
				return res, errors.New(err.Error())
			}
		}

		res.NoLab = penMedString
		return res, nil
	} else {
		hu.logging.Info(req)
		hu.logging.Info(total)
		return res, errors.New("tindakan tidak memiliki tarip")
	}

}

func (hu *hisUseCase) SaveResepPasienUsecase(req dto.RegOnSaveResepObat, userID string, bagian string) (message string, err error) {

	if len(req.Resep) == 0 {
		message := fmt.Sprintf("Resep tidak boleh kosong %s", "")
		return message, errors.New(message)
	}

	// GENERATE NOMOR RESEP
	nomorSekarang := ""
	nomor := ""
	dataBagian := ""
	kdBagian := ""
	var rumus = his.HisProsentase{}

	no, err12 := hu.hisRepository.GetNomorAntrianApotikRepository()

	hu.logging.Info("Nomor Sekarang " + nomorSekarang)
	hu.logging.Info("Nomor  " + nomor)

	// GET PERJANJIAN KHUSUS
	perjanjian, _ := hu.hisRepository.CariPerjanjianKhususRepository(req.Noreg)

	if perjanjian.Noreg != "" || len(perjanjian.Noreg) > 2 {
		rum, er11 := hu.hisRepository.CariPresentaseObatRekananRepository(perjanjian.Kodeperusahaan, "rajal")
		if er11 != nil {
			hu.logging.Info("ERROR CARI PRESENTASE")
			hu.logging.Info(er11)
		}
		rumus = rum
	}

	if bagian == "IGD001" {
		dataBagian = "UGD"
		kdBagian = "cpoli006"
	} else {
		dataBagian = bagian
		kdBagian = bagian
	}

	if err12 != nil || no.NoAntri == "" {
		hu.logging.Info(err12)
		times := time.Now()
		nomorSekarang = times.Format("2006-01-02") + "-0001"
		nomor = "0001"
	} else {
		antrianStr := strings.Split(no.NoAntri, "-")
		hu.logging.Info(antrianStr)
		str := antrianStr[3]
		noMor, _ := strconv.Atoi(str)

		var now int = noMor + 1
		nomor = strconv.Itoa(now)

		if len(nomor) == 2 {
			nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "00" + strconv.Itoa(now)
		}

		if len(nomor) == 1 {
			nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "000" + strconv.Itoa(now)
		}

		if len(nomor) == 3 {
			nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "0" + strconv.Itoa(now)

		}

		if len(nomor) == 4 {
			nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + strconv.Itoa(now)
		}
	}

	if len(nomor) == 2 {
		nomor = "00" + nomor
	}

	if len(nomor) == 1 {
		nomor = "000" + nomor
	}

	if len(nomor) == 3 {
		nomor = "0" + nomor
	}

	hu.logging.Info("PERJANJIAN OBAT ")
	hu.logging.Info(perjanjian)

	hu.logging.Info("NOMOR SEKARANG " + nomorSekarang)
	times := time.Now()
	hu.logging.Info(times)

	for i := 0; i <= len(req.Resep)-1; i++ {

		data := his.DResepOnlineHistory{
			JenisResep: "pos",
			Catatan:    req.Catatan,
			Signa:      "",
			Nomor:      "",
			KdDokter:   userID,
			Tglinput:   times.Format("2006-01-02 15:04:05"),
			NmDokter:   req.NamaUser,
			Bagian:     dataBagian,
			Dosis:      req.Resep[i].Dosis,
			Id:         req.Norm,
			Noreg:      req.Noreg,
			Kodeobat:   req.Resep[i].Kodeobat,
			Jumlah:     strconv.Itoa(req.Resep[i].Jumlah),
			Namaobat:   req.Resep[i].Namaobat,
			Satuan:     req.Resep[i].Satuan,
			Cara:       req.Resep[i].Aturan,
			Racik:      req.Resep[i].Flag,
			Alergi:     "Tidak Ada",
		}

		// INSERT PADA temp.tempapotikkeluarobat

		resep, errs22 := hu.hisRepository.InsertOnlineHistoryRepository(data)

		// GET DATA KELUAR OBAT
		ambilObat, er12 := hu.hisRepository.FindDApotikAmbilObat1Repository(req.Resep[i].Kodeobat)

		if er12 != nil {
			hu.logging.Info("DATA AMBIL OBAT TIDAK DITEMUKAN")
			hu.logging.Info(er12)
		} else {
			// INSERT TEMP OBAT
			// TENTUKAN HARGA OBAT
			if perjanjian.Kodeperusahaan == "" {
				obatJual, _ := hu.hisRepository.CariKInventoryObatRepository(req.Resep[i].Kodeobat)

				var diskon float64 = obatJual.Hargajual * rumus.Nilai
				var nowDiskon float64 = diskon + obatJual.Hargajual

				temp := his.TempTempaApotikKeluarObat{
					Tanggung:   "false",
					Nobatch:    ambilObat.Nobatch,
					Id:         req.Norm,
					Noreg:      req.Noreg,
					Bagian:     "Rajal",
					Ket:        dataBagian,
					Tglinput:   times.Format("2006-01-02 15:04:05"),
					Nokeluar:   "blom",
					Kodeobat:   req.Resep[i].Kodeobat,
					Noambil:    ambilObat.Noambil,
					Nomasuk:    ambilObat.Nomasuk,
					Namaobat:   req.Resep[i].Namaobat,
					Satuan:     req.Resep[i].Satuan,
					Hargajual:  nowDiskon,
					Hargabeli:  ambilObat.Hargabeli,
					Kadaluarsa: ambilObat.Kadaluarsa[0:10],
					Saldo:      ambilObat.Saldo,
					Jumlah:     float64(req.Resep[i].Jumlah),
					R:          0,
					Diskon:     0,
					Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
				}

				// INSERT TEMP OBAT
				hu.hisRepository.InsertTempObatKeluarObatRepository(temp)
			}

			if perjanjian.Kodeperusahaan != "" || len(perjanjian.Noreg) > 2 {
				obatJual, _ := hu.hisRepository.CariHisKInventory2PasienRekananRepository(req.Resep[i].Kodeobat, rumus.Kodeperusahaan)

				var diskon float64 = obatJual.Hargajual * rumus.Nilai
				var nowDiskon float64 = diskon + obatJual.Hargajual

				temp := his.TempTempaApotikKeluarObat{
					Tanggung:   "false",
					Nobatch:    ambilObat.Nobatch,
					Id:         req.Norm,
					Noreg:      req.Noreg,
					Bagian:     "Rajal",
					Ket:        dataBagian,
					Tglinput:   times.Format("2006-01-02 15:04:05"),
					Nokeluar:   "blom",
					Kodeobat:   req.Resep[i].Kodeobat,
					Noambil:    ambilObat.Noambil,
					Nomasuk:    ambilObat.Nomasuk,
					Namaobat:   req.Resep[i].Namaobat,
					Satuan:     req.Resep[i].Satuan,
					Hargajual:  nowDiskon,
					Hargabeli:  ambilObat.Hargabeli,
					Kadaluarsa: ambilObat.Kadaluarsa[0:10],
					Saldo:      ambilObat.Saldo,
					Jumlah:     float64(req.Resep[i].Jumlah),
					R:          0,
					Diskon:     0,
					Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
				}

				// INSERT TEMP OBAT
				hu.hisRepository.InsertTempObatKeluarObatRepository(temp)

			}

		}

		hu.logging.Info("RESEP OBAT PASIEN")
		hu.logging.Info(resep)

		if errs22 != nil {
			hu.logging.Info(errs22)
		}

		hu.logging.Info("DATA RESEP ONLINE BERHASIL DISIMPAN")

		hu.logging.Info("INSERT ANTRIAN RESEP ONLINE BERHASIL")
	}

	// SIMPAN ANTRIAN APOTIK
	apotik := his.AntrianResepApotik{
		Nomor:          nomorSekarang,
		Nama:           req.NamaPasien,
		Jenis:          "pos",
		Resep:          formatResep(req.Resep),
		Jaminput:       times.Format("2006-01-02 15:04:05"),
		Kodeperusahaan: perjanjian.Kodeperusahaan,
		Kodedokter:     userID,
		Id:             req.Norm,
		Noreg:          req.Noreg,
		Kodeapotik:     "01",
		Namaapotik:     req.NamaApotik,
		Kodeasal:       kdBagian,
		Kamar:          dataBagian,
		Kasur:          "",
		Kelas:          "",
		Ket:            req.Pelayanan,
		Tgldaftar:      times.Format("2006-01-02 15:04:05"),
		Notif:          "false",
	}

	insertAntrianApotik, err12 := hu.hisRepository.InsertAntrianApotikRepository(apotik)

	if err12 != nil {
		hu.logging.Info("ERROR INSERT ANTRIAN APOTIK")
		hu.logging.Info(err12)
	}

	hu.logging.Info(insertAntrianApotik)

	// INSERT ANTRIAN APOTIK 2
	apoitk2 := his.AntrianApotik2{
		Noreg:   req.Noreg,
		Jam:     times.Format("2006-01-02 15:04:05"),
		NoAntri: nomorSekarang,
		Nomor:   nomor,
		Notif:   "false",
	}

	apotik2, err22 := hu.hisRepository.InsertAntrianApotik2Reporitory(apoitk2)

	if err22 != nil {
		hu.logging.Info(err22)
		hu.logging.Info(apotik2)
		return "Resep gagal dikirimkan", err22
	}

	hu.logging.Info("INSERT ANTRIAN APOTIK 2 BERHASIL")

	return "Resep berhasil dikirimkan", nil
}

func (hu *hisUseCase) SaveResepPasienUseCaseV2(req dto.RegOnSaveResepObat, userID string, bagian string) (message string, err error) {

	pelayanan := ""

	if req.Pelayanan == "rajal" {
		pelayanan = "Rajal"
		if len(req.Resep) == 0 {
			message := fmt.Sprintf("Resep tidak boleh kosong %s", "")
			return message, errors.New(message)
		}

		// GENERATE NOMOR RESEP
		nomorSekarang := ""
		nomor := ""
		dataBagian := ""
		kdBagian := ""
		var rumus = his.HisProsentase{}

		no, err12 := hu.hisRepository.GetNomorAntrianApotikRepository()

		hu.logging.Info("Nomor Sekarang " + nomorSekarang)
		hu.logging.Info("Nomor  " + nomor)

		// GET PERJANJIAN KHUSUS
		perjanjian, _ := hu.hisRepository.CariPerjanjianKhususRepository(req.Noreg)

		if perjanjian.Noreg != "" || len(perjanjian.Noreg) > 2 {
			rum, er11 := hu.hisRepository.CariPresentaseObatRekananRepository(perjanjian.Kodeperusahaan, "rajal")
			if er11 != nil {
				hu.logging.Info("ERROR CARI PRESENTASE")
				hu.logging.Info(er11)
			}
			rumus = rum
		}

		if bagian == "IGD001" {
			dataBagian = "UGD"
			kdBagian = "cpoli006"
		} else {
			dataBagian = bagian
			kdBagian = bagian
		}

		if err12 != nil || no.NoAntri == "" {
			hu.logging.Info(err12)
			times := time.Now()
			nomorSekarang = times.Format("2006-01-02") + "-0001"
			nomor = "0001"
		} else {
			antrianStr := strings.Split(no.NoAntri, "-")
			hu.logging.Info(antrianStr)
			str := antrianStr[3]
			noMor, _ := strconv.Atoi(str)

			var now int = noMor + 1
			nomor = strconv.Itoa(now)

			if len(nomor) == 2 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "00" + strconv.Itoa(now)
			}

			if len(nomor) == 1 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "000" + strconv.Itoa(now)
			}

			if len(nomor) == 3 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "0" + strconv.Itoa(now)
			}

			if len(nomor) == 4 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + strconv.Itoa(now)
			}
		}

		if len(nomor) == 2 {
			nomor = "00" + nomor
		}

		if len(nomor) == 1 {
			nomor = "000" + nomor
		}

		if len(nomor) == 3 {
			nomor = "0" + nomor
		}

		hu.logging.Info("PERJANJIAN OBAT ")
		hu.logging.Info(perjanjian)

		hu.logging.Info("NOMOR SEKARANG " + nomorSekarang)
		times := time.Now()
		hu.logging.Info(times)

		for i := 0; i <= len(req.Resep)-1; i++ {
			hu.logging.Info(strconv.Itoa(req.Resep[i].Jumlah))
			hu.logging.Info(req.Resep[i].Namaobat)
			hu.logging.Info(req.Resep[i].Aturan)
			hu.logging.Info(req.Resep[i].Satuan)
			hu.logging.Info(req.Resep[i].Saldo)
			hu.logging.Info(req.Resep[i].Aso)
			hu.logging.Info(req.Resep[i].Dosis)
			hu.logging.Info(req.Resep[i].Flag)
			hu.logging.Info(req.Resep[i].JumlahRacikan)

			data := his.DResepOnlineHistory{
				JenisResep: "pos",
				Catatan:    req.Catatan,
				Signa:      "",
				Nomor:      "",
				KdDokter:   userID,
				Tglinput:   times.Format("2006-01-02 15:04:05"),
				NmDokter:   req.NamaUser,
				Bagian:     dataBagian,
				Dosis:      req.Resep[i].Dosis,
				Id:         req.Norm,
				Noreg:      req.Noreg,
				Kodeobat:   req.Resep[i].Kodeobat,
				Jumlah:     strconv.Itoa(req.Resep[i].Jumlah),
				Namaobat:   req.Resep[i].Namaobat,
				Satuan:     req.Resep[i].Satuan,
				Cara:       req.Resep[i].Aturan,
				Racik:      req.Resep[i].Flag,
				Alergi:     "Tidak Ada",
			}

			// INSERT PADA temp.tempapotikkeluarobat

			resep, errs22 := hu.hisRepository.InsertOnlineHistoryRepository(data)

			// GET DATA KELUAR OBAT
			// FindDApotikAmbilObat1Repository(kodeObat string) (res his.DapotikAmbilObat1, err error)
			ambilObat, er12 := hu.hisRepository.FindDApotikAmbilObat1Repository(req.Resep[i].Kodeobat)

			if er12 != nil {
				hu.logging.Info("DATA AMBIL OBAT TIDAK DITEMUKAN")
				hu.logging.Info(er12)
			} else {
				// INSERT TEMP OBAT
				// TENTUKAN HARGA OBAT
				if perjanjian.Kodeperusahaan == "" {
					obatJual, _ := hu.hisRepository.CariKInventoryObatRepository(req.Resep[i].Kodeobat)

					var diskon float64 = obatJual.Hargajual * rumus.Nilai
					var nowDiskon float64 = diskon + obatJual.Hargajual

					temp := his.TempTempaApotikKeluarObat{
						Tanggung:   "false",
						Nobatch:    ambilObat.Nobatch,
						Id:         req.Norm,
						Noreg:      req.Noreg,
						Bagian:     "Rajal",
						Ket:        dataBagian,
						Tglinput:   times.Format("2006-01-02 15:04:05"),
						Nokeluar:   "blom",
						Kodeobat:   req.Resep[i].Kodeobat,
						Noambil:    ambilObat.Noambil,
						Nomasuk:    ambilObat.Nomasuk,
						Namaobat:   req.Resep[i].Namaobat,
						Satuan:     req.Resep[i].Satuan,
						Hargajual:  nowDiskon,
						Hargabeli:  ambilObat.Hargabeli,
						Kadaluarsa: ambilObat.Kadaluarsa[0:10],
						Saldo:      ambilObat.Saldo,
						Jumlah:     float64(req.Resep[i].Jumlah),
						R:          0,
						Diskon:     0,
						Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
					}

					// INSERT TEMP OBAT
					hu.hisRepository.InsertTempObatKeluarObatRepository(temp)
				}
				if perjanjian.Kodeperusahaan != "" || len(perjanjian.Noreg) > 2 {
					obatJual, _ := hu.hisRepository.CariHisKInventory2PasienRekananRepository(req.Resep[i].Kodeobat, rumus.Kodeperusahaan)

					var diskon float64 = obatJual.Hargajual * rumus.Nilai
					var nowDiskon float64 = diskon + obatJual.Hargajual

					temp := his.TempTempaApotikKeluarObat{
						Tanggung:   "false",
						Nobatch:    ambilObat.Nobatch,
						Id:         req.Norm,
						Noreg:      req.Noreg,
						Bagian:     "Rajal",
						Ket:        req.Pelayanan,
						Tglinput:   times.Format("2006-01-02 15:04:05"),
						Nokeluar:   "blom",
						Kodeobat:   req.Resep[i].Kodeobat,
						Noambil:    ambilObat.Noambil,
						Nomasuk:    ambilObat.Nomasuk,
						Namaobat:   req.Resep[i].Namaobat,
						Satuan:     req.Resep[i].Satuan,
						Hargajual:  nowDiskon,
						Hargabeli:  ambilObat.Hargabeli,
						Kadaluarsa: ambilObat.Kadaluarsa[0:10],
						Saldo:      ambilObat.Saldo,
						Jumlah:     float64(req.Resep[i].Jumlah),
						R:          0,
						Diskon:     0,
						Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
					}

					// INSERT TEMP OBAT
					hu.hisRepository.InsertTempObatKeluarObatRepository(temp)

				}

			}

			hu.logging.Info("RESEP OBAT PASIEN")
			hu.logging.Info(resep)

			if errs22 != nil {
				hu.logging.Info(errs22)
			}

			hu.logging.Info("DATA RESEP ONLINE BERHASIL DISIMPAN")

			hu.logging.Info("INSERT ANTRIAN RESEP ONLINE BERHASIL")
		}

		// SIMPAN ANTRIAN APOTIK
		apotik := his.AntrianResepApotik{
			Nomor:          nomorSekarang,
			Nama:           req.NamaPasien,
			Jenis:          "pos",
			Resep:          formatResep(req.Resep),
			Jaminput:       times.Format("2006-01-02 15:04:05"),
			Kodeperusahaan: perjanjian.Kodeperusahaan,
			Kodedokter:     userID,
			Id:             req.Norm,
			Noreg:          req.Noreg,
			Kodeapotik:     "01",
			Namaapotik:     req.NamaApotik,
			Kodeasal:       kdBagian,
			Kamar:          req.Kamar,
			Kasur:          req.Kasur,
			Kelas:          req.Kelas,
			Ket:            req.Pelayanan,
			Tgldaftar:      times.Format("2006-01-02 15:04:05"),
			Notif:          "false",
		}

		insertAntrianApotik, err12 := hu.hisRepository.InsertAntrianApotikRepository(apotik)

		if err12 != nil {
			hu.logging.Info("ERROR INSERT ANTRIAN APOTIK")
			hu.logging.Info(err12)
		}

		hu.logging.Info(insertAntrianApotik)

		// INSERT ANTRIAN APOTIK 2
		apoitk2 := his.AntrianApotik2{
			Noreg:   req.Noreg,
			Jam:     times.Format("2006-01-02 15:04:05"),
			NoAntri: nomorSekarang,
			Nomor:   nomor,
			Notif:   "false",
		}

		apotik2, err22 := hu.hisRepository.InsertAntrianApotik2Reporitory(apoitk2)

		if err22 != nil {
			hu.logging.Info(err22)
			hu.logging.Info(apotik2)
			return "Resep gagal dikirimkan", err22
		}

		hu.logging.Info("INSERT ANTRIAN APOTIK 2 BERHASIL")

		return "Resep berhasil dikirimkan", nil
	}

	if req.Pelayanan == "ranap" {
		pelayanan = "Ranap"

		if len(req.Resep) == 0 {
			message := fmt.Sprintf("Resep tidak boleh kosong %s", "")
			return message, errors.New(message)
		}

		// GENERATE NOMOR RESEP
		nomorSekarang := ""
		nomor := ""
		dataBagian := ""
		kdBagian := ""

		var rumus = his.HisProsentase{}

		no, err12 := hu.hisRepository.GetNomorAntrianApotikRepository()

		// GET PERJANJIAN KHUSUS
		perjanjian, _ := hu.hisRepository.CariPerjanjianKhususRepository(req.Noreg)

		if perjanjian.Noreg != "" || len(perjanjian.Noreg) > 2 {
			rum, er11 := hu.hisRepository.CariPresentaseObatRekananRepository(perjanjian.Kodeperusahaan, "rajal")
			if er11 != nil {
				hu.logging.Info("ERROR CARI PRESENTASE")
				hu.logging.Info(er11)
			}
			rumus = rum
		}

		if bagian == "IGD001" {
			dataBagian = "UGD"
			kdBagian = "cpoli006"
		} else if bagian == "CEMP" {
			kdBagian = "CEMP"
			dataBagian = "Cempaka Lima"
		} else {
			dataBagian = bagian
			kdBagian = bagian
		}

		if err12 != nil || no.NoAntri == "" {
			hu.logging.Info(err12)
			times := time.Now()
			nomorSekarang = times.Format("2006-01-02") + "-0001"
			nomor = "0001"
		} else {
			antrianStr := strings.Split(no.NoAntri, "-")
			hu.logging.Info(antrianStr)
			str := antrianStr[3]
			noMor, _ := strconv.Atoi(str)

			var now int = noMor + 1
			nomor = strconv.Itoa(now)

			if len(nomor) == 2 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "00" + strconv.Itoa(now)
			}

			if len(nomor) == 1 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "000" + strconv.Itoa(now)
			}

			if len(nomor) == 3 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "0" + strconv.Itoa(now)
				// nomor = "0" + strconv.Itoa(now)

			}

			if len(nomor) == 4 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + strconv.Itoa(now)
				// nomor = strconv.Itoa(now)
			}
		}

		if len(nomor) == 2 {
			nomor = "00" + nomor
		}

		if len(nomor) == 1 {
			nomor = "000" + nomor
		}

		if len(nomor) == 3 {
			nomor = "0" + nomor
		}

		hu.logging.Info("PERJANJIAN OBAT ")
		hu.logging.Info(perjanjian)

		hu.logging.Info("NOMOR SEKARANG " + nomorSekarang)
		times := time.Now()
		hu.logging.Info(times)

		for i := 0; i <= len(req.Resep)-1; i++ {

			// MASUKKAN DATA TEMP APOTIK KELUAR OBAT
			// PADA RESEP RAWAT INAP

			data := his.DResepOnlineHistory{
				JenisResep: "pos",
				Catatan:    req.Catatan,
				Signa:      "",
				Nomor:      "",
				KdDokter:   userID,
				Tglinput:   times.Format("2006-01-02 15:04:05"),
				NmDokter:   req.NamaUser,
				Bagian:     dataBagian,
				Dosis:      req.Resep[i].Dosis,
				Id:         req.Norm,
				Noreg:      req.Noreg,
				Kodeobat:   req.Resep[i].Kodeobat,
				Jumlah:     strconv.Itoa(req.Resep[i].Jumlah),
				Namaobat:   req.Resep[i].Namaobat,
				Satuan:     req.Resep[i].Satuan,
				Cara:       req.Resep[i].Aturan,
				Racik:      req.Resep[i].Flag,
				Alergi:     "Tidak Ada",
			}

			// INSERT PADA temp.tempapotikkeluarobat

			resep, errs22 := hu.hisRepository.InsertOnlineHistoryRepository(data)

			// GET DATA KELUAR OBAT
			// FindDApotikAmbilObat1Repository(kodeObat string) (res his.DapotikAmbilObat1, err error)
			ambilObat, er12 := hu.hisRepository.FindDApotikAmbilObat1Repository(req.Resep[i].Kodeobat)

			if er12 != nil {
				hu.logging.Info("DATA AMBIL OBAT TIDAK DITEMUKAN")
				hu.logging.Info(er12)
			}

			// INSERT TEMP OBAT
			// TENTUKAN HARGA OBAT
			if perjanjian.Kodeperusahaan == "" {
				obatJual, _ := hu.hisRepository.CariKInventoryObatRepository(req.Resep[i].Kodeobat)

				var diskon float64 = obatJual.Hargajual * rumus.Nilai
				var nowDiskon float64 = diskon + obatJual.Hargajual

				// tempapotikkeluarobat
				temp := his.TempTempaApotikKeluarObat{
					Tanggung:   "false",
					Nobatch:    ambilObat.Nobatch,
					Id:         req.Norm,
					Noreg:      req.Noreg,
					Bagian:     pelayanan,
					Ket:        dataBagian,
					Tglinput:   times.Format("2006-01-02 15:04:05"),
					Nokeluar:   "blom",
					Kodeobat:   req.Resep[i].Kodeobat,
					Noambil:    ambilObat.Noambil,
					Nomasuk:    ambilObat.Nomasuk,
					Namaobat:   req.Resep[i].Namaobat,
					Satuan:     req.Resep[i].Satuan,
					Hargajual:  nowDiskon,
					Hargabeli:  ambilObat.Hargabeli,
					Kadaluarsa: ambilObat.Kadaluarsa[0:10],
					Saldo:      ambilObat.Saldo,
					Jumlah:     float64(req.Resep[i].Jumlah),
					R:          0,
					Diskon:     0,
					Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
				}

				// INSERT TEMP OBAT
				hu.hisRepository.InsertTempObatKeluarObatRepository(temp)
			}

			if perjanjian.Kodeperusahaan != "" || len(perjanjian.Noreg) > 2 {
				obatJual, _ := hu.hisRepository.CariHisKInventory2PasienRekananRepository(req.Resep[i].Kodeobat, rumus.Kodeperusahaan)

				var diskon float64 = obatJual.Hargajual * rumus.Nilai
				var nowDiskon float64 = diskon + obatJual.Hargajual

				temp := his.TempTempaApotikKeluarObat{
					Tanggung:   "false",
					Nobatch:    ambilObat.Nobatch,
					Id:         req.Norm,
					Noreg:      req.Noreg,
					Bagian:     pelayanan,
					Ket:        dataBagian,
					Tglinput:   times.Format("2006-01-02 15:04:05"),
					Nokeluar:   "blom",
					Kodeobat:   req.Resep[i].Kodeobat,
					Noambil:    ambilObat.Noambil,
					Nomasuk:    ambilObat.Nomasuk,
					Namaobat:   req.Resep[i].Namaobat,
					Satuan:     req.Resep[i].Satuan,
					Hargajual:  nowDiskon,
					Hargabeli:  ambilObat.Hargabeli,
					Kadaluarsa: ambilObat.Kadaluarsa[0:10],
					Saldo:      ambilObat.Saldo,
					Jumlah:     float64(req.Resep[i].Jumlah),
					R:          0,
					Diskon:     0,
					Total:      float64(nowDiskon * float64(req.Resep[i].Jumlah)),
				}

				// INSERT TEMP OBAT
				hu.hisRepository.InsertTempObatKeluarObatRepository(temp)

			}

			hu.logging.Info("RESEP OBAT PASIEN")
			hu.logging.Info(resep)

			if errs22 != nil {
				hu.logging.Info(errs22)
			}

			hu.logging.Info("DATA RESEP ONLINE BERHASIL DISIMPAN")

			hu.logging.Info("INSERT ANTRIAN RESEP ONLINE BERHASIL")
		}

		// SIMPAN ANTRIAN APOTIK
		apotik := his.AntrianResepApotik{
			Nomor:          nomorSekarang,
			Nama:           req.NamaPasien,
			Jenis:          "pos",
			Resep:          formatResep(req.Resep),
			Jaminput:       times.Format("2006-01-02 15:04:05"),
			Kodeperusahaan: perjanjian.Kodeperusahaan,
			Kodedokter:     userID,
			Id:             req.Norm,
			Noreg:          req.Noreg,
			Kodeapotik:     "01",
			Namaapotik:     req.NamaApotik,
			Kodeasal:       kdBagian,
			Kamar:          dataBagian,
			Kasur:          "",
			Kelas:          "",
			Ket:            req.Pelayanan,
			Tgldaftar:      times.Format("2006-01-02 15:04:05"),
			Notif:          "false",
		}

		insertAntrianApotik, err12 := hu.hisRepository.InsertAntrianApotikRepository(apotik)

		if err12 != nil {
			hu.logging.Info("ERROR INSERT ANTRIAN APOTIK")
			hu.logging.Info(err12)
		}

		hu.logging.Info(insertAntrianApotik)

		// INSERT ANTRIAN APOTIK 2
		apoitk2 := his.AntrianApotik2{
			Noreg:   req.Noreg,
			Jam:     times.Format("2006-01-02 15:04:05"),
			NoAntri: nomorSekarang,
			Nomor:   nomor,
			Notif:   "false",
		}

		apotik2, err22 := hu.hisRepository.InsertAntrianApotik2Reporitory(apoitk2)

		if err22 != nil {
			hu.logging.Info(err22)
			hu.logging.Info(apotik2)
			return "Resep gagal dikirimkan", err22
		}

		hu.logging.Info("INSERT ANTRIAN APOTIK 2 BERHASIL")

		return "Resep berhasil dikirimkan", nil
	}

	return "Data gagal diproses", errors.New("gagal mengirim resep")
}

func (hu *hisUseCase) SaveResepPasienManualUseCase(req dto.ReqOnSaveResepManual, userID string, bagian string) (message string, err error) {

	pelayanan := ""

	if req.Pelayanan == "rajal" {

		pelayanan = "Rajal"

		if len(req.CatatanResep) == 0 {
			message := fmt.Sprintf("Resep tidak boleh kosong %s", "")
			return message, errors.New(message)
		}

		// GENERATE NOMOR RESEP
		nomorSekarang := ""
		nomor := ""
		dataBagian := ""
		kdBagian := ""

		no, err12 := hu.hisRepository.GetNomorAntrianApotikRepository()

		hu.logging.Info("Nomor Sekarang " + nomorSekarang)
		hu.logging.Info("Nomor  " + nomor)

		// GET PERJANJIAN KHUSUS
		perjanjian, _ := hu.hisRepository.CariPerjanjianKhususRepository(req.Noreg)

		switch bagian {
		case "IGD001":
			dataBagian = "UGD"
			kdBagian = "cpoli006"
		case "ELI1":
			kdBagian = "ELI1"
			dataBagian = "Elisabeth 1"
		case "ELI2":
			kdBagian = "ELI2"
			dataBagian = "Elisabeth 2"
		case "FRA1":
			kdBagian = "FRA1"
			dataBagian = "Fransiskus 1"
		case "FRA2":
			kdBagian = "FRA2"
			dataBagian = "Fransiskus 2"
		case "MARI":
			kdBagian = "MARI"
			dataBagian = "Maria"
		default:
			dataBagian = bagian
			kdBagian = bagian
		}

		if err12 != nil || no.NoAntri == "" {
			hu.logging.Info(err12)
			times := time.Now()
			nomorSekarang = times.Format("2006-01-02") + "-0001"
			nomor = "0001"
		} else {
			antrianStr := strings.Split(no.NoAntri, "-")
			hu.logging.Info(antrianStr)
			str := antrianStr[3]
			noMor, _ := strconv.Atoi(str)

			var now int = noMor + 1
			nomor = strconv.Itoa(now)

			if len(nomor) == 2 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "00" + strconv.Itoa(now)
			}

			if len(nomor) == 1 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "000" + strconv.Itoa(now)
			}

			if len(nomor) == 3 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "0" + strconv.Itoa(now)
			}

			if len(nomor) == 4 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + strconv.Itoa(now)
			}
		}

		if len(nomor) == 2 {
			nomor = "00" + nomor
		}

		if len(nomor) == 1 {
			nomor = "000" + nomor
		}

		if len(nomor) == 3 {
			nomor = "0" + nomor
		}

		hu.logging.Info(perjanjian)

		hu.logging.Info("NOMOR SEKARANG " + nomorSekarang)
		times := time.Now()
		hu.logging.Info(times)

		data := his.DResepOnlineHistory{
			JenisResep: "pos",
			Catatan:    req.CatatanResep,
			Signa:      "",
			Nomor:      "",
			KdDokter:   userID,
			Tglinput:   times.Format("2006-01-02 15:04:05"),
			NmDokter:   req.NamaUser,
			Bagian:     dataBagian,
			Dosis:      "",
			Id:         req.NoRm,
			Noreg:      req.Noreg,
			Kodeobat:   "",
			Jumlah:     "0",
			Namaobat:   "",
			Satuan:     "",
			Cara:       "",
			Racik:      "",
			Alergi:     "Tidak Ada",
		}

		resep, errs22 := hu.hisRepository.InsertOnlineHistoryRepository(data)

		hu.logging.Info("RESEP OBAT PASIEN")
		hu.logging.Info(resep)

		if errs22 != nil {
			hu.logging.Info(errs22)
		}

		hu.logging.Info("DATA RESEP ONLINE BERHASIL DISIMPAN")

		hu.logging.Info("INSERT ANTRIAN RESEP ONLINE BERHASIL")

		// SIMPAN ANTRIAN APOTIK
		apotik := his.AntrianResepApotik{
			Nomor:          nomorSekarang,
			Nama:           req.NamaPasien,
			Jenis:          "pos",
			Resep:          req.CatatanResep,
			Jaminput:       times.Format("2006-01-02 15:04:05"),
			Kodeperusahaan: perjanjian.Kodeperusahaan,
			Kodedokter:     userID,
			Id:             req.NoRm,
			Noreg:          req.Noreg,
			Kodeapotik:     "01",
			Namaapotik:     req.NamaApotik,
			Kodeasal:       kdBagian,
			Kamar:          req.Kamar,
			Kasur:          req.Kasur,
			Kelas:          req.Kelas,
			Ket:            req.Pelayanan,
			Tgldaftar:      times.Format("2006-01-02 15:04:05"),
			Notif:          "false",
		}

		insertAntrianApotik, err12 := hu.hisRepository.InsertAntrianApotikRepository(apotik)

		if err12 != nil {
			hu.logging.Info("ERROR INSERT ANTRIAN APOTIK")
			hu.logging.Info(err12)
		}

		hu.logging.Info(insertAntrianApotik)

		// INSERT ANTRIAN APOTIK 2
		apoitk2 := his.AntrianApotik2{
			Noreg:   req.Noreg,
			Jam:     times.Format("2006-01-02 15:04:05"),
			NoAntri: nomorSekarang,
			Nomor:   nomor,
			Notif:   "false",
		}

		apotik2, err22 := hu.hisRepository.InsertAntrianApotik2Reporitory(apoitk2)

		if err22 != nil {
			hu.logging.Info(err22)
			hu.logging.Info(apotik2)
			return "Resep gagal dikirimkan", err22
		}

		hu.logging.Info("INSERT ANTRIAN APOTIK 2 BERHASIL")

		return "Resep berhasil dikirimkan", nil
	}

	if req.Pelayanan == "ranap" {
		pelayanan = "Ranap"

		// GENERATE NOMOR RESEP
		nomorSekarang := ""
		nomor := ""
		dataBagian := ""
		kdBagian := ""

		no, err12 := hu.hisRepository.GetNomorAntrianApotikRepository()

		hu.logging.Info("Nomor Sekarang " + nomorSekarang)
		hu.logging.Info("Nomor  " + nomor)

		// GET PERJANJIAN KHUSUS
		perjanjian, _ := hu.hisRepository.CariPerjanjianKhususRepository(req.Noreg)

		switch bagian {
		case "IGD001":
			dataBagian = "UGD"
			kdBagian = "cpoli006"
		case "ELI1":
			kdBagian = "ELI1"
			dataBagian = "Elisabeth 1"
		case "ELI2":
			kdBagian = "ELI2"
			dataBagian = "Elisabeth 2"
		case "FRA1":
			kdBagian = "FRA1"
			dataBagian = "Fransiskus 1"
		case "FRA2":
			kdBagian = "FRA2"
			dataBagian = "Fransiskus 2"
		case "MARI":
			kdBagian = "MARI"
			dataBagian = "Maria"
		default:
			dataBagian = bagian
			kdBagian = bagian
		}

		if err12 != nil || no.NoAntri == "" {
			hu.logging.Info(err12)
			times := time.Now()
			nomorSekarang = times.Format("2006-01-02") + "-0001"
			nomor = "0001"
		} else {
			antrianStr := strings.Split(no.NoAntri, "-")
			hu.logging.Info(antrianStr)
			str := antrianStr[3]
			noMor, _ := strconv.Atoi(str)

			var now int = noMor + 1
			nomor = strconv.Itoa(now)

			if len(nomor) == 2 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "00" + strconv.Itoa(now)
			}

			if len(nomor) == 1 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "000" + strconv.Itoa(now)
			}

			if len(nomor) == 3 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + "0" + strconv.Itoa(now)
			}

			if len(nomor) == 4 {
				nomorSekarang = antrianStr[0] + "-" + antrianStr[1] + "-" + antrianStr[2] + "-" + strconv.Itoa(now)
			}
		}

		if len(nomor) == 2 {
			nomor = "00" + nomor
		}

		if len(nomor) == 1 {
			nomor = "000" + nomor
		}

		if len(nomor) == 3 {
			nomor = "0" + nomor
		}

		hu.logging.Info("PERJANJIAN OBAT ")
		hu.logging.Info(perjanjian)

		hu.logging.Info("NOMOR SEKARANG " + nomorSekarang)
		times := time.Now()
		hu.logging.Info(times)

		data := his.DResepOnlineHistory{
			JenisResep: "pos",
			Catatan:    req.CatatanResep,
			Signa:      "",
			Nomor:      "",
			KdDokter:   userID,
			Tglinput:   times.Format("2006-01-02 15:04:05"),
			NmDokter:   req.NamaUser,
			Bagian:     dataBagian,
			Dosis:      "",
			Id:         req.NoRm,
			Noreg:      req.Noreg,
			Kodeobat:   "",
			Jumlah:     "",
			Namaobat:   "",
			Satuan:     "",
			Cara:       "",
			Racik:      "",
			Alergi:     "Tidak Ada",
		}

		// INSERT PADA temp.tempapotikkeluarobat

		resep, _ := hu.hisRepository.InsertOnlineHistoryRepository(data)

		hu.logging.Info(resep)

		// SIMPAN ANTRIAN APOTIK
		apotik := his.AntrianResepApotik{
			Nomor:          nomorSekarang,
			Nama:           req.NamaPasien,
			Jenis:          "pos",
			Resep:          req.CatatanResep,
			Jaminput:       times.Format("2006-01-02 15:04:05"),
			Kodeperusahaan: perjanjian.Kodeperusahaan,
			Kodedokter:     userID,
			Id:             req.NoRm,
			Noreg:          req.Noreg,
			Kodeapotik:     "01",
			Namaapotik:     req.NamaApotik,
			Kodeasal:       kdBagian,
			Kamar:          dataBagian,
			Kasur:          "",
			Kelas:          "",
			Ket:            req.Pelayanan,
			Tgldaftar:      times.Format("2006-01-02 15:04:05"),
			Notif:          "false",
		}

		insertAntrianApotik, err12 := hu.hisRepository.InsertAntrianApotikRepository(apotik)

		if err12 != nil {
			hu.logging.Info("ERROR INSERT ANTRIAN APOTIK")
			hu.logging.Info(err12)
		}

		hu.logging.Info(insertAntrianApotik)

		// INSERT ANTRIAN APOTIK 2
		apoitk2 := his.AntrianApotik2{
			Noreg:   req.Noreg,
			Jam:     times.Format("2006-01-02 15:04:05"),
			NoAntri: nomorSekarang,
			Nomor:   nomor,
			Notif:   "false",
		}

		apotik2, err22 := hu.hisRepository.InsertAntrianApotik2Reporitory(apoitk2)

		if err22 != nil {
			hu.logging.Info(err22)
			hu.logging.Info(apotik2)
			return "Resep gagal dikirimkan", err22
		}

		hu.logging.Info("INSERT ANTRIAN APOTIK 2 BERHASIL")

		return "Resep berhasil dikirimkan", nil
	}

	hu.logging.Info(pelayanan)
	return "Data gagal diproses", errors.New("gagal mengirim resep")
}

func formatResep(resep []dto.ResKTaripPersediaanObat) string {
	var builder strings.Builder

	for _, obat := range resep {
		line := fmt.Sprintf("%s  %d  %s S %s %s", obat.Namaobat, obat.Jumlah, obat.Satuan, obat.Dosis, obat.Satuan)
		builder.WriteString(line)
		builder.WriteString("\n")
	}

	return builder.String()
}

func (su *hisUseCase) OnGetReportPengkajianAnakUseCase(kdBagian string, noReg string, noRM string, tanggal string) (res soapDTO.ResponseAsesmenAnak, err error) {
	alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)
	perawat, _ := su.soapRepository.OnGetPengkajianAnakRepository(noReg, kdBagian)
	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(noRM, tanggal)

	mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, perawat, riwayatPerawat)
	return mapper, nil
}
