package usecase

import (
	"errors"
	"hms_api/modules/his"
	"hms_api/modules/his/dto"
	"hms_api/modules/his/entity"
	rmeRepo "hms_api/modules/rme/entity"
	soapRepo "hms_api/modules/soap/entity"
	"time"

	"github.com/sirupsen/logrus"
)

type hisUseCase struct {
	logging        *logrus.Logger
	hisRepository  entity.HisRepository
	hisMapper      entity.HisMapper
	rmeRepository  rmeRepo.RMERepository
	soapRepository soapRepo.SoapRepository
	soapMapper     soapRepo.SoapMapper
}

func NewHisUseCase(ur entity.HisRepository, logging *logrus.Logger, mapper entity.HisMapper, rme rmeRepo.RMERepository, soapRepo soapRepo.SoapRepository, soapMapper soapRepo.SoapMapper) entity.HisUsecase {
	return &hisUseCase{
		hisRepository:  ur,
		hisMapper:      mapper,
		logging:        logging,
		rmeRepository:  rme,
		soapRepository: soapRepo,
		soapMapper:     soapMapper,
	}
}

func (hu *hisUseCase) OnGetReportInstruksiMedisFarmakologiUseCase(noReg string, kdBagian string) (err error) {

	return nil
}

func (hu *hisUseCase) HistoryLaboratoriumUsecase(noReg string) (res []his.ResHasilLabor, err error) {

	labor, _ := hu.hisRepository.GetHasilLabRepository(noReg)
	var data []his.ResHasilLabor

	if len(labor) > 0 {
		for i := 0; i <= len(labor)-1; i++ {

			kel, _ := hu.hisRepository.GetKelompokLab(labor[i].NoLab, noReg)

			for _, V := range kel {
				data[i].Kelompok = V.NameGrup
				hasil, _ := hu.hisRepository.GetHasilHisoryLab(labor[i].NoLab, noReg, V.NameGrup)
				data[i].Lab = hasil

				data = append(data, his.ResHasilLabor{
					Dpjp:           labor[i].Namadokter,
					AsalPpelayanan: labor[i].KetAsalPelayanan,
					NomorLab:       labor[i].NoLab,
				})
			}

		}
	}

	if len(labor) < 1 {
		return data, nil
	}

	return data, nil

}

func (hu *hisUseCase) HistoryDhasilPenunjangUsecase(noReg string) (res []his.ResHasilRadiologiMapper, err error) {
	// CARI HASIL PENUNJANG USECASE
	radiologi, err := hu.hisRepository.GetHasilDPenunjangRadiologiRepository(noReg)

	if err != nil {
		return res, errors.New("Radiologi")
	}

	hu.logging.Info("RADIOLOGI")

	var data []his.ResHasilRadiologiMapper

	if len(radiologi) > 0 {
		for i := 0; i <= len(radiologi)-1; i++ {

			if radiologi[i].KodePenunjang == "RON001" {
				data = append(data, his.ResHasilRadiologiMapper{
					Judul:  "Radiologi",
					Hasil:  radiologi[i].Hasil,
					Uraian: radiologi[i].Uraian,
				})
			}

			if radiologi[i].KodePenunjang == "CTS001" {
				data = append(data, his.ResHasilRadiologiMapper{
					Judul:  "Citiscan",
					Hasil:  radiologi[i].Hasil,
					Uraian: radiologi[i].Uraian,
				})

			}
			if radiologi[i].KodePenunjang == "USG001" {
				data = append(data, his.ResHasilRadiologiMapper{
					Judul:  "USG",
					Hasil:  radiologi[i].Hasil,
					Uraian: radiologi[i].Uraian,
				})

			}
		}
	}

	if len(radiologi) < 1 {
		return data, nil
	}

	return data, nil

}

func (hu *hisUseCase) HistoryDHasilPemeriksaanFisioterapi(noReg string) (res []his.ResHasilFisioterapiMapper, err error) {
	radiologi, err := hu.hisRepository.GetHasilDPenunjangRadiologiRepository(noReg)

	if err != nil {
		return res, errors.New("Fisioterapi")
	}

	var data []his.ResHasilFisioterapiMapper

	if len(radiologi) > 0 {
		for i := 0; i <= len(radiologi)-1; i++ {

			if radiologi[i].KodePenunjang == "FIS001" {
				data = append(data, his.ResHasilFisioterapiMapper{
					Hasil:  radiologi[i].Hasil,
					Uraian: radiologi[i].Uraian,
				})
			}

		}
	}

	if len(radiologi) < 1 {
		return data, nil
	}

	return data, nil
}

func (hu *hisUseCase) HistoryDHasilPemeriksaanGizi(noReg string) (res []his.ResHasilFisioterapiMapper, err error) {
	radiologi, err := hu.hisRepository.GetHasilDPenunjangRadiologiRepository(noReg)

	if err != nil {
		return res, errors.New("GIzi")
	}

	var data []his.ResHasilFisioterapiMapper

	if len(radiologi) > 0 {
		for i := 0; i <= len(radiologi)-1; i++ {

			if radiologi[i].KodePenunjang == "GIZ001" {
				data = append(data, his.ResHasilFisioterapiMapper{
					Hasil:  radiologi[i].Hasil,
					Uraian: radiologi[i].Uraian,
				})
			}

		}
	}

	if len(radiologi) < 1 {
		return data, nil
	}

	return data, nil
}

func (hu *hisUseCase) HistoryPenMed(noReg string) (res []his.ResHasilDetailPenMed, err error) {
	penmed, _ := hu.hisRepository.GetHasilPenMed(noReg)
	var data []his.ResHasilDetailPenMed

	for i := 0; i <= len(penmed)-1; i++ {
		data = append(data, his.ResHasilDetailPenMed{
			NoPenmed:         penmed[i].NoPenmed,
			KdDokterPengirim: penmed[i].KdDokterPengirim,
			KetAsalPelayanan: penmed[i].KetAsalPelayanan,
			Bagian:           penmed[i].Bagian,
			Namadokter:       penmed[i].Namadokter,
		})

		detail, _ := hu.hisRepository.GetDetailHasilPenMed(penmed[i].NoPenmed, noReg)
		data[i].DetailHasilPenMedn = detail

	}

	return data, nil
}

func (hu *hisUseCase) HistoryPasien(noRM string, kdBagian string) (res []his.ResRiwayatDetailPasien, err error) {

	data, err := hu.hisRepository.GetDetailPasien(noRM, kdBagian)
	var history []his.ResRiwayatDetailPasien

	if err != nil {
		return res, nil
	}

	for i := 0; i <= len(data)-1; i++ {
		history = append(history, his.ResRiwayatDetailPasien{
			TglMasuk:               data[i].TglMasuk[0:10],
			Diagnosa:               data[i].Diagnosa,
			KeluhanUtama:           data[i].KeluhanUtama,
			RwtSekarang:            data[i].RwtSekarang,
			AsesmedRwtPenyKlrg:     data[i].AsesmedRwtPenyKlrg,
			AsesmedRwtAlergiDetail: data[i].AsesmedRwtAlergiDetail,
			AsesmedTerapi:          data[i].AsesmedTerapi,
			Noreg:                  data[i].Noreg,
			Id:                     data[i].Id,
		})
		labor, _ := hu.HistoryLaboratoriumUsecase(data[i].Noreg)
		penmed, _ := hu.HistoryPenMed(data[i].Noreg)
		history[i].ResHasilLabor = labor
		history[i].ResHasilDetailPenMed = penmed
	}

	if len(history) < 1 {
		return res, nil
	}

	return history, nil
}

func (hu *hisUseCase) InputPemeriksaanLaborUsecase(userID string, req dto.ReqInsertDetailLabor) (res his.InputLabResponse, err error) {
	// GET NOMOR LABOR
	labor, err := hu.hisRepository.GetNomorLabor(req.Noreg)
	times := time.Now()

	if err != nil {
		return res, errors.New(err.Error())
	}

	var total float64 = 0

	for i := 0; i <= len(req.DetailPemeriksaanLaborModel)-1; i++ {
		total = total + req.DetailPemeriksaanLaborModel[i].KtaripTotal.TaripKelas
	}

	if total > 0 {
		for i := 0; i <= len(req.DetailPemeriksaanLaborModel)-1; i++ {
			for _, V := range req.DetailPemeriksaanLaborModel[i].Pemeriksaan {
				// CREATE DATA
				dlabor := his.DHasilLaborModel{
					TipePemeriksaan:        his.NonCito,
					InsertDttm:             times.Format("2006-01-02 15:04:05"),
					InsertPc:               req.DeviceID,
					NoLab:                  "LAB" + req.Noreg + labor.Nomor,
					Noreg:                  req.Noreg,
					NameGrup:               V.NameGrup,
					Normal:                 V.Normal,
					Satuan:                 V.Satuan,
					Kode:                   V.Kode,
					Urut:                   V.Urut,
					InsertUserId:           userID,
					KdDrPengirim:           req.DokterPengirim,
					KetAsalPelayanan:       req.KetPoli,
					UmurPasien:             req.UmurPasien,
					Kelompok:               V.Kelompok,
					KodeBagian:             req.KodePoli,
					JamPengambilanSpesimen: times.Format("2006-01-02 15:04:05"),
					JamKirimSpesimen:       times.Format("2006-01-02 15:04:05"),
					KodeKelas:              req.KodeKelas,
				}

				hu.logging.Info(dlabor)
				_, err := hu.hisRepository.InsertLabor(dlabor)

				if err != nil {
					return res, errors.New(err.Error())
				}
			}
		}

		data := his.AntrianPasienModel{
			Mac:         req.DeviceID,
			UserId:      userID,
			Noreg:       req.Noreg,
			KdTujuan:    "LAB001",
			CodeFrom:    req.KodePoli,
			RecieveDttm: times.Format("2006-01-02 15:04:05"),
			KdDokter:    "",
			NoOrder:     "LAB" + req.Noreg + labor.Nomor,
		}

		// INPUT ANTRIAN LABOR
		_, err := hu.hisRepository.InsertAntrianPenmed(data)

		if err != nil {
			return res, errors.New(err.Error())
		}

		res.NoLab = "LAB" + req.Noreg + labor.Nomor

		return res, nil
	} else {
		return res, errors.New("tindakan tidak memiliki tarip")
	}

}

func (hu *hisUseCase) InputPemeriksaanRadiologiUseCase(userID string, req dto.ReqInsertDetailRadiologi) (res his.InputLabResponse, err error) {
	penmed, err := hu.hisRepository.GetNomorRadiologi(req.Noreg)

	times := time.Now()

	if err != nil {
		return res, errors.New(err.Error())
	}

	kdTujuan := []string{}
	penMedString := ""

	var total float64 = 0

	for i := 0; i <= len(req.DetailRadiologi)-1; i++ {
		// ==============================
		total = total + float64(req.DetailRadiologi[i].Tarip)

		// ==== FILTER LIST
		cari := searchInList(kdTujuan, req.DetailRadiologi[i].KdBag)

		if !cari {
			kdTujuan = append(kdTujuan, req.DetailRadiologi[i].KdBag)
		}

		hu.logging.Info("TARIP ")
		hu.logging.Info(total)
	}

	if total > 0 {

		if req.JenisPenunjang == "RADIOLOGI" {
			penMedString = "RAD" + req.Noreg + penmed.Nomor
		} else if req.JenisPenunjang == "GIZI" {
			penMedString = "GIZ" + req.Noreg + penmed.Nomor
		} else if req.JenisPenunjang == "FISIOTHERAPHY" {
			penMedString = "FIS" + req.Noreg + penmed.Nomor
		}

		// ================
		for i := 0; i <= len(req.DetailRadiologi)-1; i++ {
			dHasil := his.DHasilPenunjang{
				TipePemeriksaan:  his.NonCito,
				InsertDttm:       times.Format("2006-01-02 15:04:05"),
				InsertUserId:     userID,
				NoPenmed:         penMedString,
				KdDokterPengirim: req.DokterPengirim,
				KodePenunjang:    req.DetailRadiologi[i].KdBag,
				Kode:             req.DetailRadiologi[i].Kode,
				Noreg:            req.Noreg,
				KetAsalPelayanan: req.KetPoli,
				UmurPasien:       req.UmurPasien,
				KodeBagian:       req.KodePoli,
				KodeKelas:        req.KodeKelas,
			}

			_, err := hu.hisRepository.InsertRadiologi(dHasil)

			if err != nil {
				return res, errors.New(err.Error())
			}
		}

		for _, V := range kdTujuan {
			hu.logging.Info("INFO TUJUAN" + V)

			data := his.AntrianPasienModel{
				Mac:         req.DeviceID,
				UserId:      userID,
				Noreg:       req.Noreg,
				KdTujuan:    V,
				CodeFrom:    req.KodePoli,
				RecieveDttm: times.Format("2006-01-02 15:04:05"),
				KdDokter:    userID,
				NoOrder:     penMedString,
			}

			// INPUT ANTRIAN LABOR
			_, err := hu.hisRepository.InsertAntrianPenmed(data)

			if err != nil {
				return res, errors.New(err.Error())
			}
		}

		res.NoLab = penMedString
		return res, nil
	} else {
		hu.logging.Info(req)
		hu.logging.Info(total)
		return res, errors.New("tindakan tidak memiliki tarip")
	}

}

func searchInList(list []string, target string) bool {
	for _, str := range list {
		if str == target {
			return true // String found
		}
	}
	return false // String not found
}

func (hu *hisUseCase) MoveDokterToPengajarUseCase() (err error) {
	return nil
}
