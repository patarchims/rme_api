package dto

import (
	"hms_api/modules/igd"
	"hms_api/modules/lib"
)

type (
	ReqNoregV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	OnGetAsesmenIGD struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ResponseAsesmenIGD struct {
		Noreg                          string `json:"no_reg"`
		AseskepSistemMerokok           string `json:"sistem_merokok"`
		AseskepSistemMinumAlkohol      string `json:"sistem_minum_alkohol"`
		AseskepSistemSpikologis        string `json:"sistem_spikologis"`
		AseskepSistemGangguanJiwa      string `json:"sistem_gangguan_jiwa"`
		AseskepSistemBunuhDiri         string `json:"sistem_bunuh_diri"`
		AseskepSistemTraumaPsikis      string `json:"sistem_trauma_psikis"`
		AseskepSistemHambatanSosial    string `json:"sistem_hambatan_sosial"`
		AseskepSistemHambatanSpiritual string `json:"sistem_hambatan_spiritual"`
		AseskepSistemHambatanEkonomi   string `json:"sistem_hambatan_ekonomi"`
		AseskepSistemPenghasilan       string `json:"sistem_penghasilan"`
		AseskepSistemKultural          string `json:"sistem_kultural"`
		AseskepSistemAlatBantu         string `json:"sistem_alat_bantu"`
		AseskepSistemKebutuhanKhusus   string `json:"sistem_kebutuhan_khusus"`
	}

	ResponseAlergi struct {
		AlergiMakanan string `json:"alergi_makanan"`
		AlergiObat    string `json:"alergi_obat"`
		AlergiLainnya string `json:"alergi_lainnya"`
	}

	ResponsePegobatanDirumah struct {
		KdRiwayat              string `json:"kd_riwayat"`
		KdBagian               string `json:"kd_bagian"`
		Usia                   string `json:"usia"`
		Noreg                  string `json:"noreg"`
		NamaObat               string `json:"nama_obat"`
		Dosis                  string `json:"dosis"`
		CaraPemberian          string `json:"cara_pemberian"`
		Frekuensi              string `json:"frekuensi"`
		WaktuTerakhirPemberian string `json:"waktu_pemberian"`
	}

	ResponseTindakLajutIGD struct {
		TindakLanjut TindakLanjutResponse `json:"tindak_lanjut"`
		Dokter       []igd.KTaripDokter   `json:"dokter"`
		Pelayanan    []lib.KPelayanan     `json:"pelayanan"`
	}

	TindakLanjutResponse struct {
		TindakLanjut       string `json:"tindak_lanjut"`
		AlasanRujukan      string `json:"alasan_rujukan"`
		KondisiPasien      string `json:"kondisi_pasien"`
		Indikasi           string `json:"indikasi"`
		DPJP               string `json:"dpjp"`
		Ruangan            string `json:"ruangan"`
		KeteranganRujuk    string `json:"keterangan_rujuk"`
		TransportasiPulang string `json:"transportasi_pulang"`
	}

	ResponseResikoJatuhDewasaIGD struct {
		RiwayatJatuh      string `json:"riwayat_jatuh"`
		SkorRJatuh        int    `json:"skor_riwayat_jatuh"`
		Diagnosis         string `json:"diagnosis"`
		SkorDiagnosis     int    `json:"skor_diagnosis"`
		AlatBantuJalan    string `json:"alat_bantu_jalan"`
		SkorAlatBantu     int    `json:"skor_alat_bantu_jalan"`
		MenggunakanInfuse string `json:"menggunakan_infuse"`
		SkorInfuse        int    `json:"skor_menggunakan_infuse"`
		CaraBerjalan      string `json:"cara_berjalan"`
		SkorCaraBerjalan  int    `json:"skor_cara_berjalan"`
		StatusMental      string `json:"status_mental"`
		SkorStatusMental  int    `json:"skor_status_mental"`
		Total             int    `json:"total"`
	}

	ResponseResikoJatuhIGDDewasa struct {
		RiwayatJatuh      string `json:"riwayat_jatuh"`
		Diagnosis         string `json:"diagnosis"`
		AlatBantuJalan    string `json:"alat_bantu_jalan"`
		MenggunakanInfuse string `json:"menggunakan_infuse"`
		CaraBerjalan      string `json:"cara_berjalan"`
		StatusMental      string `json:"status_mental"`
		TotalSkor         int    `json:"total_skor"`
	}
)
