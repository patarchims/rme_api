package dto

import (
	"hms_api/modules/rme"
	"hms_api/modules/user"
	"time"
)

type (
	OnGetGeneralConsent struct {
		NORM      string `json:"no_rm" validate:"required"`
		KdBagian  string `json:"kd_bagian" validate:"omitempty"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
		Usia      string `json:"usia" validate:"omitempty"`
		NoReg     string `json:"no_reg" validate:"omitempty"`
	}

	OnGetGeneralConsentRanap struct {
		NORM            string `json:"no_rm" validate:"required"`
		KdBagian        string `json:"kd_bagian" validate:"omitempty"`
		NoReg           string `json:"no_reg" validate:"omitempty"`
		Pelayanan       string `json:"pelayanan" validate:"omitempty"`
		Usia            string `json:"usia" validate:"omitempty"`
		PJawabNama      string `json:"nama"`
		HubDenganPasien string `json:"hub_pasien"`
		PJawabAlamat    string `json:"alamat"`
		PJawabNoHP      string `json:"no_hp"`
		Pewenang        string `json:"pewenang"`
	}

	OnSaveGeneralConsentRanap struct {
		NORM            string `json:"no_rm" validate:"required"`
		KdBagian        string `json:"kd_bagian" validate:"omitempty"`
		NoReg           string `json:"no_reg" validate:"omitempty"`
		Pelayanan       string `json:"pelayanan" validate:"omitempty"`
		Usia            string `json:"usia" validate:"omitempty"`
		PJawabNama      string `json:"nama"`
		HubDenganPasien string `json:"hub_pasien"`
		PJawabAlamat    string `json:"alamat"`
		PJawabNoHP      string `json:"no_hp"`
		Pewenang        string `json:"pewenang"`
	}

	OnSaveRiwayatAlergi struct {
		Noreg         string `json:"no_reg" validate:"required"`
		KdBagian      string `json:"bagian" validate:"required"`
		Pelayanan     string `json:"pelayanan" validate:"required"`
		Usia          string `json:"usia" validate:"required"`
		RiwayatAlergi string `json:"riwayat_alergi" validate:"omitempty"`
		ReaksiAlergi  string `json:"reaksi_alergi" validate:"omitempty"`
	}

	OnGetTindakLanjut struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnGetAsesmenKeperawatanIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnGetAsesemenKeperawatanRANAP struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnReportAsesmenKeperawatanAnak struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
		NoRM      string `json:"no_rm" validate:"required"`
	}

	OnSaveAsesmenKeperawatanANAK struct {
		Noreg                   string `json:"no_reg" validate:"required"`
		KdBagian                string `json:"bagian" validate:"required"`
		Pelayanan               string `json:"pelayanan" validate:"required"`
		Usia                    string `json:"usia" validate:"required"`
		Pengkajian              string `json:"pengkajian" validate:"omitempty"`
		KeluhanUtama            string `json:"keluhan_utama" validate:"required"`
		RiwayatPenyakitSekarang string `json:"riwayat_penyakit_sekarang" validate:"required"`
		RiwayatPenyakitDahulu   string `json:"riwayat_penyakit_dahulu" validate:"required"`
		RiwayatPenyakitKeluarga string `json:"riwayat_penyakit_keluarga" validate:"required"`
	}

	OnSaveAsesmenKeperawatanIGD struct {
		Noreg                   string `json:"no_reg" validate:"required"`
		KdBagian                string `json:"bagian" validate:"required"`
		Pelayanan               string `json:"pelayanan" validate:"required"`
		Usia                    string `json:"usia" validate:"required"`
		Pengkajian              string `json:"pengkajian" validate:"omitempty"`
		CaraMasuk               string `json:"cara_masuk" validate:"omitempty"`
		AsalPasien              string `json:"asal_pasien" validate:"omitempty"`
		KeluhanUtama            string `json:"keluhan_utama" validate:"required"`
		RiwayatPenyakitSekarang string `json:"riwayat_penyakit_sekarang" validate:"required"`
		RiwayatPenyakitDahulu   string `json:"riwayat_penyakit_dahulu" validate:"required"`
		RiwayatPenyakitKeluarga string `json:"riwayat_penyakit_keluarga" validate:"required"`
	}

	OnGetReportAsesmenKeperawatanRANAP struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	OnSaveAsesmenKeperawatanRANAP struct {
		Noreg                   string `json:"no_reg" validate:"required"`
		KdBagian                string `json:"bagian" validate:"required"`
		Pelayanan               string `json:"pelayanan" validate:"required"`
		Usia                    string `json:"usia" validate:"required"`
		Pengkajian              string `json:"pengkajian" validate:"omitempty"`
		KeluhanUtama            string `json:"keluhan_utama" validate:"required"`
		RiwayatPenyakitSekarang string `json:"riwayat_penyakit_sekarang" validate:"required"`
		RiwayatPenyakitDahulu   string `json:"riwayat_penyakit_dahulu" validate:"required"`
		RiwayatPenyakitKeluarga string `json:"riwayat_penyakit_keluarga" validate:"required"`
	}

	OnSaveTindakLajutIGD struct {
		Noreg              string `json:"no_reg" validate:"required"`
		KdBagian           string `json:"bagian" validate:"required"`
		Pelayanan          string `json:"pelayanan" validate:"required"`
		Usia               string `json:"usia" validate:"required"`
		KeteranganRujuk    string `json:"keterangan_rujuk" validate:"omitempty"`
		TindakLanjut       string `json:"tindak_lanjut"`
		AlasanRujukan      string `json:"alasan_rujukan"`
		KondisiPasien      string `json:"kondisi_pasien"`
		TransportasiPulang string `json:"transportasi_pulang" validate:"omitempty"`
		Indikasi           string `json:"indikasi"`
		DPJP               string `json:"dpjp"`
		Ruangan            string `json:"ruangan"`
		DichartPlanning    string `json:"pendidikan_pasien"`
		PendidikanPasien   string `json:"discharge_planning"`
	}

	OnSaveGeneralConsent struct {
		NORM           string `json:"no_rm" validate:"required"`
		KdBagian       string `json:"kd_bagian" validate:"omitempty"`
		Pelayanan      string `json:"pelayanan" validate:"omitempty"`
		PJawabNama     string `json:"nama"`
		PJawabAlamat   string `json:"alamat"`
		PJawabTglLahir string `json:"tgl_lahir"`
		PJawabNoHP     string `json:"no_hp"`
		Pewenang       string `json:"pewenang"`
	}

	ReportGeneralConsent struct {
		NORM     string `json:"no_rm" validate:"required"`
		KdBagian string `json:"kd_bagian" validate:"omitempty"`
	}

	ResponseGeneralConsent struct {
		ProfilePasien user.ProfilePasien               `json:"pasien"`
		PNangungJawab MapperGeneralConsent             `json:"penangung_jawab"`
		Pengkajian    ResponsePengkajianKeperawatanIGD `json:"pengkajian"`
	}

	ResponseGeneralConsentInap struct {
		ProfilePasien user.ProfilePasien   `json:"pasien"`
		PNangungJawab MapperGeneralConsent `json:"penangung_jawab"`
		DokterAsesmen rme.AsesemenMedisIGD `json:"asesmen_dokter"`
	}

	ResponseReportGeneralConsent struct {
		ProfilePasien user.ProfilePasien   `json:"pasien"`
		PNangungJawab MapperGeneralConsent `json:"penangung_jawab"`
		Perawat       user.Karyawan        `json:"perawat"`
	}

	MapperGeneralConsent struct {
		InsertDttm     time.Time `json:"insert_dttm"`
		PJawabNama     string    `json:"nama"`
		PJawabAlamat   string    `json:"alamat"`
		PJawabTglLahir string    `json:"tgl_lahir"`
		PJawabNoHP     string    `json:"no_hp"`
		Pewenang       string    `json:"pewenang"`
		HubPasien      string    `json:"hub_pasien"`
	}
)
