package dto

import (
	"hms_api/modules/igd"
	DTONyeri "hms_api/modules/nyeri/dto"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"

	// rmeDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/user"
	"time"
)

type (
	OnGetPengkajianPersistemIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnReportPengkajianPersistemIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		NoRM      string `json:"no_rm" validate:"omitempty"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnGetPengobatanDirumahIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnReportPengkajianDewasaIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnGetPengkajianIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
	}

	OnDeteksiResikoJatuh struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	OnGetResikoJatuhPasienDewasa struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Kategori  string `json:"kategori" validate:"required"`
	}

	OnSavePengkajianResikoJatuhPasienDewasaIGD struct {
		Noreg             string `json:"no_reg" validate:"required"`
		KdBagian          string `json:"bagian" validate:"required"`
		Pelayanan         string `json:"pelayanan" validate:"required"`
		Kategori          string `json:"kategori" validate:"required"`
		RiwayatJatuh      string `json:"riwayat_jatuh"`
		Diagnosis         string `json:"diagnosis"`
		AlatBantuJalan    string `json:"alat_bantu_jalan"`
		MenggunakanInfuse string `json:"menggunakan_infuse"`
		CaraBerjalan      string `json:"cara_berjalan"`
		StatusMental      string `json:"status_mental"`
		InsertDevice      string `json:"device_id"`
		Total             int    `json:"total"`
	}

	OnSavePengkajianNutrisiDewasaIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
		N1        string `json:"n1"`
		N2        string `json:"n2"`
		Nilai     int    `json:"nilai"`
		DevicesID string `json:"device"`
	}

	OnSavePengkajianNutrisiIGD struct {
		Noreg     string `json:"no_reg" validate:"required"`
		KdBagian  string `json:"bagian" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Usia      string `json:"usia" validate:"required"`
		N1        string `json:"n1"`
		N2        string `json:"n2"`
		N3        string `json:"n3"`
		N4        string `json:"n4"`
		Nilai     int    `json:"nilai"`
		DevicesID string `json:"device"`
	}

	OnSavePengobatanDirumahIGD struct {
		Noreg             string `json:"no_reg" validate:"required"`
		KdBagian          string `json:"bagian" validate:"required"`
		Pelayanan         string `json:"pelayanan" validate:"required"`
		Usia              string `json:"usia" validate:"required"`
		RiwayatPengobatan string `json:"riwayat_pengobatan" validate:"required"`
	}

	OnSavePengkajianKehamilanAnak struct {
		Noreg            string `json:"no_reg" validate:"required"`
		KdBagian         string `json:"bagian" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
		Usia             string `json:"usia" validate:"required"`
		RiwayatKehamilan string `json:"riwayat_kehamilan" validate:"required"`
	}

	OnSaveNyeriIGD struct {
		Noreg      string `json:"no_reg" validate:"required"`
		KdBagian   string `json:"bagian" validate:"required"`
		Pelayanan  string `json:"pelayanan" validate:"required"`
		Usia       string `json:"usia" validate:"required"`
		Nyeri      string `json:"nyeri" validate:"required"`
		Metode     string `json:"metode"`
		Asesmen    string `json:"asesmen"`
		SkorNyeri  int    `json:"skor_nyeri"`
		HasilNyeri int    `json:"hasil_nyeri"`
		WaktuKaji  int    `json:"waktu_nyeri"`
		Penyebab   string `json:"penyebab"`
		Kualitas   string `json:"kualitas"`
		Penyebaran string `json:"penyebaran"`
		Keparahan  string `json:"keparahan"`
		Waktu      string `json:"waktu"`
		Lokasi     string `json:"lokasi"`
	}

	OnSavePengkajianPersistemIGD struct {
		Noreg               string `json:"no_reg" validate:"required"`
		KdBagian            string `json:"bagian" validate:"required"`
		Pelayanan           string `json:"pelayanan" validate:"required"`
		Usia                string `json:"usia" validate:"required"`
		Airways             string `json:"airways"`
		Breathing           string `json:"breathing"`
		Circulation         string `json:"circulation"`
		Disability          string `json:"disability"`
		SistemPerkemihan    string `json:"sistem_perkemihan"`
		SistemPencernaan    string `json:"sistem_pencernaan"`
		SistemIntegumen     string `json:"sistem_integumen"`
		KebidananKandungan  string `json:"sistem_kebidanan"`
		THTMATA             string `json:"sistem_tht"`
		MetaBolisme         string `json:"sistem_metabolisme"`
		PsikiatriPsikologis string `json:"sistem_psikologis"`
		SosioKulural        string `json:"sistem_sosio_kultural"`
	}

	ResponsePengkajianPersistemIGD struct {
		NoReg               string `json:"no_reg"`
		Airways             string `json:"airways"`
		Breathing           string `json:"breathing"`
		Circulation         string `json:"circulation"`
		Disability          string `json:"disability"`
		SistemPerkemihan    string `json:"sistem_perkemihan"`
		SistemPencernaan    string `json:"sistem_pencernaan"`
		SistemIntegumen     string `json:"sistem_integumen"`
		KebidananKandungan  string `json:"sistem_kebidanan"`
		THTMATA             string `json:"sistem_tht"`
		MetaBolisme         string `json:"sistem_metabolisme"`
		PsikiatriPsikologis string `json:"sistem_psikologis"`
		SosioKulural        string `json:"sistem_sosio_kultural"`
	}

	ResponsePersistemAnak struct {
		NoReg                  string `json:"no_reg"`
		EliminasiPelepasan     string `json:"eliminasi_pelepasan"`
		AktivitasIstirahat     string `json:"aktivitas_istirahat"`
		SistemKardio           string `json:"sistem_kardio"`
		SistemRespiratori      string `json:"sistem_repiratori"`
		SistemPerfusiSecebral  string `json:"sistem_perfusi_secebral"`
		SistemThermoregulasi   string `json:"sistem_thermoregulasi"`
		SistemKomunikasi       string `json:"sistem_komunikasi"`
		SistemDataPsikologis   string `json:"sistem_data_psikologis"`
		SistemNilaiKepercayaan string `json:"sistem_nilai_keperacayaan"`
	}

	ResponseDeteksiResikoJatuhIGD struct {
		NoReg      string `json:"noreg"`
		IsShow     bool   `json:"is_show"`
		Keterangan string `json:"keterangan"`
		Total      int    `json:"total"`
	}

	ResponseCPPT struct {
		InsertDttm    time.Time            `json:"insert_dttm"`
		InsertUserID  string               `json:"user_id"`
		Noreg         string               `json:"noreg"`
		Subjektif     string               `json:"subjektif"`
		Objektif      string               `json:"objektif"`
		Plan          string               `json:"plan"`
		Situation     string               `json:"situation"`
		Background    string               `json:"background"`
		Recomendation string               `json:"recomendation"`
		Asesmen       string               `json:"asesmen"`
		Perawat       rme.UserPerawatModel `json:"perawat"`
	}

	ResponseReportPengkajianIGDANAK struct {
		Persistem         ResponsePengkajianPersistemIGD         `json:"persistem"`
		Pengkajian        ResponsePengkajianKeperawatanIGD       `json:"pengkajian"`
		AsuhanKeperawatan []rme.DasKepDiagnosaModelV2            `json:"asuhan_keperawatan"`
		Cppt              []ResponseCPPT                         `json:"cppt"`
		PengobatanDirumah []ResponsePegobatanDirumah             `json:"pengobatan_dirumah"`
		PengkajianNyeri   DTONyeri.ToResponsePengkajianAwalNyeri `json:"nyeri"`
		PengkajianNutrisi DTONyeri.ToResponsePengkajianNutrisi   `json:"nutrisi"`
		Alergi            ResponseAlergi                         `json:"alergi"`
		ResikoJatuh       ResponseResikoJatuh                    `json:"resiko_jatuh"`
		Perawat           user.UserPerawat                       `json:"perawat"`
		Fisik             dto.ResponseIGD                        `json:"fisik"`
		VitalSign         soap.DVitalSignIGDDokter               `json:"vital_sign"`
		Observasi         ResponseObseVasi                       `json:"observasi"`
	}

	ResponsePengkajianKeperawatanIGDDewasa struct {
		Persistem         ResponsePengkajianPersistemIGD             `json:"persistem"`
		Pengkajian        ResponsePengkajianKeperawatanIGD           `json:"pengkajian"`
		AsuhanKeperawatan []rme.DasKepDiagnosaModelV2                `json:"asuhan_keperawatan"`
		Cppt              []ResponseCPPT                             `json:"cppt"`
		PengobatanDirumah []ResponsePegobatanDirumah                 `json:"pengobatan_dirumah"`
		PengkajianNyeri   DTONyeri.ToResponsePengkajianAwalNyeri     `json:"nyeri"`
		ResikoJatuh       ResponseResikoJatuhDewasaIGD               `json:"resiko_jatuh"`
		NutrisiDewasa     DTONyeri.ToResponsePengkajianNutrisiDewasa `json:"nutrisi"`
		Perawat           user.UserPerawat                           `json:"perawat"`
		Fisik             dto.ResponseIGD                            `json:"fisik"`
		VitalSign         soap.DVitalSignIGDDokter                   `json:"vital_sign"`
		Observasi         ResponseObseVasi                           `json:"observasi"`
	}

	ResponsePengkajianKeperawatanRawatInapAnak struct {
		Pengkajian ResponsePengkajianKeperawatan         `json:"pengkajian"`
		Pengobatan []igd.DriwayatPengobatanDirumah       `json:"pengobatan"`
		Alergi     ResponseAlergi                        `json:"alergi"`
		Nutrisi    DTONyeri.ToResponsePengkajianNutrisi  `json:"nutrisi"`
		Persistem  igd.ResponseDPengkajianPersistenRANAP `json:"persistem"`
		Asuhan     []rme.DasKepDiagnosaModelV2           `json:"asuhan"`
		Fisik      dto.ResponseIGD                       `json:"fisik"`
		Vital      soap.DVitalSignIGDDokter              `json:"vital"`
	}

	ResponseObseVasi struct {
		TD         string `json:"td"`
		Nadi       string `json:"nadi"`
		Pernapasan string `json:"pernapasan"`
		Suhu       string `json:"suhu"`
		Kesadaran  string `json:"kesadaran"`
		GCSE       string `json:"gcs_e"`
		GCSV       string `json:"gcs_v"`
		GCSM       string `json:"gcs_m"`
	}

	ResponseReportPengkajianDewasaIGD struct {
		Persistem         ResponsePengkajianPersistemIGD   `json:"persistem"`
		Pengkajian        ResponsePengkajianKeperawatanIGD `json:"pengkajian"`
		AsuhanKeperawatan []rme.DasKepDiagnosaModelV2      `json:"asuhan_keperawatan"`
		Cppt              []ResponseCPPT                   `json:"cppt"`
	}

	ResponsePengkajianIGD struct {
		Pengkajian        ResponsePengkajianKeperawatanIGD `json:"pengkajian"`
		PengobatanDirumah []ResponsePegobatanDirumah       `json:"pengobatan_dirumah"`
	}

	ResponsePengkajianAlergiIGD struct {
		Pengkajian ResponsePengkajianKeperawatanIGD `json:"pengkajian"`
	}

	ResponsePengkajianNyeriIGD struct {
		Pengkajian ResponsePengkajianKeperawatanIGD       `json:"pengkajian"`
		Nyeri      DTONyeri.ToResponsePengkajianAwalNyeri `json:"nyeri"`
	}

	ResponseSavePengkajianKeperawatanAnak struct {
		KdBagian                 string `json:"kd_bagian"`
		Noreg                    string `json:"noreg"`
		Usia                     string `json:"usia"`
		Pelayanan                string `json:"pelayanan"`
		Anamnesa                 string `json:"anamnesa"`
		CaraMasuk                string `json:"cara_masuk"`
		AsalPasien               string `json:"asal_pasien"`
		KeluhanUtama             string `json:"keluhan_utama"`
		RiwayatPenyakitSekarang  string `json:"riwayat_penyakit_sekarang"`
		RiwayatPenyakitDahulu    string `json:"riwayat_penyakit_dahulu"`
		RiwayatPenyakitKeluarga  string `json:"riwayat_penyakit_keluarga"`
		RiwayatPengobatanDirumah string `json:"riwayat_pengobatan_dirumah"`
		RiwayatAlergi            string `json:"riwayat_alergi"`
		ReaksiAlergi             string `json:"reaksi_alergi"`
		Nyeri                    string `json:"nyeri"`
	}

	ResponsePengkajianKeperawatanIGD struct {
		InsertDttm               time.Time `json:"insert_dttm"`
		KdBagian                 string    `json:"kd_bagian"`
		Noreg                    string    `json:"noreg"`
		Usia                     string    `json:"usia"`
		Pelayanan                string    `json:"pelayanan"`
		Anamnesa                 string    `json:"anamnesa"`
		CaraMasuk                string    `json:"cara_masuk"`
		AsalPasien               string    `json:"asal_masuk"`
		KeluhanUtama             string    `json:"keluhan_utama"`
		RiwayatPenyakitSekarang  string    `json:"riwayat_penyakit_sekarang"`
		RiwayatPenyakitDahulu    string    `json:"riwayat_penyakit_dahulu"`
		RiwayatPenyakitKeluarga  string    `json:"riwayat_penhyakit_keluarga"`
		RiwayatPengobatanDirumah string    `json:"riwayat_pengobatan_dirumah"`
		RiwayatAlergi            string    `json:"riwayat_alergi"`
		ReaksiAlergi             string    `json:"reaksi_alergi"`
		Nyeri                    string    `json:"nyeri"`
		RiwayatKehamilan         string    `json:"riwayat_kehamilan"`
		TindakLanjut             string    `json:"tindak_lanjut"`
		AlasanRujukan            string    `json:"alasan_rujuk"`
		KeteranganRujuk          string    `json:"keterangan_rujuk"`
		KondisiPasien            string    `json:"kondisi_pasien"`
		Indikasi                 string    `json:"indikasi"`
		TransportasiPulang       string    `json:"transportasi_pulang"`
		PendidikanPasien         string    `json:"pendidikan_pasien"`
		DischargePlanning        string    `json:"discharge_planning"`
		Dpjp                     string    `json:"dpjp"`
		Ruangan                  string    `json:"ruangan"`
	}

	ResponsePengkajianKeperawatan struct {
		InsertDttm               time.Time `json:"insert_dttm"`
		Noreg                    string    `json:"noreg"`
		Usia                     string    `json:"usia"`
		Pelayanan                string    `json:"pelayanan"`
		Anamnesa                 string    `json:"anamnesa"`
		KeluhanUtama             string    `json:"keluhan_utama"`
		RiwayatPenyakitSekarang  string    `json:"riwayat_penyakit_sekarang"`
		RiwayatPenyakitDahulu    string    `json:"riwayat_penyakit_dahulu"`
		RiwayatPenyakitKeluarga  string    `json:"riwayat_penhyakit_keluarga"`
		RiwayatPengobatanDirumah string    `json:"riwayat_pengobatan_dirumah"`
		RiwayatAlergi            string    `json:"riwayat_alergi"`
		ReaksiAlergi             string    `json:"reaksi_alergi"`
		Nyeri                    string    `json:"nyeri"`
		RiwayatKehamilan         string    `json:"riwayat_kehamilan"`
	}

	// RESPONSE TINDAK LANJUT IGD
	ResponseTindakLanjutIGD struct {
		TindakLanjut  string `json:"tindak_lanjut"`
		AlasanRujukan string `json:"alasan_rujukan"`
		KondisiPasien string `json:"kondisi_pasien"`
		Indikasi      string `json:"indikasi"`
		Dpjp          string `json:"dpjp"`
		Ruangan       string `json:"ruangan"`
	}

	ResponseResikoJatuh struct {
		Umur             string `json:"umur"`
		JenisKelamin     string `json:"jenis_kelamin"`
		Diagnosa         string `json:"dignosa"`
		GangguanKognitif string `json:"gangguan_kognitif"`
		FaktorLingkungan string `json:"faktor_lingkungan"`
		Response         string `json:"response"`
		PenggunaanObat   string `json:"penggunaan_obat"`
		Total            int    `json:"total"`
	}

	ResponsePengkajianKeperawatanAnak struct {
		Pelayanan               string `json:"pelayanan"`
		Anamnesa                string `json:"anamnesa"`
		CaraMasuk               string `json:"cara_masuk"`
		AsalPasien              string `json:"asal_pasien"`
		KeluhanUtama            string `json:"keluhan_utama"`
		RiwayatPenyakitSekarang string `json:"riwayat_penyakit_sekarang"`
		RiwayatPenyakitDahulu   string `json:"riwayat_penyakit_dahulu"`
		RiwayatPenyakitKeluarga string `json:"riwayat_penyakit_keluarga"`
	}

	ResponsePengkajianKeperawatanRANAP struct {
		Pelayanan               string `json:"pelayanan"`
		Anamnesa                string `json:"anamnesa"`
		KeluhanUtama            string `json:"keluhan_utama"`
		RiwayatPenyakitSekarang string `json:"riwayat_penyakit_sekarang"`
		RiwayatPenyakitDahulu   string `json:"riwayat_penyakit_dahulu"`
		RiwayatPenyakitKeluarga string `json:"riwayat_penyakit_keluarga"`
	}
)
