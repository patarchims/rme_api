package entity

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"hms_api/modules/lib"
	DTONyeri "hms_api/modules/nyeri/dto"
	"hms_api/modules/rme"
	RMEDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	SOAPDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type IGDMapper interface {
	ToResponsePersistemRANAP(data igd.DpengkajianPersistem) (res igd.ResponsePersistemRANAP)
	ToResponseAsesemenRANAP(data igd.PengkajianKeperawatan, asesmenIGD igd.AsesmenDokter) (res dto.ResponsePengkajianKeperawatanRANAP)
	ToResponseAsesmen(data igd.PengkajianKeperawatan, asesmenDokter igd.AsesmenDokter) dto.ResponsePengkajianKeperawatanAnak
	ToMappingPengkajianAlergi(sistem igd.PengkajianKeperawatan) (res dto.ResponsePengkajianAlergiIGD)
	ToMappingGeneralConsent(userProfile user.ProfilePasien, general igd.DGeneralConsent, pengkajian igd.PengkajianKeperawatan) (res dto.ResponseGeneralConsent)
	ToMappingGeneralConsentRanap(userProfile user.ProfilePasien, general igd.DGeneralConsent, dokterAsesmen rme.AsesemenMedisIGD, pengkajian igd.PengkajianKeperawatan) (res dto.ResponseGeneralConsentInap)
	TOMapperResponsePengkajianNyeriIGD(Pengkajian igd.PengkajianKeperawatan, nyeri DTONyeri.ToResponsePengkajianAwalNyeri) (res dto.ResponsePengkajianNyeriIGD)
	ToMappingAsesmenKeperawatanIGD(pengkajian igd.PengkajianKeperawatan, riwayat []igd.DriwayatPengobatanDirumah) (res dto.ResponsePengkajianIGD)
	ToMappingPengkjianPersistemIGD(data igd.PengkjianPersistemIGD) (res dto.ResponseAsesmenIGD)
	ToMappingPengkajianPersistemIGD(persistem igd.DpengkajianPersistem, vital SOAPDTO.TandaVitalIGDResponse) (res dto.ResponsePengkajianPersistemIGD)
	ToMappingReportPengkajianPersistem(sistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT, pengobatan []igd.DriwayatPengobatanDirumah, nyeri DTONyeri.ToResponsePengkajianAwalNyeri, mapperNutrisi DTONyeri.ToResponsePengkajianNutrisi, alergi []rme.DAlergi, resikoJatuh igd.DRisikoJatuh, perawat user.UserPerawat, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponseReportPengkajianIGDANAK)
	ToMappingReportPengkajianIGDDewasa(sistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT) (res dto.ResponseReportPengkajianDewasaIGD)
	ToMappingCPPT(cppt []igd.DCPPT) (res []dto.ResponseCPPT)
	ToMappingReportGeneralConsent(pasienMapper user.ProfilePasien, general igd.DGeneralConsent, karyawan user.Karyawan) (res dto.ResponseReportGeneralConsent)

	// ============
	ToMappingResponseTindakLanjutIGD(data igd.PengkajianKeperawatan, dokter []igd.KTaripDokter, pelayanan []lib.KPelayanan) (res dto.ResponseTindakLajutIGD)
	ToResponsePengkajianResikoJatuhIGDDewasa(data igd.DRisikoJatuh) (res dto.ResponseResikoJatuhDewasaIGD)

	ToResponseReportPengkajianKeperawatanIGDDewasa(persistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT, pengobatan []igd.DriwayatPengobatanDirumah, nyeri DTONyeri.ToResponsePengkajianAwalNyeri, resiko dto.ResponseResikoJatuhDewasaIGD, nutrisi DTONyeri.ToResponsePengkajianNutrisiDewasa, perawat user.UserPerawat, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponsePengkajianKeperawatanIGDDewasa)
	ToResponsePengkajianDewasaReport(data igd.DRisikoJatuh) (res dto.ResponseResikoJatuhDewasaIGD)
	ToMappingPengkajianRawatInapAnak(data igd.PengkajianKeperawatan, pengobatan []igd.DriwayatPengobatanDirumah, alergi []rme.DAlergi, mapperNutrisi DTONyeri.ToResponsePengkajianNutrisi, persistem igd.DpengkajianPersistem, asuhan []rme.DasKepDiagnosaModelV2, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponsePengkajianKeperawatanRawatInapAnak)
	ToMappingDeteksiResikoJatuh(data igd.DeteksiResikoJatuhResponse) (res dto.ResponseDeteksiResikoJatuhIGD)
}

type IGDRepository interface {
	OnGetDeteksiResikoJatuhRespository(noReg string, kdBagian string) (res igd.DeteksiResikoJatuhResponse, err error)
	OnGetGeneralConsentRAJALRepository(noRM string, kdBagian string) (res igd.DGeneralConsent, err error)
	OnGetResikoJatuhRANA_DEWASA_Repository(kdBagian string, noReg string) (res igd.DRisikoJatuh, err error)
	OnGetPengkajianKeperawatanIGDDewasaRepository(kdBagian string, pelayanan string, usia string, noReg string) (res igd.PengkajianKeperawatan, err error)
	OnGetGeneralConsentRANAP(noRM string, kdBagian string) (res igd.DGeneralConsent, err error)
	OnSaveGeneralConsentRepository(data igd.DGeneralConsent) (res igd.DGeneralConsent, err error)
	OnUpdatgeGeneralConsentRepository(data igd.DGeneralConsent, noRm string, kdBagian string) (res igd.DGeneralConsent, err error)
	OnGetAsesmenDokterRANAPRepository(noReg string, kdBagian string) (res igd.AsesmenDokter, err error)
	OnGetPengkajianKeperawatanRepository(kdBagian string, pelayanan string, noReg string) (res igd.PengkajianKeperawatan, err error)
	OnGetPengkajianPersistemIGDRepository(kdBagian string, noReg string, usia string, pelayanan string) (res igd.DpengkajianPersistem, err error)
	OnGeAsesmenDokterRepository(noReg string, kdBagian string) (res igd.AsesmenDokter, err error)
	OnGetCPPPTRepository(noReg string, kdBagian string) (res []igd.DCPPT, err error)
	OnGetRiwayatPengobatanDiRumahRepository(usia string, kdBagian string, noReg string) (res []igd.DriwayatPengobatanDirumah, err error)
	OnGetDResikoJatuhIGDRajalRepositroy(kdBagian string, noReg string, person string) (res igd.DRisikoJatuh, err error)
	GetPengkajianKeperawatanRepository(kdBagian string, pelayanan string, usia string, noReg string) (res igd.PengkajianKeperawatan, err error)
	OnSavePengkajianKeperawatanRespository(data igd.PengkajianKeperawatan) (res igd.PengkajianKeperawatan, err error)
	OnUpdatePengkajianKeperawatanRepository(data igd.PengkajianKeperawatan, noReg string, usia string, kdBagian string, pelayann string) (res igd.PengkajianKeperawatan, err error)
	OnGetResikoJatuhIGDDewasaRepository(pelayanan string, kdBagian string, noReg string) (res igd.DRisikoJatuh, err error)
	OnSavePengkajianPersisetmIGDRepository(req dto.OnSavePengkajianPersistemIGD) (res igd.DpengkajianPersistem, err error)
	OnUpdateDataPengkajianPersistemIGDRepository(noReg string, kdBagian string, pelayanan string, usia string, data igd.DpengkajianPersistem) (res igd.DpengkajianPersistem, err error)
	OnSaveResikoJatuhIGDDewasaRepository(data igd.DRisikoJatuh) (res igd.DRisikoJatuh, err error)
	OnUpdateResikoJatuhIGDDewasaRepository(data igd.DRisikoJatuh, kdBagian string, noReg string, kategori string) (res igd.DRisikoJatuh, err error)
	OnGeAsesmenDokter(noReg string, kdBagian string) (res igd.AsesmenDokter, err error)
	OnGetAllDPJPRepository() (res []igd.KTaripDokter, err error)
	ON_GET_RESIKO_JATUH_IGD_DEWASA_REPOSITORY(NoReg string, KDBagian string) (res igd.DRisikoJatuh, err error)

	OnGetResikoJatuhRANA_ANAK_Repository(kdBagian string, noReg string) (res igd.DRisikoJatuh, err error)
}

type IGDUseCase interface {
	OnDeteksiResikoJatuhIGDUseCase(req dto.OnDeteksiResikoJatuh) (res dto.ResponseDeteksiResikoJatuhIGD)
	OnGetReportPengkajianKeperawatanInapAnakUseCase(req dto.OnReportAsesmenKeperawatanAnak) (res dto.ResponsePengkajianKeperawatanRawatInapAnak)
	// ====
	OnSaveAsesmenRANAPKeperawatanUseCase(req dto.OnSaveAsesmenKeperawatanRANAP, userID string) (message string, err error)
	OnGetAsesmenKeperawatanRanapUseCase(req dto.OnGetAsesemenKeperawatanRANAP) (res dto.ResponsePengkajianKeperawatanRANAP)
	OnSaveGenaralConsentRANAPUseCase(req dto.OnSaveGeneralConsentRanap, userID string) (res dto.ResponseGeneralConsent, message string, err error)
	OnGetGeneralConsentRANAPUseCase(req dto.OnGetGeneralConsentRanap) (res dto.ResponseGeneralConsentInap)
	OnGetPengkajianIGDDewasaUseCase(req dto.OnReportPengkajianPersistemIGD) (res dto.ResponsePengkajianKeperawatanIGDDewasa, err error)
	OnSaveResikoJatuhPasienDewasaIGDUseCase(req dto.OnSavePengkajianResikoJatuhPasienDewasaIGD, userID string) (message string, err error)
	OnGetResikoJatuhPasienDewasaIGDUseCase(req dto.OnGetResikoJatuhPasienDewasa) (res dto.ResponseResikoJatuhDewasaIGD, err error)
	OnSavePengkajianNutrisiDewasaIGDUseCase(req dto.OnSavePengkajianNutrisiDewasaIGD, userID string, deviceID string) (res DTONyeri.ToResponsePengkajianNutrisi, message string, err error)
	OnGetPengkajianNutrisiDewasaIGDUseCase(req dto.OnGetPengkajianIGD) (res DTONyeri.ToResponsePengkajianNutrisiDewasa, err error)
	OnSaveAsesmenKeperawatanUseCase(req dto.OnSaveAsesmenKeperawatanIGD, userID string) (message string, err error)
	OnSaveAsesmenKeperawatanANAKRanapUseCase(req dto.OnSaveAsesmenKeperawatanANAK, userID string) (message string, err error)

	OnGetAsesmenKeperawatanUseCase(req dto.OnGetAsesmenKeperawatanIGD) (res dto.ResponsePengkajianKeperawatanAnak)
	OnSaveTindakLanjutUseCase(req dto.OnSaveTindakLajutIGD, userID string) (message string, err error)
	OnGetKondisiTindakLanjutUseCase(req dto.OnGetTindakLanjut) (res dto.ResponseTindakLajutIGD)
	OnSavePengkajianAlergiAnakIGDAnakUseCase(req dto.OnSaveRiwayatAlergi, userID string) (message string, err error)
	OnReportGeneralCOnsentUseCase(req dto.ReportGeneralConsent) (res dto.ResponseReportGeneralConsent)
	OnGetGeneralConsentUseCase(req dto.OnGetGeneralConsent) (res dto.ResponseGeneralConsent)
	OnSaveGeneralConsentUseCase(req dto.OnSaveGeneralConsent, userID string) (res dto.ResponseGeneralConsent, message string, err error)

	// ===== //
	OnGetPengkajianALergiIGDUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianAlergiIGD)
	OnSavePengkajianKehamilanAnakUseCase(req dto.OnSavePengkajianKehamilanAnak, userID string) (message string, err error)
	OnSavePengkajianNutrisiIGDUseCase(req dto.OnSavePengkajianNutrisiIGD, userID string, deviceID string) (res DTONyeri.ToResponsePengkajianNutrisi, message string, err error)
	OnGetPengkajianNutrisiIGDUseCase(req dto.OnGetPengkajianIGD) (res DTONyeri.ToResponsePengkajianNutrisi, err error)
	OnGetPengkajianNyeriIGDAnakUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianNyeriIGD, err error)
	OnSavePengkajianNyeriIGDUseCase(req dto.OnSaveNyeriIGD, userID string) (message string, err error)
	OnSavePengobatanDiRumahIGDUseCase(req dto.OnSavePengobatanDirumahIGD, userID string) (message string, err error)
	OnReportPengkajianKeperawatanIGDUDewasaUseCase(req dto.OnReportPengkajianDewasaIGD) (res dto.ResponseReportPengkajianDewasaIGD, err error)
	OnGetReportPengkajianPersistemIGDUsecase(req dto.OnReportPengkajianPersistemIGD) (res dto.ResponseReportPengkajianIGDANAK, err error)
	OnSavePengkajianPersistemIGDUseCase(req dto.OnSavePengkajianPersistemIGD) (res igd.DpengkajianPersistem, message string, err error)
	OnGetPengkajianIGDAnakUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianIGD, err error)
}
