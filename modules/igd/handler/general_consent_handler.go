package handler

import (
	"fmt"
	"hms_api/modules/igd/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (ig *IGDHandler) OnGetGeneralConsentIGDFiberHandlerRANAP(c *fiber.Ctx) error {
	payload := new(dto.OnGetGeneralConsentRanap)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetGeneralConsentRANAPUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveGeneralConsentIGDRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveGeneralConsentRanap)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	res, message, er123 := ig.IGDUseCase.OnSaveGenaralConsentRANAPUseCase(*payload, userID)

	if er123 != nil {
		ig.Logging.Error(er123.Error())
		response := helper.APIResponse(message, http.StatusCreated, er123.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetGeneralConsentIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetGeneralConsent)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	ig.Logging.Info("GENERAL CONSENT")
	ig.Logging.Info(*payload)

	data := ig.IGDUseCase.OnGetGeneralConsentUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnReportGeneralConsentIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReportGeneralConsent)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnReportGeneralCOnsentUseCase(*payload)

	response := helper.APIResponse("message", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveGeneralConsentIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveGeneralConsent)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	data, message, er11 := ig.IGDUseCase.OnSaveGeneralConsentUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
