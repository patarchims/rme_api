package handler

import (
	"hms_api/modules/igd/dto"
	"hms_api/modules/igd/entity"
	soapEntity "hms_api/modules/soap/entity"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type IGDHandler struct {
	IGDMapper     entity.IGDMapper
	IGDUseCase    entity.IGDUseCase
	IGDRepository entity.IGDRepository
	Logging       *logrus.Logger
	SoapUseCase   soapEntity.SoapUseCase
}

func (ig *IGDHandler) OnGetAsesmenIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetAsesmenIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ig.IGDRepository.OnGetAsesmenIGDRepository(modulID, payload.Noreg, payload.Person, *payload)
	// ig.Logging.Info(modulID)
	// userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	ig.Logging.Info(modulID)

	// res, er113 := ig.

	// res, err12 := ig.IGDRepository.OnGetAsesmenIGDRepository(modulID, payload.Noreg, payload.Person, *payload)

	// if err12 != nil {
	// 	response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// ig.Logging.Info(res)
	// // mapper := ig.IGDMapper.ToMappingPengkjianPersistemIGD(res)

	response := helper.APIResponse("OK", http.StatusOK, "mapper")
	return c.Status(fiber.StatusOK).JSON(response)
}
