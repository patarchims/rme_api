package handler

import (
	"fmt"
	"hms_api/modules/igd/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (ig *IGDHandler) OnGetPengkajianKeperawatanAnakRepository(c *fiber.Ctx) error {
	payload := new(dto.OnReportPengkajianPersistemIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetReportPengkajianPersistemIGDUsecase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianAnakIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReportPengkajianPersistemIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetReportPengkajianPersistemIGDUsecase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetReportAsesmenKeperawatanDeasaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReportPengkajianPersistemIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianIGDDewasaUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetRiwayatPengobatanDirumahIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengobatanDirumahIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ONGET PENGKAJIAN

	return nil
}

func (ig *IGDHandler) OnReportPengkajianKeperawatanDewasaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReportPengkajianDewasaIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnReportPengkajianKeperawatanIGDUDewasaUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianPengobatanDirumahIGdAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	ig.Logging.Info("PAYLOAD")
	ig.Logging.Info(payload)

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianIGDAnakUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianNyeriIGDAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	ig.Logging.Info("PAYLOAD")
	ig.Logging.Info(payload)

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianIGDAnakUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	ig.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianRiwayatPengobatanDirumahAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengobatanDirumahIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ig.Logging.Info("DATA")
	ig.Logging.Info(payload)

	userID := c.Locals("userID").(string)
	message, er11 := ig.IGDUseCase.OnSavePengobatanDiRumahIGDUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianNyeriKeperawatanANAKIGD(c *fiber.Ctx) error {
	payload := new(dto.OnSaveNyeriIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ig.Logging.Info("DATA")
	userID := c.Locals("userID").(string)

	message, er11 := ig.IGDUseCase.OnSavePengkajianNyeriIGDUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianKeperawatanAnakIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ResponseSavePengkajianKeperawatanAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	// ON SAVE USECASE

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	return nil
}

func (ig *IGDHandler) OnSavePengkajianKeperawatanIGDFiberHandler(c *fiber.Ctx) error {
	return nil
}

func (ig *IGDHandler) OnSavePengkajianPersistemIGDHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianPersistemIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		ig.Logging.Info(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ig.Logging.Info(payload)

	// GET PENGKAJIAN PERSISTEM IGD
	data, message, er11 := ig.IGDUseCase.OnSavePengkajianPersistemIGDUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianPersistemIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianPersistemIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		ig.Logging.Info(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	vital, _ := ig.SoapUseCase.OnGetTandaVitalIGDPerawatUserCase(payload.KdBagian, "Dokter", payload.Noreg, "rajal")
	persistem, er11 := ig.IGDRepository.OnGetPengkajianPersistemIGDRepository(payload.KdBagian, payload.Noreg, payload.Usia, payload.Pelayanan)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING DATA PERSISTEM IGD
	mapper := ig.IGDMapper.ToMappingPengkajianPersistemIGD(persistem, vital)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}
