package handler

import (
	"fmt"
	"hms_api/modules/igd/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (ig *IGDHandler) OnDeteksiResikoJatuhFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeteksiResikoJatuh)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	res := ig.IGDUseCase.OnDeteksiResikoJatuhIGDUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianNyeriIGDAnakUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianNutrisiIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianNutrisiIGDUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianNutrisiDewasaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetPengkajianNutrisiDewasaIGDUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetPengkajianResikoJatuhPasienDewasaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetResikoJatuhPasienDewasa)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er11 := ig.IGDUseCase.OnGetResikoJatuhPasienDewasaIGDUseCase(*payload)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianResikoJatuhPasienDewasaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianResikoJatuhPasienDewasaIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ig.Logging.Info("TOTAL DATA")
	ig.Logging.Info(payload.Total)

	userID := c.Locals("userID").(string)
	message, er11 := ig.IGDUseCase.OnSaveResikoJatuhPasienDewasaIGDUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse("Gagal medapatkan data", http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianNutrisiDewasaFiberHandlerIGD(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianNutrisiDewasaIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	data, message, er11 := ig.IGDUseCase.OnSavePengkajianNutrisiDewasaIGDUseCase(*payload, userID, payload.DevicesID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSavePengkajianNutrisiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianNutrisiIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	ig.Logging.Info("DATA")
	ig.Logging.Info(payload)

	userID := c.Locals("userID").(string)
	data, message, er11 := ig.IGDUseCase.OnSavePengkajianNutrisiIGDUseCase(*payload, userID, payload.DevicesID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveRiwayatAlergiIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveRiwayatAlergi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)

	// ==========
	ig.Logging.Info("DATA RIWAYAT ALERGI ANAK")
	ig.Logging.Info(payload)
	message, er11 := ig.IGDUseCase.OnSavePengkajianAlergiAnakIGDAnakUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (ig *IGDHandler) OnSavePengkajianAwalIGDRiwayatKehamilanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianKehamilanAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	ig.Logging.Info("DATA")
	ig.Logging.Info(payload)
	userID := c.Locals("userID").(string)

	message, err12 := ig.IGDUseCase.OnSavePengkajianKehamilanAnakUseCase(*payload, userID)

	if err12 != nil {
		ig.Logging.Error(err12.Error())
		response := helper.APIResponse(message, http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetRiwayatAlergiIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetPengkajianALergiIGDUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
