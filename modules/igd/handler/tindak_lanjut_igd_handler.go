package handler

import (
	"fmt"
	"hms_api/modules/igd/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (ig *IGDHandler) OnGetKondisiTindakLanjutIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetTindakLanjut)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	tindak := ig.IGDUseCase.OnGetKondisiTindakLanjutUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, tindak)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (ig *IGDHandler) OnGetAsesmenKeperawatanIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetAsesmenKeperawatanIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	ig.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetAsesmenKeperawatanUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnReportAsesmenKeperawatanANAKFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReportAsesmenKeperawatanAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	ig.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat diproses", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetReportPengkajianKeperawatanInapAnakUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetAsesmenKeperawatanANAKFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetAsesemenKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	ig.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetAsesmenKeperawatanRanapUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveAsesmenKeperawatanANAKFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveAsesmenKeperawatanANAK)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	message, er122 := ig.IGDUseCase.OnSaveAsesmenKeperawatanANAKRanapUseCase(*payload, userID)

	if er122 != nil {
		response := helper.APIResponse(message, http.StatusCreated, er122.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetAsesmenKeperawatanRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetAsesemenKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	ig.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data := ig.IGDUseCase.OnGetAsesmenKeperawatanRanapUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnGetReportAsesmenKeperawatanRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetReportAsesmenKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	return nil
}

func (ig *IGDHandler) OnSaveAsesmenKeperawatanIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveAsesmenKeperawatanIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	message, er122 := ig.IGDUseCase.OnSaveAsesmenKeperawatanUseCase(*payload, userID)

	if er122 != nil {
		ig.Logging.Error(er122.Error())
		response := helper.APIResponse(message, http.StatusCreated, er122.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveAsesmenKeperawatanRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveAsesmenKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	ig.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	message, er122 := ig.IGDUseCase.OnSaveAsesmenRANAPKeperawatanUseCase(*payload, userID)

	if er122 != nil {
		ig.Logging.Error(er122.Error())
		response := helper.APIResponse(message, http.StatusCreated, er122.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (ig *IGDHandler) OnSaveKondisiTindakLanjutIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveTindakLajutIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	userID := c.Locals("userID").(string)
	message, er11 := ig.IGDUseCase.OnSaveTindakLanjutUseCase(*payload, userID)

	if er11 != nil {
		ig.Logging.Error(er11.Error())
		response := helper.APIResponse(message, http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}
