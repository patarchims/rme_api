package igd

import (
	"hms_api/modules/his"
	"hms_api/modules/rme"
	"time"
)

type (
	PengkjianPersistemIGD struct {
		InsertDttm                     string
		UpdDttm                        string
		KeteranganPerson               string
		InsertUserId                   string
		InsertPc                       string
		TglMasuk                       string
		TglKeluar                      string
		JamCheckOut                    string
		JamCheckInRanap                string
		KdBagian                       string
		Noreg                          string
		KdDpjp                         string
		AseskepSistemMerokok           string
		AseskepSistemMinumAlkohol      string
		AseskepSistemSpikologis        string
		AseskepSistemGangguanJiwa      string
		AseskepSistemBunuhDiri         string
		AseskepSistemTraumaPsikis      string
		AseskepSistemHambatanSosial    string // KEBUTUHAN SPRITUAL
		AseskepSistemHambatanSpiritual string
		AseskepSistemHambatanEkonomi   string
		AseskepSistemPenghasilan       string
		AseskepSistemKultural          string
		AseskepSistemAlatBantu         string
		AseskepSistemKebutuhanKhusus   string
	}

	// GET PENGKAJIAN IGD FROM DPENGKAJIAN KEPERAWATAN
	PengkajianKeperawatan struct {
		IDPengkajian             int                   `gorm:"column:id_pengkajian;primaryKey;autoIncrement"`
		InsertDttm               time.Time             `gorm:"column:insert_dttm;default:0000-00-00 00:00:00"`
		KdBagian                 string                `gorm:"column:kd_bagian;size:50"`
		UserID                   string                `gorm:"column:user_id;size:50"`
		Noreg                    string                `gorm:"column:noreg;size:225"`
		Usia                     string                `gorm:"column:usia;type:enum('DEWASA','ANAK');default:DEWASA"`
		Pelayanan                string                `gorm:"column:pelayanan;type:enum('RAJAL','RANAP')"`
		Anamnesa                 string                `gorm:"column:anamnesa;size:225"`
		CaraMasuk                string                `gorm:"column:cara_masuk;size:225"`
		AsalPasien               string                `gorm:"column:asal_pasien;size:225"`
		KeluhanUtama             string                `gorm:"column:keluhan_utama;size:225"`
		RiwayatPenyakitSekarang  string                `gorm:"column:riwayat_penyakit_sekarang;type:text"`
		RiwayatPenyakitDahulu    string                `gorm:"column:riwayat_penyakit_dahulu;type:text"`
		RiwayatPenyakitKeluarga  string                `gorm:"column:riwayat_penyakit_keluarga;type:text"`
		RiwayatPengobatanDirumah string                `gorm:"column:riwayat_pengobatan_dirumah;type:enum('TIDAK ADA','ADA')"`
		RiwayatAlergi            string                `gorm:"column:riwayat_alergi;type:enum('TIDAK ADA','ADA')"`
		ReaksiAlergi             string                `gorm:"column:reaksi_alergi;size:225"`
		Nyeri                    string                `gorm:"column:nyeri;type:enum('TIDAK ADA','ADA')"`
		RiwayatKehamilan         string                `gorm:"column:riwayat_kehamilan;"`
		TindakLanjut             string                `gorm:"column:tindak_lanjut;"`
		AlasanRujukan            string                `gorm:"column:alasan_rujukan;"`
		KeteranganRujuk          string                `gorm:"column:keterangan_rujuk;"`
		KondisiPasien            string                `gorm:"column:kondisi_pasien;"`
		Indikasi                 string                `gorm:"column:indikasi;"`
		TransportasiPulang       string                `gorm:"column:transportasi_pulang;"`
		DPJP                     string                `gorm:"column:dpjp;"`
		Ruangan                  string                `gorm:"column:ruangan;"`
		PendidikanPasien         string                `gorm:"column:pendidikan_pasien;"`
		DischargePlanning        string                `gorm:"column:discharge_planning;"`
		KPelayanan               rme.KPelayanan        `gorm:"foreignKey:Ruangan" json:"bagian"`
		KDokter                  his.KTaripDokterModel `gorm:"foreignKey:DPJP" json:"dokter"`
		Perawat                  rme.UserPerawatModel  `gorm:"foreignKey:UserID" json:"user"`
	}

	// DCPPT
	DCPPT struct {
		IDCPPT        int                  `gorm:"column:id_cppt;primaryKey;autoIncrement"`
		InsertDttm    time.Time            `gorm:"column:insert_dttm;not null;default:0000-00-00 00:00:00"`
		UpdDttm       time.Time            `gorm:"column:upd_dttm;not null;default:CURRENT_TIMESTAMP;autoUpdateTime"`
		InsertUserID  string               `gorm:"column:insert_user_id;size:30;not null;default:''"`
		InsertPC      string               `gorm:"column:insert_pc;size:100;default:''"`
		Kelompok      string               `gorm:"column:kelompok;size:30;default:''"`
		Pelayanan     string               `gorm:"column:pelayanan;size:10;default:''"`
		KdBagian      string               `gorm:"column:kd_bagian;size:30;not null;default:''"`
		Tanggal       time.Time            `gorm:"column:tanggal;not null;default:0000-00-00"`
		Noreg         string               `gorm:"column:noreg;size:30;not null;default:''"`
		DPJP          string               `gorm:"column:dpjp;size:30;default:''"`
		Subjektif     string               `gorm:"column:subjektif;type:text"`
		Objektif      string               `gorm:"column:objektif;type:text"`
		Asesmen       string               `gorm:"column:asesmen;type:text"`
		Plan          string               `gorm:"column:plan;type:text"`
		Situation     string               `gorm:"column:situation;type:text"`
		Background    string               `gorm:"column:background;type:text"`
		Recomendation string               `gorm:"column:recomendation;type:text"`
		InstruksiPPA  string               `gorm:"column:instruksi_ppa;type:text"`
		PPAFingerTTD  string               `gorm:"column:ppa_finger_ttd;size:30;default:''"`
		PPAFingerTgl  time.Time            `gorm:"column:ppa_finger_tgl;default:0000-00-00"`
		PPAFingerJam  string               `gorm:"column:ppa_finger_jam;type:time;default:'00:00:00'"`
		VerifikasiTTD string               `gorm:"column:verifikasi_ttd;size:30;default:''"`
		VerifikasiTgl time.Time            `gorm:"column:verifikasi_tgl;default:0000-00-00"`
		VerifikasiJam string               `gorm:"column:verifikasi_jam;type:time;default:'00:00:00'"`
		Perawat       rme.UserPerawatModel `gorm:"foreignKey:InsertUserID" json:"user"`
	}

	DriwayatPengobatanDirumah struct {
		InsertDttm             time.Time `gorm:"column:insert_dttm;type:datetime;default:'0000-00-00 00:00:00'"`
		UpdDttm                time.Time `gorm:"column:upd_dttm;type:timestamp;default:CURRENT_TIMESTAMP"`
		InsertUserID           string    `gorm:"column:insert_user_id;type:varchar(50);default:''"`
		InsertPC               string    `gorm:"column:insert_pc;type:varchar(100);default:''"`
		KdRiwayat              string    `gorm:"column:kd_riwayat;type:varchar(225);primaryKey;default:''"`
		KdBagian               string    `gorm:"column:kd_bagian;type:varchar(225);default:''"`
		Usia                   string    `gorm:"column:usia;type:enum('DEWASA', 'ANAK');default:'DEWASA'"`
		Noreg                  string    `gorm:"column:noreg;type:varchar(30);default:''"`
		NamaObat               string    `gorm:"column:nama_obat;type:varchar(225);default:''"`
		Dosis                  string    `gorm:"column:dosis;type:varchar(225);default:''"`
		CaraPemberian          string    `gorm:"column:cara_pemberian;type:varchar(225);default:''"`
		Frekuensi              string    `gorm:"column:frekuensi;type:varchar(225);default:''"`
		WaktuTerakhirPemberian string    `gorm:"column:waktu_terakhir_pemberian;type:varchar(225);default:''"`
	}

	// ==========

	DGeneralConsent struct {
		IDGeneral       int       `gorm:"column:id_general;primaryKey;autoIncrement"`
		InsertDttm      time.Time `gorm:"column:insert_dttm;not null;default:'0000-00-00 00:00:00'"`
		UserID          string    `gorm:"column:user_id;size:225;not null;default:''"`
		NoRM            string    `gorm:"column:no_rm;size:50;not null;default:''"`
		KdBagian        string    `gorm:"column:kd_bagian;size:225;default:''"`
		Pelayanan       string    `gorm:"column:pelayanan;type:enum('RAJAL', 'RANAP');default:null"`
		PJawabNama      string    `gorm:"column:pjawab_nama;size:225;not null;default:''"`
		PJawabAlamat    string    `gorm:"column:pjawab_alamat;size:225;not null;default:''"`
		PJawabTglLahir  string    `gorm:"column:pjawab_tgllahir;size:225;not null;default:''"`
		PJawabNoHP      string    `gorm:"column:pjawab_nohp;size:225;not null;default:''"`
		Pewenang        string    `gorm:"column:pewenang;type:text"`
		HubDenganPasien string    `gorm:"column:hub_pasien;type:text" json:"hub_pasien"`
	}

	KTaripDokter struct {
		Namadokter   string `gorm:"column:namadokter;" json:"nama_dokter"`
		Spesialisasi string `gorm:"column:spesialisasi;" json:"spesialisasi"`
		IDDokter     string `gorm:"column:iddokter;" json:"iddokter"`
	}

	AsesmenDokter struct {
		InsertDttm        time.Time             `json:"insert_dttm"`         // Timestamp of insertion
		Noreg             string                `json:"noreg"`               // Registration number
		AsesmedKeluhUtama string                `json:"asesmed_keluh_utama"` // Main complaint in medical assessment
		AsesmedRwytSkrg   string                `json:"asesmed_rwyt_skrg"`   // Current medical history
		AsesmedRwytDahulu string                `json:"asesmed_rwyt_dahulu"` // Past medical history
		Pelayanan         string                `json:"pelayanan"`           // Service provided
		KdBagian          string                `json:"kd_bagian"`           // Department code
		KeteranganPerson  string                `json:"keterangan_person"`   // Person's description
		InsertUserID      string                `json:"insert_user_id"`
		Dokter            his.KTaripDokterModel `gorm:"foreignKey:InsertUserID" json:"dokter"`
	}

	DRisikoJatuh struct {
		InsertDttm       time.Time
		KetPerson        string `gorm:"column:ket_person;" json:"ket_person"`
		Pelayanan        string `gorm:"column:pelayanan;" json:"pelayanan"`
		InserUserId      string `gorm:"column:insert_user_id;" json:"user_id"`
		KDBagian         string `gorm:"column:kd_bagian;" json:"kd_bagian"`
		NOReg            string `gorm:"column:noreg;" json:"noreg"`
		Kategori         string `gorm:"column:kategori;" json:"kategori"`
		Usia             string `gorm:"column:usia;" json:"usia"`
		JenisKelamin     string `gorm:"column:jenis_kelamin;" json:"jenis_kelamin"`
		Diagnosis        string `gorm:"column:diagnosis;" json:"diagnosis"`
		GangguanKognitif string `gorm:"column:gangguan_kognitif;" json:"gangguan_kognitif"`
		FaktorLingkungan string `gorm:"column:faktor_lingkungan;" json:"faktor_lingkungan"`
		Respon           string `gorm:"column:respon;" json:"respon"`
		PenggunaanObat   string `gorm:"column:penggunaan_obat;" json:"penggunaan_obat"`
		RJatuh           string `gorm:"column:r_jatuh;" json:"r_jatuh"`
		Aktivitas        string `gorm:"column:aktivitas;" json:"aktivitas"`
		Mobilisasi       string `gorm:"column:mobilisasi;" json:"mobilisasi"`
		Kognitif         string `gorm:"column:kognitif;" json:"kongnitif"`
		DefisitSensori   string `gorm:"column:defisit_sensori;" json:"defisit_sensori"`
		Pengobatan       string `gorm:"column:pengobatan;" json:"pengobatan"`
		Komorbiditas     string `gorm:"column:komorbiditas;" json:"komorbiditas"`
		BAmbulasi        string `gorm:"column:b_ambulasi;" json:"b_ambulasi"`
		Terapi           string `gorm:"column:terapi;" json:"terapi"`
		TerpasangInfuse  string `gorm:"column:terpasang_infuse;" json:"terpasang_infuse"`
		GayaBerjalan     string `gorm:"column:gaya_berjalan;" json:"gaya_berjalan"`
		StatusMental     string `gorm:"column:status_mental;" json:"status_mental"`
		AlatBantu1       string `gorm:"column:alat_bantu1;" json:"alat_bantu1"`
		AlatBantu2       string `gorm:"column:alat_bantu2;" json:"alat_bantu2"`
		AlatBantu3       string `gorm:"column:alat_bantu3;" json:"alat_bantu3"`
		InsertPC         string `gorm:"column:insert_pc;" json:"device_id"`
		Total            int    `gorm:"column:total;" json:"total"`
	}
)

func (DGeneralConsent) TableName() string {
	return "vicore_rme.dgeneral_consent"
}

func (AsesmenDokter) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (DRisikoJatuh) TableName() string {
	return "vicore_rme.drisiko_jatuh"
}

func (KTaripDokter) TableName() string {
	return "his.ktaripdokter"
}

// TableName sets the insert_dttm table name
func (DriwayatPengobatanDirumah) TableName() string {
	return "vicore_rme.driwayat_pengobatan_dirumah"
}

func (PengkjianPersistemIGD) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DCPPT) TableName() string {
	return "vicore_rme.dcppt"
}

func (PengkajianKeperawatan) TableName() string {
	return "vicore_rme.dpengkajian_keperawatan"
}
