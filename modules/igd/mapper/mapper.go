package mapper

import (
	"fmt"
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"hms_api/modules/igd/entity"
	"hms_api/modules/lib"
	DTONyeri "hms_api/modules/nyeri/dto"
	"hms_api/modules/rme"
	RMEDTO "hms_api/modules/rme/dto"
	rmeEntity "hms_api/modules/rme/entity"

	// rmeMapper "hms_api/modules/rme/mapper"
	"hms_api/modules/soap"
	SOAPDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type IGDMapperImple struct {
	RMEMapper rmeEntity.RMEMapper
}

func NewIGDMapperImple(RMEMapper rmeEntity.RMEMapper) entity.IGDMapper {
	return &IGDMapperImple{
		RMEMapper: RMEMapper,
	}
}

func (im *IGDMapperImple) ToMappingPengkjianPersistemIGD(data igd.PengkjianPersistemIGD) (res dto.ResponseAsesmenIGD) {
	return dto.ResponseAsesmenIGD{
		Noreg:                          data.Noreg,
		AseskepSistemMerokok:           data.AseskepSistemMerokok,
		AseskepSistemMinumAlkohol:      data.AseskepSistemMinumAlkohol,
		AseskepSistemSpikologis:        data.AseskepSistemSpikologis,
		AseskepSistemGangguanJiwa:      data.AseskepSistemGangguanJiwa,
		AseskepSistemBunuhDiri:         data.AseskepSistemBunuhDiri,
		AseskepSistemTraumaPsikis:      data.AseskepSistemTraumaPsikis,
		AseskepSistemHambatanSosial:    data.AseskepSistemHambatanSosial,
		AseskepSistemHambatanSpiritual: data.AseskepSistemHambatanSpiritual,
		AseskepSistemHambatanEkonomi:   data.AseskepSistemHambatanEkonomi,
		AseskepSistemPenghasilan:       data.AseskepSistemPenghasilan,
		AseskepSistemKultural:          data.AseskepSistemKultural,
		AseskepSistemAlatBantu:         data.AseskepSistemAlatBantu,
		AseskepSistemKebutuhanKhusus:   data.AseskepSistemKebutuhanKhusus,
	}
}

// TO MAPPING PENGKAJIAN PERSISTEM IGD
func (im *IGDMapperImple) ToMappingPengkajianPersistemIGD(persistem igd.DpengkajianPersistem, vital SOAPDTO.TandaVitalIGDResponse) (res dto.ResponsePengkajianPersistemIGD) {

	// TAMBAHKAN MESSAGE
	if persistem.NoReg == "" {
		disability := fmt.Sprintf("Tingkat Kesadaran :\nGCS: E:%s M:%s V:%s  Total :  \nGelisah:\nKejang :\nPupil :\nReaksi Cahaya :\nAmnesia :\nTrauma Kepasa :\nRiwayat Jatuh :\nKronologis :\nParese/Plegis:\nNyeri Kepala :\n Pandangan Berputar :\nSejak :\nOyong :Sejak :\nKaku Kuduk:Sejak\nBicara Pelo :\nSejak :", vital.GCSE, vital.GCSM, vital.GCSV)
		metobolisme := fmt.Sprintf("TD : %s Mmhg.\nNadi :  %s x/i.\nTemp : %s°C\nBB: %s Kg. TB: %s CM. Lingkar Perut : .... CM\n Labor: .......\n", vital.Td, vital.Nadi, vital.Suhu, vital.Bb, vital.Tb)

		return dto.ResponsePengkajianPersistemIGD{
			NoReg:               persistem.NoReg,
			Airways:             "Jalan Napas :\nSuara Napas :\n",
			Breathing:           "Sesak Napas :\nFrekuensi Napas :   x/i\nIrama Napas\nGangguan Pola Napas:\nBatuk :\nTrakea: \nTracheostomy:\nAlat Bantu Napas di Rumah : \n",
			Circulation:         "Nadi :  x/i\nIrama :\nTD:  /  Mmhg\nDistensi vena jugularis :\nAkral : \nTugor Kulit:\nPengisian Kapiler :\nEdema : \nPendarahan :\nTrauma :\nIctus cordis :\nHR :  x/i\nBunyi Jantung\nNyeri Dada :\nKarakteristik : ",
			Disability:          disability,
			SistemPerkemihan:    "Pola BAK : - x/i\nBAK :\nKeluhan:\nTrauma :\nTidak Bisa BAK :\nScrotum Bengkak/Sakit :\nSejak :\n",
			SistemPencernaan:    "Keluhan :\nSakit Perut : \nBAB : Sejak\nPlatus :\nSejak :\nMucosa Mulut :\nRongga Mulut:\nTertelan Racun :\nMinum Racun (ingin bunuh diri)\nJenis:\nTgl : ..../..../... jam:\nBising Usus :\nFrekuensi : /x/menit\nNyeri Tekan :\nLokasi :\n\nHati/Limpa :\n\nTrauma :\n",
			SistemIntegumen:     "Keluhan :\nTugor Kulit :\nSendi :\nROM :\nLokasi:\nTrauma .\nLuka Bakar :\n\nGrade : di\nLuka :\nDislokasi :\nFraktur :\n",
			KebidananKandungan:  "Haid pertama umur : - tahun\nHaid :-\nHsif Terakhir : -/-/-",
			THTMATA:             "Mata :\nTekanan Bola Mata Meningkat :\nMata Cekung : \nBenda Asing :\nTrauma:\nNyeri Telinga :\nBenda Asing :.\nPendengaran :\nNyeri Menelan :\n",
			MetaBolisme:         metobolisme,
			PsikiatriPsikologis: "-",
			SosioKulural:        "Pendidikan terahir :\nPekerjaan :.\nHubungan dengan anggota keluarga :\nHubungan dengan orang tua :-\nNilai/Aturan khusus Agama/Adat :\n,Sebutkan :\n",
		}
	}

	return dto.ResponsePengkajianPersistemIGD{
		NoReg:               persistem.NoReg,
		Airways:             persistem.AirWays,
		Breathing:           persistem.Breathing,
		Circulation:         persistem.Circulation,
		Disability:          persistem.DisabilityNeurologis,
		SistemPerkemihan:    persistem.SistemPerkemihan,
		SistemPencernaan:    persistem.SistemPencernaan,
		SistemIntegumen:     persistem.SistemIntegumenMusculoskletal,
		KebidananKandungan:  persistem.KebidananKandungan,
		THTMATA:             persistem.ThtMata,
		MetaBolisme:         persistem.Metabolisme,
		PsikiatriPsikologis: persistem.PsikiatriPsikologis,
		SosioKulural:        persistem.SosioKultural,
	}
}

func (im *IGDMapperImple) ToMapperResponseObseVasi(fisik RMEDTO.ResponseIGD, vitalSIgn soap.DVitalSignIGDDokter) (res dto.ResponseObseVasi) {
	return dto.ResponseObseVasi{
		TD:         vitalSIgn.Td,
		Nadi:       vitalSIgn.Nadi,
		Pernapasan: vitalSIgn.Pernafasan,
		Suhu:       vitalSIgn.Suhu,
		Kesadaran:  fisik.Kesadaran,
		GCSE:       fisik.GcsE,
		GCSV:       fisik.GcsV,
		GCSM:       fisik.GcsM,
	}
}

// ToResponseResikoJatuh

func (im *IGDMapperImple) ToMapingAlergiPasien(alergi []rme.DAlergi) dto.ResponseAlergi {
	if len(alergi) < 1 {
		return dto.ResponseAlergi{
			AlergiMakanan: "",
			AlergiObat:    "",
			AlergiLainnya: "",
		}
	} else {
		return dto.ResponseAlergi{
			AlergiObat:    FilterByKelompok(alergi, "obat")[0].Alergi,
			AlergiMakanan: FilterByKelompok(alergi, "makanan")[0].Alergi,
			AlergiLainnya: FilterByKelompok(alergi, "lain")[0].Alergi,
		}
	}

}

func FilterByKelompok(data []rme.DAlergi, kelompok string) []rme.DAlergi {
	var filteredData []rme.DAlergi

	for _, item := range data {
		if item.Kelompok == kelompok {
			filteredData = append(filteredData, item)
		} else {
			filteredData = make([]rme.DAlergi, 1)
		}
	}
	return filteredData
}

func (im *IGDMapperImple) TOMapperResponsePengkajianNyeriIGD(Pengkajian igd.PengkajianKeperawatan, nyeri DTONyeri.ToResponsePengkajianAwalNyeri) (res dto.ResponsePengkajianNyeriIGD) {
	return dto.ResponsePengkajianNyeriIGD{
		Pengkajian: im.ToMappingPengkajianKeperawatanIGD(Pengkajian),
		Nyeri:      nyeri,
	}
}

func (im *IGDMapperImple) ToMappingReportPengkajianIGDDewasa(sistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT) (res dto.ResponseReportPengkajianDewasaIGD) {
	return dto.ResponseReportPengkajianDewasaIGD{
		Persistem:         sistem,
		AsuhanKeperawatan: asuhan,
		Pengkajian:        im.ToMappingPengkajianKeperawatanIGD(pengkajian),
		Cppt:              im.ToMappingCPPT(cppt),
	}
}

func (im *IGDMapperImple) ToMappingPengkajianAlergi(sistem igd.PengkajianKeperawatan) (res dto.ResponsePengkajianAlergiIGD) {
	return dto.ResponsePengkajianAlergiIGD{
		Pengkajian: im.ToMappingPengkajianKeperawatanIGD(sistem),
	}
}

func (im *IGDMapperImple) ToMappingPegkajianRawatInapAnakBangsal(pengkajian igd.PengkajianKeperawatan) (res dto.ResponsePengkajianKeperawatan) {
	return dto.ResponsePengkajianKeperawatan{
		InsertDttm:               pengkajian.InsertDttm,
		Noreg:                    pengkajian.Noreg,
		Usia:                     pengkajian.Usia,
		Pelayanan:                pengkajian.Pelayanan,
		Anamnesa:                 pengkajian.Anamnesa,
		KeluhanUtama:             pengkajian.KeluhanUtama,
		RiwayatPenyakitSekarang:  pengkajian.RiwayatPenyakitSekarang,
		RiwayatPenyakitDahulu:    pengkajian.RiwayatPenyakitDahulu,
		RiwayatPenyakitKeluarga:  pengkajian.RiwayatPenyakitKeluarga,
		RiwayatPengobatanDirumah: pengkajian.RiwayatPengobatanDirumah,
		RiwayatAlergi:            pengkajian.RiwayatAlergi,
		ReaksiAlergi:             pengkajian.ReaksiAlergi,
		Nyeri:                    pengkajian.Nyeri,
		RiwayatKehamilan:         toKehamilan(pengkajian.RiwayatKehamilan),
	}
}

func (im *IGDMapperImple) ToMappingPengkajianKeperawatanIGD(pengkajian igd.PengkajianKeperawatan) (res dto.ResponsePengkajianKeperawatanIGD) {
	return dto.ResponsePengkajianKeperawatanIGD{
		InsertDttm:               pengkajian.InsertDttm,
		KdBagian:                 pengkajian.KdBagian,
		Noreg:                    pengkajian.Noreg,
		Usia:                     pengkajian.Usia,
		Pelayanan:                pengkajian.Pelayanan,
		Anamnesa:                 pengkajian.Anamnesa,
		CaraMasuk:                pengkajian.CaraMasuk,
		AsalPasien:               pengkajian.AsalPasien,
		KeluhanUtama:             pengkajian.KeluhanUtama,
		RiwayatPenyakitSekarang:  pengkajian.RiwayatPenyakitSekarang,
		RiwayatPenyakitDahulu:    pengkajian.RiwayatPenyakitDahulu,
		RiwayatPenyakitKeluarga:  pengkajian.RiwayatPenyakitKeluarga,
		RiwayatPengobatanDirumah: pengkajian.RiwayatPengobatanDirumah,
		RiwayatAlergi:            pengkajian.RiwayatAlergi,
		ReaksiAlergi:             pengkajian.ReaksiAlergi,
		Nyeri:                    pengkajian.Nyeri,
		RiwayatKehamilan:         toKehamilan(pengkajian.RiwayatKehamilan),
		TindakLanjut:             pengkajian.TindakLanjut,
		AlasanRujukan:            pengkajian.AlasanRujukan,
		KeteranganRujuk:          pengkajian.KeteranganRujuk,
		KondisiPasien:            pengkajian.KondisiPasien,
		Indikasi:                 pengkajian.Indikasi,
		TransportasiPulang:       pengkajian.TransportasiPulang,
		Dpjp:                     pengkajian.KDokter.Namadokter,
		Ruangan:                  pengkajian.KPelayanan.Bagian,
	}
}

func (im *IGDMapperImple) ToMappingAsesmenKeperawatanIGD(pengkajian igd.PengkajianKeperawatan, riwayat []igd.DriwayatPengobatanDirumah) (res dto.ResponsePengkajianIGD) {

	return dto.ResponsePengkajianIGD{
		Pengkajian:        im.ToMappingPengkajianKeperawatanIGD(pengkajian),
		PengobatanDirumah: im.TOMAPINGPPengobatanDirumah(riwayat),
	}
}

func (im *IGDMapperImple) TOMAPINGPPengobatanDirumah(data []igd.DriwayatPengobatanDirumah) (res []dto.ResponsePegobatanDirumah) {
	var riwayats []dto.ResponsePegobatanDirumah

	if len(data) < 1 || data != nil {
		riwayats = make([]dto.ResponsePegobatanDirumah, 0)
	}

	if len(data) > 0 {
		for _, V := range data {
			riwayats = append(riwayats, dto.ResponsePegobatanDirumah{
				KdRiwayat:              V.KdRiwayat,
				KdBagian:               V.KdBagian,
				Usia:                   V.Usia,
				Noreg:                  V.Noreg,
				NamaObat:               V.NamaObat,
				Dosis:                  V.Dosis,
				CaraPemberian:          V.CaraPemberian,
				Frekuensi:              V.Frekuensi,
				WaktuTerakhirPemberian: V.WaktuTerakhirPemberian,
			})
		}
		return riwayats

	}

	return riwayats
}

func (im *IGDMapperImple) ToMappingCPPT(cppt []igd.DCPPT) (res []dto.ResponseCPPT) {
	var cppts []dto.ResponseCPPT

	if len(cppt) < 1 || cppt != nil {
		cppts = make([]dto.ResponseCPPT, 0)
	}

	if len(cppt) > 0 {
		for _, V := range cppts {
			cppts = append(cppts, dto.ResponseCPPT{
				InsertDttm:    V.InsertDttm,
				InsertUserID:  V.InsertUserID,
				Noreg:         V.Noreg,
				Subjektif:     V.Subjektif,
				Objektif:      V.Objektif,
				Plan:          V.Plan,
				Situation:     V.Situation,
				Background:    V.Background,
				Recomendation: V.Recomendation,
				Perawat:       V.Perawat,
				Asesmen:       V.Asesmen,
			})
		}

		return cppts
	}

	return cppts
}

func toKehamilan(value string) (message string) {
	if value == "" {
		return "Riwayat Kehamilan : \nKehamilan : .... bulan\nRiwayat Persalinan :\nBBL : ... Gram\nPBL : .... Cm\nLK: ..... Cm\nLD : .... Cm\nRiwayat Post Natal:..\nRiwayat Imunisasi Dasar :"
	} else {
		return value
	}
}

func (im *IGDMapperImple) ToMappingGeneralConsent(userProfile user.ProfilePasien, general igd.DGeneralConsent, pengkajian igd.PengkajianKeperawatan) (res dto.ResponseGeneralConsent) {
	return dto.ResponseGeneralConsent{
		ProfilePasien: userProfile,
		PNangungJawab: im.ToMappingPenangungJawab(general),
		Pengkajian:    im.ToMappingPengkajianKeperawatanIGD(pengkajian),
	}
}

func (im *IGDMapperImple) ToMappingGeneralConsentRanap(userProfile user.ProfilePasien, general igd.DGeneralConsent, dokterAsesmen rme.AsesemenMedisIGD, pengkajian igd.PengkajianKeperawatan) (res dto.ResponseGeneralConsentInap) {
	return dto.ResponseGeneralConsentInap{
		PNangungJawab: im.ToMappingPenangungJawab(general),
		ProfilePasien: userProfile,
		DokterAsesmen: dokterAsesmen,
	}
}

func (im *IGDMapperImple) ToMappingPenangungJawab(data igd.DGeneralConsent) (res dto.MapperGeneralConsent) {
	return dto.MapperGeneralConsent{
		PJawabNama:     data.PJawabNama,
		PJawabAlamat:   data.PJawabAlamat,
		PJawabTglLahir: data.PJawabTglLahir,
		PJawabNoHP:     data.PJawabNoHP,
		InsertDttm:     data.InsertDttm,
		Pewenang:       toWewenang(data.Pewenang),
		HubPasien:      data.HubDenganPasien,
	}
}

func toWewenang(value string) (message string) {
	if value == "" {
		return "1. ......................................................\n2. ......................................................"
	} else {
		return value
	}
}

func (im *IGDMapperImple) ToMappingReportGeneralConsent(pasienMapper user.ProfilePasien, general igd.DGeneralConsent, karyawan user.Karyawan) (res dto.ResponseReportGeneralConsent) {
	return dto.ResponseReportGeneralConsent{
		ProfilePasien: pasienMapper,
		PNangungJawab: im.ToMappingPenangungJawab(general),
		Perawat:       karyawan,
	}
}

func (im *IGDMapperImple) ToMappingResponseTindakLanjutIGD(data igd.PengkajianKeperawatan, dokter []igd.KTaripDokter, pelayanan []lib.KPelayanan) (res dto.ResponseTindakLajutIGD) {
	return dto.ResponseTindakLajutIGD{
		TindakLanjut: dto.TindakLanjutResponse{
			TindakLanjut:       data.TindakLanjut,
			AlasanRujukan:      data.AlasanRujukan,
			KondisiPasien:      data.KondisiPasien,
			Indikasi:           data.Indikasi,
			DPJP:               data.DPJP,
			KeteranganRujuk:    data.KeteranganRujuk,
			TransportasiPulang: data.TransportasiPulang,
			Ruangan:            data.Ruangan,
		},
		Dokter:    dokter,
		Pelayanan: pelayanan,
	}
}

func (imt *IGDMapperImple) ToResponseResikoJatuh(resiko igd.DRisikoJatuh) dto.ResponseResikoJatuh {
	return dto.ResponseResikoJatuh{
		Umur:             resiko.Usia,
		JenisKelamin:     resiko.JenisKelamin,
		Diagnosa:         resiko.Diagnosis,
		GangguanKognitif: resiko.GangguanKognitif,
		FaktorLingkungan: resiko.FaktorLingkungan,
		Response:         resiko.Respon,
		PenggunaanObat:   resiko.PenggunaanObat,
		Total:            resiko.Total,
	}
}

func (im *IGDMapperImple) ToResponsePengkajianResikoJatuhIGDDewasa(data igd.DRisikoJatuh) (res dto.ResponseResikoJatuhDewasaIGD) {
	return dto.ResponseResikoJatuhDewasaIGD{
		RiwayatJatuh:      data.RJatuh,
		Diagnosis:         data.Diagnosis,
		AlatBantuJalan:    data.AlatBantu1,
		MenggunakanInfuse: data.TerpasangInfuse,
		CaraBerjalan:      data.GayaBerjalan,
		StatusMental:      data.StatusMental,
		Total:             data.Total,
	}
}

func (im *IGDMapperImple) ToResponsePengkajianDewasaReport(data igd.DRisikoJatuh) (res dto.ResponseResikoJatuhDewasaIGD) {
	return dto.ResponseResikoJatuhDewasaIGD{
		RiwayatJatuh:      toValue(data.RJatuh, "Riwayat jatuh, yang baru atau dalam 3 bulan terakhir"),
		Diagnosis:         toValue(data.Diagnosis, "Diagnosis Medis Sekunder >1"),
		AlatBantuJalan:    toValue(data.AlatBantu1, "Alat bantu jalan : \n"+data.AlatBantu1),
		MenggunakanInfuse: toValue(data.TerpasangInfuse, "Menggunakan Infus"),
		CaraBerjalan:      toValue(data.GayaBerjalan, "Cara berjalan/ berpindah \n"+data.GayaBerjalan),
		StatusMental:      toValue(data.StatusMental, "Status Mental\n"+data.StatusMental),
		Total:             data.Total,
	}
}

func (im *IGDMapperImple) ToMappingReportPengkajianPersistem(sistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT, pengobatan []igd.DriwayatPengobatanDirumah, nyeri DTONyeri.ToResponsePengkajianAwalNyeri, nutrisi DTONyeri.ToResponsePengkajianNutrisi, alergi []rme.DAlergi, resikoJatuh igd.DRisikoJatuh, perawat user.UserPerawat, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponseReportPengkajianIGDANAK) {
	return dto.ResponseReportPengkajianIGDANAK{
		Persistem:         sistem,
		AsuhanKeperawatan: asuhan,
		Pengkajian:        im.ToMappingPengkajianKeperawatanIGD(pengkajian),
		Cppt:              im.ToMappingCPPT(cppt),
		PengobatanDirumah: im.TOMAPINGPPengobatanDirumah(pengobatan),
		PengkajianNyeri:   nyeri,
		PengkajianNutrisi: nutrisi,
		Alergi:            im.ToMapingAlergiPasien(alergi),
		ResikoJatuh:       im.ToResponseResikoJatuh(resikoJatuh),
		Perawat:           perawat,
		Fisik:             fisik,
		VitalSign:         vital,
		Observasi:         im.ToMapperResponseObseVasi(fisik, vital),
	}
}

func (im *IGDMapperImple) ToResponseReportPengkajianKeperawatanIGDDewasa(persistem dto.ResponsePengkajianPersistemIGD, asuhan []rme.DasKepDiagnosaModelV2, pengkajian igd.PengkajianKeperawatan, cppt []igd.DCPPT, pengobatan []igd.DriwayatPengobatanDirumah, nyeri DTONyeri.ToResponsePengkajianAwalNyeri, resikoJatuh dto.ResponseResikoJatuhDewasaIGD, nutrisi DTONyeri.ToResponsePengkajianNutrisiDewasa, perawat user.UserPerawat, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponsePengkajianKeperawatanIGDDewasa) {
	return dto.ResponsePengkajianKeperawatanIGDDewasa{
		Persistem:         persistem,
		AsuhanKeperawatan: asuhan,
		Pengkajian:        im.ToMappingPengkajianKeperawatanIGD(pengkajian),
		Cppt:              im.ToMappingCPPT(cppt),
		PengobatanDirumah: im.TOMAPINGPPengobatanDirumah(pengobatan),
		PengkajianNyeri:   nyeri,
		ResikoJatuh:       resikoJatuh,
		NutrisiDewasa:     nutrisi,
		Perawat:           perawat,
		Fisik:             fisik,
		VitalSign:         vital,
		Observasi:         im.ToMapperResponseObseVasi(fisik, vital),
	}
}

func (im *IGDMapperImple) ToResponseAsesmen(data igd.PengkajianKeperawatan, asesmenIGd igd.AsesmenDokter) dto.ResponsePengkajianKeperawatanAnak {

	return dto.ResponsePengkajianKeperawatanAnak{
		Pelayanan:               data.Pelayanan,
		Anamnesa:                data.Anamnesa,
		CaraMasuk:               data.CaraMasuk,
		AsalPasien:              data.AsalPasien,
		RiwayatPenyakitKeluarga: data.RiwayatPenyakitKeluarga,
		KeluhanUtama:            toValue(data.KeluhanUtama, asesmenIGd.AsesmedKeluhUtama),
		RiwayatPenyakitSekarang: toValue(data.RiwayatPenyakitSekarang, asesmenIGd.AsesmedRwytSkrg),
		RiwayatPenyakitDahulu:   toValue(data.RiwayatPenyakitDahulu, asesmenIGd.AsesmedRwytDahulu),
	}
}

func (im *IGDMapperImple) ToResponsePersistemRANAP(data igd.DpengkajianPersistem) (res igd.ResponsePersistemRANAP) {
	return igd.ResponsePersistemRANAP{
		NutrisiDanHidrasi:      data.NutrisiDanHidrasi,
		Eliminasi:              data.Eliminasi,
		Aktivitas:              data.Aktivitas,
		SistemKardioRepiratori: data.SistemKardioRepiratori,
		SistemPerfusiSecebral:  data.SistemPerfusiSecebral,
		Thermoregulasi:         data.Thermoregulasi,
		SistemPerfusiPerifer:   data.SistemPerfusiPerifer,
		Integumen:              data.Integumen,
		Proteksi:               data.Proteksi,
		SeksualReproduksi:      data.SeksualReproduksi,
	}
}

func (im *IGDMapperImple) ToResponsePersistemAnak(data igd.DpengkajianPersistem) (res dto.ResponsePersistemAnak) {
	return dto.ResponsePersistemAnak{
		NoReg: data.NoReg,
	}
}

func (im *IGDMapperImple) ToMappingDeteksiResikoJatuh(data igd.DeteksiResikoJatuhResponse) (res dto.ResponseDeteksiResikoJatuhIGD) {
	return dto.ResponseDeteksiResikoJatuhIGD{
		NoReg:      data.Noreg,
		Total:      data.Total,
		IsShow:     isShow(data.Total),
		Keterangan: toKeterangan(data.Total),
	}
}

func toKeterangan(angka int) (message string) {
	if angka >= 25 && angka <= 44 {
		return "Risiko Sedang  : Pasang gelang resiko jatuh berwarna kuning"
	}

	if angka >= 45 {
		return "Risiko Tinggi : Pasang gelang resiko jatuh berwarna kuning"
	}

	return "Tidak berisiko dan risiko rendah"

}

func isShow(angka int) (res bool) {
	if angka >= 25 {
		return true
	} else {
		return false
	}
}

func (im *IGDMapperImple) ToMappingPengkajianRawatInapAnak(data igd.PengkajianKeperawatan, Pengobatan []igd.DriwayatPengobatanDirumah, alergi []rme.DAlergi, nutirsi DTONyeri.ToResponsePengkajianNutrisi, persistem igd.DpengkajianPersistem, Asuhan []rme.DasKepDiagnosaModelV2, fisik RMEDTO.ResponseIGD, vital soap.DVitalSignIGDDokter) (res dto.ResponsePengkajianKeperawatanRawatInapAnak) {
	return dto.ResponsePengkajianKeperawatanRawatInapAnak{
		Pengkajian: im.ToMappingPegkajianRawatInapAnakBangsal(data),
		Alergi:     im.ToMapingAlergiPasien(alergi),
		Pengobatan: Pengobatan,
		Nutrisi:    nutirsi,
		Persistem:  im.RMEMapper.ToMappingResponsePengkajianRANAP(persistem),
		Asuhan:     Asuhan,
		Fisik:      fisik,
		Vital:      vital,
	}
}

func (im *IGDMapperImple) ToResponseAsesemenRANAP(data igd.PengkajianKeperawatan, asesmenIGD igd.AsesmenDokter) (res dto.ResponsePengkajianKeperawatanRANAP) {
	return dto.ResponsePengkajianKeperawatanRANAP{
		Pelayanan:               data.Pelayanan,
		Anamnesa:                data.Anamnesa,
		KeluhanUtama:            data.KeluhanUtama,
		RiwayatPenyakitSekarang: toValue(data.RiwayatPenyakitSekarang, asesmenIGD.AsesmedRwytSkrg),
		RiwayatPenyakitDahulu:   toValue(data.RiwayatPenyakitDahulu, asesmenIGD.AsesmedRwytDahulu),
		RiwayatPenyakitKeluarga: toValue(data.RiwayatPenyakitKeluarga, asesmenIGD.AsesmedKeluhUtama),
	}
}

func toValue(value string, value1 string) (message string) {
	if value == "" {
		return value1
	} else {
		return value
	}
}

func (im *IGDMapperImple) ToIGDResponseResikoJatuh(data igd.DRisikoJatuh) (res dto.ResponseResikoJatuhIGDDewasa) {
	return dto.ResponseResikoJatuhIGDDewasa{
		RiwayatJatuh: data.RJatuh,
		Diagnosis:    data.Diagnosis,
	}
}
