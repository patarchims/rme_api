package igd

import "time"

type (
	ResponseDPengkajianPersistenRANAP struct {
		NutrisiDanHidrasi         string `json:"nutrisi_hidrasi"`
		Eliminasi                 string `json:"eliminasi"`
		Aktivitas                 string `json:"aktivasi"`
		SistemKardioRepiratori    string `json:"kardio_respiratori"`
		SistemPerfusiSecebral     string `json:"perfusi_secebral"`
		Thermoregulasi            string `json:"thermoregulasi"`
		SistemPerfusiPerifer      string `json:"sistem_perfusi_perifer"`
		SistemPencernaan          string `json:"sistem_pencernaan"`
		Integumen                 string `json:"integumen"`
		Proteksi                  string `json:"proteksi"`
		SeksualReproduksi         string `json:"seksual_reproduksi"`
		PersistemKomunikasi       string `json:"persistem_reproduksi"`
		PersistemDataSpikologis   string `json:"persistem_spikologis"`
		PersistemNilaiKepercayaan string `json:"persistem_nilai_kepercayaan"`
	}

	DeteksiResikoJatuhResponse struct {
		Noreg     string `json:"noreg"`
		KetPerson string `json:"person"`
		Pelayanan string `json:"pelayanan"`
		KdBagian  string `json:"kd_bagian"`
		Total     int    `json:"total"`
	}

	DpengkajianPersistem struct {
		IDPersistem                   int       `gorm:"column:id_persistem;primaryKey;autoIncrement" json:"id"`
		InsertDttm                    time.Time `gorm:"column:insert_dttm;default:0000-00-00 00:00:00" json:"insert_dttm"`
		KdBagian                      string    `gorm:"column:kd_bagian;size:50;default:''" json:"kd_bagian"`
		Pelayanan                     string    `gorm:"column:pelayanan;type:enum('RAJAL', 'RANAP');default:NULL" json:"pelayanan"`
		Usia                          string    `gorm:"column:usia;type:enum('ANAK', 'DEWASA');default:NULL" json:"usia"`
		NoReg                         string    `gorm:"column:noreg;size:255;default:NULL" json:"no_reg"`
		AirWays                       string    `gorm:"column:air_ways;type:text" json:"air_ways"`
		Breathing                     string    `gorm:"column:breathing;type:text" json:"breathing"`
		Circulation                   string    `gorm:"column:circulation;type:text" json:"circulation"`
		DisabilityNeurologis          string    `gorm:"column:disability_neurologis;type:text" json:"disability_neurologis"`
		SistemPerkemihan              string    `gorm:"column:sistem_perkemihan;type:text" json:"sistem_perkemihan"`
		SistemPencernaan              string    `gorm:"column:sistem_pencernaan;type:text" json:"sistem_pencernaan"`
		SistemIntegumenMusculoskletal string    `gorm:"column:sistem_integumen_musculoskletal;type:text" json:"sistem_integumen"`
		KebidananKandungan            string    `gorm:"column:kebidanan_kandungan;type:text" json:"kebidanan_kandungan"`
		ThtMata                       string    `gorm:"column:tht_mata;type:text" json:"tht_mata"`
		Metabolisme                   string    `gorm:"column:metabolisme;type:text" json:"metabolisme"`
		PsikiatriPsikologis           string    `gorm:"column:psikiatri_psikologis;type:text" json:"psikiatri_psikologis"`
		SosioKultural                 string    `gorm:"column:sosio_kultural;type:text" json:"sosial_kultural"`
		NutrisiDanHidrasi             string    `gorm:"column:nutrisi_dan_hidrasi;type:text" json:"nutrisi_hidrasi"`
		Eliminasi                     string    `gorm:"column:eliminasi;type:text" json:"eliminasi"`
		Aktivitas                     string    `gorm:"column:aktivitas;type:text" json:"aktivasi"`
		SistemKardioRepiratori        string    `gorm:"column:sistem_kardio_repiratori;type:text" json:"kardio_respiratori"`
		SistemPerfusiSecebral         string    `gorm:"column:sistem_perfusi_secebral;type:text" json:"perfusi_secebral"`
		Thermoregulasi                string    `gorm:"column:thermoregulasi;type:text" json:"thermoregulasi"`
		SistemPerfusiPerifer          string    `gorm:"column:sistem_perfusi_perifer;type:text" json:"sistem_perfusi_perifer"`
		Integumen                     string    `gorm:"column:integumen;type:text" json:"integumen"`
		Proteksi                      string    `gorm:"column:proteksi;type:text" json:"proteksi"`
		SeksualReproduksi             string    `gorm:"column:seksual_repoduksi;type:text" json:"seksual_reproduksi"`
		PersistemKomunikasi           string    `gorm:"column:persistem_komunikasi;type:text" json:"persistem_reproduksi"`
		PersistemDataSpikologis       string    `gorm:"column:persistem_data_spikologis;type:text" json:"persistem_spikologis"`
		PersistemNilaiKepercayaan     string    `gorm:"column:persistem_nilai_kepercayaan;type:text" json:"persistem_nilai_kepercayaan"`
	}

	ResponsePersistemRANAP struct {
		NutrisiDanHidrasi         string `gorm:"column:nutrisi_dan_hidrasi;type:text" json:"nutrisi_hidrasi"`
		Eliminasi                 string `gorm:"column:eliminasi;type:text" json:"eliminasi"`
		Aktivitas                 string `gorm:"column:aktivitas;type:text" json:"aktivasi"`
		SistemKardioRepiratori    string `gorm:"column:sistem_kardio_repiratori;type:text" json:"kardio_respiratori"`
		SistemPerfusiSecebral     string `gorm:"column:sistem_perfusi_secebral;type:text" json:"perfusi_secebral"`
		Thermoregulasi            string `gorm:"column:thermoregulasi;type:text" json:"thermoregulasi"`
		SistemPerfusiPerifer      string `gorm:"column:sistem_perfusi_perifer;type:text" json:"sistem_perfusi_perifer"`
		Integumen                 string `gorm:"column:integumen;type:text" json:"integumen"`
		Proteksi                  string `gorm:"column:proteksi;type:text" json:"proteksi"`
		SeksualReproduksi         string `gorm:"column:seksual_repoduksi;type:text" json:"seksual_reproduksi"`
		PersistemKomunikasi       string `json:"persistem_reproduksi"`
		PersistemDataSpikologis   string `json:"persistem_spikologis"`
		PersistemNilaiKepercayaan string `json:"persistem_nilai_kepercayaan"`
	}
)

func (DpengkajianPersistem) TableName() string {
	return "vicore_rme.dpengkajian_persistem"
}
