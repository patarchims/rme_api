package repository

import "hms_api/modules/igd"

func (ig *igdRepository) OnGetGeneralConsentRAJALRepository(noRM string, kdBagian string) (res igd.DGeneralConsent, err error) {
	result := ig.DB.Model(&res).Where(&igd.DGeneralConsent{
		KdBagian: kdBagian, NoRM: noRM, Pelayanan: "RAJAL",
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetGeneralConsentRANAP(noRM string, kdBagian string) (res igd.DGeneralConsent, err error) {
	result := ig.DB.Model(&res).Where(&igd.DGeneralConsent{
		KdBagian: kdBagian, NoRM: noRM, Pelayanan: "RANAP",
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnSaveGeneralConsentRepository(data igd.DGeneralConsent) (res igd.DGeneralConsent, err error) {
	result := ig.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnUpdatgeGeneralConsentRepository(data igd.DGeneralConsent, noRm string, kdBagian string) (res igd.DGeneralConsent, err error) {
	result := ig.DB.Where(igd.DGeneralConsent{
		KdBagian: kdBagian, NoRM: noRm,
	}).Updates(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}
