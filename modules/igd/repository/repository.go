package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"hms_api/modules/igd/entity"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type igdRepository struct {
	DB            *gorm.DB
	MapperIGd     entity.IGDMapper
	IGdRepository entity.IGDRepository
	Logging       *logrus.Logger
}

func NewIgdRepository(db *gorm.DB, logging *logrus.Logger, mapper entity.IGDMapper) entity.IGDRepository {
	return &igdRepository{
		DB:        db,
		Logging:   logging,
		MapperIGd: mapper,
	}
}

func (ig *igdRepository) OnGeAsesmenDokterRepository(noReg string, kdBagian string) (res igd.AsesmenDokter, err error) {
	results := ig.DB.Select("insert_dttm, noreg, asesmed_keluh_utama, asesmed_rwyt_skrg, asesmed_rwyt_dahulu, pelayanan, kd_bagian, keterangan_person,insert_user_id").Where(igd.AsesmenDokter{
		Noreg: noReg, Pelayanan: "rajal", KdBagian: kdBagian,
	}).Preload("Dokter").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetDResikoJatuhIGDRajalRepositroy(kdBagian string, noReg string, person string) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Where(igd.DRisikoJatuh{
		Pelayanan: "rajal", KDBagian: kdBagian, NOReg: noReg, Kategori: "Anak", KetPerson: person,
	}).First(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetAsesmenIGDRepository(kdBagian string, noReg string, person string, req dto.OnGetAsesmenIGD) (res igd.PengkjianPersistemIGD, err error) {
	query := `SELECT insert_dttm, upd_dttm, keterangan_person, insert_user_id, insert_pc,tgl_masuk, tgl_keluar, jam_check_out, jam_check_in_ranap, kd_bagian, noreg, kd_dpjp, aseskep_sistem_merokok, aseskep_sistem_minum_alkohol, aseskep_sistem_spikologis, aseskep_sistem_gangguan_jiwa, aseskep_sisetem_bunuh_diri, aseskep_sistem_trauma_psikis,aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_spiritual, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_penghasilan, aseskep_sistem_kultural, aseskep_sistem_alat_bantu, aseskep_sistem_kebutuhan_khusus FROM vicore_rme.dcppt_soap_pasien WHERE noreg=? AND kd_bagian=? AND keterangan_person=? LIMIT 1`

	result := ig.DB.Raw(query, noReg, kdBagian, person).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (ig *igdRepository) OnGetAllDPJPRepository() (res []igd.KTaripDokter, err error) {
	results := ig.DB.Select("iddokter, namadokter, spesialisasi").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGeAsesmenDokter(noReg string, kdBagian string) (res igd.AsesmenDokter, err error) {
	results := ig.DB.Select("insert_dttm, noreg, asesmed_keluh_utama, asesmed_rwyt_skrg, asesmed_rwyt_dahulu, pelayanan, kd_bagian, keterangan_person,insert_user_id").Where(igd.AsesmenDokter{
		Noreg: noReg, Pelayanan: "rajal", KdBagian: kdBagian,
	}).Preload("Dokter").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetAsesmenDokterRANAPRepository(noReg string, kdBagian string) (res igd.AsesmenDokter, err error) {
	results := ig.DB.Select("insert_dttm, noreg, asesmed_keluh_utama, asesmed_rwyt_skrg, asesmed_rwyt_dahulu, pelayanan, kd_bagian, keterangan_person, insert_user_id").Where(igd.AsesmenDokter{
		Noreg: noReg, Pelayanan: "ranap", KdBagian: kdBagian,
	}).Preload("Dokter").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetResikoJatuhRANA_ANAK_Repository(kdBagian string, noReg string) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Where(igd.DRisikoJatuh{
		Pelayanan: "ranap", KDBagian: kdBagian, NOReg: noReg, Kategori: "Anak",
	}).First(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetResikoJatuhRANA_DEWASA_Repository(kdBagian string, noReg string) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Where(igd.DRisikoJatuh{
		Pelayanan: "ranap", KDBagian: kdBagian, NOReg: noReg, Kategori: "PERAWAT",
	}).First(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) ON_GET_RESIKO_JATUH_IGD_DEWASA_REPOSITORY(NoReg string, KDBagian string) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Where(igd.DRisikoJatuh{
		Kategori: "Dewasa", NOReg: NoReg, KDBagian: KDBagian,
	}).First(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}
