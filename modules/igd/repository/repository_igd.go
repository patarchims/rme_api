package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"time"
)

func (ig *igdRepository) OnSavePengkajianKeperawatanIGDDewasaRepository(data igd.PengkajianKeperawatan) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (kb *igdRepository) OnGetRiwayatPengobatanDiRumahRepository(usia string, kdBagian string, noReg string) (res []igd.DriwayatPengobatanDirumah, err error) {
	result := kb.DB.Model(&igd.DriwayatPengobatanDirumah{}).Where(igd.DriwayatPengobatanDirumah{
		Noreg: noReg, KdBagian: kdBagian, Usia: usia,
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetCPPPTRepository(noReg string, kdBagian string) (res []igd.DCPPT, err error) {
	errs := ig.DB.Where(igd.DCPPT{
		Noreg: noReg, KdBagian: kdBagian,
	}).Preload("Perawat").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetPengkajianKeperawatanIGDDewasaRepository(kdBagian string, pelayanan string, usia string, noReg string) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Model(&res).Where(&igd.PengkajianKeperawatan{
		KdBagian: kdBagian, Pelayanan: pelayanan, Usia: usia, Noreg: noReg,
	}).Preload("KPelayanan").Preload("KDokter").Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) GetPengkajianKeperawatanRepository(kdBagian string, pelayanan string, usia string, noReg string) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Model(&res).Where(&igd.PengkajianKeperawatan{
		KdBagian: kdBagian, Pelayanan: pelayanan, Usia: usia, Noreg: noReg,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetPengkajianKeperawatanRepository(kdBagian string, pelayanan string, noReg string) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Model(&res).Where(&igd.PengkajianKeperawatan{
		KdBagian: kdBagian, Pelayanan: pelayanan, Noreg: noReg,
	}).Preload("Perawat").Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnSavePengkajianKeperawatanRespository(data igd.PengkajianKeperawatan) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnUpdatePengkajianKeperawatanRepository(data igd.PengkajianKeperawatan, noReg string, usia string, kdBagian string, pelayann string) (res igd.PengkajianKeperawatan, err error) {
	result := ig.DB.Where(igd.PengkajianKeperawatan{
		KdBagian: kdBagian, Noreg: noReg, Pelayanan: pelayann, Usia: usia,
	}).Updates(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetPengkajianPersistemIGDRepository(kdBagian string, noReg string, usia string, pelayanan string) (res igd.DpengkajianPersistem, err error) {

	result := ig.DB.Model(&res).Where("noreg= ? AND kd_bagian=? AND usia=? AND pelayanan=?", noReg, kdBagian, usia, pelayanan).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnUpdateDataPengkajianPersistemIGDRepository(noReg string, kdBagian string, pelayanan string, usia string, data igd.DpengkajianPersistem) (res igd.DpengkajianPersistem, err error) {
	result := ig.DB.Model(&res).Where(&igd.DpengkajianPersistem{
		KdBagian: kdBagian, Pelayanan: pelayanan, Usia: usia, NoReg: noReg,
	}).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (ig *igdRepository) OnSavePengkajianPersisetmIGDRepository(req dto.OnSavePengkajianPersistemIGD) (res igd.DpengkajianPersistem, err error) {

	data := igd.DpengkajianPersistem{
		InsertDttm:                    time.Now(),
		KdBagian:                      req.KdBagian,
		Pelayanan:                     req.Pelayanan,
		Usia:                          req.Usia,
		NoReg:                         req.Noreg,
		AirWays:                       req.Airways,
		Breathing:                     req.Breathing,
		Circulation:                   req.Circulation,
		DisabilityNeurologis:          req.Disability,
		SistemPerkemihan:              req.SistemPerkemihan,
		SistemPencernaan:              req.SistemPencernaan,
		SistemIntegumenMusculoskletal: req.SistemIntegumen,
		KebidananKandungan:            req.KebidananKandungan,
		ThtMata:                       req.THTMATA,
		Metabolisme:                   req.MetaBolisme,
		PsikiatriPsikologis:           req.PsikiatriPsikologis,
		SosioKultural:                 req.SosioKulural,
	}

	result := ig.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnGetDeteksiResikoJatuhRespository(noReg string, kdBagian string) (res igd.DeteksiResikoJatuhResponse, err error) {
	query := `SELECT noreg, ket_person, pelayanan, kd_bagian, total FROM vicore_rme.drisiko_jatuh WHERE kd_bagian=? AND noreg=?`

	result := ig.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (ig *igdRepository) OnGetResikoJatuhIGDDewasaRepository(pelayanan string, kdBagian string, noReg string) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Where(igd.DRisikoJatuh{
		KetPerson: "Perawat", Pelayanan: pelayanan, KDBagian: kdBagian,
		NOReg: noReg, Kategori: "Dewasa",
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (ig *igdRepository) OnSaveResikoJatuhIGDDewasaRepository(data igd.DRisikoJatuh) (res igd.DRisikoJatuh, err error) {
	result := ig.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (lu *igdRepository) OnUpdateResikoJatuhIGDDewasaRepository(data igd.DRisikoJatuh, kdBagian string, noReg string, kategori string) (res igd.DRisikoJatuh, err error) {
	result := lu.DB.Where(&igd.DRisikoJatuh{
		KDBagian: kdBagian,
		NOReg:    noReg,
		Kategori: kategori,
	}).Updates(&data).Scan(&res)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}
