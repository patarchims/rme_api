package usecase

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"time"
)

func (iu *igdUseCase) OnGetGeneralConsentUseCase(req dto.OnGetGeneralConsent) (res dto.ResponseGeneralConsent) {
	pasien, _ := iu.userRepo.OnGetProfilePasienRepository(req.NORM)
	pasienMapper := iu.userMapper.ToMappingProfilePasien(pasien)
	general, _ := iu.igdRepository.OnGetGeneralConsentRAJALRepository(req.NORM, req.KdBagian)
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.NoReg)
	mapperGeneralCOnset := iu.igdMapper.ToMappingGeneralConsent(pasienMapper, general, pengkajian)
	return mapperGeneralCOnset
}

func (iu *igdUseCase) OnGetGeneralConsentRANAPUseCase(req dto.OnGetGeneralConsentRanap) (res dto.ResponseGeneralConsentInap) {
	pasien, _ := iu.userRepo.OnGetProfilePasienRepository(req.NORM)
	pasienMapper := iu.userMapper.ToMappingProfilePasien(pasien)
	general, _ := iu.igdRepository.OnGetGeneralConsentRANAP(req.NORM, req.KdBagian)

	// ==
	dokter, _ := iu.kebidananRepository.GetAsesmenAwalMedisRawatInapReportDokter(req.NoReg, req.KdBagian, "rajal")
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.NoReg)

	mapperGeneralConset := iu.igdMapper.ToMappingGeneralConsentRanap(pasienMapper, general, dokter, pengkajian)

	return mapperGeneralConset
}

func (iu *igdUseCase) OnSaveGenaralConsentRANAPUseCase(req dto.OnSaveGeneralConsentRanap, userID string) (res dto.ResponseGeneralConsent, message string, err error) {
	general, err11 := iu.igdRepository.OnGetGeneralConsentRAJALRepository(req.NORM, req.KdBagian)

	if err11 != nil || general.NoRM == "" {
		save := igd.DGeneralConsent{
			InsertDttm:      time.Now(),
			UserID:          userID,
			NoRM:            req.NORM,
			KdBagian:        req.KdBagian,
			Pelayanan:       req.Pelayanan,
			PJawabNama:      req.PJawabNama,
			PJawabAlamat:    req.PJawabAlamat,
			PJawabNoHP:      req.PJawabNoHP,
			Pewenang:        req.Pewenang,
			HubDenganPasien: req.HubDenganPasien,
		}

		_, err12 := iu.igdRepository.OnSaveGeneralConsentRepository(save)

		if err12 != nil {
			return res, "Tidak dapat disimpan", err12
		}

		return res, "Data berhasil disimpan", nil
	} else {
		udpate := igd.DGeneralConsent{
			PJawabNama:      req.PJawabNama,
			PJawabAlamat:    req.PJawabAlamat,
			PJawabNoHP:      req.PJawabNoHP,
			Pewenang:        req.Pewenang,
			HubDenganPasien: req.HubDenganPasien,
		}

		_, terupdate := iu.igdRepository.OnUpdatgeGeneralConsentRepository(udpate, req.NORM, req.KdBagian)

		if terupdate != nil {
			return res, "Tidak dapat diubah", terupdate
		}
		return res, "Data berhasil diubah", nil
	}
}

func (iu *igdUseCase) OnSaveGeneralConsentUseCase(req dto.OnSaveGeneralConsent, userID string) (res dto.ResponseGeneralConsent, message string, err error) {
	general, err11 := iu.igdRepository.OnGetGeneralConsentRAJALRepository(req.NORM, req.KdBagian)

	if err11 != nil || general.NoRM == "" {
		save := igd.DGeneralConsent{
			InsertDttm:     time.Now(),
			UserID:         userID,
			NoRM:           req.NORM,
			KdBagian:       req.KdBagian,
			Pelayanan:      req.Pelayanan,
			PJawabNama:     req.PJawabNama,
			PJawabAlamat:   req.PJawabAlamat,
			PJawabTglLahir: req.PJawabTglLahir,
			PJawabNoHP:     req.PJawabNoHP,
			Pewenang:       req.Pewenang,
		}

		_, err12 := iu.igdRepository.OnSaveGeneralConsentRepository(save)

		if err12 != nil {
			return res, "Tidak dapat disimpan", err12
		}

		return res, "Data berhasil disimpan", nil
	} else {
		udpate := igd.DGeneralConsent{
			PJawabNama:     req.PJawabNama,
			PJawabAlamat:   req.PJawabAlamat,
			PJawabTglLahir: req.PJawabTglLahir,
			PJawabNoHP:     req.PJawabNoHP,
			Pewenang:       req.Pewenang,
		}

		_, terupdate := iu.igdRepository.OnUpdatgeGeneralConsentRepository(udpate, req.NORM, req.KdBagian)

		if terupdate != nil {
			return res, "Tidak dapat diubah", terupdate
		}
		return res, "Data berhasil diubah", nil
	}
}

func (iu *igdUseCase) OnReportGeneralCOnsentUseCase(req dto.ReportGeneralConsent) (res dto.ResponseReportGeneralConsent) {
	pasien, _ := iu.userRepo.OnGetProfilePasienRepository(req.NORM)
	general, _ := iu.igdRepository.OnGetGeneralConsentRAJALRepository(req.NORM, req.KdBagian)
	pasienMapper := iu.userMapper.ToMappingProfilePasien(pasien)
	karyawan, _ := iu.userRepo.GetKaryawanRepository(general.UserID)

	mapperGeneral := iu.igdMapper.ToMappingReportGeneralConsent(pasienMapper, general, karyawan)
	return mapperGeneral
}
