package usecase

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"time"
)

func (iu *igdUseCase) OnSavePengkajianKehamilanAnakUseCase(req dto.OnSavePengkajianKehamilanAnak, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if er11 != nil || pengkajian.Noreg == "" {
		var data = igd.PengkajianKeperawatan{
			InsertDttm:       time.Now(),
			KdBagian:         req.KdBagian,
			Noreg:            req.Noreg,
			Usia:             req.Usia,
			Pelayanan:        req.Pelayanan,
			UserID:           userID,
			RiwayatKehamilan: req.RiwayatKehamilan,
		}

		_, e11 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(data)

		if e11 != nil {
			return "Data gagal disimpan", e11
		}

		return "Data berhasil disimpan", nil

	} else {
		var data = igd.PengkajianKeperawatan{
			KdBagian:         req.KdBagian,
			RiwayatKehamilan: req.RiwayatKehamilan,
		}

		_, err11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(data, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if err11 != nil {
			return "Data gagal disimpan", err11
		}

		return "Data berhasi diupdate", nil
	}
}
