package usecase

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"hms_api/modules/nyeri"
	DTONyeri "hms_api/modules/nyeri/dto"
	"time"
)

func (iu *igdUseCase) OnGetPengkajianNutrisiIGDUseCase(req dto.OnGetPengkajianIGD) (res DTONyeri.ToResponsePengkajianNutrisi, err error) {
	nyeri, _ := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, "ANAK")
	mapper := iu.nyeriMapper.ToMappingToResponsePengkajianNutrisi(nyeri)

	return mapper, nil
}

func (iu *igdUseCase) OnGetPengkajianNutrisiDewasaIGDUseCase(req dto.OnGetPengkajianIGD) (res DTONyeri.ToResponsePengkajianNutrisiDewasa, err error) {
	nutrisi, _ := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, "DEWASA")
	mapper := iu.nyeriMapper.ToMappingResponsePengkajianNutrisi(nutrisi)

	return mapper, nil
}

func (iu *igdUseCase) OnSavePengkajianAlergiAnakIGDAnakUseCase(req dto.OnSaveRiwayatAlergi, userID string) (message string, err error) {
	alergi, err11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if err11 != nil || alergi.Noreg == "" {
		save := igd.PengkajianKeperawatan{
			InsertDttm:    time.Now(),
			Usia:          req.Usia,
			Pelayanan:     req.Pelayanan,
			KdBagian:      req.KdBagian,
			Noreg:         req.Noreg,
			UserID:        userID,
			RiwayatAlergi: req.RiwayatAlergi,
			ReaksiAlergi:  req.ReaksiAlergi,
		}

		// OnSavePengkajianKeperawatanRespository
		_, er11 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(save)

		if er11 != nil {
			return "Data gagal disimpan", er11
		}

		return "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		update := igd.PengkajianKeperawatan{
			RiwayatAlergi: req.RiwayatAlergi,
			ReaksiAlergi:  req.ReaksiAlergi,
		}

		_, er11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(update, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if er11 != nil {
			return "Data gagal diubah", er11
		}

		return "Data berhasil diubah", nil
	}
}

func (iu *igdUseCase) OnSavePengkajianNutrisiIGDUseCase(req dto.OnSavePengkajianNutrisiIGD, userID string, deviceID string) (res DTONyeri.ToResponsePengkajianNutrisi, message string, err error) {
	// CEK PENGKAJIAN NYERI
	dataNyeri, err11 := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, "ANAK")

	if err11 != nil || dataNyeri.Noreg == "" {
		// SAVE DATA
		save := nyeri.DPengkajianNutrisi{
			InsertDttm:   time.Now(),
			UpdDttm:      time.Now(),
			InsertUserID: userID,
			InsertPc:     deviceID,
			Usia:         req.Usia,
			Pelayanan:    req.Pelayanan,
			KdBagian:     req.KdBagian,
			Noreg:        req.Noreg,
			N1:           req.N1,
			N2:           req.N2,
			N3:           req.N3,
			N4:           req.N4,
			Nilai:        req.Nilai,
		}

		_, er11 := iu.nyeriRepository.OnSavePengkajianNyeriRepository(save)

		if er11 != nil {
			return res, "Data gagal disimpan", er11
		}

		return res, "Data berhasil disimpan", nil
	} else {

		update := nyeri.DPengkajianNutrisi{
			Noreg: req.Noreg,
			N1:    req.N1,
			N2:    req.N2,
			N3:    req.N3,
			N4:    req.N4,
			Usia:  req.Usia,
			Nilai: req.Nilai,
		}

		_, er11 := iu.nyeriRepository.OnUpdatePengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Usia, update)

		if er11 != nil {
			return res, "Data gagal diupdate", er11
		}

		return res, "Data berhasil diupdate", nil
	}
}

func (iu *igdUseCase) OnSavePengkajianNutrisiDewasaIGDUseCase(req dto.OnSavePengkajianNutrisiDewasaIGD, userID string, deviceID string) (res DTONyeri.ToResponsePengkajianNutrisi, message string, err error) {
	// CEK PENGKAJIAN NYERI
	dataNyeri, err11 := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Usia)

	if err11 != nil || dataNyeri.Noreg == "" {
		// SAVE DATA
		save := nyeri.DPengkajianNutrisi{
			InsertDttm:   time.Now(),
			UpdDttm:      time.Now(),
			InsertUserID: userID,
			InsertPc:     deviceID,
			Usia:         req.Usia,
			Pelayanan:    req.Pelayanan,
			KdBagian:     req.KdBagian,
			Noreg:        req.Noreg,
			N1:           req.N1,
			N2:           req.N2,
			Nilai:        req.Nilai,
		}

		_, er11 := iu.nyeriRepository.OnSavePengkajianNyeriRepository(save)

		if er11 != nil {
			return res, "Data gagal disimpan", er11
		}

		return res, "Data berhasil disimpan", nil
	} else {

		update := nyeri.DPengkajianNutrisi{
			Noreg: req.Noreg,
			N1:    req.N1,
			N2:    req.N2,
			Usia:  req.Usia,
			Nilai: req.Nilai,
		}

		_, er11 := iu.nyeriRepository.OnUpdatePengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Usia, update)

		if er11 != nil {
			return res, "Data gagal diupdate", er11
		}

		return res, "Data berhasil diupdate", nil
	}
}
