package usecase

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"time"
)

func (iu *igdUseCase) OnGetResikoJatuhPasienDewasaIGDUseCase(req dto.OnGetResikoJatuhPasienDewasa) (res dto.ResponseResikoJatuhDewasaIGD, err error) {
	resiko, _ := iu.igdRepository.OnGetResikoJatuhIGDDewasaRepository(req.Pelayanan, req.KdBagian, req.Noreg)
	mapper := iu.igdMapper.ToResponsePengkajianResikoJatuhIGDDewasa(resiko)

	return mapper, nil
}

// DETEKSI RESIKO JATUH
func (iu *igdUseCase) OnDeteksiResikoJatuhIGDUseCase(req dto.OnDeteksiResikoJatuh) (res dto.ResponseDeteksiResikoJatuhIGD) {
	data, _ := iu.igdRepository.OnGetDeteksiResikoJatuhRespository(req.Noreg, "IGD001")
	mapping := iu.igdMapper.ToMappingDeteksiResikoJatuh(data)
	return mapping
}

func (iu *igdUseCase) OnSaveResikoJatuhPasienDewasaIGDUseCase(req dto.OnSavePengkajianResikoJatuhPasienDewasaIGD, userID string) (messretuage string, err error) {
	resiko, er11 := iu.igdRepository.OnGetResikoJatuhIGDDewasaRepository(req.Pelayanan, req.KdBagian, req.Noreg)

	if er11 != nil || resiko.NOReg == "" {
		data := igd.DRisikoJatuh{
			InsertDttm:      time.Now(),
			KetPerson:       "Perawat",
			Pelayanan:       req.Pelayanan,
			InserUserId:     userID,
			KDBagian:        req.KdBagian,
			NOReg:           req.Noreg,
			Kategori:        req.Kategori,
			Total:           req.Total,
			RJatuh:          req.RiwayatJatuh,
			Diagnosis:       req.Diagnosis,
			AlatBantu1:      req.AlatBantuJalan,
			TerpasangInfuse: req.MenggunakanInfuse,
			GayaBerjalan:    req.CaraBerjalan,
			StatusMental:    req.StatusMental,
			InsertPC:        req.InsertDevice,
		}
		_, er11 := iu.igdRepository.OnSaveResikoJatuhIGDDewasaRepository(data)

		if er11 != nil {
			return "Data gagal disimpan", er11
		}
		return "Data berhasil disimpan", nil
	} else {
		data := igd.DRisikoJatuh{
			RJatuh:          req.RiwayatJatuh,
			Diagnosis:       req.Diagnosis,
			AlatBantu1:      req.AlatBantuJalan,
			TerpasangInfuse: req.MenggunakanInfuse,
			GayaBerjalan:    req.CaraBerjalan,
			Total:           req.Total,
			StatusMental:    req.StatusMental,
		}

		_, er11 := iu.igdRepository.OnUpdateResikoJatuhIGDDewasaRepository(data, req.KdBagian, req.Noreg, req.Kategori)

		if er11 != nil {
			return "Data gagal diupdate", er11
		}
		return "Data berhasil diupdate", nil
	}

}
