package usecase

import (
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"time"
)

func (iu *igdUseCase) OnGetKondisiTindakLanjutUseCase(req dto.OnGetTindakLanjut) (res dto.ResponseTindakLajutIGD) {
	pengkajian, _ := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	dokter, _ := iu.igdRepository.OnGetAllDPJPRepository()
	pelayanan, _ := iu.libRepository.GetKPelayananRepository()

	tindakLajut := iu.igdMapper.ToMappingResponseTindakLanjutIGD(pengkajian, dokter, pelayanan)
	return tindakLajut
}

func (iu *igdUseCase) OnSaveTindakLanjutUseCase(req dto.OnSaveTindakLajutIGD, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if pengkajian.Noreg == "" || er11 != nil {
		save := igd.PengkajianKeperawatan{
			InsertDttm:         time.Now(),
			Usia:               req.Usia,
			Pelayanan:          req.Pelayanan,
			KdBagian:           req.KdBagian,
			Noreg:              req.Noreg,
			UserID:             userID,
			Indikasi:           req.Indikasi,
			Ruangan:            req.Ruangan,
			KondisiPasien:      req.KondisiPasien,
			DPJP:               req.DPJP,
			AlasanRujukan:      req.AlasanRujukan,
			TindakLanjut:       req.TindakLanjut,
			KeteranganRujuk:    req.KeteranganRujuk,
			TransportasiPulang: req.TransportasiPulang,
			DischargePlanning:  req.DichartPlanning,
			PendidikanPasien:   req.PendidikanPasien,
		}
		_, er111 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(save)

		if er11 != nil {
			return "Data gagal disimpan", er111
		}
		return "Data berhasil disimpan", nil
	} else {

		var update = igd.PengkajianKeperawatan{}

		if req.TindakLanjut == "DIRAWAT" {
			update = igd.PengkajianKeperawatan{
				Indikasi:        req.Indikasi,
				Ruangan:         req.Ruangan,
				KondisiPasien:   req.KondisiPasien,
				DPJP:            req.DPJP,
				TindakLanjut:    req.TindakLanjut,
				AlasanRujukan:   "-",
				KeteranganRujuk: "-",
			}
		}

		if req.TindakLanjut == "DIRUJUK" {
			update = igd.PengkajianKeperawatan{
				Indikasi:           "-",
				Ruangan:            "-",
				KondisiPasien:      "-",
				DPJP:               "-",
				TindakLanjut:       req.TindakLanjut,
				AlasanRujukan:      req.AlasanRujukan,
				KeteranganRujuk:    req.KeteranganRujuk,
				TransportasiPulang: "-",
			}
		}

		if req.TindakLanjut == "PULANG" {
			update = igd.PengkajianKeperawatan{
				TransportasiPulang: req.TransportasiPulang,
				KondisiPasien:      req.KondisiPasien,
				Indikasi:           "-",
				Ruangan:            "-",
				DPJP:               "-",
				TindakLanjut:       req.TindakLanjut,
				AlasanRujukan:      "-",
				KeteranganRujuk:    "-",
			}
		}

		_, er11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(update, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if er11 != nil {
			return "Data gagal diupdate", er11
		}

		return "Data berhasil diupdate", nil
	}

}

func (iu *igdUseCase) OnGetAsesmenKeperawatanUseCase(req dto.OnGetAsesmenKeperawatanIGD) (res dto.ResponsePengkajianKeperawatanAnak) {
	// GET ASESMEN
	pengkajian, _ := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	asesmenIGD, _ := iu.igdRepository.OnGeAsesmenDokter(req.Noreg, req.KdBagian)

	mapper := iu.igdMapper.ToResponseAsesmen(pengkajian, asesmenIGD)
	return mapper
}

func (iu *igdUseCase) OnGetAsesmenKeperawatanRanapUseCase(req dto.OnGetAsesemenKeperawatanRANAP) (res dto.ResponsePengkajianKeperawatanRANAP) {

	pengkajian, _ := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	asesmenIGD, _ := iu.igdRepository.OnGetAsesmenDokterRANAPRepository(req.Noreg, req.KdBagian)

	mapper := iu.igdMapper.ToResponseAsesemenRANAP(pengkajian, asesmenIGD)

	return mapper
}

func (iu *igdUseCase) OnSaveAsesmenKeperawatanANAKRanapUseCase(req dto.OnSaveAsesmenKeperawatanANAK, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	if pengkajian.Noreg == "" || er11 != nil {
		save := igd.PengkajianKeperawatan{
			InsertDttm:              time.Now(),
			Usia:                    req.Usia,
			Pelayanan:               req.Pelayanan,
			KdBagian:                req.KdBagian,
			Noreg:                   req.Noreg,
			UserID:                  userID,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
		}

		_, er111 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(save)

		if er111 != nil {
			return "Data gagal disimpan", er111
		}

		return "Data berhasil disimpan", nil
	} else {
		update := igd.PengkajianKeperawatan{
			KeluhanUtama:            req.KeluhanUtama,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
		}

		_, er11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(update, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if er11 != nil {
			return "Data gagal diupdate", er11
		}

		return "Data berhasil diupdate", nil
	}
}

func (iu *igdUseCase) OnSaveAsesmenKeperawatanUseCase(req dto.OnSaveAsesmenKeperawatanIGD, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if pengkajian.Noreg == "" || er11 != nil {
		// SAVE DATA
		save := igd.PengkajianKeperawatan{
			InsertDttm:              time.Now(),
			Usia:                    req.Usia,
			Pelayanan:               req.Pelayanan,
			KdBagian:                req.KdBagian,
			Noreg:                   req.Noreg,
			UserID:                  userID,
			CaraMasuk:               req.CaraMasuk,
			KeluhanUtama:            req.KeluhanUtama,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
			AsalPasien:              req.AsalPasien,
		}

		_, er111 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(save)

		if er111 != nil {
			return "Data gagal disimpan", er111
		}

		return "Data berhasil disimpan", nil
	} else {
		update := igd.PengkajianKeperawatan{
			CaraMasuk:               req.CaraMasuk,
			KeluhanUtama:            req.KeluhanUtama,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
			AsalPasien:              req.AsalPasien,
		}

		_, er11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(update, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if er11 != nil {
			return "Data gagal diupdate", er11
		}

		return "Data berhasil diupdate", nil
	}
}

func (iu *igdUseCase) OnSaveAsesmenRANAPKeperawatanUseCase(req dto.OnSaveAsesmenKeperawatanRANAP, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if pengkajian.Noreg == "" || er11 != nil {
		save := igd.PengkajianKeperawatan{
			InsertDttm:              time.Now(),
			Usia:                    req.Usia,
			Pelayanan:               req.Pelayanan,
			KdBagian:                req.KdBagian,
			Noreg:                   req.Noreg,
			UserID:                  userID,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
		}

		_, er111 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(save)

		if er111 != nil {
			return "Data gagal disimpan", er111
		}

		return "Data berhasil disimpan", nil
	} else {
		update := igd.PengkajianKeperawatan{
			KeluhanUtama:            req.KeluhanUtama,
			RiwayatPenyakitSekarang: req.RiwayatPenyakitSekarang,
			RiwayatPenyakitDahulu:   req.RiwayatPenyakitDahulu,
			RiwayatPenyakitKeluarga: req.RiwayatPenyakitKeluarga,
			Anamnesa:                req.Pengkajian,
		}

		_, er11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(update, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if er11 != nil {
			return "Data gagal diupdate", er11
		}

		return "Data berhasil diupdate", nil
	}
}
