package usecase

import (
	"errors"
	"hms_api/modules/igd"
	"hms_api/modules/igd/dto"
	"hms_api/modules/igd/entity"
	kebidananEntity "hms_api/modules/kebidanan/entity"
	libEntity "hms_api/modules/lib/entity"
	"hms_api/modules/nyeri"
	nyeriEntity "hms_api/modules/nyeri/entity"
	rmeEntity "hms_api/modules/rme/entity"
	soapEntity "hms_api/modules/soap/entity"
	userEntity "hms_api/modules/user/entity"

	// igdEntity "hms_api/modules/igd/entity"
	"time"

	"github.com/sirupsen/logrus"
)

type igdUseCase struct {
	logging             *logrus.Logger
	igdMapper           entity.IGDMapper
	igdRepository       entity.IGDRepository
	rmeRepository       rmeEntity.RMERepository
	nyeriRepository     nyeriEntity.NyeriRepository
	nyeriMapper         nyeriEntity.NyeriMapper
	userRepo            userEntity.UserRepository
	userMapper          userEntity.IUserMapper
	libRepository       libEntity.LibRepository
	soapRepository      soapEntity.SoapRepository
	soapUseCase         soapEntity.SoapUseCase
	rmeMapper           rmeEntity.RMEMapper
	kebidananRepository kebidananEntity.KebidananRepository
}

func NewIgdUseCase(ir entity.IGDRepository, logging *logrus.Logger, mapper entity.IGDMapper, rmeRepo rmeEntity.RMERepository, nyeriRepo nyeriEntity.NyeriRepository, nyeriMapper nyeriEntity.NyeriMapper, userRepo userEntity.UserRepository, userMapper userEntity.IUserMapper, libRepo libEntity.LibRepository, soapRepo soapEntity.SoapRepository, soapUseCase soapEntity.SoapUseCase, rmeMaper rmeEntity.RMEMapper, kebidananRepo kebidananEntity.KebidananRepository) entity.IGDUseCase {
	return &igdUseCase{
		logging:             logging,
		igdMapper:           mapper,
		igdRepository:       ir,
		rmeRepository:       rmeRepo,
		nyeriRepository:     nyeriRepo,
		nyeriMapper:         nyeriMapper,
		userRepo:            userRepo,
		userMapper:          userMapper,
		libRepository:       libRepo,
		soapRepository:      soapRepo,
		soapUseCase:         soapUseCase,
		rmeMapper:           rmeMaper,
		kebidananRepository: kebidananRepo,
	}
}

func (iu *igdUseCase) OnSavePengkajianPersistemIGDUseCase(req dto.OnSavePengkajianPersistemIGD) (res igd.DpengkajianPersistem, message string, err error) {
	get, e111 := iu.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.Noreg, req.Usia, req.Pelayanan)

	if e111 != nil || get.NoReg == "" {
		data, er123 := iu.igdRepository.OnSavePengkajianPersisetmIGDRepository(req)

		if er123 != nil {
			return res, "Data gagal disimpan", er123
		}

		return data, "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		updates := igd.DpengkajianPersistem{
			AirWays: req.Airways, Breathing: req.Breathing, Circulation: req.Circulation,
			DisabilityNeurologis: req.Disability, SistemPerkemihan: req.SistemPerkemihan, SistemPencernaan: req.SistemPencernaan, SistemIntegumenMusculoskletal: req.SistemIntegumen,
			KebidananKandungan: req.KebidananKandungan, ThtMata: req.THTMATA, Metabolisme: req.MetaBolisme, PsikiatriPsikologis: req.PsikiatriPsikologis, SosioKultural: req.SosioKulural,
		}
		update, er11 := iu.igdRepository.OnUpdateDataPengkajianPersistemIGDRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Usia, updates)

		if er11 != nil {
			return res, "Data gagal diubah", er11
		}

		return update, "Data berhasil diubah", nil
	}
}

func (iu *igdUseCase) OnGetReportPengkajianKeperawatanInapAnakUseCase(req dto.OnReportAsesmenKeperawatanAnak) (res dto.ResponsePengkajianKeperawatanRawatInapAnak) {
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	pengobatan, _ := iu.igdRepository.OnGetRiwayatPengobatanDiRumahRepository(req.Usia, req.KdBagian, req.Noreg)
	riwayatAlergi, _ := iu.rmeRepository.OnGetRiwayatAlergiPasienRepository(req.NoRM, req.KdBagian)
	nutrisi, _ := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Usia)
	mapperNutrisi := iu.nyeriMapper.ToMappingToResponsePengkajianNutrisi(nutrisi)
	persistem, _ := iu.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.Noreg, req.Usia, req.Pelayanan)
	asuhan, _ := iu.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(req.Noreg, req.KdBagian)
	fisik, _ := iu.rmeRepository.GetPemeriksaanFisikAnakBangsalRepository(req.Noreg, req.KdBagian)
	mapperFisik := iu.rmeMapper.ToPemeriksaanFisikIGDResponse(fisik)
	vital, _ := iu.soapRepository.GetTandaVitalBangsalRepository(req.Noreg, req.KdBagian)
	// GetTandaVitalBangsalRepository

	mapper := iu.igdMapper.ToMappingPengkajianRawatInapAnak(pengkajian, pengobatan, riwayatAlergi, mapperNutrisi, persistem, asuhan, mapperFisik, vital)
	return mapper
}

func (iu *igdUseCase) OnGetPengkajianIGDDewasaUseCase(req dto.OnReportPengkajianPersistemIGD) (res dto.ResponsePengkajianKeperawatanIGDDewasa, err error) {
	persistem, _ := iu.igdRepository.OnGetPengkajianPersistemIGDRepository("IGD001", req.Noreg, req.Usia, req.Pelayanan)
	vitals, _ := iu.soapUseCase.OnGetTandaVitalIGDPerawatUserCase("IGD001", "Dokter", req.Noreg, "rajal")
	maperSistem := iu.igdMapper.ToMappingPengkajianPersistemIGD(persistem, vitals)
	asuhan, _ := iu.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(req.Noreg, "IGD001")
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository("IGD001", req.Pelayanan, req.Usia, req.Noreg)
	cppt, _ := iu.igdRepository.OnGetCPPPTRepository(req.Noreg, "IGD001")
	pengobatan, _ := iu.igdRepository.OnGetRiwayatPengobatanDiRumahRepository(req.Usia, "IGD001", req.Noreg)
	nyeri, _ := iu.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.Noreg, "IGD001", req.Pelayanan, "AWAL")
	mapperNyeri := iu.nyeriMapper.ToMapperResponsePengkajianAwalNyeri(nyeri)

	resiko, _ := iu.igdRepository.OnGetResikoJatuhIGDDewasaRepository(req.Pelayanan, "IGD001", req.Noreg)
	mapperResiko := iu.igdMapper.ToResponsePengkajianDewasaReport(resiko)
	nutrisi, _ := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, "IGD001", req.Pelayanan, "DEWASA")
	mapperNutrisi := iu.nyeriMapper.ToMappingResponsePengkajianNutrisi(nutrisi)
	perawat, _ := iu.userRepo.CariUserPerawatRepository(pengkajian.UserID)

	fisi, _ := iu.rmeRepository.GetPemeriksaanFisikReportIGDRepository(req.Noreg)
	mapperFisik := iu.rmeMapper.ToPemeriksaanFisikIGDResponse(fisi)
	vital, _ := iu.soapRepository.GetTandaVitalIGDReportdDokterRepository(req.Noreg)

	mapper := iu.igdMapper.ToResponseReportPengkajianKeperawatanIGDDewasa(maperSistem, asuhan, pengkajian, cppt, pengobatan, mapperNyeri, mapperResiko, mapperNutrisi, perawat, mapperFisik, vital)
	return mapper, nil
}

func (iu *igdUseCase) OnGetReportPengkajianPersistemIGDUsecase(req dto.OnReportPengkajianPersistemIGD) (res dto.ResponseReportPengkajianIGDANAK, err error) {
	persistem, _ := iu.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.Noreg, req.Usia, req.Pelayanan)
	vitals, _ := iu.soapUseCase.OnGetTandaVitalIGDPerawatUserCase(req.KdBagian, "Dokter", req.Noreg, "rajal")
	maperSistem := iu.igdMapper.ToMappingPengkajianPersistemIGD(persistem, vitals)
	asuhan, _ := iu.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(req.Noreg, req.KdBagian)
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	cppt, _ := iu.igdRepository.OnGetCPPPTRepository(req.Noreg, req.KdBagian)
	pengobatan, _ := iu.igdRepository.OnGetRiwayatPengobatanDiRumahRepository(req.Usia, req.KdBagian, req.Noreg)
	nyeri, _ := iu.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, "AWAL")
	mapperNyeri := iu.nyeriMapper.ToMapperResponsePengkajianAwalNyeri(nyeri)
	nutrisi, _ := iu.nyeriRepository.OnGetPengkajianNutrisiRepository(req.Noreg, req.KdBagian, req.Pelayanan, "ANAK")
	mapperNutrisi := iu.nyeriMapper.ToMappingToResponsePengkajianNutrisi(nutrisi)

	perawat, _ := iu.userRepo.CariUserPerawatRepository(pengkajian.UserID)
	riwayatAlergi, _ := iu.rmeRepository.OnGetRiwayatAlergiPasienRepository(req.NoRM, req.KdBagian)
	resikoJatuh, _ := iu.igdRepository.OnGetDResikoJatuhIGDRajalRepositroy(req.KdBagian, req.Noreg, "Perawat")

	vital, _ := iu.soapRepository.GetTandaVitalIGDReportdDokterRepository(req.Noreg)
	fisi, _ := iu.rmeRepository.GetPemeriksaanFisikReportIGDRepository(req.Noreg)
	mapperFisik := iu.rmeMapper.ToPemeriksaanFisikIGDResponse(fisi)

	// =============== GET NYERI
	mapper := iu.igdMapper.ToMappingReportPengkajianPersistem(maperSistem, asuhan, pengkajian, cppt, pengobatan, mapperNyeri, mapperNutrisi, riwayatAlergi, resikoJatuh, perawat, mapperFisik, vital)
	return mapper, nil
}

func (iu *igdUseCase) OnReportPengkajianKeperawatanIGDUDewasaUseCase(req dto.OnReportPengkajianDewasaIGD) (res dto.ResponseReportPengkajianDewasaIGD, err error) {
	persistem, _ := iu.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.Noreg, req.Usia, req.Pelayanan)
	asuhan, _ := iu.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(req.Noreg, req.KdBagian)
	vitals, _ := iu.soapUseCase.OnGetTandaVitalIGDPerawatUserCase(req.KdBagian, "Dokter", req.Noreg, "rajal")
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Noreg, req.Usia, req.Pelayanan)
	cppt, _ := iu.igdRepository.OnGetCPPPTRepository(req.Noreg, req.KdBagian)

	maperSistem := iu.igdMapper.ToMappingPengkajianPersistemIGD(persistem, vitals)
	mapper := iu.igdMapper.ToMappingReportPengkajianIGDDewasa(maperSistem, asuhan, pengkajian, cppt)

	return mapper, nil
}

func (iu *igdUseCase) OnGetPengkajianIGDAnakUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianIGD, err error) {
	pengobatan, _ := iu.igdRepository.OnGetRiwayatPengobatanDiRumahRepository(req.Usia, req.KdBagian, req.Noreg)
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	mapper := iu.igdMapper.ToMappingAsesmenKeperawatanIGD(pengkajian, pengobatan)
	return mapper, nil
}

func (ig *igdUseCase) OnGetPengkajianALergiIGDUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianAlergiIGD) {
	alergi, _ := ig.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	mapperData := ig.igdMapper.ToMappingPengkajianAlergi(alergi)

	return mapperData
}

func (iu *igdUseCase) OnGetPengkajianNyeriIGDAnakUseCase(req dto.OnGetPengkajianIGD) (res dto.ResponsePengkajianNyeriIGD, err error) {
	pengkajian, _ := iu.igdRepository.OnGetPengkajianKeperawatanIGDDewasaRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)
	nyeri, _ := iu.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, "AWAL")
	mapperNyeri := iu.nyeriMapper.ToMapperResponsePengkajianAwalNyeri(nyeri)
	mapper := iu.igdMapper.TOMapperResponsePengkajianNyeriIGD(pengkajian, mapperNyeri)

	return mapper, nil
}

func (iu *igdUseCase) OnSavePengobatanDiRumahIGDUseCase(req dto.OnSavePengobatanDirumahIGD, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if er11 != nil || pengkajian.Noreg == "" {
		var data = igd.PengkajianKeperawatan{
			InsertDttm:               time.Now(),
			KdBagian:                 req.KdBagian,
			UserID:                   userID,
			Noreg:                    req.Noreg,
			Usia:                     req.Usia,
			Pelayanan:                req.Pelayanan,
			RiwayatPengobatanDirumah: req.RiwayatPengobatan,
		}
		_, e11 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(data)

		if e11 != nil {
			return "Data gagal disimpan", e11
		}

		return "Data berhasil disimpan", nil

	} else {
		// UPDATE DATA
		var data = igd.PengkajianKeperawatan{
			InsertDttm:               time.Now(),
			KdBagian:                 req.KdBagian,
			Noreg:                    req.Noreg,
			Usia:                     req.Usia,
			RiwayatPengobatanDirumah: req.RiwayatPengobatan,
		}

		_, err11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(data, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if err11 != nil {
			return "Data gagal disimpan", err11
		}

		return "Data berhasi diupdate", nil
	}
}

func (iu *igdUseCase) OnSavePengkajianNyeriIGDUseCase(req dto.OnSaveNyeriIGD, userID string) (message string, err error) {
	pengkajian, er11 := iu.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.Noreg)

	if er11 != nil || pengkajian.Noreg == "" {
		var data = igd.PengkajianKeperawatan{
			InsertDttm: time.Now(),
			KdBagian:   req.KdBagian,
			Noreg:      req.Noreg,
			Usia:       req.Usia,
			Pelayanan:  req.Pelayanan,
			Nyeri:      req.Nyeri,
		}
		_, e11 := iu.igdRepository.OnSavePengkajianKeperawatanRespository(data)

		if e11 != nil {
			return "Data gagal disimpan", e11
		}
		// JIKA ADA NYERI
		if req.Nyeri == "ADA" {
			if req.SkorNyeri == 0 {
				return "Skala Nyeri tidak boleh kosong", errors.New("skala Nyeri tidak boleh kosong")
			}
			// LAKUKAN SIMPAN NYERI
			if req.Lokasi == "" {
				return "Lokasi nyeri tidak boleh kosong", errors.New("lokasi nyeri tidak boleh kosong")
			}
			// CEK APAKAH ADA ASESMEN ULANG NYERI
			if req.Penyebaran == "" {
				return "Penyebaran tidak boleh kosong", errors.New("penyebaran tidak boleh kosong")
			}

			if req.Penyebab == "" {
				return "Penyebab tidak boleh kosong", errors.New(("penyebab tidak boleh kosong"))
			}

			if req.Waktu == "" {
				return "Waktu tidak boleh kosong", errors.New("waktu tidak boleh kosong")
			}

			nyeriCek, er11 := iu.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Asesmen)

			if er11 != nil || nyeriCek.Noreg == "" {
				var nyeri = nyeri.DasesmenUlangNyeri{
					InsertDttm: time.Now(),
					Noreg:      req.Noreg,
					KdBagian:   req.KdBagian,
					Metode:     req.Metode,
					Asesmen:    req.Asesmen,
					SkorNyeri:  req.SkorNyeri,
					Penyebab:   req.Penyebab,
					WaktuKaji:  req.WaktuKaji,
					Kualitas:   req.Kualitas,
					Penyebaran: req.Penyebaran,
					Keparahan:  req.Keparahan,
					Waktu:      req.Waktu,
					Lokasi:     req.Lokasi,
					HasilNyeri: req.HasilNyeri,
					UserID:     userID,
				}
				iu.nyeriRepository.OnSaveAsesmenUlangNyeriRepository(nyeri)
				iu.logging.Info("LAKUKAN SIMPAN PENGKAJIAN NYERI")
			}

			if er11 == nil || len(nyeriCek.Noreg) > 1 {
				// UPDATE DATA
				var nyeri = nyeri.DasesmenUlangNyeri{
					Metode:     req.Metode,
					Asesmen:    req.Asesmen,
					SkorNyeri:  req.SkorNyeri,
					Penyebab:   req.Penyebab,
					WaktuKaji:  req.WaktuKaji,
					Kualitas:   req.Kualitas,
					Penyebaran: req.Penyebaran,
					Keparahan:  req.Keparahan,
					Waktu:      req.Waktu,
					Lokasi:     req.Lokasi,
					HasilNyeri: req.HasilNyeri,
				}
				iu.nyeriRepository.OnUpdateNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Asesmen, nyeri)

				iu.logging.Info("UPDATE DATA NYERI")
			}

			return "Data berhasil diupdate", nil
		}

		return "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA

		var data = igd.PengkajianKeperawatan{
			InsertDttm: time.Now(),
			KdBagian:   req.KdBagian,
			Noreg:      req.Noreg,
			Usia:       req.Usia,
			Nyeri:      req.Nyeri,
		}

		_, err11 := iu.igdRepository.OnUpdatePengkajianKeperawatanRepository(data, req.Noreg, req.Usia, req.KdBagian, req.Pelayanan)

		if err11 != nil {
			return "Data gagal disimpan", err11
		}

		// JIKA ADA NYERI
		if req.Nyeri == "ADA" {
			if req.SkorNyeri == 0 {
				return "Skala Nyeri tidak boleh kosong", errors.New("skala Nyeri tidak boleh kosong")
			}
			// LAKUKAN SIMPAN NYERI
			if req.Lokasi == "" {
				return "Lokasi nyeri tidak boleh kosong", errors.New("lokasi nyeri tidak boleh kosong")
			}
			// CEK APAKAH ADA ASESMEN ULANG NYERI
			if req.Penyebaran == "" {
				return "Penyebaran tidak boleh kosong", errors.New("penyebaran tidak boleh kosong")
			}

			if req.Penyebab == "" {
				return "Penyebab tidak boleh kosong", errors.New(("penyebab tidak boleh kosong"))
			}

			if req.Waktu == "" {
				return "Waktu tidak boleh kosong", errors.New("waktu tidak boleh kosong")
			}

			// LAKUKAN SIMPAN NYERI
			// CEK APAKAH ADA ASESMEN ULANG NYERI
			nyeriCek, er11 := iu.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Asesmen)

			if er11 != nil || nyeriCek.Noreg == "" {
				var nyeri = nyeri.DasesmenUlangNyeri{
					InsertDttm: time.Now(),
					Noreg:      req.Noreg,
					KdBagian:   req.KdBagian,
					Metode:     req.Metode,
					Asesmen:    req.Asesmen,
					SkorNyeri:  req.SkorNyeri,
					Penyebab:   req.Penyebab,
					WaktuKaji:  req.WaktuKaji,
					Kualitas:   req.Kualitas,
					Penyebaran: req.Penyebaran,
					Keparahan:  req.Keparahan,
					Waktu:      req.Waktu,
					Lokasi:     req.Lokasi,
					HasilNyeri: req.HasilNyeri,
					UserID:     userID,
				}
				iu.nyeriRepository.OnSaveAsesmenUlangNyeriRepository(nyeri)
				iu.logging.Info("LAKUKAN SIMPAN PENGKAJIAN NYERI")
			}

			if er11 == nil || len(nyeriCek.Noreg) > 1 {
				// UPDATE DATA
				var nyeri = nyeri.DasesmenUlangNyeri{
					Metode:     req.Metode,
					Asesmen:    req.Asesmen,
					SkorNyeri:  req.SkorNyeri,
					Penyebab:   req.Penyebab,
					WaktuKaji:  req.WaktuKaji,
					Kualitas:   req.Kualitas,
					Penyebaran: req.Penyebaran,
					Keparahan:  req.Keparahan,
					Waktu:      req.Waktu,
					Lokasi:     req.Lokasi,
					HasilNyeri: req.HasilNyeri,
				}
				iu.nyeriRepository.OnUpdateNyeriRepository(req.Noreg, req.KdBagian, req.Pelayanan, req.Asesmen, nyeri)

				iu.logging.Info("UPDATE DATA NYERI")
			}

			return "Data berhasil diupdate", nil
		}

		return "Data berhasi diupdate", nil
	}

}
