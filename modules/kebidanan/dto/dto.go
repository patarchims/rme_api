package dto

import (
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	"hms_api/modules/user"
)

type (
	// ================ ON GET ASESMEN IGD
	OnGetAsesmenIGD struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ResponseAsesmenIGD struct {
		Noreg                          string `json:"no_reg"`
		AseskepSistemMerokok           string `json:"sistem_merokok"`
		AseskepSistemMinumAlkohol      string `json:"sistem_minum_alkohol"`
		AseskepSistemSpikologis        string `json:"sistem_spikologis"`
		AseskepSistemGangguanJiwa      string `json:"sistem_gangguan_jiwa"`
		AseskepSistemBunuhDiri         string `json:"sistem_bunuh_diri"`
		AseskepSistemTraumaPsikis      string `json:"sistem_trauma_psikis"`
		AseskepSistemHambatanSosial    string `json:"sistem_hambatan_sosial"`
		AseskepSistemHambatanSpiritual string `json:"sistem_hambatan_spiritual"`
		AseskepSistemHambatanEkonomi   string `json:"sistem_hambatan_ekonomi"`
		AseskepSistemPenghasilan       string `json:"sistem_penghasilan"`
		AseskepSistemKultural          string `json:"sistem_kultural"`
		AseskepSistemAlatBantu         string `json:"sistem_alat_bantu"`
		AseskepSistemKebutuhanKhusus   string `json:"sistem_kebutuhan_khusus"`
	}

	ReqNoregV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqGetVitalSignBidan struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqDeleteDiagnosaKebidanan struct {
		NoDiagnosa int `json:"no_diagnosa" validate:"required"`
	}

	ReqAsesmenAwalKebidanan struct {
		NoReg                   string `json:"no_reg"`
		DeviceID                string `json:"device_id"`
		KeluhanUtama            string `json:"keluhan_utama"`
		RiwayatPenyakitSekarang string `json:"penyakit_sekarang"`
		KeluhanYangMenyertai    string `json:"keluhan_menyertai"`
		RiwayatMenstruasi       string `json:"riwayat_menstruasi"`
		SiklusHaid              string `json:"siklus_haid"`
		RiwayatPernikahan       string `json:"riwayat_pernikahan"`
		RiwayatPenyakitDahulu   string `json:"penyakit_dahulu"`
		RiwayatPenyakitKeluarga string `json:"rwt_penyakit_keluarga"`
		Person                  string `json:"person"`
		Pelayanan               string `json:"pelayanan"`
	}

	ReqGetAsesmenAwalKebidanan struct {
		Noreg   string `json:"no_reg" validate:"required"`
		NoRm    string `json:"no_rm" validate:"omitempty"`
		Person  string `json:"person" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
	}

	ReqAsesmenKebidanan struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	RegDianosaBandingIGDDokter struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqReportPengkajianKebidanan struct {
		Noreg  string `json:"no_reg" validate:"required"`
		NoRM   string `json:"no_rm" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqOnSaveAsesmenKebidanan struct {
		Noreg                            string `json:"no_reg" validate:"required"`
		Person                           string `json:"person" validate:"required"`
		Pelayanan                        string `json:"pelayanan" validate:"required"`
		Dpjp                             string `json:"dpjp" validate:"required"`
		DeviceID                         string `json:"device_id" validate:"required"`
		AseskepKehamilanGravida          string `json:"gravida" validate:"omitempty"`
		AseskepKehamilanAbortus          string `json:"abortus" validate:"omitempty"`
		AseskepKehamilanPara             string `json:"para" validate:"omitempty"`
		AseskepKehamilanHaid             string `json:"haid" validate:"omitempty"`
		AseskepKehamilanHaidTerakhir     string `json:"haid_terakhir" validate:"omitempty"`
		AseskepUsiaKehamilan             string `json:"usia_kehamilan" validate:"omitempty"`
		AseskepPartusHpl                 string `json:"partus_hpl" validate:"omitempty"`
		AseskepKehamilanLeopold1         string `json:"leopold1" validate:"omitempty"`
		AseskepKehamilanLeopold2         string `json:"leopold2" validate:"omitempty"`
		AseskepKehamilanLeopold3         string `json:"leopold3" validate:"omitempty"`
		AseskepKehamilanLeopold4         string `json:"leopold4" validate:"omitempty"`
		AseskepKehamilanHodge            string `json:"hodge" validate:"omitempty"`
		AseskepKehamilanInspeksi         string `json:"inspeksi" validate:"omitempty"`
		AseskepKehamilanInspekuloV       string `json:"inspekulo_v" validate:"omitempty"`
		AseskepKehamilanInspekuloP       string `json:"inspekulo_p" validate:"omitempty"`
		AseskepKehamilanPemeriksaanDalam string `json:"pemeriksaan_dalam" validate:"omitempty"`
		AseskepKehamilanHamilMuda        string `json:"hamil_muda" validate:"omitempty"`
		AseskepKehamilanHamilTua         string `json:"hamil_tua" validate:"omitempty"`
		AseskepKehamilanTbj              string `json:"tbj" validate:"omitempty"`
		AseskepKehamilanTfu              string `json:"tfu" validate:"omitempty"`
		AseskepKehamilanNyeriTekan       string `json:"nyeri_tekan" validate:"omitempty"`
		AseskepKehamilanPalpasi          string `json:"palpasi" validate:"omitempty"`
	}

	ResAsesmenKebidanan struct {
		AseskepKehamilanGravida          string `json:"gravida"`
		AseskepKehamilanAbortus          string `json:"abortus"`
		AseskepKehamilanPara             string `json:"para"`
		AseskepKehamilanHaid             string `json:"haid"`
		AseskepKehamilanHaidTerakhir     string `json:"haid_terakhir"`
		AseskepKehamilan                 string `json:"kehamilan"`
		AseskepUsiaKehamilan             string `json:"usia_kehamilan"`
		AseskepPartusHpl                 string `json:"partus_hpl"`
		AseskepKehamilanLeopold1         string `json:"leopold1"`
		AseskepKehamilanLeopold2         string `json:"leopold2"`
		AseskepKehamilanLeopold3         string `json:"leopold3"`
		AseskepKehamilanLeopold4         string `json:"leopold4"`
		AseskepKehamilanHodge            string `json:"hodge"`
		AseskepKehamilanInspeksi         string `json:"inspeksi"`
		AseskepKehamilanInspekuloV       string `json:"inspekulo_v"`
		AseskepKehamilanInspekuloP       string `json:"inspekulo_p"`
		AseskepKehamilanPemeriksaanDalam string `json:"pemeriksaan_dalam"`
	}

	RegSaveDianosaBandingIGDDokter struct {
		Noreg           string `json:"no_reg" validate:"required"`
		Person          string `json:"person" validate:"required"`
		DiagnosaBanding string `json:"diagnosa_banding" validate:"required"`
		DevicesID       string `json:"device_id" validate:"required"`
		Pelayanan       string `json:"pelayanan" validate:"required"`
	}

	RegPengkajianPersistem struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	RegPengkajianPersistemAnak struct {
		Noreg string `json:"no_reg" validate:"required"`
		NoRm  string `json:"no_rm" validate:"required"`
	}

	ReqPengkajianPersistemICU struct {
		Noreg   string `json:"no_reg" validate:"required"`
		Person  string `json:"person" validate:"required"`
		NoRM    string `json:"no_rm" validate:"omitempty"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
	}

	ReqPengkajianICU struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
		NoRM   string `json:"no_rm" validate:"omitempty"`
	}

	ReqReportTriaseIGDDokter struct {
		Noreg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
		NoRM    string `json:"no_rm" validate:"omitempty"`
	}

	ReqReportPengkajianAwalDokter struct {
		Noreg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
		NoRM    string `json:"no_rm" validate:"omitempty"`
	}

	ReqReportPengkajianAwalPerawat struct {
		Noreg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
		NoRM    string `json:"no_rm" validate:"omitempty"`
	}

	ReqGetTriaseIGDDokter struct {
		Noreg     string `json:"no_reg" validate:"required"`
		Person    string `json:"person" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
	}

	ResponseFungsional struct {
		F1    string `json:"f1"`
		F2    string `json:"f2"`
		F3    string `json:"f3"`
		F4    string `json:"f4"`
		F5    string `json:"f5"`
		F6    string `json:"f6"`
		F7    string `json:"f7"`
		F8    string `json:"f8"`
		F9    string `json:"f9"`
		F10   string `json:"f10"`
		Nilai int    `json:"nilai"`
	}

	ResNutrisi struct {
		N1    string `json:"n1"`
		N2    string `json:"n2"`
		Nilai int    `json:"nilai"`
	}

	OnReqSaveTriaseIGDDokter struct {
		Noreg                   string `json:"noreg"`
		Person                  string `json:"person"`
		Pelayanan               string `json:"pelayanan"`
		DevicesID               string `json:"devices_id"`
		Kategori                string `json:"kategori"`
		AseskepKehamilanDjj     string `json:"ddj"`
		AseskepAlasanMasuk      string `json:"alasan_masuk"`
		AseskepCaraMasuk        string `json:"cara_masuk"`
		AseskepPenyebabCedera   string `json:"penyebab_cedera"`
		AseskepKehamilan        string `json:"kehamilan"`
		AseskepKehamilanGravida string `json:"gravida"`
		AseskepKehamilanPara    string `json:"para"`
		AseskepKehamilanAbortus string `json:"abortus"`
		AseskepKehamilanHpht    string `json:"hpht"`
		AseskepKehamilanTtp     string `json:"ttp"`
		GangguanPerilaku        string `json:"gangguan_perilaku"`
		SkalaNyeri              int    `json:"skala_nyeri"`
		SkalaNyeriP             string `json:"nyeri_p"`
		SkalaNyeriQ             string `json:"nyeri_q"`
		SkalaNyeriR             string `json:"nyeri_r"`
		SkalaNyeriS             string `json:"nyeri_s"`
		SkalaNyeriT             string `json:"nyeri_t"`
		SkalaTriase             string `json:"skala_triase"`
		FlaccWajah              int    `json:"flag_wajah"`
		FlaccKaki               int    `json:"flag_kaki"`
		FlaccAktifitas          int    `json:"flag_aktifitas"`
		FlaccMenangis           int    `json:"flag_menangis"`
		FlaccBersuara           int    `json:"flag_bersuara"`
		FlaccTotal              int    `json:"flag_total"`
	}

	RegPengkajianKebidanan struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqSavePengkajianPersistem struct {
		Noreg                          string `json:"no_reg" validate:"required"`
		Person                         string `json:"person" validate:"required"`
		Pelayanan                      string `json:"pelayanan" validate:"required"`
		DeviceID                       string `json:"divice_id" validate:"required"`
		AseskepSistemEliminasiBak      string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab      string `json:"eliminasi_bab"`
		AseskepSistemIstirahat         string `json:"istirahat"`
		AseskepSistemAktivitas         string `json:"aktivitas"`
		AseskepSistemMandi             string `json:"mandi"`
		AseskepSistemBerpakaian        string `json:"berpakaian"`
		AseskepSistemMakan             string `json:"makan"`
		AseskepSistemEliminasi         string `json:"eliminasi"`
		AseskepSistemMobilisasi        string `json:"mobilisasi"`
		AseskepSistemKardiovaskuler    string `json:"kardiovaskuler"`
		AseskepSistemRespiratori       string `json:"respiratori"`
		AseskepSistemSecebral          string `json:"secebral"`
		AseskepSistemPerfusiPerifer    string `json:"perfusi_perifer"`
		AseskepSistemPencernaan        string `json:"pencernaan"`
		AseskepSistemIntegumen         string `json:"integumen"`
		AseskepSistemKenyamanan        string `json:"kenyamanan"`
		AseskepSistemProteksi          string `json:"proteksi"`
		AseskepSistemPapsSmer          string `json:"paps_smer"`
		AseskepSistemHamil             string `json:"hamil"`
		AseskepSistemPendarahan        string `json:"pendarahan"`
		AseskepSistemHambatanBahasa    string `json:"hambatan_bahasa"`
		AseskepSistemCaraBelajar       string `json:"cara_belajar"`
		AseskepSistemBahasaSehari      string `json:"bahasa_sehari"`
		AseskepSistemSpikologis        string `json:"spikologis"`
		AseskepSistemHambatanSosial    string `json:"hambatan_sosial"`
		AseskepSistemHambatanEkonomi   string `json:"hambatan_ekonomi"`
		AseskepSistemHambatanSpiritual string `json:"spiritual"`
		AseskepSistemResponseEmosi     string `json:"response_emosi"`
		AseskepSistemNilaiKepercayaan  string `json:"nilai_kepercayaan"`
		AseskepSistemPresepsiSakit     string `json:"presepsi_sakit"`
		AseskepSistemKhususKepercayaan string `json:"khusus_kepercayaan"`
		Thermoregulasi                 string `json:"thermoregulasi"`
	}

	ReqSavePengkajianKeperawatan struct {
		Noreg                          string `json:"no_reg" validate:"required"`
		Person                         string `json:"person" validate:"required"`
		Pelayanan                      string `json:"pelayanan" validate:"required"`
		DeviceID                       string `json:"divice_id" validate:"required"`
		AseskepSistemEliminasiBak      string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab      string `json:"eliminasi_bab"`
		AseskepSistemIstirahat         string `json:"istirahat"`
		AseskepSistemAktivitas         string `json:"aktivitas"`
		AseskepSistemMandi             string `json:"mandi"`
		AseskepSistemBerpakaian        string `json:"berpakaian"`
		AseskepSistemMakan             string `json:"makan"`
		AseskepSistemEliminasi         string `json:"eliminasi"`
		AseskepSistemMobilisasi        string `json:"mobilisasi"`
		AseskepSistemKardiovaskuler    string `json:"kardiovaskuler"`
		AseskepSistemRespiratori       string `json:"respiratori"`
		AseskepSistemSecebral          string `json:"secebral"`
		AseskepSistemPerfusiPerifer    string `json:"perfusi_perifer"`
		AseskepSistemPencernaan        string `json:"pencernaan"`
		AseskepSistemIntegumen         string `json:"integumen"`
		AseskepSistemKenyamanan        string `json:"kenyamanan"`
		AseskepSistemProteksi          string `json:"proteksi"`
		AseskepSistemPapsSmer          string `json:"paps_smer"`
		AseskepSistemHamil             string `json:"hamil"`
		AseskepSistemPendarahan        string `json:"pendarahan"`
		AseskepSistemHambatanBahasa    string `json:"hambatan_bahasa"`
		AseskepSistemCaraBelajar       string `json:"cara_belajar"`
		AseskepSistemBahasaSehari      string `json:"bahasa_sehari"`
		AseskepSistemSpikologis        string `json:"spikologis"`
		AseskepSistemHambatanSosial    string `json:"hambatan_sosial"`
		AseskepSistemHambatanEkonomi   string `json:"hambatan_ekonomi"`
		AseskepSistemHambatanSpiritual string `json:"spiritual"`
		AseskepSistemResponseEmosi     string `json:"response_emosi"`
		AseskepSistemNilaiKepercayaan  string `json:"nilai_kepercayaan"`
		AseskepSistemPresepsiSakit     string `json:"presepsi_sakit"`
		AseskepSistemKhususKepercayaan string `json:"khusus_kepercayaan"`
		Penerjemah                     string `json:"penerjemah"`
		Thermoregulasi                 string `json:"thermoregulasi"`
		ThermoregulasiDingin           string `json:"thermoregulasi_dingin"`
		Usus                           string `json:"pencernaan_usus"`
		SakitKepala                    string `json:"sakit_kepala"`
		KekuatanOtot                   string `json:"kekuatan_otot"`
		AnggotaGerak                   string `json:"anggota_gerak"`
		Bicara                         string `json:"bicara"`
		PerubahanStatusMental          string `json:"perubahan_status_mental"`
		RiwayatHipertensi              string `json:"riwayat_hipertensi"`
		Akral                          string `json:"akral"`
		Batuk                          string `json:"batuk"`
		SuaraNapas                     string `json:"suara_napas"`
		Merokok                        string `json:"merokok"`
		Nutrisi                        string `json:"nutrisi"`
		MasalahProstat                 string `json:"masalah_prostat"`
		Genitalia                      string `json:"genitalia"`
	}

	ReqPemeriksaanFisikKebidanan struct {
		Noreg     string `json:"no_reg" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
	}

	ReqPengobatanDirumah struct {
		KdRiwayat string `json:"kd_riwayat" validate:"required"`
		Noreg     string `json:"no_reg" validate:"required"`
	}

	ReqRiwayatKehamilan struct {
		DeviceID        string `json:"device_id" binding:"required"`
		Tahun           string `json:"tahun" binding:"required"`
		Tempat          string `json:"tempat" binding:"required"`
		NoReg           string `json:"no_reg" binding:"required"`
		Umur            string `json:"umur" binding:"required"`
		JenisPersalinan string `json:"jenis_persalinan" binding:"required"`
		Penolong        string `json:"penolong" binding:"required"`
		Penyulit        string `json:"penyulit" binding:"required"`
		Nifas           string `json:"nifas" binding:"required"`
		JKelamin        string `json:"j_kelamin" binding:"required"`
		BeratBadan      string `json:"berat_badan" binding:"required"`
		KeadaanSekarang string `json:"keadaan_sekarang" binding:"required"`
	}

	ReqRiwayatPengobatanDirumah struct {
		USIA                   string `json:"usia" validate:"omitempty"`
		DeviceID               string `json:"device_id" binding:"required"`
		UserID                 string `json:"user_id" binding:"required"`
		NoReg                  string `json:"no_reg" binding:"required"`
		NamaObat               string `json:"nama_obat" binding:"required"`
		Dosis                  string `json:"dosis" binding:"required"`
		CaraPemberian          string `json:"cara_pemberian" binding:"required"`
		Frekuensi              string `json:"frekuensi" binding:"required"`
		WaktuTerakhirPemberian string `json:"waktu_pemberian" binding:"required"`
	}

	ReqUpdateRiwayatPengobatanDirumah struct {
		KdRiwayat              string `json:"kd_riwayat" binding:"required"`
		NamaObat               string `json:"nama_obat" binding:"required"`
		Dosis                  string `json:"dosis" binding:"required"`
		CaraPemberian          string `json:"cara_pemberian" binding:"required"`
		Frekuensi              string `json:"frekuensi" binding:"required"`
		WaktuTerakhirPemberian string `json:"waktu_pemberian" binding:"required"`
	}

	ResVitalSignKebidanan struct {
		TekananDarah string `json:"tekanan_darah" validate:"omitempty"`
		Nadi         string `json:"nadi" validate:"omitempty"`
		Pernapasan   string `json:"pernapasan" validate:"omitempty"`
		Suhu         string `json:"suhu" validate:"omitempty"`
		TinggiBadan  string `json:"tinggi_badan" validate:"omitempty"`
		BeratBadan   string `json:"berat_badan" validate:"omitempty"`
		DDJ          string `json:"ddj" validate:"omitempty"`
		GcsE         string `json:"gcs_e" validate:"omitempty"`
		GcsV         string `json:"gcs_v" validate:"omitempty"`
		GcsM         string `json:"gcs_m" validate:"omitempty"`
	}

	ResDPengkajianFungsional struct {
		Noreg    string `json:"noreg"`
		F1       string `json:"f1"`
		NilaiF1  int    `json:"nilai_f1"`
		F2       string `json:"f2"`
		NilaiF2  int    `json:"nilai_f2"`
		F3       string `json:"f3"`
		NilaiF3  int    `json:"nilai_f3"`
		F4       string `json:"f4"`
		NilaiF4  int    `json:"nilai_f4"`
		F5       string `json:"f5"`
		NilaiF5  int    `json:"nilai_f5"`
		F6       string `json:"f6"`
		NilaiF6  int    `json:"nilai_f6"`
		F7       string `json:"f7"`
		NilaiF7  int    `json:"nilai_f7"`
		F8       string `json:"f8"`
		NilaiF8  int    `json:"nilai_f8"`
		F9       string `json:"f9"`
		NilaiF9  int    `json:"nilai_f9"`
		F10      string `json:"f10"`
		NilaiF10 int    `json:"nilai_f10"`
		Nilai    int    `json:"nilai"`
	}

	ResDiagnosaKebidananResponse struct {
		KodeDiagnosa string `json:"kode_diagnosa"`
		NamaDiagnosa string `json:"nama_diagnosa"`
		IsSelected   bool   `json:"is_selected"`
	}

	ResponseSaveDiagnosaKebidanan struct {
		Diagnosa []ResDiagnosaKebidananResponse `json:"diagnosa"`
		DeviceID string                         `json:"device_id"`
		Noreg    string                         `json:"no_reg"`
	}

	ResDeleteRiwayatKehamilan struct {
		ID    int    `json:"id" binding:"required"`
		Noreg string `json:"no_reg" validate:"required"`
	}

	ResPengkajianNutrisi struct {
		Noreg   string `json:"noreg"`
		N1      string `json:"n1"`
		N2      string `json:"n2"`
		Nilai   int    `json:"nilai"`
		NilaiN1 int    `json:"nilai_n1"`
		NilaiN2 int    `json:"nilai_n2"`
	}

	ResPengkajianNutrisiAnak struct {
		Noreg   string `json:"noreg"`
		N1      string `json:"n1"`
		N2      string `json:"n2"`
		N3      string `json:"n3"`
		N4      string `json:"n4"`
		Nilai   int    `json:"nilai"`
		NilaiN1 int    `json:"nilai_n1"`
		NilaiN2 int    `json:"nilai_n2"`
		NilaiN3 int    `json:"nilai_n3"`
		NilaiN4 int    `json:"nilai_n4"`
	}

	ReqPengkajianNutrisi struct {
		Noreg    string `json:"noreg"`
		DeviceID string `json:"device_id" binding:"required"`
		N1       string `json:"n1" binding:"required"`
		N2       string `json:"n2" binding:"required"`
		Nilai    int    `json:"nilai" binding:"required"`
	}

	ReqPengkajianNutrisiAnak struct {
		Noreg    string `json:"no_reg"`
		DeviceID string `json:"device_id" binding:"required"`
		N1       string `json:"n1" binding:"required"`
		N2       string `json:"n2" binding:"required"`
		N3       string `json:"n3" binding:"required"`
		N4       string `json:"n4" binding:"required"`
		Nilai    int    `json:"nilai" binding:"required"`
	}

	OnSavePengkajianNIpsAnak struct {
		Noreg                 string `json:"noreg" binding:"required"`
		Pelayanan             string `json:"pelayanan" binding:"required"`
		Person                string `json:"person" binding:"required"`
		DeviceID              string `json:"device_id" binding:"required"`
		EkspresiWajah         string `json:"ekspresi_wajah"`
		KdDokter              string `json:"kd_dokter"`
		Tangisan              string `json:"tangisan"`
		PolaNapas             string `json:"pola_napas"`
		Tangan                string `json:"tangan"`
		Kaki                  string `json:"kaki"`
		Kesadaran             string `json:"kesadaran"`
		Total                 int    `json:"total"`
		AseskepNyeri          int    `json:"nyeri"`
		AseskepLokasiNyeri    string `json:"lokasi_nyeri"`
		AseskepFrekuensiNyeri string `json:"frekuensi_nyeri"`
		AseskepNyeriMenjalar  string `json:"nyeri_menjalar"`
		AseskepKualitasNyeri  string `json:"kualitas_nyeri"`
	}

	ResponseAsesmenUlangKeperawatanIntensive struct {
		Asesmen                 string                             `json:"asesmen"`
		CaraMasuk               string                             `json:"cara_masuk"`
		KeluhanUtama            string                             `json:"keluhan_utama"`
		Dari                    string                             `json:"dari"`
		RiwayatPenyakitSekarang string                             `json:"penyakit_sekarang"`
		RiwayatPenyakitDahulu   string                             `json:"penyakit_dahulu"`
		ReaksiYangMuncul        string                             `json:"yang_muncul"`
		TransfusiDarah          string                             `json:"transfusi_darah"`
		RiwayatMerokok          string                             `json:"riwayat_merokok"`
		RiwayatMinumanKeras     string                             `json:"minuman_keras"`
		AlcoholMempegaruhiHidup string                             `json:"alcohol_mempengaruhi"`
		RiwayatPenyakitKeluarga []rme.DAlergi                      `json:"penyakit_keluarga"`
		PenyakitDahulu          []rme.RiwayatPenyakitDahuluPerawat `json:"riwayat_dahulu"`
	}

	OnRegAsesmenUlangKeperawatanIntensive struct {
		Noreg                   string `json:"no_reg"`
		Tanggal                 string `json:"tanggal"`
		KdDPJP                  string `json:"kd_dpjp"`
		NoRM                    string `json:"no_rm"`
		Pelayanan               string `json:"pelayanan"`
		DeviceID                string `json:"device_id"`
		Person                  string `json:"person"`
		Asesmen                 string `json:"asesmen"`
		CaraMasuk               string `json:"cara_masuk"`
		KeluhanUtama            string `json:"keluhan_utama"`
		Dari                    string `json:"dari"`
		RiwayatPenyakitSekarang string `json:"penyakit_sekarang"`
		RiwayatPenyakitDahulu   string `json:"penyakit_dahulu"`
		RiwayatAlergi           string `json:"riwayat_alergi"`
		ReaksiYangMuncul        string `json:"yang_muncul"`
		TransfusiDarah          string `json:"transfusi_darah"`
		RiwayatMerokok          string `json:"riwayat_merokok"`
		RiwayatMinumanKeras     string `json:"minuman_keras"`
		AlcoholMempegaruhiHidup string `json:"alcohol_mempengaruhi"`
	}

	ReqCariPengkajianNutrisiAnak struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	ReqAsesmenNyeriIcu struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	AsesmenNyeiICU struct {
		AseskepNyeri          int    `json:"nyeri"`
		AseskepLokasiNyeri    string `json:"lokasi_nyeri"`
		AseskepFrekuensiNyeri string `json:"frekuensi_nyeri"`
		AseskepNyeriMenjalar  string `json:"nyeri_menjalar"`
		AseskepKualitasNyeri  string `json:"kualitas_nyeri"`
	}

	ReqInsertAsesmenNyeriICU struct {
		KdDokter              string `json:"kd_dokter" binding:"required"`
		DeviceID              string `json:"device_id" binding:"required"`
		Pelayanan             string `json:"pelayanan" binding:"required"`
		Person                string `json:"person" binding:"required"`
		NoReg                 string `json:"no_reg" binding:"required"`
		Kategori              string `json:"kategori" binding:"required"`
		AseskepNyeri          int    `json:"nyeri"`
		AseskepLokasiNyeri    string `json:"lokasi_nyeri"`
		AseskepFrekuensiNyeri string `json:"frekuensi_nyeri"`
		AseskepNyeriMenjalar  string `json:"nyeri_menjalar"`
		AseskepKualitasNyeri  string `json:"kualitas_nyeri"`
	}

	OnReportAsesmenUlangPerawatanIntensive struct {
		NoReg   string `json:"no_reg" binding:"required"`
		NoRM    string `json:"no_rm" binding:"required"`
		Tanggal string `json:"tanggal" binding:"required"`
	}

	ResDiagnosaKebidanan struct {
		NoDiagnosa   int    `json:"no_diagnosa"`
		KodeDiagnosa string `json:"kode_diagnosa"`
		NamaDiagnosa string `json:"nama_diagnosa"`
	}

	ResponseReportDiagnosaKebidanan struct {
		Diagnosa     []ResDiagnosaKebidanan               `json:"diagnosa"`
		Implementasi []kebidanan.DImplementasiKeperawatan `json:"implementasi"`
	}

	ResPenyakitKeluarga struct {
		Tanggal  string `json:"tanggal"`
		Penyakit string `json:"penyakit"`
	}

	RegDPengkajianFungsional struct {
		DeviceID string `json:"device_id" binding:"required"`
		NoReg    string `json:"noreg" binding:"required"`
		F1       string `json:"f1" binding:"required"`
		NilaiF1  int    `json:"nilai_f1" validate:"omitempty"`
		F2       string `json:"f2" binding:"required"`
		NilaiF2  int    `json:"nilai_f2" validate:"omitempty"`
		F3       string `json:"f3" binding:"required"`
		NilaiF3  int    `json:"nilai_f3" validate:"omitempty"`
		F4       string `json:"f4" binding:"required"`
		NilaiF4  int    `json:"nilai_f4" validate:"omitempty"`
		F5       string `json:"f5" binding:"required"`
		NilaiF5  int    `json:"nilai_f5" validate:"omitempty"`
		F6       string `json:"f6" binding:"required"`
		NilaiF6  int    `json:"nilai_f6" validate:"omitempty"`
		F7       string `json:"f7" binding:"required"`
		NilaiF7  int    `json:"nilai_f7" validate:"omitempty"`
		F8       string `json:"f8" binding:"required"`
		NilaiF8  int    `json:"nilai_f8" validate:"omitempty"`
		F9       string `json:"f9" binding:"required"`
		NilaiF9  int    `json:"nilai_f9" validate:"omitempty"`
		F10      string `json:"f10" binding:"required"`
		NilaiF10 int    `json:"nilai_f10" validate:"omitempty"`
		Nilai    int    `json:"nilai" binding:"required"`
	}

	RequestVitalSignKebidanan struct {
		DeviceID     string `json:"device_id" binding:"required"`
		Pelayanan    string `json:"pelayanan" binding:"required"`
		Person       string `json:"person" binding:"required"`
		NoReg        string `json:"no_reg" binding:"required"`
		TekananDarah string `json:"tekanan_darah" validate:"omitempty"`
		Nadi         string `json:"nadi" validate:"omitempty"`
		Pernapasan   string `json:"pernapasan" validate:"omitempty"`
		Suhu         string `json:"suhu" validate:"omitempty"`
		TinggiBadan  string `json:"tinggi_badan" validate:"omitempty"`
		BeratBadan   string `json:"berat_badan" validate:"omitempty"`
		DDJ          string `json:"ddj" validate:"omitempty"`
		GcsE         string `json:"gcs_e" validate:"omitempty"`
		GcsV         string `json:"gcs_v" validate:"omitempty"`
		GcsM         string `json:"gcs_m" validate:"omitempty"`
		Tfu          string `json:"tfu" validate:"omitempty"`
		Pupil        string `json:"pupil" validate:"omitempty"`
		Kesadaran    string `json:"kesadaran" validate:"omitempty"`
		Akral        string `json:"akral" validate:"omitempty"`
	}

	PengkajianKebidananResponse struct {
		TglMasuk                         string `json:"tgl_masuk"`
		TglKeluar                        string `json:"tgl_keluar"`
		Pelayanan                        string `json:"pelayanan"`
		KdBagian                         string `json:"kd_bagian"`
		Noreg                            string `json:"noreg"`
		AseskepKel                       string `json:"keluhan_utama"`
		AseskepRwytPnykt                 string `json:"rwt_penyakit"`
		AseskepRwytPnyktDahulu           string `json:"penyakit_dahulu"`
		AseskepRwytMenstruasi            string `json:"menstruasi"`
		AseskepSiklusHaid                string `json:"siklus_haid"`
		AseskepRwytPernikahan            string `json:"rwt_pernikahan"`
		AseskepReaksiAlergi              string `json:"alergi"`
		AseskepKehamilanGravida          string `json:"gravida"`
		AseskepKehamilanPara             string `json:"para"`
		AseskepKehamilanAbortus          string `json:"abortus"`
		AseskepKehamilanHpht             string `json:"hpht"`
		AseskepKehamilanTtp              string `json:"tpp"`
		AseskepKehamilanLeopold1         string `json:"leopold1"`
		AseskepKehamilanLeopold2         string `json:"leopold2"`
		AseskepKehamilanLeopold3         string `json:"leopold3"`
		AseskepKehamilanLeopold4         string `json:"leopold4"`
		AseskepSistemPresepsiSakit       string `json:"presepsi_sakit"`
		AseskepSistemKhususKepercayaan   string `json:"kepercayaan"`
		AseskepSistemResponseEmosi       string `json:"response_emosi"`
		AseskepSistemHambatanSosial      string `json:"hambatan_sosial"`
		AseskepSistemSpikologis          string `json:"spikologis"`
		AseskepSistemThermoregulasi      string `json:"thermoregulasi"`
		AseskepSistemBahasaSehari        string `json:"bahasa_sehari"`
		AseskepSistemCaraBelajar         string `json:"cara_belajar"`
		AseskepSistemHambatanBahasa      string `json:"hambatan_bahasa"`
		AseskepSistemPendarahan          string `json:"sistem_pendarahan"`
		AseskepSistemHamil               string `json:"hamil"`
		AseskepSistemPapsSmer            string `json:"paps_smer"`
		AseskepSistemProteksi            string `json:"proteksi"`
		AseskepSistemKenyamanan          string `json:"kenyamanan"`
		AseskepSistemIntegumen           string `json:"integumen"`
		AseskepSistemPencernaan          string `json:"pencernaan"`
		AseskepSistemPerfusiPerifer      string `json:"perfusi_perifer"`
		AseskepSistemSecebral            string `json:"secebral"`
		AseskepSistemRespiratori         string `json:"respiratori"`
		AseskepSistemKardiovaskuler      string `json:"kardiovaskuler"`
		AseskepSistemMobilisasi          string `json:"mobilisasi"`
		AseskepSistemEliminasi           string `json:"eliminasi"`
		AseskepSistemMakan               string `json:"sistem_makan"`
		AseskepSistemBerpakaian          string `json:"sistem_berpakaian"`
		AseskepSistemMandi               string `json:"sistem_mandi"`
		AseskepKehamilanInspekuloP       string `json:"inspekulo_p"`
		AseskepKehamilanInspekuloV       string `json:"inspekulo_v"`
		AseskepKehamilanHaid             string `json:"haid"`
		AseskepKehamilanPemeriksaanDalam string `json:"pemeriksaan_dalam"`
		AseskepSistemNilaiKepercayaan    string `json:"nilai_kepercayaan"`
		AseskepPartusHpl                 string `json:"hpl"`
		AseskepKehamilanTbj              string `json:"tbj"`
		AseskepKehamilanTfu              string `json:"tfu"`
		AseskepKehamilanHamilMuda        string `json:"hamil_muda"`
		AseskepKehamilanHamilTua         string `json:"hamil_tua"`
		AseskepKehamilanUsiaKehamilan    string `json:"usia_kehamilan"`
		AseskepKehamilanHaidTerakhir     string `json:"haid_terakhir"`
		AseskepSistemEliminasiBak        string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab        string `json:"eliminasi_bab"`
		AseskepSistemIstirahat           string `json:"istirahat"`
		AseskepSistemAktivitas           string `json:"aktivitas"`
		AseskepSistemHambatanEkonomi     string `json:"ekonomi"`
		AseskepSistemHambatanSpiritual   string `json:"spiritual"`
		AseskepKelMenyertai              string `json:"keluhan_menyertai"`
		AseskepKehamilanHis              string `json:"kehamilan_his"`
		AseskepKehamilanLendir           string `json:"kehamilan_lendir"`
	}

	ResponserPengkajianKebidanan struct {
		Fisik                       TandaVitalBangsalKebidananResponse    `json:"vital_sign"`
		PengkajianKebidananResponse PengkajianKebidananResponse           `json:"pengkajian"`
		RiwayatKehamilan            []kebidanan.DRiwayatKehamilan         `json:"kehamilan"`
		PengobatanDirumah           []kebidanan.DRiwayatPengobatanDirumah `json:"pengobatan_dirumah"`
		FisikBidan                  kebidanan.DPemFisikKebidanan          `json:"fisik"`
		Nutrisi                     kebidanan.DPengkajianNutrisi          `json:"nutrisi"`
		Fungsional                  kebidanan.DPengkajianFungsional       `json:"fungsional"`
		Diagnosa                    []kebidanan.DDiagnosaKebidanan        `json:"diagnosa"`
		DemFisik                    kebidanan.DPemFisikKebidanan          `json:"fisik_kebidanan"`
		PenyakitKeluarga            []rme.DAlergi                         `json:"alergi"`
		User                        user.Karyawan                         `json:"user"`
		Karu                        user.Karyawan                         `json:"karu"`
	}

	ResponsePengkajianKebidananBatuRajaV2 struct {
		User user.Karyawan `json:"user"`
		Karu user.Karyawan `json:"karu"`
	}

	ReqInputAllKebidanan struct {
		Data string `json:"data" binding:"required"`
	}

	RequestSaveResikoJatuhKebidanan struct {
		InsertPc        string `json:"insert_pc" binding:"required"`
		Pelayanan       string `json:"pelayanan" binding:"required"`
		Noreg           string `json:"noreg" binding:"required"`
		Kategori        string `json:"kategori" binding:"required"`
		RJatuh          string `json:"r_jatuh" binding:"required"`
		Diagnosis       string `json:"diagnosis" binding:"required"`
		AlatBantu1      string `json:"alat_bantu1" binding:"required"`
		AlatBantu2      string `json:"alat_bantu2" binding:"required"`
		AlatBantu3      string `json:"alat_bantu3" binding:"required"`
		TerpasangInfuse string `json:"terpasang_infuse" binding:"required"`
		Total           int    `json:"total" binding:"required"`
	}

	ReqFisikKebidanan struct {
		DeviceID          string `json:"device_id"  binding:"required"`
		KetPerson         string `json:"person"  binding:"required"`
		Pelayanan         string `json:"pelayanan"  binding:"required"`
		Noreg             string `json:"noreg"  binding:"required"`
		Kepala            string `json:"kepala"  binding:"required"`
		Mata              string `json:"mata"  binding:"required"`
		Telinga           string `json:"telinga"  binding:"required"`
		Mulut             string `json:"mulut"  binding:"required"`
		Gigi              string `json:"gigi"  binding:"required"`
		Hidung            string `json:"hidung"  binding:"required"`
		Tenggorokan       string `json:"tenggorokan"  binding:"required"`
		Payudara          string `json:"payudara"  binding:"required"`
		LeherDanBahu      string `json:"leher_dan_bahu"  binding:"required"`
		MucosaMulut       string `json:"mucosa_mulut" binding:"required"`
		Abdomen           string `json:"abdomen" binding:"required"`
		NutrisiDanHidrasi string `json:"nutrisi_dan_hidrasi" binding:"required"`
	}

	ReqGetFisikKebidanan struct {
		Noreg     string `json:"noreg"  binding:"required"`
		Pelayanan string `json:"pelayanan"  binding:"required"`
		Person    string `json:"person"  binding:"required"`
	}

	// REPORT PERKEMBAGAN PASIEN
	RegPerkembanganPasien struct {
		Noreg string `json:"noreg"  binding:"required"`
	}

	RequestPengkajianPersistemICU struct {
		Noreg                          string `json:"noreg" binding:"required"`
		Pelayanan                      string `json:"pelayanan" binding:"required"`
		Person                         string `json:"person" binding:"required"`
		InsertPC                       string `json:"insert_pc" binding:"required"`
		KodeDokter                     string `json:"kode_dokter" binding:"required"`
		Airway                         string `json:"airway"`
		Breathing                      string `json:"breathing"`
		Circulation                    string `json:"circulation"`
		Nutrisi                        string `json:"nutrisi"`
		MasalahDenganNutrisi           string `json:"masalah_dengan_nutrisi"`
		Makan                          string `json:"makan"`
		PadaBayi                       string `json:"pada_bayi"`
		Minum                          string `json:"minum"`
		EliminasiBak                   string `json:"eliminasi_bak"`
		EliminasiBab                   string `json:"eliminasi_bab"`
		AktivitasAtauIstirahat         string `json:"aktivitas_istirahat"`
		SistemEliminasi                string `json:"sistem_eliminasi"`
		SistemBerpakaian               string `json:"sistem_berpakaian"`
		SistemMandi                    string `json:"sistem_mandi"`
		SistemMobilisasi               string `json:"sistem_mobilisasi"`
		Aktivitas                      string `json:"aktivitas"`
		Berjalan                       string `json:"berjalan"`
		PenggunaanAlatBantu            string `json:"penggunaan_alat_bantu"`
		PerfusiSerebral                string `json:"perfusi_serebral"`
		Pupil                          string `json:"pupil"`
		RefleksCahaya                  string `json:"refleks_cahaya"`
		PerfusiRenal                   string `json:"perfusi_renal"`
		PerfusiGastrointestinal        string `json:"pefusi_gastroinestinal"`
		Abdomen                        string `json:"abdomen"`
		Thermoregulasi                 string `json:"thermoregulasi"`
		Kenyamanan                     string `json:"kenyamanan"`
		Kualitas                       string `json:"kualitas"`
		Pola                           string `json:"pola"`
		NyeriMempengaruhi              string `json:"nyeri_mempengaruhi"`
		StatusMental                   string `json:"status_mental"`
		Kejang                         string `json:"kejang"`
		PasangPengamanTempatTidur      string `json:"pasang_pengaman_tempat_tidur"`
		BelMudaDijangkau               string `json:"bel_muda_dijangkau"`
		Penglihatan                    string `json:"penglihatan"`
		Pendengaran                    string `json:"pendengaran"`
		Hamil                          string `json:"hamil"`
		PemeriksaanCervixTerakhir      string `json:"pemeriksaan_cervix_terakhir"`
		PemeriksaanPayudaraSendiri     string `json:"pemeriksaan_payudara_sendiri"`
		MamografiTerakhirTanggal       string `json:"mamografi_terakhir_tanggal"`
		PenggunaanAlatKontrasepsi      string `json:"penggunaan_alat_kontrasepsi"`
		Bicara                         string `json:"bicara"`
		BahasaSehariHari               string `json:"bahasa_sehari_hari"`
		PerluPenerjemah                string `json:"perlu_penerjemah"`
		BahasaIsyarat                  string `json:"bahasa_isyarat"`
		HambatanBelajar                string `json:"hambatan_belajar"`
		CaraBelajarDisukai             string `json:"cara_belajar_disukai"`
		TingkatPendidikan              string `json:"tingkat_pendidikan"`
		PotensialKebutuhanPembelajaran string `json:"potensial_kebutuhan_pembelajaran"`
		ResponseEmosi                  string `json:"response_emosi"`
		Pekerjaan                      string `json:"sistem_sosial"`
		TingkatBersama                 string `json:"tingkat_bersama"`
		KondisiLingkunganDirumah       string `json:"kondisi_lingkungan_dirumah"`
		NilaiKepercayaan               string `json:"nilai_kepercayaan"`
		MenjalankanIbadah              string `json:"menjalankan_ibadah"`
		PresepsiTerhadapSakit          string `json:"presepsi_terhadap_sakit"`
		KunjunganPemimpin              string `json:"kunjungan_pemimpin"`
		NilaiAturanKhusus              string `json:"nilai_aturan_khusus"`
	}

	ResponsePengkajianNyeriNips struct {
		Noreg                 string `json:"noreg"`
		EkspresiWajah         string `json:"ekspresi_wajah"`
		Tangisan              string `json:"tangisan"`
		PolaNapas             string `json:"pola_napas"`
		Tangan                string `json:"tangan"`
		Kaki                  string `json:"kaki"`
		Kesadaran             string `json:"kesadaran"`
		Total                 int    `json:"total"`
		AseskepNyeri          int    `json:"nyeri"`
		AseskepLokasiNyeri    string `json:"lokasi_nyeri"`
		AseskepFrekuensiNyeri string `json:"frekuensi_nyeri"`
		AseskepNyeriMenjalar  string `json:"nyeri_menjalar"`
		AseskepKualitasNyeri  string `json:"kualitas_nyeri"`
	}

	ReportAsesmenIntensiveICU struct {
		RiwayatAlergi         []rme.DAlergi                         `json:"alergi"`
		AsesmenUlangIntensive ResponseAsesmenIntensiICU             `json:"asesmen"`
		PemeriksaanFisik      ResponsePemeriksaanFisikICU           `json:"pemeriksaan_fisik"`
		PengkajianPersistem   ResponsePengkajianPersistemICU        `json:"pengkajian_persistem"`
		PengobatanDirumah     []kebidanan.DRiwayatPengobatanDirumah `json:"pengobatan_dirumah"`
		AsuhanKeperawatan     []rme.DasKepDiagnosaModelV2           `json:"asuhan_keperawatan"`
	}

	ResponseAsesmenIntensiICU struct {
		KdDpjp                       string `json:"kd_dpjp"`
		Asesmen                      string `json:"asesmen"`
		AseskepRwtDari               string `json:"rwt_dari"`
		AseskepCaraMasuk             string `json:"cara_masuk"`
		AseskepAsalMasuk             string `json:"asal_masuk"`
		AseskepKel                   string `json:"keluhan"`
		AseskepRwytPnykt             string `json:"riwayat_penyakit"`
		AseskepReaksiYangMuncul      string `json:"reaksi_yang_muncul"`
		AseskepReaksiAlergi          string `json:"reaksi_alergi"`
		AseskepTransfusiDarah        string `json:"transfusi_darah"`
		AseskepRwtMerokok            string `json:"rwt_merokok"`
		AseskepRwtMinumanKeras       string `json:"rwt_minuman_keras"`
		AseskepRwtAlkoholMempegaruhi string `json:"rwt_alkohol_mempegaruhi"`
		Nama                         string `json:"nama"`
	}

	OnGetPengkajianAwal struct {
		NoReg   string `json:"no_reg"`
		Person  string `json:"person"`
		Tanggal string `json:"tanggal"`
	}

	ResponsePengkajianAwalAnak struct {
		Pengkajian              string `json:"pengkajian"`
		KeluhanUtama            string `json:"keluhan_utama"`
		RiwayatPenyakitKeluarga string `json:"penyakit_keluarga"`
		RiwayatAlergi           string `json:"riwayat_alergi"`
		ReaksiTimbul            string `json:"reaksi_timbul"`
	}

	//======//
	ResponsePemeriksaanFisikICU struct {
		Kesadaran   string `json:"kesadaran"`
		Kepala      string `json:"kepala"`
		Rambut      string `json:"rambut"`
		Wajah       string `json:"wajah"`
		Mata        string `json:"mata"`
		Telinga     string `json:"telinga"`
		Hidung      string `json:"hidung"`
		Mulut       string `json:"mulut"`
		Gigi        string `json:"gigi"`
		Lidah       string `json:"lidah"`
		Tenggorokan string `json:"tenggorokan"`
		Leher       string `json:"leher"`
		Dada        string `json:"dada"`
		Respirasi   string `json:"respirasi"`
		Jantung     string `json:"jantung"`
		Integumen   string `json:"integumen"`
		Ekstremitas string `json:"ekstremitas"`
		Genetalia   string `json:"genetalia"`
	}
)
