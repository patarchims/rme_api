package dto

import (
	"hms_api/modules/his"
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"

	rmeDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type (
	TandaVitalBangsalKebidananResponse struct {
		Td           string `json:"tekanan_darah"`
		Pernafasan   string `json:"pernapasan"`
		Suhu         string `json:"suhu"`
		Nadi         string `json:"nadi"`
		Ddj          string `json:"ddj"`
		BeratBadan   string `json:"berat_badan"`
		TinggiBandan string `json:"tinggi_badan"`
		GCSE         string `json:"gcs_e"`
		GCSV         string `json:"gcs_v"`
		GCSM         string `json:"gcs_m"`
		Tfu          string `json:"tfu"`
		Kesadaran    string `json:"kesadaran"`
		Pupil        string `json:"pupil"`
		Akral        string `json:"akral"`
	}

	AsesemenKebidanan struct {
		Noreg                   string `json:"noreg"`
		AseskepKel              string `json:"keluhan_utama"`
		AseskepRwytPenyakit     string `json:"rwyt_penyakit"`
		AseskepRwytMenstruasi   string `json:"rwyt_menstruasi"`
		AseskepKelMenyertai     string `json:"keluhan_menyertai"`
		AseskepSiklusHaid       string `json:"siklus_haid"`
		RiwayatPenyakitDahulu   string `json:"penyakit_dahulu"`
		AseskepRwytPernikahan   string `json:"rwyt_penikahan"`
		RiwayatPenyakitKeluarga string `json:"penyakit_keluarga"`
	}

	ResponsePengkajianAnak struct {
		PengkajianAnak soap.PengkajianAwalKeperawatan `json:"keluhan_utama"`
	}

	ResponseKeluhanUtamaKeperawatanAnak struct {
	}

	ResponseAsesemenKebidanan struct {
		AsesemenKebidanan AsesemenKebidanan                  `json:"asesmen"`
		Alergi            []rme.DAlergi                      `json:"riwayat_keluarga"`
		Riwayat           []rme.RiwayatPenyakitDahuluPerawat `json:"riwayat_terdahulu"`
	}

	ResponseDiagnosaBanding struct {
		DiagnosaBanding string `json:"diagnosa_banding"`
		NoReg           string `json:"noreg"`
	}

	ResponsePengkajianPersistemKebidanan struct {
		AseskepSistemEliminasiBak      string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab      string `json:"eliminasi_bab"`
		AseskepSistemIstirahat         string `json:"istirahat"`
		AseskepSistemAktivitas         string `json:"aktivitas"`
		AseskepSistemMandi             string `json:"mandi"`
		AseskepSistemBerpakaian        string `json:"berpakaian"`
		AseskepSistemMakan             string `json:"makan"`
		AseskepSistemEliminasi         string `json:"eliminasi"`
		AseskepSistemMobilisasi        string `json:"mobilisasi"`
		AseskepSistemKardiovaskuler    string `json:"kardiovaskuler"`
		AseskepSistemRespiratori       string `json:"respiratori"`
		AseskepSistemSecebral          string `json:"secebral"`
		AseskepSistemPerfusiPerifer    string `json:"perfusi_perifer"`
		AseskepSistemPencernaan        string `json:"pencernaan"`
		AseskepSistemIntegumen         string `json:"integumen"`
		AseskepSistemKenyamanan        string `json:"kenyamanan"`
		AseskepSistemProteksi          string `json:"proteksi"`
		AseskepSistemPapsSmer          string `json:"paps_smer"`
		AseskepSistemHamil             string `json:"hamil"`
		AseskepSistemPendarahan        string `json:"pendarahan"`
		AseskepSistemHambatanBahasa    string `json:"hambatan_bahasa"`
		AseskepSistemCaraBelajar       string `json:"cara_belajar"`
		AseskepSistemBahasaSehari      string `json:"bahasa_sehari"`
		AseskepSistemSpikologis        string `json:"spikologis"`
		AseskepSistemHambatanSosial    string `json:"hambatan_sosial"`
		AseskepSistemHambatanEkonomi   string `json:"hambatan_ekonomi"`
		AseskepSistemHambatanSpiritual string `json:"spiritual"`
		AseskepSistemResponseEmosi     string `json:"response_emosi"`
		AseskepSistemNilaiKepercayaan  string `json:"nilai_kepercayaan"`
		AseskepSistemPresepsiSakit     string `json:"presepsi_sakit"`
		AseskepSistemKhususKepercayaan string `json:"khusus_kepercayaan"`
		AseskepSistemThermoregulasi    string `json:"thermoregulasi"`
	}

	ResponsePengkajianPersistemKeperawatan struct {
		AseskepSistemEliminasiBak         string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab         string `json:"eliminasi_bab"`
		AseskepSistemIstirahat            string `json:"istirahat"`
		AseskepSistemAktivitas            string `json:"aktivitas"`
		AseskepSistemMandi                string `json:"mandi"`
		AseskepSistemBerpakaian           string `json:"berpakaian"`
		AseskepSistemMakan                string `json:"makan"`
		AseskepSistemEliminasi            string `json:"eliminasi"`
		AseskepSistemMobilisasi           string `json:"mobilisasi"`
		AseskepSistemKardiovaskuler       string `json:"kardiovaskuler"`
		AseskepSistemRespiratori          string `json:"respiratori"`
		AseskepSistemSecebral             string `json:"secebral"`
		AseskepSistemPerfusiPerifer       string `json:"perfusi_perifer"`
		AseskepSistemPencernaan           string `json:"pencernaan"`
		AseskepSistemIntegumen            string `json:"integumen"`
		AseskepSistemKenyamanan           string `json:"kenyamanan"`
		AseskepSistemProteksi             string `json:"proteksi"`
		AseskepSistemPapsSmer             string `json:"paps_smer"`
		AseskepGenitalia                  string `json:"genitalia"`
		AseskepSistemHamil                string `json:"hamil"`
		AseskepSistemPendarahan           string `json:"pendarahan"`
		AseskepSistemHambatanBahasa       string `json:"hambatan_bahasa"`
		AseskepSistemCaraBelajar          string `json:"cara_belajar"`
		AseskepSistemBahasaSehari         string `json:"bahasa_sehari"`
		AseskepSistemSpikologis           string `json:"spikologis"`
		AseskepSistemHambatanSosial       string `json:"hambatan_sosial"`
		AseskepSistemHambatanEkonomi      string `json:"hambatan_ekonomi"`
		AseskepSistemHambatanSpiritual    string `json:"spiritual"`
		AseskepSistemResponseEmosi        string `json:"response_emosi"`
		AseskepSistemNilaiKepercayaan     string `json:"nilai_kepercayaan"`
		AseskepSistemPresepsiSakit        string `json:"presepsi_sakit"`
		AseskepSistemKhususKepercayaan    string `json:"khusus_kepercayaan"`
		AseskepSistemThermoregulasi       string `json:"thermoregulasi"`
		AseskepSistemThermoregulasiDingin string `json:"thermoregulasi_dingin"`
		Penerjemah                        string `json:"penerjemah"`
		Usus                              string `json:"pencernaan_usus"`
		SakitKepala                       string `json:"sakit_kepala"`
		KekuatanOtot                      string `json:"kekuatan_otot"`
		AnggotaGerak                      string `json:"anggota_gerak"`
		Bicara                            string `json:"bicara"`
		PerubahanStatusMental             string `json:"perubahan_status_mental"`
		RiwayatHipertensi                 string `json:"riwayat_hipertensi"`
		Akral                             string `json:"akral"`
		Batuk                             string `json:"batuk"`
		SuaraNapas                        string `json:"suara_napas"`
		Merokok                           string `json:"merokok"`
		Nutrisi                           string `json:"nutrisi"`
	}

	ResponsePengkajianKebidanan struct {
		AseskepKehamilanGravida          string `json:"gravida"`
		AseskepKehamilanAbortus          string `json:"abortus"`
		AseskepKehamilanPara             string `json:"para"`
		AseskepKehamilanLeopold1         string `json:"leopold1"`
		AseskepKehamilanLeopold2         string `json:"leopold2"`
		AseskepKehamilanLeopold3         string `json:"leopold3"`
		AseskepKehamilanLeopold4         string `json:"leopold4"`
		AseskepKehamilanHaid             string `json:"haid"`
		AseskepKehamilanPemeriksaanDalam string `json:"pemeriksaan_dalam"`
		AseskepKehamilanInspekuloP       string `json:"inspekulo_p"`
		AseskepKehamilanInspekuloV       string `json:"ispekulo_v"`
		AseskepKehamilanPalpasi          string `json:"palpasi"`
		AseskepKehamilanInspeksi         string `json:"inspeksi"`
		AseskepKehamilanHodge            string `json:"hodge"`
		AseskepKehamilanNyeriTekan       string `json:"nyeri_tekan"`
		AseskepKehamilanTbj              string `json:"tbj"`
		AseskepKehamilanTfu              string `json:"tfu"`
		AseskepKehamilanHamilTua         string `json:"hamil_tua"`
		AseskepKehamilanHamilMuda        string `json:"hamil_muda"`
		AseskepKehamilanHpl              string `json:"hpl"`
		AseskepKehamilanUsiaKehamilan    string `json:"usia_kehamilan"`
		AseskepKehamilanHaidTerakhir     string `json:"haid_terakhir"`
	}

	ResponseKebidanann struct {
		AseskepKehamilanGravida          string `json:"gravida"`
		AseskepKehamilanAbortus          string `json:"abortus"`
		AseskepKehamilanPara             string `json:"para"`
		AseskepKehamilanHaid             string `json:"haid"`
		AseskepKehamilanHaidTerakhir     string `json:"haid_terakhir"`
		AseskepUsiaKehamilan             string `json:"usia_kehamilan"`
		AseskepPartusHpl                 string `json:"partus_hpl"`
		AseskepKehamilanLeopold1         string `json:"leopold1"`
		AseskepKehamilanLeopold2         string `json:"leopold2"`
		AseskepKehamilanLeopold3         string `json:"leopold3"`
		AseskepKehamilanLeopold4         string `json:"leopold4"`
		AseskepKehamilanHodge            string `json:"hodge"`
		AseskepKehamilanInspeksi         string `json:"inspeksi"`
		AseskepKehamilanInspekuloV       string `json:"inspekulo_v"`
		AseskepKehamilanInspekuloP       string `json:"inspekulo_p"`
		AseskepKehamilanPemeriksaanDalam string `json:"pemeriksaan_dalam"`
		AseskepKehamilanHamilMuda        string `json:"hamil_muda"`
		AseskepKehamilanHamilTua         string `json:"hamil_tua"`
		AseskepKehamilanTbj              string `json:"tbj"`
		AseskepKehamilanTfu              string `json:"tfu"`
		AseskepKehamilanNyeriTekan       string `json:"nyeri_tekan"`
		AseskepKehamilanPalpasi          string `json:"palpasi"`
	}

	ResposeTriaseIGDDokter struct {
		Jam                     string `json:"jam"`
		TanggalMasuk            string `json:"tanggal_masuk"`
		UserID                  string `json:"user_id"`
		AseskepKehamilanDjj     string `json:"ddj"`
		AseskepAlasanMasuk      string `json:"alasan_masuk"`
		AseskepCaraMasuk        string `json:"cara_masuk"`
		AseskepPenyebabCedera   string `json:"penyebab_cedera"`
		AseskepKehamilan        string `json:"kehamilan"`
		AseskepKehamilanGravida string `json:"gravida"`
		AseskepKehamilanPara    string `json:"para"`
		AseskepKehamilanAbortus string `json:"abortus"`
		AseskepKehamilanHpht    string `json:"hpht"`
		AseskepKehamilanTtp     string `json:"ttp"`
		GangguanPerilaku        string `json:"gangguan_perilaku"`
		SkalaNyeri              int    `json:"skala_nyeri"`
		SkalaNyeriP             string `json:"nyeri_p"`
		SkalaNyeriQ             string `json:"nyeri_q"`
		SkalaNyeriR             string `json:"nyeri_r"`
		SkalaNyeriS             string `json:"nyeri_s"`
		SkalaNyeriT             string `json:"nyeri_t"`
		SkalaTriase             string `json:"skala_triase"`
		FlaccWajah              int    `json:"flag_wajah"`
		FlaccKaki               int    `json:"flag_kaki"`
		FlaccAktifitas          int    `json:"flag_aktifitas"`
		FlaccMenangis           int    `json:"flag_menangis"`
		FlaccBersuara           int    `json:"flag_bersuara"`
		FlaccTotal              int    `json:"flag_total"`
	}

	ReportTriaseIGDDokterResponse struct {
		Triase          ResposeTriaseIGDDokter        `json:"triase"`
		Karyawan        user.Karyawan                 `json:"karyawan"`
		PemFisik        rme.PemeriksaanFisikModel     `json:"fisik"`
		TandaVital      soapDTO.TandaVitalIGDResponse `json:"vital"`
		KeluhanUtamaIGD rmeDTO.ResposeKeluhanUtamaIGD `json:"keluhan_utama"`
		Pasien          user.ProfilePasien            `json:"pasien"`
	}

	ReportPengkajianAwalDokterResponse struct {
		PemFisik    rme.PemeriksaanFisikModel        `json:"fisik"`
		Vital       soap.DVitalSignIGDDokter         `json:"vital"`
		Asesmen     rme.AsesemenMedisIGD             `json:"asesmen"`
		RwtPenyakit []rme.KeluhanUtama               `json:"riwayat_penyakit_dahulu"`
		Alergi      []rme.DAlergi                    `json:"riwayat_keluarga"`
		Labor       []his.ResHasilLaborTableLama     `json:"labor"`
		Radiologi   []his.RegHasilRadiologiTabelLama `json:"radiologi"`
		Diagnosa    []soap.DiagnosaResponse          `json:"diagnosa"`
	}

	ReportPengkajianKeperawatanResponse struct {
		PemFisik              rmeDTO.ResponsePemeriksaanFisik       `json:"fisik"`
		Vital                 soap.DVitalSignIGDDokter              `json:"vital"`
		Asesmen               rme.AsesmenPerawat                    `json:"asesmen"`
		Alergi                []ResPenyakitKeluarga                 `json:"riwayat_keluarga"`
		Pengobatan            []kebidanan.DRiwayatPengobatanDirumah `json:"pengobatan"`
		Asuhan                []rme.DasKepDiagnosaModelV2           `json:"asuhan"`
		Fungsional            ResponseFungsional                    `json:"fungsional"`
		RiwayatPenyakitDahulu []rme.RiwayatPenyakitDahuluPerawat    `json:"riwayat_penyakit_dahulu"`
		Nutrisi               ResNutrisi                            `json:"nutrisi"`
	}

	ResponsePengkajianPersistemICU struct {
		Airway                         string `json:"airway"`
		Breathing                      string `json:"breathing"`
		Circulation                    string `json:"circulation"`
		Nutrisi                        string `json:"nutrisi"`
		Makan                          string `json:"makan"`
		PadaBayi                       string `json:"pada_bayi"`
		Minum                          string `json:"minum"`
		EliminasiBak                   string `json:"eliminasi_bak"`
		EliminasiBab                   string `json:"eliminasi_bab"`
		AktivitasAtauIstirahat         string `json:"aktivitas_istirahat"`
		Aktivitas                      string `json:"aktivitas"`
		Berjalan                       string `json:"berjalan"`
		PenggunaanAlatBantu            string `json:"penggunaan_alat_bantu"`
		PerfusiSerebral                string `json:"perfusi_serebral"`
		Pupil                          string `json:"pupil"`
		RefleksCahaya                  string `json:"refleks_cahaya"`
		PerfusiRenal                   string `json:"perfusi_renal"`
		PerfusiGastrointestinal        string `json:"pefusi_gastroinestinal"`
		Abdomen                        string `json:"abdomen"`
		Thermoregulasi                 string `json:"thermoregulasi"`
		Kenyamanan                     string `json:"kenyamanan"`
		Kualitas                       string `json:"kualitas"`
		Pola                           string `json:"pola"`
		NyeriMempengaruhi              string `json:"nyeri_mempengaruhi"`
		StatusMental                   string `json:"status_mental"`
		Kejang                         string `json:"kejang"`
		PasangPengamanTempatTidur      string `json:"pasang_pengaman_tempat_tidur"`
		BelMudaDijangkau               string `json:"bel_muda_dijangkau"`
		Penglihatan                    string `json:"penglihatan"`
		Pendengaran                    string `json:"pendengaran"`
		Hamil                          string `json:"hamil"`
		PemeriksaanCervixTerakhir      string `json:"pemeriksaan_cervix_terakhir"`
		PemeriksaanPayudaraSendiri     string `json:"pemeriksaan_payudara_sendiri"`
		MamografiTerakhirTanggal       string `json:"mamografi_terakhir_tanggal"`
		PenggunaanAlatKontrasepsi      string `json:"penggunaan_alat_kontrasepsi"`
		Bicara                         string `json:"bicara"`
		BahasaSehariHari               string `json:"bahasa_sehari_hari"`
		PerluPenerjemah                string `json:"perlu_penerjemah"`
		BahasaIsyarat                  string `json:"bahasa_isyarat"`
		HambatanBelajar                string `json:"hambatan_belajar"`
		CaraBelajarDisukai             string `json:"cara_belajar_disukai"`
		TingkatPendidikan              string `json:"tingkat_pendidikan"`
		PotensialKebutuhanPembelajaran string `json:"potensial_kebutuhan_pembelajaran"`
		ResponseEmosi                  string `json:"response_emosi"`
		Pekerjaan                      string `json:"sistem_sosial"`
		TingkatBersama                 string `json:"tingkat_bersama"`
		KondisiLingkunganDirumah       string `json:"kondisi_lingkungan_dirumah"`
		NilaiKepercayaan               string `json:"nilai_kepercayaan"`
		MenjalankanIbadah              string `json:"menjalankan_ibadah"`
		PresepsiTerhadapSakit          string `json:"presepsi_terhadap_sakit"`
		KunjunganPemimpin              string `json:"kunjungan_pemimpin"`
		NilaiAturanKhusus              string `json:"nilai_aturan_khusus"`
		AseskepSistemMandi             string `json:"mandi"`
		AseskepSistemBerpakaian        string `json:"berpakaian"`
		AseskepSistemMakan             string `json:"sistem_makan"`
		AseskepSistemEliminasi         string `json:"sistem_eliminasi"`
		AseskepSistemMobilisasi        string `json:"sistem_mobilisasi"`
		AseskepMslhDenganNutrisi       string `json:"masalah_dengan_nutrisi"`
	}
)
