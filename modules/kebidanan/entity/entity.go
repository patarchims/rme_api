package entity

import (
	"hms_api/modules/his"
	"hms_api/modules/kebidanan"
	"hms_api/modules/kebidanan/dto"
	"hms_api/modules/rme"
	rmeDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	soapDto "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type KebidananMapper interface {
	ToResponsePengkajianAnak(perawat soap.PengkajianAwalKeperawatan) (res dto.ResponsePengkajianAnak)
	ToPemeriksaanFisikICUMapper(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisikICU)
	ToReponseAsesmenIntensiICUMapper(data kebidanan.AsesmenUlangPerawatanIntensive) (res dto.ResponseAsesmenIntensiICU)
	ToResponseICUMapper(asesmenUlang kebidanan.AsesmenUlangPerawatanIntensive, alergi []rme.DAlergi, fisik dto.ResponsePemeriksaanFisikICU, sistem dto.ResponsePengkajianPersistemICU, pengobatan []kebidanan.DRiwayatPengobatanDirumah, asuhanKeperawatan []rme.DasKepDiagnosaModelV2) (res dto.ReportAsesmenIntensiveICU)
	ToNyeriICUResponseMapper(data rme.PengkajianNyeri) (res dto.AsesmenNyeiICU)
	ToAsesmenUlangPerawatanIntesiveMapper(data kebidanan.AsesmenUlangPerawatanIntensive, penyakitKeluarga []rme.DAlergi, penyakitDahulu []rme.RiwayatPenyakitDahuluPerawat) (res dto.ResponseAsesmenUlangKeperawatanIntensive)
	ToPengkajianNyeriNips(data kebidanan.PengkajianNyeriNIPS, nyeri rme.PengkajianNyeri) (res dto.ResponsePengkajianNyeriNips)
	// MAPPER TO
	ToPengkajianNutrisiAnakMapper(data kebidanan.DPengkajianNutrisiAnak) (res dto.ResPengkajianNutrisiAnak)
	ToMappingResponsePengkajianPersistemICU(data kebidanan.AsesmenPengkajianPersistemICUBangsal) (value dto.ResponsePengkajianPersistemICU)

	ToVitalSignKebidananMapper(data rme.DVitalSign, gCSE string, gcsV string, gcsM string, ddj string) (res dto.ResVitalSignKebidanan)
	ToFungsionalKebidananMapper(data kebidanan.DPengkajianFungsional) (res dto.ResDPengkajianFungsional)
	ToPengkajianNutrisiMapper(data kebidanan.DPengkajianNutrisi) (res dto.ResPengkajianNutrisi)
	ToMappingDiagnosaKebidanan(data []kebidanan.KDiagnosaKebidanan) (value []dto.ResDiagnosaKebidananResponse)

	//================== REPORT KEBIDANAN
	ToMappingDiagnosaSaveResponseKebidanan(data []kebidanan.DDiagnosaKebidanan) (value []dto.ResDiagnosaKebidanan)
	ToMappingReportPelaksanaanKebidanan(data []kebidanan.DDiagnosaKebidanan, kper []kebidanan.DImplementasiKeperawatan) (res dto.ResponseReportDiagnosaKebidanan)
	ToMapperResponseTandaVitalKebidananBangsal(data1 kebidanan.DVitalSignKebidananBangsal, data2 kebidanan.DPemFisikKebidananBangsalRepository) (value dto.TandaVitalBangsalKebidananResponse)

	// ============================= KEBIDANAN
	ToMapperResponseAsesmenKebidananBangsal(data kebidanan.AsesemenKebidananBangsal, alerg []rme.DAlergi, dahulu []rme.RiwayatPenyakitDahuluPerawat) (res dto.ResponseAsesemenKebidanan)

	ToMapperReponsePengkajianKebidananBangsal(data kebidanan.AsesemenPersistemKebidananBangsal) (res dto.ResponsePengkajianPersistemKebidanan)
	ToMapperResponseKebidanan(data kebidanan.PengkajianKebidanan) (res dto.ResponsePengkajianKebidanan)
	ToMapperResponseDiagnosaBanding(data kebidanan.DiagnosaBandingDokter) (res dto.ResponseDiagnosaBanding)

	ToMapperReponsePengkajianKeperawatanBangsal(data kebidanan.AsesemenPersistemKeperawatanBangsal) (res dto.ResponsePengkajianPersistemKeperawatan)

	// MAPPER
	ToResponseAsesmenKebidanan(data kebidanan.AsesmenKebidananModel) (res dto.ResponseKebidanann)
	ToResponsePengkajianKebidanan(data kebidanan.ReportPengkajianKebidanan, vital dto.TandaVitalBangsalKebidananResponse, riwayat []kebidanan.DRiwayatKehamilan, dirumah []kebidanan.DRiwayatPengobatanDirumah, fisik kebidanan.DPemFisikKebidanan, nutrisi kebidanan.DPengkajianNutrisi, fungsional kebidanan.DPengkajianFungsional, diagnosa []kebidanan.DDiagnosaKebidanan, demFisik kebidanan.DPemFisikKebidanan, penyakitKeluarga []rme.DAlergi, karyawan user.Karyawan, karu user.Karyawan) (res dto.ResponserPengkajianKebidanan)

	// MAPPER DATA
	ToResponseTriaseIGDDokter(data kebidanan.TriaseIGDDokter, data1 kebidanan.AsesmenTriaseIGD) (res dto.ResposeTriaseIGDDokter)
	ToReseponseMapperTriaseIGDDokter(data dto.ResposeTriaseIGDDokter, karyawan user.Karyawan, pemFisik rme.PemeriksaanFisikModel, tandaVital soapDto.TandaVitalIGDResponse, keluhan rmeDTO.ResposeKeluhanUtamaIGD, pasien user.ProfilePasien) (res dto.ReportTriaseIGDDokterResponse)

	// ====  MAPPER REPORT PENGKAJIAN AWAL DOKTER
	ToResponseMapperPengkajianAwalDokter(pemFisik rme.PemeriksaanFisikModel, vital soap.DVitalSignIGDDokter, asesmen rme.AsesemenMedisIGD, rwtPenyakit []rme.KeluhanUtama, alergi []rme.DAlergi, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, diagnosa []soap.DiagnosaResponse) (res dto.ReportPengkajianAwalDokterResponse)
	ToResponseMapperPengkajianKeperawatan(pemFisik rmeDTO.ResponsePemeriksaanFisik, alergi []rme.DAlergi, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, diagnosa []soap.DiagnosaResponse, vital soap.DVitalSignIGDDokter, asesmen rme.AsesmenPerawat, pengobatan []kebidanan.DRiwayatPengobatanDirumah, asuhan []rme.DasKepDiagnosaModelV2, fungsional dto.ResponseFungsional, riwayatPenyakitDahulu []rme.RiwayatPenyakitDahuluPerawat, nutrisi kebidanan.DPengkajianNutrisi) (res dto.ReportPengkajianKeperawatanResponse)
	ToReponseFungsionalMapper(data kebidanan.DPengkajianFungsional) (res dto.ResponseFungsional)
	ToMappingNutrisiMapper(data kebidanan.DPengkajianNutrisi) (res dto.ResNutrisi)
	ToNyeriICUResponseMapper2(data kebidanan.PengkajianNyeriModel) (res dto.AsesmenNyeiICU)
}

type KebidananRepository interface {
	OnGetDImplementasiKeperawatanRepository(noDaskep string) (res []kebidanan.DImplementasiKeperawatan, err error)
	OnUpdatePengkajianPersistemAnakRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	OnUpdatePengkajianPersistemRepository(noReg string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	GetPengkajianPersistemAnakRepository(kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)

	GetPengkajianPersistemDewasaRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	OnUpdatePengkajianPersistemDewasaRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)

	OnUpdateRiwayatPengobatanDirumahRepository(req dto.ReqUpdateRiwayatPengobatanDirumah) (res kebidanan.DRiwayatPengobatanDirumah, err error)
	OnGetAsesmenNIPSRepository(noReg string, kdBagian string) (res kebidanan.PengkajianNyeriNIPS, err error)

	OnUpdateAsessmenNIPSRepository(noReg string, kdBagian string, data kebidanan.PengkajianNyeriNIPS) (res kebidanan.PengkajianNyeriNIPS, err error)
	OnSaveAssesmenNIPSRepository(data kebidanan.PengkajianNyeriNIPS) (res kebidanan.PengkajianNyeriNIPS, message string, err error)
	//===//
	OnGetReportPengkajianPersistemICURepository(kdBagian string, noReg string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)
	OnGetAsesmenUlangPerawatanIntensiveICURepository(noReg string) (res kebidanan.AsesmenUlangPerawatanIntensive, err error)
	OnUpdateAsesmenNyeriICURepository(userID string, kategori string, noReg string, pelayanan string, data kebidanan.PengkajianNyeriModel) (res kebidanan.PengkajianNyeriModel, message string, err error)
	OnSaveAsesmenNyeriIcuRepository(userID string, data kebidanan.PengkajianNyeriModel) (res kebidanan.PengkajianNyeriModel, message string, err error)
	OnUpdateAsesmenIntensiveRepository(noReg string, data kebidanan.AsesmenUlangPerawatanIntensive) (res kebidanan.AsesmenUlangPerawatanIntensive, err error)
	OnInsertAsesmenIntensiveRespository(data kebidanan.AsesmenUlangPerawatanIntensive) (res kebidanan.AsesmenUlangPerawatanIntensive, err error)
	OnGetAsesmenUlangPerawatIntensive(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesmenUlangPerawatanIntensive, err error)
	// GET PENGKAJIAN NYERI NIPS
	OnGetPengkajianNutrisiNipsRepository(noReg string) (res kebidanan.PengkajianNyeriNIPS, err error)
	OnGetPengkajianNyeriNipsRepository(noReg string, kdBagian string) (res rme.PengkajianNyeri, err error)

	GetPengkajianPersistemAnakKeperawatanRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKeperawatanBangsal, err error)

	OnSavePengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)
	OnUpdatePengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)

	// =----- ON GET PENGKAJIAN NUTRISI ANAK
	OnGetPengkajianNutrisiAnakRepository(noReg string) (res kebidanan.DPengkajianNutrisiAnak, err error)
	OnUpdatePengkajianNutrisiAnakRepository(noReg string, req dto.ReqPengkajianNutrisiAnak) (res dto.ResPengkajianNutrisiAnak, err error)
	//=========
	// ===== PENGKAJIAN PERSISTEM ICU REPOSITORY
	OnGetPengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)
	OnGetAllKebidananRepository() (res []kebidanan.KDiagnosaKebidanan, err error)
	GetPemeriksaanFisikPengkjianKebidanan(noReg string, pelayanan string) (res kebidanan.PemeriksaanFisikKebidanan, err error)
	OnSaveDiagnosaRepository(userID string, bagian string, device string, noReg string, kodeDiagnosa string, namaDiagnosa string) (err error)
	InsertRiwayatPengobatanDirumahRepository(req dto.ReqRiwayatPengobatanDirumah, kdBagian string) (message string, res kebidanan.DRiwayatPengobatanDirumah, err error)
	OnGetRiwayatPengobatanDirumahRepository(noReg string) (res []kebidanan.DRiwayatPengobatanDirumah, err error)
	OnSaveAllKebidananReporitory(uuID string, namaBagian string) (err error)
	DeleteRiwayatPengobatanDirumahRepository(uid string) (message string, err error)
	UpdateDemfisikGCSMRepository(noReg string, req dto.RequestVitalSignKebidanan) (res rme.DPemfisikBidanGCS, err error)
	InsertVitalSignKebidananRepository(userID string, bagian string, req dto.RequestVitalSignKebidanan) (res rme.DVitalSign, err error)
	InsertGCSDataDemfisikRepository(userID string, bagian string, req dto.RequestVitalSignKebidanan) (res rme.DPemfisikBidanGCS, err error)
	OnGetVitalSignKebidananRepository(noReg string, pelayanan string) (res rme.DVitalSign, err error)
	UpdateVitalSignKebidananRepository(noReg string, req dto.RequestVitalSignKebidanan) (res rme.DVitalSign, err error)
	InsertRiwayatKehamilanRepository(userID string, req dto.ReqRiwayatKehamilan) (res kebidanan.DRiwayatKehamilan, message string, err error)
	OnGetRiwayatKehamilanRepository(noReg string) (res []kebidanan.DRiwayatKehamilan, err error)
	OnGetResikoJatuhKebidananRepository(noReg string, person string) (res kebidanan.DRisikoJatuhKebidanan, err error)
	OnSaveResikoJatuhKebidananRepository(userID string, bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (res kebidanan.DRisikoJatuhKebidanan, err error)
	OnUpdateResikoJatuhKebidananRepository(bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (res kebidanan.DRisikoJatuhKebidanan, err error)
	OnGetPemeriksaanFisikKebidananRepository(noReg string, pelayanan string, person string) (res kebidanan.DPemFisikKebidanan, err error)
	OnGetPemeriksaanFisikKebidananReportRepository(noReg string, person string) (res kebidanan.DPemFisikKebidanan, err error)
	OnSavePemeriksaanFisikKebidananRepository(userID string, bagian string, req dto.ReqFisikKebidanan) (res kebidanan.DPemFisikKebidanan, err error)
	OnUpdatePemeriksaanFisikKebidananRepository(userID string, bagian string, req dto.ReqFisikKebidanan) (res kebidanan.DPemFisikKebidanan, err error)
	OnGetPengkajianFungsionalRepository(noReg string) (res kebidanan.DPengkajianFungsional, err error)
	OnSavePengkajianFungsionalKebidahanRepository(userID string, bagian string, req dto.RegDPengkajianFungsional) (res dto.ResDPengkajianFungsional, err error)
	OnUpdatePengkajianFungsionalKebidananRepository(noReg string, req dto.RegDPengkajianFungsional) (res dto.ResDPengkajianFungsional, err error)
	OnGetPengkajianNutrisiRepository(noReg string) (res kebidanan.DPengkajianNutrisi, err error)
	OnSavePengkajianNutrisiRepository(userID string, bagian string, req dto.ReqPengkajianNutrisi) (res dto.ResPengkajianNutrisi, err error)
	OnSavePengkajianNutrisiAnakRepository(userID string, bagian string, req dto.ReqPengkajianNutrisiAnak) (res dto.ResPengkajianNutrisiAnak, err error)
	OnUpdatePengkajianNutrisi(noReg string, req dto.ReqPengkajianNutrisi) (res dto.ResPengkajianNutrisi, err error)
	OnDeleteRiwayatPengobatanDirumahRepository(req dto.ReqPengobatanDirumah) (err error)
	OnGetDiagnosaKebidananRepository(noReg string) (res []kebidanan.DDiagnosaKebidanan, err error)
	OnDeleteDataDiagnosaKebidananRepository(noDiagnosa int) (message string, err error)
	OnDeleteRiwayatKehamilanRepository(id int) (err error)

	GetVitalSignKebidananBangsalRepository(userID string, kdBagian string, person string, noreg string) (res kebidanan.DVitalSignKebidananBangsal, err error)
	GetPemFisikKebidananRepository(userID string, kdBagian string, person string, noreg string) (res kebidanan.DPemFisikKebidananBangsalRepository, err error)

	// KEBIDANAN BANGSAL
	GetVitalSignKebidananBangsalReportRepository(kdBagian string, person string, noreg string) (res kebidanan.DVitalSignKebidananBangsal, err error)
	GetPemFisikKebidananReportRepository(kdBagian string, person string, noreg string) (res kebidanan.DPemFisikKebidananBangsalRepository, err error)

	// ===================== KEBIDANAN
	GetAsesmenKebidananRepository(kdBagian string, person string, noReg string) (res kebidanan.AsesemenKebidananBangsal, err error)
	OnSaveAsesmenKebidananRepository(data kebidanan.AsesemenKebidananBangsal) (res kebidanan.AsesemenKebidananBangsal, err error)
	OnUpdateAsesmenKebidananRepository(noReg string, kdBagian string, req dto.ReqAsesmenAwalKebidanan) (res kebidanan.AsesemenKebidananBangsal, err error)

	// ============================= PENGKAJIAN PERSISTEM
	GetPengkajianPersistemKebidananRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	OnSavePengkajianPersistemKebidananRepository(data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)

	// =================== PENGKAJIAN MARI
	OnGetPengkajianPersistemMariaRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	OnUpdatePengkajianPersistemMariaRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	// ====================

	GetPengkajianPersistemKeperawatanRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKeperawatanBangsal, err error)

	// === PENGKAJIAN KEBIDANAN
	OnUpdatePengkajianPersistemKebidananRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)
	GetPengkajianKebidananRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.PengkajianKebidanan, err error)

	// =============== DIAGNOSA BANDING
	GetDiagnosaBandingIGDDokterRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.DiagnosaBandingDokter, err error)
	OnUpdateDiagnosaBandingRespository(noReg string, userID string, data kebidanan.DiagnosaBandingDokter) (res kebidanan.DiagnosaBandingDokter, err error)
	OnSaveDiagnosaBandingRespository(data kebidanan.DiagnosaBandingDokter) (res kebidanan.DiagnosaBandingDokter, err error)

	// ================= ASESMEN KEBIDANAN REPOSITORY
	OnGetAsemenKebidananRepository(noReg string, userID string, kdBagian string, person string) (res kebidanan.AsesmenKebidananModel, err error)

	OnSaveAsesmenKebidananRepositoryV2(data kebidanan.AsesmenKebidananModel) (res kebidanan.AsesmenKebidananModel, err error)
	OnUpdateAsesmenKebidananRepositoryV2(noReg string, data kebidanan.AsesmenKebidananModel) (res kebidanan.AsesmenKebidananModel, err error)

	// ===================================== ON GET PENGKAJIAN KEBIDANAN
	OnGetPengkajianKebidananRepository(noReg string) (res kebidanan.ReportPengkajianKebidanan, err error)

	// MAPPING DATA
	OnGetTriaseIGDDokterRepository(noReg string, userID string, kdBagian string, pelayanan string) (res kebidanan.TriaseIGDDokter, err error)
	OnGetAsesmenTriaseIGDRepository(noReg string, userID string, kdBagian string, pelayanan string) (res kebidanan.AsesmenTriaseIGD, err error)

	// =================== ONSAVE TRIASE
	OnSaveAsesmenTriaseIGDRepository(data kebidanan.AsesmenTriaseIGD) (res kebidanan.AsesmenTriaseIGD, err error)
	OnSaveTriaseIGDDokterRepository(data kebidanan.TriaseIGDDokter) (res kebidanan.TriaseIGDDokter, err error)

	// UPDATE DATA
	OnUpdateTriaseIGDDokterRepository(noReg string, userID string, kdBagian string, pelayanan string, data kebidanan.TriaseIGDDokter) (res kebidanan.TriaseIGDDokter, err error)
	OnUpdateAsesmenTriaseIGDRepository(noReg string, userID string, kdBagian string, pelayanan string, data kebidanan.AsesmenTriaseIGD) (res kebidanan.AsesmenTriaseIGD, err error)

	// ONGET DATA TRIASE IGD DOKTER
	OnGetReportTriaseIGDDokterRepository(noReg string) (res kebidanan.TriaseIGDDokter, err error)
	OnGetReportAsesmenTriaseIGDRepository(noReg string) (res kebidanan.AsesmenTriaseIGD, err error)
	OnGetPemeriksaanFisikTriaseIGDRepository(noReg string) (res rme.PemeriksaanFisikModel, err error)

	// REPORT ASESMEN MEDIS DOKTER
	OnGetPemeriksaanFisikMedisDokter(noReg string) (res rme.PemeriksaanFisikModel, err error)
	GetTandaVitalIGDDokterRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error)
	GetAsesmenAwalMedisRawatInapReportDokter(noreg string, kdBagian string, pelayanan string) (res rme.AsesemenMedisIGD, err error)

	// REPORT PENGKAJIAN PERAWAT
	OnGetPemeriksaanFisikPerawatRepository(noReg string, bagian string) (res rme.PemeriksaanFisikModel, err error)
	GetAsesmenAwalMedisRawatInapReportKeperawat(noreg string, kdBagian string, pelayanan string) (res rme.AsesemenMedisIGD, err error)
	GetTandaVitalPerawatRawatInapRepository(kdBagian string, noreg string) (res soap.DVitalSignIGDDokter, err error)

	// REPORT KEPERAWATAN
	GetAsesmenAwalRawatInapRepository(noreg string, kdBagian string) (res rme.AsesmenPerawat, err error)
	GetPengkajianPersistemKeperawatanReportRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)

	//=========================== REPORT PENGKAJIAN KEBIDANAN
	OnGetPengkajianKebidananV2Repository(noReg string, kdBagian string) (res kebidanan.ReportPengkajianKebidanan, err error)
}

type KebidananUsecase interface {
	// ============== REPORT KEBIDANAN
	OnGetReportPengkajianKebidananBatuRajaUseCaseV2(noReg string, noRM string) (res dto.ResponsePengkajianKebidananBatuRajaV2, err error)
	//==== END REPORT KEBIDANAN
	// ====================== PEMERIKSAAN FISIK KEBIDANAN
	OnSavePengkajianPersistemKebidananUseCase(req dto.ReqSavePengkajianPersistem, userID string, person string, kdBagian string) (messaage string, err error)
	OnSavePengkajianPersistemAnakUseCase(req dto.ReqSavePengkajianKeperawatan, person string, userID string, kdBagian string) (message string, err error)

	// OnUpdatePengkajianPersistemDewasaRepository(noReg string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error)

	// ON SAVE DATA
	OnSaveAsesmenNyeriAnakUseCase(userID string, bagian string, noReg string, req dto.OnSavePengkajianNIpsAnak) (message string, err error)
	OnGetPengkajianAwalAssesmenAnakUseCase(userID string, kdBagian string, req dto.OnGetPengkajianAwal) (message string, res dto.ResponsePengkajianAnak, err error)
	// OnGetPengkajianAwalAssesmenAnakUseCase(userID string, kdBagian string, req dto.OnGetPengkajianAwal) (message string, res dto.ResponsePengkajianAnak, err error)
	// OnGetPengkajianAwalAsesmenAnakUseCase(userID string, kdBagian string, req dto.OnGetPengkajianAwal) (message string, res dto.ResponsePengkajianAnak, err error)
	OnReportAsesmenIntensiveUseCase(noReg string, tanggal string, noRM string, kdBagian string) (res dto.ReportAsesmenIntensiveICU, err error)
	OnSaveAssesmenNyeriIcuUseCase(userID string, bagian string, noReg string, req dto.ReqInsertAsesmenNyeriICU) (message string, res dto.AsesmenNyeiICU, err error)
	OnSaveAsesmenIntensiveUseCase(userID string, kdBagian string, req dto.OnRegAsesmenUlangKeperawatanIntensive) (res dto.ResponseAsesmenUlangKeperawatanIntensive, message string, err error)
	OnSavePengkajianNutrisiAnakUsecase(userID string, bagian string, noReg string, req dto.ReqPengkajianNutrisiAnak) (message string, res dto.ResPengkajianNutrisiAnak, err error)
	OnSavePengkajianPersistemIcuICUUsecase(userID string, modulID string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res dto.ResponsePengkajianPersistemICU, message string, err error)
	// ===================================== KEBIDANAN USECASE ====================================== //
	OnGetReportPengkajianKebidananUseCase(kdBagian string, person string, noReg string, noRM string) (res dto.ResponserPengkajianKebidanan, err error)
	OnSaveAsesmenPengkajianKeperawatanDewasaPersistemUseCase(req dto.ReqSavePengkajianKeperawatan, userID string, person string, kdBagian string) (message string, err error)
	OnSaveDiagnosaKebidananUsecase(req dto.ResponseSaveDiagnosaKebidanan, bagian string, userID string) (res []dto.ResDiagnosaKebidanan, message string, err error)
	// SaveVitalSignKebidanan(userID string, bagian string, noReg string, pelayanan string, req dto.RequestVitalSignKebidanan) (res rme.DVitalSign, message string, err error)
	SaveVitalSignKebidananUsecase(userID string, bagian string, noReg string, pelayanan string, req dto.RequestVitalSignKebidanan) (res dto.ResVitalSignKebidanan, message string, err error)
	SaveResikoJatuhKebidananUsecase(noReg string, userID string, bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (message string, res kebidanan.DRisikoJatuhKebidanan, err error)
	OnSavePemeriksaanFisikKebidananUsecase(userID string, bagian string, noReg string, pelayanan string, req dto.ReqFisikKebidanan) (message string, res kebidanan.DPemFisikKebidanan, err error)
	OnSavePengkajianFungsionalKebidahanUsecase(userID string, bagian string, noReg string, req dto.RegDPengkajianFungsional) (message string, res dto.ResDPengkajianFungsional, err error)
	OnSavePengkajianNutrisiUsecase(userID string, bagian string, noReg string, req dto.ReqPengkajianNutrisi) (message string, res dto.ResPengkajianNutrisi, err error)

	// ================== VITAL SIGN
	OnGetTandaVitalKebidananBangsalUseCase(userID string, kdBagian string, person string, noReg string) (res dto.TandaVitalBangsalKebidananResponse, err error)
	OnSaveAsesmenKebidananBangsalUseCase(req dto.ReqAsesmenAwalKebidanan, userID string, person string, kdBagian string) (message string, err error)
	OnSaveAsesmenPengkajianPersistemUseCase(req dto.ReqSavePengkajianPersistem, userID string, person string, kdBagian string) (message string, err error)
	OnSaveDiagnosaBandingUseCase(userID string, kdBagian string, reg dto.RegSaveDianosaBandingIGDDokter) (message string, err error)

	// KEBIDAHAN KELUHAN UTAMA
	OnGetKeluhanUtamaKebidananUsecase(userID string, kdBagian string, req dto.ReqGetAsesmenAwalKebidanan) (res dto.ResponseAsesemenKebidanan, message string, err error)
	OnGetAsesmenKebidananUsecase(userID string, noReg string, kdBagian string, person string) (res dto.ResponseKebidanann, message string, err error)
	OnSaveAsesmenKebidananUseCase(userID string, kdBagian string, req dto.ReqOnSaveAsesmenKebidanan) (res dto.ResponseKebidanann, message string, err error)

	// USECASE TRIASE
	OnGetTriaseIGDDokterUseCase(noReg string, userID string, kdBagian string, pelayanan string) (res dto.ResposeTriaseIGDDokter, err error)
	OnSaveTriaseIGDDokterUseCase(noReg string, userID string, kdBagian string, pelayanan string, req dto.OnReqSaveTriaseIGDDokter) (message string, res dto.ResposeTriaseIGDDokter, err error)
	OnGetReportTriaseIGDDokterUseCase(noReg string, noRm string, tanggal string) (res dto.ReportTriaseIGDDokterResponse)
	OnGetReportTriasePonekUseCase(noReg string, noRm string, tanggal string) (res dto.ReportTriaseIGDDokterResponse)

	OnGetReportPengkajianAwalMedisDokterUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res dto.ReportPengkajianAwalDokterResponse, err error)
	OnGetReportPengkajianAwalPerawatUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res dto.ReportPengkajianKeperawatanResponse, err error)
}
