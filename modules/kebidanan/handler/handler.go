package handler

import (
	"hms_api/modules/kebidanan/dto"
	"hms_api/modules/kebidanan/entity"
	rmeRepo "hms_api/modules/rme/entity"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type KebidananHandler struct {
	KebidananMapper     entity.KebidananMapper
	KebidananUseCase    entity.KebidananUsecase
	KebidananRepository entity.KebidananRepository
	RMERepository       rmeRepo.RMERepository
	Logging             *logrus.Logger
}

func (hh *KebidananHandler) InsertDRiwayatKehamilanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatKehamilan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA RIWAYAT KEHAMILAN FIBER HANDLER

	return nil
}

func (hh *KebidananHandler) InsertRiwayatPengobatanDirumahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatPengobatanDirumah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA RIWAYAT KEHAMILAN FIBER HANDLER
	modulID := c.Locals("modulID").(string)

	message, data, errs1 := hh.KebidananRepository.InsertRiwayatPengobatanDirumahRepository(*payload, modulID)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *KebidananHandler) OnUpdateRiwayatPengobatanDirumahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqUpdateRiwayatPengobatanDirumah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs1 := hh.KebidananRepository.OnUpdateRiwayatPengobatanDirumahRepository(*payload)

	if errs1 != nil {
		response := helper.APIResponseFailure("data gagal diupdate", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("data berhasil diupdate", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (hh *KebidananHandler) OnGetRiwayatPengobatanDirumahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err3 := hh.KebidananRepository.OnGetRiwayatPengobatanDirumahRepository(*&payload.Noreg)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnDeleteRiwayatPengobatanDirumahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPengobatanDirumah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	_ = hh.KebidananRepository.OnDeleteRiwayatPengobatanDirumahRepository(*payload)

	data, err3 := hh.KebidananRepository.OnGetRiwayatPengobatanDirumahRepository(*&payload.Noreg)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	hh.Logging.Info("DELETE REIWAYAT PENGOBATAN")
	hh.Logging.Info(data)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetRiwayatKehamilanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err3 := hh.KebidananRepository.OnGetRiwayatKehamilanRepository(*&payload.Noreg)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetResikoJatuhKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	person := c.Locals("person").(string)

	data, err3 := hh.KebidananRepository.OnGetResikoJatuhKebidananRepository(*&payload.Noreg, person)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPengkajianFungsionalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err3 := hh.KebidananRepository.OnGetPengkajianFungsionalRepository(payload.Noreg)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapepr := hh.KebidananMapper.ToFungsionalKebidananMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, mapepr)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSavePengkajianFungsionalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegDPengkajianFungsional)
	errs := c.BodyParser(&payload)

	hh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	messaage, fungsional, err12 := hh.KebidananUseCase.OnSavePengkajianFungsionalKebidahanUsecase(userID, modulID, payload.NoReg, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messaage, http.StatusOK, fungsional)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPengkajianNutrisiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err3 := hh.KebidananRepository.OnGetPengkajianNutrisiRepository(payload.Noreg)

	mapepr := hh.KebidananMapper.ToPengkajianNutrisiMapper(data)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, mapepr)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPengkajianNutrisiAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err3 := hh.KebidananRepository.OnGetPengkajianNutrisiAnakRepository(payload.Noreg)

	mapepr := hh.KebidananMapper.ToPengkajianNutrisiAnakMapper(data)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, mapepr)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSavePengkajianNutrisiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPengkajianNutrisi)
	errs := c.BodyParser(&payload)

	hh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	messaage, fungsional, err12 := hh.KebidananUseCase.OnSavePengkajianNutrisiUsecase(userID, modulID, payload.Noreg, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messaage, http.StatusOK, fungsional)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ON SAVE PENGKAJIAN NUTRISI FIBER HANDLER
func (hh *KebidananHandler) OnSavePengkajianNutrisiAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPengkajianNutrisiAnak)
	errs := c.BodyParser(&payload)

	hh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	hh.Logging.Info("NUTRISI ANAK")
	hh.Logging.Info(payload)

	messaage, fungsional, err12 := hh.KebidananUseCase.OnSavePengkajianNutrisiAnakUsecase(userID, modulID, payload.Noreg, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messaage, http.StatusOK, fungsional)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSavePengkajianNyeriNipsAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianNIpsAnak)
	errs := c.BodyParser(&payload)

	hh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	mesages, err1 := hh.KebidananUseCase.OnSaveAsesmenNyeriAnakUseCase(userID, modulID, payload.Noreg, *payload)

	// ON SAVE ASESMEN NYERI

	if err1 != nil {
		response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesages, http.StatusOK, mesages)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPengkajianNyeriNipsAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqCariPengkajianNutrisiAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	data, er12 := hh.KebidananRepository.OnGetPengkajianNutrisiNipsRepository(payload.NoReg)

	// CARI PENGKAJIAN NYERI
	hh.Logging.Info("DATA NUTRISI NIPS")
	hh.Logging.Info(data)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	nyeri, _ := hh.KebidananRepository.OnGetPengkajianNyeriNipsRepository(payload.NoReg, modulID)
	hh.Logging.Info("DATA NYERI")
	hh.Logging.Info(nyeri)

	mapper := hh.KebidananMapper.ToPengkajianNyeriNips(data, nyeri)

	hh.Logging.Info("GET DATA")
	hh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPengkajianAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetPengkajianAwal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	messages, res, err12 := hh.KebidananUseCase.OnGetPengkajianAwalAssesmenAnakUseCase(userID, modulID, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messages, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetAsesmenNyeriICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenNyeriIcu)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	nyeri, er123 := hh.KebidananRepository.OnGetPengkajianNyeriNipsRepository(payload.NoReg, modulID)

	if er123 != nil {
		response := helper.APIResponseFailure(er123.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// GET DATA DISINI
	hh.Logging.Info("GET DATA DISNI")
	hh.Logging.Info(nyeri)

	// MAPPING ASESMEN ICU
	mapper := hh.KebidananMapper.ToNyeriICUResponseMapper(nyeri)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnReportAsesmenIntensiveFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReportAsesmenUlangPerawatanIntensive)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET ASESMEN // ULANG KEPERAWATAN
	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)
	// OnReportAsesmenIntensiveUseCase

	report, er11 := hh.KebidananUseCase.OnReportAsesmenIntensiveUseCase(payload.NoReg, payload.Tanggal, payload.NoRM, modulID)

	hh.Logging.Info("REPORT ASESMEN INTENSIVE USECASE")
	hh.Logging.Info(report)

	if er11 != nil {
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, report)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSaveAsesmenNyeriICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInsertAsesmenNyeriICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, ress, err12 := hh.KebidananUseCase.OnSaveAssesmenNyeriIcuUseCase(userID, modulID, payload.NoReg, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, ress)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnGetPemeriksaanFisikKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetFisikKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err12 := hh.KebidananRepository.OnGetPemeriksaanFisikKebidananRepository(payload.Noreg, payload.Pelayanan, payload.Person)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnCreateReportPerkembanganPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetFisikKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err12 := hh.KebidananRepository.OnGetPemeriksaanFisikKebidananRepository(payload.Noreg, payload.Pelayanan, payload.Person)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSavePemeriksaanFisikKebidahanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqFisikKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, data, err12 := hh.KebidananUseCase.OnSavePemeriksaanFisikKebidananUsecase(userID, modulID, payload.Noreg, payload.Pelayanan, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSaveResikoJatuhFiberhandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSaveResikoJatuhKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	person := c.Locals("person").(string)

	message, data, err3 := hh.KebidananUseCase.SaveResikoJatuhKebidananUsecase(payload.Noreg, userID, modulID, person, *payload)

	if err3 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnSaveRiwayatKehamilanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatKehamilan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	hh.Logging.Info(payload)
	save, message, err3 := hh.KebidananRepository.InsertRiwayatKehamilanRepository(userID, *payload)

	if err3 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (hh *KebidananHandler) OnDeleteRiwayatKehamilanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ResDeleteRiwayatKehamilan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		hh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	hh.Logging.Info(payload)
	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)
	// res, mesage, err2 := hh.KebidananUseCase.OnSaveDiagnosaKebidananUsecase(*payload, modulID, userID)
	err12 := hh.KebidananRepository.OnDeleteRiwayatKehamilanRepository(payload.ID)

	data, _ := hh.KebidananRepository.OnGetRiwayatKehamilanRepository(*&payload.Noreg)

	if err12 != nil {
		response := helper.APIResponse(err12.Error(), http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, data)
	hh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) GetVitalSignBangsalBidan(c *fiber.Ctx) error {
	payload := new(dto.ReqGetVitalSignBidan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload)

	fisik, fisikErr := rh.KebidananUseCase.OnGetTandaVitalKebidananBangsalUseCase(userID, modulID, payload.Person, payload.Noreg)

	// CARI PEMERIKSAAN FISIK BANGSAL

	if fisikErr != nil {
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, fisikErr.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, fisik)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) GetDiagnosaKebidananHandler(c *fiber.Ctx) error {
	return nil
}

func (rh *KebidananHandler) InputAllDiagnosaKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInputAllKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// JIKA DATA DITEMUKAN LAKUKAN PARSING SLKI
	parts := strings.Split(payload.Data, ",")

	for _, part := range parts {

		if part != "" {
			rh.Logging.Info(parts)
			_ = rh.KebidananRepository.OnSaveAllKebidananReporitory(uuid.NewString(), part)
		}
		// INPUT DATA
	}

	return nil
}

func (rh *KebidananHandler) GetAllDiagnosaKebidananHandler(c *fiber.Ctx) error {
	listPoli, errs1 := rh.KebidananRepository.OnGetAllKebidananRepository()

	if errs1 != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(listPoli) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMappingDiagnosaKebidanan(listPoli)
	response := helper.APIResponse("OK", http.StatusOK, mapper)

	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *KebidananHandler) GetPengkajianFisikKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	save, err3 := rh.KebidananRepository.GetPemeriksaanFisikPengkjianKebidanan(payload.Noreg, payload.Pelayanan)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// mapping data
	// mapper := rh.KebidananMapper.ToMappingDiagnosaSaveResponseKebidanan(save)
	rh.Logging.Info(save)

	response := helper.APIResponse("OK", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) SaveDiagnosaKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ResponseSaveDiagnosaKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	res, mesage, err2 := rh.KebidananUseCase.OnSaveDiagnosaKebidananUsecase(*payload, modulID, userID)

	if err2 != nil {
		response := helper.APIResponse(mesage, http.StatusCreated, err2.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnPatchDataDiagnosaKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	save, err3 := rh.KebidananRepository.OnGetDiagnosaKebidananRepository(payload.Noreg)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// mapping data
	mapper := rh.KebidananMapper.ToMappingDiagnosaSaveResponseKebidanan(save)
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetReportDiagnoasaKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	save, err3 := rh.KebidananRepository.OnGetDiagnosaKebidananRepository(payload.Noreg)
	kper, _ := rh.KebidananRepository.OnGetDImplementasiKeperawatanRepository(payload.Noreg)

	// GET PELAKSANAAN

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// mapping data // GET DATA
	mapper := rh.KebidananMapper.ToMappingReportPelaksanaanKebidanan(save, kper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnDeleteDataDiagnosaKebidananhandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteDiagnosaKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)
	mesage, err3 := rh.KebidananRepository.OnDeleteDataDiagnosaKebidananRepository(payload.NoDiagnosa)

	if err3 != nil {
		response := helper.APIResponseFailure(err3.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(mesage, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSaveVitalSignBangsalBidan(c *fiber.Ctx) error {
	payload := new(dto.RequestVitalSignKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("DATA VITAL SIGN BANGSAL")
	rh.Logging.Info(payload)

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	person := c.Locals("person").(string)

	// CEK APAKAH DATA SUDAH ADA
	rh.Logging.Info("LAKUKAN UPDATE VITALSIGN")
	rme, message, errs1 := rh.KebidananUseCase.SaveVitalSignKebidananUsecase(userID, modulID, payload.NoReg, payload.Pelayanan, *payload)

	if errs1 != nil {
		response := helper.APIResponse(message, http.StatusCreated, errs1.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(person)

	response := helper.APIResponse(message, http.StatusOK, rme)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

// KEBIDANAN HANDLER
func (rh *KebidananHandler) OnSaveAsesmenKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenAwalKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("SAVE ASESMEN KEBIDANAN HANDLER")
	rh.Logging.Info(payload.RiwayatPenyakitKeluarga)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSaveAsesmenKebidananBangsalUseCase(*payload, userID, payload.Person, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetAsesmenKebidahanHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetAsesmenAwalKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err123 := rh.KebidananUseCase.OnGetKeluhanUtamaKebidananUsecase(userID, modulID, *payload)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("DATA ASESMEN KEBIDANAN")
	rh.Logging.Info(data)

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetPengkajianPersistemHandler(c *fiber.Ctx) error {
	payload := new(dto.RegPengkajianPersistem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	data, err123 := rh.KebidananRepository.OnGetPengkajianPersistemMariaRepository(modulID, payload.Noreg)

	rh.Logging.Info("DATA ")
	rh.Logging.Info(data)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMapperReponsePengkajianKebidananBangsal(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ===================== PENGKAJIAN PERSISTEM ICU
func (rh *KebidananHandler) OnGetPengkajianPersistemICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPengkajianICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload)

	data, err123 := rh.KebidananRepository.OnGetPengkajianPersistemICURepository(userID, modulID, payload.Person, payload.Noreg)

	rh.Logging.Info("PENGKAJIAN PERSISTEM ICU")
	rh.Logging.Info(data)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMappingResponsePengkajianPersistemICU(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetAsesmenUlangKeperatanIntenveFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPengkajianPersistemICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ON GET ASESMEN//ULANG KEPERAWATAN
	data, err12 := rh.KebidananRepository.OnGetAsesmenUlangPerawatIntensive(userID, modulID, payload.Person, payload.Noreg)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	penyakitKeluarga, _ := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRM)
	riwayatPenyakit, _ := rh.RMERepository.OnGetRiwayatPenyakitDahuluPerawat(payload.NoRM, payload.Tanggal)

	mapper := rh.KebidananMapper.ToAsesmenUlangPerawatanIntesiveMapper(data, penyakitKeluarga, riwayatPenyakit)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSaveAsesmenIntensiveFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnRegAsesmenUlangKeperawatanIntensive)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// UPDATE ASESMEN INTENSIVE
	saveData, message, er123 := rh.KebidananUseCase.OnSaveAsesmenIntensiveUseCase(userID, modulID, *payload)

	if er123 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, saveData)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPersistemICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestPengkajianPersistemICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("SISTEM ELIMINASI")
	rh.Logging.Info(payload.SistemEliminasi)
	rh.Logging.Info("SISTEM MOBILISASI")
	rh.Logging.Info(payload.SistemMobilisasi)

	data, message, err123 := rh.KebidananUseCase.OnSavePengkajianPersistemIcuICUUsecase(userID, modulID, payload.Person, payload.Noreg, *payload)

	if err123 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetPengkajianPersistemKeperawatanHandler(c *fiber.Ctx) error {
	payload := new(dto.RegPengkajianPersistem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err123 := rh.KebidananRepository.GetPengkajianPersistemKeperawatanRepository(userID, modulID, payload.Person, payload.Noreg)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMapperReponsePengkajianKeperawatanBangsal(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetPengkajianPersistemAnakKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegPengkajianPersistemAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	data, err123 := rh.KebidananRepository.GetPengkajianPersistemAnakKeperawatanRepository(modulID, payload.Noreg)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMapperReponsePengkajianKeperawatanBangsal(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPerSistemHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianPersistem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("DATA PENGKAJIAN PERSISTEM")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSaveAsesmenPengkajianPersistemUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPerSistemKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianPersistem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("DATA PENGKAJIAN PERSISTEM")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSavePengkajianPersistemKebidananUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPersistemKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSaveAsesmenPengkajianKeperawatanDewasaPersistemUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPersistemDewasaKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSaveAsesmenPengkajianKeperawatanDewasaPersistemUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPersistemAnakKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("PRINT GENITALIA")
	rh.Logging.Info(payload.Genitalia)

	rh.Logging.Info("PRINT DATA")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSavePengkajianPersistemAnakUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSavePengkajianPersistemFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.KebidananUseCase.OnSaveAsesmenPengkajianKeperawatanDewasaPersistemUseCase(*payload, userID, payload.Noreg, modulID)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetPengkajianKebidananHandler(c *fiber.Ctx) error {
	payload := new(dto.RegPengkajianKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// kebidanans, err12 := su.kebidananRepository.GetAsesmenKebidananRepository(userID, kdBagian, person, req.NoReg)
	data, err123 := rh.KebidananRepository.GetPengkajianKebidananRepository(userID, modulID, payload.Person, payload.Noreg)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.KebidananMapper.ToMapperResponseKebidanan(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetAsesmenFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err123 := rh.KebidananUseCase.OnGetAsesmenKebidananUsecase(userID, payload.Noreg, modulID, payload.Person)

	if err123 != nil {
		response := helper.APIResponseFailure(err123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSaveAsesmenKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveAsesmenKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("HAID TERAKHIR")
	rh.Logging.Info(payload.AseskepKehamilanHaidTerakhir)

	data, message, err12 := rh.KebidananUseCase.OnSaveAsesmenKebidananUseCase(userID, modulID, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) ReportAsesmenPengkajianKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportPengkajianKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err12 := rh.KebidananUseCase.OnGetReportPengkajianKebidananUseCase(modulID, payload.Person, payload.Noreg, payload.NoRM)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) ReportAsesmenPengkajianKebidananAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportPengkajianKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// REPORT KENGKAJIAN KEBIDANAN USECASE
	res, er112 := rh.KebidananUseCase.OnGetReportPengkajianKebidananBatuRajaUseCaseV2(payload.Noreg, payload.NoRM)

	if er112 != nil {
		response := helper.APIResponseFailure(er112.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info(res)

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *KebidananHandler) TriaseIGDDokterKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetTriaseIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// GET TRIASE IGD DOKTER
	data, err12 := rh.KebidananUseCase.OnGetTriaseIGDDokterUseCase(payload.Noreg, userID, modulID, payload.Pelayanan)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnSaveTriaseIGDDokterKebidananFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReqSaveTriaseIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, data, err121 := rh.KebidananUseCase.OnSaveTriaseIGDDokterUseCase(payload.Noreg, userID, modulID, payload.Pelayanan, *payload)

	if err121 != nil {
		response := helper.APIResponseFailure(err121.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnReportTriaseIGDDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportTriaseIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	report := rh.KebidananUseCase.OnGetReportTriaseIGDDokterUseCase(payload.Noreg, payload.NoRM, payload.Tanggal)

	response := helper.APIResponse("OK", http.StatusOK, report)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnReportTriasePonekFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportTriaseIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	report := rh.KebidananUseCase.OnGetReportTriasePonekUseCase(payload.Noreg, payload.NoRM, payload.Tanggal)
	response := helper.APIResponse("OK", http.StatusOK, report)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnGetDiagnosaBandingHandler(c *fiber.Ctx) error {
	payload := new(dto.RegDianosaBandingIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	get, err12 := rh.KebidananRepository.GetDiagnosaBandingIGDDokterRepository(userID, modulID, payload.Person, payload.Noreg)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ToMapperResponseDiagnosaBanding(data kebidanan.DiagnosaBandingDokter) (res dto.ResponseDiagnosaBanding)

	mapper := rh.KebidananMapper.ToMapperResponseDiagnosaBanding(get)

	rh.Logging.Info("CETAK DATA DIAGNOSA BANDING")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *KebidananHandler) OnSaveDiagnosaBandingHandler(c *fiber.Ctx) error {
	payload := new(dto.RegSaveDianosaBandingIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, err12 := rh.KebidananUseCase.OnSaveDiagnosaBandingUseCase(userID, modulID, *payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *KebidananHandler) OnGetTriaseIGDHandler(c *fiber.Ctx) error {
	payload := new(dto.RegSaveDianosaBandingIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	return nil
}

func (rh *KebidananHandler) OnReportPengkajianAwalMedisHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportPengkajianAwalDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err12 := rh.KebidananUseCase.OnGetReportPengkajianAwalMedisDokterUseCase(payload.Noreg, modulID, "Dokter", "ranap", payload.Tanggal, payload.NoRM)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *KebidananHandler) OnReportPengkajianAwalKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportPengkajianAwalPerawat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err12 := rh.KebidananUseCase.OnGetReportPengkajianAwalPerawatUseCase(payload.Noreg, modulID, "Perawat", "ranap", payload.Tanggal, payload.NoRM)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}
