package kebidanan

type (
	// TRIASE IGD DOKTER
	TriaseIGDDokter struct {
		InsertDttm     string
		InsertDevice   string
		InsertUserId   string
		KetPerson      string
		Pelayanan      string
		Kategori       string
		KdBagian       string
		Noreg          string
		SkalaNyeri     int
		SkalaNyeriP    string
		SkalaNyeriQ    string
		SkalaNyeriR    string
		SkalaNyeriS    string
		SkalaNyeriT    string
		FlaccWajah     int
		FlaccKaki      int
		FlaccAktifitas int
		FlaccMenangis  int
		FlaccBersuara  int
		FlaccTotal     int
		SkalaTriase    int
		SkalaTriaseIgd string
	}

	AsesmenTriaseIGD struct {
		InsertDttm              string
		InsertPc                string
		InsertUserId            string
		TglMasuk                string
		KeteranganPerson        string
		Pelayanan               string
		KdBagian                string
		Noreg                   string
		AseskepGangguanPerilaku string
		AseskepKehamilanDjj     string
		AseskepAlasanMasuk      string
		AseskepCaraMasuk        string
		AseskepPenyebabCedera   string
		AseskepKehamilan        string
		AseskepKehamilanGravida string
		AseskepKehamilanPara    string
		AseskepKehamilanAbortus string
		AseskepKehamilanHpht    string
		AseskepKehamilanTtp     string
	}
)

func (TriaseIGDDokter) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (AsesmenTriaseIGD) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
