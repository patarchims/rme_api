package kebidanan

import "hms_api/modules/user"

type (
	DRiwayatKehamilan struct {
		Id                   int    `json:"id"`
		InsertDttm           string `json:"insert_dttm"`
		InsertUserId         string `json:"user_id"`
		InsertPc             string `json:"device_id"`
		NoRm                 string `json:"no_rm"`
		KdRiwayat            string `json:"kd_riwayat"`
		TahunPersalinan      string `json:"tahun"`
		TempatPersalinan     string `json:"tempat"`
		Noreg                string `json:"noreg"`
		UmurKehamilan        string `json:"umur"`
		JenisPersalinan      string `json:"jenis_persalinan"`
		KomplikasiHamil      string `json:"komplikasi_hamil"`
		KomplikasiPersalinan string `json:"komplikasi_persalinan"`
		Penolong             string `json:"penolong"`
		Penyulit             string `json:"penyulit"`
		Nifas                string `json:"nifas"`
		Jk                   string `json:"jk"`
		Bb                   string `json:"bb"`
		KeadaanSekarang      string `json:"keadaan_sekarang"`
	}

	DRiwayatPengobatanDirumah struct {
		InsertDttm             string `json:"insert_dttm"`
		InsertUserId           string `json:"user_id"`
		InsertPc               string `json:"device_id"`
		KdRiwayat              string `json:"kd_riwayat"`
		Noreg                  string `json:"noreg"`
		NamaObat               string `json:"nama_obat"`
		Dosis                  string `json:"dosis"`
		CaraPemberian          string `json:"cara_pemberian"`
		Frekuensi              string `json:"frekuensi"`
		WaktuTerakhirPemberian string `json:"waktu"`
		KdBagian               string `json:"kd_bagian"`
		Usia                   string `json:"usia"`
	}

	ResponseRiwayatPengobatanDiRumah struct {
		Noreg                  string `json:"noreg"`
		NamaObat               string `json:"nama_obat"`
		Dosis                  string `json:"dosis"`
		CaraPemberian          string `json:"cara_pemberian"`
		Frekuensi              string `json:"frekuensi"`
		WaktuTerakhirPemberian string `json:"waktu"`
		KdBagian               string `json:"kd_bagian"`
		Usia                   string `json:"usia"`
	}

	DRisikoJatuhKebidanan struct {
		InsertDttm      string `json:"insert_dttm"`
		UpdDttm         string `json:"upd_dttm"`
		InsertUserId    string `json:"user_id"`
		InsertPc        string `json:"insert_pc"`
		KetPerson       string `json:"person"`
		Pelayanan       string `json:"pelayanan"`
		KdBagian        string `json:"kd_bagian"`
		Noreg           string `json:"noreg"`
		Kategori        string `json:"kategori"`
		RJatuh          string `json:"r_jatuh"`
		Diagnosis       string `json:"diagnosis"`
		AlatBantu1      string `json:"alat_bantu1"`
		AlatBantu2      string `json:"alat_bantu2"`
		AlatBantu3      string `json:"alat_bantu3"`
		TerpasangInfuse string `json:"terpasang_infuse"`
		Total           int    `json:"total"`
	}

	// DPEMFISIK
	DPemFisikKebidanan struct {
		InsertDttm        string `json:"insert_dttm"`
		UpdateDttm        string `json:"upd_dttm"`
		InsertDevice      string `json:"device"`
		InsertUserId      string `json:"user_id"`
		Kategori          string `json:"kategori"`
		KetPerson         string `json:"person"`
		Pelayanan         string `json:"pelayanan"`
		KdBagian          string `json:"kd_bagian"`
		Noreg             string `json:"noreg"`
		Kepala            string `json:"kepala"`
		Mata              string `json:"mata"`
		Telinga           string `json:"telinga"`
		LeherDanBahu      string `json:"leher_dah_bahu"`
		Mulut             string `json:"mulut"`
		MucosaMulut       string `json:"mucosa_mulut"`
		Gigi              string `json:"gigi"`
		Hidung            string `json:"hidung"`
		Tenggorokan       string `json:"tenggorokan"`
		Payudara          string `json:"payudara"`
		Abdomen           string `json:"abdomen"`
		NutrisiDanHidrasi string `json:"nutrisi_dan_hidrasi"`
	}

	DPengkajianFungsional struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string `json:"upd_dttm"`
		InsertPc     string `json:"device"`
		InsertUserId string `json:"user_id"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		F1           string `json:"f1"`
		F2           string `json:"f2"`
		F3           string `json:"f3"`
		F4           string `json:"f4"`
		F5           string `json:"f5"`
		F6           string `json:"f6"`
		F7           string `json:"f7"`
		F8           string `json:"f8"`
		F9           string `json:"f9"`
		F10          string `json:"f10"`
		Nilai        int    `json:"nilai"`
	}

	DPengkajianNutrisi struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string `json:"upd_dttm"`
		InsertPc     string `json:"device"`
		InsertUserId string `json:"user_id"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		N1           string `json:"n1"`
		N2           string `json:"n2"`
		Nilai        int    `json:"nilai"`
	}

	DPengkajianNutrisiAnak struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string `json:"upd_dttm"`
		InsertPc     string `json:"device"`
		InsertUserId string `json:"user_id"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		N1           string `json:"n1"`
		N2           string `json:"n2"`
		N3           string `json:"n3"`
		N4           string `json:"n4"`
		Nilai        int    `json:"nilai"`
	}

	DiagnosaKebidanan struct {
		InsertDttm   string `json:"insert_dttm"`
		NoDiagnosa   string `json:"no_diangosa"`
		UpdDttm      string `json:"upd_dttm"`
		InsertPc     string `json:"insert_pc"`
		InsertUserId string `json:"user_id"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		CodeDiagnosa string `json:"code_diagnosa"`
	}

	KDiagnosaKebidanan struct {
		KodeDiagnosa string `json:"kode_diagnosa"`
		NamaDiagnosa string `json:"nama_diagnosa"`
	}

	DDiagnosaKebidanan struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string `json:"upd_dttm"`
		InsertPc     string `json:"insert_pc"`
		InsertUserId string `json:"user_id"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		NoDiagnosa   int    `json:"no_diagnosa"`
		KodeDiagnosa string `json:"kode_diagnosa"`
		NamaDiagnosa string `json:"nama_diagnosa"`
	}

	// DATA KEBIDANAN
	DImplementasiKeperawatan struct {
		Id         int           `json:"id"`
		InsertDttm string        `json:"insert_dttm"`
		NoDaskep   string        `json:"no_daskep"`
		UserId     string        `json:"user_id"`
		KdBagian   string        `json:"kd_bagian"`
		Deskripsi  string        `json:"deskripsi"`
		Karyawan   user.Karyawan `gorm:"foreignKey:UserId" json:"karyawan"`
	}

	PemeriksaanFisikKebidanan struct {
		InsertDttm               string `json:"insert_dttm"`
		UpdDttm                  string `json:"upd_dttm"`
		InsertPc                 string `json:"device"`
		InsertUserId             string `json:"user_id"`
		KdBagian                 string `json:"kd_bagian"`
		Noreg                    string `json:"noreg"`
		Pelayanan                string `json:"pelayanan"`
		ObgHamilG                string
		ObgHamilP                string
		ObgHamilAb               string
		ObgMens                  string
		ObgTerakhirHaid          string
		ObgUsiaKehamilan         string
		ObgPerkiraanPartus       string
		ObgHamilMuda             string
		ObgHamilTua              string
		ObgLeopold1              string
		ObgLeopold2              string
		ObgLeopold3              string
		ObgLeopold4              string
		ObgTfu                   string
		ObgTbj                   string
		ObgNyeriTekan            string
		ObgAuskultasiDdj         string
		ObgAuskultasi            string
		ObgHis                   string
		ObgHisTgl                string
		ObgHisKualitas           string
		ObgHisFrek               string
		ObgHisDetail             string
		ObgPengeluaranPerVaginam string
		ObgPengeluaranTgl        string
		ObgPembukaan             string
		ObgPortio_eff            string
		ObgPresentasi            string
		ObgPenurun               string
		ObgGynekologiHodge       string
		ObgGynekologiInspeksi    string
		ObgGynekologiPalpasi     string
		ObgGynekologiVagina      string
		ObgGynekologiPortio      string
		ObgGynekologiPemeriksaan string
	}

	// KEBIDANAN	// SELECT

	// KEBIDANAN REPOSITORY
	DVitalSignKebidananBangsal struct {
		InsertDttm   string
		InsertUserId string
		Pelayanan    string
		KdBagian     string
		InsertDevice string
		Kategori     string
		KetPerson    string
		Noreg        string
		Td           string
		Tb           string
		Bb           string
		Nadi         string
		Suhu         string
		Pernafasan   string
	}

	DPemFisikKebidananBangsalRepository struct {
		InsertDttm   string
		Noreg        string
		Pelayanan    string
		InsertUserId string
		KdBagian     string
		InsertDevice string
		Kategori     string
		KetPerson    string
		GcsE         string
		GcsV         string
		GcsM         string
		Ddj          string
		Tfu          string
		Pupil        string
		Kesadaran    string
		Akral        string
	}

	// ASESMEN KEBIDANAN
	AsesemenKebidananBangsal struct {
		InsertDttm       string
		KeteranganPerson string
		InsertUserId     string
		InsertPc         string
		TglMasuk         string
		TglKeluar        string
		Pelayanan        string
		KdBagian         string
		Noreg            string
		KdDpjp           string
		AseskepKel       string
		AseskepRwytPnykt string
		// SELECT aseskep_rwt_pnykt_keluarga FROM vicore_rme.`dcppt_soap_pasien`
		AseskepRwtPnyktKeluarga string
		AseskepRwytMenstruasi   string
		AseskepKelMenyertai     string
		AseskepSiklusHaid       string
		AseskepRwytPernikahan   string
		AseskepRwytPnyktDahulu  string
	}

	AsesmenKebidananModel struct {
		InsertDttm                       string
		KeteranganPerson                 string
		InsertUserId                     string
		InsertPc                         string
		TglMasuk                         string
		KdBagian                         string
		Pelayanan                        string
		KdDpjp                           string
		Noreg                            string
		AseskepKehamilanGravida          string
		AseskepKehamilanAbortus          string
		AseskepKehamilanPara             string
		AseskepKehamilanHaid             string
		AseskepHaidTerakhir              string
		AseskepKehamilanUsiaKehamilan    string
		AseskepUsiaKehamilan             string
		AseskepPartusHpl                 string
		AseskepKehamilanLeopold1         string
		AseskepKehamilanLeopold2         string
		AseskepKehamilanLeopold3         string
		AseskepKehamilanLeopold4         string
		AseskepKehamilanHodge            string
		AseskepKehamilanInspeksi         string
		AseskepKehamilanInspekuloV       string
		AseskepKehamilanInspekuloP       string
		AseskepKehamilanPemeriksaanDalam string
		AseskepKehamilanHamilMuda        string
		AseskepKehamilanHamilTua         string
		AseskepKehamilanTbj              string
		AseskepKehamilanTfu              string
		AseskepKehamilanNyeriTekan       string
		AseskepKehamilanPalpasi          string
	}

	AsesemenPersistemKebidananBangsal struct {
		InsertDttm                     string
		KeteranganPerson               string
		InsertUserId                   string
		InsertPc                       string
		TglMasuk                       string
		TglKeluar                      string
		Pelayanan                      string
		KdBagian                       string
		Noreg                          string
		AseskepSistemEliminasiBak      string
		AseskepSistemMasalahProstat    string
		AseskepSistemEliminasiBab      string
		AseskepSistemIstirahat         string
		AseskepSistemAktivitas         string
		AseskepSistemMandi             string
		AseskepSistemBerpakaian        string
		AseskepSistemMakan             string
		AseskepSistemEliminasi         string
		AseskepSistemMobilisasi        string
		AseskepSistemKardiovaskuler    string
		AseskepSistemRespiratori       string
		AseskepSistemSecebral          string
		AseskepSistemPerfusiPerifer    string
		AseskepSistemPencernaan        string
		AseskepSistemIntegumen         string
		AseskepSistemKenyamanan        string
		AseskepSistemProteksi          string
		AseskepSistemPapsSmer          string
		AseskepSistemHamil             string
		AseskepSistemPendarahan        string
		AseskepSistemHambatanBahasa    string
		AseskepSistemCaraBelajar       string
		AseskepSistemBahasaSehari      string
		AseskepSistemSpikologis        string
		AseskepSistemHambatanSosial    string
		AseskepSistemHambatanEkonomi   string
		AseskepSistemHambatanSpiritual string
		AseskepSistemResponseEmosi     string
		AseskepSistemNilaiKepercayaan  string
		AseskepSistemPresepsiSakit     string
		AseskepSistemGenitalia         string
		AseskepSistemThermoregulasi    string
		AseskepSistemKhususKepercayaan string
		AseskepSistemPenerjemah        string
		AseskepSistemPencernaanUsus    string
		AseskepSistemLemahAnggotaGerak string
		AseskepSistemSakitKepala       string
		AseskepSistemStatusMental      string
		AseskepSistemBicara            string
		AseskepSistemRiwayatHipertensi string
		AseskepSistemKekuatanOtot      string
		AseskepSistemAkral             string
		AseskepSistemBatuk             string
		AseskepSistemSuaraNapas        string
		AseskepSistemMerokok           string
		AseskepSistemNutrisi           string
	}

	AsesmenUlangPerawatanIntensive struct {
		InsertDttm                   string
		KeteranganPerson             string
		TglMasuk                     string
		InsertUserId                 string
		InsertPc                     string
		Pelayanan                    string
		KdBagian                     string
		Noreg                        string
		KdDpjp                       string
		Asesmen                      string
		AseskepRwtDari               string
		AseskepCaraMasuk             string
		AseskepAsalMasuk             string
		AseskepKel                   string
		AseskepRwytPnykt             string
		AseskepRwytPnyktDahulu       string
		AseskepReaksiYangMuncul      string
		AseskepReaksiAlergi          string
		AseskepTransfusiDarah        string
		AseskepRwtMerokok            string
		AseskepRwtMinumanKeras       string
		AseskepRwtAlkoholMempegaruhi string
		Nama                         string
	}

	AsesemenAnakKeperawatan struct {
		Pengkajian              string
		KeluhanUtama            string
		RiwayatPenyakitSekarang string
		RiwayatAlergi           string
		ReaksiYangTimbul        string
	}

	AsesmenPengkajianPersistemICUBangsal struct {
		InsertDttm                               string
		UpdDttm                                  string
		KeteranganPerson                         string
		InsertUserId                             string
		InsertPc                                 string
		TglMasuk                                 string
		TglKeluar                                string
		JamCheckOut                              string
		JamCheckInRanap                          string
		Pelayanan                                string
		KdBagian                                 string
		Noreg                                    string
		KdDpjp                                   string
		AseskepSistemAirway                      string
		AsekepSistemBreathing                    string
		AseskepSistemCirculation                 string
		AseskepSistemNutrisi                     string
		AseskepSistemPadaBayi                    string
		AseskepSistemMinum                       string
		AseskepSistemEliminasiBak                string
		AseskepSistemEliminasiBab                string
		AseskepSistemTidurAtauIstirahat          string
		AseskepSistemAktivitas                   string
		AseskepSistemBerjalan                    string
		AseskepSistemPenggunaanAlatBantu         string
		AseskepSistemPerfusiSerebral             string
		AseskepSistemPupil                       string
		AseskepSistemRefleksCahaya               string
		AseskepSistemPerfusiRenal                string
		AseskepSistemPerfusiGastrointestinal     string
		AseskepSistemAbdomen                     string
		AseskepSistemThermoregulasi              string
		AseskepSistemKenyamanan                  string
		AsekepSistemKualitas                     string
		AsekepSistemPola                         string
		AseskepSistemNyeriMempegaruhi            string
		AseskepSistemStatusMental                string
		AseskepSistemKejang                      string
		AseskepSistemPasangPengamananTempatTidur string
		AseskepSistemBelMudahDijangkau           string
		AseskepSistemPenglihatan                 string
		AseskepSistemPendengaran                 string
		AseskepSistemHamil                       string
		AseskepSistemCervixTerakhir              string
		AseskepSistemPenggunaanAlatKontrasepsi   string
		AseskepPersistemMasalahProstat           string
		AseskepSistemBicara                      string
		AseskepBahasaSehariHari                  string
		AseskepPerluPenerjemah                   string
		AseskepBahasaIsyarat                     string
		AseskepHambatanBelajar                   string
		AseskepCaraBelajarYangDisukai            string
		AseskepTingkatPendidikan                 string
		AseskepKebutuhanPembelajaran             string
		AseskepReponseEmosi                      string
		AseskepSistemPekerjaan                   string
		AseskepSistemTinggalBersama              string
		AseskepKondisiLingkungan                 string
		AseskepSistemAksesMasuk                  string
		AseskepSistemKamarMandi                  string
		AseskepSistemNilaiKepercayaan            string
		AseskepSistemMenjalankanIbadah           string
		AseskepSistemPresepsiTerhadapSakit       string
		AseskepSistemPemimpinAgama               string
		AseskepSistemMandi                       string
		AseskepSistemBerpakaian                  string
		AseskepSistemMakan                       string
		AseskepSistemEliminasi                   string
		AseskepSistemMobilisasi                  string
		AseskepMslhDenganNutrisi                 string
	}

	AsesemenPersistemKeperawatanBangsal struct {
		InsertDttm                     string
		KeteranganPerson               string
		InsertUserId                   string
		InsertPc                       string
		TglMasuk                       string
		TglKeluar                      string
		Pelayanan                      string
		KdBagian                       string
		Noreg                          string
		AseskepSistemEliminasiBak      string
		AseskepSistemEliminasiBab      string
		AseskepSistemIstirahat         string
		AseskepSistemAktivitas         string
		AseskepSistemMandi             string
		AseskepSistemBerpakaian        string
		AseskepSistemMakan             string
		AseskepSistemEliminasi         string
		AseskepSistemMobilisasi        string
		AseskepSistemGenitalia         string
		AseskepSistemKardiovaskuler    string
		AseskepSistemRespiratori       string
		AseskepSistemSecebral          string
		AseskepSistemPerfusiPerifer    string
		AseskepSistemPencernaan        string
		AseskepSistemIntegumen         string
		AseskepSistemKenyamanan        string
		AseskepSistemProteksi          string
		AseskepSistemPapsSmer          string
		AseskepSistemHamil             string
		AseskepSistemPendarahan        string
		AseskepSistemHambatanBahasa    string
		AseskepSistemCaraBelajar       string
		AseskepSistemBahasaSehari      string
		AseskepSistemSpikologis        string
		AseskepSistemHambatanSosial    string
		AseskepSistemHambatanEkonomi   string
		AseskepSistemHambatanSpiritual string
		AseskepSistemPenerjemah        string
		AseskepSistemResponseEmosi     string
		AseskepSistemNilaiKepercayaan  string
		AseskepSistemPresepsiSakit     string
		AseskepSistemThermoregulasi    string
		// aseskep_sistem_thermoregulasi_dingin
		AseskepSistemThermoregulasiDingin string
		AseskepSistemKhususKepercayaan    string
		AseskepSistemPencernaanUsus       string
		AseskepSistemLemahAnggotaGerak    string
		AseskepSistemSakitKepala          string
		AseskepSistemStatusMental         string
		AseskepSistemBicara               string
		AseskepSistemRiwayatHipertensi    string
		AseskepSistemKekuatanOtot         string
		AseskepSistemAkral                string
		AseskepSistemBatuk                string
		AseskepSistemSuaraNapas           string
		AseskepSistemMerokok              string
		AseskepSistemNutrisi              string
	}

	// PENKAJIAN KEBIDANAN
	PengkajianKebidanan struct {
		InsertDttm                       string
		KeteranganPerson                 string
		InsertUserId                     string
		InsertPc                         string
		TglMasuk                         string
		TglKeluar                        string
		Pelayanan                        string
		KdBagian                         string
		Noreg                            string
		AseskepKehamilanGravida          string
		AseskepKehamilanAbortus          string
		AseskepKehamilanPara             string
		AseskepKehamilanLeopold1         string
		AseskepKehamilanLeopold2         string
		AseskepKehamilanLeopold3         string
		AseskepKehamilanLeopold4         string
		AseskepKehamilanHaid             string
		AseskepKehamilanPemeriksaanDalam string
		AseskepKehamilanInspekuloP       string
		AseskepKehamilanInspekuloV       string
		AseskepKehamilanPalpasi          string
		AseskepKehamilanInspeksi         string
		AseskepKehamilanHodge            string
		AseskepKehamilanNyeriTekan       string
		AseskepKehamilanTbj              string
		AseskepKehamilanTfu              string
		AseskepKehamilanHamilTua         string
		AseskepKehamilanHamilMuda        string
		AseskepKehamilanHpl              string
		AseskepKehamilanUsiaKehamilan    string
		AseskepKehamilanHaidTerakhir     string
	}

	DiagnosaBandingDokter struct {
		InsertDttm          string
		KeteranganPerson    string
		InsertUserId        string
		InsertPc            string
		TglMasuk            string
		TglKeluar           string
		Pelayanan           string
		KdBagian            string
		Noreg               string
		AsesmedDiagBanding  string
		KdDpjp              string
		AsesmedDokterTtd    string
		AsesmedDokterTtdTgl string
		AsesmedDokterTtdJam string
	}

	// ===================== //
	ReportPengkajianKebidanan struct {
		InsertDttm                       string
		UpdDttm                          string
		KeteranganPerson                 string
		InsertUserId                     string
		InsertPc                         string
		TglMasuk                         string
		TglKeluar                        string
		Pelayanan                        string
		KdBagian                         string
		Noreg                            string
		AseskepKel                       string
		AseskepKelMenyertai              string
		AseskepRwytPnykt                 string
		AseskepRwytPnyktDahulu           string
		AseskepRwytMenstruasi            string
		AseskepSiklusHaid                string
		AseskepRwytPernikahan            string
		AseskepReaksiAlergi              string
		AseskepKehamilanGravida          string
		AseskepKehamilanPara             string
		AseskepKehamilanAbortus          string
		AseskepKehamilanHpht             string
		AseskepKehamilanTtp              string
		AseskepKehamilanLeopold1         string
		AseskepKehamilanLeopold2         string
		AseskepKehamilanLeopold3         string
		AseskepKehamilanLeopold4         string
		AseskepSistemPresepsiSakit       string
		AseskepSistemKhususKepercayaan   string
		AseskepSistemResponseEmosi       string
		AseskepSistemHambatanSosial      string
		AseskepSistemSpikologis          string
		AseskepSistemThermoregulasi      string
		AseskepSistemBahasaSehari        string
		AseskepSistemCaraBelajar         string
		AseskepSistemHambatanBahasa      string
		AseskepSistemPendarahan          string
		AseskepSistemHamil               string
		AseskepSistemPapsSmer            string
		AseskepSistemProteksi            string
		AseskepSistemKenyamanan          string
		AseskepSistemIntegumen           string
		AseskepSistemPencernaan          string
		AseskepSistemPerfusiPerifer      string
		AseskepSistemSecebral            string
		AseskepSistemRespiratori         string
		AseskepSistemKardiovaskuler      string
		AseskepSistemMobilisasi          string
		AseskepSistemEliminasi           string
		AseskepSistemMakan               string
		AseskepSistemBerpakaian          string
		AseskepSistemMandi               string
		AseskepKehamilanInspekuloP       string
		AseskepKehamilanInspekuloV       string
		AseskepKehamilanHaid             string
		AseskepKehamilanPemeriksaanDalam string
		AseskepSistemNilaiKepercayaan    string
		AseskepPartusHpl                 string
		AseskepKehamilanTbj              string
		AseskepKehamilanTfu              string
		AseskepKehamilanHamilMuda        string
		AseskepKehamilanHamilTua         string
		AseskepKehamilanUsiaKehamilan    string
		AseskepKehamilanHaidTerakhir     string
		AseskepSistemEliminasiBak        string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab        string `json:"eliminasi_bab"`
		AseskepSistemIstirahat           string `json:"istirahat"`
		AseskepSistemAktivitas           string `json:"aktivitas"`
		AseskepSistemHambatanEkonomi     string `json:"ekonomi"`
		AseskepSistemHambatanSpiritual   string `json:"spiritual"`
		AseskepKehamilanLendir           string
		AseskepKehamilanHis              string
	}

	PengkajianNyeriModel struct {
		Noreg                 string
		InsertDttm            string
		KeteranganPerson      string
		InsertUserId          string
		InsertPc              string
		Pelayanan             string
		KdBagian              string
		KdDpjp                string
		TglMasuk              string
		AseskepNyeri          int
		AseskepLokasiNyeri    string
		AseskepFrekuensiNyeri string
		AseskepNyeriMenjalar  string
		AseskepKualitasNyeri  string
	}

	// PENGKAJIAN NYERI NIPS ANAK
	PengkajianNyeriNIPS struct {
		Noreg         string
		InsertDttm    string
		KdBagian      string
		EkspresiWajah string
		Tangisan      string
		PolaNapas     string
		Tangan        string
		Kaki          string
		Kesadaran     string
		Total         int
	}

	// PENGKAJIAN  NYERI ICU
	PengkajianNyeriICU struct{}
)

func (DVitalSignKebidananBangsal) TableName() string {
	return "vicore_rme.dvital_sign"
}

func (PengkajianNyeriModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
func (AsesmenUlangPerawatanIntensive) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (PengkajianNyeriNIPS) TableName() string {
	return "vicore_rme.dnyeri_nips"
}

func (DPengkajianNutrisi) TableName() string {
	return "vicore_rme.dpengkajian_nutrisi"
}

func (AsesmenPengkajianPersistemICUBangsal) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (ReportPengkajianKebidanan) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesemenPersistemKebidananBangsal) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
func (AsesmenKebidananModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DiagnosaBandingDokter) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (PengkajianKebidanan) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesemenKebidananBangsal) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DPemFisikKebidananBangsalRepository) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (DRiwayatKehamilan) TableName() string {
	return "vicore_rme.driwayat_kehamilan"
}

func (PemeriksaanFisikKebidanan) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (KDiagnosaKebidanan) TableName() string {
	return "vicore_lib.kdiagnosa_kebidanan"
}

func (DImplementasiKeperawatan) TableName() string {
	return "vicore_rme.dimplementasi_keperawatan"
}

func (DDiagnosaKebidanan) TableName() string {
	return "vicore_rme.ddiagnosa_kebidanan"
}

func (DiagnosaKebidanan) TableName() string {
	return "vicore_rme.ddiagnosa_kebidanan"
}

func (DPengkajianNutrisiAnak) TableName() string {
	return "vicore_rme.dpengkajian_nutrisi"
}

func (DPengkajianFungsional) TableName() string {
	return "vicore_rme.dpengkajian_fungsional"
}

func (DPemFisikKebidanan) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (DRiwayatPengobatanDirumah) TableName() string {
	return "vicore_rme.driwayat_pengobatan_dirumah"
}
func (DRisikoJatuhKebidanan) TableName() string {
	return "vicore_rme.drisiko_jatuh"
}
