package mapper

import (
	"hms_api/modules/his"
	"hms_api/modules/kebidanan"
	"hms_api/modules/kebidanan/dto"
	kebidananDTO "hms_api/modules/kebidanan/dto"
	"hms_api/modules/kebidanan/entity"
	"hms_api/modules/rme"
	rmeDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	soapDto "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type KebidananMapperImpl struct{}

func NewKebidananMapperImple() entity.KebidananMapper {
	return &KebidananMapperImpl{}
}

// func (km *KebidananMapperImpl) ToReposePengobatanDirumah() kebidananDTO.

func (km *KebidananMapperImpl) ToVitalSignKebidananMapper(data rme.DVitalSign, gCSE string, gcsV string, gcsM string, ddj string) (res kebidananDTO.ResVitalSignKebidanan) {
	return kebidananDTO.ResVitalSignKebidanan{
		DDJ:          ddj,
		TekananDarah: data.Td,
		Nadi:         data.Nadi,
		Pernapasan:   data.Pernafasan,
		Suhu:         data.Suhu,
		TinggiBadan:  data.Tb,
		BeratBadan:   data.Bb,
		GcsE:         gCSE,
		GcsV:         gcsV,
		GcsM:         gcsM,
	}
}

func (a *KebidananMapperImpl) ToMappingDiagnosaKebidanan(data []kebidanan.KDiagnosaKebidanan) (value []kebidananDTO.ResDiagnosaKebidananResponse) {

	for _, V := range data {
		value = append(value, kebidananDTO.ResDiagnosaKebidananResponse{
			KodeDiagnosa: V.KodeDiagnosa,
			NamaDiagnosa: V.NamaDiagnosa, IsSelected: false,
		})
	}

	return value
}

func (a *KebidananMapperImpl) ToMappingDiagnosaSaveResponseKebidanan(data []kebidanan.DDiagnosaKebidanan) (value []kebidananDTO.ResDiagnosaKebidanan) {

	for _, V := range data {
		value = append(value, kebidananDTO.ResDiagnosaKebidanan{
			NoDiagnosa:   V.NoDiagnosa,
			KodeDiagnosa: V.KodeDiagnosa,
			NamaDiagnosa: V.NamaDiagnosa,
		})
	}

	return value
}

func (a *KebidananMapperImpl) ToMappingReportPelaksanaanKebidanan(data []kebidanan.DDiagnosaKebidanan, kper []kebidanan.DImplementasiKeperawatan) (res dto.ResponseReportDiagnosaKebidanan) {

	if len(data) > 0 {
		for _, V := range data {
			res.Diagnosa = append(res.Diagnosa, kebidananDTO.ResDiagnosaKebidanan{
				NoDiagnosa:   V.NoDiagnosa,
				KodeDiagnosa: V.KodeDiagnosa,
				NamaDiagnosa: V.NamaDiagnosa,
			})
		}
	}

	if len(kper) > 0 {
		for _, V := range kper {
			res.Implementasi = append(res.Implementasi, kebidanan.DImplementasiKeperawatan{
				Id:         V.Id,
				InsertDttm: V.InsertDttm,
				NoDaskep:   V.NoDaskep,
				UserId:     V.UserId,
				KdBagian:   V.KdBagian,
				Deskripsi:  V.Deskripsi,
				Karyawan:   V.Karyawan,
			})
		}
	}

	if len(kper) == 0 {
		res.Implementasi = make([]kebidanan.DImplementasiKeperawatan, 0)
	}

	if len(data) == 0 {
		res.Diagnosa = make([]kebidananDTO.ResDiagnosaKebidanan, 0)
	}

	return res
}

func (km *KebidananMapperImpl) ToFungsionalKebidananMapper(data kebidanan.DPengkajianFungsional) (res kebidananDTO.ResDPengkajianFungsional) {
	var angka1 = 0
	var angka2 = 0
	var angka3 = 0
	var angka4 = 0
	var angka5 = 0
	var angka6 = 0
	var angka7 = 0
	var angka8 = 0
	var angka9 = 0
	var angka10 = 0

	if data.F8 == "Dengan Bantuan" {
		angka8 = 5
	}

	if data.F8 == "Mandiri" {
		angka8 = 10
	}

	if data.F9 == "Dengan Bantuan" {
		angka9 = 5
	}

	if data.F9 == "Mandiri" {
		angka9 = 10
	}

	if data.F10 == "Dengan Bantuan" {
		angka10 = 5
	}

	if data.F10 == "Mandiri" {
		angka10 = 10
	}

	if data.F1 == "Dengan Bantuan" {
		angka1 = 5
	}

	if data.F1 == "Mandiri" {
		angka1 = 10
	}

	if data.F2 == "Dengan Bantuan" {
		angka2 = 5
	}

	if data.F2 == "Mandiri" {
		angka2 = 10
	}

	if data.F3 == "Dengan Bantuan" {
		angka3 = 10
	}

	if data.F3 == "Mandiri" {
		angka3 = 15
	}

	if data.F4 == "Dengan Bantuan" {
		angka4 = 0
	}

	if data.F4 == "Mandiri" {
		angka4 = 5
	}

	if data.F5 == "Dengan Bantuan" {
		angka5 = 0
	}

	if data.F5 == "Mandiri" {
		angka5 = 5
	}

	if data.F6 == "Dengan Bantuan" {
		angka6 = 10
	}

	if data.F6 == "Mandiri" {
		angka6 = 15
	}

	if data.F7 == "Dengan Bantuan" {
		angka7 = 5
	}

	if data.F7 == "Mandiri" {
		angka7 = 10
	}

	return kebidananDTO.ResDPengkajianFungsional{
		Noreg:    data.Noreg,
		F1:       data.F1,
		NilaiF1:  angka1,
		NilaiF2:  angka2,
		NilaiF3:  angka3,
		NilaiF4:  angka4,
		NilaiF5:  angka5,
		NilaiF6:  angka6,
		NilaiF7:  angka7,
		NilaiF8:  angka8,
		NilaiF9:  angka9,
		NilaiF10: angka10,
		F2:       data.F2,
		F3:       data.F3,
		F4:       data.F4,
		F5:       data.F5,
		F6:       data.F6,
		F7:       data.F7,
		F8:       data.F8,
		F9:       data.F9,
		F10:      data.F10,
		Nilai:    data.Nilai,
	}
}

func (km *KebidananMapperImpl) ToPengkajianNutrisiMapper(data kebidanan.DPengkajianNutrisi) (res kebidananDTO.ResPengkajianNutrisi) {
	return kebidananDTO.ResPengkajianNutrisi{
		Noreg:   data.Noreg,
		N1:      data.N1,
		N2:      data.N2,
		Nilai:   data.Nilai,
		NilaiN1: 0,
		NilaiN2: 0,
	}
}

func (km *KebidananMapperImpl) ToPengkajianNyeriNips(data kebidanan.PengkajianNyeriNIPS, nyeri rme.PengkajianNyeri) (res dto.ResponsePengkajianNyeriNips) {
	return dto.ResponsePengkajianNyeriNips{
		EkspresiWajah:         data.EkspresiWajah,
		Tangisan:              data.Tangisan,
		PolaNapas:             data.PolaNapas,
		Tangan:                data.Tangan,
		Noreg:                 data.Noreg,
		Kesadaran:             data.Kesadaran,
		Kaki:                  data.Kaki,
		AseskepNyeri:          nyeri.AseskepNyeri,
		AseskepLokasiNyeri:    nyeri.AseskepLokasiNyeri,
		AseskepFrekuensiNyeri: nyeri.AseskepFrekuensiNyeri,
		AseskepNyeriMenjalar:  nyeri.AseskepNyeriMenjalar,
		AseskepKualitasNyeri:  nyeri.AseskepKualitasNyeri,
		Total:                 data.Total,
	}
}

func (km *KebidananMapperImpl) ToAsesmenUlangPerawatanIntesiveMapper(data kebidanan.AsesmenUlangPerawatanIntensive, penyakitKeluarga []rme.DAlergi, dahulus []rme.RiwayatPenyakitDahuluPerawat) (res dto.ResponseAsesmenUlangKeperawatanIntensive) {
	var penyakitKeluargas = []rme.DAlergi{}

	if len(penyakitKeluarga) == 0 || penyakitKeluarga == nil {
		penyakitKeluargas = make([]rme.DAlergi, 0)
	} else {
		penyakitKeluargas = penyakitKeluarga
	}

	var dahulu = []rme.RiwayatPenyakitDahuluPerawat{}

	if len(dahulus) == 0 || dahulus == nil {
		dahulu = make([]rme.RiwayatPenyakitDahuluPerawat, 0)
	} else {
		dahulu = dahulus
	}

	return dto.ResponseAsesmenUlangKeperawatanIntensive{
		Asesmen:                 data.Asesmen,
		CaraMasuk:               data.AseskepCaraMasuk,
		KeluhanUtama:            data.AseskepKel,
		Dari:                    data.AseskepRwtDari,
		RiwayatPenyakitSekarang: data.AseskepRwytPnykt,
		ReaksiYangMuncul:        data.AseskepReaksiYangMuncul,
		TransfusiDarah:          data.AseskepTransfusiDarah,
		RiwayatMerokok:          data.AseskepRwtMerokok,
		RiwayatMinumanKeras:     data.AseskepRwtMinumanKeras,
		AlcoholMempegaruhiHidup: data.AseskepRwtAlkoholMempegaruhi,
		RiwayatPenyakitDahulu:   data.AseskepRwytPnyktDahulu,
		RiwayatPenyakitKeluarga: penyakitKeluargas,
		PenyakitDahulu:          dahulu,
	}
}

func (km *KebidananMapperImpl) ToPengkajianNutrisiAnakMapper(data kebidanan.DPengkajianNutrisiAnak) (res kebidananDTO.ResPengkajianNutrisiAnak) {
	var nilai1 int = 0
	var nilai2 int = 0
	var nilai3 int = 0
	var nilai4 int = 0

	switch data.N1 {
	case "Tidak":
		nilai1 = 0
	case "Ya":
		nilai1 = 1
	default:
		nilai1 = 0
	}

	switch data.N2 {
	case "Tidak":
		nilai2 = 0
	case "Ya":
		nilai2 = 1
	default:
		nilai2 = 0
	}

	switch data.N3 {
	case "Tidak":
		nilai3 = 0
	case "Ya":
		nilai3 = 1
	default:
		nilai3 = 0
	}

	switch data.N4 {
	case "Tidak":
		nilai4 = 0
	case "Ya":
		nilai4 = 2
	default:
		nilai4 = 0
	}

	return kebidananDTO.ResPengkajianNutrisiAnak{
		Noreg:   data.Noreg,
		N1:      data.N1,
		N2:      data.N2,
		N3:      data.N3,
		N4:      data.N4,
		Nilai:   data.Nilai,
		NilaiN1: nilai1,
		NilaiN2: nilai2,
		NilaiN3: nilai3,
		NilaiN4: nilai4,
	}
}

func (a *KebidananMapperImpl) ToMapperResponseTandaVitalKebidananBangsal(data1 kebidanan.DVitalSignKebidananBangsal, data2 kebidanan.DPemFisikKebidananBangsalRepository) (value kebidananDTO.TandaVitalBangsalKebidananResponse) {
	var e = ""
	var v = ""
	var m = ""

	if data2.GcsE == "" || data2.GcsE == " " {
		e = " "
	} else {
		e = data2.GcsE
	}

	if data2.GcsM == "" || data2.GcsM == " " {
		m = " "
	} else {
		m = data2.GcsM
	}

	if data2.GcsV == "" || data2.GcsV == " " {
		v = " "
	} else {
		v = data2.GcsV
	}

	return kebidananDTO.TandaVitalBangsalKebidananResponse{
		GCSE:         e,
		GCSV:         v,
		GCSM:         m,
		Nadi:         data1.Nadi,
		Suhu:         data1.Suhu,
		TinggiBandan: data1.Tb,
		Td:           data1.Td,
		BeratBadan:   data1.Bb,
		Pernafasan:   data1.Pernafasan,
		Ddj:          data2.Ddj,
		Tfu:          data2.Tfu,
		Kesadaran:    data2.Kesadaran,
		Pupil:        data2.Pupil,
		Akral:        data2.Akral,
	}
}

func (a *KebidananMapperImpl) ToMapperResponseAsesmenKebidananBangsal(data kebidanan.AsesemenKebidananBangsal, alerg []rme.DAlergi, dahulu []rme.RiwayatPenyakitDahuluPerawat) (res kebidananDTO.ResponseAsesemenKebidanan) {

	var dataAlergi = []rme.DAlergi{}
	var dataDahulu = []rme.RiwayatPenyakitDahuluPerawat{}

	if len(alerg) == 0 || alerg == nil {
		dataAlergi = make([]rme.DAlergi, 0)
	} else {
		dataAlergi = alerg
	}

	if len(dahulu) == 0 || dahulu == nil {
		dataDahulu = make([]rme.RiwayatPenyakitDahuluPerawat, 0)
	} else {
		dataDahulu = dahulu
	}

	return kebidananDTO.ResponseAsesemenKebidanan{
		Alergi:  dataAlergi,
		Riwayat: dataDahulu,
		AsesemenKebidanan: kebidananDTO.AsesemenKebidanan{
			Noreg:                   data.Noreg,
			AseskepKel:              data.AseskepKel,
			AseskepRwytPenyakit:     data.AseskepRwytPnykt,
			AseskepRwytMenstruasi:   data.AseskepRwytMenstruasi,
			AseskepKelMenyertai:     data.AseskepKelMenyertai,
			AseskepSiklusHaid:       data.AseskepSiklusHaid,
			AseskepRwytPernikahan:   data.AseskepRwytPernikahan,
			RiwayatPenyakitDahulu:   data.AseskepRwytPnyktDahulu,
			RiwayatPenyakitKeluarga: data.AseskepRwtPnyktKeluarga,
		},
	}
}

func (a *KebidananMapperImpl) ToMapperResponseDiagnosaBanding(data kebidanan.DiagnosaBandingDokter) (res kebidananDTO.ResponseDiagnosaBanding) {
	return kebidananDTO.ResponseDiagnosaBanding{
		DiagnosaBanding: data.AsesmedDiagBanding,
		NoReg:           data.Noreg,
	}
}

func (a *KebidananMapperImpl) ToMapperReponsePengkajianKebidananBangsal(data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidananDTO.ResponsePengkajianPersistemKebidanan) {
	return kebidananDTO.ResponsePengkajianPersistemKebidanan{
		AseskepSistemEliminasiBak:      data.AseskepSistemEliminasiBak,
		AseskepSistemEliminasiBab:      data.AseskepSistemEliminasiBab,
		AseskepSistemIstirahat:         data.AseskepSistemIstirahat,
		AseskepSistemAktivitas:         data.AseskepSistemAktivitas,
		AseskepSistemMandi:             data.AseskepSistemMandi,
		AseskepSistemBerpakaian:        data.AseskepSistemBerpakaian,
		AseskepSistemMakan:             data.AseskepSistemMakan,
		AseskepSistemEliminasi:         data.AseskepSistemEliminasi,
		AseskepSistemMobilisasi:        data.AseskepSistemMobilisasi,
		AseskepSistemKardiovaskuler:    data.AseskepSistemKardiovaskuler,
		AseskepSistemRespiratori:       data.AseskepSistemRespiratori,
		AseskepSistemSecebral:          data.AseskepSistemSecebral,
		AseskepSistemPerfusiPerifer:    data.AseskepSistemPerfusiPerifer,
		AseskepSistemPencernaan:        data.AseskepSistemPencernaan,
		AseskepSistemIntegumen:         data.AseskepSistemIntegumen,
		AseskepSistemKenyamanan:        data.AseskepSistemKenyamanan,
		AseskepSistemProteksi:          data.AseskepSistemProteksi,
		AseskepSistemPapsSmer:          data.AseskepSistemPapsSmer,
		AseskepSistemHamil:             data.AseskepSistemHamil,
		AseskepSistemPendarahan:        data.AseskepSistemPendarahan,
		AseskepSistemHambatanBahasa:    data.AseskepSistemHambatanBahasa,
		AseskepSistemCaraBelajar:       data.AseskepSistemCaraBelajar,
		AseskepSistemBahasaSehari:      data.AseskepSistemBahasaSehari,
		AseskepSistemSpikologis:        data.AseskepSistemSpikologis,
		AseskepSistemHambatanSosial:    data.AseskepSistemHambatanSosial,
		AseskepSistemHambatanEkonomi:   data.AseskepSistemHambatanEkonomi,
		AseskepSistemHambatanSpiritual: data.AseskepSistemHambatanSpiritual,
		AseskepSistemResponseEmosi:     data.AseskepSistemResponseEmosi,
		AseskepSistemNilaiKepercayaan:  data.AseskepSistemNilaiKepercayaan,
		AseskepSistemPresepsiSakit:     data.AseskepSistemPresepsiSakit,
		AseskepSistemKhususKepercayaan: data.AseskepSistemKhususKepercayaan,
		AseskepSistemThermoregulasi:    data.AseskepSistemThermoregulasi,
	}
}

func (a *KebidananMapperImpl) ToMapperReponsePengkajianKeperawatanBangsal(data kebidanan.AsesemenPersistemKeperawatanBangsal) (res kebidananDTO.ResponsePengkajianPersistemKeperawatan) {
	return kebidananDTO.ResponsePengkajianPersistemKeperawatan{
		AseskepSistemEliminasiBak:         data.AseskepSistemEliminasiBak,
		AseskepSistemEliminasiBab:         data.AseskepSistemEliminasiBab,
		AseskepSistemIstirahat:            data.AseskepSistemIstirahat,
		AseskepSistemAktivitas:            data.AseskepSistemAktivitas,
		AseskepSistemMandi:                data.AseskepSistemMandi,
		AseskepSistemBerpakaian:           data.AseskepSistemBerpakaian,
		AseskepSistemMakan:                data.AseskepSistemMakan,
		AseskepSistemEliminasi:            data.AseskepSistemEliminasi,
		AseskepSistemThermoregulasiDingin: data.AseskepSistemThermoregulasiDingin,
		AseskepSistemMobilisasi:           data.AseskepSistemMobilisasi,
		AseskepSistemKardiovaskuler:       data.AseskepSistemKardiovaskuler,
		AseskepSistemRespiratori:          data.AseskepSistemRespiratori,
		AseskepSistemSecebral:             data.AseskepSistemSecebral,
		AseskepSistemPerfusiPerifer:       data.AseskepSistemPerfusiPerifer,
		AseskepSistemPencernaan:           data.AseskepSistemPencernaan,
		AseskepSistemIntegumen:            data.AseskepSistemIntegumen,
		AseskepSistemKenyamanan:           data.AseskepSistemKenyamanan,
		AseskepSistemProteksi:             data.AseskepSistemProteksi,
		AseskepSistemPapsSmer:             data.AseskepSistemPapsSmer,
		AseskepSistemHamil:                data.AseskepSistemHamil,
		AseskepSistemPendarahan:           data.AseskepSistemPendarahan,
		AseskepSistemHambatanBahasa:       data.AseskepSistemHambatanBahasa,
		AseskepSistemCaraBelajar:          data.AseskepSistemCaraBelajar,
		AseskepSistemBahasaSehari:         data.AseskepSistemBahasaSehari,
		AseskepSistemSpikologis:           data.AseskepSistemSpikologis,
		AseskepSistemHambatanSosial:       data.AseskepSistemHambatanSosial,
		AseskepSistemHambatanEkonomi:      data.AseskepSistemHambatanEkonomi,
		AseskepSistemHambatanSpiritual:    data.AseskepSistemHambatanSpiritual,
		AseskepSistemResponseEmosi:        data.AseskepSistemResponseEmosi,
		AseskepSistemNilaiKepercayaan:     data.AseskepSistemNilaiKepercayaan,
		AseskepSistemPresepsiSakit:        data.AseskepSistemPresepsiSakit,
		AseskepSistemKhususKepercayaan:    data.AseskepSistemKhususKepercayaan,
		AseskepSistemThermoregulasi:       data.AseskepSistemThermoregulasi,
		Penerjemah:                        data.AseskepSistemPenerjemah,
		AseskepGenitalia:                  data.AseskepSistemGenitalia,
		Usus:                              data.AseskepSistemPencernaanUsus,
		SakitKepala:                       data.AseskepSistemSakitKepala,
		KekuatanOtot:                      data.AseskepSistemKekuatanOtot,
		AnggotaGerak:                      data.AseskepSistemLemahAnggotaGerak,
		Bicara:                            data.AseskepSistemBicara,
		PerubahanStatusMental:             data.AseskepSistemStatusMental,
		RiwayatHipertensi:                 data.AseskepSistemRiwayatHipertensi,
		Akral:                             data.AseskepSistemAkral,
		Batuk:                             data.AseskepSistemBatuk,
		SuaraNapas:                        data.AseskepSistemSuaraNapas,
		Merokok:                           data.AseskepSistemMerokok,
		Nutrisi:                           data.AseskepSistemNutrisi,
	}
}

func (a *KebidananMapperImpl) ToMapperResponseKebidanan(data kebidanan.PengkajianKebidanan) (res kebidananDTO.ResponsePengkajianKebidanan) {
	return kebidananDTO.ResponsePengkajianKebidanan{
		AseskepKehamilanGravida:          data.AseskepKehamilanGravida,
		AseskepKehamilanAbortus:          data.AseskepKehamilanAbortus,
		AseskepKehamilanPara:             data.AseskepKehamilanPara,
		AseskepKehamilanLeopold1:         data.AseskepKehamilanLeopold1,
		AseskepKehamilanLeopold2:         data.AseskepKehamilanLeopold2,
		AseskepKehamilanLeopold3:         data.AseskepKehamilanLeopold3,
		AseskepKehamilanLeopold4:         data.AseskepKehamilanLeopold4,
		AseskepKehamilanHaid:             data.AseskepKehamilanHaid,
		AseskepKehamilanPemeriksaanDalam: data.AseskepKehamilanPemeriksaanDalam,
		AseskepKehamilanInspekuloP:       data.AseskepKehamilanInspekuloP,
		AseskepKehamilanInspekuloV:       data.AseskepKehamilanInspekuloV,
		AseskepKehamilanPalpasi:          data.AseskepKehamilanPalpasi,
		AseskepKehamilanInspeksi:         data.AseskepKehamilanInspeksi,
		AseskepKehamilanHodge:            data.AseskepKehamilanHodge,
		AseskepKehamilanNyeriTekan:       data.AseskepKehamilanNyeriTekan,
		AseskepKehamilanTbj:              data.AseskepKehamilanTbj,
		AseskepKehamilanTfu:              data.AseskepKehamilanTfu,
		AseskepKehamilanHamilTua:         data.AseskepKehamilanHamilTua,
		AseskepKehamilanHamilMuda:        data.AseskepKehamilanHamilMuda,
		AseskepKehamilanHpl:              data.AseskepKehamilanHpl,
		AseskepKehamilanUsiaKehamilan:    data.AseskepKehamilanUsiaKehamilan,
		AseskepKehamilanHaidTerakhir:     data.AseskepKehamilanHaidTerakhir,
	}
}

func (a *KebidananMapperImpl) ToResponseAsesmenKebidanan(data kebidanan.AsesmenKebidananModel) (res kebidananDTO.ResponseKebidanann) {
	return kebidananDTO.ResponseKebidanann{
		AseskepKehamilanGravida:          data.AseskepKehamilanGravida,
		AseskepKehamilanAbortus:          data.AseskepKehamilanAbortus,
		AseskepKehamilanPara:             data.AseskepKehamilanPara,
		AseskepKehamilanHaid:             data.AseskepKehamilanHaid,
		AseskepKehamilanHaidTerakhir:     data.AseskepHaidTerakhir,
		AseskepUsiaKehamilan:             data.AseskepUsiaKehamilan,
		AseskepPartusHpl:                 data.AseskepPartusHpl,
		AseskepKehamilanLeopold1:         data.AseskepKehamilanLeopold1,
		AseskepKehamilanLeopold2:         data.AseskepKehamilanLeopold2,
		AseskepKehamilanLeopold3:         data.AseskepKehamilanLeopold3,
		AseskepKehamilanLeopold4:         data.AseskepKehamilanLeopold4,
		AseskepKehamilanHodge:            data.AseskepKehamilanHodge,
		AseskepKehamilanInspeksi:         data.AseskepKehamilanInspeksi,
		AseskepKehamilanInspekuloV:       data.AseskepKehamilanInspekuloV,
		AseskepKehamilanInspekuloP:       data.AseskepKehamilanInspekuloP,
		AseskepKehamilanPemeriksaanDalam: data.AseskepKehamilanPemeriksaanDalam,
		AseskepKehamilanHamilMuda:        data.AseskepKehamilanHamilMuda,
		AseskepKehamilanHamilTua:         data.AseskepKehamilanHamilTua,
		AseskepKehamilanTbj:              data.AseskepKehamilanTbj,
		AseskepKehamilanTfu:              data.AseskepKehamilanTfu,
		AseskepKehamilanNyeriTekan:       data.AseskepKehamilanNyeriTekan,
		AseskepKehamilanPalpasi:          data.AseskepKehamilanPalpasi,
	}
}

func (a *KebidananMapperImpl) ToResponsePengkajianKebidanan(data kebidanan.ReportPengkajianKebidanan, vital kebidananDTO.TandaVitalBangsalKebidananResponse, riwayat []kebidanan.DRiwayatKehamilan, dirumah []kebidanan.DRiwayatPengobatanDirumah, fisik kebidanan.DPemFisikKebidanan, nutrisi kebidanan.DPengkajianNutrisi, funsional kebidanan.DPengkajianFungsional, diagnosa []kebidanan.DDiagnosaKebidanan, demFisik kebidanan.DPemFisikKebidanan, penyakitKeluarga []rme.DAlergi, karyawan user.Karyawan, karu user.Karyawan) (res kebidananDTO.ResponserPengkajianKebidanan) {
	var tglM = ""
	var tglK = ""
	var tglH = ""

	if len(data.TglMasuk) > 10 {
		tglM = data.TglMasuk[0:10]
	}

	if len(data.TglMasuk) > 10 {
		tglK = data.TglKeluar[0:10]
	}

	if len(data.TglMasuk) > 10 {
		tglH = data.AseskepKehamilanHaidTerakhir[0:10]
	}

	var dianosaOne = []kebidanan.DDiagnosaKebidanan{}
	var kelurga = []rme.DAlergi{}

	if len(diagnosa) == 0 || diagnosa == nil {
		dianosaOne = make([]kebidanan.DDiagnosaKebidanan, 0)
	} else {
		dianosaOne = diagnosa
	}

	if len(penyakitKeluarga) == 0 || penyakitKeluarga == nil {
		kelurga = make([]rme.DAlergi, 0)
	} else {
		kelurga = penyakitKeluarga
	}

	return kebidananDTO.ResponserPengkajianKebidanan{
		Karu:              karu,
		User:              karyawan,
		PenyakitKeluarga:  kelurga,
		DemFisik:          demFisik,
		Diagnosa:          dianosaOne,
		Fungsional:        funsional,
		Nutrisi:           nutrisi,
		FisikBidan:        fisik,
		PengobatanDirumah: dirumah,
		RiwayatKehamilan:  riwayat,

		Fisik: kebidananDTO.TandaVitalBangsalKebidananResponse{
			Td:           vital.Td,
			Pernafasan:   vital.Pernafasan,
			Suhu:         vital.Suhu,
			Nadi:         vital.Nadi,
			Ddj:          vital.Ddj,
			BeratBadan:   vital.BeratBadan,
			TinggiBandan: vital.TinggiBandan,
			GCSE:         vital.GCSE,
			GCSV:         vital.GCSV,
			GCSM:         vital.GCSM,
			Tfu:          vital.Tfu,
		},

		PengkajianKebidananResponse: kebidananDTO.PengkajianKebidananResponse{
			TglMasuk:                         tglM,
			TglKeluar:                        tglK,
			Pelayanan:                        data.Pelayanan,
			KdBagian:                         data.KdBagian,
			Noreg:                            data.Noreg,
			AseskepKel:                       data.AseskepKel,
			AseskepRwytPnykt:                 data.AseskepRwytPnykt,
			AseskepRwytPnyktDahulu:           data.AseskepRwytPnyktDahulu,
			AseskepRwytMenstruasi:            data.AseskepRwytMenstruasi,
			AseskepSiklusHaid:                data.AseskepSiklusHaid,
			AseskepRwytPernikahan:            data.AseskepRwytPernikahan,
			AseskepReaksiAlergi:              data.AseskepReaksiAlergi,
			AseskepKehamilanGravida:          data.AseskepKehamilanGravida,
			AseskepKehamilanPara:             data.AseskepKehamilanPara,
			AseskepKehamilanAbortus:          data.AseskepKehamilanAbortus,
			AseskepKehamilanHpht:             data.AseskepKehamilanHpht,
			AseskepKehamilanTtp:              data.AseskepKehamilanTtp,
			AseskepKehamilanLeopold1:         data.AseskepKehamilanLeopold1,
			AseskepKehamilanLeopold2:         data.AseskepKehamilanLeopold2,
			AseskepKehamilanLeopold3:         data.AseskepKehamilanLeopold3,
			AseskepKehamilanLeopold4:         data.AseskepKehamilanLeopold4,
			AseskepSistemPresepsiSakit:       data.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan:   data.AseskepSistemKhususKepercayaan,
			AseskepSistemResponseEmosi:       data.AseskepSistemResponseEmosi,
			AseskepSistemHambatanSosial:      data.AseskepSistemHambatanSosial,
			AseskepSistemSpikologis:          data.AseskepSistemSpikologis,
			AseskepSistemThermoregulasi:      data.AseskepSistemThermoregulasi,
			AseskepSistemBahasaSehari:        data.AseskepSistemBahasaSehari,
			AseskepSistemCaraBelajar:         data.AseskepSistemCaraBelajar,
			AseskepSistemHambatanBahasa:      data.AseskepSistemHambatanBahasa,
			AseskepSistemPendarahan:          data.AseskepSistemPendarahan,
			AseskepSistemHamil:               data.AseskepSistemHamil,
			AseskepSistemPapsSmer:            data.AseskepSistemPapsSmer,
			AseskepSistemProteksi:            data.AseskepSistemProteksi,
			AseskepSistemKenyamanan:          data.AseskepSistemKenyamanan,
			AseskepSistemIntegumen:           data.AseskepSistemIntegumen,
			AseskepSistemPencernaan:          data.AseskepSistemPencernaan,
			AseskepSistemPerfusiPerifer:      data.AseskepSistemPerfusiPerifer,
			AseskepSistemSecebral:            data.AseskepSistemSecebral,
			AseskepSistemRespiratori:         data.AseskepSistemRespiratori,
			AseskepSistemKardiovaskuler:      data.AseskepSistemKardiovaskuler,
			AseskepSistemMobilisasi:          data.AseskepSistemMobilisasi,
			AseskepSistemEliminasi:           data.AseskepSistemEliminasi,
			AseskepSistemMakan:               data.AseskepSistemMakan,
			AseskepSistemBerpakaian:          data.AseskepSistemBerpakaian,
			AseskepSistemMandi:               data.AseskepSistemMandi,
			AseskepKehamilanInspekuloP:       data.AseskepKehamilanInspekuloP,
			AseskepKehamilanInspekuloV:       data.AseskepKehamilanInspekuloV,
			AseskepKehamilanHaid:             data.AseskepKehamilanHaid,
			AseskepKehamilanPemeriksaanDalam: data.AseskepKehamilanPemeriksaanDalam,
			AseskepSistemNilaiKepercayaan:    data.AseskepSistemNilaiKepercayaan,
			AseskepPartusHpl:                 tglH,
			AseskepKehamilanTbj:              data.AseskepKehamilanTbj,
			AseskepKehamilanTfu:              data.AseskepKehamilanTfu,
			AseskepKehamilanHamilMuda:        data.AseskepKehamilanHamilMuda,
			AseskepKehamilanHamilTua:         data.AseskepKehamilanHamilTua,
			AseskepKehamilanUsiaKehamilan:    data.AseskepKehamilanUsiaKehamilan,
			AseskepKehamilanHaidTerakhir:     data.AseskepKehamilanHaidTerakhir,
			AseskepSistemEliminasiBak:        data.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:        data.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:           data.AseskepSistemIstirahat,
			AseskepSistemAktivitas:           data.AseskepSistemAktivitas,
			AseskepSistemHambatanEkonomi:     data.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual:   data.AseskepSistemHambatanSpiritual,
			AseskepKelMenyertai:              data.AseskepKelMenyertai,
			AseskepKehamilanHis:              data.AseskepKehamilanHis,
			AseskepKehamilanLendir:           data.AseskepKehamilanLendir,
		},
	}
}

func (a *KebidananMapperImpl) ToResponseTriaseIGDDokter(data kebidanan.TriaseIGDDokter, data1 kebidanan.AsesmenTriaseIGD) (res kebidananDTO.ResposeTriaseIGDDokter) {

	var tanggal = ""

	if len(data1.TglMasuk) > 9 {
		tanggal = data1.TglMasuk[0:10]
	} else {
		tanggal = data1.TglMasuk
	}

	return kebidananDTO.ResposeTriaseIGDDokter{
		Jam:                     data1.InsertDttm,
		TanggalMasuk:            tanggal,
		UserID:                  data1.InsertUserId,
		AseskepKehamilanDjj:     data1.AseskepKehamilanDjj,
		AseskepAlasanMasuk:      data1.AseskepAlasanMasuk,
		AseskepCaraMasuk:        data1.AseskepCaraMasuk,
		AseskepPenyebabCedera:   data1.AseskepPenyebabCedera,
		AseskepKehamilan:        data1.AseskepKehamilan,
		AseskepKehamilanGravida: data1.AseskepKehamilanGravida,
		AseskepKehamilanPara:    data1.AseskepKehamilanPara,
		AseskepKehamilanAbortus: data1.AseskepKehamilanAbortus,
		AseskepKehamilanHpht:    data1.AseskepKehamilanHpht,
		AseskepKehamilanTtp:     data1.AseskepKehamilanTtp,
		GangguanPerilaku:        data1.AseskepGangguanPerilaku,
		SkalaNyeri:              data.SkalaNyeri,
		SkalaNyeriP:             data.SkalaNyeriP,
		SkalaNyeriQ:             data.SkalaNyeriQ,
		SkalaNyeriR:             data.SkalaNyeriR,
		SkalaNyeriS:             data.SkalaNyeriS,
		SkalaNyeriT:             data.SkalaNyeriT,
		SkalaTriase:             data.SkalaTriaseIgd,
		FlaccWajah:              data.FlaccWajah,
		FlaccKaki:               data.FlaccKaki,
		FlaccAktifitas:          data.FlaccAktifitas,
		FlaccMenangis:           data.FlaccMenangis,
		FlaccBersuara:           data.FlaccBersuara,
		FlaccTotal:              data.FlaccTotal,
	}
}

func (a *KebidananMapperImpl) ToReseponseMapperTriaseIGDDokter(data kebidananDTO.ResposeTriaseIGDDokter, karyawan user.Karyawan, pemFisik rme.PemeriksaanFisikModel, vital soapDto.TandaVitalIGDResponse, rmeDTO rmeDTO.ResposeKeluhanUtamaIGD, pasien user.ProfilePasien) (res kebidananDTO.ReportTriaseIGDDokterResponse) {
	return kebidananDTO.ReportTriaseIGDDokterResponse{
		Triase:          data,
		Karyawan:        karyawan,
		PemFisik:        pemFisik,
		TandaVital:      vital,
		KeluhanUtamaIGD: rmeDTO,
		Pasien:          pasien,
	}

}

func (a *KebidananMapperImpl) ToResponseMapperPengkajianAwalDokter(pemFisik rme.PemeriksaanFisikModel, vital soap.DVitalSignIGDDokter, asesmen rme.AsesemenMedisIGD, rwtPenyakit []rme.KeluhanUtama, alergi []rme.DAlergi, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, diagnosa []soap.DiagnosaResponse) (res kebidananDTO.ReportPengkajianAwalDokterResponse) {

	var rwtPenyakits = []rme.KeluhanUtama{}
	var alergis = []rme.DAlergi{}
	var labors = []his.ResHasilLaborTableLama{}
	var radiologis = []his.RegHasilRadiologiTabelLama{}
	var diagnosas = []soap.DiagnosaResponse{}

	if len(rwtPenyakit) == 0 || rwtPenyakit == nil {
		rwtPenyakits = make([]rme.KeluhanUtama, 0)
	} else {
		rwtPenyakits = rwtPenyakit
	}

	if len(alergi) == 0 || alergi == nil {
		alergis = make([]rme.DAlergi, 0)
	} else {
		alergis = alergi
	}

	if len(labor) == 0 || labor == nil {
		labors = make([]his.ResHasilLaborTableLama, 0)
	} else {
		labors = labor
	}

	if len(radiologi) == 0 || radiologi == nil {
		radiologis = make([]his.RegHasilRadiologiTabelLama, 0)
	} else {
		radiologis = radiologi
	}

	if len(diagnosa) == 0 || diagnosa == nil {
		diagnosas = make([]soap.DiagnosaResponse, 0)
	} else {
		diagnosas = diagnosa
	}

	return kebidananDTO.ReportPengkajianAwalDokterResponse{
		PemFisik:    pemFisik,
		Vital:       vital,
		Asesmen:     asesmen,
		RwtPenyakit: rwtPenyakits,
		Alergi:      alergis,
		Labor:       labors,
		Radiologi:   radiologis,
		Diagnosa:    diagnosas,
	}

}

func (a *KebidananMapperImpl) ToResponseMapperPengkajianKeperawatan(pemFisik rmeDTO.ResponsePemeriksaanFisik, alergi []rme.DAlergi, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, diagnosa []soap.DiagnosaResponse, vital soap.DVitalSignIGDDokter, asemen rme.AsesmenPerawat, pengobatan []kebidanan.DRiwayatPengobatanDirumah, asuhan []rme.DasKepDiagnosaModelV2, fungsional dto.ResponseFungsional, riwayatPenyakitDahulu []rme.RiwayatPenyakitDahuluPerawat, nutrisi kebidanan.DPengkajianNutrisi) (res kebidananDTO.ReportPengkajianKeperawatanResponse) {

	var rwtPenyakits = []rme.RiwayatPenyakitDahuluPerawat{}
	var alergis = []rme.DAlergi{}
	var pengobatangs = []kebidanan.DRiwayatPengobatanDirumah{}
	var asuhans = []rme.DasKepDiagnosaModelV2{}

	if len(riwayatPenyakitDahulu) == 0 || riwayatPenyakitDahulu == nil {
		rwtPenyakits = make([]rme.RiwayatPenyakitDahuluPerawat, 0)
	} else {
		rwtPenyakits = riwayatPenyakitDahulu
	}

	if len(alergi) == 0 || alergi == nil {
		alergis = make([]rme.DAlergi, 0)
	} else {
		alergis = alergi
	}

	if len(pengobatan) == 0 || pengobatan == nil {
		pengobatangs = make([]kebidanan.DRiwayatPengobatanDirumah, 0)
	} else {
		pengobatangs = pengobatan
	}

	if len(asuhan) == 0 || asuhan == nil {
		asuhans = make([]rme.DasKepDiagnosaModelV2, 0)
	} else {
		asuhans = asuhan
	}

	nutrisis := a.ToMappingNutrisiMapper(nutrisi)

	return kebidananDTO.ReportPengkajianKeperawatanResponse{
		Fungsional:            fungsional,
		PemFisik:              pemFisik,
		Asesmen:               asemen,
		Pengobatan:            pengobatangs,
		Vital:                 vital,
		Asuhan:                asuhans,
		RiwayatPenyakitDahulu: rwtPenyakits,
		Alergi:                a.ToMappingRiwayatPenyakitKeluarga(alergis),
		Nutrisi:               nutrisis,
	}

}

func (a *KebidananMapperImpl) ToReponseFungsionalMapper(data kebidanan.DPengkajianFungsional) (res kebidananDTO.ResponseFungsional) {
	return kebidananDTO.ResponseFungsional{
		F1:    data.F1,
		F2:    data.F2,
		F3:    data.F3,
		F4:    data.F4,
		F5:    data.F5,
		F6:    data.F6,
		F7:    data.F7,
		F8:    data.F8,
		F9:    data.F9,
		F10:   data.F10,
		Nilai: data.Nilai,
	}
}

func (a *KebidananMapperImpl) ToMappingNutrisiMapper(data kebidanan.DPengkajianNutrisi) (res kebidananDTO.ResNutrisi) {
	return kebidananDTO.ResNutrisi{
		N1:    data.N1,
		N2:    data.N2,
		Nilai: data.Nilai,
	}
}

func (a *KebidananMapperImpl) ToMappingRiwayatPenyakitKeluarga(data []rme.DAlergi) (value []kebidananDTO.ResPenyakitKeluarga) {

	for _, V := range data {
		value = append(value, kebidananDTO.ResPenyakitKeluarga{
			Tanggal:  V.InsertDttm[0:10],
			Penyakit: V.Alergi,
		})
	}

	return value
}

func (a *KebidananMapperImpl) ToNyeriICUResponseMapper(data rme.PengkajianNyeri) (res dto.AsesmenNyeiICU) {

	return dto.AsesmenNyeiICU{
		AseskepNyeri:          data.AseskepNyeri,
		AseskepLokasiNyeri:    data.AseskepLokasiNyeri,
		AseskepFrekuensiNyeri: data.AseskepFrekuensiNyeri,
		AseskepNyeriMenjalar:  data.AseskepNyeriMenjalar,
		AseskepKualitasNyeri:  data.AseskepKualitasNyeri,
	}
}
func (a *KebidananMapperImpl) ToNyeriICUResponseMapper2(data kebidanan.PengkajianNyeriModel) (res dto.AsesmenNyeiICU) {

	return dto.AsesmenNyeiICU{
		AseskepNyeri:          data.AseskepNyeri,
		AseskepLokasiNyeri:    data.AseskepLokasiNyeri,
		AseskepFrekuensiNyeri: data.AseskepFrekuensiNyeri,
		AseskepNyeriMenjalar:  data.AseskepNyeriMenjalar,
		AseskepKualitasNyeri:  data.AseskepKualitasNyeri,
	}
}

func (a *KebidananMapperImpl) ToReponseAsesmenIntensiICUMapper(data kebidanan.AsesmenUlangPerawatanIntensive) (res dto.ResponseAsesmenIntensiICU) {
	return dto.ResponseAsesmenIntensiICU{
		KdDpjp:                       data.KdDpjp,
		Asesmen:                      data.Asesmen,
		AseskepRwtDari:               data.AseskepRwtDari,
		AseskepCaraMasuk:             data.AseskepCaraMasuk,
		AseskepAsalMasuk:             data.AseskepAsalMasuk,
		AseskepKel:                   data.AseskepKel,
		AseskepRwytPnykt:             data.AseskepRwytPnykt,
		AseskepReaksiYangMuncul:      data.AseskepReaksiYangMuncul,
		AseskepReaksiAlergi:          data.AseskepReaksiAlergi,
		AseskepTransfusiDarah:        data.AseskepTransfusiDarah,
		AseskepRwtMerokok:            data.AseskepRwtMerokok,
		AseskepRwtMinumanKeras:       data.AseskepRwtMinumanKeras,
		AseskepRwtAlkoholMempegaruhi: data.AseskepRwtAlkoholMempegaruhi,
		Nama:                         data.Nama,
	}
}

// MAPPER DATA
func (a *KebidananMapperImpl) ToResponsePengkajianAnak(perawat soap.PengkajianAwalKeperawatan) (res dto.ResponsePengkajianAnak) {
	return dto.ResponsePengkajianAnak{
		PengkajianAnak: perawat,
	}
}

func (a *KebidananMapperImpl) ToResponseICUMapper(asesmenUlang kebidanan.AsesmenUlangPerawatanIntensive, alergi []rme.DAlergi, fisik dto.ResponsePemeriksaanFisikICU, sistem dto.ResponsePengkajianPersistemICU, pengobatan []kebidanan.DRiwayatPengobatanDirumah, asuhanKeperawatan []rme.DasKepDiagnosaModelV2) (res dto.ReportAsesmenIntensiveICU) {
	var alergis = []rme.DAlergi{}
	var pengobatans = []kebidanan.DRiwayatPengobatanDirumah{}
	var asuhanKeperawatans = []rme.DasKepDiagnosaModelV2{}

	if len(alergis) == 0 {
		alergis = make([]rme.DAlergi, 0)
	} else {
		alergis = alergi
	}

	if len(pengobatan) == 0 {
		pengobatans = make([]kebidanan.DRiwayatPengobatanDirumah, 0)
	} else {
		pengobatans = pengobatan
	}

	if len(asuhanKeperawatan) == 0 {
		asuhanKeperawatans = make([]rme.DasKepDiagnosaModelV2, 0)
	} else {
		asuhanKeperawatans = asuhanKeperawatan
	}

	return kebidananDTO.ReportAsesmenIntensiveICU{
		AsesmenUlangIntensive: a.ToReponseAsesmenIntensiICUMapper(asesmenUlang),
		RiwayatAlergi:         alergis,
		PemeriksaanFisik:      fisik,
		PengkajianPersistem:   sistem,
		PengobatanDirumah:     pengobatans,
		AsuhanKeperawatan:     asuhanKeperawatans,
	}

}

func (a *KebidananMapperImpl) ToPemeriksaanFisikICUMapper(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisikICU) {
	return dto.ResponsePemeriksaanFisikICU{
		Kesadaran:   data.Kesadaran,
		Kepala:      data.Kepala,
		Rambut:      data.Rambut,
		Wajah:       data.Wajah,
		Mata:        data.Mata,
		Telinga:     data.Telinga,
		Hidung:      data.Hidung,
		Mulut:       data.Mulut,
		Gigi:        data.Gigi,
		Lidah:       data.Lidah,
		Tenggorokan: data.Tenggorokan,
		Leher:       data.Leher,
		Dada:        data.Dada,
		Respirasi:   data.Respirasi,
		Jantung:     data.Jantung,
		Integumen:   data.Integument,
		Ekstremitas: data.Ekstremitas,
		Genetalia:   data.Genetalia,
	}
}

func (a *KebidananMapperImpl) ToMappingResponsePengkajianPersistemICU(data kebidanan.AsesmenPengkajianPersistemICUBangsal) (value dto.ResponsePengkajianPersistemICU) {
	return dto.ResponsePengkajianPersistemICU{
		Airway:                         data.AseskepSistemAirway,
		Breathing:                      data.AsekepSistemBreathing,
		Circulation:                    data.AseskepSistemCirculation,
		Nutrisi:                        data.AseskepSistemNutrisi,
		Makan:                          data.AseskepSistemMakan,
		PadaBayi:                       data.AseskepSistemPadaBayi,
		Minum:                          data.AseskepSistemMinum,
		EliminasiBak:                   data.AseskepSistemEliminasiBak,
		EliminasiBab:                   data.AseskepSistemEliminasiBab,
		AktivitasAtauIstirahat:         data.AseskepSistemTidurAtauIstirahat,
		Aktivitas:                      data.AseskepSistemAktivitas,
		Berjalan:                       data.AseskepSistemBerjalan,
		PenggunaanAlatBantu:            data.AseskepSistemPenggunaanAlatBantu,
		PerfusiSerebral:                data.AseskepSistemPerfusiSerebral,
		Pupil:                          data.AseskepSistemPupil,
		RefleksCahaya:                  data.AseskepSistemRefleksCahaya,
		PerfusiRenal:                   data.AseskepSistemPerfusiRenal,
		PerfusiGastrointestinal:        data.AseskepSistemPerfusiGastrointestinal,
		Abdomen:                        data.AseskepSistemAbdomen,
		Thermoregulasi:                 data.AseskepSistemThermoregulasi,
		Kenyamanan:                     data.AseskepSistemKenyamanan,
		Kualitas:                       data.AsekepSistemKualitas,
		Pola:                           data.AsekepSistemPola,
		NyeriMempengaruhi:              data.AseskepSistemNyeriMempegaruhi,
		StatusMental:                   data.AseskepSistemStatusMental,
		Kejang:                         data.AseskepSistemKejang,
		PasangPengamanTempatTidur:      data.AseskepSistemPasangPengamananTempatTidur,
		BelMudaDijangkau:               data.AseskepSistemBelMudahDijangkau,
		Penglihatan:                    data.AseskepSistemPenglihatan,
		Pendengaran:                    data.AseskepSistemPendengaran,
		Hamil:                          data.AseskepSistemHamil,
		PemeriksaanCervixTerakhir:      data.AseskepSistemCervixTerakhir,
		Bicara:                         data.AseskepSistemBicara,
		BahasaIsyarat:                  data.AseskepBahasaIsyarat,
		PerluPenerjemah:                data.AseskepPerluPenerjemah,
		CaraBelajarDisukai:             data.AseskepCaraBelajarYangDisukai,
		HambatanBelajar:                data.AseskepHambatanBelajar,
		TingkatPendidikan:              data.AseskepTingkatPendidikan,
		Pekerjaan:                      data.AseskepSistemPekerjaan,
		NilaiKepercayaan:               data.AseskepSistemNilaiKepercayaan,
		ResponseEmosi:                  data.AseskepReponseEmosi,
		BahasaSehariHari:               data.AseskepBahasaSehariHari,
		TingkatBersama:                 data.AseskepSistemTinggalBersama,
		KondisiLingkunganDirumah:       data.AseskepKondisiLingkungan,
		PenggunaanAlatKontrasepsi:      data.AseskepSistemPenggunaanAlatKontrasepsi,
		KunjunganPemimpin:              data.AseskepSistemPemimpinAgama,
		MenjalankanIbadah:              data.AseskepSistemMenjalankanIbadah,
		PotensialKebutuhanPembelajaran: data.AseskepKebutuhanPembelajaran,
		PresepsiTerhadapSakit:          data.AseskepSistemPresepsiTerhadapSakit,
		AseskepSistemMandi:             data.AseskepSistemMandi,
		AseskepSistemMakan:             data.AseskepSistemMakan,
		AseskepSistemEliminasi:         data.AseskepSistemEliminasi,
		AseskepSistemMobilisasi:        data.AseskepSistemMobilisasi,
		AseskepSistemBerpakaian:        data.AseskepSistemBerpakaian,
		AseskepMslhDenganNutrisi:       data.AseskepMslhDenganNutrisi,
	}
}

func (a *KebidananMapperImpl) ToMappingResponsePengkajianKebidananBatuRajaV2() (res dto.ResponsePengkajianKebidananBatuRajaV2) {
	return kebidananDTO.ResponsePengkajianKebidananBatuRajaV2{
		User: res.User,
		Karu: res.Karu,
	}
}
