package respository

import (
	"errors"
	"fmt"
	"hms_api/modules/kebidanan"
	"hms_api/modules/kebidanan/dto"
	"hms_api/modules/kebidanan/entity"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	soapRepo "hms_api/modules/soap/entity"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type kebidananRepository struct {
	DB       *gorm.DB
	Mapper   entity.KebidananMapper
	SoapRepo soapRepo.SoapRepository
	Logging  *logrus.Logger
}

func NewKebidananRepository(db *gorm.DB, logging *logrus.Logger, mapper entity.KebidananMapper) entity.KebidananRepository {
	return &kebidananRepository{
		Mapper:  mapper,
		DB:      db,
		Logging: logging,
	}
}

func (kb *kebidananRepository) InsertVitalSignKebidananRepository(userID string, bagian string, req dto.RequestVitalSignKebidanan) (res rme.DVitalSign, err error) {
	times := time.Now()

	result := kb.DB.Create(&rme.DVitalSign{
		UpdDttm:      times.Format("2006-01-02 15:04:05"),
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertUserId: userID,
		InsertDevice: req.DeviceID,
		Pelayanan:    req.Pelayanan,
		Kategori:     "Bidan",
		KetPerson:    req.Person,
		Noreg:        req.NoReg,
		KdBagian:     bagian,
		Td:           req.TekananDarah,
		Suhu:         req.Suhu,
		Pernafasan:   req.Pernapasan,
		Nadi:         req.Nadi,
		Tb:           req.TinggiBadan,
		Bb:           req.BeratBadan,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (kb *kebidananRepository) InsertGCSDataDemfisikRepository(userID string, bagian string, req dto.RequestVitalSignKebidanan) (res rme.DPemfisikBidanGCS, err error) {
	times := time.Now()

	result := kb.DB.Create(&rme.DPemfisikBidanGCS{
		UpdateDttm:   times.Format("2006-01-02 15:04:05"),
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertUserId: userID,
		InsertDevice: req.DeviceID,
		Pelayanan:    req.Pelayanan,
		Kategori:     "Bidan",
		KetPerson:    req.Person,
		Noreg:        req.NoReg,
		KdBagian:     bagian,
		GcsE:         req.GcsE,
		GcsV:         req.GcsV,
		GcsM:         req.GcsM,
		Ddj:          req.DDJ,
		Tfu:          req.Tfu,
		Kesadaran:    req.Kesadaran,
		Pupil:        req.Pupil,
		Akral:        req.Akral,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (kb *kebidananRepository) UpdateVitalSignKebidananRepository(noReg string, req dto.RequestVitalSignKebidanan) (res rme.DVitalSign, err error) {
	result := kb.DB.Model(&rme.DVitalSign{}).Where("noreg = ? AND kategori=? AND pelayanan=?", noReg, "Bidan", req.Pelayanan).Updates(&rme.DVitalSign{
		Td:         req.TekananDarah,
		Suhu:       req.Suhu,
		Pernafasan: req.Pernapasan,
		Nadi:       req.Nadi,
		Tb:         req.TinggiBadan,
		Bb:         req.BeratBadan,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) UpdateDemfisikGCSMRepository(noReg string, req dto.RequestVitalSignKebidanan) (res rme.DPemfisikBidanGCS, err error) {
	result := kb.DB.Model(&rme.DPemfisikBidanGCS{}).Where("noreg = ? AND kategori=? AND pelayanan=?", noReg, "Bidan", req.Pelayanan).Updates(&rme.DPemfisikBidanGCS{
		GcsE:      req.GcsE,
		GcsV:      req.GcsV,
		GcsM:      req.GcsM,
		Tfu:       req.Tfu,
		Ddj:       req.DDJ,
		Pupil:     req.Pupil,
		Kesadaran: req.Kesadaran,
		Akral:     req.Akral,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetVitalSignKebidananRepository(noReg string, pelayanan string) (res rme.DVitalSign, err error) {
	errs := kb.DB.Where("noreg=? AND pelayanan=? AND kategori=?", noReg, pelayanan, "Bidan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPengkajianNyeriNipsRepository(noReg string, kdBagian string) (res rme.PengkajianNyeri, err error) {
	query := `SELECT noreg, insert_dttm, keterangan_person, insert_user_id, insert_pc, pelayanan, kd_bagian, kd_dpjp,
	tgl_masuk, tgl_keluar, aseskep_nyeri, aseskep_lokasi_nyeri, aseskep_frekuensi_nyeri, aseskep_nyeri_menjalar, 
	aseskep_kualitas_nyeri FROM vicore_rme.dcppt_soap_pasien WHERE noreg=? AND kd_bagian=?  AND keterangan_person=?`

	result := kb.DB.Raw(query, noReg, kdBagian, "Anak").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (kb *kebidananRepository) OnGetPengkajianNutrisiNipsRepository(noReg string) (res kebidanan.PengkajianNyeriNIPS, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) InsertRiwayatKehamilanRepository(userID string, req dto.ReqRiwayatKehamilan) (res kebidanan.DRiwayatKehamilan, message string, err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DRiwayatKehamilan{
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
		KdRiwayat:        "RWT_" + uuid.NewString(),
		InsertPc:         req.DeviceID,
		InsertUserId:     userID,
		TahunPersalinan:  req.Tahun,
		TempatPersalinan: req.Tempat,
		Noreg:            req.NoReg,
		UmurKehamilan:    req.Umur,
		JenisPersalinan:  req.JenisPersalinan,
		Penolong:         req.Penolong,
		Penyulit:         req.Penyulit,
		Nifas:            req.Nifas,
		Jk:               req.JKelamin,
		Bb:               req.BeratBadan,
		KeadaanSekarang:  req.KeadaanSekarang,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, message, errors.New(message)
	}

	return res, "Data berhasil disimpan", nil
}

func (kb *kebidananRepository) OnSaveAsesmenNyeriIcuRepository(userID string, data kebidanan.PengkajianNyeriModel) (res kebidanan.PengkajianNyeriModel, message string, err error) {

	result := kb.DB.Create(&data)

	if result.Error != nil {
		messages := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, messages, errors.New(messages)
	}

	return res, "Data berhasil disimpan", nil
}

func (kb *kebidananRepository) OnSaveAssesmenNIPSRepository(data kebidanan.PengkajianNyeriNIPS) (res kebidanan.PengkajianNyeriNIPS, message string, err error) {

	result := kb.DB.Create(&data)

	if result.Error != nil {
		messages := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, messages, errors.New(messages)
	}

	return res, message, nil
}

func (kb *kebidananRepository) OnUpdateAsessmenNIPSRepository(noReg string, kdBagian string, data kebidanan.PengkajianNyeriNIPS) (res kebidanan.PengkajianNyeriNIPS, err error) {

	result := kb.DB.Model(&kebidanan.PengkajianNyeriNIPS{}).Where("noreg=? AND kd_bagian=?", noReg, kdBagian).Updates(&data).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil

}

func (kb *kebidananRepository) OnGetAsesmenNIPSRepository(noReg string, kdBagian string) (res kebidanan.PengkajianNyeriNIPS, err error) {

	result := kb.DB.Model(&kebidanan.PengkajianNyeriNIPS{}).Where("noreg=? AND kd_bagian=?", noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnUpdateAsesmenNyeriICURepository(userID string, bagian string, noReg string, pelayanan string, data kebidanan.PengkajianNyeriModel) (res kebidanan.PengkajianNyeriModel, message string, err error) {

	result := kb.DB.Model(&kebidanan.PengkajianNyeriModel{}).Where("noreg=? AND kd_bagian=? AND pelayanan=? AND keterangan_person=?", noReg, bagian, pelayanan, "Anak").Updates(&data).Scan(&res)

	if result.Error != nil {
		return res, "Data gagal diupdate", result.Error
	}

	return res, "Data berhasil diupdate", nil
}

func (kb *kebidananRepository) GetRiwayatKehamilanRepository(noReg string) (res []kebidanan.DRiwayatKehamilan, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) InsertRiwayatPengobatanDirumahRepository(req dto.ReqRiwayatPengobatanDirumah, kdBagian string) (message string, res kebidanan.DRiwayatPengobatanDirumah, err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DRiwayatPengobatanDirumah{
		InsertDttm:             times.Format("2006-01-02 15:04:05"),
		InsertPc:               req.DeviceID,
		InsertUserId:           req.UserID,
		Noreg:                  req.NoReg,
		KdRiwayat:              "RWT_" + uuid.NewString(),
		NamaObat:               req.NamaObat,
		Dosis:                  req.Dosis,
		CaraPemberian:          req.CaraPemberian,
		Frekuensi:              req.Frekuensi,
		WaktuTerakhirPemberian: req.WaktuTerakhirPemberian,
		KdBagian:               kdBagian,
		Usia:                   req.USIA,
	}).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return message, res, errors.New(message)
	}

	return "Data berhasil disimpan", res, nil
}

func (kb *kebidananRepository) OnUpdateRiwayatPengobatanDirumahRepository(req dto.ReqUpdateRiwayatPengobatanDirumah) (res kebidanan.DRiwayatPengobatanDirumah, err error) {
	result := kb.DB.Model(&kebidanan.DRiwayatPengobatanDirumah{}).Where("kd_riwayat= ?", req.KdRiwayat).Updates(&kebidanan.DRiwayatPengobatanDirumah{
		NamaObat:               req.NamaObat,
		Dosis:                  req.Dosis,
		CaraPemberian:          req.CaraPemberian,
		Frekuensi:              req.Frekuensi,
		WaktuTerakhirPemberian: req.WaktuTerakhirPemberian,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) DeleteRiwayatPengobatanDirumahRepository(uid string) (message string, err error) {
	errs := kb.DB.Where("kd_riwayat", uid).Delete(&kebidanan.DRiwayatPengobatanDirumah{})

	if errs != nil {
		return "Data gagal dihapus", errs.Error
	}

	return "Data berhasil dihapus", nil
}

func (kb *kebidananRepository) OnGetRiwayatPengobatanDirumahRepository(noReg string) (res []kebidanan.DRiwayatPengobatanDirumah, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnDeleteRiwayatPengobatanDirumahRepository(req dto.ReqPengobatanDirumah) (err error) {
	errs := kb.DB.Where("kd_riwayat=?", req.KdRiwayat).Delete(&kebidanan.DRiwayatPengobatanDirumah{})

	if errs != nil {
		return errs.Error
	}

	return nil
}
func (kb *kebidananRepository) OnDeleteRiwayatKehamilanRepository(id int) (err error) {
	errs := kb.DB.Where("id=?", id).Delete(&kebidanan.DRiwayatKehamilan{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

func (kb *kebidananRepository) OnGetRiwayatKehamilanRepository(noReg string) (res []kebidanan.DRiwayatKehamilan, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetResikoJatuhKebidananRepository(noReg string, person string) (res kebidanan.DRisikoJatuhKebidanan, err error) {
	errs := kb.DB.Where("noreg=? AND kategori=?", noReg, strings.ToUpper(person)).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPengkajianFungsionalRepository(noReg string) (res kebidanan.DPengkajianFungsional, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPengkajianNutrisiRepository(noReg string) (res kebidanan.DPengkajianNutrisi, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPengkajianNutrisiAnakRepository(noReg string) (res kebidanan.DPengkajianNutrisiAnak, err error) {
	errs := kb.DB.Where("noreg=?", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnSaveResikoJatuhKebidananRepository(userID string, bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (res kebidanan.DRisikoJatuhKebidanan, err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DRisikoJatuhKebidanan{
		InsertDttm:      times.Format("2006-01-02 15:04:05"),
		InsertUserId:    userID,
		UpdDttm:         times.Format("2006-01-02 15:04:05"),
		InsertPc:        req.InsertPc,
		KetPerson:       person,
		Pelayanan:       req.Pelayanan,
		KdBagian:        bagian,
		Noreg:           req.Noreg,
		Kategori:        strings.ToUpper(person),
		RJatuh:          req.RJatuh,
		Diagnosis:       req.Diagnosis,
		AlatBantu1:      req.AlatBantu1,
		AlatBantu2:      req.AlatBantu2,
		AlatBantu3:      req.AlatBantu3,
		TerpasangInfuse: req.TerpasangInfuse,
		Total:           req.Total,
	}).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (kb *kebidananRepository) OnUpdateResikoJatuhKebidananRepository(bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (res kebidanan.DRisikoJatuhKebidanan, err error) {
	result := kb.DB.Model(&kebidanan.DRisikoJatuhKebidanan{}).Where("noreg = ? AND kategori=? AND pelayanan=?", req.Noreg, strings.ToUpper(person), req.Pelayanan).Updates(&kebidanan.DRisikoJatuhKebidanan{
		RJatuh:          req.RJatuh,
		Diagnosis:       req.Diagnosis,
		AlatBantu1:      req.AlatBantu1,
		AlatBantu2:      req.AlatBantu2,
		AlatBantu3:      req.AlatBantu3,
		TerpasangInfuse: req.TerpasangInfuse,
		Total:           req.Total,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPemeriksaanFisikKebidananRepository(noReg string, pelayanan string, person string) (res kebidanan.DPemFisikKebidanan, err error) {
	errs := kb.DB.Where("noreg=? AND kategori=? AND pelayanan=? AND ket_person=?", noReg, "Bidan", pelayanan, person).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPemeriksaanFisikKebidananReportRepository(noReg string, person string) (res kebidanan.DPemFisikKebidanan, err error) {
	errs := kb.DB.Where("noreg=? AND kategori=? AND ket_person=?", noReg, "Bidan", person).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) GetPemeriksaanFisikPengkjianKebidanan(noReg string, pelayanan string) (res kebidanan.PemeriksaanFisikKebidanan, err error) {
	errs := kb.DB.Where("noreg=?  AND pelayanan=?", noReg, pelayanan).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetAllKebidananRepository() (res []kebidanan.KDiagnosaKebidanan, err error) {
	var results []kebidanan.KDiagnosaKebidanan

	errs := kb.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (kb *kebidananRepository) OnSaveDiagnosaRepository(userID string, bagian string, device string, noReg string, kodeDiagnosa string, namaDiagnosa string) (err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DDiagnosaKebidanan{
		InsertUserId: userID,
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		UpdDttm:      times.Format("2006-01-02 15:04:05"),
		InsertPc:     device,
		KdBagian:     bagian,
		Noreg:        noReg,
		KodeDiagnosa: kodeDiagnosa,
		NamaDiagnosa: namaDiagnosa,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return errors.New(message)
	}

	return nil
}

func (kb *kebidananRepository) OnGetDiagnosaKebidananRepository(noReg string) (res []kebidanan.DDiagnosaKebidanan, err error) {
	errs := kb.DB.Where("noreg=? ", noReg).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetDImplementasiKeperawatanRepository(noDaskep string) (res []kebidanan.DImplementasiKeperawatan, err error) {
	ersr := kb.DB.Where("no_daskep=?", noDaskep).Preload("Karyawan").Order("dimplementasi_keperawatan.id asc").Find(&res).Error
	if ersr != nil {
		return res, ersr
	}

	return res, nil
}

func (kb *kebidananRepository) OnDeleteDataDiagnosaKebidananRepository(noDiagnosa int) (message string, err error) {
	errs := kb.DB.Where("no_diagnosa", noDiagnosa).Delete(&kebidanan.DDiagnosaKebidanan{})

	if errs != nil {
		return "Data gagal dihapus", errs.Error
	}

	return "Data berhasil dihapus", nil
}

func (kb *kebidananRepository) OnSaveAllKebidananReporitory(uuID string, namaBagian string) (err error) {
	result := kb.DB.Create(&kebidanan.KDiagnosaKebidanan{
		KodeDiagnosa: uuID,
		NamaDiagnosa: namaBagian,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return errors.New(message)
	}
	return nil
}

func (kb *kebidananRepository) OnSavePemeriksaanFisikKebidananRepository(userID string, bagian string, req dto.ReqFisikKebidanan) (res kebidanan.DPemFisikKebidanan, err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DPemFisikKebidanan{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		InsertUserId:      userID,
		InsertDevice:      req.DeviceID,
		UpdateDttm:        times.Format("2006-01-02 15:04:05"),
		Kategori:          "Bidan",
		KetPerson:         req.KetPerson,
		KdBagian:          bagian,
		Noreg:             req.Noreg,
		Kepala:            req.Kepala,
		Mata:              req.Mata,
		Telinga:           req.Telinga,
		Mulut:             req.Mulut,
		Gigi:              req.Gigi,
		Hidung:            req.Hidung,
		Tenggorokan:       req.Tenggorokan,
		Payudara:          req.Payudara,
		LeherDanBahu:      req.LeherDanBahu,
		Pelayanan:         req.Pelayanan,
		MucosaMulut:       req.MucosaMulut,
		Abdomen:           req.Abdomen,
		NutrisiDanHidrasi: req.NutrisiDanHidrasi,
	}).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (kb *kebidananRepository) OnUpdatePemeriksaanFisikKebidananRepository(userID string, bagian string, req dto.ReqFisikKebidanan) (res kebidanan.DPemFisikKebidanan, err error) {
	result := kb.DB.Model(&kebidanan.DPemFisikKebidanan{}).Where("noreg = ? AND kategori=? AND pelayanan=?", req.Noreg, "Bidan", req.Pelayanan).Updates(&kebidanan.DPemFisikKebidanan{
		Mata:              req.Mata,
		Gigi:              req.Gigi,
		Kepala:            req.Kepala,
		Telinga:           req.Telinga,
		Payudara:          req.Payudara,
		LeherDanBahu:      req.LeherDanBahu,
		Mulut:             req.Mulut,
		Tenggorokan:       req.Tenggorokan,
		Hidung:            req.Hidung,
		MucosaMulut:       req.MucosaMulut,
		Abdomen:           req.Abdomen,
		NutrisiDanHidrasi: req.NutrisiDanHidrasi,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnSaveRiwayatKehamilanKebidananRepository(userID string, req dto.ReqRiwayatKehamilan) (res kebidanan.DRiwayatKehamilan, err error) {
	times := time.Now()

	result := kb.DB.Create(&kebidanan.DRiwayatKehamilan{
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
		InsertUserId:     userID,
		TahunPersalinan:  req.Tahun,
		TempatPersalinan: req.Tempat,
		Noreg:            req.NoReg,
		UmurKehamilan:    req.Umur,
		JenisPersalinan:  req.JenisPersalinan,
		Penolong:         req.Penolong,
		Penyulit:         req.Penyulit,
		Nifas:            req.Nifas,
		Jk:               req.JKelamin,
		Bb:               req.BeratBadan,
		KeadaanSekarang:  req.KeadaanSekarang,
	}).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (kb *kebidananRepository) OnSavePengkajianFungsionalKebidahanRepository(userID string, bagian string, req dto.RegDPengkajianFungsional) (res dto.ResDPengkajianFungsional, err error) {
	times := time.Now()

	var fungsional = kebidanan.DPengkajianFungsional{}

	result := kb.DB.Create(&kebidanan.DPengkajianFungsional{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertUserId: userID,
		UpdDttm:      times.Format("2006-01-02 15:04:05"),
		InsertPc:     req.DeviceID,
		KdBagian:     bagian,
		Noreg:        req.NoReg,
		F1:           req.F1,
		F2:           req.F2,
		F3:           req.F3,
		F4:           req.F4,
		F5:           req.F5,
		F6:           req.F6,
		F7:           req.F7,
		F8:           req.F8,
		F9:           req.F9,
		F10:          req.F10,
		Nilai:        req.Nilai,
	}).Scan(&fungsional)

	mapper := kb.Mapper.ToFungsionalKebidananMapper(fungsional)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return mapper, errors.New(message)
	}

	return mapper, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianFungsionalKebidananRepository(noReg string, req dto.RegDPengkajianFungsional) (res dto.ResDPengkajianFungsional, err error) {

	var fungsional = kebidanan.DPengkajianFungsional{}

	result := kb.DB.Model(&kebidanan.DPengkajianFungsional{}).Where("noreg = ?", noReg).Updates(&kebidanan.DPengkajianFungsional{
		F1:    req.F1,
		F3:    req.F3,
		F2:    req.F2,
		F4:    req.F4,
		F5:    req.F5,
		F6:    req.F6,
		F7:    req.F7,
		F9:    req.F9,
		F8:    req.F8,
		F10:   req.F10,
		Nilai: req.Nilai,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	mapper := kb.Mapper.ToFungsionalKebidananMapper(fungsional)

	return mapper, nil
}

func (kb *kebidananRepository) OnSavePengkajianNutrisiRepository(userID string, bagian string, req dto.ReqPengkajianNutrisi) (res dto.ResPengkajianNutrisi, err error) {
	times := time.Now()

	var fungsional = kebidanan.DPengkajianNutrisi{}

	result := kb.DB.Create(&kebidanan.DPengkajianNutrisi{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertUserId: userID,
		UpdDttm:      times.Format("2006-01-02 15:04:05"),
		InsertPc:     req.DeviceID,
		KdBagian:     bagian,
		Noreg:        req.Noreg,
		N1:           req.N1,
		N2:           req.N2,
		Nilai:        req.Nilai,
	}).Scan(&fungsional)

	mapper := kb.Mapper.ToPengkajianNutrisiMapper(fungsional)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return mapper, errors.New(message)
	}

	return mapper, nil
}

func (kb *kebidananRepository) OnSavePengkajianNutrisiAnakRepository(userID string, bagian string, req dto.ReqPengkajianNutrisiAnak) (res dto.ResPengkajianNutrisiAnak, err error) {
	times := time.Now()

	var fungsional = kebidanan.DPengkajianNutrisiAnak{}

	result := kb.DB.Create(&kebidanan.DPengkajianNutrisiAnak{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertUserId: userID,
		UpdDttm:      times.Format("2006-01-02 15:04:05"),
		InsertPc:     req.DeviceID,
		KdBagian:     bagian,
		Noreg:        req.Noreg,
		N1:           req.N1,
		N2:           req.N2,
		N3:           req.N3,
		N4:           req.N4,
		Nilai:        req.Nilai,
	}).Scan(&fungsional)

	mapper := kb.Mapper.ToPengkajianNutrisiAnakMapper(fungsional)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return mapper, errors.New(message)
	}

	return mapper, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianNutrisi(noReg string, req dto.ReqPengkajianNutrisi) (res dto.ResPengkajianNutrisi, err error) {

	var fungsional = kebidanan.DPengkajianNutrisi{}

	result := kb.DB.Model(&kebidanan.DPengkajianNutrisi{}).Where("noreg = ?", noReg).Updates(&kebidanan.DPengkajianNutrisi{
		N1:    req.N1,
		N2:    req.N2,
		Nilai: req.Nilai,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	mapper := kb.Mapper.ToPengkajianNutrisiMapper(fungsional)

	return mapper, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianNutrisiAnakRepository(noReg string, req dto.ReqPengkajianNutrisiAnak) (res dto.ResPengkajianNutrisiAnak, err error) {

	var anak = kebidanan.DPengkajianNutrisiAnak{}

	result := kb.DB.Model(&anak).Where("noreg=?", noReg).Updates(&kebidanan.DPengkajianNutrisiAnak{
		N1:    req.N1,
		N2:    req.N2,
		N3:    req.N3,
		N4:    req.N4,
		Nilai: req.Nilai,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	mapper := kb.Mapper.ToPengkajianNutrisiAnakMapper(anak)

	return mapper, nil
}

func (sr *kebidananRepository) GetVitalSignKebidananBangsalRepository(userID string, kdBagian string, person string, noreg string) (res kebidanan.DVitalSignKebidananBangsal, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, td, tb, bb, nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetVitalSignKebidananBangsalReportRepository(kdBagian string, person string, noreg string) (res kebidanan.DVitalSignKebidananBangsal, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, td, tb, bb, nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where  kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPemFisikKebidananRepository(userID string, kdBagian string, person string, noreg string) (res kebidanan.DPemFisikKebidananBangsalRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, ddj, pupil, kesadaran, akral, tfu FROM vicore_rme.dpem_fisik where insert_user_id=? AND kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPemFisikKebidananReportRepository(kdBagian string, person string, noreg string) (res kebidanan.DPemFisikKebidananBangsalRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, ddj, tfu FROM vicore_rme.dpem_fisik where  kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// ASESMEN KEBIDANAN =================================// ASESMEN KEBIDANAN
func (sr *kebidananRepository) GetAsesmenKebidananRepository(kdBagian string, person string, noReg string) (res kebidanan.AsesemenKebidananBangsal, err error) {
	query := `SELECT insert_dttm, keterangan_person,  insert_user_id, insert_pc, tgl_masuk, tgl_keluar, pelayanan, kd_bagian, noreg, kd_dpjp, aseskep_kel, aseskep_rwyt_pnykt, aseskep_rwyt_menstruasi, aseskep_kel_menyertai, aseskep_siklus_haid, aseskep_rwyt_pernikahan, aseskep_rwt_pnykt_keluarga, aseskep_rwyt_pnykt_dahulu FROM vicore_rme.dcppt_soap_pasien where  kd_bagian=? AND keterangan_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// ASESMEN KEBIDANAN =================================// ASESMEN KEBIDANAN PERSISTEM
func (sr *kebidananRepository) GetPengkajianPersistemKebidananRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	query := `SELECT insert_dttm, keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_thermoregulasi, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetPengkajianPersistemMariaRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {

	query := `SELECT insert_dttm, keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_thermoregulasi, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, "Bidan", noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPengkajianPersistemDewasaRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	query := `SELECT insert_dttm,	keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_thermoregulasi, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, "Dewasa", noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPengkajianPersistemAnakRepository(kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	query := `SELECT insert_dttm,	keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_thermoregulasi, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where  kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, "Anak", noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetPengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error) {
	errs := sr.DB.Where("noreg=? AND  kd_bagian=? AND keterangan_person=?", noReg, kdBagian, person).Find(&res)
	if errs != nil {
		return res, errs.Error
	}
	return res, nil
}

func (sr *kebidananRepository) OnGetReportPengkajianPersistemICURepository(kdBagian string, noReg string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error) {
	errs := sr.DB.Where("noreg=?  AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, "Perawat").Find(&res)
	if errs != nil {
		return res, errs.Error
	}
	return res, nil
}

func (sr *kebidananRepository) OnGetAsesmenUlangPerawatIntensive(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesmenUlangPerawatanIntensive, err error) {

	errs := sr.DB.Where("noreg=? AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, person).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetAsesmenUlangPerawatanIntensiveICURepository(noReg string) (res kebidanan.AsesmenUlangPerawatanIntensive, err error) {
	query := `SELECT insert_dttm, keterangan_person, tgl_masuk,  insert_pc, pelayanan, kd_bagian, noreg, kd_dpjp, asesmen,
	aseskep_rwt_dari, aseskep_cara_masuk, aseskep_asal_masuk, aseskep_kel, aseskep_rwyt_pnykt, aseskep_reaksi_yang_muncul, aseskep_reaksi_alergi, 
	aseskep_transfusi_darah, aseskep_rwt_alkohol_mempegaruhi, aseskep_rwt_minuman_keras, pg.nama AS nama
	FROM vicore_rme.dcppt_soap_pasien  AS dp INNER JOIN mutiara.pengajar AS pg ON  pg.id = dp.insert_user_id where noreg=? AND keterangan_person=? AND kd_bagian=?`

	result := sr.DB.Raw(query, noReg, "Perawat", "ICU").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnSavePengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error) {
	times := time.Now()

	tanggal, _ := sr.SoapRepo.GetJamMasukPasienRepository(req.Noreg)

	result := sr.DB.Create(&kebidanan.AsesmenPengkajianPersistemICUBangsal{
		InsertDttm:                               times.Format("2006-01-02 15:04:05"),
		InsertUserId:                             userID,
		UpdDttm:                                  times.Format("2006-01-02 15:04:05"),
		KeteranganPerson:                         req.Person,
		InsertPc:                                 req.InsertPC,
		TglMasuk:                                 tanggal.Tanggal[0:10],
		Pelayanan:                                req.Pelayanan,
		Noreg:                                    req.Noreg,
		KdBagian:                                 kdBagian,
		AseskepSistemMakan:                       req.Makan,
		KdDpjp:                                   req.KodeDokter,
		AseskepSistemAirway:                      req.Airway,
		AseskepSistemHamil:                       req.Hamil,
		AseskepSistemMinum:                       req.Minum,
		AseskepSistemAktivitas:                   req.Aktivitas,
		AsekepSistemBreathing:                    req.Breathing,
		AseskepSistemThermoregulasi:              req.Thermoregulasi,
		AseskepSistemNutrisi:                     req.Nutrisi,
		AseskepSistemPadaBayi:                    req.PadaBayi,
		AseskepSistemEliminasiBak:                req.EliminasiBak,
		AseskepSistemCirculation:                 req.Circulation,
		AseskepSistemEliminasiBab:                req.EliminasiBab,
		AseskepSistemRefleksCahaya:               req.RefleksCahaya,
		AseskepSistemPupil:                       req.Pupil,
		AseskepSistemPerfusiRenal:                req.PerfusiRenal,
		AseskepSistemAbdomen:                     req.Abdomen,
		AsekepSistemPola:                         req.Pola,
		AsekepSistemKualitas:                     req.Kualitas,
		AseskepSistemKejang:                      req.Kejang,
		AseskepSistemBerjalan:                    req.Berjalan,
		AseskepSistemNyeriMempegaruhi:            req.NyeriMempengaruhi,
		AseskepSistemPenglihatan:                 req.Penglihatan,
		AseskepSistemPerfusiSerebral:             req.PerfusiSerebral,
		AseskepSistemBicara:                      req.Bicara,
		AseskepSistemKenyamanan:                  req.Kenyamanan,
		AseskepSistemStatusMental:                req.StatusMental,
		AseskepSistemPenggunaanAlatBantu:         req.PenggunaanAlatBantu,
		AseskepSistemCervixTerakhir:              req.PemeriksaanCervixTerakhir,
		AseskepSistemPendengaran:                 req.Pendengaran,
		AseskepBahasaSehariHari:                  req.BahasaSehariHari,
		AseskepPerluPenerjemah:                   req.PerluPenerjemah,
		AseskepBahasaIsyarat:                     req.BahasaIsyarat,
		AseskepHambatanBelajar:                   req.HambatanBelajar,
		AseskepTingkatPendidikan:                 req.TingkatPendidikan,
		AseskepCaraBelajarYangDisukai:            req.CaraBelajarDisukai,
		AseskepReponseEmosi:                      req.ResponseEmosi,
		AseskepSistemTinggalBersama:              req.TingkatBersama,
		AseskepSistemPekerjaan:                   req.Pekerjaan,
		AseskepKebutuhanPembelajaran:             req.PotensialKebutuhanPembelajaran,
		AseskepKondisiLingkungan:                 req.KondisiLingkunganDirumah,
		AseskepSistemNilaiKepercayaan:            req.NilaiKepercayaan,
		AseskepSistemMenjalankanIbadah:           req.MenjalankanIbadah,
		AseskepSistemPemimpinAgama:               req.KunjunganPemimpin,
		AseskepSistemPerfusiGastrointestinal:     req.PerfusiGastrointestinal,
		AseskepSistemPasangPengamananTempatTidur: req.PasangPengamanTempatTidur,
		AseskepSistemTidurAtauIstirahat:          req.AktivitasAtauIstirahat,
		AseskepMslhDenganNutrisi:                 req.MasalahDenganNutrisi,
		AseskepSistemMandi:                       req.SistemMandi,
		AseskepSistemEliminasi:                   req.SistemEliminasi,
		AseskepSistemMobilisasi:                  req.SistemMobilisasi,
		AseskepSistemBerpakaian:                  req.SistemBerpakaian,
		AseskepSistemKamarMandi:                  req.SistemMandi,
	}).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnUpdatePengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error) {

	times := time.Now()

	var datas = kebidanan.AsesmenPengkajianPersistemICUBangsal{
		UpdDttm:                                  times.Format("2006-01-02 15:04:05"),
		AseskepSistemAirway:                      req.Airway,
		Pelayanan:                                req.Pelayanan,
		KeteranganPerson:                         req.Person,
		KdDpjp:                                   req.KodeDokter,
		Noreg:                                    req.Noreg,
		AsekepSistemBreathing:                    req.Breathing,
		AseskepSistemCirculation:                 req.Circulation,
		AseskepSistemNutrisi:                     req.Nutrisi,
		AseskepSistemMakan:                       req.Makan,
		AseskepSistemPadaBayi:                    req.PadaBayi,
		AseskepSistemMinum:                       req.Minum,
		AseskepSistemEliminasiBak:                req.EliminasiBak,
		AseskepSistemAksesMasuk:                  req.SistemMandi,
		AseskepSistemEliminasiBab:                req.EliminasiBab,
		AseskepSistemAktivitas:                   req.Aktivitas,
		AseskepSistemBerjalan:                    req.Berjalan,
		AseskepSistemPerfusiSerebral:             req.PerfusiSerebral,
		AseskepSistemPupil:                       req.Pupil,
		AseskepSistemRefleksCahaya:               req.RefleksCahaya,
		AseskepSistemAbdomen:                     req.Abdomen,
		AseskepSistemThermoregulasi:              req.Thermoregulasi,
		AsekepSistemPola:                         req.Pola,
		AseskepSistemPerfusiRenal:                req.PerfusiRenal,
		AseskepSistemPenggunaanAlatBantu:         req.PenggunaanAlatBantu,
		AseskepSistemPerfusiGastrointestinal:     req.PerfusiGastrointestinal,
		AseskepSistemKenyamanan:                  req.Kenyamanan,
		AseskepSistemNyeriMempegaruhi:            req.NyeriMempengaruhi,
		AsekepSistemKualitas:                     req.Kualitas,
		AseskepSistemKejang:                      req.Kejang,
		AseskepSistemPasangPengamananTempatTidur: req.PasangPengamanTempatTidur,
		AseskepSistemTidurAtauIstirahat:          req.AktivitasAtauIstirahat,
		AseskepSistemStatusMental:                req.StatusMental,
		AseskepSistemPendengaran:                 req.Pendengaran,
		AseskepSistemHamil:                       req.Hamil,
		AseskepSistemBelMudahDijangkau:           req.BelMudaDijangkau,
		AseskepSistemPenglihatan:                 req.Penglihatan,
		AseskepSistemCervixTerakhir:              req.PemeriksaanCervixTerakhir,
		AseskepSistemBicara:                      req.Bicara,
		AseskepBahasaSehariHari:                  req.BahasaSehariHari,
		AseskepPerluPenerjemah:                   req.PerluPenerjemah,
		AseskepSistemPenggunaanAlatKontrasepsi:   req.PenggunaanAlatKontrasepsi,
		AseskepHambatanBelajar:                   req.HambatanBelajar,
		AseskepCaraBelajarYangDisukai:            req.CaraBelajarDisukai,
		AseskepTingkatPendidikan:                 req.TingkatPendidikan,
		AseskepKebutuhanPembelajaran:             req.PotensialKebutuhanPembelajaran,
		AseskepReponseEmosi:                      req.ResponseEmosi,
		AseskepSistemPekerjaan:                   req.Pekerjaan,
		AseskepSistemTinggalBersama:              req.TingkatBersama,
		AseskepKondisiLingkungan:                 req.KondisiLingkunganDirumah,
		AseskepBahasaIsyarat:                     req.BahasaIsyarat,
		AseskepSistemMenjalankanIbadah:           req.MenjalankanIbadah,
		AseskepSistemPemimpinAgama:               req.KunjunganPemimpin,
		AseskepSistemMandi:                       req.SistemMandi,
		AseskepSistemBerpakaian:                  req.SistemBerpakaian,
		AseskepSistemEliminasi:                   req.SistemEliminasi,
		AseskepSistemMobilisasi:                  req.SistemMobilisasi,
		AseskepMslhDenganNutrisi:                 req.MasalahDenganNutrisi,
		AseskepSistemPresepsiTerhadapSakit:       req.PresepsiTerhadapSakit,
		AseskepSistemNilaiKepercayaan:            req.NilaiKepercayaan,
		AseskepSistemKamarMandi:                  req.SistemMandi,
	}

	result := sr.DB.Model(&res).Where("noreg=?", noReg).Updates(&datas).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

// ============== //
func (sr *kebidananRepository) GetPengkajianPersistemKeperawatanRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesemenPersistemKeperawatanBangsal, err error) {
	query := `SELECT insert_dttm, keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_pencernaan_usus, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_nutrisi, aseskep_sistem_hambatan_bahasa , aseskep_sistem_lemah_anggota_gerak, aseskep_sistem_bicara, aseskep_sistem_sakit_kepala,aseskep_sistem_status_mental,aseskep_sistem_riwayat_hipertensi, aseskep_sistem_batuk, aseskep_sistem_suara_napas,aseskep_sistem_merokok, aseskep_sistem_kekuatan_otot,  aseskep_sistem_thermoregulasi, aseskep_sistem_thermoregulasi_dingin, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_penerjemah, aseskep_sistem_akral, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where  kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPengkajianPersistemAnakKeperawatanRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKeperawatanBangsal, err error) {
	query := `SELECT insert_dttm, keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_pencernaan_usus, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_nutrisi, aseskep_sistem_hambatan_bahasa , aseskep_sistem_lemah_anggota_gerak, aseskep_sistem_bicara, aseskep_sistem_sakit_kepala,aseskep_sistem_status_mental,aseskep_sistem_riwayat_hipertensi, aseskep_sistem_batuk, aseskep_sistem_suara_napas,aseskep_sistem_merokok, aseskep_sistem_kekuatan_otot,  aseskep_sistem_thermoregulasi, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_penerjemah, aseskep_sistem_akral,aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_genitalia,  aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where  kd_bagian=? AND keterangan_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, "Anak", noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetPengkajianKebidananRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.PengkajianKebidanan, err error) {
	query := `SELECT insert_dttm, keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan,kd_bagian,noreg, aseskep_kehamilan_gravida, aseskep_kehamilan_abortus,aseskep_kehamilan_para,aseskep_kehamilan_leopold1, aseskep_kehamilan_leopold2, aseskep_kehamilan_leopold3, aseskep_kehamilan_leopold4, aseskep_kehamilan_haid, aseskep_kehamilan_pemeriksaan_dalam, aseskep_kehamilan_inspekulo_p, aseskep_kehamilan_inspekulo_v, aseskep_kehamilan_palpasi, aseskep_kehamilan_inspeksi, aseskep_kehamilan_hodge, aseskep_kehamilan_nyeri_tekan, aseskep_kehamilan_tbj, aseskep_kehamilan_tfu, aseskep_kehamilan_hamil_tua, aseskep_kehamilan_hamil_muda, aseskep_kehamilan_hpl, aseskep_kehamilan_usia_kehamilan, aseskep_kehamilan_haid_terakhir from vicore_rme.dcppt_soap_pasien  where insert_user_id=? AND kd_bagian=? AND keterangan_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetDiagnosaBandingIGDDokterRepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.DiagnosaBandingDokter, err error) {
	query := `SELECT insert_dttm, keterangan_person, insert_user_id, asesmed_diag_banding , insert_pc, kd_bagian, noreg  FROM vicore_rme.dcppt_soap_dokter  where insert_user_id=? AND kd_bagian=? AND keterangan_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnSavePengkajianPersistemKebidananRepository(data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil

}
func (sr *kebidananRepository) OnSaveDiagnosaBandingRespository(data kebidanan.DiagnosaBandingDokter) (res kebidanan.DiagnosaBandingDokter, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil

}

// AsesmenUlangPerawatanIntensive
func (sr *kebidananRepository) OnInsertAsesmenIntensiveRespository(data kebidanan.AsesmenUlangPerawatanIntensive) (res kebidanan.AsesmenUlangPerawatanIntensive, err error) {
	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *kebidananRepository) OnUpdateAsesmenIntensiveRepository(noReg string, data kebidanan.AsesmenUlangPerawatanIntensive) (res kebidanan.AsesmenUlangPerawatanIntensive, err error) {

	result := sr.DB.Model(&res).Where("noreg=?", noReg).Updates(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *kebidananRepository) OnSavePengkajianKebidananRepository(data kebidanan.PengkajianKebidanan) (res kebidanan.PengkajianKebidanan, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil

}

func (kb *kebidananRepository) OnUpdatePengkajianPersistemKebidananRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND keterangan_person=? AND kd_bagian=?", noReg, data.KeteranganPerson, kdBagian).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}
func (kb *kebidananRepository) OnUpdatePengkajianPersistemMariaRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND keterangan_person=? AND kd_bagian=?", noReg, "Bidan", kdBagian).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianPersistemDewasaRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND keterangan_person=? AND kd_bagian=?", noReg, "Dewasa", kdBagian).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianPersistemAnakRepository(noReg string, kdBagian string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND keterangan_person=? AND kd_bagian=?", noReg, "Anak", kdBagian).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianPersistemRepository(noReg string, data kebidanan.AsesemenPersistemKebidananBangsal) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND keterangan_person=?", noReg, data.KeteranganPerson).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdateDiagnosaBandingRespository(noReg string, userID string, data kebidanan.DiagnosaBandingDokter) (res kebidanan.DiagnosaBandingDokter, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND insert_user_id =?", noReg, userID).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdatePengkajianKebidananRepository(noReg string, data kebidanan.PengkajianKebidanan) (res kebidanan.PengkajianKebidanan, err error) {
	result := kb.DB.Model(&res).Where("noreg = ?", noReg).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (sr *kebidananRepository) OnSaveAsesmenKebidananRepository(data kebidanan.AsesemenKebidananBangsal) (res kebidanan.AsesemenKebidananBangsal, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil

}

func (kb *kebidananRepository) OnUpdateAsesmenKebidananRepository(noReg string, kdBagian string, req dto.ReqAsesmenAwalKebidanan) (res kebidanan.AsesemenKebidananBangsal, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND kd_bagian=?", noReg, kdBagian).Updates(&kebidanan.AsesemenKebidananBangsal{
		AseskepKel:              req.KeluhanUtama,
		AseskepRwytPnykt:        req.RiwayatPenyakitSekarang,
		AseskepRwytMenstruasi:   req.RiwayatMenstruasi,
		AseskepKelMenyertai:     req.KeluhanYangMenyertai,
		AseskepSiklusHaid:       req.SiklusHaid,
		AseskepRwytPernikahan:   req.RiwayatPernikahan,
		AseskepRwtPnyktKeluarga: req.RiwayatPenyakitKeluarga,
		AseskepRwytPnyktDahulu:  req.RiwayatPenyakitDahulu,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetAsemenKebidananRepository(noReg string, userID string, kdBagian string, person string) (res kebidanan.AsesmenKebidananModel, err error) {

	errs := kb.DB.Where("noreg=? AND insert_user_id=? AND kd_bagian=? AND keterangan_person=?", noReg, userID, kdBagian, person).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil

}

func (kb *kebidananRepository) OnSaveAsesmenKebidananRepositoryV2(data kebidanan.AsesmenKebidananModel) (res kebidanan.AsesmenKebidananModel, err error) {
	result := kb.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdateAsesmenKebidananRepositoryV2(noReg string, data kebidanan.AsesmenKebidananModel) (res kebidanan.AsesmenKebidananModel, err error) {
	result := kb.DB.Model(&res).Where("noreg = ?", noReg).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

// GET PENGKAJIAN KEBIDANAN REPOSITORY
func (kb *kebidananRepository) OnGetPengkajianKebidananV2Repository(noReg string, kdBagian string) (res kebidanan.ReportPengkajianKebidanan, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND kd_bagian", noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetPengkajianKebidananRepository(noReg string) (res kebidanan.ReportPengkajianKebidanan, err error) {
	result := kb.DB.Model(&res).Where("noreg = ?", noReg).Updates(&res).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

// ON GET TRIASE IGD DOKTER
func (kb *kebidananRepository) OnGetTriaseIGDDokterRepository(noReg string, userID string, kdBagian string, pelayanan string) (res kebidanan.TriaseIGDDokter, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND insert_user_id=? AND kd_bagian=? AND pelayanan=?", noReg, userID, kdBagian, pelayanan).Updates(&res).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnGetAsesmenTriaseIGDRepository(noReg string, userID string, kdBagian string, pelayanan string) (res kebidanan.AsesmenTriaseIGD, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND insert_user_id=? AND kd_bagian=? AND pelayanan=?", noReg, userID, kdBagian, pelayanan).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (kb *kebidananRepository) OnSaveAsesmenTriaseIGDRepository(data kebidanan.AsesmenTriaseIGD) (res kebidanan.AsesmenTriaseIGD, err error) {
	result := kb.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnSaveTriaseIGDDokterRepository(data kebidanan.TriaseIGDDokter) (res kebidanan.TriaseIGDDokter, err error) {
	result := kb.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdateTriaseIGDDokterRepository(noReg string, userID string, kdBagian string, pelayanan string, data kebidanan.TriaseIGDDokter) (res kebidanan.TriaseIGDDokter, err error) {
	result := kb.DB.Model(&res).Where("noreg=? AND insert_user_id=?  AND kd_bagian=? AND pelayanan=?", noReg, userID, kdBagian, pelayanan).Updates(&data).Scan(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (kb *kebidananRepository) OnUpdateAsesmenTriaseIGDRepository(noReg string, userID string, kdBagian string, pelayanan string, data kebidanan.AsesmenTriaseIGD) (res kebidanan.AsesmenTriaseIGD, err error) {

	result := kb.DB.Model(&res).Where("noreg = ? AND insert_user_id=? AND kd_bagian=? AND pelayanan=?", noReg, userID, kdBagian, pelayanan).Updates(&data).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

// ON GET REPORT TRIASE IGD DOKTER
func (kb *kebidananRepository) OnGetReportTriaseIGDDokterRepository(noReg string) (res kebidanan.TriaseIGDDokter, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND kd_bagian=? AND pelayanan=?", noReg, "IGD001", "rajal").Find(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (kb *kebidananRepository) OnGetReportAsesmenTriaseIGDRepository(noReg string) (res kebidanan.AsesmenTriaseIGD, err error) {
	result := kb.DB.Model(&res).Where("noreg = ? AND kd_bagian=? AND pelayanan=?", noReg, "IGD001", "rajal").First(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetPemeriksaanFisikTriaseIGDRepository(noReg string) (res rme.PemeriksaanFisikModel, err error) {

	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  "IGD001",
		Noreg:     noReg,
		KetPerson: "Dokter",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetPemeriksaanFisikMedisDokter(noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		Pelayanan: "ranap",
		Noreg:     noReg,
		KetPerson: "Dokter",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) OnGetPemeriksaanFisikPerawatRepository(noReg string, bagian string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		Pelayanan: "ranap",
		Noreg:     noReg,
		KdBagian:  bagian,
		KetPerson: "Perawat",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetTandaVitalIGDDokterRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=? AND pelayanan=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noreg, pelayanan).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data di dapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetTandaVitalPerawatRawatInapRepository(kdBagian string, noreg string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=? AND pelayanan=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, "Perawat", noreg, "ranap").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data di dapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *kebidananRepository) GetAsesmenAwalMedisRawatInapReportDokter(noreg string, kdBagian string, pelayanan string) (res rme.AsesemenMedisIGD, err error) {

	query := `SELECT ds.insert_dttm AS insert_dttm, ds.keterangan_person, ds.insert_user_id, ds.pelayanan, ds.kd_bagian, ds.noreg, ds.asesmed_keluh_utama, ds.asesmed_rwyt_skrg, ds.asesmed_lokalis_image, ds.asesmen_prognosis, ds.asesmed_konsul_ke, ds.asesmed_alasan_opname, ds.asesmed_terapi, ds.asesmed_konsul_ke, kt.namadokter, ds.jam_masuk, ds.tgl_masuk , ds.asesmed_diag_banding as diagnosa_banding FROM vicore_rme.dcppt_soap_dokter AS ds LEFT JOIN his.ktaripdokter AS kt ON  ds.insert_user_id=kt.iddokter where ds.kd_bagian=? AND ds.noreg=? AND ds.keterangan_person=? AND ds.pelayanan=?`

	result := sr.DB.Raw(query, kdBagian, noreg, "Dokter", "ranap").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *kebidananRepository) GetAsesmenAwalMedisRawatInapReportKeperawat(noreg string, kdBagian string, pelayanan string) (res rme.AsesemenMedisIGD, err error) {

	query := `SELECT ds.keterangan_person, ds.insert_user_id, ds.pelayanan, ds.kd_bagian, ds.noreg, ds.asesmed_keluh_utama, ds.asesmed_rwyt_skrg, ds.asesmed_lokalis_image, ds.asesmen_prognosis, ds.asesmed_konsul_ke, ds.asesmed_alasan_opname, ds.asesmed_terapi, ds.asesmed_konsul_ke, kt.namadokter, ds.jam_masuk, ds.tgl_masuk , ds.asesmed_diag_banding as diagnosa_banding FROM vicore_rme.dcppt_soap_dokter AS ds LEFT JOIN his.ktaripdokter AS kt ON  ds.insert_user_id=kt.iddokter where ds.kd_bagian=? AND ds.noreg=? AND ds.keterangan_person=? AND ds.pelayanan=?`

	result := sr.DB.Raw(query, kdBagian, noreg, "Perawat", "ranap").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *kebidananRepository) GetAsesmenAwalRawatInapRepository(noreg string, kdBagian string) (res rme.AsesmenPerawat, err error) {

	query := `SELECT insert_dttm,  keterangan_person as person , aseskep_jenpel_detail as jenpel_detail, tgl_masuk, tgl_keluar, kd_bagian, noreg, aseskep_kel, insert_user_id as user_id, aseskep_jenpel as jenpel, aseskep_rwyt_pnykt as rwyt_penyakit, aseskep_rwyt_pnykt_dahulu as rwyt_penyakit_dahulu, pg.nama AS nama, aseskep_reaksi_alergi as reaksi_alergi, aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas,aseskep_sistem_nutrisi, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_bicara, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_kekuatan_otot,  aseskep_sistem_thermoregulasi, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis,  aseskep_sistem_penerjemah, aseskep_sistem_hambatan_sosial, aseskep_sistem_status_mental,aseskep_sistem_riwayat_hipertensi, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_lemah_anggota_gerak, aseskep_sistem_hambatan_spiritual,  aseskep_sistem_sakit_kepala,aseskep_sistem_akral, aseskep_sistem_batuk, aseskep_sistem_suara_napas, aseskep_sistem_merokok, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_pencernaan_usus, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan  FROM vicore_rme.dcppt_soap_pasien  AS dp INNER JOIN mutiara.pengajar AS pg ON  pg.id = dp.insert_user_id where dp.kd_bagian=? AND dp.noreg=? AND dp.keterangan_person=? AND dp.pelayanan=?`

	result := sr.DB.Raw(query, kdBagian, noreg, "Perawat", "ranap").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *kebidananRepository) GetPengkajianPersistemKeperawatanReportRepository(kdBagian string, noReg string) (res kebidanan.AsesemenPersistemKebidananBangsal, err error) {
	query := `SELECT insert_dttm,	keterangan_person, insert_user_id,insert_pc, tgl_masuk,tgl_keluar, pelayanan, kd_bagian,noreg,aseskep_sistem_eliminasi_bak,aseskep_sistem_eliminasi_bab, aseskep_sistem_istirahat, aseskep_sistem_aktivitas, aseskep_sistem_mandi, aseskep_sistem_berpakaian, aseskep_sistem_makan,aseskep_sistem_eliminasi, aseskep_sistem_mobilisasi, aseskep_sistem_kardiovaskuler,aseskep_sistem_respiratori, aseskep_sistem_secebral, aseskep_sistem_perfusi_perifer,aseskep_sistem_pencernaan, aseskep_sistem_integumen,aseskep_sistem_kenyamanan, aseskep_sistem_nutrisi, aseskep_sistem_proteksi,aseskep_sistem_paps_smer, aseskep_sistem_hamil, aseskep_sistem_pendarahan, aseskep_sistem_hambatan_bahasa, aseskep_sistem_cara_belajar, aseskep_sistem_bahasa_sehari,aseskep_sistem_spikologis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_hambatan_spiritual, aseskep_sistem_response_emosi, aseskep_sistem_nilai_kepercayaan, aseskep_sistem_presepsi_sakit, aseskep_sistem_khusus_kepercayaan FROM vicore_rme.dcppt_soap_pasien where kd_bagian=? AND keterangan_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, "Perawat", noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
