package usecase

import (
	"errors"
	hu "hms_api/modules/his/entity"
	"hms_api/modules/kebidanan"
	"hms_api/modules/kebidanan/dto"
	"hms_api/modules/kebidanan/entity"
	rme "hms_api/modules/rme/entity"
	en "hms_api/modules/soap/entity"
	un "hms_api/modules/user/entity"
	"time"

	"github.com/sirupsen/logrus"
)

type kebidananUseCase struct {
	logging             *logrus.Logger
	kebidananMapper     entity.KebidananMapper
	kebidananRepository entity.KebidananRepository
	rmeRepository       rme.RMERepository
	rmeMapper           rme.RMEMapper
	soapRepository      en.SoapRepository
	soapUsecase         en.SoapUseCase
	userRepository      un.UserRepository
	hisUseCase          hu.HisUsecase
}

func NewKebidananUseCase(ur entity.KebidananRepository, logging *logrus.Logger, mapper entity.KebidananMapper, sr en.SoapRepository, rr rme.RMERepository, urs un.UserRepository, su en.SoapUseCase, rm rme.RMEMapper, hs hu.HisUsecase) entity.KebidananUsecase {
	return &kebidananUseCase{
		soapUsecase:         su,
		userRepository:      urs,
		rmeRepository:       rr,
		kebidananMapper:     mapper,
		kebidananRepository: ur,
		logging:             logging,
		soapRepository:      sr,
		rmeMapper:           rm,
		hisUseCase:          hs,
	}
}

func (ku *kebidananUseCase) SaveVitalSignKebidananUsecase(userID string, bagian string, noReg string, pelayanan string, req dto.RequestVitalSignKebidanan) (res dto.ResVitalSignKebidanan, message string, err error) {
	kebidan, errs := ku.kebidananRepository.OnGetVitalSignKebidananRepository(noReg, pelayanan)

	ku.logging.Info("VITAL SIGN KEBIDANAN USECASE")
	ku.logging.Info(kebidan)

	if errs != nil || kebidan.Noreg == "" {
		ku.logging.Info("SIMPAN DATA")
		data, err2 := ku.kebidananRepository.InsertVitalSignKebidananRepository(userID, bagian, req)
		datas, _ := ku.kebidananRepository.InsertGCSDataDemfisikRepository(userID, bagian, req)

		if err2 != nil {
			return res, "data gagal disimpan", errors.New(err2.Error())
		}

		// LAKUKAN MAPPER
		mapper := ku.kebidananMapper.ToVitalSignKebidananMapper(data, datas.GcsE, datas.GcsV, datas.GcsM, datas.Ddj)

		return mapper, "data berhasil disimpan", nil
	} else {
		ku.logging.Info("UPDATE DATA VITAL SIGN")
		update, err4 := ku.kebidananRepository.UpdateVitalSignKebidananRepository(req.NoReg, req)
		updates, _ := ku.kebidananRepository.UpdateDemfisikGCSMRepository(req.NoReg, req)

		mapper := ku.kebidananMapper.ToVitalSignKebidananMapper(update, updates.GcsE, updates.GcsV, updates.GcsM, updates.Ddj)
		if err4 != nil {
			return res, "data gagal diupdate", errors.New(err4.Error())
		}

		return mapper, "data berhasil diupdate", nil
	}

}

func (ku *kebidananUseCase) OnGetKeluhanUtamaKebidananUsecase(userID string, kdBagian string, req dto.ReqGetAsesmenAwalKebidanan) (res dto.ResponseAsesemenKebidanan, message string, err error) {
	asesmen, _ := ku.kebidananRepository.GetAsesmenKebidananRepository(kdBagian, "Bidan", req.Noreg)
	alergi, _ := ku.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	dahulu, _ := ku.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

	mapper := ku.kebidananMapper.ToMapperResponseAsesmenKebidananBangsal(asesmen, alergi, dahulu)
	return mapper, message, nil
}

// SAVE DIAGNOSA KEBIDANAN
func (ku *kebidananUseCase) OnSaveDiagnosaKebidananUsecase(req dto.ResponseSaveDiagnosaKebidanan, bagian string, userID string) (res []dto.ResDiagnosaKebidanan, message string, err error) {

	if len(req.Diagnosa) > 0 {

		for i := 0; i <= len(req.Diagnosa)-1; i++ {
			errs := ku.kebidananRepository.OnSaveDiagnosaRepository(userID, bagian, req.DeviceID, req.Noreg, req.Diagnosa[i].KodeDiagnosa, req.Diagnosa[i].NamaDiagnosa)

			if errs != nil {
				return res, message, errs
			}
		}

	}

	// GET DATA DIAGNOSA

	data, _ := ku.kebidananRepository.OnGetDiagnosaKebidananRepository(req.Noreg)

	mapper := ku.kebidananMapper.ToMappingDiagnosaSaveResponseKebidanan(data)

	return mapper, "Data berhasil disimpan", nil
}

func (ku *kebidananUseCase) SaveResikoJatuhKebidananUsecase(noReg string, userID string, bagian string, person string, req dto.RequestSaveResikoJatuhKebidanan) (message string, res kebidanan.DRisikoJatuhKebidanan, err error) {
	// CHECK APAKAH SUDAH ADA DATA
	data, errs := ku.kebidananRepository.OnGetResikoJatuhKebidananRepository(noReg, person)

	if errs != nil || data.Noreg == "" {
		ku.logging.Info("INSERT DATA RESIKO JATUH")
		insert, errs := ku.kebidananRepository.OnSaveResikoJatuhKebidananRepository(userID, bagian, person, req)

		if errs != nil {
			return "Data gagal disimpan", insert, errs
		}

		return "Data berhasil disimpan", insert, nil
	} else {
		ku.logging.Info("UPADTE DATA RESIKO JATUH")
		update, errs := ku.kebidananRepository.OnUpdateResikoJatuhKebidananRepository(bagian, person, req)

		if errs != nil {
			return "Data gagal diubah", update, errs
		}

		return "Data berhasil diubah", update, nil
	}
}

func (ku *kebidananUseCase) OnSavePemeriksaanFisikKebidananUsecase(userID string, bagian string, noReg string, pelayanan string, req dto.ReqFisikKebidanan) (message string, res kebidanan.DPemFisikKebidanan, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	data, errs := ku.kebidananRepository.OnGetPemeriksaanFisikKebidananRepository(noReg, pelayanan, req.KetPerson)

	if errs != nil || data.Noreg == "" {
		// INSERT DATA
		kebidanan, err12 := ku.kebidananRepository.OnSavePemeriksaanFisikKebidananRepository(userID, bagian, req)
		ku.logging.Info("INSERT DATA PEMERIKSAAN FISIK KEBIDANAN")
		if err12 != nil {
			return "Data gagal disimpan", kebidanan, err12
		}
		return "Data berhasil disimpan", kebidanan, nil
	} else {
		// LAKUKAN UPDATE DATA
		update, err12 := ku.kebidananRepository.OnUpdatePemeriksaanFisikKebidananRepository(userID, bagian, req)
		ku.logging.Info("INSERT DATA PEMERIKSAAN FISIK KEBIDANAN")

		if err12 != nil {
			return "Data gagal diubah", update, err12
		}

		return "Data berhasil diubah", update, nil
	}

}

func (ku *kebidananUseCase) OnSavePengkajianFungsionalKebidahanUsecase(userID string, bagian string, noReg string, req dto.RegDPengkajianFungsional) (message string, res dto.ResDPengkajianFungsional, err error) {
	data, err3 := ku.kebidananRepository.OnGetPengkajianFungsionalRepository(noReg)

	if err3 != nil || data.Noreg == "" {
		fungsional, err12 := ku.kebidananRepository.OnSavePengkajianFungsionalKebidahanRepository(userID, bagian, req)

		if err12 != nil {
			return "Data gagal disimpan", fungsional, err12
		}
		return "Data berhasil disimpan", fungsional, nil
	} else {
		fungsional, err12 := ku.kebidananRepository.OnUpdatePengkajianFungsionalKebidananRepository(noReg, req)

		if err12 != nil {
			return "Data gagal diubah", fungsional, err12
		}

		return "Data berhasil diubah", fungsional, nil
	}
}

func (ku *kebidananUseCase) OnSavePengkajianNutrisiUsecase(userID string, bagian string, noReg string, req dto.ReqPengkajianNutrisi) (message string, res dto.ResPengkajianNutrisi, err error) {
	// CHECK ADATA APAKAH ADA
	data, err3 := ku.kebidananRepository.OnGetPengkajianNutrisiRepository(noReg)

	if err3 != nil || data.Noreg == "" {
		fungsional, err12 := ku.kebidananRepository.OnSavePengkajianNutrisiRepository(userID, bagian, req)

		if err12 != nil {
			return "Data gagal disimpan", fungsional, err12
		}

		return "Data berhasil disimpan", fungsional, nil
	} else {

		fungsional, err12 := ku.kebidananRepository.OnUpdatePengkajianNutrisi(noReg, req)

		if err12 != nil {
			return "Data gagal diubah", fungsional, err12
		}

		return "Data berhasil diubah", fungsional, nil
	}

}

// == ON SAVE PENGKAJIAN NUTRISI ANAK
func (ku *kebidananUseCase) OnSavePengkajianNutrisiAnakUsecase(userID string, bagian string, noReg string, req dto.ReqPengkajianNutrisiAnak) (message string, res dto.ResPengkajianNutrisiAnak, err error) {
	// CHECK ADATA APAKAH ADA
	data, err3 := ku.kebidananRepository.OnGetPengkajianNutrisiAnakRepository(noReg)

	if err3 != nil || data.Noreg == "" {
		fungsional, err12 := ku.kebidananRepository.OnSavePengkajianNutrisiAnakRepository(userID, bagian, req)

		if err12 != nil {
			return "Data gagal disimpan", fungsional, err12
		}

		return "Data berhasil disimpan", fungsional, nil
	} else {
		fungsional, err12 := ku.kebidananRepository.OnUpdatePengkajianNutrisiAnakRepository(noReg, req)

		if err12 != nil {
			return "Data gagal diubah", fungsional, err12
		}

		return "Data berhasil diubah", fungsional, nil
	}
}

func (ku *kebidananUseCase) OnSaveAsesmenNyeriAnakUseCase(userID string, bagian string, noReg string, req dto.OnSavePengkajianNIpsAnak) (message string, err error) {
	// CEK APAKAH SUDAH ADA DATANYA
	getSkala, er123 := ku.rmeRepository.OnGetSkalaNyeriKeperawatanAnakRepository(noReg, bagian)

	if er123 != nil || getSkala.Noreg == "" {
		ku.logging.Info("Data Di SImpan")
		times := time.Now()
		tanggal, _ := ku.soapRepository.GetJamMasukPasienRepository(noReg)

		var onSave = kebidanan.PengkajianNyeriModel{
			Noreg:                 noReg,
			InsertDttm:            times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:      "Anak",
			InsertUserId:          userID,
			InsertPc:              req.DeviceID,
			Pelayanan:             req.Pelayanan,
			KdBagian:              bagian,
			KdDpjp:                req.KdDokter,
			TglMasuk:              tanggal.Tanggal[0:10],
			AseskepNyeri:          req.AseskepNyeri,
			AseskepLokasiNyeri:    req.AseskepLokasiNyeri,
			AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri,
			AseskepNyeriMenjalar:  req.AseskepNyeriMenjalar,
			AseskepKualitasNyeri:  req.AseskepKualitasNyeri,
		}

		_, messages, er12 := ku.kebidananRepository.OnSaveAsesmenNyeriIcuRepository(userID, onSave)

		onGetNips, er11 := ku.kebidananRepository.OnGetAsesmenNIPSRepository(noReg, bagian)

		if er11 != nil || onGetNips.Noreg == "" {
			// SAVE NYERI NIPS
			var nips = kebidanan.PengkajianNyeriNIPS{
				Noreg:         noReg,
				InsertDttm:    times.Format("2006-01-02 15:04:05"),
				KdBagian:      bagian,
				EkspresiWajah: req.EkspresiWajah,
				Tangisan:      req.Tangisan,
				PolaNapas:     req.PolaNapas,
				Tangan:        req.Tangan,
				Kaki:          req.Kaki,
				Kesadaran:     req.Kesadaran,
				Total:         req.Total,
			}

			_, _, nil := ku.kebidananRepository.OnSaveAssesmenNIPSRepository(nips)

			if er12 != nil {
				return messages, er12
			}
		} else {

			var updateNips = kebidanan.PengkajianNyeriNIPS{
				EkspresiWajah: req.EkspresiWajah,
				Tangisan:      req.Tangisan,
				PolaNapas:     req.PolaNapas,
				Tangan:        req.Tangan,
				Kaki:          req.Kaki,
				Kesadaran:     req.Kesadaran,
				Total:         req.Total,
			}

			ku.kebidananRepository.OnUpdateAsessmenNIPSRepository(noReg, bagian, updateNips)

		}

		return message, nil

	} else {
		// UPDATE DATA
		ku.logging.Info("Data Di Di Update")
		var update = kebidanan.PengkajianNyeriModel{
			KeteranganPerson:      "Anak",
			AseskepNyeri:          req.AseskepNyeri,
			AseskepLokasiNyeri:    req.AseskepLokasiNyeri,
			AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri,
			AseskepNyeriMenjalar:  req.AseskepNyeriMenjalar,
			AseskepKualitasNyeri:  req.AseskepKualitasNyeri,
		}
		times := time.Now()

		_, messages, er12 := ku.kebidananRepository.OnUpdateAsesmenNyeriICURepository(userID, bagian, req.Noreg, req.Pelayanan, update)

		onGetNips, er11 := ku.kebidananRepository.OnGetAsesmenNIPSRepository(noReg, bagian)

		if er11 != nil || onGetNips.Noreg == "" {
			// SAVE NYERI NIPS
			var nips = kebidanan.PengkajianNyeriNIPS{
				Noreg:         noReg,
				InsertDttm:    times.Format("2006-01-02 15:04:05"),
				KdBagian:      bagian,
				EkspresiWajah: req.EkspresiWajah,
				Tangisan:      req.Tangisan,
				PolaNapas:     req.PolaNapas,
				Tangan:        req.Tangan,
				Kaki:          req.Kaki,
				Kesadaran:     req.Kesadaran,
				Total:         req.Total,
			}

			_, _, nil := ku.kebidananRepository.OnSaveAssesmenNIPSRepository(nips)

			if er12 != nil {
				return messages, er12
			}
		} else {

			var updateNips = kebidanan.PengkajianNyeriNIPS{
				EkspresiWajah: req.EkspresiWajah,
				Tangisan:      req.Tangisan,
				PolaNapas:     req.PolaNapas,
				Tangan:        req.Tangan,
				Kaki:          req.Kaki,
				Kesadaran:     req.Kesadaran,
				Total:         req.Total,
			}

			ku.kebidananRepository.OnUpdateAsessmenNIPSRepository(noReg, bagian, updateNips)

		}

		return messages, nil
	}

}

func (ku *kebidananUseCase) OnSaveAssesmenNyeriIcuUseCase(userID string, bagian string, noReg string, req dto.ReqInsertAsesmenNyeriICU) (message string, res dto.AsesmenNyeiICU, err error) {
	getSkala, er123 := ku.rmeRepository.OnGetSkalaNyeriKeperawatanDewasaRepository(noReg, bagian)

	ku.logging.Info("GET ASESMEN NYERI")
	ku.logging.Info(getSkala)

	if er123 != nil || getSkala.Noreg == "" {
		times := time.Now()
		tanggal, _ := ku.soapRepository.GetJamMasukPasienRepository(noReg)

		var onSave = kebidanan.PengkajianNyeriModel{
			Noreg:                 noReg,
			InsertDttm:            times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:      req.Person,
			InsertUserId:          userID,
			InsertPc:              req.DeviceID,
			Pelayanan:             req.Pelayanan,
			KdBagian:              bagian,
			KdDpjp:                req.KdDokter,
			TglMasuk:              tanggal.Tanggal[0:10],
			AseskepNyeri:          req.AseskepNyeri,
			AseskepLokasiNyeri:    req.AseskepLokasiNyeri,
			AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri,
			AseskepNyeriMenjalar:  req.AseskepNyeriMenjalar,
			AseskepKualitasNyeri:  req.AseskepKualitasNyeri,
		}

		saveData, messages, er12 := ku.kebidananRepository.OnSaveAsesmenNyeriIcuRepository(userID, onSave)

		if er12 != nil {
			return "Data gagal disimpan", res, er12
		}

		mapper := ku.kebidananMapper.ToNyeriICUResponseMapper2(saveData)

		return messages, mapper, er12

	} else {
		// UPDATE DATA
		var update = kebidanan.PengkajianNyeriModel{
			AseskepNyeri:          req.AseskepNyeri,
			AseskepLokasiNyeri:    req.AseskepLokasiNyeri,
			AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri,
			AseskepNyeriMenjalar:  req.AseskepNyeriMenjalar,
			AseskepKualitasNyeri:  req.AseskepKualitasNyeri,
		}

		update, messages, er12 := ku.kebidananRepository.OnUpdateAsesmenNyeriICURepository(userID, bagian, req.NoReg, req.Pelayanan, update)

		if er12 != nil {
			return messages, res, er12
		}

		mapper := ku.kebidananMapper.ToNyeriICUResponseMapper2(update)

		return messages, mapper, nil
	}
}

// ON GET PENGKAJIAN
func (su *kebidananUseCase) OnGetTandaVitalKebidananBangsalUseCase(userID string, kdBagian string, person string, noReg string) (
	res dto.TandaVitalBangsalKebidananResponse, err error) {

	vital, _ := su.kebidananRepository.GetVitalSignKebidananBangsalRepository(userID, kdBagian, person, noReg)

	// GET PEMFISIK IGD DOKTER
	fisik, _ := su.kebidananRepository.GetPemFisikKebidananRepository(userID, kdBagian, person, noReg)

	mapper := su.kebidananMapper.ToMapperResponseTandaVitalKebidananBangsal(vital, fisik)

	su.logging.Info(mapper)

	return mapper, nil
}

func (su *kebidananUseCase) OnSaveAsesmenKebidananBangsalUseCase(req dto.ReqAsesmenAwalKebidanan, userID string, person string, kdBagian string) (message string, err error) {
	// GET ASESMEN KEBIDANAN
	kebidanans, err12 := su.kebidananRepository.GetAsesmenKebidananRepository(kdBagian, person, req.NoReg)

	su.logging.Info("DATA KEBIDANAN")
	su.logging.Info(kebidanans)

	if err12 != nil || kebidanans.Noreg == "" {
		// SIMPAN DATA

		// DAPATKAN TANGGAL MASUK DARI DREGISTER
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.NoReg)
		times := time.Now()

		var bidan = kebidanan.AsesemenKebidananBangsal{
			InsertDttm:              times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:        req.Person,
			InsertUserId:            userID,
			TglMasuk:                tanggal.Tanggal[0:10],
			TglKeluar:               times.Format("2006-01-02"),
			InsertPc:                req.DeviceID,
			Pelayanan:               req.Pelayanan,
			KdBagian:                kdBagian,
			Noreg:                   req.NoReg,
			AseskepKel:              req.KeluhanUtama,
			AseskepRwytPnykt:        req.RiwayatPenyakitSekarang,
			AseskepRwytMenstruasi:   req.RiwayatMenstruasi,
			AseskepKelMenyertai:     req.KeluhanYangMenyertai,
			AseskepSiklusHaid:       req.SiklusHaid,
			AseskepRwytPernikahan:   req.RiwayatPernikahan,
			AseskepRwtPnyktKeluarga: req.RiwayatPenyakitKeluarga,
			AseskepRwytPnyktDahulu:  req.RiwayatPenyakitDahulu,
		}

		_, errq := su.kebidananRepository.OnSaveAsesmenKebidananRepository(bidan)

		if errq != nil {
			return "Data gagal disimpan", errq
		}

		return "Data berhasil disimpan", nil

	} else {
		// UPDATE DATA ASESMEN KEBIDANAN
		_, err12 := su.kebidananRepository.OnUpdateAsesmenKebidananRepository(req.NoReg, kdBagian, req)

		if err12 != nil {
			return "Data gagal diupdate", err12
		}

		return "Data berhasil diupdate", nil

	}
}

func (su *kebidananUseCase) OnSaveAsesmenPengkajianPersistemUseCase(req dto.ReqSavePengkajianPersistem, userID string, person string, kdBagian string) (message string, err error) {
	// GET DATA
	data, err1 := su.kebidananRepository.GetPengkajianPersistemKebidananRepository(userID, kdBagian, req.Person, req.Noreg)

	times := time.Now()

	if err1 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			InsertDttm:                     times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:               req.Person,
			InsertUserId:                   userID,
			InsertPc:                       req.DeviceID,
			TglMasuk:                       tanggal.Tanggal[0:10],
			TglKeluar:                      times.Format("2006-01-02"),
			Pelayanan:                      req.Pelayanan,
			KdBagian:                       kdBagian,
			Noreg:                          req.Noreg,
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
		}
		// SIMPAN DATA
		_, err12 := su.kebidananRepository.OnSavePengkajianPersistemKebidananRepository(datas)

		if err12 != nil {
			return "Data gagal disimpan", err12
		}

		return "Data berhasil disimpan", nil
	} else {

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
		}

		_, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemKebidananRepository(req.Noreg, kdBagian, datas)

		if er12 != nil {
			return "Data gagal diubah", er12
		}

		return "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSavePengkajianPersistemKebidananUseCase(req dto.ReqSavePengkajianPersistem, userID string, person string, kdBagian string) (messaage string, err error) {
	data, err1 := su.kebidananRepository.OnGetPengkajianPersistemMariaRepository(kdBagian, req.Noreg)

	times := time.Now()

	if err1 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			InsertDttm:                     times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:               "Bidan",
			InsertUserId:                   userID,
			InsertPc:                       req.DeviceID,
			TglMasuk:                       tanggal.Tanggal[0:10],
			TglKeluar:                      times.Format("2006-01-02"),
			Pelayanan:                      req.Pelayanan,
			KdBagian:                       kdBagian,
			Noreg:                          req.Noreg,
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
		}
		// SIMPAN DATA
		_, err12 := su.kebidananRepository.OnSavePengkajianPersistemKebidananRepository(datas)

		if err12 != nil {
			return "Data gagal disimpan", err12
		}

		return "Data berhasil disimpan", nil
	} else {

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
		}

		_, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemMariaRepository(req.Noreg, kdBagian, datas)

		if er12 != nil {
			return "Data gagal diubah", er12
		}

		return "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSaveAsesmenPengkajianKeperawatanDewasaPersistemUseCase(req dto.ReqSavePengkajianKeperawatan, userID string, person string, kdBagian string) (message string, err error) {
	// GET DATA
	data, err1 := su.kebidananRepository.GetPengkajianPersistemDewasaRepository(userID, kdBagian, req.Person, req.Noreg)

	times := time.Now()

	if err1 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			InsertDttm:                     times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:               "Dewasa",
			InsertUserId:                   userID,
			InsertPc:                       req.DeviceID,
			TglMasuk:                       tanggal.Tanggal[0:10],
			TglKeluar:                      times.Format("2006-01-02"),
			Pelayanan:                      req.Pelayanan,
			KdBagian:                       kdBagian,
			Noreg:                          req.Noreg,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
			AseskepSistemGenitalia:         req.ThermoregulasiDingin,
		}
		// SIMPAN DATA
		_, err12 := su.kebidananRepository.OnSavePengkajianPersistemKebidananRepository(datas)

		if err12 != nil {
			return "Data gagal disimpan", err12
		}

		return "Data berhasil disimpan", nil
	} else {

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
			AseskepSistemGenitalia:         req.ThermoregulasiDingin,
		}

		_, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemDewasaRepository(req.Noreg, kdBagian, datas)

		if er12 != nil {
			return "Data gagal diubah", er12
		}

		return "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSavePengkajianPersistemAnakUseCase(req dto.ReqSavePengkajianKeperawatan, person string, userID string, kdBagian string) (message string, err error) {

	data, err1 := su.kebidananRepository.GetPengkajianPersistemAnakRepository(kdBagian, req.Person, req.Noreg)

	times := time.Now()

	if err1 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			InsertDttm:                     times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:               "Anak",
			InsertUserId:                   userID,
			InsertPc:                       req.DeviceID,
			TglMasuk:                       tanggal.Tanggal[0:10],
			TglKeluar:                      times.Format("2006-01-02"),
			Pelayanan:                      req.Pelayanan,
			KdBagian:                       kdBagian,
			Noreg:                          req.Noreg,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemGenitalia:         req.Genitalia,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
		}
		// SIMPAN DATA
		_, err12 := su.kebidananRepository.OnSavePengkajianPersistemKebidananRepository(datas)

		if err12 != nil {
			return "Data gagal disimpan", err12
		}

		return "Data berhasil disimpan", nil
	} else {

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemGenitalia:         req.Genitalia,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
		}

		_, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemAnakRepository(req.Noreg, kdBagian, datas)

		if er12 != nil {
			return "Data gagal diubah", er12
		}

		return "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSaveAsesmenPengkajianKeperawatanPersistemUseCase(req dto.ReqSavePengkajianKeperawatan, userID string, person string, kdBagian string) (message string, err error) {
	// GET DATA
	data, err1 := su.kebidananRepository.GetPengkajianPersistemDewasaRepository(userID, kdBagian, req.Person, req.Noreg)

	times := time.Now()

	if err1 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			InsertDttm:                     times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:               req.Person,
			InsertUserId:                   userID,
			InsertPc:                       req.DeviceID,
			TglMasuk:                       tanggal.Tanggal[0:10],
			TglKeluar:                      times.Format("2006-01-02"),
			Pelayanan:                      req.Pelayanan,
			KdBagian:                       kdBagian,
			Noreg:                          req.Noreg,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
		}
		// SIMPAN DATA
		_, err12 := su.kebidananRepository.OnSavePengkajianPersistemKebidananRepository(datas)

		if err12 != nil {
			return "Data gagal disimpan", err12
		}

		return "Data berhasil disimpan", nil
	} else {
		datas := kebidanan.AsesemenPersistemKebidananBangsal{
			AseskepSistemEliminasiBak:      req.AseskepSistemEliminasiBak,
			AseskepSistemEliminasiBab:      req.AseskepSistemEliminasiBab,
			AseskepSistemIstirahat:         req.AseskepSistemIstirahat,
			AseskepSistemAktivitas:         req.AseskepSistemAktivitas,
			AseskepSistemMandi:             req.AseskepSistemMandi,
			AseskepSistemBerpakaian:        req.AseskepSistemBerpakaian,
			AseskepSistemMasalahProstat:    req.MasalahProstat,
			AseskepSistemMakan:             req.AseskepSistemMakan,
			AseskepSistemEliminasi:         req.AseskepSistemEliminasi,
			AseskepSistemMobilisasi:        req.AseskepSistemMobilisasi,
			AseskepSistemPenerjemah:        req.Penerjemah,
			AseskepSistemPencernaanUsus:    req.Usus,
			AseskepSistemKardiovaskuler:    req.AseskepSistemKardiovaskuler,
			AseskepSistemRespiratori:       req.AseskepSistemRespiratori,
			AseskepSistemSecebral:          req.AseskepSistemSecebral,
			AseskepSistemPerfusiPerifer:    req.AseskepSistemPerfusiPerifer,
			AseskepSistemPencernaan:        req.AseskepSistemPencernaan,
			AseskepSistemIntegumen:         req.AseskepSistemIntegumen,
			AseskepSistemKenyamanan:        req.AseskepSistemKenyamanan,
			AseskepSistemProteksi:          req.AseskepSistemProteksi,
			AseskepSistemPapsSmer:          req.AseskepSistemPapsSmer,
			AseskepSistemHamil:             req.AseskepSistemHamil,
			AseskepSistemHambatanBahasa:    req.AseskepSistemBahasaSehari,
			AseskepSistemPendarahan:        req.AseskepSistemPendarahan,
			AseskepSistemCaraBelajar:       req.AseskepSistemCaraBelajar,
			AseskepSistemBahasaSehari:      req.AseskepSistemBahasaSehari,
			AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
			AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
			AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
			AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
			AseskepSistemResponseEmosi:     req.AseskepSistemResponseEmosi,
			AseskepSistemNilaiKepercayaan:  req.AseskepSistemNilaiKepercayaan,
			AseskepSistemPresepsiSakit:     req.AseskepSistemPresepsiSakit,
			AseskepSistemKhususKepercayaan: req.AseskepSistemKhususKepercayaan,
			AseskepSistemThermoregulasi:    req.Thermoregulasi,
			AseskepSistemLemahAnggotaGerak: req.AnggotaGerak,
			AseskepSistemSakitKepala:       req.SakitKepala,
			AseskepSistemStatusMental:      req.PerubahanStatusMental,
			AseskepSistemBicara:            req.Bicara,
			AseskepSistemRiwayatHipertensi: req.RiwayatHipertensi,
			AseskepSistemKekuatanOtot:      req.KekuatanOtot,
			AseskepSistemAkral:             req.Akral,
			AseskepSistemBatuk:             req.Batuk,
			AseskepSistemSuaraNapas:        req.SuaraNapas,
			AseskepSistemNutrisi:           req.Nutrisi,
			AseskepSistemMerokok:           req.Merokok,
		}

		_, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemRepository(req.Noreg, datas)

		if er12 != nil {
			return "Data gagal diubah", er12
		}

		return "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSaveDiagnosaBandingUseCase(userID string, kdBagian string, reg dto.RegSaveDianosaBandingIGDDokter) (message string, err error) {
	// GET DIANOSA BANDING
	dianosa, err1 := su.kebidananRepository.GetDiagnosaBandingIGDDokterRepository(userID, kdBagian, reg.Person, reg.Noreg)

	if err1 != nil || dianosa.Noreg == "" {
		// SIMPAN DATA
		times := time.Now()
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(reg.Noreg)

		data := kebidanan.DiagnosaBandingDokter{
			InsertDttm:          times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:    reg.Person,
			InsertUserId:        userID,
			InsertPc:            reg.DevicesID,
			TglMasuk:            tanggal.Tanggal[0:10],
			TglKeluar:           times.Format("2006-01-02"),
			Pelayanan:           reg.Pelayanan,
			AsesmedDiagBanding:  reg.DiagnosaBanding,
			KdDpjp:              userID,
			AsesmedDokterTtd:    userID,
			AsesmedDokterTtdTgl: times.Format("2006-01-02"),
			AsesmedDokterTtdJam: times.Format("15:04:05"),
			KdBagian:            kdBagian,
			Noreg:               reg.Noreg,
		}

		_, er22 := su.kebidananRepository.OnSaveDiagnosaBandingRespository(data)

		if er22 != nil {
			return "Data gagal disimpan", er22
		}

		return "Data berhasil disimapn", nil
	} else {
		datas := kebidanan.DiagnosaBandingDokter{
			AsesmedDiagBanding: reg.DiagnosaBanding,
		}

		_, err1 := su.kebidananRepository.OnUpdateDiagnosaBandingRespository(reg.Noreg, userID, datas)

		if err1 != nil {
			return "Data gagal diupdate", err1
		}

		return "Data berhasil diupdate", nil
	}

}

func (su *kebidananUseCase) OnGetAsesmenKebidananUsecase(userID string, noReg string, kdBagian string, person string) (res dto.ResponseKebidanann, message string, err error) {

	data, err12 := su.kebidananRepository.OnGetAsemenKebidananRepository(noReg, userID, kdBagian, "Bidan")

	if err12 != nil {
		return res, "Gagal mendapatkan data", err12
	}

	asesen := su.kebidananMapper.ToResponseAsesmenKebidanan(data)

	return asesen, message, nil
}

func (su *kebidananUseCase) OnGerReportTandaVitalKebidananBangsalUseCase(kdBagian string, person string, noReg string) (res dto.TandaVitalBangsalKebidananResponse, err error) {

	vital, _ := su.kebidananRepository.GetVitalSignKebidananBangsalReportRepository(kdBagian, person, noReg)

	// GET PEMFISIK IGD DOKTER
	su.logging.Info(vital)
	fisik, _ := su.kebidananRepository.GetPemFisikKebidananReportRepository(kdBagian, person, noReg)
	su.logging.Info(fisik)

	mapper := su.kebidananMapper.ToMapperResponseTandaVitalKebidananBangsal(vital, fisik)

	return mapper, nil
}

func (su *kebidananUseCase) OnGetReportPengkajianKebidananUseCase(kdBagian string, person string, noReg string, noRM string) (res dto.ResponserPengkajianKebidanan, err error) {
	kebidanan, errs1 := su.kebidananRepository.OnGetPengkajianKebidananRepository(noReg)
	vital, _ := su.OnGerReportTandaVitalKebidananBangsalUseCase(kdBagian, person, noReg)
	fisik, _ := su.kebidananRepository.OnGetPemeriksaanFisikKebidananRepository(kdBagian, person, noReg)
	riwayatKehamilan, _ := su.kebidananRepository.OnGetRiwayatKehamilanRepository(noReg)
	nutrisi, _ := su.kebidananRepository.OnGetPengkajianNutrisiRepository(noReg)

	su.kebidananRepository.OnGetPemeriksaanFisikKebidananRepository(noReg, "ranap", person)
	fisikKebi, _ := su.kebidananRepository.OnGetPemeriksaanFisikKebidananReportRepository(noReg, person)

	dianosa, _ := su.kebidananRepository.OnGetDiagnosaKebidananRepository(noReg)
	penyakitKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)

	// ===== //
	pengobatan, _ := su.kebidananRepository.OnGetRiwayatPengobatanDirumahRepository(noReg)
	funsional, _ := su.kebidananRepository.OnGetPengkajianFungsionalRepository(noReg)
	karyawan, _ := su.userRepository.GetKaryawanRepository(kebidanan.InsertUserId)
	karu, _ := su.userRepository.GetKaryawanRepository("S13")

	if errs1 != nil {
		return res, errs1
	}

	// MAPPING DATA
	mapper := su.kebidananMapper.ToResponsePengkajianKebidanan(kebidanan, vital, riwayatKehamilan, pengobatan, fisik, nutrisi, funsional, dianosa, fisikKebi, penyakitKeluarga, karyawan, karu)

	return mapper, nil
}

func (su *kebidananUseCase) OnGetReportPengkajianKebidananBatuRajaUseCaseV2(noReg string, noRM string) (res dto.ResponsePengkajianKebidananBatuRajaV2, err error) {
	// GET PENGKAJIAN KEBIDANAN REPOSITORY != "GET PENGKAJIAN KEBIDANAN"
	su.kebidananRepository.OnGetPengkajianKebidananRepository(noReg)

	// MAPPING DATA
	// su.kebidananMapper.ToMapperReponsePengkajianKeperawatanBangsal()

	return res, nil
}

func (su *kebidananUseCase) OnSaveAsesmenKebidananUseCase(userID string, kdBagian string, req dto.ReqOnSaveAsesmenKebidanan) (res dto.ResponseKebidanann, message string, err error) {
	// GET DATA APAKAH ADA
	data, e11 := su.kebidananRepository.OnGetAsemenKebidananRepository(req.Noreg, userID, kdBagian, "Bidan")

	if e11 != nil || data.Noreg == "" {
		// LAKUKAN INSERT DATA
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		times := time.Now()

		var dat = kebidanan.AsesmenKebidananModel{
			InsertDttm:                       times.Format("2006-01-02 15:04:05"),
			InsertUserId:                     userID,
			KeteranganPerson:                 "Bidan",
			TglMasuk:                         tanggal.Tanggal[0:10],
			InsertPc:                         req.DeviceID,
			KdBagian:                         kdBagian,
			Noreg:                            req.Noreg,
			Pelayanan:                        req.Pelayanan,
			KdDpjp:                           req.Dpjp,
			AseskepKehamilanGravida:          req.AseskepKehamilanGravida,
			AseskepKehamilanAbortus:          req.AseskepKehamilanAbortus,
			AseskepKehamilanPara:             req.AseskepKehamilanPara,
			AseskepKehamilanHaid:             req.AseskepKehamilanHaid,
			AseskepHaidTerakhir:              req.AseskepKehamilanHaidTerakhir,
			AseskepUsiaKehamilan:             req.AseskepUsiaKehamilan,
			AseskepPartusHpl:                 req.AseskepPartusHpl,
			AseskepKehamilanLeopold1:         req.AseskepKehamilanLeopold1,
			AseskepKehamilanLeopold2:         req.AseskepKehamilanLeopold2,
			AseskepKehamilanLeopold3:         req.AseskepKehamilanLeopold3,
			AseskepKehamilanLeopold4:         req.AseskepKehamilanLeopold4,
			AseskepKehamilanHodge:            req.AseskepKehamilanHodge,
			AseskepKehamilanInspeksi:         req.AseskepKehamilanInspeksi,
			AseskepKehamilanInspekuloV:       req.AseskepKehamilanInspekuloV,
			AseskepKehamilanInspekuloP:       req.AseskepKehamilanInspekuloP,
			AseskepKehamilanPemeriksaanDalam: req.AseskepKehamilanPemeriksaanDalam,
			AseskepKehamilanHamilMuda:        req.AseskepKehamilanHamilMuda,
			AseskepKehamilanHamilTua:         req.AseskepKehamilanHamilTua,
			AseskepKehamilanTbj:              req.AseskepKehamilanTbj,
			AseskepKehamilanUsiaKehamilan:    req.AseskepUsiaKehamilan,
			AseskepKehamilanTfu:              req.AseskepKehamilanTfu,
			AseskepKehamilanNyeriTekan:       req.AseskepKehamilanNyeriTekan,
			AseskepKehamilanPalpasi:          req.AseskepKehamilanPalpasi,
		}

		simpan, er12 := su.kebidananRepository.OnSaveAsesmenKebidananRepositoryV2(dat)

		if er12 != nil {
			return res, "Data gagal disimpan", er12
		}

		// LAKUKAN MAPPING DATA
		mapper := su.kebidananMapper.ToResponseAsesmenKebidanan(simpan)

		return mapper, "Data berhasil disimpan", nil

	} else {
		var dat = kebidanan.AsesmenKebidananModel{
			AseskepKehamilanGravida:          req.AseskepKehamilanGravida,
			AseskepKehamilanAbortus:          req.AseskepKehamilanAbortus,
			AseskepKehamilanPara:             req.AseskepKehamilanPara,
			AseskepKehamilanHaid:             req.AseskepKehamilanHaid,
			AseskepHaidTerakhir:              req.AseskepKehamilanHaidTerakhir,
			AseskepUsiaKehamilan:             req.AseskepUsiaKehamilan,
			AseskepPartusHpl:                 req.AseskepPartusHpl,
			AseskepKehamilanLeopold1:         req.AseskepKehamilanLeopold1,
			AseskepKehamilanLeopold2:         req.AseskepKehamilanLeopold2,
			AseskepKehamilanLeopold3:         req.AseskepKehamilanLeopold3,
			AseskepKehamilanLeopold4:         req.AseskepKehamilanLeopold4,
			AseskepKehamilanHodge:            req.AseskepKehamilanHodge,
			AseskepKehamilanInspeksi:         req.AseskepKehamilanInspeksi,
			AseskepKehamilanInspekuloV:       req.AseskepKehamilanInspekuloV,
			AseskepKehamilanInspekuloP:       req.AseskepKehamilanInspekuloP,
			AseskepKehamilanPemeriksaanDalam: req.AseskepKehamilanPemeriksaanDalam,
			AseskepKehamilanHamilMuda:        req.AseskepKehamilanHamilMuda,
			AseskepKehamilanHamilTua:         req.AseskepKehamilanHamilTua,
			AseskepKehamilanTbj:              req.AseskepKehamilanTbj,
			AseskepKehamilanUsiaKehamilan:    req.AseskepUsiaKehamilan,
			AseskepKehamilanTfu:              req.AseskepKehamilanTfu,
			AseskepKehamilanNyeriTekan:       req.AseskepKehamilanNyeriTekan,
			AseskepKehamilanPalpasi:          req.AseskepKehamilanPalpasi,
		}

		update, err12 := su.kebidananRepository.OnUpdateAsesmenKebidananRepositoryV2(req.Noreg, dat)

		if err12 != nil {
			return res, "Data gagal disimpan", err12
		}

		mapper := su.kebidananMapper.ToResponseAsesmenKebidanan(update)

		return mapper, "Data berhasil diubah", nil
	}
}

func (su *kebidananUseCase) OnSaveAsesmenIntensiveUseCase(userID string, kdBagian string, req dto.OnRegAsesmenUlangKeperawatanIntensive) (res dto.ResponseAsesmenUlangKeperawatanIntensive, message string, err error) {
	// CEK APAKAH SUDAH ADA ASESMEN INTENSIVE
	data, e11 := su.kebidananRepository.OnGetAsemenKebidananRepository(req.Noreg, userID, kdBagian, req.Person)

	if e11 != nil || data.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		times := time.Now()

		var data = kebidanan.AsesmenUlangPerawatanIntensive{
			InsertDttm:                   times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:             req.Person,
			InsertUserId:                 userID,
			TglMasuk:                     tanggal.Tanggal[0:10],
			InsertPc:                     req.DeviceID,
			Pelayanan:                    req.Pelayanan,
			KdBagian:                     kdBagian,
			Noreg:                        req.Noreg,
			KdDpjp:                       req.KdDPJP,
			Asesmen:                      req.Asesmen,
			AseskepRwtDari:               req.Dari,
			AseskepCaraMasuk:             req.CaraMasuk,
			AseskepAsalMasuk:             req.Dari,
			AseskepKel:                   req.KeluhanUtama,
			AseskepRwytPnykt:             req.RiwayatPenyakitDahulu,
			AseskepReaksiYangMuncul:      req.ReaksiYangMuncul,
			AseskepReaksiAlergi:          req.RiwayatAlergi,
			AseskepTransfusiDarah:        req.TransfusiDarah,
			AseskepRwtMerokok:            req.RiwayatMerokok,
			AseskepRwtMinumanKeras:       req.RiwayatMinumanKeras,
			AseskepRwtAlkoholMempegaruhi: req.AlcoholMempegaruhiHidup,
			AseskepRwytPnyktDahulu:       req.RiwayatPenyakitDahulu,
		}

		// INSERT DATA
		save, er123 := su.kebidananRepository.OnInsertAsesmenIntensiveRespository(data)

		if er123 != nil {
			return res, "Data gagal disimpan", er123
		}

		// riwayatPenyakit, _ := rh.RMERepository.OnGetRiwayatPenyakitDahuluPerawat(payload.NoRM, payload.Tanggal)
		penyakitKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRM)
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRM, req.Tanggal)
		mapper := su.kebidananMapper.ToAsesmenUlangPerawatanIntesiveMapper(save, penyakitKeluarga, riwayat)

		return mapper, "Data berhasil disimpan", nil

	} else {
		// UPDATE DATA ASESMEN ULANG PERAWATAN INTENSIVE OnUpdateAsesmenIntensiveRepository
		var udpates1 = kebidanan.AsesmenUlangPerawatanIntensive{
			KeteranganPerson:             req.Person,
			InsertUserId:                 userID,
			InsertPc:                     req.DeviceID,
			Pelayanan:                    req.Pelayanan,
			KdBagian:                     kdBagian,
			Noreg:                        req.Noreg,
			KdDpjp:                       req.KdDPJP,
			Asesmen:                      req.Asesmen,
			AseskepRwtDari:               req.Dari,
			AseskepCaraMasuk:             req.CaraMasuk,
			AseskepAsalMasuk:             req.Dari,
			AseskepKel:                   req.KeluhanUtama,
			AseskepRwytPnykt:             req.RiwayatPenyakitSekarang,
			AseskepReaksiYangMuncul:      req.ReaksiYangMuncul,
			AseskepReaksiAlergi:          req.RiwayatAlergi,
			AseskepTransfusiDarah:        req.TransfusiDarah,
			AseskepRwtMerokok:            req.RiwayatMerokok,
			AseskepRwtMinumanKeras:       req.RiwayatMinumanKeras,
			AseskepRwtAlkoholMempegaruhi: req.AlcoholMempegaruhiHidup,
			AseskepRwytPnyktDahulu:       req.RiwayatPenyakitDahulu,
		}

		update, erUpdate := su.kebidananRepository.OnUpdateAsesmenIntensiveRepository(req.Noreg, udpates1)

		if erUpdate != nil {
			return res, "Data gagal diupdate", erUpdate
		}

		// JIKA BERHASIL DI UPDATE
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRM, req.Tanggal)
		penyakitKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRM)

		mapper := su.kebidananMapper.ToAsesmenUlangPerawatanIntesiveMapper(update, penyakitKeluarga, riwayat)

		return mapper, "Data berhasil diupdate", nil

	}
}

// ON REPORT ASESMEN INTENSIVE ICU USE-CASE
func (su *kebidananUseCase) OnReportAsesmenIntensiveUseCase(noReg string, tanggal string, noRM string, kdBagian string) (res dto.ReportAsesmenIntensiveICU, err error) {
	// GET ASESMEN ULANG PERAWATAN INTENSIVE // ON GET ASESMEN // ULANG KEPERAWATAN
	asesmenUlang, _ := su.kebidananRepository.OnGetAsesmenUlangPerawatanIntensiveICURepository(noReg)
	penyakitKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)
	fisik, _ := su.rmeRepository.OnGetPemeriksaanFisikICURepository(noReg)
	mapperFisik := su.kebidananMapper.ToPemeriksaanFisikICUMapper(fisik)
	sistem, _ := su.kebidananRepository.OnGetReportPengkajianPersistemICURepository("ICU", noReg)
	mappersICU := su.kebidananMapper.ToMappingResponsePengkajianPersistemICU(sistem)
	riwayatPengobatan, _ := su.kebidananRepository.OnGetRiwayatPengobatanDirumahRepository(noReg)
	asuhan, _ := su.rmeRepository.GetDataAsuhanKeperawatanRepositoryV2(noReg, "ICU", "Closed")

	// OnReportAsesmenIntensiveUseCase
	// OnReportAsesmenIntensiveUseCase
	mapper := su.kebidananMapper.ToResponseICUMapper(asesmenUlang, penyakitKeluarga, mapperFisik, mappersICU, riwayatPengobatan, asuhan)

	// ReportAsesmenIGD struct {
	// 	AsesmenIGD            rme.AsesemenMedisIGD             `json:"asesmen"`
	// 	RiwayatPenyakitDahulu []rme.KeluhanUtama               `json:"riwayat"`
	// 	Alergi                []rme.DAlergi                    `json:"alergi"`
	// 	RencanaTindakLanjut   soap.RencanaTindakLanjutModel    `json:"tindak_lanjut"`
	// 	Diagnosa              []soap.DiagnosaResponse          `json:"diagnosa"`
	// 	PemeriksaanFisik      ResponseIGD                      `json:"fisik"`
	// 	VitalSign             soap.DVitalSignIGDDokter         `json:"vital_sign"`
	// 	Labor                 []his.ResHasilLaborTableLama     `json:"labor"`
	// 	Radiologi             []his.RegHasilRadiologiTabelLama `json:"radiologi"`
	// }

	return mapper, nil
}

func (su *kebidananUseCase) OnGetTriaseIGDDokterUseCase(noReg string, userID string, kdBagian string, pelayanan string) (res dto.ResposeTriaseIGDDokter, err error) {

	triase, ersr := su.kebidananRepository.OnGetTriaseIGDDokterRepository(noReg, userID, kdBagian, pelayanan)
	asesIGD, _ := su.kebidananRepository.OnGetAsesmenTriaseIGDRepository(noReg, userID, kdBagian, pelayanan)
	mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesIGD)

	if ersr != nil {
		return res, nil
	}

	return mapperData, nil
}

func (su *kebidananUseCase) OnSaveTriaseIGDDokterUseCase(noReg string, userID string, kdBagian string, pelayanan string, req dto.OnReqSaveTriaseIGDDokter) (message string, res dto.ResposeTriaseIGDDokter, err error) {
	// CEK TERLEBIH DAHULU APAKAH DATA SUDAH ADA
	asesmenTriase, err12 := su.kebidananRepository.OnGetAsesmenTriaseIGDRepository(noReg, userID, kdBagian, pelayanan)

	if err12 != nil || asesmenTriase.Noreg == "" {
		// SIMPAN DATA DISINI
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(noReg)

		times := time.Now()
		var asesmen = kebidanan.AsesmenTriaseIGD{
			InsertDttm:              times.Format("2006-01-02 15:04:05"),
			TglMasuk:                tanggal.Tanggal[0:10],
			InsertPc:                req.DevicesID,
			InsertUserId:            userID,
			KeteranganPerson:        req.Person,
			Pelayanan:               req.Pelayanan,
			KdBagian:                kdBagian,
			Noreg:                   req.Noreg,
			AseskepKehamilanDjj:     req.AseskepKehamilanDjj,
			AseskepAlasanMasuk:      req.AseskepAlasanMasuk,
			AseskepCaraMasuk:        req.AseskepCaraMasuk,
			AseskepPenyebabCedera:   req.AseskepPenyebabCedera,
			AseskepKehamilan:        req.AseskepKehamilan,
			AseskepKehamilanGravida: req.AseskepKehamilanGravida,
			AseskepKehamilanPara:    req.AseskepKehamilanPara,
			AseskepKehamilanAbortus: req.AseskepKehamilanAbortus,
			AseskepKehamilanHpht:    req.AseskepKehamilanHpht,
			AseskepKehamilanTtp:     req.AseskepKehamilanTtp,
			AseskepGangguanPerilaku: req.GangguanPerilaku,
		}

		_, ee12 := su.kebidananRepository.OnSaveAsesmenTriaseIGDRepository(asesmen)

		// INSERT TRIASE
		var triase = kebidanan.TriaseIGDDokter{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DevicesID,
			InsertUserId:   userID,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			Kategori:       req.Kategori,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			SkalaNyeri:     req.SkalaNyeri,
			SkalaNyeriP:    req.SkalaNyeriP,
			SkalaNyeriQ:    req.SkalaNyeriQ,
			SkalaNyeriR:    req.SkalaNyeriR,
			SkalaNyeriS:    req.SkalaNyeriS,
			SkalaNyeriT:    req.SkalaNyeriT,
			FlaccWajah:     req.FlaccWajah,
			FlaccKaki:      req.FlaccKaki,
			FlaccAktifitas: req.FlaccAktifitas,
			FlaccMenangis:  req.FlaccMenangis,
			FlaccBersuara:  req.FlaccBersuara,
			FlaccTotal:     req.FlaccTotal,
			SkalaTriaseIgd: req.SkalaTriase,
		}

		su.kebidananRepository.OnSaveTriaseIGDDokterRepository(triase)
		mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesmen)

		if ee12 != nil {
			return "Data gagal disimpan", mapperData, nil
		}

		return "Data berhasil disimpan", mapperData, nil

	} else {
		// UPDATE DATA TRIASE DOKTER IGD
		var asesmen = kebidanan.AsesmenTriaseIGD{
			AseskepKehamilanDjj:     req.AseskepKehamilanDjj,
			AseskepAlasanMasuk:      req.AseskepAlasanMasuk,
			AseskepCaraMasuk:        req.AseskepCaraMasuk,
			AseskepPenyebabCedera:   req.AseskepPenyebabCedera,
			AseskepKehamilan:        req.AseskepKehamilan,
			AseskepKehamilanGravida: req.AseskepKehamilanGravida,
			AseskepKehamilanPara:    req.AseskepKehamilanPara,
			AseskepKehamilanAbortus: req.AseskepKehamilanAbortus,
			AseskepKehamilanHpht:    req.AseskepKehamilanHpht,
			AseskepKehamilanTtp:     req.AseskepKehamilanTtp,
			AseskepGangguanPerilaku: req.GangguanPerilaku,
		}

		var triase = kebidanan.TriaseIGDDokter{
			SkalaNyeri:     req.SkalaNyeri,
			SkalaNyeriP:    req.SkalaNyeriP,
			SkalaNyeriQ:    req.SkalaNyeriQ,
			SkalaNyeriR:    req.SkalaNyeriR,
			SkalaNyeriS:    req.SkalaNyeriS,
			SkalaNyeriT:    req.SkalaNyeriT,
			FlaccWajah:     req.FlaccWajah,
			FlaccKaki:      req.FlaccKaki,
			FlaccAktifitas: req.FlaccAktifitas,
			FlaccMenangis:  req.FlaccMenangis,
			FlaccBersuara:  req.FlaccBersuara,
			FlaccTotal:     req.FlaccTotal,
			SkalaTriaseIgd: req.SkalaTriase,
		}

		_, er12 := su.kebidananRepository.OnUpdateTriaseIGDDokterRepository(noReg, userID, kdBagian, pelayanan, triase)

		if er12 != nil {
			mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesmen)
			return "Data gagal disimpan", mapperData, er12
		}

		su.kebidananRepository.OnUpdateAsesmenTriaseIGDRepository(noReg, userID, kdBagian, pelayanan, asesmen)

		mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesmen)

		return "Data berhasil diubah", mapperData, nil

	}
}

func (su *kebidananUseCase) OnGetReportTriaseIGDDokterUseCase(noReg string, noRm string, tanggal string) (res dto.ReportTriaseIGDDokterResponse) {
	// AMBIL DATA REPORT TRIASE IGD DOKTER
	// AsesmenTriaseIGD
	triase, _ := su.kebidananRepository.OnGetReportTriaseIGDDokterRepository(noReg)
	asesmen, _ := su.kebidananRepository.OnGetReportAsesmenTriaseIGDRepository(noReg)
	mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesmen)
	karyawan, _ := su.userRepository.GetKaryawanRepository(triase.InsertUserId)

	pemFisik, _ := su.kebidananRepository.OnGetPemeriksaanFisikTriaseIGDRepository(noReg)
	tandaVital, _ := su.soapUsecase.OnGetTandaReportVitalIGDDokterUserCase("IGD001", "Dokter", noReg)
	pasien, _ := su.userRepository.OnGetDProfilePasienRepository(noRm)

	riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(noRm, tanggal)
	riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRm)
	keluUtama, _ := su.rmeRepository.GetAsesemenKeluhanUtamaIGDDokter("IGD001", noReg, "Dokter")

	mapperRiwayat := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, keluUtama)
	report := su.kebidananMapper.ToReseponseMapperTriaseIGDDokter(mapperData, karyawan, pemFisik, tandaVital, mapperRiwayat, pasien)

	return report
}

func (su *kebidananUseCase) OnGetReportTriasePonekUseCase(noReg string, noRm string, tanggal string) (res dto.ReportTriaseIGDDokterResponse) {
	// AMBIL DATA REPORT TRIASE IGD DOKTER
	triase, _ := su.kebidananRepository.OnGetReportTriaseIGDDokterRepository(noReg)
	asesmen, _ := su.kebidananRepository.OnGetReportAsesmenTriaseIGDRepository(noReg)
	mapperData := su.kebidananMapper.ToResponseTriaseIGDDokter(triase, asesmen)
	karyawan, _ := su.userRepository.GetKaryawanRepository(triase.InsertUserId)

	// OnGetPemeriksaanFisikTriaseIGDRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	pemFisik, _ := su.kebidananRepository.OnGetPemeriksaanFisikTriaseIGDRepository(noReg)
	// OnGetTandaReportVitalIGDDokterUserCase
	tandaVital, _ := su.soapUsecase.OnGetTandaReportVitalIGDDokterUserCase("PONEK", "Perawat", noReg)

	// riwayatDahulu, _ := rh.RMERepository.OnGetRiwayatPenyakitDahulu(noRm, tanggal)
	riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(noRm, tanggal)
	riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRm)
	pasien, _ := su.userRepository.OnGetDProfilePasienRepository(noRm)
	keluUtama, _ := su.rmeRepository.GetAsesemenKeluhanUtamaIGDDokter("PONEK", noReg, "Perawat")

	mapperRiwayat := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, keluUtama)

	report := su.kebidananMapper.ToReseponseMapperTriaseIGDDokter(mapperData, karyawan, pemFisik, tandaVital, mapperRiwayat, pasien)

	return report
}

func (su *kebidananUseCase) OnGetReportPengkajianAwalMedisDokterUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res dto.ReportPengkajianAwalDokterResponse, err error) {
	// GET PEMERIKSAAN FISIK PENGKAJIAN AWAL MEDIS DOKTER
	pemFisik, _ := su.kebidananRepository.OnGetPemeriksaanFisikMedisDokter(noReg)
	vital, _ := su.kebidananRepository.GetTandaVitalIGDDokterRepository(kdBagian, person, noReg, pelayanan)
	asesmen, _ := su.kebidananRepository.GetAsesmenAwalMedisRawatInapReportDokter(noReg, kdBagian, pelayanan)
	dahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(noReg, tanggal)
	keluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)
	diagnosa, _ := su.soapRepository.GetDiagnosaRepositoryReportBangsal(noReg, kdBagian)

	labor, _ := su.hisUseCase.HistoryLaboratoriumUsecaseV2(noReg)
	radiologi, _ := su.hisUseCase.HistoryRadiologiUsecaseV2(noReg)

	su.logging.Info("DATA DOKTER")
	su.logging.Info(asesmen)

	mapper := su.kebidananMapper.ToResponseMapperPengkajianAwalDokter(pemFisik, vital, asesmen, dahulu, keluarga, labor, radiologi, diagnosa)

	return mapper, nil
}

func (su *kebidananUseCase) OnGetReportPengkajianAwalPerawatUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res dto.ReportPengkajianKeperawatanResponse, err error) {
	pemFisik, _ := su.kebidananRepository.OnGetPemeriksaanFisikPerawatRepository(noReg, kdBagian)

	vital, _ := su.kebidananRepository.GetTandaVitalPerawatRawatInapRepository(kdBagian, noReg)
	asesmen, _ := su.kebidananRepository.GetAsesmenAwalRawatInapRepository(noReg, kdBagian)

	fisik := su.rmeMapper.ToMapperPemeriksaanFisikReponse(pemFisik)
	pengobatan, _ := su.kebidananRepository.OnGetRiwayatPengobatanDirumahRepository(noReg)

	keluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)
	diagnosa, _ := su.soapRepository.GetDiagnosaRepositoryReportBangsal(noReg, kdBagian)

	labor, _ := su.hisUseCase.HistoryLaboratoriumUsecaseV2(noReg)
	radiologi, _ := su.hisUseCase.HistoryRadiologiUsecaseV2(noReg)

	nutrisi, _ := su.kebidananRepository.OnGetPengkajianNutrisiRepository(noReg)
	asuhan, _ := su.rmeRepository.GetDataAsuhanKeperawatanRepositoryV2(noReg, kdBagian, "Closed")

	fungsional, _ := su.kebidananRepository.OnGetPengkajianFungsionalRepository(noReg)
	maperFungsional := su.kebidananMapper.ToReponseFungsionalMapper(fungsional)

	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(noRM, tanggal)

	mapper := su.kebidananMapper.ToResponseMapperPengkajianKeperawatan(fisik, keluarga, labor, radiologi, diagnosa, vital, asesmen, pengobatan, asuhan, maperFungsional, riwayatPerawat, nutrisi)

	return mapper, nil
}

// === //
func (su *kebidananUseCase) OnGetPengkajianAwalAssesmenAnakUseCase(userID string, kdBagian string, req dto.OnGetPengkajianAwal) (message string, res dto.ResponsePengkajianAnak, err error) {

	perawat, _ := su.soapRepository.OnGetPengkajianAnakRepository(req.NoReg, kdBagian)

	// MAPPING PENKJIAN ANAK
	mapperPerawat := su.kebidananMapper.ToResponsePengkajianAnak(perawat)
	return message, mapperPerawat, nil
}

func (su *kebidananUseCase) OnSavePengkajianPersistemIcuICUUsecase(userID string, modulID string, person string, noReg string, req dto.RequestPengkajianPersistemICU) (res dto.ResponsePengkajianPersistemICU, message string, err error) {
	data, err123 := su.kebidananRepository.OnGetPengkajianPersistemICURepository(userID, modulID, person, noReg)
	// DATA PENGJIAN ACU
	if err123 != nil || data.Noreg == "" {
		data, err1 := su.kebidananRepository.OnSavePengkajianPersistemICURepository(userID, modulID, person, noReg, req)

		if err1 != nil {
			mapper := su.kebidananMapper.ToMappingResponsePengkajianPersistemICU(data)
			return mapper, "Data gagal disimpan", nil
		}

		return res, "Data berhasil disimpan", nil
	} else {
		// UPDATE PENGKAJIAN PERSISTEM ICU
		udpate, er12 := su.kebidananRepository.OnUpdatePengkajianPersistemICURepository(userID, data.KdBagian, person, noReg, req)

		if er12 != nil {
			mapper := su.kebidananMapper.ToMappingResponsePengkajianPersistemICU(udpate)
			return mapper, "Data gagal diubah", nil
		}

		return res, "Data berhasil diubah", nil

	}
}
