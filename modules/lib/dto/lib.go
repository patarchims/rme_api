package dto

type (
	ReqResikoJatuh struct {
		KategoriUmur string `json:"kategori_umur"`
		Jenis        string `json:"jenis"`
	}
	GetLibInput struct {
		Title string `uri:"title" binding:"required"`
	}

	GetKategoriBankData struct {
		Kategori string `uri:"kategori" binding:"required"`
	}

	JadwalDokterResult struct {
		BukaPoli   string `json:"buka"`
		TutupPoli  string `json:"tutup"`
		NamaDokter string `json:"namaDokter"`
		Spesialis  string `json:"spesialis"`
		NamaPoli   string `json:"namaPelayanan"`
		QuotaSun   int    `json:"quotaSun"`
		QuotaMon   int    `json:"quotaMon"`
		QuotaTue   int    `json:"quotaTue"`
		QuotaWed   int    `json:"quotaWed"`
		QuotaThu   int    `json:"quotaThu"`
		QuotaFri   int    `json:"quotaFri"`
		QuotaSat   int    `json:"quotaSat"`
		Senin      string `json:"senin"`
		Selasa     string `json:"selasa"`
		Rabu       string `json:"rabu"`
		Kamis      string `json:"kamis"`
		Jumat      string `json:"jumat"`
		Sabtu      string `json:"sabtu"`
	}

	InventoryResult struct {
		NamaObat    string `json:"namaObat"`
		Kemasan     string `json:"kemasan"`
		Satuan      string `json:"satuan"`
		HargaJual   int    `json:"hargaJual"`
		HargaTelkom int    `json:"hargaTelkom"`
		Keterangan  string `json:"keterangan"`
		Maximal     int    `json:"maximal"`
	}

	GetTaripRad struct {
		KodeBagian string `json:"kode_bagian"`
		Debitur    string `json:"debitur"`
		Kode       string `json:"kode"`
	}

	GetTaripRadV2 struct {
		KodeBagian string `json:"kode_bagian" validate:"required"`
		Debitur    string `json:"debitur" validate:"required"`
		Kode       string `json:"kode" validate:"required"`
	}
)
