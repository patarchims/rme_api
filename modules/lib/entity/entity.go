package entity

import (
	"hms_api/modules/lib"
	"hms_api/modules/lib/dto"
)

// UserUseCase
type LibUseCase interface {
}

// UserRepository
type LibRepository interface {
	OnGetKPelayananRepositoryByKdPelayanan(kode string) (res lib.KPelayanan, err error)
	GetKPelayananRepository() (res []lib.KPelayanan, err error)
	OnGetKpelayananRepositoryByKode(kdBagian string) (res lib.KPelayanan, err error)
	GetAllKICD9Repository() (res []lib.KICD9, err error)

	GetVersion() (res lib.Version, err error)
	GetAllKVitalSign() (res []lib.KVitalSign, err error)
	GetAllKICD10() (res []lib.KICD10, err error)
	GetAllSoapPasien() (res []lib.DcpptSoapPasien, err error)
	GetJadwalDokter() (res []lib.KJadwalDokter, err error)
	GetDebitur() (res []lib.KDebitur, err error)
	GetDosis() (res []lib.KFARDosis, err error)
	GetGolongan() (res []lib.KFARGolongan, err error)
	GetKemasan() (res []lib.KFARKemasan, err error)
	GetKomposisi() (res []lib.KFARKomposisi, err error)
	GetPenggunaan() (res []lib.KFarPenggunaan, err error)
	GetSatuan() (res []lib.KFarSatuan, err error)
	GetSuku() (res []lib.KSuku, err error)
	GetProcedure() (res []lib.KProcedure, err error)
	GetRuangKasus() (res []lib.KRuangKasus, err error)
	GetKRuangPerawatan() (res []lib.KRuangPerawatan, err error)
	GetInventory() (res []lib.KInventory, err error)
	GetBankData(kategori string) (res []lib.KBankData, err error)
	GetDataICD10(kode string) (res []lib.KIcd10, err error)
	GetICD10(kode string, limit int, offset int) (res []lib.KIcd10, err error)
	GetAllICD() (res []lib.KIcd10, err error)
	// =============== GET DOKTER SPESIALIS ====== //
	GetDokterSpesialis() (res []lib.DokterSpesialisModel, err error)
	GetKTaripDokterRepository() (res []lib.DokterSpesialisModel, err error)
	GetProcedureByBagian(bagian string) (res []lib.KProcedureModel, err error)
	// GET PROCEDURE FISIOTERAPI
	GetProcedureFisioTerapi(kdBagian string) (res []lib.KFisioterapi, err error)
	GetRadiologi() (res []lib.TaripFisioTerapi, err error)
	GetTaripRadiologi(req dto.GetTaripRad) (res lib.DetailTaripRadioLogi, err error)
	GetTaripRadiologiV2(req dto.GetTaripRadV2) (res lib.DetailTaripRadioLogi, err error)

	GetFaktorResikoJatuhRespository(kategori string, jenis string) (res []lib.KFartorResiko, err error)
	GetAllInventoryObatRepository() (res []lib.KInventoryObat, err error)
}

// Mapper“
type ILibMapper interface {
	ToJadwalDokterMapper(data []lib.KJadwalDokter) (value []dto.JadwalDokterResult)
	ToInventoryMapper(data []lib.KInventory) (value []dto.InventoryResult)
}
