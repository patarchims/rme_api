package handler

import (
	"hms_api/modules/lib/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (dh *LibHandler) GetDokterSpesialisFiberHandler(c *fiber.Ctx) error {
	data, err := dh.LibRepository.GetKTaripDokterRepository()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetResikoJatuhPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResikoJatuh)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	value, err := dh.LibRepository.GetFaktorResikoJatuhRespository(payload.KategoriUmur, payload.Jenis)

	if err != nil {
		response := helper.APIResponseFailure("Tidak dapat menemukan library", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetReasesmenResikoJatuhAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResikoJatuh)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	value, err := dh.LibRepository.GetFaktorResikoJatuhRespository(payload.KategoriUmur, payload.Jenis)

	if err != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetKInventoryObatFiberHandler(c *fiber.Ctx) error {
	data, err := dh.LibRepository.GetAllInventoryObatRepository()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) OnGetAllKICD9FiberHandler(c *fiber.Ctx) error {

	data, err := dh.LibRepository.GetAllKICD9Repository()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetAllKInventoryObatFiberHandler(c *fiber.Ctx) error {
	data, err := dh.LibRepository.GetAllInventoryObatRepository()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	obat := []string{}

	for _, V := range data {
		obat = append(obat, V.Namaobat)
	}

	response := helper.APIResponse("Ok", http.StatusOK, obat)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetVersionFiberHandler(c *fiber.Ctx) error {
	value, err := dh.LibRepository.GetVersion()
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}
