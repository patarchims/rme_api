package handler

import (
	"hms_api/modules/lib/dto"
	"hms_api/modules/lib/entity"
	"hms_api/pkg/helper"
	"strconv"

	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type LibHandler struct {
	LibUseCase    entity.LibUseCase
	LibRepository entity.LibRepository
	LibMapper     entity.ILibMapper
	Logging       *logrus.Logger
}

func (dh *LibHandler) GeneratePdf(c *gin.Context) {

	response := helper.APIResponse("Ok", http.StatusOK, "listDebitur")
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetLibrary(c *gin.Context) {
	var input dto.GetLibInput

	err := c.ShouldBindUri(&input)
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure("Tidak dapat menemukan library", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	switch input.Title {
	case "kpelayanan":
		// TAMPILKAN KPELAYANAN
		listPoli, err := dh.LibRepository.GetKPelayananRepository()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listPoli) == 0 {
			dh.Logging.Info("Data Kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "version":
		value, err := dh.LibRepository.GetVersion()
		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		response := helper.APIResponse("OK", http.StatusOK, value)
		dh.Logging.Info(response)
		c.JSON(http.StatusOK, response)
		return
	case "kvital-sign":
		listPoli, err := dh.LibRepository.GetAllKVitalSign()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listPoli) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "kicd10":
		listICD, err := dh.LibRepository.GetAllKICD10()
		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listICD) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listICD)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "debitur":
		listDebitur, err := dh.LibRepository.GetGolongan()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listDebitur) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listDebitur)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "dosis":
		listJadwalDokter, err := dh.LibRepository.GetDosis()
		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listJadwalDokter) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listJadwalDokter)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "golongan":
		listDebitur, err := dh.LibRepository.GetGolongan()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listDebitur) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listDebitur)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return
	case "procedure":
		listPoli, err := dh.LibRepository.GetProcedure()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(listPoli) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info("OK")
		c.JSON(http.StatusOK, response)
		return

	default:
		dh.Logging.Info("Data tidak ditemukan")
		response := helper.APIResponseFailure("Data tidak dapat ditemukan", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

}

func (dh *LibHandler) GetLibraryV2(c *fiber.Ctx) error {

	var title = c.Params("title")

	switch title {
	case "kpelayanan":
		// TAMPILKAN KPELAYANAN
		listPoli, err := dh.LibRepository.GetKPelayananRepository()

		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			dh.Logging.Info(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(listPoli) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			dh.Logging.Info(response)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)

	case "version":
		value, err := dh.LibRepository.GetVersion()
		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			dh.Logging.Error(response)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, value)
		dh.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)

	case "kvital-sign":
		listPoli, err := dh.LibRepository.GetAllKVitalSign()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		if len(listPoli) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)

	case "kicd10":
		listICD, err := dh.LibRepository.GetAllKICD10()
		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		if len(listICD) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, listICD)
		dh.Logging.Info("OK")
		return c.Status(fiber.StatusCreated).JSON(response)

	case "debitur":
		listDebitur, err := dh.LibRepository.GetGolongan()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(listDebitur) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, listDebitur)
		dh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)

	case "dosis":
		listJadwalDokter, err := dh.LibRepository.GetDosis()
		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		if len(listJadwalDokter) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)
		}

		response := helper.APIResponse("Ok", http.StatusOK, listJadwalDokter)
		dh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)
	case "golongan":
		listDebitur, err := dh.LibRepository.GetGolongan()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(listDebitur) == 0 {
			dh.Logging.Info("Data kosong")
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)
		}

		response := helper.APIResponse("Ok", http.StatusOK, listDebitur)
		dh.Logging.Info("OK")
		return c.JSON(response)

	case "procedure":
		listPoli, err := dh.LibRepository.GetProcedure()

		if err != nil {
			dh.Logging.Error(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		if len(listPoli) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			dh.Logging.Info(response)
			return c.Status(fiber.StatusAccepted).JSON(response)
		}

		response := helper.APIResponse("Ok", http.StatusOK, listPoli)
		dh.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)

	default:
		response := helper.APIResponseFailure("Data tidak dapat ditemukan", http.StatusCreated)
		dh.Logging.Info(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}
}

// GET ALL ICD10
func (dh *LibHandler) GetICDAll(c *gin.Context) {
	data, err := dh.LibRepository.GetAllICD()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetICDAllV2(c *fiber.Ctx) error {
	data, err := dh.LibRepository.GetAllICD()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

// WITH PAGINATION
func (dh *LibHandler) GetICD(c *gin.Context) {
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "10"))
	keyword := c.Query("code")
	offset := (page - 1) * limit
	data, err := dh.LibRepository.GetICD10(keyword, limit, offset)

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	dh.Logging.Info("OK")
	response := helper.APIResponse("OK", http.StatusOK, data)
	c.JSON(http.StatusOK, response)

}

func (dh *LibHandler) GetICDV2(c *fiber.Ctx) error {

	keyword := c.Query("code")
	offset := (1 - 1) * 10
	data, err := dh.LibRepository.GetICD10(keyword, 10, offset)

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	dh.Logging.Info("OK")
	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (dh *LibHandler) InsertICD(c *gin.Context) {
}

func (dh *LibHandler) InsertICDV2(c *fiber.Ctx) error {
	response := helper.APIResponse("OK", http.StatusOK, "data")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetBankData(c *gin.Context) {
	var input dto.GetKategoriBankData

	err := c.ShouldBindUri(&input)
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	value, err := dh.LibRepository.GetBankData(input.Kategori)

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure("Tidak dapat menemukan library", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(value) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, value)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetBankDataV2(c *fiber.Ctx) error {
	var kategori = c.Params("kategori")

	value, err := dh.LibRepository.GetBankData(kategori)

	if err != nil {
		response := helper.APIResponseFailure("Tidak dapat menemukan library", http.StatusCreated)
		dh.Logging.Error(err)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(value) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, value)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetGolongan(c *gin.Context) {
	listDebitur, err := dh.LibRepository.GetGolongan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listDebitur) == 0 {
		dh.Logging.Info("OK")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listDebitur)
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetDebitur(c *gin.Context) {
	listDebitur, err := dh.LibRepository.GetDebitur()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listDebitur) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, listDebitur)
	c.JSON(http.StatusOK, response)
}

// GET DOKTER SPESIALIS
func (dh *LibHandler) GetDokterSpesialis(c *gin.Context) {
	data, err := dh.LibRepository.GetDokterSpesialis()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(data) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

// @Summary		Jadwal Pasien
// @Description	Melihat jadwal dokter
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]dto.JadwalDokterResult
// @Router			/jadwal-dokter/ [get]
func (dh *LibHandler) GetJadwalDokter(c *gin.Context) {
	listJadwalDokter, err := dh.LibRepository.GetJadwalDokter()
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	m := map[string]any{}
	if len(listJadwalDokter) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	mapper := dh.LibMapper.ToJadwalDokterMapper(listJadwalDokter)
	m["list"] = mapper

	response := helper.APIResponse("Ok", http.StatusOK, m)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetJadwalDokterV2(c *fiber.Ctx) error {
	listJadwalDokter, err := dh.LibRepository.GetJadwalDokter()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	m := map[string]any{}
	if len(listJadwalDokter) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	mapper := dh.LibMapper.ToJadwalDokterMapper(listJadwalDokter)
	m["list"] = mapper

	response := helper.APIResponse("Ok", http.StatusOK, m)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetDosis(c *gin.Context) {
	listJadwalDokter, err := dh.LibRepository.GetDosis()
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listJadwalDokter) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listJadwalDokter)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

// @Summary		Penggunaan Obat
// @Description	Data penggunaan obat
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]lib.KFARKomposisi
// @Router			/penggunaan/ [get]
func (dh *LibHandler) Penggunaan(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetKomposisi()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) PenggunaanV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetKomposisi()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusCreated).JSON(response)
}

// @Summary		Data Satuan
// @Description	Data master satuan
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]lib.KFarSatuan
// @Router			/satuan/ [get]
func (dh *LibHandler) GetSatuan(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetSatuan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetSatuanV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetSatuan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Data Suku
// @Description	Data master suku
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]lib.KSuku
// @Router			/suku/ [get]
func (dh *LibHandler) GetSuku(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetSuku()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetSukuV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetSuku()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Get Komposisi
// @Description	Get data Komposisi
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	 []lib.KFARKomposisi
// @Success 500 {object} error
// @Router			/komposisi/ [get]
func (dh *LibHandler) GetKomposisi(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetKomposisi()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetKomposisiV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetKomposisi()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Get K Ruang Perawatan
// @Description	Get data KRuang Perawatan
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	 []lib.KRuangPerawatan
// @Success 500 {object} error
// @Router			/getKRuangPerawatan/ [get]
func (dh *LibHandler) GetKRuangPerawat(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetKRuangPerawatan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data Kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetKRuangPerawatV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetKRuangPerawatan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data Kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Data Ruang Kasus
// @Description	Data master ruang kasus
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]lib.KRuangKasus
// @Router			/getRuangKasus/ [get]
func (dh *LibHandler) GetRuangKasus(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetRuangKasus()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetRuangKasusV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetRuangKasus()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetProcedure(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetProcedure()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

// @Summary		Data Iventory
// @Description	Data master inventory
// @Tags			SOAP
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	 []dto.InventoryResult
// @Router			/inventory/ [get]
func (dh LibHandler) GetInventory(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetInventory()

	if err != nil {
		dh.Logging.Error(err.Error())
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	m := map[string]any{}
	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	mapper := dh.LibMapper.ToInventoryMapper(listPoli)
	m["list"] = mapper

	response := helper.APIResponse("Ok", http.StatusOK, m)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh LibHandler) GetInventoryV2(c *fiber.Ctx) error {
	listPoli, err := dh.LibRepository.GetInventory()

	if err != nil {
		dh.Logging.Error(err.Error())
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	m := map[string]any{}
	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	mapper := dh.LibMapper.ToInventoryMapper(listPoli)
	m["list"] = mapper

	response := helper.APIResponse("Ok", http.StatusOK, m)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetAllKPelayanan(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetKPelayananRepository()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetVersion(c *gin.Context) {
	value, err := dh.LibRepository.GetVersion()
	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, value)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetKVitalSign(c *gin.Context) {
	listPoli, err := dh.LibRepository.GetAllKVitalSign()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listPoli) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listPoli)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetKICD10(c *gin.Context) {
	listICD, err := dh.LibRepository.GetAllKICD10()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listICD) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listICD)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (dh *LibHandler) GetSoapPasien(c *gin.Context) {
	listSoap, err := dh.LibRepository.GetAllSoapPasien()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listSoap) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listSoap)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (dh *LibHandler) GetSoapPasienV2(c *fiber.Ctx) error {
	listSoap, err := dh.LibRepository.GetAllSoapPasien()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(listSoap) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, listSoap)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetKemasan(c *gin.Context) {
	listSoap, err := dh.LibRepository.GetKemasan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(listSoap) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, listSoap)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetKemasanV2(c *fiber.Ctx) error {
	listSoap, err := dh.LibRepository.GetKemasan()

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(listSoap) == 0 {
		dh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, listSoap)
	dh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetProcedurByBagian(c *gin.Context) {
	idKec := c.Param("kelompok")
	list, err := dh.LibRepository.GetProcedureByBagian(idKec)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *LibHandler) GetProcedurByBagianV2(c *fiber.Ctx) error {
	var idKec = c.Params("kelompok")
	list, err := dh.LibRepository.GetProcedureByBagian(idKec)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *LibHandler) GetDesProcedure(c *gin.Context) {
	idKec := c.Param("kelompok")

	if idKec != "RAD" {
		list, err := dh.LibRepository.GetProcedureFisioTerapi(idKec)

		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(list) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, list)
		c.JSON(http.StatusOK, response)
	} else {
		list, err := dh.LibRepository.GetRadiologi()

		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		if len(list) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			c.JSON(http.StatusAccepted, response)
			return
		}

		response := helper.APIResponse("Ok", http.StatusOK, list)
		c.JSON(http.StatusOK, response)
	}
}

func (dh *LibHandler) GetDesProcedureV2(c *fiber.Ctx) error {
	idKec := c.Params("kelompok")

	if idKec != "RAD" {
		list, err := dh.LibRepository.GetProcedureFisioTerapi(idKec)

		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(list) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, list)
		return c.Status(fiber.StatusOK).JSON(response)
	} else {
		list, err := dh.LibRepository.GetRadiologi()

		if err != nil {
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if len(list) == 0 {
			response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
			return c.Status(fiber.StatusAccepted).JSON(response)

		}

		response := helper.APIResponse("Ok", http.StatusOK, list)
		return c.Status(fiber.StatusOK).JSON(response)
	}
}

func (dh *LibHandler) GetDetailRadiologi(c *gin.Context) {
	payload := new(dto.GetTaripRad)
	err := c.ShouldBind(&payload)

	if err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Detail pasien", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	//============== GET KODE BAGIAN
	list, err := dh.LibRepository.GetTaripRadiologi(*payload)

	dh.Logging.Info(list)

	if err != nil {
		dh.Logging.Error(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	dh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (dh *LibHandler) GetDetailRadiologiV2(c *fiber.Ctx) error {
	payload := new(dto.GetTaripRadV2)

	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	//============== GET KODE BAGIAN
	list, errs := dh.LibRepository.GetTaripRadiologiV2(*payload)

	dh.Logging.Info(list)

	if errs != nil {
		dh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	dh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)

}
