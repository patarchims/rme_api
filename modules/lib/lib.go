package lib

import (
	"time"
)

type (
	KBankData struct {
		KdBag       string `json:"kdBagian"`
		Kategori    string `json:"kategori"`
		KdDeskripsi string `json:"kdDeskripsi"`
		Deskripsi   string `json:"deskripsi"`
	}

	KFartorResiko struct {
		KodeFaktor         string               `gorm:"primaryKey:KodeFaktor" json:"kode_faktor"`
		NamaFaktor         string               `json:"nama_faktor"`
		KategoriUmur       string               `json:"kategori_umur"`
		Jenis              string               `json:"jenis"`
		No_Urut            int                  `json:"no_urut"`
		KResikoJatuhPasien []KResikoJatuhPasien `gorm:"foreignKey:KodeFaktor" json:"resiko_jatuh"`
	}

	KResikoJatuhPasien struct {
		KodeResiko     string `json:"kode_resiko"`
		KodeFaktor     string `json:"kode_faktor"`
		KategoriFaktor string `json:"kategori_faktor"`
		No_Urut        int    `json:"no_urut"`
		Skor           int    `json:"skor"`
	}

	KDebitur struct {
		Kode       string  `json:"kode"`
		Debitur    string  `json:"debitur"`
		Alamat     string  `json:"alamat"`
		Telp       string  `json:"telp"`
		Fax        string  `json:"fax"`
		Email      string  `json:"email"`
		Nomor      string  `json:"nomor"`
		Masa       string  `json:"masa"`
		Lingkup    string  `json:"lingkup"`
		Jangka     string  `json:"jangka"`
		Denda      string  `json:"denda"`
		Verifikasi string  `json:"verifikasi"`
		Profit     float32 `json:"profit"`
		Adm        float32 `json:"adm"`
		Honor      float32 `json:"honor"`
		Visite     float32 `json:"visite"`
		Lab        float32 `json:"lab"`
		Penmed     float32 `json:"penmed"`
		Kamar      float32 `json:"kamar"`
		Tahun      string  `json:"tahun"`
		Bagi       float32 `json:"bagi"`
		HonorDrOk  float32 `json:"honorDrOK"`
		Lainnya    float32 `json:"lainnya"`
	}

	DresumeMedik struct {
		InsertDttm       time.Timer `json:"insertDttm"`
		UpdDttm          time.Timer `json:"updateDttm"`
		InsertUserID     string     `json:"interUserID"`
		InsertPc         string     `json:"insertPc"`
		TglMasuk         time.Timer `json:"tglMasuk"`
		TglKeluar        time.Timer `json:"tglKeluar"`
		JamCheckOut      time.Timer `json:"jamCheckOut"`
		Id               string     `json:"id"`
		Noreg            string     `json:"noreg"`
		KdDokter         string     `json:"kdDokter"`
		Resep            string     `json:"resep"`
		PrimaryIcd       string     `json:"primaryICD"`
		SecondaryIcd     string     `json:"secondary"`
		Prosedur         string     `json:"prosedur"`
		Kasus            string     `json:"kasus"`
		KasusMeninggal   string     `json:"kasusMeninggal"`
		Triase           string     `json:"triase"`
		JenisKasus       string     `json:"jenisKasus"`
		TindakLanjut     string     `json:"tindakLanjut"`
		ResponseIgd      string     `json:"responseIgd"`
		TipeKunjungan    string     `json:"tipeKunjungan"`
		GolonganPenyakit string     `json:"golonganPenyakit"`
		Emergency        string     `json:"emergency"`
		Note             string     `json:"note"`
	}

	DcpptSoapPasien struct {
		InsertUserId   string `json:"insertUserID"`
		InsertPc       string `json:"insertPC"`
		TglMasuk       string `json:"tglMasuk"`
		TglKeluar      string `json:"tglKeluar"`
		JamCheckOut    string `json:"jamCheckOut"`
		KdBagian       string `json:"kdbagian"`
		Noreg          string `json:"noreg"`
		KdPpa          string `json:"kdPPA"`
		KdDpjp         string `json:"kdDpjp"`
		Subjectif      string `json:"subjectif"`
		Objectif       string `json:"objectif"`
		Asesmen        string `json:"asesmen"`
		Plan           string `json:"plan"`
		PpaPascaBedah  string `json:"ppaPascaBedah"`
		IcdKerja       string `json:"icdKerja"`
		IcdDeferensial string `json:"icdDeferensial"`
		Icd9           string `json:"icd9"`
		Note           string `json:"note"`
	}

	KPelayanan struct {
		KdBag         string `json:"kd_bagian"`
		Bagian        string `json:"bagian"`
		Pelayanan     string `json:"pelayanan"`
		MapingBpjs    string `json:"maping_bpjs"`
		AsesmenActive bool   `json:"asesmen_active"`
	}

	Version struct {
		AndroidVersion float32 `gorm:"column:android_version;" json:"android_version"`
		IosVersion     float32 `gorm:"column:ios_version;" json:"ios_version"`
		WindowsVersion float32 `gorm:"column:windows_version;" json:"windows_version"`
	}

	KVitalSign struct {
		Kode      string `json:"kode"`
		JenisGrup string `json:"jenisGrup"`
		Deskripsi string `json:"deskripsi"`
	}

	KICD10 struct {
		S1          string `gorm:"column:s1;" json:"s1"`
		Code        string `gorm:"column:code;" json:"code"`
		Code2       string `gorm:"primaryKey:Code2" json:"code2"`
		Description string `gorm:"column:description;" json:"description"`
		Inpatient   string `gorm:"column:inpatient;" json:"inpatient"`
		Outpatient  string `gorm:"column:outpatient;" json:"outpatient"`
	}

	// KFAR DOSIS
	KFARDosis struct {
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
	}

	// KFAR GOLONGAN
	KFARGolongan struct {
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
	}

	KFARKemasan struct {
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
	}

	KFARKomposisi struct {
		InDttm    time.Time `json:"inDttm"`
		Kode      string    `json:"kode"`
		Deskripsi string    `json:"deskripsi"`
	}

	KFarPenggunaan struct {
		InDttm    time.Time `json:"inDttm"`
		Kode      string    `json:"kode"`
		Deskripsi string    `json:"deskripsi"`
	}

	KFarSatuan struct {
		InDttm    time.Time `json:"inDttm"`
		Kode      string    `json:"kode"`
		Deskripsi string    `json:"deskripsi"`
	}

	KinaCBG struct {
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
	}

	KTaripSIMRS struct {
		KdDebitur string  `json:"kdDebitur"`
		KdBagian  string  `json:"kdBagian"`
		Kode      string  `json:"kode"`
		KdKelas   string  `json:"kdKelas"`
		Tarip     float32 `json:"tarip"`
	}

	KInventory struct {
		EbBud              string  `json:"ebBud"`
		IntService         string  `json:"intService"`
		Diskon             string  `json:"diskon"`
		Kondisi            string  `json:"kondisi"`
		Maximal            float32 `json:"maximal"`
		Satuan2            string  `json:"satuan2"`
		Jumlah             string  `json:"jumlah"`
		Aso                int     `json:"aso"`
		KodeObat           string  `json:"kodeObat"`
		Namaobat           string  `json:"namaObat"`
		Ukuran             string  `json:"ukuran"`
		Kemasan            string  `json:"kemasan"`
		Satuan             string  `json:"satuan"`
		Penerimaan         float32 `json:"penerimaan"`
		Pengeluaran        float32 `json:"pengeluaran"`
		PenerimaanFarmasi  float32 `json:"penerimaanFarmasi"`
		PengeluaranFarmasi float32 `json:"pengeluaranFarmasi"`
		SaldoFarmasi       float32 `json:"saldoFarmasi"`
		Minimum            float32 `json:"minimum"`
		JenisBarang        string  `json:"jenisBarang"`
		JenisObat          string  `json:"jenisObat"`
		Gen                string  `json:"gen"`
		Hargajual          float32 `json:"hargaJual"`
		Hargaakses         float32 `json:"hargaAkses"`
		Hargatelkom        float32 `json:"hargaTelkom"`
		Otc                string  `json:"otc"`
		Keterangan         string  `json:"keterangan"`
		Barcode            string  `json:"barcode"`
		Indikasi           string  `json:"indikasi"`
		Kodeprin           string  `json:"kodeprin"`
		Principal          string  `json:"principal"`
		Komposisi          string  `json:"komposisi"`
		Komposisi2         string  `json:"komposisi2"`
		Bpjs               string  `json:"bpjs"`
		Ppra               string  `json:"ppra"`
	}

	KJadwalDokter struct {
		OpenQueBeforeMinute int    `json:"OpenQueBeforeMinute"`
		CloseQueAfterMinute int    `json:"CloseQueAfterMinute"`
		EstimasiPerPasien   int    `json:"estimasiPerPasien"`
		Minggu              string `json:"minggu"`
		Senin               string `json:"senin"`
		Selasa              string `json:"selasa"`
		Rabu                string `json:"rabu"`
		Kamis               string `json:"kamis"`
		Jumat               string `json:"jumat"`
		Sabtu               string `json:"sabtu"`
		Sun                 int    `json:"sun"`
		Mon                 int    `json:"mon"`
		Tue                 int    `json:"tue"`
		Wed                 int    `json:"wed"`
		Thu                 int    `json:"thu"`
		Fri                 int    `json:"int"`
		Sat                 int    `json:"sat"`
		BukaPoli            string `json:"bukaPoli"`
		TutupPoli           string `json:"tutupPoli"`
		UrlQrcode           string `json:"urlQRcode"`
		Iddokter            string `json:"iddokter"`
		KodeSpesialisasi    string `json:"kodeSpesialisasi"`
		MapingAntrol        string `json:"mapingAntrol"`
		MapingPoli          string `json:"mapingPoli"`
		StatusDokter        string `json:"statusDokter"`
		Drahli              string `json:"drahli"`
		QuotaPasien         int    `json:"quotaPasien"`
		QuotaPasienSun      int    `json:"quotaPasienSun"`
		QuotaPasienMon      int    `json:"quotaPasienMon"`
		QuotaPasienTue      int    `json:"quotaPasienTue"`
		QuotaPasienWed      int    `json:"quotaPasienWed"`
		QuotaPasienThu      int    `json:"quotaPasienThu"`
		QuotaPasienFri      int    `json:"quotaPasienFri"`
		QuotaPasienSat      int    `json:"quotaPasienSat"`
		COA                 string `json:"coa"`
		COA2                string `json:"coa2"`
		INHEALTH            string `json:"inhealth"`
		NamaDokter          string `json:"namaDokter"`
		Spesialis           string `json:"spesialis"`
		NamaPelayanan       string `json:"namaPelayanan"`
	}

	KKelas struct {
		Kode      string `json:"kode"`
		Namakelas string `json:"namaKelas"`
		Deskripsi string `json:"deskripsi"`
	}

	KLoket struct {
		Kunci      string    `json:"kunci"`
		UpdDttm    time.Time `json:"updateDttm"`
		InsertDttm time.Time `json:"insertDttm"`
		Group      string    `json:"group"`
		Kode       string    `json:"kode"`
		Loket      string    `json:"loket"`
		Deskripsi  string    `json:"deskripsi"`
	}

	KPekerjaan struct {
		Kd string `json:"kode"`
		Nm string `json:"nm"`
	}

	KProcedure struct {
		Divisi       string `json:"divisi"`
		KdBag        string `json:"kdBag"`
		Kode         string `json:"kode"`
		MapingInacbg string `json:"MappingInacbg"`
		Grup         string `json:"grup"`
		NameGrup     string `json:"nameGrup"`
		Kelompok     string `json:"kelompok"`
		Deskripsi    string `json:"deskripsi"`
		Satuan       string `json:"satuan"`
		NilaiKritis  string `json:"nilaiKritis"`
		Normal       string `json:"normal"`
		NormalAnak   string `json:"normalAnak"`
		Anak36       string `json:"anak36"`
		Anak6Keatas  string `json:"anak6Keatas"`
		NormalBayi   string `json:"normalBayi"`
	}

	KRuangPerawatan struct {
		String       string    `json:"notif"`
		CreateDttm   time.Time `json:"createDttm"`
		Status       string    `json:"status"`
		KodeKasus    string    `json:"kodeKasus"`
		GenderKamar  string    `json:"genderKamar"`
		KodeRef      string    `json:"kodeRef"`
		KodeKamar    string    `json:"kodeKamar"`
		NamaKamar    string    `json:"namaKamar"`
		KodeKelas    string    `json:"kodeKelas"`
		NoBed        string    `json:"noBed"`
		HargaKamar   float32   `json:"hargaKamar"`
		Noreg        float32   `json:"noreg"`
		KdDokter     string    `json:"kodeDokter"`
		CheckIn      string    `json:"checkIn"`
		LastCheckOut string    `json:"lastCheckOut"`
		InfoKosong   string    `json:"InfoKosong"`
	}

	KRuangKasus struct {
		Kode      string `json:"kode"`
		Deskripsi string `json:"Deskripsi"`
	}

	KSpesialisasi struct {
		Kd    string `json:"kode"`
		Nm    string `json:"nama"`
		Gelar string `json:"gelar"`
	}

	KSuku struct {
		Kd string `json:"kode"`
		Nm string `json:"nama"`
	}

	KIcd10 struct {
		Chapter     string `json:"chapter"`
		S1          string `json:"s1"`
		Code        string `json:"code"`
		Code2       string `json:"code2"`
		Description string `json:"description"`
		Severity    string `json:"severity"`
		Inpatient   string `json:"inpatient"`
		Outpatient  string `json:"outpatient"`
	}

	KTaripVisite struct {
		KdDebitur string  `json:"kdDebitur"`
		KdDokter  string  `json:"kdDokter"`
		KdKelas   string  `json:"kdKelas"`
		Tarip     float32 `json:"tarip"`
	}

	DokterSpesialisModel struct {
		Idk       string `json:"idk"`
		Nama      string `json:"nama"`
		Spesialis string `json:"spesialis"`
		Gelar     string `json:"gelar"`
	}

	//============= PROCEDURE MODEL
	KProcedureModel struct {
		Kel      string `json:"kel"`
		Grup     string `json:"grup"`
		NameGrup string `json:"name_grup"`
		Icd      string `json:"icd"`
		Urut     int64  `json:"urut"`
	}

	TaripFisioTerapi struct {
		KdBag     string `json:"kd_bag"`
		Icd9      string `json:"icd9"`
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
		Tarip     int    `json:"tarip"`
	}

	KFisioterapi struct {
		Kode      string `json:"kode"`
		KdBag     string `json:"kd_bag"`
		Icd9      string `json:"icd9"`
		Deskripsi string `json:"deskripsi"`
	}

	// Detail Tarip Radiologi
	DetailTaripRadioLogi struct {
		KdBag     string `json:"kd_bag"`
		Kode      string `json:"kode"`
		Deskripsi string `json:"deskripsi"`
		Tarip     int    `json:"tarip"`
	}

	// ANTRIAN PASIEN
	AtrianPasienModel struct {
		UserId      string
		ReceiveDttm string
		Noreg       string
		KdTujuan    string
		KdDokter    string
		NoOrder     string
		Enum        string
	}

	// INVENTORY OBAT
	KInventoryObat struct {
		Kodeobat string
		Namaobat string
	}

	//===================== //
	KICD9 struct {
		Code        string `json:"kode"`
		Code2       string `json:"kode2"`
		Description string `json:"description"`
		Class       string `json:"class"`
		Inpatient   string `json:"inpatient"`
		Outpatient  string `json:"outpatient"`
	}
)

func (KFisioterapi) TableName() string {
	return "vicore_lib.ktaripvisite"
}

func (KICD9) TableName() string {
	return "vicore_lib.k_icd9"
}

func (KInventoryObat) TableName() string {
	return "vicore_lib.kinventory"
}

func (KFartorResiko) TableName() string {
	return "vicore_lib.kfaktor_resiko"
}
func (KResikoJatuhPasien) TableName() string {
	return "vicore_lib.kresiko_jatuh"
}
func (KTaripVisite) TableName() string {
	return "vicore_lib.ktaripvisite"
}

func (KTaripSIMRS) TableName() string {
	return "vicore_lib.ktaripsimrs"
}

func (KJadwalDokter) TableName() string {
	return "vicore_lib.kjadwaldokter"
}

func (KPelayanan) TableName() string {
	return "vicore_lib.kpelayanan"
}
func (KICD10) TableName() string {
	return "vicore_lib.k_icd10"
}

func (Version) TableName() string {
	return "vicore_reg.app_version"
}

func (DresumeMedik) TableName() string {
	return "vicore_rme.dresume_medik"
}

func (KFarPenggunaan) TableName() string {
	return "vicore_lib.kfar_penggunaan"
}

func (KIcd10) TableName() string {
	return "vicore_lib.k_icd10"
}

func (DcpptSoapPasien) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (KSuku) TableName() string {
	return "vicore_lib.ksuku"
}

func (KInventory) TableName() string {
	return "vicore_lib.KInventory"
}

func (KProcedure) TableName() string {
	return "vicore_lib.kprocedure"
}

func (KFARDosis) TableName() string {
	return "vicore_lib.kfar_dosis"
}

func (KFARKemasan) TableName() string {
	return "vicore_lib.kfar_kemasan"
}

func (KDebitur) TableName() string {
	return "vicore_lib.kdebitur"
}
func (KBankData) TableName() string {
	return "vicore_lib.kbank_data"
}

func (KFARKomposisi) TableName() string {
	return "vicore_lib.kfar_komposisi"
}

func (KFarSatuan) TableName() string {
	return "vicore_lib.kfar_satuan"
}

func (KFARGolongan) TableName() string {
	return "vicore_lib.kfar_golongan"
}

func (KVitalSign) TableName() string {
	return "vicore_lib.kvital_sign"
}
