package mapper

import (
	"hms_api/modules/lib"
	"hms_api/modules/lib/dto"
	"hms_api/modules/lib/entity"
)

type LibMapperImpl struct{}

func NewLibMapperImple() entity.ILibMapper { return &LibMapperImpl{} }

func (a *LibMapperImpl) ToInventoryMapper(data []lib.KInventory) (value []dto.InventoryResult) {

	for _, V := range data {
		value = append(value, dto.InventoryResult{
			NamaObat:    V.Namaobat,
			Kemasan:     V.Kemasan,
			Satuan:      V.Satuan,
			HargaJual:   int(V.Hargajual),
			HargaTelkom: int(V.Hargatelkom),
			Keterangan:  V.Keterangan,
			Maximal:     int(V.Maximal),
		})
	}

	return value
}

func (a *LibMapperImpl) ToJadwalDokterMapper(data []lib.KJadwalDokter) (value []dto.JadwalDokterResult) {

	for _, V := range data {
		value = append(value, dto.JadwalDokterResult{
			BukaPoli:   V.BukaPoli,
			TutupPoli:  V.TutupPoli,
			NamaDokter: V.NamaDokter,
			Spesialis:  V.Spesialis,
			NamaPoli:   V.NamaPelayanan,
			QuotaSun:   V.QuotaPasienSun,
			QuotaMon:   V.QuotaPasienMon,
			QuotaTue:   V.QuotaPasienTue,
			QuotaWed:   V.QuotaPasienWed,
			QuotaThu:   V.QuotaPasienThu,
			QuotaFri:   V.QuotaPasienFri,
			QuotaSat:   V.QuotaPasienSat,
			Senin:      V.Senin,
			Selasa:     V.Selasa,
			Rabu:       V.Rabu,
			Kamis:      V.Kamis,
			Jumat:      V.Jumat,
			Sabtu:      V.Sabtu,
		})
	}

	return value
}
