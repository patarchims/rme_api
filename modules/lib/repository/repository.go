package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/lib"
	"hms_api/modules/lib/dto"
	"hms_api/modules/lib/entity"

	"github.com/rs/zerolog/log"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type libRepository struct {
	DB      *gorm.DB
	Logging *logrus.Logger
}

func NewLibRepository(db *gorm.DB, Logging *logrus.Logger) entity.LibRepository {
	return &libRepository{
		DB:      db,
		Logging: Logging,
	}
}

func (lu *libRepository) GetJadwalDokter() (res []lib.KJadwalDokter, err error) {
	query := `SELECT a.*, b.nama AS nama_dokter, c.nm AS spesialis, d.bagian AS nama_pelayanan FROM vicore_lib.ktaripdokter AS a LEFT JOIN vicore_hrd.kemployee AS b ON a.iddokter=b.idk LEFT JOIN vicore_lib.kspesialisasi AS c ON a.kode_spesialisasi=c.kd LEFT JOIN vicore_lib.kpelayanan AS d ON a.maping_poli=d.maping_bpjs`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	lu.Logging.Info(result.Statement)

	return res, nil
}

// GET DEBITUR
func (lu *libRepository) GetDebitur() (res []lib.KDebitur, err error) {
	var results []lib.KDebitur

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

// GET DOSIS
func (lu *libRepository) GetDosis() (res []lib.KFARDosis, err error) {
	var results []lib.KFARDosis

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetGolongan() (res []lib.KFARGolongan, err error) {
	var results []lib.KFARGolongan

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) OnGetKpelayananRepositoryByKode(kdBagian string) (res lib.KPelayanan, err error) {
	errs := lu.DB.Where("kd_bag=?", kdBagian).Find(&res).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *libRepository) GetKPelayananRepository() (res []lib.KPelayanan, err error) {
	var results []lib.KPelayanan

	errs := lu.DB.Where("asesmen_active=?", true).Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) OnGetKPelayananRepositoryByKdPelayanan(kode string) (res lib.KPelayanan, err error) {
	err12 := lu.DB.Where(lib.KPelayanan{KdBag: kode}).Find(&res)

	if err12 != nil {
		message := fmt.Sprintf("Error %s, data tidak ditemukan", err12.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *libRepository) GetVersion() (res lib.Version, err error) {

	query := `SELECT android_version, ios_version, windows_version from vicore_reg.app_version`
	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (lu *libRepository) GetAllKVitalSign() (res []lib.KVitalSign, err error) {
	var results []lib.KVitalSign

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetPenggunaan() (res []lib.KFarPenggunaan, err error) {
	var results []lib.KFarPenggunaan

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetSatuan() (res []lib.KFarSatuan, err error) {
	var results []lib.KFarSatuan

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetProcedure() (res []lib.KProcedure, err error) {
	var results []lib.KProcedure

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetSuku() (res []lib.KSuku, err error) {
	var results []lib.KSuku

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetKRuangPerawatan() (res []lib.KRuangPerawatan, err error) {
	var results []lib.KRuangPerawatan

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetInventory() (res []lib.KInventory, err error) {
	var results []lib.KInventory

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetRuangKasus() (res []lib.KRuangKasus, err error) {
	var results []lib.KRuangKasus

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetKomposisi() (res []lib.KFARKomposisi, err error) {
	var results []lib.KFARKomposisi

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetAllKICD10() (res []lib.KICD10, err error) {
	var results []lib.KICD10

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetAllSoapPasien() (res []lib.DcpptSoapPasien, err error) {

	var results []lib.DcpptSoapPasien

	errs := lu.DB.Find(&results).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetAllICD() (res []lib.KIcd10, err error) {

	result := lu.DB.Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (lu *libRepository) GetICD10(kode string, limit int, offset int) (res []lib.KIcd10, err error) {
	var results []lib.KIcd10

	data := lu.DB.Where("code LIKE ?", "%"+kode+"%").Offset(offset).Limit(limit).Find(&results)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		return results, errors.New(message)
	}

	return results, nil

}

func (lu *libRepository) GetDataICD10(kode string) (res []lib.KIcd10, err error) {
	var results []lib.KIcd10

	data := lu.DB.Where("code=?", kode).Find(&results)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		return results, errors.New(message)
	}

	return results, nil

}

func (lu *libRepository) GetBankData(kategori string) (res []lib.KBankData, err error) {
	var results []lib.KBankData

	data := lu.DB.Where("kategori=?", kategori).Find(&results)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		log.Logger.Info().Msg(message)
		return results, errors.New(message)
	}

	return results, nil
}

func (lu *libRepository) GetKemasan() (res []lib.KFARKemasan, err error) {
	var kemasans []lib.KFARKemasan

	errs := lu.DB.Find(&kemasans).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return kemasans, errors.New(message)
	}

	return kemasans, nil
}

func (lu *libRepository) GetDokterSpesialis() (res []lib.DokterSpesialisModel, err error) {
	query := `SELECT idk,  nama, b.nm AS spesialis, b.gelar FROM vicore_hrd.kemployee AS a
	INNER JOIN  vicore_lib.kspesialisasi AS b ON b.kd = a.mapping_spesialis WHERE b.gelar != ""`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

// GET DOKTER SPESIALISAI
func (lu *libRepository) GetKTaripDokterRepository() (res []lib.DokterSpesialisModel, err error) {
	query := `SELECT iddokter AS idk, namadokter AS nama , spesialisasi AS spesialis, pendidikan AS gelar  FROM  his.ktaripdokter  `

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (lu *libRepository) GetProcedureByBagian(bagian string) (res []lib.KProcedureModel, err error) {

	query := `SELECT DISTINCT(kelompok) AS kel,grup,name_grup,icd9 AS icd,  urut FROM vicore_lib.kprocedure WHERE kd_bag=? GROUP BY kel  ORDER BY urut ASC`

	result := lu.DB.Raw(query, bagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *libRepository) GetProcedureFisioTerapi(kdBagian string) (res []lib.KFisioterapi, err error) {
	query := `SELECT kode, kd_bag, icd9, deskripsi FROM vicore_lib.kprocedure  WHERE kd_bag=?`

	result := lu.DB.Raw(query, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *libRepository) GetTaripFisioTerapi(kdBagian string) (res []lib.TaripFisioTerapi, err error) {

	query := `SELECT kprocedure.kd_bag,kprocedure.kode,kprocedure.deskripsi,ktaripsimrs.tarip FROM kprocedure LEFT JOIN ktaripsimrs ON kprocedure.kode=ktaripsimrs.kode AND kprocedure.kd_bag=ktaripsimrs.kd_bagian WHERE kprocedure.kd_bag=? AND ktaripsimrs.kd_debitur='BPJS' AND ktaripsimrs.kd_kelas='00' AND kprocedure.kode=?`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (lu *libRepository) GetTaripRadiologi(req dto.GetTaripRad) (res lib.DetailTaripRadioLogi, err error) {
	query := `SELECT a.kd_bag, a.kode, a.deskripsi, b.tarip FROM vicore_lib.kprocedure AS a LEFT JOIN vicore_lib.ktaripsimrs AS b ON a.kode=b.kode AND a.kd_bag=b.kd_bagian WHERE a.kd_bag=?
	AND b.kd_debitur=? AND b.kd_kelas='00' AND a.kode=?`

	result := lu.DB.Raw(query, req.KodeBagian, req.Debitur, req.Kode).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	lu.Logging.Info(result.Statement)
	return res, nil
}

func (lu *libRepository) GetTaripRadiologiV2(req dto.GetTaripRadV2) (res lib.DetailTaripRadioLogi, err error) {
	query := `SELECT a.kd_bag, a.kode, a.deskripsi, b.tarip FROM vicore_lib.kprocedure AS a LEFT JOIN vicore_lib.ktaripsimrs AS b ON a.kode=b.kode AND a.kd_bag=b.kd_bagian WHERE a.kd_bag=?
	AND b.kd_debitur=? AND b.kd_kelas='00' AND a.kode=?`

	result := lu.DB.Raw(query, req.KodeBagian, req.Debitur, req.Kode).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	lu.Logging.Info(result.Statement)
	return res, nil
}

func (lu *libRepository) GetRadiologi() (res []lib.TaripFisioTerapi, err error) {

	query := `SELECT  * FROM vicore_lib.kprocedure WHERE kd_bag="CTS001" OR kd_bag="EKG001" OR kd_bag="MAM001" OR kd_bag="RON001" OR kd_bag="USG001"`

	result := lu.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *libRepository) GetFaktorResikoJatuhRespository(kategori string, jenis string) (res []lib.KFartorResiko, err error) {
	ersr := sr.DB.Where("kategori_umur=? AND jenis=?", kategori, jenis).Order("no_urut ASC").Preload("KResikoJatuhPasien",
		func(db *gorm.DB) *gorm.DB {
			return db.Order("vicore_lib.kresiko_jatuh.no_urut ASC")
		}).Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}

func (sr *libRepository) GetAllInventoryObatRepository() (res []lib.KInventoryObat, err error) {
	var obat []lib.KInventoryObat

	errs := sr.DB.Find(&obat).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return obat, errors.New(message)
	}

	return obat, nil
}

func (sr *libRepository) GetAllKICD9Repository() (res []lib.KICD9, err error) {
	var icd []lib.KICD9

	errs := sr.DB.Find(&icd).Error

	if errs != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs.Error())
		return icd, errors.New(message)
	}

	return icd, nil
}
