package dto

type (
	ToResponsePengkajianAwalNyeri struct {
		Noreg      string `json:"noreg"`
		Metode     string `json:"metode"`
		Asesmen    string `json:"asesmen"`
		Pelayanan  string `json:"pelayanan"`
		SkorNyeri  int    `json:"skor_nyeri"`
		HasilNyeri int    `json:"hasil_nyeri"`
		WaktuKaji  int    `json:"waktu_nyeri"`
		Penyebab   string `json:"penyebab"`
		Kualitas   string `json:"kualitas"`
		Penyebaran string `json:"penyebaran"`
		Keparahan  string `json:"keparahan"`
		Waktu      string `json:"waktu"`
		Lokasi     string `json:"lokasi"`
	}

	// RESPONSE PENGKAJIAN NUTRISI
	ToResponsePengkajianNutrisi struct {
		Usia      string `json:"usia"`
		Pelayanan string `json:"pelayanan"`
		KdBagian  string `json:"bagian"`
		Noreg     string `json:"noreg"`
		N1        string `json:"n1"`
		N2        string `json:"n2"`
		N3        string `json:"n3"`
		N4        string `json:"n4"`
		Nilai     int    `json:"nilai"`
	}

	ToResponsePengkajianNutrisiDewasa struct {
		Usia      string `json:"usia"`
		Pelayanan string `json:"pelayanan"`
		KdBagian  string `json:"bagian"`
		Noreg     string `json:"noreg"`
		N1        string `json:"n1"`
		N2        string `json:"n2"`
		Nilai     int    `json:"nilai"`
	}
)
