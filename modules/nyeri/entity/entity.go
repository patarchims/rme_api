package entity

import (
	"hms_api/modules/nyeri"
	"hms_api/modules/nyeri/dto"
)

type NyeriRepository interface {
	OnGetPengkajianNutrisiRepository(noReg string, kdBagian string, pelayanan string, usia string) (res nyeri.DPengkajianNutrisi, err error)
	OnUpdateNyeriRepository(noReg string, kdBagian string, pelayanan string, asesmen string, update nyeri.DasesmenUlangNyeri) (res nyeri.DasesmenUlangNyeri, err error)
	OnSaveAsesmenUlangNyeriRepository(data nyeri.DasesmenUlangNyeri) (res nyeri.DasesmenUlangNyeri, err error)
	OnGetAsesmenUlangNyeriRepository(noReg string, kdBagian string, pelayanan string, asesmen string) (res nyeri.DasesmenUlangNyeri, err error)
	OnUpdatePengkajianNutrisiRepository(noReg string, kdBagian string, pelayanan string, usia string, update nyeri.DPengkajianNutrisi) (res nyeri.DPengkajianNutrisi, err error)
	OnSavePengkajianNyeriRepository(data nyeri.DPengkajianNutrisi) (res nyeri.DPengkajianNutrisi, err error)
}

type NyeriMapper interface {
	ToMapperResponsePengkajianAwalNyeri(data nyeri.DasesmenUlangNyeri) (res dto.ToResponsePengkajianAwalNyeri)
	ToMappingToResponsePengkajianNutrisi(data nyeri.DPengkajianNutrisi) (res dto.ToResponsePengkajianNutrisi)
	ToMappingResponsePengkajianNutrisi(data nyeri.DPengkajianNutrisi) (res dto.ToResponsePengkajianNutrisiDewasa)
}

type NyeriUseCase interface{}
