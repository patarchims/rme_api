package mapper

import (
	"hms_api/modules/nyeri"
	"hms_api/modules/nyeri/dto"
	"hms_api/modules/nyeri/entity"
)

type NyeriMapperImple struct{}

func NewNyeriImple() entity.NyeriMapper { return &NyeriMapperImple{} }

func (a *NyeriMapperImple) ToMapperResponsePengkajianAwalNyeri(data nyeri.DasesmenUlangNyeri) (res dto.ToResponsePengkajianAwalNyeri) {
	return dto.ToResponsePengkajianAwalNyeri{
		Noreg:      data.Noreg,
		Metode:     toMethode(data.Metode),
		Asesmen:    data.Asesmen,
		Pelayanan:  data.Pelayanan,
		SkorNyeri:  data.SkorNyeri,
		HasilNyeri: data.HasilNyeri,
		WaktuKaji:  data.WaktuKaji,
		Penyebab:   data.Penyebab,
		Kualitas:   data.Kualitas,
		Penyebaran: data.Penyebaran,
		Keparahan:  data.Keparahan,
		Waktu:      data.Waktu,
		Lokasi:     data.Lokasi,
	}
}

func (a *NyeriMapperImple) ToMappingToResponsePengkajianNutrisi(data nyeri.DPengkajianNutrisi) (res dto.ToResponsePengkajianNutrisi) {
	return dto.ToResponsePengkajianNutrisi{
		Usia:      data.Usia,
		Pelayanan: data.Pelayanan,
		KdBagian:  data.KdBagian,
		Noreg:     data.Noreg,
		N1:        data.N1,
		N2:        data.N2,
		N3:        data.N3,
		N4:        data.N4,
		Nilai:     data.Nilai,
	}
}

func (a *NyeriMapperImple) ToMappingResponsePengkajianNutrisi(data nyeri.DPengkajianNutrisi) (res dto.ToResponsePengkajianNutrisiDewasa) {
	return dto.ToResponsePengkajianNutrisiDewasa{
		Usia:      data.Usia,
		Pelayanan: data.Pelayanan,
		KdBagian:  data.KdBagian,
		Noreg:     data.Noreg,
		N1:        data.N1,
		N2:        data.N2,
		Nilai:     data.Nilai,
	}
}

func toMethode(value string) string {
	if value == "" {
		return "WONG_BAKER_FACE"
	} else {
		return value
	}
}
