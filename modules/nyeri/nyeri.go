package nyeri

import "time"

type (
	DasesmenUlangNyeri struct {
		IDUlangNyeri   int       `gorm:"column:id_ulang_nyeri;primaryKey;autoIncrement"`
		InsertDttm     time.Time `gorm:"column:insert_dttm;default:0000-00-00 00:00:00"`
		IDFarmakologi  int       `gorm:"column:id_farmakologi;default:0"`
		Noreg          string    `gorm:"column:noreg;default:0"`
		UserID         string    `gorm:"column:user_id;default:''"`
		KdBagian       string    `gorm:"column:kd_bagian;default:''"`
		Metode         string    `gorm:"column:metode;type:enum('WONG_BAKER_FACE','CONFORT_SCALE','NIPS','RATING_SCALE','FLACGS');default:WONG_BAKER_FACE"`
		Asesmen        string    `gorm:"column:asesmen;type:enum('AWAL','ULANG');default:AWAL"`
		Pelayanan      string    `gorm:"column:pelayanan;type:enum('RAJAL','RANAP');default:RAJAL"`
		SkorNyeri      int       `gorm:"column:skor_nyeri;default:0"`
		HasilNyeri     int       `gorm:"column:hasil_nyeri;default:1"`
		NamaObat       string    `gorm:"column:nama_obat;default:''"`
		DosisFrekuensi string    `gorm:"column:dosis_frekuensi;default:''"`
		Rute           string    `gorm:"column:rute;default:''"`
		NonFarmakologi int       `gorm:"column:non_farmakologi;default:0"`
		WaktuKaji      int       `gorm:"column:waktu_kaji;default:0"`
		Penyebab       string    `gorm:"column:penyebab;default:''"`
		Kualitas       string    `gorm:"column:kualitas;default:''"`
		Penyebaran     string    `gorm:"column:penyebaran;default:''"`
		Keparahan      string    `gorm:"column:keparahan;default:''"`
		Waktu          string    `gorm:"column:waktu;default:''"`
		Lokasi         string    `gorm:"column:lokasi;default:''"`
	}

	DPengkajianNutrisi struct {
		InsertDttm   time.Time `gorm:"column:insert_dttm;type:datetime"`
		UpdDttm      time.Time `gorm:"column:upd_dttm;type:timestamp"`
		InsertPc     string    `gorm:"column:insert_pc;type:varchar(225)"`
		InsertUserID string    `gorm:"column:insert_user_id;type:varchar(225)"`
		Usia         string    `gorm:"column:usia;type:enum('ANAK','DEWASA');default:'ANAK'"`
		Pelayanan    string    `gorm:"column:pelayanan;type:enum('RAJAL','RANAP');default:'RAJAL'"`
		KdBagian     string    `gorm:"column:kd_bagian;type:varchar(225)"`
		Noreg        string    `gorm:"column:noreg;type:varchar(225)"`
		N1           string    `gorm:"column:n1;type:varchar(50);default:''"`
		N2           string    `gorm:"column:n2;type:varchar(50);default:''"`
		N3           string    `gorm:"column:n3;type:varchar(50);default:''"`
		N4           string    `gorm:"column:n4;type:varchar(50);default:''"`
		Nilai        int       `gorm:"column:nilai;type:int;not null;default:0"`
	}

	// TableName sets the table name for the DPengkajianNutrisi struct.

)

func (DPengkajianNutrisi) TableName() string {
	return "vicore_rme.dpengkajian_nutrisi"
}

func (DasesmenUlangNyeri) TableName() string {
	return "vicore_rme.dasesmen_ulang_nyeri"
}
