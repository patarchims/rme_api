package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/nyeri"
)

func (rr *nyeriRepository) OnUpdatePengkajianNutrisiRepository(noReg string, kdBagian string, pelayanan string, usia string, update nyeri.DPengkajianNutrisi) (res nyeri.DPengkajianNutrisi, err error) {
	result := rr.DB.Model(&nyeri.DPengkajianNutrisi{}).Where(&nyeri.DPengkajianNutrisi{
		Noreg:     noReg,
		KdBagian:  kdBagian,
		Pelayanan: pelayanan,
		Usia:      usia,
	}).Updates(&update)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (rr *nyeriRepository) OnGetPengkajianNutrisiRepository(noReg string, kdBagian string, pelayanan string, usia string) (res nyeri.DPengkajianNutrisi, err error) {
	result := rr.DB.Model(&nyeri.DPengkajianNutrisi{}).Where(&nyeri.DPengkajianNutrisi{
		Noreg:     noReg,
		KdBagian:  kdBagian,
		Pelayanan: pelayanan,
		Usia:      usia,
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (rr *nyeriRepository) OnSavePengkajianNyeriRepository(data nyeri.DPengkajianNutrisi) (res nyeri.DPengkajianNutrisi, err error) {
	result := rr.DB.Create(&data).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
