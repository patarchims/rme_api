package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/nyeri"
	"hms_api/modules/nyeri/entity"

	"gorm.io/gorm"
)

type nyeriRepository struct {
	DB *gorm.DB
}

func NewRegionalRepository(db *gorm.DB) entity.NyeriRepository {
	return &nyeriRepository{
		DB: db,
	}
}

func (rr *nyeriRepository) OnGetAsesmenUlangNyeriRepository(noReg string, kdBagian string, pelayanan string, asesmen string) (res nyeri.DasesmenUlangNyeri, err error) {
	result := rr.DB.Model(&nyeri.DasesmenUlangNyeri{}).Where(&nyeri.DasesmenUlangNyeri{
		Noreg:     noReg,
		KdBagian:  kdBagian,
		Pelayanan: pelayanan,
		Asesmen:   asesmen,
	}).Find(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (rr *nyeriRepository) OnUpdateNyeriRepository(noReg string, kdBagian string, pelayanan string, asesmen string, update nyeri.DasesmenUlangNyeri) (res nyeri.DasesmenUlangNyeri, err error) {
	result := rr.DB.Model(&nyeri.DasesmenUlangNyeri{}).Where(&nyeri.DasesmenUlangNyeri{
		Noreg:     noReg,
		KdBagian:  kdBagian,
		Pelayanan: pelayanan,
		Asesmen:   asesmen,
	}).Updates(&update)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (rr *nyeriRepository) OnSaveAsesmenUlangNyeriRepository(data nyeri.DasesmenUlangNyeri) (res nyeri.DasesmenUlangNyeri, err error) {
	result := rr.DB.Create(&data).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
