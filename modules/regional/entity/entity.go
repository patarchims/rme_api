package entity

import (
	"hms_api/modules/regional"
)

type RegionalRepository interface {
	GetAllProvinsi() (res []regional.Provinsi, err error)
	GetAllKabupatenByProv(strProv string) (res []regional.Kabupaten, err error)
	GetAllKecamatanByKab(strKab string) (res []regional.Kecamatan, err error)
	GetAllKelurahanByKec(strKel string) (res []regional.Kelurahan, err error)
	GetProvAndKab(strProv string) (res []regional.ProvAndKab, err error)
	GetKabAndKec(strKab string) (res []regional.KecAndKab, err error)
	GetKelurahanByKec(strKec string) (res []regional.KeluarahanModel, err error)
}

// Mapper“
type IRMEMapper interface {
	// ToJadwalDokterMapper(data []lib.KJadwalDokter) (value []dto.JadwalDokterResult)
	// ToInventoryMapper(data []lib.KInventory) (value []dto.InventoryResult)
}
