package handler

import (
	"hms_api/modules/regional/entity"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type RegionalHandler struct {
	RegionalRepo entity.RegionalRepository
	Logging      *logrus.Logger
}

// @Summary		Data Regional Kelurahan By Kecamatan
// @Description	Data master regional kelurahan by kecamatan
// @Tags			Regional
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]regional.KeluarahanModel
// @Router			/kelurahan-kec/:namaKec{namakecamatan}/ [get]
func (dh *RegionalHandler) GetKelurahanByKec(c *gin.Context) {
	namaKec := c.Param("namaKec")
	list, err := dh.RegionalRepo.GetKelurahanByKec(namaKec)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *RegionalHandler) GetKelurahanByKecV2(c *fiber.Ctx) error {
	namaKec := c.Params("namaKec")
	list, err := dh.RegionalRepo.GetKelurahanByKec(namaKec)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Data Regional Kabupaten Dan Kecamatan
// @Description	Data master regional kabupaten dan kecamatan
// @Tags			Regional
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]regional.KecAndKab
// @Router			/kecamatan-kab/:namaKab{namaKabupaten}/ [get]

func (dh *RegionalHandler) GetKabAndKec(c *gin.Context) {
	namaKab := c.Param("namaKab")
	list, err := dh.RegionalRepo.GetKabAndKec(namaKab)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *RegionalHandler) GetKabAndKecV2(c *fiber.Ctx) error {
	namaKab := c.Params("namaKab")
	list, err := dh.RegionalRepo.GetKabAndKec(namaKab)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Data Regional Provisi Dan Kabupatan
// @Description	Data master regional
// @Tags			Regional
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	[]regional.ProvAndKab
// @Router			/prov-kab/:namaProv{namaProv}/ [get]
func (dh *RegionalHandler) GetProvAndKab(c *gin.Context) {
	namaProv := c.Param("namaProv")
	list, err := dh.RegionalRepo.GetProvAndKab(namaProv)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *RegionalHandler) GetProvAndKabV2(c *fiber.Ctx) error {
	namaProv := c.Params("namaProv")
	list, err := dh.RegionalRepo.GetProvAndKab(namaProv)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Data Regional
// @Description	Data master regional
// @Tags			Regional
// @Produce		json
// @Accept			*/*
// @securityDefinitions.apikey ApiKeyAuth
// @Success		200		{object}	 []regional.Provinsi
// @Router			/provinsi/ [get]
func (dh *RegionalHandler) GetAllProvinsi(c *gin.Context) {

	list, err := dh.RegionalRepo.GetAllProvinsi()
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data provinsi kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *RegionalHandler) GetAllProvinsiV2(c *fiber.Ctx) error {

	list, err := dh.RegionalRepo.GetAllProvinsi()
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(list) == 0 {
		response := helper.APIResponseFailure("Data provinsi kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *RegionalHandler) GetKabupaten(c *gin.Context) {
	idProv := c.Param("idProv")
	list, err := dh.RegionalRepo.GetAllKabupatenByProv(idProv)
	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}
	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kabupaten kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)
}

func (dh *RegionalHandler) GetKecamatan(c *gin.Context) {
	idKab := c.Param("idKab")
	list, err := dh.RegionalRepo.GetAllKecamatanByKab(idKab)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}
	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kecamatan kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)

}

func (dh *RegionalHandler) GetKelurahan(c *gin.Context) {
	idKec := c.Param("idKec")
	list, err := dh.RegionalRepo.GetAllKelurahanByKec(idKec)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}
	if len(list) == 0 {
		response := helper.APIResponseFailure("Data kelurahan kosong", http.StatusAccepted)
		c.JSON(http.StatusAccepted, response)
		return
	}

	response := helper.APIResponse("Ok", http.StatusOK, list)
	c.JSON(http.StatusOK, response)

}
