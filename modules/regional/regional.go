package regional

type (
	Provinsi struct {
		IdProv string `json:"IDProv"`
		Nama   string `json:"NamaProv"`
	}
	Kecamatan struct {
		IdKec string `json:"IDKec"`
		IdKab string `json:"IDKab"`
		Nama  string `json:"NamaKec"`
	}

	Kelurahan struct {
		IdKel string `json:"IDKel"`
		IdKec string `json:"IDKec"`
		Nama  string `json:"NamaKel"`
	}

	Kabupaten struct {
		IdKab  string `json:"IDKab"`
		IdProv string `json:"IDProv"`
		Nama   string `json:"NamaKab"`
	}

	ProvAndKab struct {
		Provinsi  string `json:"Provinsi"`
		Kabupaten string `json:"Kabupaten"`
	}

	KecAndKab struct {
		Kecamatan string `json:"Kecamatan"`
		Kabupaten string `json:"Kabupaten"`
	}

	KeluarahanModel struct {
		Kelurahan string `json:"Kelurahan"`
		Kecamatan string `json:"Kecamatan"`
	}
)
