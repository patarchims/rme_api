package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/regional"
	"hms_api/modules/regional/entity"

	"gorm.io/gorm"
)

type regionalRepository struct {
	DB *gorm.DB
}

func NewRegionalRepository(db *gorm.DB) entity.RegionalRepository {
	return &regionalRepository{
		DB: db,
	}
}

func (rr *regionalRepository) GetAllProvinsi() (res []regional.Provinsi, err error) {
	query := "SELECT * FROM vicore_reg.provinsi"
	result := rr.DB.Raw(query).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetAllKabupatenByProv(strProv string) (res []regional.Kabupaten, err error) {
	query := "SELECT * FROM vicore_reg.kabupaten WHERE id_prov=?"
	result := rr.DB.Raw(query, strProv).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetAllKecamatanByKab(strKab string) (res []regional.Kecamatan, err error) {
	query := "SELECT * FROM vicore_reg.kecamatan WHERE id_kab=?"
	result := rr.DB.Raw(query, strKab).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetAllKelurahanByKec(strKel string) (res []regional.Kelurahan, err error) {
	query := "SELECT * FROM vicore_reg.kelurahan WHERE id_kec=?"
	result := rr.DB.Raw(query, strKel).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetProvAndKab(strProv string) (res []regional.ProvAndKab, err error) {
	query := "SELECT a.nama AS provinsi, b.nama AS kabupaten FROM  vicore_reg.provinsi AS a LEFT JOIN vicore_reg.kabupaten  AS b ON  b.id_prov=a.id_prov WHERE a.nama=?"
	result := rr.DB.Raw(query, strProv).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetKabAndKec(strKab string) (res []regional.KecAndKab, err error) {
	query := "SELECT a.nama AS kecamatan, b.nama AS kabupaten FROM  vicore_reg.kecamatan AS a LEFT JOIN vicore_reg.kabupaten AS b ON a.id_kab = b.id_kab WHERE b.nama=?"
	result := rr.DB.Raw(query, strKab).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *regionalRepository) GetKelurahanByKec(strKec string) (res []regional.KeluarahanModel, err error) {
	query := "SELECT a.nama AS kelurahan, b.nama AS kecamatan FROM  vicore_reg.kelurahan AS a INNER JOIN vicore_reg.kecamatan AS b ON b.id_kec = a.id_kec WHERE b.nama=?"
	result := rr.DB.Raw(query, strKec).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}
