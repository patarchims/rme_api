package dto

import (
	"hms_api/modules/his"
	"hms_api/modules/lib"
	"hms_api/modules/report"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
)

type (
	// ==========
	ResponseCPPTWithPasien struct {
		CPPT      []report.ReportCPPTPasien `json:"cppt"`
		Pasien    user.ProfilePasien        `json:"pasien"`
		Pelayanan lib.KPelayanan            `json:"pelayanan"`
	}

	// ==========
	ResTriaseResponse struct {
		Triase          report.TriaseReport        `json:"triase"`
		KeluhanUtama    string                     `json:"keluhan_utama"`
		Alergi          []rme.DAlergi              `json:"alergi"`
		SoapDokter      report.SoapDokerResponse   `json:"soap_dokter"`
		Fisik           PemeriksaanFisik           `json:"pemeriksaan_fisik"`
		AsesmenBidan    report.KeperawatanBidanIGD `json:"asesmen_bidan"`
		RiwayatPenyakit []soap.RiwayatPenyakit     `json:"riwayat"`
	}

	ResponseAsesmenAwalKeperawatanIGD struct {
		Alergi []rme.DAlergi `json:"alergi"`
	}

	ResRawatInapAnak struct {
		Pasien report.DProfilePasien `json:"pasien"`
	}

	// ASESSMEN KEPERAWATAN

	ResDiagnosis struct {
		AsesmedDiagP  string `json:"diagnosa_primer"`
		AsesmedDiagS1 string `json:"diagnosa_sekunder1"`
		AsesmedDiagS2 string `json:"diagnosa_sekunder2"`
		AsesmedDiagS3 string `json:"diagnosa_sekunder3"`
		AsesmedDiagS4 string `json:"diagnosa_sekunder4"`
		AsesmedDiagS5 string `json:"diagnosa_sekunder5"`
		AsesmedDiagS6 string `json:"diagnosa_sekunder6"`
	}

	ResRingkasanPulang struct {
		Pasien          report.DProfilePasien  `json:"pasien"`
		DeskripsiPulang report.RingkasanPulang `json:"desc_pulang"`
	}

	ResReportIntervensiResikoJatuh struct {
		ResikoJatuh []rme.DIntervensiResikoJatuh `json:"resiko_jatuh"`
	}

	ResponseKeluhanUtama struct {
		KeluhanUtama string `json:"keluhan_utama"`
	}

	ResTandaVital struct {
		JalanNapas string `json:"jalan_napas"`
		Td         string `json:"td"`
		Pernafasan string `json:"pernafasan"`

		Nadi         string `json:"nadi"`
		Spo2         string `json:"spo2"`
		E            string `json:"e"`
		V            string `json:"v"`
		M            string `json:"m"`
		ReflexCahaya string `json:"cahaya"`
		Suhu         string `json:"suhu"`
		Akral        string `json:"akral"`
		PupilKanan   string `json:"pupil_kanan"`
		PupilKiri    string `json:"pupil_kiri"`
	}

	ReqReport struct {
		NoReg string `json:"no_reg" binding:"required"`
		NoRm  string `json:"no_rm" binding:"required"`
	}

	ReqReportIntervensiResiko struct {
		NoRm  string `json:"no_rm" binding:"required"`
		NoReg string `json:"no_reg" binding:"required"`
	}

	RequestReport struct {
		NoReg string `json:"no_reg"`
	}

	RequestReportAsesmenAwalKeperawatan struct {
		NoRm string `json:"no_rm" binding:"required"`
	}

	RequestAntonioKeperawatan struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	ReqNoRegHarapan struct {
		NoReg string `json:"no_reg" binding:"required"`
		NoRm  string `json:"no_rm" binding:"required"`
	}

	RequestAntonioKebidanan struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	RequestNoreg struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	RequestNoRM struct {
		NoRm string `json:"no_rm"`
	}

	ReporPegkajianRawatInapAnak struct {
		NoRM     string `json:"no_rm"`
		NoReg    string `json:"no_reg"`
		KdBagian string `json:"kd_bagian"`
	}

	ResponsePengkajianAwalKeperwatanAnak struct {
		Perawat soap.PengkajianAwalKeperawatan `json:"asesmen"`
	}

	SkalaFlacc struct {
		Wajah     int `json:"wajah"`
		Kaki      int `json:"kaki"`
		Aktivitas int `json:"aktivitas"`
		Menangis  int `json:"menangis"`
		Bersuara  int `json:"bersuara"`
	}

	ImageLokalis struct {
		Image string `json:"image"`
	}

	NyeriResponse struct {
		SkalaNyeri int    `json:"skala_nyeri"`
		SkalaP     string `json:"skala_p"`
		SkalaQ     string `json:"skala_q"`
		SkalaR     string `json:"skala_r"`
		SkalaS     string `json:"skala_s"`
		SkalaT     string `json:"skala_t"`
	}

	PemeriksaanFisik struct {
		Mata           string `json:"mata"`
		Tht            string `json:"tht"`
		Mulut          string `json:"mulut"`
		Gigi           string `json:"gigi"`
		Leher          string `json:"leher"`
		Thyroid        string `json:"thyroid"`
		LeherLainnya   string `json:"leherlain"`
		Dada           string `json:"dada"`
		DindingDada    string `json:"dinding_dada"`
		Jantung        string `json:"jantung"`
		Paru           string `json:"paru"`
		Perut          string `json:"perut"`
		PeristatikUsus string `json:"peristik_usus"`

		SuaraJantung        string `json:"suara_jantung"`
		SuaraParu           string `json:"suara_paru"`
		DindingPerut        string `json:"dinding_paru"`
		Hati                string `json:"hati"`
		Lien                string `json:"lien"`
		Peristaltik         string `json:"persitaltik"`
		AbdomenLainnya      string `json:"abdomen_lain"`
		Kulit               string `json:"kulit"`
		Ginjal              string `json:"ginjal"`
		Genetalia           string `json:"genetalia"`
		EkstremitasSuperior string `json:"superior"`
		EkstremitasInferior string `json:"inferior"`
		Namaperawat         string `json:"perawat"`
		Namadokter          string `json:"dokter"`
	}

	// RESPONSE

	ResponseRingksanPulangIGD struct {
		NoReg                   string                   `json:"no_reg"`
		TanggalMasuk            string                   `json:"tgl_masuk"`
		JamKedatangan           string                   `json:"jam_kedatangan"`
		NoRM                    string                   `json:"no_rm"`
		NamaPasien              string                   `json:"nama_pasien"`
		AlergiObat              string                   `json:"alergi_obat"`
		AlergiMakanan           string                   `json:"alergi_makanan"`
		AlergiLainnya           string                   `json:"alergi_lainnya"`
		ReaksiAlergi            string                   `json:"reaksi_alergi"`
		AlasanMasukRumahSakit   string                   `json:"alasan_masuk"`
		KondisiPasienSaatPulang string                   `json:"kondisi"`
		Diagnosa                []soap.DiagnosaResponse  `json:"diagnosa"`
		Tindakan                []soap.TindakanResponse  `json:"tindakan"`
		Perawat                 user.UserPerawat         `json:"perawat"`
		Obat                    []his.DApotikKeluarObat1 `json:"obat"`
	}

	ResponseAlergi struct {
		Id       string
		Kelompok string
		Alergi   string
		KdBagian string
	}

	KeluhanUtamaFromDokter struct {
		Noreg    string
		TglMasuk string
		Keluhan  string
		RwtSkrg  string
	}
)
