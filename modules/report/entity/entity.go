package entity

import (
	"hms_api/modules/his"
	"hms_api/modules/lib"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/rme"
	rmeDTO "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/user"
)

type ReportRepository interface {
	// ALGERI
	GetKeluhanUtamaFromDokterRepository(noReg string) (res dto.KeluhanUtamaFromDokter, err error)
	GetResponseAlergi(noRM string) (res []dto.ResponseAlergi, err error)
	FindDregisterPasienRepositpru(noReg string) (res report.DRegisterPasien, err error)
	OnUpdateNoregDImplementasiKeperawatan(noReg string, noDaskep string, id int) (res rme.DImplementasiKeperawatan, err error)
	OnGetAllDImplementasiKeperawatan() (res []rme.DImplementasiKeperawatan, err error)
	OnGetRiwayatAlergiReporitory(noRM string) (res []rme.DAlergi, err error)
	//=====
	GetDataPasienRepository(NoRm string) (res report.DProfilePasien, err error)
	GetDataDpemFisikRepository(noReg string) (res report.DPemFisik, err error)
	GetDataSoapDokterRepository(Noreg string, KdBagian string) (res report.SoapDokter, err error)
	GetRingkasanPulangRepository(noReg string, kdBagian string) (res report.RingkasanPulang, err error)
	GetSoapPerawatRepository(noReg string, kdBagian string) (res soap.DcpptSoapPasienModel, err error)
	GetProfilePasienRepository(NoRm string) (res report.DProfilePasien, err error)

	// RESIKO JATUH
	// ==== REPORT INTERVENSI RESIKO JATUH ====== //
	GetDataIntervensiIntervensiResikoJatuhReporitory(kdBagian string, noReg string) (res []rme.DIntervensiRisiko, err error)
	GetIntervensiResikojatuhReporitory(noReg string, kdBagian string) (res []rme.DIntervensiResikoJatuh, err error)
	GetDataResikoJatuhAnakRepository(noReg string, kdBagian string) (res report.ResikoJatuhAnak, err error)
	GetResikojatuhMorseRepository(noReg string, kdBagian string) (res report.ResikoJatuhMorse, err error)

	// ===================== REPORT TRIASE ===================
	GetTriaseReporitory(noReg string, userID string, kdBagian string, kategori string) (res report.TriaseReport, err error)
	GetSoapDokterRepository(noReg string, kdBagian string) (res report.SoapDokerResponse, err error)
	GetPemeriksaanFisikIGDDokter(noReg string, kdBagian string) (res dto.PemeriksaanFisik, err error)
	// === keperawatan bidan
	GetAsesmenKeperawatanBidanIGDReporitory(noReg string, kdBagian string) (res report.KeperawatanBidanIGD, err error)
	GetResikoJatuhDewasaRepository(noReg string, kdBagian string) (res report.ResikoJatuhDewasa, err error)
	GetReportPerkembanganPasienRepository(noReg string) (res []report.ViewPerkembanganPasien, err error)
	GetViewIntervensiRepository(noRM string) (res []report.ViewIntervensi, err error)
	GetIntervensiRepository(noRM string) (res []report.DasKepDiagnosaModel, err error)

	// INTERVENSI
	ViewIntervensiRepository(noRM string) (res []report.ViewIntervensi, err error)
	GetDaskepSikiRepository(noDaskep string) (res []report.DaskepSikiModel, err error)
	GetReportCPPTPasien(noRM string) (res []report.ReportCPPTPasien, err error)

	// === resiko jatuh
	GetTindakanDiagnosaRepository(noDaskep string) (res []report.DImplementasiTindakanKeperawatan, err error)
	FindDaskepDiagnosaRepository(noDaskep string) (res rme.DasKepDiagnosaTable, err error)
	GetReasesmenResikoJatuhAnakRepository(noReg string, kdBagian string) (res report.ResikoJatuhDewasa, err error)
	OnGetDImplementasiKeperawatanRepositoryByNoreg(noReg string) (res []rme.DImplementasiKeperawatan, err error)
}

type ReportMapper interface {
	// ToMapperReportTriase(triase report.TriaseReport, kel soap.KeluhanUtamaPerawatIGD) (data dto.ResTriaseResponse)
	ToMapperReportRingksanPulangIGD(register report.DRegisterPasien, alergi []dto.ResponseAlergi, keluhan dto.KeluhanUtamaFromDokter, diagnosa []soap.DiagnosaResponse, tindakan []soap.TindakanResponse, asesmenPerawat soap.AsesemenIGD, perawat user.UserPerawat, obat []his.DApotikKeluarObat1) (data dto.ResponseRingksanPulangIGD)
	ToMapperReportTriase(kel soap.KeluhanUtamaPerawatIGD, triase report.TriaseReport, alergi []rme.DAlergi, dokter report.SoapDokerResponse, fisik dto.PemeriksaanFisik, bidan report.KeperawatanBidanIGD, riwayat []soap.RiwayatPenyakit) (data dto.ResTriaseResponse)
	ToMapperReportRingkasanPulang(pasien report.DProfilePasien, pulang report.RingkasanPulang) (data dto.ResRingkasanPulang)
	ToMapperReportPengkajianPasienRawatInapAnak(pasien report.DProfilePasien) (data dto.ResRawatInapAnak)
	ToMapperReportIntervensiResikoJatuh(intervensi []rme.DIntervensiResikoJatuh) (data dto.ResReportIntervensiResikoJatuh)

	// reprot
	ToMapperReportAsesmenAwalKeperawatanIGD(alergi []rme.DAlergi) (data dto.ResponseAsesmenAwalKeperawatanIGD)
	ToMappingPengkajianAnakKeperawatanRawatInap(data soap.PengkajianAwalKeperawatan) (res dto.ResponsePengkajianAwalKeperwatanAnak)

	// MAPPING DATA
	ToResponseMapperCPPT(pasien user.ProfilePasien, cppt []report.ReportCPPTPasien, kPelayanan lib.KPelayanan) (res dto.ResponseCPPTWithPasien)
}

type ReportUsecase interface {
	OnGetReportImplementasiKeperawatanAntonioUseCase() (message string)
	GetIntervensiPasienUsecase(noRM string) (res []report.ResponseIntervensi, err error)
	GetIntervensiPasienUsecaseV2(noRM string, kdBagian string) (res report.ResponseIntervensiV2, err error)
	OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(noReg string) (res rmeDTO.ResponsePengkajianIGDKeperawatanAntonio, err error)

	// === report asesmen keperawatan igd
	OnGetReportAsesmenAwalKeperawatanIGDANtonioUseCase(noRM string) (res dto.ResponseAsesmenAwalKeperawatanIGD, err error)

	// ================================= REPORT PENGKAJIAN AWAL ANAK
	OnReportPengkajianAwalAnakUseCase(noReg string, kdBagian string) (res dto.ResponsePengkajianAwalKeperwatanAnak, err error)
}
