package handler

import (
	"hms_api/modules/report/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (dh *ReportHandler) OnGetReportAsesemenAwalDokterAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestAntonioKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportUsecase.OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) ReportAsesmenAwalKebidananAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestAntonioKebidanan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportUsecase.OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) OnReportDImplementasiKeperawatanAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportRepository.OnGetDImplementasiKeperawatanRepositoryByNoreg(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *ReportHandler) OnUpdateImplementasiKeperawatanFiberHandler(c *fiber.Ctx) error {
	message := sh.ReportUsecase.OnGetReportImplementasiKeperawatanAntonioUseCase()

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}
