package handler

import (
	hisEntity "hms_api/modules/his/entity"
	libEntity "hms_api/modules/lib/entity"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/report/entity"
	rmeEntity "hms_api/modules/rme/entity"
	soapEntity "hms_api/modules/soap/entity"
	userEntity "hms_api/modules/user/entity"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type ReportHandler struct {
	ReportRepository entity.ReportRepository
	UserRepository   userEntity.UserRepository
	SoapRepository   soapEntity.SoapRepository
	RMERepository    rmeEntity.RMERepository
	ReportUsecase    entity.ReportUsecase
	Logging          *logrus.Logger
	HisRepository    hisEntity.HisRepository
	ReportMapper     entity.ReportMapper
	UserMapper       userEntity.IUserMapper
	LibRepository    libEntity.LibRepository
}

func (dh *ReportHandler) ReportTriaseIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// GET KELUHAN UTAMA // GET ALERGI
	triase, _ := dh.ReportRepository.GetTriaseReporitory(payload.NoReg, userID, modulID, "Perawat")
	keluhan, _ := dh.SoapRepository.GetKeluhanUtamaIGDRepository(payload.NoReg, modulID)
	alergi, _ := dh.RMERepository.GetRiwayatAlergiRepository(payload.NoRm)
	soapDokter, _ := dh.ReportRepository.GetSoapDokterRepository(payload.NoReg, modulID)

	fisik, _ := dh.ReportRepository.GetPemeriksaanFisikIGDDokter(payload.NoReg, modulID)
	asesmenBidan, _ := dh.ReportRepository.GetAsesmenKeperawatanBidanIGDReporitory(payload.NoReg, modulID)

	// riwayat, _ := dh.SoapRepository.GetRiwayatPenyakitSebelumnya(payload.NoRm)
	riwayat, _ := dh.SoapRepository.GetRiwayatPenyakitSebelumnya(payload.NoRm)

	// GET SOAP DOKTER

	dh.Logging.Info(triase)

	mapper := dh.ReportMapper.ToMapperReportTriase(keluhan, triase, alergi, soapDokter, fisik, asesmenBidan, riwayat)

	dh.Logging.Info("OK")

	response := helper.APIResponse("Ok", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) ReportRingkasanPulangIGDFiberhandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	dh.Logging.Info("GET DATA RINGKASAN PULANG")
	// modulID := c.Locals("modulID").(string)
	// report, _ := dh.ReportRepository.GetDataPasienRepository(payload.NoRm)
	// AMBIL DATA RINGKASAN PULANG
	// pulangDetail, _ := dh.ReportRepository.GetRingkasanPulangRepository(payload.NoReg, modulID)
	// dh.Logging.Info(report)

	// if len(report.Id) > 1 {
	// 	report.Tgllahir = report.Tgllahir[0:10]
	// }

	// mapper := dh.ReportMapper.ToMapperReportRingkasanPulang(report, pulangDetail)

	response := helper.APIResponse("Ok", http.StatusOK, "mapper")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) ReportPengkajianRawatInapAnak(c *fiber.Ctx) error {
	payload := new(dto.ReqReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// report pengkajian anak
	// report, err2 := dh.ReportRepository.GetDataPasienRepository(payload.NoRm)

	// if len(report.Id) > 1 {
	// 	report.Tgllahir = report.Tgllahir[0:10]
	// }

	// if err2 != nil {
	// 	dh.Logging.Error(err2.Error())
	// 	response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// mapper := dh.ReportMapper.ToMapperReportPengkajianPasienRawatInapAnak(report)

	dh.Logging.Info("OK")

	response := helper.APIResponse("Ok", http.StatusOK, "mapper")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) ReportIntervensiResikoJatuhHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportIntervensiResiko)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ===== //
	modulID := c.Locals("modulID").(string)

	data, ers := dh.ReportRepository.GetIntervensiResikojatuhReporitory(payload.NoReg, modulID)

	dh.Logging.Info("DATA INTERVENSI RESKO JATUH")

	dh.Logging.Info(data)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := dh.ReportMapper.ToMapperReportIntervensiResikoJatuh(data)

	response := helper.APIResponse("Ok", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (dh *ReportHandler) GetResikoJatuhAnakHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ===== //
	modulID := c.Locals("modulID").(string)

	data, ers := dh.ReportRepository.GetDataResikoJatuhAnakRepository(payload.NoReg, modulID)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) OnGetReasesmenResikoJatuhAnakHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ===== //
	modulID := c.Locals("modulID").(string)
	data, ers := dh.ReportRepository.GetDataResikoJatuhAnakRepository(payload.NoReg, modulID)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetReportPelaksanaanPasienHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ===== //
	// modulID := c.Locals("modulID").(string)

	data, errs := dh.ReportRepository.GetReportPerkembanganPasienRepository(payload.NoReg)

	if errs != nil {
		dh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) ViewIntervensiPasienhandler(c *fiber.Ctx) error {
	payload := new(dto.RequestNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ===== //
	// modulID := c.Locals("modulID").(string)

	data, errs := dh.ReportRepository.GetViewIntervensiRepository(payload.NoRm)

	if errs != nil {
		dh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetViewIntervensiPasienHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := dh.ReportUsecase.GetIntervensiPasienUsecase(payload.NoRm)

	if errs != nil {
		dh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetViewIntervensiPasienHandlerV2(c *fiber.Ctx) error {
	var noRM = c.Query("no_rm")
	var kdbagian = c.Query("kd_bagian")

	dh.Logging.Info(noRM)

	data, errs := dh.ReportUsecase.GetIntervensiPasienUsecaseV2(noRM, kdbagian)

	if errs != nil {
		dh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetResikoJatuhMorseHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, ers := dh.ReportRepository.GetResikojatuhMorseRepository(payload.NoReg, modulID)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetResikoJatuhDewasaHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, ers := dh.ReportRepository.GetResikoJatuhDewasaRepository(payload.NoReg, modulID)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetReasesmenResikoJatuhAnakHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestReport)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, ers := dh.ReportRepository.GetReasesmenResikoJatuhAnakRepository(payload.NoReg, modulID)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetReportCpptPasienHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, ers := dh.ReportRepository.GetReportCPPTPasien(payload.NoRm)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		var datas = make([]report.ReportCPPTPasien, 0)
		response := helper.APIResponse("Ok", http.StatusOK, datas)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("Ok", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) GetReportCpptPasienHandlerV2(c *fiber.Ctx) error {
	payload := new(dto.RequestNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		dh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DATA USER
	// pasien, _ := su.userRepository.OnGetProfilePasienRepository(reg.NoRM
	modulID := c.Locals("modulID").(string)
	data, ers := dh.ReportRepository.GetReportCPPTPasien(payload.NoRm)
	pasien, _ := dh.UserRepository.OnGetProfilePasienRepository(payload.NoRm)
	bagian, _ := dh.LibRepository.OnGetKpelayananRepositoryByKode(modulID)

	// mapperPasien := su.userMapper.ToMappingProfilePasien(pasien)
	dh.Logging.Info("DATA PASIEN")
	dh.Logging.Info(pasien)

	mapperPasien := dh.UserMapper.ToMappingProfilePasien(pasien)

	if ers != nil {
		dh.Logging.Error(ers.Error())
		response := helper.APIResponseFailure(ers.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := dh.ReportMapper.ToResponseMapperCPPT(mapperPasien, data, bagian)

	response := helper.APIResponse("Ok", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) OnGetReportAsesmenAwalBatuRajaKeperawatanHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestAntonioKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportUsecase.OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (dh *ReportHandler) OnGetReportAsesmenAwalHarapanKeperawatanHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoRegHarapan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportUsecase.OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *ReportHandler) OnGetRingksanPulangIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoRegHarapan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, _ := uh.ReportRepository.FindDregisterPasienRepositpru(payload.NoReg)
	alergi, _ := uh.ReportRepository.GetResponseAlergi(payload.NoRm)
	keluhan, _ := uh.ReportRepository.GetKeluhanUtamaFromDokterRepository(payload.NoReg)
	dignosa, _ := uh.SoapRepository.GetDiagnosaRepositoryBangsal(payload.NoReg, "IGD001")
	tindakan, _ := uh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.NoReg, "IGD001")
	asesmenPerawat, _ := uh.SoapRepository.OnGetAsesmenIGDRepository("IGD001", payload.NoReg)
	perawat, _ := uh.UserRepository.CariUserPerawatRepository(asesmenPerawat.InsertUserId)
	obat, _ := uh.HisRepository.OnGetInstruksiMedisFarmataologiRepository(payload.NoRm, payload.NoReg)
	// OnGetInstruksiMedisFarmataologiRepository
	// asesmenPerawat.ase

	// value, err := sh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.Noreg, modulID)

	mapper := uh.ReportMapper.ToMapperReportRingksanPulangIGD(data, alergi, keluhan, dignosa, tindakan, asesmenPerawat, perawat, obat)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}
