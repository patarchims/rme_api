package handler

import (
	"fmt"
	"hms_api/modules/report/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (dh *ReportHandler) OnReportPengkajianAwalMedisDokterHarapan(c *fiber.Ctx) error {
	payload := new(dto.RequestNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat diproses", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er1 := dh.ReportRepository.OnGetDImplementasiKeperawatanRepositoryByNoreg(payload.NoReg)

	if er1 != nil {
		dh.Logging.Error(er1.Error())
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}
