package mapper

import (
	"hms_api/modules/lib"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/user"
)

func (r *ReportImple) ToResponseMapperCPPT(pasien user.ProfilePasien, cppt []report.ReportCPPTPasien, pelayanan lib.KPelayanan) (res dto.ResponseCPPTWithPasien) {

	if len(cppt) == 0 {
		return dto.ResponseCPPTWithPasien{
			CPPT:      make([]report.ReportCPPTPasien, 0),
			Pasien:    pasien,
			Pelayanan: pelayanan,
		}
	}

	return dto.ResponseCPPTWithPasien{
		CPPT:      cppt,
		Pasien:    pasien,
		Pelayanan: pelayanan,
	}
}
