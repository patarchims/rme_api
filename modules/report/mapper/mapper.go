package mapper

import (
	"hms_api/modules/his"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/report/entity"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
)

type ReportImple struct{}

func NewReportImple() entity.ReportMapper {
	return &ReportImple{}
}

// REPORT MAPPER
func (r *ReportImple) ToMapperReportTriase(kel soap.KeluhanUtamaPerawatIGD, triase report.TriaseReport, alergi []rme.DAlergi, dokter report.SoapDokerResponse, fisik dto.PemeriksaanFisik, bidan report.KeperawatanBidanIGD, riwayat []soap.RiwayatPenyakit) (data dto.ResTriaseResponse) {

	if dokter.AsesmedLokalisImage == "" {
		dokter.AsesmedLokalisImage = "lokalis_default.png"
	}

	if len(riwayat) == 0 {
		return dto.ResTriaseResponse{
			KeluhanUtama:    kel.AseskepKel,
			Triase:          triase,
			Alergi:          alergi,
			Fisik:           fisik,
			SoapDokter:      dokter,
			AsesmenBidan:    bidan,
			RiwayatPenyakit: make([]soap.RiwayatPenyakit, 0),
		}
	}

	if riwayat == nil {
		return dto.ResTriaseResponse{
			KeluhanUtama:    kel.AseskepKel,
			Triase:          triase,
			Alergi:          alergi,
			Fisik:           fisik,
			SoapDokter:      dokter,
			AsesmenBidan:    bidan,
			RiwayatPenyakit: make([]soap.RiwayatPenyakit, 0),
		}
	}

	return dto.ResTriaseResponse{
		KeluhanUtama:    kel.AseskepKel,
		Triase:          triase,
		Alergi:          alergi,
		Fisik:           fisik,
		SoapDokter:      dokter,
		AsesmenBidan:    bidan,
		RiwayatPenyakit: riwayat,
	}
}

func (r *ReportImple) ToMapperReportRingkasanPulang(pasien report.DProfilePasien, pulang report.RingkasanPulang) (data dto.ResRingkasanPulang) {

	return dto.ResRingkasanPulang{
		Pasien:          pasien,
		DeskripsiPulang: pulang,
	}
}

func (r *ReportImple) ToMapperReportPengkajianPasienRawatInapAnak(pasien report.DProfilePasien) (data dto.ResRawatInapAnak) {
	return dto.ResRawatInapAnak{
		Pasien: pasien,
	}
}

func (r *ReportImple) ToMapperReportIntervensiResikoJatuh(intervensi []rme.DIntervensiResikoJatuh) (data dto.ResReportIntervensiResikoJatuh) {
	if len(intervensi) == 0 {
		return dto.ResReportIntervensiResikoJatuh{
			ResikoJatuh: make([]rme.DIntervensiResikoJatuh, 0),
		}
	}

	return dto.ResReportIntervensiResikoJatuh{
		ResikoJatuh: intervensi,
	}
}

func (r *ReportImple) ToMappingPengkajianAnakKeperawatanRawatInap(data soap.PengkajianAwalKeperawatan) (res dto.ResponsePengkajianAwalKeperwatanAnak) {
	return dto.ResponsePengkajianAwalKeperwatanAnak{
		Perawat: data,
	}
}

func (r *ReportImple) ToMapperReportAsesmenAwalKeperawatanIGD(alergi []rme.DAlergi) (data dto.ResponseAsesmenAwalKeperawatanIGD) {

	if len(alergi) == 0 {
		return dto.ResponseAsesmenAwalKeperawatanIGD{
			Alergi: make([]rme.DAlergi, 0),
		}
	}

	return dto.ResponseAsesmenAwalKeperawatanIGD{
		Alergi: alergi,
	}
}

func (r *ReportImple) ToMapperReportRingksanPulangIGD(register report.DRegisterPasien, alergi []dto.ResponseAlergi, keluhan dto.KeluhanUtamaFromDokter, diagnosa []soap.DiagnosaResponse, tindakan []soap.TindakanResponse, asesmenPerawat soap.AsesemenIGD, perawat user.UserPerawat, daftarObat []his.DApotikKeluarObat1) (data dto.ResponseRingksanPulangIGD) {
	obat := func(r dto.ResponseAlergi) bool {
		return r.Kelompok == "obat"
	}

	lain := func(r dto.ResponseAlergi) bool {
		return r.Kelompok == "lain"
	}

	makanan := func(r dto.ResponseAlergi) bool {
		return r.Kelompok == "lain"
	}

	var tindakans []soap.TindakanResponse
	var diagnosas []soap.DiagnosaResponse
	var daftarObats []his.DApotikKeluarObat1

	if len(tindakan) == 0 {
		tindakans = make([]soap.TindakanResponse, 0)
	} else {
		tindakans = tindakan
	}

	if len(diagnosa) == 0 {
		diagnosas = make([]soap.DiagnosaResponse, 0)
	} else {
		diagnosas = diagnosa
	}

	if len(daftarObat) == 0 {
		daftarObats = make([]his.DApotikKeluarObat1, 0)
	} else {
		daftarObats = daftarObat
	}

	// Apply the filter
	filter := FilterResponseAlergi(alergi, obat)
	filterLain := FilterResponseAlergi(alergi, lain)
	filterMakanan := FilterResponseAlergi(alergi, makanan)

	var alergiObat = ""
	var alergiLain = ""
	var alergiMakanan = ""

	if len(filter) > 0 {
		alergiObat = filter[0].Alergi
	} else {
		alergiObat = ""
	}

	if len(filterLain) > 0 {
		alergiLain = filterLain[0].Alergi
	} else {
		alergiLain = ""
	}

	if len(filterMakanan) > 0 {
		alergiMakanan = filterMakanan[0].Alergi
	} else {
		alergiMakanan = ""
	}

	return dto.ResponseRingksanPulangIGD{
		NoReg:                   register.Noreg,
		TanggalMasuk:            register.Tanggal,
		JamKedatangan:           register.Jam,
		NamaPasien:              register.Nama,
		NoRM:                    register.Id,
		AlergiObat:              alergiObat,
		AlergiLainnya:           alergiLain,
		AlergiMakanan:           alergiMakanan,
		AlasanMasukRumahSakit:   keluhan.Keluhan,
		Diagnosa:                diagnosas,
		Tindakan:                tindakans,
		ReaksiAlergi:            asesmenPerawat.AseskepReaksiAlergi,
		Perawat:                 perawat,
		KondisiPasienSaatPulang: asesmenPerawat.AseskepPulang1,
		Obat:                    daftarObats,
	}
}

func FilterResponseAlergi(items []dto.ResponseAlergi, filterFunc func(dto.ResponseAlergi) bool) []dto.ResponseAlergi {
	var filteredItems []dto.ResponseAlergi
	for _, item := range items {
		if filterFunc(item) {
			filteredItems = append(filteredItems, item)
		}
	}
	return filteredItems
}
