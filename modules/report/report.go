package report

import (
	"hms_api/modules/lib"
	"hms_api/modules/user"
)

type (
	// DATA PASIEN
	DProfilePasien struct {
		Nik          string `json:"nik"`
		Id           string `json:"id"`
		Firstname    string `json:"firstname"`
		Jeniskelamin string `json:"jenis_kelamin"`
		TempatLahir  string `json:"tempat_lahir"`
		Tgllahir     string `json:"tgl_lahir"`
		Umurth       int    `json:"umur_thn"`
		Umurbln      int    `json:"umur_bln"`
		Suku         string `json:"suku"`
		Alamat       string `json:"alamat"`
		Agama        string `json:"agama"`
		Kelurahan    string `json:"keluarahan"`
		Kabupaten    string `json:"kabupaten"`
		Propinsi     string `json:"propinsi"`
		Negara       string `json:"negara"`
		Telp         string `json:"telp"`
		Pendidikan   string `json:"pendidikan"`
		Pekerjaan    string `json:"pekerjaan"`
		Status       string `json:"status"`
		Peksuami     string `json:"peksuami"`
		Kunjungan    string `json:"kunjungan"`
		Hp           string `json:"hp"`
		Namaayah     string `json:"nama_ayah"`
		Namaibu      string `json:"nama_ibu"`
	}

	// DPEMFISIK
	DPemFisik struct {
		Kategori    string
		Noreg       string
		KdBagian    string
		Akral       string
		Kesadaran   string
		Kepala      string
		Rambut      string
		Wajah       string
		KeadaanUmum string
		JalanNafas  string
		Td          string
		Pernafasan  string
		Nadi        string

		Suhu        string
		Spo2        string
		CahayaKanan string
		CahayaKiri  string

		// GSC
		GcsE string
		GcsM string
		GcsV string
		// ==================== Kepala
		Gangguan string
		// GangguanDetail      string
		Kulit               string
		Abdomen             string
		Ginjal              string
		GinjalDetail        string
		AbdomenLainnya      string
		PeristatikUsus      string
		Thyroid             string
		Hati                string
		Paru                string
		Mata                string
		Tht                 string
		Telinga             string
		Hidung              string
		Mulut               string
		Gigi                string
		Lidah               string
		Tenggorokan         string
		Leher               string
		Lien                string
		LeherLainnya        string
		Dada                string
		Respirasi           string
		Perut               string
		Jantung             string
		Integument          string
		Ekstremitas         string
		EkstremitasSuperior string
		EkstremitasInferior string
		KemampuanGenggam    string
		Genetalia           string
		Anus                string
		Punggung            string
		LainLain            string

		// puppil
		PupilKiri  string
		PupilKanan string

		// ======================================= PENAMBAHAN PEMERIKSAAN FISIK PADA ANAK
		Dindingdada                string
		DindingdadaRetEpigastrium  string
		DindingdadaRetSuprastermal string
		DindingdadaRetraksi        string
		Hepar                      string
		HeparDetail                string
		Limpa                      string
		LimpaDetail                string
		Ouf                        string
		TugorKulit                 string

		PemeriksaanFisik string

		SkalaNyeri     int
		SkalaNyeriP    string
		SkalaNyeriQ    string
		SkalaNyeriR    string
		SkalaNyeriS    string
		SkalaNyeriT    string
		FlaccWajah     int
		FlaccKaki      int
		FlaccAktifitas int
		FlaccMenangis  int
		FlaccBersuara  int
		FlaccTotal     int
		SkalaTriase    int
	}

	SoapDokter struct {
		InsertDttm            string `json:"insertDttm"`
		KdBagian              string `json:"kdBagian"`
		Noreg                 string `json:"noreg"`
		AsesmedKonsulKeAlasan string `json:"alasan_konsul"`
		AsesmedTerapi         string `json:"terapi"`
		AsesmedAlasanOpname   string `json:"alasan_opname"`
		AsesmedKonsulKe       string `json:"konsul_ke"`
		AsesmenPrognosis      string `json:"prognosis"`
		TglMasuk              string `json:"tgl_masuk"`
		JamMasuk              string `json:"jam_masuk"`
		KdDpjp                string `json:"kd_dpjp"`
		KeteranganPerson      string `json:"person"`
		InsertUserId          string `json:"user_id"`
		InsertPc              string `json:"device_id"`
		Pelayanan             string `json:"pelayanan"`
		AsesmedLokalisImage   string `json:"lokalis_images"`

		// NYERI gorm:"column:age_of_the_beast"`
		AsesmedDiagP  string `gorm:"column:asesmed_diagP" json:"diagnosa_primer"`
		AsesmedDiagS1 string `gorm:"column:asesmed_diagS1" json:"diagnosa_sekunder1"`
		AsesmedDiagS2 string `gorm:"column:asesmed_diagS2" json:"diagnosa_sekunder2"`
		AsesmedDiagS3 string `gorm:"column:asesmed_diagS3" json:"diagnosa_sekunder3"`
		AsesmedDiagS4 string `gorm:"column:asesmed_diagS4" json:"diagnosa_sekunder4"`
		AsesmedDiagS5 string `gorm:"column:asesmed_diagS5" json:"diagnosa_sekunder5"`
		AsesmedDiagS6 string `gorm:"column:asesmed_diagS6" json:"diagnosa_sekunder6"`
	}

	// REPORT RINGKASAN PULANG
	RingkasanPulang struct {
		TglMasuk         string `json:"tgl_masuk"`
		KdBagian         string `json:"kd_bagian"`
		Pelayanan        string `json:"pelayanan"`
		TglKeluar        string `json:"tgl_keluar"`
		AsesmedDokterTtd string `json:"ttd_dokter"`
		Namadokter       string `json:"nama_dokter"`
		AsesmedAnamnesa  string `json:"anamnesa"`
		Primer           string `json:"primer"`
		PrimerDesc       string `json:"primer_desc"`
		AsesmedPemFisik  string `json:"pemfisik"`
		TglSelesai       string `json:"tgl_selesai"`
		JamSelesai       string `json:"jam_selesai"`
		Bagian           string `json:"bagian"`
		AsesmedDiagS1    string `json:"diag_s1"`
		Secundar1        string `json:"secundary1"`
		AsesmedDiagS2    string `json:"asesmed_diags2"`
		Diags2           string `json:"diags2"`
		AsesmedDiagS3    string `json:"asesmed_diags3"`
		DiagS3           string `json:"diag_s3"`
		AsesmedDiagS4    string `json:"asesmed_diags4"`
		DiagS4           string `json:"diag_s4"`
		AsesmedPros1     string `json:"asesmed_pros"`
		Prosedur1        string `json:"prosedur1"`
	}

	// GET RESIKO JATUH ANAK
	ResikoJatuhAnak struct {
		InsertDttm       string `json:"insertDttm"`
		KetPerson        string `json:"ket_person"`
		Pelayanan        string `json:"pelayanan"`
		KdBagian         string `json:"kd_bagian"`
		Noreg            string `json:"noreg"`
		Kategori         string `json:"kategori"`
		Usia             string `json:"usia"`
		JenisKelamin     string `json:"jenis_kelamin"`
		Diagnosis        string `json:"diagnosis"`
		GangguanKognitif string `json:"gangguan_kognitif"`
		FaktorLingkungan string `json:"faktor_lingkungan"`
		Respon           string `json:"respon"`
		PenggunaanObat   string `json:"penggunaan_obat"`
		Namaperawat      string `json:"nama_perawat"`
		Keterangan       string `json:"keterangan"`
		Total            int    `json:"total"`
	}

	ResikoJatuhMorse struct {
		InsertDttm      string `json:"insertDttm"`
		Pelayanan       string `json:"pelayanan"`
		KdBagian        string `json:"kd_bagian"`
		Noreg           string `json:"noreg"`
		Kategori        string `json:"kategori"`
		KetPerson       string `json:"ket_person"`
		RJatuh          string `json:"r_jatuh"`
		Diagnosis       string `json:"diagnosis"`
		BAmbulasi       string `json:"ambulasi"`
		Terapi          string `json:"terapi"`
		TerpasangInfuse string `json:"terpasang_infuse"`
		GayaBerjalan    string `json:"gaya_berjalan"`
		StatusMental    string `json:"status_mental"`
		Total           int    `json:"total"`
	}

	ResikoJatuhDewasa struct {
		InsertDttm     string `json:"insertDttm"`
		Pelayanan      string `json:"pelayanan"`
		KdBagian       string `json:"kd_bagian"`
		Noreg          string `json:"noreg"`
		Kategori       string `json:"kategori"`
		Usia           string `json:"usia"`
		RJatuh         string `json:"r_jatuh"`
		Aktivitas      string `json:"aktivitas"`
		Mobilisasi     string `json:"mobilitas"`
		Kognitif       string `json:"kognitif"`
		DefisitSensori string `json:"defisit_sensori"`
		Pengobatan     string `json:"pengobatan"`
		Komorbiditas   string `json:"komorbiditas"`
		Total          int    `json:"total"`
		Namaperawat    string `json:"nama_perawat"`
	}

	SoapDokerResponse struct {
		InsertDttm          string `json:"inser_dttm"`
		KeteranganPerson    string `json:"ket_person"`
		InsertUserId        string `json:"user_id"`
		TglMasuk            string `json:"tgl_masuk"`
		Pelayanan           string `json:"pelayanan"`
		KdBagian            string `json:"kd_bagian"`
		Noreg               string `json:"noreg"`
		KdDpjp              string `json:"kd_dpjp"`
		AsesmedKeluhUtama   string `json:"keluhan_utama"`
		AsesmedTelaah       string `json:"telaah"`
		AsesmedKesadaran    string `json:"kesadaran"`
		AsesmedJenpel       string `json:"jenpel"`
		AsesmedLokalisImage string `json:"image_lokalis"`
		Namadokter          string `json:"nama_dokter"`
		P                   string `json:"primari"`
		Desp                string `json:"des_p"`
		S1                  string `json:"s1"`
		Des1                string `json:"des1"`
		S2                  string `json:"s2"`
		Des2                string `json:"des2"`
		S3                  string `json:"s3"`
		Des3                string `json:"des3"`
		S4                  string `json:"s4"`
		Des4                string `json:"des4"`
		S5                  string `json:"s5"`
		Des5                string `json:"des5"`
		S6                  string `json:"s6"`
		Des6                string `json:"des6"`
		AsesmedAlasanOpname string `json:"alasan_opname"`
		AsesmedTerapi       string `json:"terapi"`
		AsesmedKonsulKe     string `json:"konsul_ke"`
	}

	TriaseReport struct {
		InsertUserId   string `json:"user_id"`
		InsertDttm     string `json:"insert_dttm"`
		KdBagian       string `json:"kd_bagian"`
		Kategori       string `json:"kategori"`
		Pelayanan      string `json:"pelayanan"`
		Noreg          string `json:"noreg"`
		JalanNafas     string `json:"jalan_nafas"`
		PupilKiri      string `json:"pupil_kiri"`
		PupilKanan     string `json:"pupil_kanan"`
		Refleks        string `json:"refleks"`
		Td             string `json:"td"`
		Nadi           string `json:"nadi"`
		Suhu           string `json:"suhu"`
		Pernafasan     string `json:"pernafasan"`
		Spo2           string `json:"spo2"`
		GcsE           string `json:"gcs_e"`
		GcsV           string `json:"gcs_v"`
		GcsM           string `json:"gcs_m"`
		Akral          string `json:"akral"`
		CahayaKanan    string `json:"cahaya_kanan"`
		CahayaKiri     string `json:"cahaya_kiri"`
		Gangguan       string `json:"gangguan"`
		GangguanDetail string `json:"gangguan_detail"`
		SkalaNyeri     int    `json:"nyeri"`
		SkalaNyeriP    string `json:"nyeri_p"`
		SkalaNyeriQ    string `json:"nyeri_q"`
		SkalaNyeriR    string `json:"nyeri_r"`
		SkalaNyeriS    string `json:"nyeri_s"`
		SkalaNyeriT    string `json:"nyeri_t"`
		FlaccWajah     int    `json:"wajah"`
		FlaccKaki      int    `json:"kaki"`
		FlaccAktifitas int    `json:"aktifitas"`
		FlaccMenangis  int    `json:"menangis"`
		FlaccBersuara  int    `json:"bersuara"`
		SkalaTriase    int    `json:"skala_triase"`
		Namaperawat    string `json:"nama_perawat"`
		Namadokter     string `json:"nama_dokter"`
	}

	// REPORT ASESMEN KEPERAWATAN BIDAN
	KeperawatanBidanIGD struct {
		InsertUserId                string `json:"user_id"`
		InsertDttm                  string `json:"insert_dttm"`
		KdBagian                    string `json:"kd_bagian"`
		Pelayanan                   string `json:"pelayanan"`
		Noreg                       string `json:"noreg"`
		AseskepPerolehanInfo        string `json:"info"`
		AseskepPerolehanDetail      string `json:"info_detail"`
		AseskepRwytPnykt            string `json:"rwt_penyakit"`
		AseskepCaraMasuk            string `json:"cara_masuk"`
		AseskepAsalMasuk            string `json:"asal_masuk"`
		AseskepSkalaNyeri           string `json:"skala_nyeri"`
		AseskepFrekuensiNyeri       string `json:"frekuensi_nyeri"`
		AseskepLamaNyeri            string `json:"lama_nyeri"`
		AseskepNyeriMenjalar        string `json:"menjalar"`
		AseskepNyeriMenjalanDetail  string `json:"menjalar_detail"`
		AseskepKualitasNyeri        string `json:"kualitas_nyeri"`
		AseskepNyeriPemicu          string `json:"pemicu"`
		AseskepNyeriPengurang       string `json:"pengurang"`
		AseskepAsesFungsional       string `json:"fungsional"`
		AseskepAsesFungsionalDetail string `json:"fungsional_detail"`
		AseskepRj1                  string `json:"rj1"`
		AseskepRj2                  string `json:"rj2"`
		AseskepHslKajiRj            string `json:"hasil_rj"`
		AseskepHslKajiRjTind        string `json:"hasil_rj_tind"`
		AseskepDekubitus1           string `json:"dekubitus1"`
		AseskepDekubitus2           string `json:"dekubitus2"`
		AseskepDekubitus3           string `json:"dekubitus3"`
		AseskepDekubitus4           string `json:"dekubitus4"`
		AseskepDekubitusAnak        string `json:"dekubitus_anak"`
		AseskepKehamilan            string `json:"kehamilan"`
		AseskepKehamilanGravida     string `json:"gravida"`
		AseskepKehamilanPara        string `json:"para"`
		AseskepKehamilanAbortus     string `json:"abortus"`
		AseskepKehamilanHpht        string `json:"hpht"`
		AseskepKehamilanTtp         string `json:"ttp"`
		AseskepKehamilanLeopold1    string `json:"leopold1"`
		AseskepKehamilanLeopold2    string `json:"leopold2"`
		AseskepKehamilanLeopold3    string `json:"leopold3"`
		AseskepKehamilanLeopold4    string `json:"leopold4"`
		AseskepPulangKondisi        string `json:"kondisi_pulang"`
		AseskepPulangTransportasi   string `json:"transportasi"`
		AseskepKehamilanDjj         string `json:"djj"`
		AseskepKehamilanVt          string `json:"vt"`
		AseskepPulang1              string `json:"pulang1"`
		AseskepPulang1Detail        string `json:"pulang_detail"`
		AseskepPulang2              string `json:"pulang2"`
		AseskepPulang2Detail        string `json:"pulang2_detail"`
		AseskepPulang3              string `json:"pulang3"`
		AseskepPulang3Detail        string `json:"pulang3_detail"`
		JamCheckOut                 string `json:"jam_check_out"`
		JamCheckInRanap             string `json:"jam_check_in_ranap"`
		CaraKeluar                  string `json:"cara_keluar"`
		Namaperawat                 string `json:"nama_perawat"`
	}

	// PERKEMBAGNAN PASIEN
	ViewPerkembanganPasien struct {
		InsertDttm  string `json:"insert_dttm"`
		Noreg       string `json:"noreg"`
		InsertPc    string `json:"insert_pc"`
		KetPerson   string `json:"person"`
		Namaperawat string `json:"nama_perawat"`
		NoDaskep    string `json:"no_daskep"`
		KdBagian    string `json:"bagian"`
		Intervensi  string `json:"intervensi"`
	}

	ResponseIntervensi struct {
		InsertDttm  string                             `json:"insert_dttm"`
		InsertPc    string                             `json:"insert_pc"`
		Noreg       string                             `json:"noreg"`
		KetPerson   string                             `json:"person"`
		Namaperawat string                             `json:"nama_perawat"`
		NoDaskep    string                             `json:"no_daskep"`
		KodeSdki    string                             `json:"kode_sdki"`
		Id          string                             `json:"no_rm"`
		Siki        []DaskepSikiModel                  `json:"siki"`
		Tindakan    []DImplementasiTindakanKeperawatan `json:"tindakan"`
	}

	ResponseIntervensiV2 struct {
		Pasien     user.ProfilePasien   `json:"pasien"`
		Intervensi []ResponseIntervensi `json:"intervensi"`
		Pelayanan  lib.KPelayanan       `json:"pelayanan"`
	}

	ViewIntervensi struct {
		InsertDttm  string `json:"insert_dttm"`
		InsertPc    string `json:"insert_pc"`
		Noreg       string `json:"noreg"`
		KetPerson   string `json:"person"`
		Namaperawat string `json:"nama_perawat"`
		NoDaskep    string `json:"no_daskep"`
		KodeSdki    string `json:"kode_sdki"`
		Id          string `json:"no_rm"`
	}

	DasKepDiagnosaModel struct {
		UserId     string            `json:"user_id"`
		Tanggal    string            `json:"tanggal"`
		Noreg      string            `gorm:"primaryKey:Noreg" json:"no_reg"`
		Hasil      string            `json:"hasil"`
		KodeSdki   string            `json:"kode_diagnosa"`
		NoDaskep   string            `gorm:"primaryKey:NoDaskep" json:"no_daskep"`
		DRegister  DRegisterPasien   `gorm:"foreignKey:Noreg" json:"dregister"`
		DaskepSIKI []DaskepSikiModel `gorm:"foreignKey:NoDaskep" json:"deskripsi_siki"`
	}

	// DATA DASKEP SLKI
	DaskepSLKIModel struct {
		InsertDttm string `json:"insert_dttm"`
		NoDaskep   string `json:"no_daskep"`
		IdKriteria int    `gorm:"primaryKey:IdKriteria" json:"id_kriteria"`
		KodeSlki   string `json:"kode_slki"`
		NamaSlki   string `json:"nama_sllki"`
		Kategori   string `json:"kategori"`
		NoUrut     int    `json:"no_urut"`
		Hasil      int    `json:"hasil"`
		Target     int    `json:"target"`
		Waktu      int    `json:"waktu"`
	}

	// GET DREGISTER
	DRegisterPasien struct {
		Id      string `json:"id"`
		Noreg   string `gorm:"primaryKey:noreg" json:"noreg"`
		Nama    string `json:"nama"`
		Tanggal string
		Jam     string
	}

	// DATA SIKI
	DaskepSikiModel struct {
		InsertDttm string `json:"insert_dttm"`
		NoDaskep   string `json:"no_daskep"`
		IdSiki     int    `json:"id_siki"`
		KodeSiki   string `json:"kode_siki"`
		Nama       string `json:"nama_siki"`
		Kategori   string `json:"kategori"`
		NoUrut     int    `json:"no_urut"`
	}

	ReportCPPTPasien struct {
		InsertDttm    string `json:"insert_dttm"`
		KdBagian      string `json:"kd_bagian"`
		Tanggal       string `json:"tanggal"`
		Noreg         string `json:"noreg"`
		Plan          string `json:"plan"`
		Subjektif     string `json:"subjektif"`
		Situation     string `json:"situation"`
		Background    string `json:"background"`
		Recomendation string `json:"recomendation"`
		Objektif      string `json:"objektif"`
		Asesmen       string `json:"asesmen"`
		Kelompok      string `json:"kelompok"`
		InstruksiPpa  string `json:"instruksi_ppa"`
		Namadokter    string `json:"nama_dokter"`
		Namaperawat   string `json:"nama_perawat"`
		Namabagian    string `json:"nama_bagian"`
		Id            string `json:"no_rm"`
	}

	DImplementasiTindakanKeperawatan struct {
		Id         int              `gorm:"primaryKey;column:id" json:"id"`
		InsertDttm string           `json:"insert_dttm"`
		NoDaskep   string           `json:"no_daskep"`
		UserId     string           `json:"user_id"`
		KdBagian   string           `json:"kd_bagian"`
		Deskripsi  string           `json:"deskripsi"`
		Perawat    UserPerawatModel `gorm:"foreignKey:UserId" json:"perawat"`
		Pelayanan  KPelayanan       `gorm:"foreignKey:KdBagian" json:"bagian"`
	}

	KPelayanan struct {
		KdBag     string `gorm:"primaryKey;column:kd_bag" json:"kd_bag"`
		Bagian    string `json:"bagian"`
		Pelayanan string `json:"pelayanan"`
	}

	UserPerawatModel struct {
		Idperawat     string `gorm:"primaryKey;column:idperawat" json:"id_perawat"`
		Namaperawat   string `json:"nama"`
		Alamat        string `json:"alamat"`
		Jeniskelamin  string `json:"jenis_kelamin"`
		Statusperawat string `json:"status"`
	}
)

func (KPelayanan) TableName() string {
	return "vicore_lib.kpelayanan"
}

func (DImplementasiTindakanKeperawatan) TableName() string {
	return "vicore_rme.dimplementasi_keperawatan"
}

// USER PERAWAT

func (UserPerawatModel) TableName() string {
	return "his.kperawat"
}

func (DasKepDiagnosaModel) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}

func (DRegisterPasien) TableName() string {
	return "rekam.dregister"
}

// DASKEP SLKI9
func (DaskepSLKIModel) TableName() string {
	return "vicore_rme.daskep_slki"
}

func (DaskepSikiModel) TableName() string {
	return "vicore_rme.daskep_siki"
}

func (DProfilePasien) TableName() string {
	return "his.dprofilpasien"
}

func (ViewPerkembanganPasien) TableName() string {
	return "vicore_rme.view_perkembangan_pasien"
}

func (ViewIntervensi) TableName() string {
	return "vicore_rme.view_intervensi"
}

func (DPemFisik) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (SoapDokter) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}
