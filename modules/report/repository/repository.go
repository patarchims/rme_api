package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/report/entity"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type reportRepository struct {
	DB      *gorm.DB
	Logging *logrus.Logger
}

func NewReortRespository(db *gorm.DB, Logging *logrus.Logger) entity.ReportRepository {
	return &reportRepository{
		DB:      db,
		Logging: Logging,
	}
}

func (sr *reportRepository) OnGetRiwayatAlergiReporitory(noRM string) (res []rme.DAlergi, err error) {
	errs := sr.DB.Limit(5).Where(&rme.DAlergi{Id: noRM, KdBagian: "IGD001"}).Find(&res).Order("no DESC").Error

	if errs != nil {
		sr.Logging.Info(errs.Error())
		return res, errs
	}

	return res, nil
}

// REPORT
func (rr *reportRepository) GetDataSoapDokterRepository(Noreg string, KdBagian string) (res report.SoapDokter, err error) {
	results := rr.DB.Where(&report.SoapDokter{Noreg: Noreg, KdBagian: KdBagian}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

// REPORT REPOSITORY
func (rr *reportRepository) GetDataPasienRepository(NoRm string) (res report.DProfilePasien, err error) {
	results := rr.DB.Where(&report.DProfilePasien{Id: NoRm}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}
func (rr *reportRepository) GetProfilePasienRepository(NoRm string) (res report.DProfilePasien, err error) {
	results := rr.DB.Where(&report.DProfilePasien{Id: NoRm}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (rr *reportRepository) GetDataDpemFisikRepository(noReg string) (res report.DPemFisik, err error) {
	results := rr.DB.Where(&report.DPemFisik{Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (rr *reportRepository) GetTriaseReporitory(noReg string, userID string, kdBagian string, kategori string) (res report.TriaseReport, err error) {
	query := ` SELECT * FROM vicore_rme.view_triase  WHERE kd_bagian=? AND kategori =? AND pelayanan='rajal' AND noreg=? ORDER BY insert_dttm desc`

	result := rr.DB.Raw(query, kdBagian, kategori, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *reportRepository) GetDataIntervensiIntervensiResikoJatuhReporitory(kdBagian string, noReg string) (res []rme.DIntervensiRisiko, err error) {

	times := time.Now()

	query := `SELECT * FROM vicore_rme.dintervensi_risiko AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=? AND kd_bagian=?  AND noreg=?`

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *reportRepository) GetIntervensiResikojatuhReporitory(noReg string, kdBagian string) (res []rme.DIntervensiResikoJatuh, err error) {

	query := `CALL vicore_rme.GetIntervensiResiko(?, ?)`

	result := sr.DB.Raw(query, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetRingkasanPulangRepository(noReg string, kdBagian string) (res report.RingkasanPulang, err error) {
	query := `SELECT  dcr.tgl_masuk, dcr.kd_bagian, dcr.pelayanan,dcr.tgl_keluar, dcr.asesmed_dokter_ttd, ked.namadokter, dcr.asesmed_anamnesa, dcr.asesmed_diagP AS primer, kdiagP.description AS primer_desc, dcr.asesmed_pem_fisik, SUBSTRING(dcr.insert_dttm, 1,10) AS tgl_selesai,  SUBSTRING(dcr.insert_dttm, 12,8) AS jam_selesai, kp.bagian, dcr.asesmed_diagS1, kdiagS1.description AS secundar1, dcr.asesmed_diagS2, kdiagS2.description AS diags2, dcr.asesmed_diagS3, kdiagS3.description AS diag_s3, dcr.asesmed_diags4, kdiagS4.description AS diag_s4, dcr.asesmed_pros1, kpros1.Description AS prosedur1 FROM vicore_rme.dcppt_soap_dokter AS dcr LEFT JOIN rekam.dregister AS dr ON dcr.noreg=dr.noreg LEFT JOIN vicore_lib.k_icd10 AS kdiagP ON dcr.asesmed_diagP=kdiagP.code2 LEFT JOIN vicore_lib.k_icd10 AS kdiagS1 ON dcr.asesmed_diagS1=kdiagS1.code2 LEFT JOIN vicore_lib.k_icd10 AS kdiagS2 ON dcr.asesmed_diagS2=kdiagS2.code2 LEFT JOIN vicore_lib.k_icd10 AS kdiagS3 ON dcr.asesmed_diagS3=kdiagS3.code2 LEFT JOIN vicore_lib.k_icd10 AS kdiagS4 ON dcr.asesmed_diagS4=kdiagS4.code2 LEFT JOIN vicore_lib.k_icd9 AS kpros1 ON dcr.asesmed_pros1=kpros1.Code2 LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN his.ktaripdokter AS ked ON dcr.asesmed_dokter_ttd=ked.iddokter LEFT JOIN vicore_lib.kpelayanan AS kp ON dcr.kd_bagian=kp.kd_bag WHERE dcr.noreg=? AND dcr.kd_bagian=?`

	result := rr.DB.Raw(query, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (rr *reportRepository) GetSoapPerawatRepository(noReg string, kdBagian string) (res soap.DcpptSoapPasienModel, err error) {
	results := rr.DB.Where(&soap.DcpptSoapPasienModel{Noreg: noReg, KdBagian: kdBagian}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

// GET RESIKO JATUH ANAK
func (rr *reportRepository) GetDataResikoJatuhAnakRepository(noReg string, kdBagian string) (res report.ResikoJatuhAnak, err error) {
	query := `CALL vicore_rme.GetDataResikoJatuhAnak(?, ?)`

	result := rr.DB.Raw(query, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetResikojatuhMorseRepository(noReg string, kdBagian string) (res report.ResikoJatuhMorse, err error) {
	query := ` SELECT * FROM vicore_rme.view_resiko_morse  WHERE DATE(insert_dttm) = CURDATE() AND kd_bagian=? AND kategori ='Morse' AND noreg=?;`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetResikoJatuhDewasaRepository(noReg string, kdBagian string) (res report.ResikoJatuhDewasa, err error) {
	query := `SELECT * FROM vicore_rme.view_risiko_jatuh_dewasa  WHERE DATE(insert_dttm) = CURDATE() AND kd_bagian=? AND noreg=?;`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetReasesmenResikoJatuhAnakRepository(noReg string, kdBagian string) (res report.ResikoJatuhDewasa, err error) {
	query := `select dj.insert_dttm AS insert_dttm,dj.insert_user_id AS insert_user_id,dj.pelayanan AS pelayanan, dj.kd_bagian AS kd_bagian, dj.noreg AS noreg,dj.kategori AS kategori,dj.usia AS usia,dj.r_jatuh AS r_jatuh,dj.aktivitas AS aktivitas,dj.mobilisasi AS mobilisasi,dj.kognitif AS kognitif,dj.defisit_sensori AS defisit_sensori,dj.pengobatan AS pengobatan,dj.komorbiditas AS komorbiditas,dj.total AS total,kp.namaperawat AS namaperawat from (vicore_rme.drisiko_jatuh dj left join his.kperawat kp on((kp.idperawat = dj.insert_user_id))) 
	where (dj.kategori = 'ReAsesmen-Anak' AND dj.kd_bagian =? AND dj.noreg =?);`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetSoapDokterRepository(noReg string, kdBagian string) (res report.SoapDokerResponse, err error) {
	query := ` SELECT * FROM vicore_rme.view_soap_dokter  WHERE kd_bagian=?  AND noreg=?;`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetPemeriksaanFisikIGDDokter(noReg string, kdBagian string) (res dto.PemeriksaanFisik, err error) {
	query := ` select df.insert_dttm AS insert_dttm,df.insert_device AS insert_device,df.kategori AS kategori,df.pelayanan AS pelayanan,df.insert_user_id AS insert_user_id,df.kd_bagian AS kd_bagian,df.noreg AS noreg,df.mata AS mata,df.tht AS tht,df.mulut AS mulut,df.gigi AS gigi,df.leher AS leher,df.thyroid AS thyroid,df.leher_lainnya AS leher_lainnya,df.dada AS dada,df.jantung AS jantung,df.paru AS paru,df.perut AS perut,df.hati AS hati,df.lien AS lien,df.peristatik_usus AS peristatik_usus,df.abdomen_lainnya AS abdomen_lainnya,df.kulit AS kulit,df.ginjal AS ginjal,df.genetalia AS genetalia,df.ekstremitas_superior AS ekstremitas_superior,df.ekstremitas_inferior AS ekstremitas_inferior,pr.namaperawat AS namaperawat,kp.namadokter AS namadokter from ((vicore_rme.dpem_fisik df left join his.ktaripdokter kp on((kp.iddokter = df.insert_user_id))) left join his.kperawat pr on((pr.idperawat = df.insert_user_id)))  WHERE kd_bagian=?  AND noreg=? AND kategori="Dokter" LIMIT 1`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetAsesmenKeperawatanBidanIGDReporitory(noReg string, kdBagian string) (res report.KeperawatanBidanIGD, err error) {
	query := `SELECT * FROM vicore_rme.view_keperawatan_bidan  WHERE kd_bagian=?  AND noreg=? AND keterangan_person="Perawat" LIMIT 1`

	result := rr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetReportPerkembanganPasienRepository(noReg string) (res []report.ViewPerkembanganPasien, err error) {
	results := rr.DB.Where(&report.ViewPerkembanganPasien{Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil

}

func (rr *reportRepository) GetViewIntervensiRepository(noRM string) (res []report.ViewIntervensi, err error) {
	ersr := rr.DB.Where("id=?", noRM).Preload("DaskepSLKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("view_intervensi.insert_dttm asc")
	}).Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}

func (rr *reportRepository) ViewIntervensiRepository(noRM string) (res []report.ViewIntervensi, err error) {
	ersr := rr.DB.Where("id=?", noRM).Find(&res).Error

	if ersr != nil {
		return res, ersr
	}

	return res, nil
}

func (sr *reportRepository) GetIntervensiRepository(noRM string) (res []report.DasKepDiagnosaModel, err error) {
	ersr := sr.DB.Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Preload("DRegister", func(db *gorm.DB) *gorm.DB {
		return db.Where("id=?", noRM)
	}).Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil

}

func (sr *reportRepository) GetTindakanDiagnosaRepository(noDaskep string) (res []report.DImplementasiTindakanKeperawatan, err error) {
	results := sr.DB.Where(&report.DImplementasiTindakanKeperawatan{NoDaskep: noDaskep}).Order("dimplementasi_keperawatan.id asc").Preload("Perawat").Preload("Pelayanan").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (rr *reportRepository) GetDaskepSikiRepository(noDaskep string) (res []report.DaskepSikiModel, err error) {
	results := rr.DB.Where(&report.DaskepSikiModel{NoDaskep: noDaskep}).Order("kategori DESC").Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (rr *reportRepository) GetReportCPPTPasien(noRM string) (res []report.ReportCPPTPasien, err error) {
	query := `SELECT dsp.insert_dttm, dsp.kd_bagian, dsp.kelompok, dsp.tanggal, dsp.noreg, dsp.situation, dsp.background,  dsp.recomendation, dsp.subjektif, dsp.objektif, dsp.asesmen, dsp.plan, dsp.instruksi_ppa, ke.namadokter AS namadokter, ke2.nama AS namaperawat, kp.bagian AS namabagian, dp.id FROM vicore_rme.dcppt AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN his.ktaripdokter AS ke ON dsp.insert_user_id=ke.iddokter 
	LEFT JOIN mutiara.pengajar AS ke2 ON dsp.insert_user_id=ke2.id LEFT JOIN vicore_lib.kpelayanan AS kp ON dsp.kd_bagian=kp.kd_bag WHERE dp.id=? ORDER BY insert_dttm DESC`

	result := rr.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetResponseAlergi(noRM string) (res []dto.ResponseAlergi, err error) {
	query := `SELECT id, kelompok, alergi, kd_bagian FROM vicore_rme.dalergi  WHERE id=? ORDER BY NO DESC  LIMIT 3`

	result := rr.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) GetKeluhanUtamaFromDokterRepository(noReg string) (res dto.KeluhanUtamaFromDokter, err error) {
	query := `SELECT dsp.noreg, dsp.tgl_masuk, dsp.asesmed_keluh_utama AS keluhan, dsp.asesmed_rwyt_skrg AS rwt_skrg FROM vicore_rme.dcppt_soap_dokter AS dsp WHERE noreg=?`

	result := rr.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) OnGetAllDImplementasiKeperawatan() (res []rme.DImplementasiKeperawatan, err error) {

	result := rr.DB.Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (rr *reportRepository) OnUpdateNoregDImplementasiKeperawatan(noReg string, noDaskep string, id int) (res rme.DImplementasiKeperawatan, err error) {
	errs := rr.DB.Where(rme.DImplementasiKeperawatan{
		NoDaskep: noDaskep,
		Id:       id,
	}).Updates(rme.DImplementasiKeperawatan{
		NoReg: noReg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (rr *reportRepository) FindDaskepDiagnosaRepository(noDaskep string) (res rme.DasKepDiagnosaTable, err error) {
	errs := rr.DB.Where(rme.DImplementasiKeperawatan{
		NoDaskep: noDaskep,
	}).First(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}
func (rr *reportRepository) FindDregisterPasienRepositpru(noReg string) (res report.DRegisterPasien, err error) {
	errs := rr.DB.Where(report.DRegisterPasien{
		Noreg: noReg,
	}).First(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

// Perawat
func (rr *reportRepository) OnGetDImplementasiKeperawatanRepositoryByNoreg(noReg string) (res []rme.DImplementasiKeperawatan, err error) {
	errs := rr.DB.Where(rme.DImplementasiKeperawatan{
		NoReg: noReg,
	}).Preload("Perawat").Preload("Pelayanan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil

}
