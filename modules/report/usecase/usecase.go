package usecase

import (
	libEntity "hms_api/modules/lib/entity"
	"hms_api/modules/report"
	"hms_api/modules/report/dto"
	"hms_api/modules/report/entity"
	rmeDTO "hms_api/modules/rme/dto"
	rmeRepo "hms_api/modules/rme/entity"
	soapRepo "hms_api/modules/soap/entity"
	repoUser "hms_api/modules/user/entity"

	"github.com/sirupsen/logrus"
)

type reportUsecase struct {
	logging          *logrus.Logger
	reportRepository entity.ReportRepository
	reportMapper     entity.ReportMapper
	rmeRepository    rmeRepo.RMERepository
	remMapper        rmeRepo.RMEMapper
	soapRepository   soapRepo.SoapRepository
	soapMapper       soapRepo.SoapMapper
	userRepository   repoUser.UserRepository
	userMapper       repoUser.IUserMapper
	libRepository    libEntity.LibRepository
}

func NewReportUsecase(ur entity.ReportRepository, logging *logrus.Logger, mapper entity.ReportMapper, rmeRepo rmeRepo.RMERepository, rmeMapper rmeRepo.RMEMapper, soapRepo soapRepo.SoapRepository, soapMapper soapRepo.SoapMapper, userRepo repoUser.UserRepository, userMapper repoUser.IUserMapper, libRepository libEntity.LibRepository) entity.ReportUsecase {
	return &reportUsecase{
		reportRepository: ur,
		reportMapper:     mapper,
		logging:          logging,
		rmeRepository:    rmeRepo,
		remMapper:        rmeMapper,
		soapRepository:   soapRepo,
		soapMapper:       soapMapper,
		userRepository:   userRepo,
		userMapper:       userMapper,
		libRepository:    libRepository,
	}
}

func (su *reportUsecase) OnReportPengkajianAwalAnakUseCase(noReg string, kdBagian string) (res dto.ResponsePengkajianAwalKeperwatanAnak, err error) {
	perawat, _ := su.soapRepository.OnGetPengkajianAnakRepository(noReg, kdBagian)
	mapper := su.reportMapper.ToMappingPengkajianAnakKeperawatanRawatInap(perawat)

	return mapper, nil
}

func (su *reportUsecase) GetIntervensiPasienUsecase(noRM string) (res []report.ResponseIntervensi, err error) {

	intervensi, errs := su.reportRepository.ViewIntervensiRepository(noRM)

	var dat []report.ResponseIntervensi

	if errs != nil {
		return res, errs
	}

	for a := 0; a <= len(intervensi)-1; a++ {
		siki, _ := su.reportRepository.GetDaskepSikiRepository(intervensi[a].NoDaskep)
		tindakan, _ := su.reportRepository.GetTindakanDiagnosaRepository(intervensi[a].NoDaskep)

		sat := report.ResponseIntervensi{
			InsertDttm:  intervensi[a].InsertDttm,
			InsertPc:    intervensi[a].InsertPc,
			Noreg:       intervensi[a].Noreg,
			KetPerson:   intervensi[a].KetPerson,
			Namaperawat: intervensi[a].Namaperawat,
			NoDaskep:    intervensi[a].NoDaskep,
			KodeSdki:    intervensi[a].KodeSdki,
			Siki:        siki,
			Id:          intervensi[a].Id,
			Tindakan:    tindakan,
		}

		dat = append(dat, sat)

	}

	return dat, nil
}
func (su *reportUsecase) GetIntervensiPasienUsecaseV2(noRM string, kdBagian string) (res report.ResponseIntervensiV2, err error) {

	intervensi, errs := su.reportRepository.ViewIntervensiRepository(noRM)

	var dat []report.ResponseIntervensi

	if errs != nil {
		return res, errs
	}

	for a := 0; a <= len(intervensi)-1; a++ {
		siki, _ := su.reportRepository.GetDaskepSikiRepository(intervensi[a].NoDaskep)
		tindakan, _ := su.reportRepository.GetTindakanDiagnosaRepository(intervensi[a].NoDaskep)

		sat := report.ResponseIntervensi{
			InsertDttm:  intervensi[a].InsertDttm,
			InsertPc:    intervensi[a].InsertPc,
			Noreg:       intervensi[a].Noreg,
			KetPerson:   intervensi[a].KetPerson,
			Namaperawat: intervensi[a].Namaperawat,
			NoDaskep:    intervensi[a].NoDaskep,
			KodeSdki:    intervensi[a].KodeSdki,
			Siki:        siki,
			Id:          intervensi[a].Id,
			Tindakan:    tindakan,
		}

		dat = append(dat, sat)

	}

	pasien, _ := su.userRepository.OnGetProfilePasienRepository(noRM)
	mapperPasien := su.userMapper.ToMappingProfilePasien(pasien)
	pelayanan, _ := su.libRepository.OnGetKPelayananRepositoryByKdPelayanan(kdBagian)

	response := report.ResponseIntervensiV2{
		Pasien:     mapperPasien,
		Intervensi: dat,
		Pelayanan:  pelayanan,
	}

	return response, nil
}

func (su *reportUsecase) OnGetReportAsesmenAwalKeperawatanIGDANtonioUseCase(noRM string) (res dto.ResponseAsesmenAwalKeperawatanIGD, err error) {
	// GET ALERGI
	alergi, _ := su.reportRepository.OnGetRiwayatAlergiReporitory(noRM)
	mapper := su.reportMapper.ToMapperReportAsesmenAwalKeperawatanIGD(alergi)

	return mapper, nil
}

func (su *reportUsecase) OnGetReportPengkajianAwalKeperawatanIGDAntonioUseCase(noReg string) (res rmeDTO.ResponsePengkajianIGDKeperawatanAntonio, err error) {
	data, _ := su.rmeRepository.OnGetReporAsesmenAwalKeperawatanIGDAntonioRepository(noReg)
	nyeri, _ := su.soapRepository.GetDcpptSoapPasien("IGD001", noReg)

	pemFisik, _ := su.rmeRepository.GetPemeriksaanFisikIGDPerawatRepository("Dokter", "IGD001", noReg)
	pemFisikMapper := su.remMapper.ToMapperPemeriksaanFisikReponse(pemFisik)

	nyeriMapper := su.soapMapper.ToResSkriningNyeri(nyeri)
	asuhan, _ := su.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(noReg, "IGD001")
	pengkajianIGD, _ := su.soapRepository.OnGetAsesmenIGDRepository("IGD001", noReg)

	// USER REPOSITORY
	karyawatn, _ := su.userRepository.GetKaryawanRepository(pengkajianIGD.InsertUserId)
	mapper := su.remMapper.ToMappingPengkajianAwalKeperawatanIGDAntonio(data, nyeriMapper, asuhan, pemFisikMapper, pengkajianIGD, karyawatn)

	return mapper, nil
}

func (su *reportUsecase) OnGetReportImplementasiKeperawatanAntonioUseCase() (message string) {
	implementasi, _ := su.reportRepository.OnGetAllDImplementasiKeperawatan()

	if len(implementasi) > 0 {
		su.logging.Info("LAKUKAN DATA BERIKUT INI")
		for a := 0; a <= len(implementasi)-1; a++ {
			daskep, _ := su.reportRepository.FindDaskepDiagnosaRepository(implementasi[a].NoDaskep)
			update, _ := su.reportRepository.OnUpdateNoregDImplementasiKeperawatan(daskep.Noreg, implementasi[a].NoDaskep, implementasi[a].Id)
			su.logging.Info(update)
		}
		return "OK"
	} else {
		su.logging.Info("GAGA MELAKUKAN DATA")
		return "GAGAL"
	}

}
