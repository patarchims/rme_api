package dto

import (
	"hms_api/modules/rme"
	"hms_api/modules/soap"

	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

type (

	// REPOSENSE PENGKJIAN IGD KEPERAWATAN ANTONIO
	ResponsePengkajianIGDKeperawatanAntonio struct {
		PengkajiaPersistem ResponsePengkajianPersistemIGD `json:"pengkajian_persistem"`
		Nyeri              soapDTO.ResSkriningNyeriIGD    `json:"nyeri"`
		Asuhan             []rme.DasKepDiagnosaModelV2    `json:"asuhan"`
		Pemfisik           ResponsePemeriksaanFisik       `json:"pemfisik"`
		AsesmenIGD         soap.AsesemenIGD               `json:"asesmen"`
		User               user.Karyawan                  `json:"user"`
	}

	ResponsePengkajianPersistemIGD struct {
		TglMasuk                       string `json:"tgl_masuk"`
		TglKeluar                      string `json:"tgl_keluar"`
		JamCheckOut                    string `json:"jam_check_out"`
		JamCheckInRanap                string `json:"jam_check_in"`
		KdBagian                       string `json:"kd_bagian"`
		KdDpjp                         string `json:"kd_dpjp"`
		AseskepSistemMerokok           string `json:"sistem_merokok"`
		AseskepSistemMinumAlkohol      string `json:"sistem_minum_alkohol"`
		AseskepSistemSpikologis        string `json:"sistem_spikologis"`
		AseskepSistemGangguanJiwa      string `json:"sistem_gangguan_jiwa"`
		AseskepSistemBunuhDiri         string `json:"sistem_bunuh_diri"`
		AseskepSistemTraumaPsikis      string `json:"sistem_trauma_psikis"`
		AseskepSistemHambatanSosial    string `json:"sistem_hambatan_sosial"`
		AseskepSistemHambatanSpiritual string `json:"sistem_hambatan_spiritual"`
		AseskepSistemHambatanEkonomi   string `json:"sistem_hambatan_ekonomi"`
		AseskepSistemPenghasilan       string `json:"sistem_penghasilan"`
		AseskepSistemKultural          string `json:"sistem_kultural"`
		AseskepSistemAlatBantu         string `json:"sistem_alat_bantu"`
		AseskepSistemKebutuhanKhusus   string `json:"sistem_kebutuhan_khusus"`
	}
)
