package dto

import (
	"hms_api/modules/igd"
	"hms_api/modules/report"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
	"time"
)

type (
	ResponseProfilePasien struct {
	}

	// PERUBAHAN PADA ASUHAN KEPERAWATAN
	RegSaveAsuhanKeperawatanV2 struct {
		Noreg      string       `json:"noreg" validate:"required"`
		Intervensi []Intervensi `json:"intervensi"  validate:"required"`
		Sdki       string       `json:"sdki" validate:"required"`
		Siki       string       `json:"siki" validate:"required"`
	}

	RegIntervensiV2 struct {
		Slki string `json:"slki" validate:"required"`
	}

	ReqIntervensiResikoJatuh struct {
		RisikoJatuh []KFartorResiko `json:"intervensi" validate:"required"`
		Shift       string          `json:"shift" validate:"required"`
		Noreg       string          `json:"noreg" validate:"required"`
		Person      string          `json:"person" validate:"required"`
		Kategori    string          `json:"kategori" validate:"required"`
		DeviceId    string          `json:"device_id" validate:"required"`
		Pelayanan   string          `json:"pelayanan" validate:"required"`
	}

	ReqResikoJatuhPasien struct {
		RisikoJatuh []KFartorResiko `json:"intervensi" validate:"required"`
		Noreg       string          `json:"noreg" validate:"required"`
		Person      string          `json:"person" validate:"required"`
		Kategori    string          `json:"kategori" validate:"required"`
		DeviceID    string          `json:"device_id" validate:"required"`
		Pelayanan   string          `json:"pelayanan" validate:"required"`
		Skor        int             `json:"skor" validate:"omitempty"`
		Jenis       string          `json:"jenis" validate:"required"`
	}

	KFartorResiko struct {
		KodeFaktor         string               `json:"kode_faktor"`
		NamaFaktor         string               `json:"nama_faktor"`
		KategoriUmur       string               `json:"kategori_umur"`
		Jenis              string               `json:"jenis"`
		NoUrut             int                  `json:"no_urut"`
		KResikoJatuhPasien []KResikoJatuhPasien `json:"resiko_jatuh"`
	}

	KResikoJatuhPasien struct {
		IsEnable       bool   `json:"is_enable"`
		KodeResiko     string `json:"kode_resiko"`
		KodeFaktor     string `json:"kode_faktor"`
		KategoriFaktor string `json:"kategori_faktor"`
		NoUrut         int    `json:"no_urut"`
		Skor           int    `json:"skor"`
	}

	RegNoreg struct {
		NoReg string `json:"no_reg"`
	}

	ReqKodeSDKI struct {
		KodeSDKI string `json:"kode_sdki"`
	}

	ReqKodeDiagnosa struct {
		KodeDiagnosa string `json:"kode_diagnosa" validate:"required"`
	}

	RegSikiV2 struct {
		Siki string `json:"siki" validate:"required"`
	}

	ReqAsesmenKeperawatanV2 struct {
		Asesemen   Asesemen     `json:"asesmen" validate:"required"`
		Intervensi []Intervensi `json:"intervensi"  validate:"omitempty"`
		Sdki       string       `json:"sdki" validate:"omitempty"`
		Siki       string       `json:"siki" validate:"omitempty"`
	}

	ReqNoregV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	OnGetAsesmenIGD struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ResponseAsesmenIGD struct {
		Noreg                          string `json:"no_reg"`
		AseskepSistemMerokok           string `json:"sistem_merokok"`
		AseskepSistemMinumAlkohol      string `json:"sistem_minum_alkohol"`
		AseskepSistemSpikologis        string `json:"sistem_spikologis"`
		AseskepSistemGangguanJiwa      string `json:"sistem_gangguan_jiwa"`
		AseskepSistemBunuhDiri         string `json:"sistem_bunuh_diri"`
		AseskepSistemTraumaPsikis      string `json:"sistem_trauma_psikis"`
		AseskepSistemHambatanSosial    string `json:"sistem_hambatan_sosial"`
		AseskepSistemHambatanSpiritual string `json:"sistem_hambatan_spiritual"`
		AseskepSistemHambatanEkonomi   string `json:"sistem_hambatan_ekonomi"`
		AseskepSistemPenghasilan       string `json:"sistem_penghasilan"`
		AseskepSistemKultural          string `json:"sistem_kultural"`
		AseskepSistemAlatBantu         string `json:"sistem_alat_bantu"`
		AseskepSistemKebutuhanKhusus   string `json:"sistem_kebutuhan_khusus"`
	}

	ReqSaveAsesmenIGD struct {
		Noreg                          string `json:"no_reg"`
		AseskepSistemMerokok           string `json:"sistem_merokok"`
		AseskepSistemMinumAlkohol      string `json:"sistem_minum_alkohol"`
		AseskepSistemSpikologis        string `json:"sistem_spikologis"`
		AseskepSistemGangguanJiwa      string `json:"sistem_gangguan_jiwa"`
		AseskepSistemBunuhDiri         string `json:"sistem_bunuh_diri"`
		AseskepSistemTraumaPsikis      string `json:"sistem_trauma_psikis"`
		AseskepSistemHambatanSosial    string `json:"sistem_hambatan_sosial"`
		AseskepSistemHambatanSpiritual string `json:"sistem_hambatan_spiritual"`
		AseskepSistemHambatanEkonomi   string `json:"sistem_hambatan_ekonomi"`
		AseskepSistemPenghasilan       string `json:"sistem_penghasilan"`
		AseskepSistemKultural          string `json:"sistem_kultural"`
		AseskepSistemAlatBantu         string `json:"sistem_alat_bantu"`
		Person                         string `json:"person"`
		AseskepSistemKebutuhanKhusus   string `json:"sistem_kebutuhan_khusus"`
		DeviceID                       string `json:"device_id"`
	}

	RegIntervensi struct {
		Slki string `json:"slki" binding:"required"`
	}

	RegSiki struct {
		Siki string `json:"siki" binding:"required"`
	}

	ReqNoreg struct {
		Noreg string `json:"no_reg" binding:"required"`
	}

	ReqOnSavePemberianTerapiCairan struct {
		Noreg        string `json:"no_reg" binding:"required"`
		NamaCairan   string `json:"nama_cairan" binding:"omitempty"`
		DosisMakro   int    `json:"dosis_makro" binding:"omitempty"`
		DosisMikro   int    `json:"dosis_mikro" binding:"omitempty"`
		FlsKe        int    `json:"fls_ke" binding:"omitempty"`
		Obat         string `json:"obat" binding:"omitempty"`
		Keterangan   string `json:"keterangan" binding:"omitempty"`
		SisaInfuse   int    `json:"sisa_infuse" binding:"omitempty"`
		JumlahInfuse int    `json:"jumlah" binding:"omitempty"`
	}

	ReqOnSaveDCairanIntake struct {
		Noreg  string `json:"no_reg" binding:"required"`
		NGT    int    `json:"ngt" binding:"omitempty"`
		Minum  int    `json:"minum" binding:"omitempty"`
		Infuse int    `json:"infuse" binding:"omitempty"`
	}

	ReqOnDeleteInTake struct {
		ID int `json:"ID" binding:"required"`
	}

	ReqOnGetCairanInTake struct {
		NoReg string `json:"no_reg" binding:"required"`
	}

	PenkajianPersistemRANAP struct {
		DPemFIsik rme.DPemFisikModel                    `json:"dpemfisik"`
		Persistem igd.ResponseDPengkajianPersistenRANAP `json:"pesistem"`
	}

	ReqOnGetPengkajianPersistem struct {
		NoReg     string `json:"no_reg" binding:"required"`
		KdBagian  string `json:"kd_bagian" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
		Usia      string `json:"usia" binding:"required"`
		Kategori  string `json:"kategori" binding:"required"`
		Person    string `json:"person" binding:"required"`
	}

	ReqOnGetPengkajianAwalKeperawatanRANAP struct {
		NoReg     string `json:"no_reg" binding:"required"`
		KdBagian  string `json:"kd_bagian" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
		Usia      string `json:"usia" binding:"required"`
		Kategori  string `json:"kategori" binding:"required"`
		Person    string `json:"person" binding:"required"`
	}

	OnSavePengkajianPersistemBangsal struct {
		NoReg     string `json:"no_reg" binding:"required"`
		KdBagian  string `json:"kd_bagian" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
		Usia      string `json:"usia" binding:"required"`
		Kategori  string `json:"kategori" binding:"required"`
		Person    string `json:"person" binding:"required"`
		DevicesID string `json:"device_id" binding:"required"`
		// TAMBAHKAN PENGKAJIAN PERSISTEM
		Kepala       string `json:"kepala"`
		Mata         string `json:"mata"`
		Telinga      string `json:"telinga"`
		LeherDanBahu string `json:"leher_dan_bahu"`
		Hidung       string `json:"hidung"`
		Mulut        string `json:"mulut"`
		Gigi         string `json:"gigi"`
		Dada         string `json:"dada"`
		Abdomen      string `json:"abdomen"`
		// TAMBAHKAN DATA PERSISTEM
		NutrisiDanHidrasi      string `gorm:"column:nutrisi_dan_hidrasi;type:text" json:"nutrisi_hidrasi"`
		Eliminasi              string `gorm:"column:eliminasi;type:text" json:"eliminasi"`
		Aktivitas              string `gorm:"column:aktivitas;type:text" json:"aktivasi"`
		SistemKardioRepiratori string `gorm:"column:sistem_kardio_repiratori;type:text" json:"kardio_respiratori"`
		SistemPerfusiSecebral  string `gorm:"column:sistem_perfusi_secebral;type:text" json:"perfusi_secebral"`
		Thermoregulasi         string `gorm:"column:thermoregulasi;type:text" json:"thermoregulasi"`
		SistemPerfusiPerifer   string `gorm:"column:sistem_perfusi_perifer;type:text" json:"sistem_perfusi_perifer"`
		Integumen              string `gorm:"column:integumen;type:text" json:"integumen"`
		Proteksi               string `gorm:"column:proteksi;type:text" json:"proteksi"`
		SeksualReproduksi      string `gorm:"column:seksual_repoduksi;type:text" json:"seksual_reproduksi"`
		SistemPencernaan       string `gorm:"column:sistem_pencernaan;type:text" json:"sistem_pencernaan"`
	}

	ReqOnSaveCiaranOutPut struct {
		Noreg      string `json:"no_reg" binding:"required"`
		Urine      int    `json:"urine" binding:"required"`
		NGT        int    `json:"ngt" binding:"required"`
		WSD        int    `json:"wsd" binding:"required"`
		Drain      int    `json:"drain" binding:"required"`
		Muntah     int    `json:"muntah" binding:"required"`
		Pendarahan int    `json:"pendarahan" binding:"required"`
	}

	ReqOnGetReportPemberianTerapiCairan struct {
		Noreg string `json:"no_reg" binding:"required"`
		NoRM  string `json:"no_rm" binding:"omitempty"`
	}

	ResPengkajianKeperawatan struct {
		NoReg     string `json:"no_reg" binding:"required"`
		KdBagian  string `json:"kd_bagian" binding:"required"`
		Usia      string `json:"usia"`
		Pelayanan string `json:"pelayanan"`
		Person    string `json:"person"`
		Kategori  string `json:"kategori"`
	}

	ReqFisik struct {
		Noreg  string `json:"no_reg" binding:"required"`
		Person string `json:"person" binding:"required"`
	}

	ReqVitalSign struct {
		Noreg     string `json:"no_reg" binding:"required"`
		Person    string `json:"person" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
	}

	OnSaveVitalSignICU struct {
		Kategori     string `json:"kategori" validate:"required"`
		DeviceID     string `json:"device_id" binding:"required"`
		Noreg        string `json:"no_reg" binding:"required"`
		Person       string `json:"person" binding:"required"`
		Pelayanan    string `json:"pelayanan" binding:"required"`
		TekananDarah string `json:"tekanan_darah"`
		Nadi         string `json:"nadi"`
		BeratBadan   string `json:"berat_badan"`
		Suhu         string `json:"suhu"`
		Pernapasan   string `json:"pernapasan"`
		TinggiBadan  string `json:"tinggi_badan"`
		Pupil        string `json:"pupil"`
		Spo2         string `json:"spo2"`
	}

	GetNoreg struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	GetAsuhanKeperawatan struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Status string `json:"status" validate:"required"`
	}

	ReqOnUpdateDaskep struct {
		Noreg    string `json:"no_reg" validate:"required"`
		NoDaskep string `json:"no_daskep" validate:"required"`
	}

	ReqRiwayatAlergi struct {
		NoRM  string `json:"no_rm" validate:"required"`
		NoReg string `json:"no_reg" validate:"required"`
	}

	ReqCPPTSBbar struct {
		NoReg string `json:"no_reg" validate:"required"`
	}

	ReqSaveCPPTSBar struct {
		NoReg         string `json:"no_reg" validate:"required"`
		DeviceID      string `json:"device_id" validate:"required"`
		Kelompok      string `json:"kelompok" validate:"required"`
		Situation     string `json:"situation" validate:"required"`
		Asesmen       string `json:"asesmen" validate:"required"`
		Background    string `json:"background" validate:"required"`
		Recomendation string `json:"recomendation" validate:"required"`
		Ppa           string `json:"ppa" validate:"omitempty"`
		Pelayanan     string `json:"pelayanan" validate:"omitempty"`
		Dpjp          string `json:"dpjp" validate:"omitempty"`
	}

	ReqDasKepDiagnosaModel struct {
		Tanggal    string                `json:"tanggal"`
		Noreg      string                `json:"no_reg"`
		Hasil      string                `json:"hasil"`
		KodeSdki   string                `json:"kode_diagnosa"`
		NoDaskep   string                `json:"no_daskep"`
		SDKIModel  rme.SDKIModel         `json:"diagnosa"`
		DaskepSLKI []rme.DaskepSLKIModel `json:"deskripsi_slki"`
		DaskepSIKI []rme.DaskepSikiModel `json:"deskripsi_siki"`
	}

	AsesmenNyeiICU struct {
		AseskepNyeri          int    `json:"nyeri"`
		AseskepLokasiNyeri    string `json:"lokasi_nyeri"`
		AseskepFrekuensiNyeri string `json:"frekuensi_nyeri"`
		AseskepNyeriMenjalar  string `json:"nyeri_menjalar"`
		AseskepKualitasNyeri  string `json:"kualitas_nyeri"`
	}

	ReqUpdateeCPPTSBar struct {
		Situation     string `json:"situation" validate:"required"`
		Asesmen       string `json:"asesmen" validate:"required"`
		Background    string `json:"background" validate:"required"`
		Recomendation string `json:"recomendation" validate:"required"`
		IdCppt        int    `json:"id_cppt" validate:"required"`
	}

	ReqSaveRiwayatPenyakitKelluarga struct {
		NoRm     string `json:"no_rm" validate:"required"`
		Penyakit string `json:"penyakit" validate:"required"`
	}

	ReqOnSaveActionCpptFiberHandler struct {
		IdCppt    int    `json:"id_cppt" validate:"required"`
		Deskripsi string `json:"deskripsi" validate:"required"`
	}

	ReqImplementasiTindakan struct {
		NoAskep   string `json:"no_askep" validate:"required"`
		Deskripsi string `json:"deskripsi" validate:"required"`
	}

	ReqSavePengkajianPersistemIGD struct {
		NoReg                   string `json:"no_reg" validate:"required"`
		SistemMerokok           string `json:"sistem_merokok"`
		SistemMinumAlkohol      string `json:"minum_alkohol"`
		SistemSpikologis        string `json:"spikologis"`
		SistemGangguanJiwa      string `json:"gangguan_jiwa"`
		SistemBunuhDiri         string `json:"bunuh_diri"`
		SistemTraumaPsikis      string `json:"trauma_psikis"`
		SistemHambatanSosial    string `json:"hambatan_sosial"`
		SistemHambatanSpiritual string `json:"hambatan_spiritual"`
		SistemHambatanEkonomi   string `json:"hambatan_ekonomi"`
		SistemPenghasilan       string `json:"penghasilan"`
		SistemKultural          string `json:"kultural"`
		SistemAlatBantu         string `json:"alat_bantu"`
		SistemKebutuhanKhusus   string `json:"kebutuhan_khusus"`
		Person                  string `json:"person"`
	}

	ReqPengkajianPersistemIGD struct {
		NoReg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqTindakan struct {
		NoAskep string `json:"no_askep" validate:"required"`
	}

	ReqDeleteTindakan struct {
		IDTindakan int `json:"id_tindakan" validate:"required"`
	}

	ReqOnUpdateActionCpptFiberHandler struct {
		IdPelaksanaan int    `json:"id" validate:"required"`
		Deskripsi     string `json:"deskripsi" validate:"required"`
	}

	ReqOnUpdateKartuObservasi struct {
		IdObservasi  int    `json:"id_observasi"`
		T            string `json:"t"`
		N            string `json:"n"`
		P            string `json:"p"`
		S            string `json:"s"`
		Cvp          string `json:"cvp"`
		Ekg          string `json:"ekg"`
		PupilKi      string `json:"pupil_kiri"`
		PupilKa      string `json:"pupil_kanan"`
		RedaksiKi    string `json:"redaksi_kiri"`
		RedaksiKa    string `json:"redaksi_kanan"`
		AnggotaBadan string `json:"anggota_badan"`
		Kesadaran    string `json:"kesadaran"`
		SputumWarna  string `json:"sputum_warna"`
		IsiCup       string `json:"isi_cup"`
		Keterangan   string `json:"keterangan"`
	}

	ResponseCairanKartu struct {
		IdKartu           int    `json:"id_kartu"`
		Jam               string `json:"jam"`
		CairanMasuk1      string `json:"cairan_masuk1"`
		CairanMasuk2      string `json:"cairan_masuk2"`
		CairanMasuk3      string `json:"cairan_masuk3"`
		CairanMasukNgt    string `json:"cairan_masuk_ngt"`
		NamaCairan        string `json:"nama_cairan"`
		CairanKeluarUrine string `json:"cairan_keluar_urine"`
		CairanKeluarNgt   string `json:"cairan_keluar_ngt"`
		DrainDll          string `json:"drain_dll"`
		Keterangan        string `json:"keterangan"`
	}

	ReqOnSaveKartuObservasi struct {
		NoReg        string `json:"noreg" validate:"required"`
		T            string `json:"t"`
		N            string `json:"n"`
		P            string `json:"p"`
		S            string `json:"s"`
		CVP          string `json:"cvp"`
		EKG          string `json:"ekg"`
		UkuranKi     string `json:"ukuran_ki"`
		UkuranKa     string `json:"ukuran_ka"`
		RedaksiKi    string `json:"redaksi_ki"`
		RedaksiKa    string `json:"redaksi_ka"`
		AnggotaGerak string `json:"anggota_gerak"`
		Kesadaran    string `json:"kesadaran"`
		SputumWarna  string `json:"sputum_warna"`
		IsiCup       string `json:"isi_cup"`
		Keterangan   string `json:"keterangan"`
	}

	ResponseDKartuObservasi struct {
		IdObservasi  int    `json:"id_observasi"`
		Jam          string `json:"jam"`
		T            string `json:"t"`
		N            string `json:"n"`
		P            string `json:"p"`
		S            string `json:"s"`
		Cvp          string `json:"cvp"`
		Ekg          string `json:"ekg"`
		PupilKi      string `json:"pupil_kiri"`
		PupilKa      string `json:"pupil_kanan"`
		RedaksiKi    string `json:"redaksi_kiri"`
		RedaksiKa    string `json:"redaksi_kanan"`
		AnggotaBadan string `json:"anggota_badan"`
		Kesadaran    string `json:"kesadaran"`
		SputumWarna  string `json:"sputum_warna"`
		IsiCup       string `json:"isi_cup"`
		Keterangan   string `json:"keterangan"`
	}

	OnGetKartuObservasi struct {
		Noreg string `json:"noreg" validate:"required"`
	}

	OnDeleteKartuCairan struct {
		IdKartu int `json:"id_kartu" validate:"required"`
	}

	OnDeleteKartuObservasi struct {
		IDKartuObservasi int `json:"id_kartu" validate:"required"`
	}

	OnUpdateKartuCairan struct {
		IdKartu           int    `json:"id_kartu" validate:"required"`
		CairanMasuk1      string `json:"cairan_masuk1"`
		CairanMasuk2      string `json:"cairan_masuk2"`
		CairanMasuk3      string `json:"cairan_masuk3"`
		CairanMasukNgt    string `json:"cairan_masuk_ngt"`
		NamaCairan        string `json:"nama_cairan"`
		CairanKeluarUrine string `json:"cairan_keluar_urine"`
		CairanKeluarNgt   string `json:"cairan_keluar_ngt"`
		Drain             string `json:"drain_dll"`
		Keterangan        string `json:"keterangan"`
	}

	OnSaveKartuCairan struct {
		Noreg             string `json:"noreg" validate:"required"`
		CairanMasuk1      string `json:"cairan_masuk1"`
		CairanMasuk2      string `json:"cairan_masuk2"`
		CairanMasuk3      string `json:"cairan_masuk3"`
		CairanMasukNgt    string `json:"cairan_masuk_ngt"`
		NamaCairan        string `json:"nama_cairan"`
		CairanKeluarUrine string `json:"cairan_keluar_urine"`
		CairanKeluarNgt   string `json:"cairan_keluar_ngt"`
		Drain             string `json:"drain_dll"`
		Keterangan        string `json:"keterangan"`
	}

	ReqOnDeleteCPTTSBAR struct {
		IdCppt int `json:"id_cppt" validate:"required"`
	}

	ReqKeluhanUtama struct {
		NoRM    string `json:"no_rm" validate:"required"`
		NoReg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"required"`
		Person  string `json:"person" validate:"required"`
	}

	ReqPemeriksaanFisikIGDDokter struct {
		DeviceID            string `json:"device_id" validate:"required"`
		Pelayanan           string `json:"pelayanan" validate:"required"`
		Person              string `json:"person" validate:"required"`
		Noreg               string `json:"noreg" validate:"required"`
		Kepala              string `json:"kepala" validate:"omitempty"`
		Mata                string `json:"mata" validate:"omitempty"`
		Tht                 string `json:"tht" validate:"omitempty"`
		Mulut               string `json:"mulut" validate:"omitempty"`
		Leher               string `json:"leher" validate:"omitempty"`
		Dada                string `json:"dada" validate:"omitempty"`
		Jantung             string `json:"jantung" validate:"omitempty"`
		Paru                string `json:"paru" validate:"omitempty"`
		Perut               string `json:"perut" validate:"omitempty"`
		Hati                string `json:"hati" validate:"omitempty"`
		Limpa               string `json:"limpa" validate:"omitempty"`
		Ginjal              string `json:"ginjal" validate:"omitempty"`
		AlatKelamin         string `json:"alat_kelamin" validate:"omitempty"`
		AnggotaGerak        string `json:"anggota_gerak" validate:"omitempty"`
		Refleks             string `json:"refleks" validate:"omitempty"`
		KekuatanOtot        string `json:"kekuatan_otot" validate:"omitempty"`
		Kulit               string `json:"kulit" validate:"omitempty"`
		KelenjarGetahBening string `json:"getah_bening" validate:"omitempty"`
		RtVt                string `json:"rtvt" validate:"omitempty"`
		JalanNafas          string `json:"jalan_nafas" validate:"omitempty"`
		Sirkulasi           string `json:"sirkulasi" validate:"omitempty"`
		Gigi                string `json:"gigi" validate:"omitempty"`
		Abdomen             string `json:"abdomen" validate:"omitempty"`
		Hidung              string `json:"hidung" validate:"omitempty"`
		Telinga             string `json:"telinga" validate:"omitempty"`
	}

	ReqOnSavePemeriksaanFisikICU struct {
		DeviceID    string `json:"device_id" validate:"required"`
		Pelayanan   string `json:"pelayanan" validate:"required"`
		Person      string `json:"person" validate:"required"`
		Noreg       string `json:"noreg" validate:"required"`
		GcsE        string `json:"gcs_e"`
		GcsM        string `json:"gcs_m"`
		GcsV        string `json:"gcs_V"`
		Kesadaran   string `json:"kesadaran"`
		Kepala      string `json:"kepala"`
		Rambut      string `json:"rambut"`
		Wajah       string `json:"wajah"`
		Mata        string `json:"mata"`
		Telinga     string `json:"telinga"`
		Hidung      string `json:"hidung"`
		Mulut       string `json:"mulut"`
		Gigi        string `json:"gigi"`
		Lidah       string `json:"lidah"`
		Tenggorokan string `json:"tenggorokan"`
		Leher       string `json:"leher"`
		Dada        string `json:"dada"`
		Respirasi   string `json:"respirasi"`
		Jantung     string `json:"jantung"`
		Integumen   string `json:"integumen"`
		Ekstremitas string `json:"ekstremitas"`
		Pupil       string `json:"pupil"`
	}

	ReqOnSavePemeriksaanFisikAnak struct {
		DeviceID          string `json:"device_id" validate:"required"`
		Pelayanan         string `json:"pelayanan" validate:"required"`
		Person            string `json:"person" validate:"required"`
		Noreg             string `json:"noreg" validate:"required"`
		Mata              string `json:"mata"`
		Telinga           string `json:"telinga"`
		Hidung            string `json:"hidung"`
		Mulut             string `json:"mulut"`
		LeherDanBahu      string `json:"leher_dan_bahu"`
		Dada              string `json:"dada"`
		Abdomen           string `json:"abdomen"`
		Punggung          string `json:"punggung"`
		Peristaltik       string `json:"peristaltik"`
		NutrisiDanHidrasi string `json:"nutrisi_dan_hidrasi"`
	}

	ReqPemeriksaanFisikIGDDokterMethodist struct {
		DeviceID            string `json:"device_id" validate:"required"`
		Pelayanan           string `json:"pelayanan" validate:"required"`
		Person              string `json:"person" validate:"required"`
		Noreg               string `json:"noreg" validate:"required"`
		Kepala              string `json:"kepala" validate:"omitempty"`
		Mata                string `json:"mata" validate:"omitempty"`
		Tht                 string `json:"tht" validate:"omitempty"`
		Mulut               string `json:"mulut" validate:"omitempty"`
		Gigi                string `json:"gigi" validate:"omitempty"`
		Leher               string `json:"leher" validate:"omitempty"`
		KelenjarGetahBening string `json:"getah_bening" validate:"omitempty"`
		Dada                string `json:"dada" validate:"omitempty"`
		Jantung             string `json:"jantung" validate:"omitempty"`
		Paru                string `json:"paru" validate:"omitempty"`
		Perut               string `json:"perut" validate:"omitempty"`
		Hati                string `json:"hati" validate:"omitempty"`
		Limpa               string `json:"limpa" validate:"omitempty"`
		Usus                string `json:"usus" validate:"omitempty"`
		AbdomenLainnya      string `json:"abdomen_lainnya" validate:"omitempty"`
		Ginjal              string `json:"ginjal" validate:"omitempty"`
		AlatKelamin         string `json:"alat_kelamin" validate:"omitempty"`
		Anus                string `json:"anus" validate:"omitempty"`
		Superior            string `json:"superior" validate:"omitempty"`
		Inferior            string `json:"inferior" validate:"omitempty"`
	}

	RegPemeriksaanFisikDokterAntonio struct {
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
		Person           string `json:"person" validate:"required"`
		Noreg            string `json:"noreg" validate:"required"`
		PemeriksaanFisik string `json:"pem_fisik" validate:"required"`
	}

	ReqPemeriksaanFisik struct {
		DeviceID            string `json:"device_id" validate:"required"`
		Pelayanan           string `json:"pelayanan" validate:"required"`
		Person              string `json:"person" validate:"required"`
		Noreg               string `json:"noreg" validate:"required"`
		Kepala              string `json:"kepala" validate:"omitempty"`
		Mata                string `json:"mata" validate:"omitempty"`
		Tht                 string `json:"tht" validate:"omitempty"`
		Mulut               string `json:"mulut" validate:"omitempty"`
		Leher               string `json:"leher" validate:"omitempty"`
		Dada                string `json:"dada" validate:"omitempty"`
		Jantung             string `json:"jantung" validate:"omitempty"`
		Paru                string `json:"paru" validate:"omitempty"`
		Perut               string `json:"perut" validate:"omitempty"`
		Hati                string `json:"hati" validate:"omitempty"`
		Limpa               string `json:"limpa" validate:"omitempty"`
		Ginjal              string `json:"ginjal" validate:"omitempty"`
		AlatKelamin         string `json:"alat_kelamin" validate:"omitempty"`
		AnggotaGerak        string `json:"anggota_gerak" validate:"omitempty"`
		Refleks             string `json:"refleks" validate:"omitempty"`
		KekuatanOtot        string `json:"kekuatan_otot" validate:"omitempty"`
		Kulit               string `json:"kulit" validate:"omitempty"`
		KelenjarGetahBening string `json:"getah_bening" validate:"omitempty"`
		RtVt                string `json:"rtvt" validate:"omitempty"`
		JalanNafas          string `json:"jalan_nafas" validate:"omitempty"`
		Sirkulasi           string `json:"sirkulasi" validate:"omitempty"`
		Gigi                string `json:"gigi" validate:"omitempty"`
		Abdomen             string `json:"abdomen" validate:"omitempty"`
		Hidung              string `json:"hidung" validate:"omitempty"`
		Telinga             string `json:"telinga" validate:"omitempty"`
	}

	ReqPemeriksaanFisikPerina struct {
		DeviceID     string `json:"device_id" validate:"required"`
		Pelayanan    string `json:"pelayanan" validate:"required"`
		Person       string `json:"person" validate:"required"`
		Noreg        string `json:"noreg" validate:"required"`
		Kesadaran    string `json:"kesadaran" validate:"omitempty"`
		GcsE         string `json:"gcs_e" validate:"omitempty"`
		GcsV         string `json:"gcs_v" validate:"omitempty"`
		GcsM         string `json:"gcs_m" validate:"omitempty"`
		Kelainan     string `json:"kelainan" validate:"omitempty"`
		TonickNecK   string `json:"tonick_neck" validate:"omitempty"`
		Kepala       string `json:"kepala" validate:"omitempty"`
		Wajah        string `json:"wajah" validate:"omitempty"`
		Telinga      string `json:"telinga" validate:"omitempty"`
		Hidung       string `json:"hidung" validate:"omitempty"`
		Mulut        string `json:"mulut" validate:"omitempty"`
		Refleks      string `json:"refleks" validate:"omitempty"`
		LeherDanBahu string `json:"leher_dah_bahu" validate:"omitempty"`
		Dada         string `json:"dada" validate:"omitempty"`
		Abdomen      string `json:"abdomen" validate:"omitempty"`
		Punggung     string `json:"punggung" validate:"omitempty"`
		Integumen    string `json:"integumen" validate:"omitempty"`
		Ekstremitas  string `json:"ekstremitas" validate:"omitempty"`
		Genetalia    string `json:"genetalia" validate:"omitempty"`
		Anus         string `json:"anus" validate:"omitempty"`
		// TAMBAHKAN PEMERIKSAAN FISIK BAYI
		// "babinski": fisik.babinski,
		// "refleks_rooting": fisik.refleksRooting,
		// "refleks_swallowing": fisik.refleksSwallowing,
		// "refleks_sucking": fisik.refleksSucking,
		// "refleks_moro": fisik.refleksMoro,
		// "refleks_graps": fisik.refleksGraps,
		Babinski   string `json:"babinski" validate:"omitempty"`
		Rooting    string `json:"refleks_rooting" validate:"omitempty"`
		Swallowing string `json:"refleks_swallowing" validate:"omitempty"`
		Sucking    string `json:"refleks_sucking" validate:"omitempty"`
		Moro       string `json:"refleks_moro" validate:"omitempty"`
		Graps      string `json:"refleks_graps" validate:"omitempty"`
	}

	ReqDVitalSignPerina struct {
		DeviceID      string `json:"device_id" validate:"required"`
		Pelayanan     string `json:"pelayanan" validate:"required"`
		Kategori      string `json:"kategori" validate:"required"`
		Person        string `json:"person" validate:"required"`
		Noreg         string `json:"noreg" validate:"required"`
		Tb            string `json:"tb" validate:"omitempty"`
		HR            string `json:"hr" validate:"omitempty"`
		RR            string `json:"rr" validate:"omitempty"`
		Td            string `json:"td" validate:"omitempty"`
		Spo2          string `json:"spo2" validate:"omitempty"`
		Bb            string `json:"bb" validate:"omitempty"`
		LingkarKepala string `json:"lingkar_kepala" validate:"omitempty"`
		LingkarLengan string `json:"lingkar_lengan" validate:"omitempty"`
		LingkarDada   string `json:"lingkar_dada" validate:"omitempty"`
		LingkarPerut  string `json:"lingkar_perut" validate:"omitempty"`
		WarnaKulit    string `json:"warna_kulit" validate:"omitempty"`
		KeadaanUmum   string `json:"keadaan_umum" validate:"omitempty"`
	}

	OnReqVitalSign struct {
		Noreg     string `json:"no_reg" binding:"required"`
		Person    string `json:"person" binding:"required"`
		Pelayanan string `json:"pelayanan" binding:"required"`
	}

	ReqRiwayatKehamilanPerina struct {
		DeviceID             string `json:"device_id" validate:"required"`
		Pelayanan            string `json:"pelayanan" validate:"required"`
		Person               string `json:"person" validate:"required"`
		Noreg                string `json:"noreg" validate:"required"`
		NoRm                 string `json:"no_rm" validate:"required"`
		TahunKelahiran       string `json:"tahu_kelahiran" validate:"omitempty"`
		JenisKelamin         string `json:"jenis_kelamin" validate:"omitempty"`
		TempatPersalinan     string `json:"tempat_persalinan" validate:"omitempty"`
		BeratBadan           string `json:"berat_badan" validate:"omitempty"`
		Keadaan              string `json:"keadaan" validate:"omitempty"`
		KomplikasiKehamilan  string `json:"komplikasi_kehamilan" validate:"omitempty"`
		KomplikasiPersalinan string `json:"komplikasi_persalinan" validate:"omitempty"`
		JenisPersalinan      string `json:"jenis_persalinan" validate:"omitempty"`
	}

	ReqDeleteRiwayatKehamilanPerina struct {
		KdRiwayat string `json:"kd_riwayat" validate:"required"`
		Noreg     string `json:"no_reg" validate:"required"`
		NoRm      string `json:"no_rm" validate:"required"`
	}

	ReqGetPemeriksaanFisikIGDDokter struct {
		Noreg  string `json:"noreg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqPemeriksaanFisikDokter struct {
		Noreg  string `json:"noreg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	RegGetPemeriksaanFisikICU struct {
		Noreg  string `json:"noreg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqGetPemeriksaanFisikAnak struct {
		Noreg  string `json:"noreg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqAsesmenBayiPerina struct {
		Noreg  string `json:"no_reg" validate:"required"`
		NoRm   string `json:"no_rm" validate:"required"`
		Person string `json:"person" validate:"omitempty"`
	}

	ReqOnSaveAsesemenKeperawatanBayi struct {
		DeviceID                    string `json:"device_id" validate:"required"`
		Person                      string `json:"person" validate:"required"`
		NoReg                       string `json:"no_reg" validate:"required"`
		NoRm                        string `json:"no_rm" validate:"required"`
		Pelayanan                   string `json:"pelayanan" validate:"required"`
		Dpjp                        string `json:"dpjp" validate:"omitempty"`
		AseskepBayiDokterObgyn      string `json:"dokter_obgyn" validate:"omitempty"`
		AseskepBayiDokterAnak       string `json:"dokter_anak" validate:"omitempty"`
		PrenatalPersalinan          string `json:"aseskep_bayi_prenatal_persalinan" validate:"omitempty"`
		NamaAyah                    string `json:"nama_ayah" validate:"omitempty"`
		RiwayatPenyakitAyah         string `json:"riwayat_penyakit_ayah" validate:"omitempty"`
		AseskepBayiPekerjaanAyah    string `json:"pekerjaan_ayah" validate:"omitempty"`
		AseskepBayiPerkawinanAyahKe string `json:"perkawinan_ayah" validate:"omitempty"`
		// === END AYAH
		AseskepUsiaKehamilan       string `json:"usia_kehamilan" validate:"omitempty"`
		AseskepBayiNamaIbu         string `json:"nama_ibu" validate:"omitempty"`
		AseskepBayiPerkawinanIbuKe string `json:"perkawinan_ibu" validate:"omitempty"`
		AseskepBayiRwtPenyakitIbu  string `json:"penyakit_ibu" validate:"omitempty"`
		PrenatalPendarahan         string `json:"pendarahan_prenatal" validate:"omitempty"`
		PekerjaanIbu               string `json:"pekerjaan_ibu" validate:"omitempty"`
		// === PENANGGUNG JAWA IBU
		AseskepBayiNamaPjawab                string `json:"nama_pjawab" validate:"omitempty"`
		AseskepBayiUsiaPjawab                string `json:"usia_pjawab" validate:"omitempty"`
		AseskepBayiPekerjaanPjawab           string `json:"pekerjaan_pjawab" validate:"omitempty"`
		AseskepBayiUsiaPersalinan            string `json:"usia_persalinan" validate:"omitempty"`
		AseskepBayiTglLahir                  string `json:"tgl_lahir" validate:"omitempty"`
		AseskepBayiMenangis                  string `json:"menangis" validate:"omitempty"`
		AseskepBayiJk                        string `json:"jenis_kelamin" validate:"omitempty"`
		AseskepBayiKeterangan                string `json:"keterangan" validate:"omitempty"`
		AseskepBayiPrenatalUsiaKehamilan     string `json:"prenatal_usia_kehamilan" validate:"omitempty"`
		AseskepBayiPrenatalKomplikasi        string `json:"prenatal_komplikasi" validate:"omitempty"`
		AseskepBayiPrenatalHis               string `json:"prenatal_his" validate:"omitempty"`
		AseskepBayiPrenatalTtp               string `json:"prenatal_ttp" validate:"omitempty"`
		AseskepBayiPrenatalKetuban           string `json:"prenatal_ketuban" validate:"omitempty"`
		AseskepBayiPrenatalJam               string `json:"prenatal_jam" validate:"omitempty"`
		AseskepBayiPrenatalKejang            string `json:"prenatal_kejang" validate:"omitempty"`
		AseskepBayiPrenatalRasaMengejam      string `json:"prenatal_rasa_mengejam" validate:"omitempty"`
		AseskepBayiPrenatalNyeriPada         string `json:"prenatal_nyeri_pada" validate:"omitempty"`
		AseskepBayiPrenatalObatObatan        string `json:"prenatal_obat_obatan" validate:"omitempty"`
		AseskepBayiPrenatalKebiasaanIbu      string `json:"prenatal_kebiasaan_ibu" validate:"omitempty"`
		AseskepBayiPrenatalJumlahHari        string `json:"prenatal_jumlah_hari" validate:"omitempty"`
		AseskepBayiPrenatalRiwayatPersalinan string `json:"prenatal_riwayat_persalinan" validate:"omitempty"`
		AseskepBayiRwtUsiaPersalinan         string `json:"rwt_usia_persalinan" validate:"omitempty"`
		AseskepBayiRwtLahirDengan            string `json:"rwt_lahir_dengan" validate:"omitempty"`
		AseskepBayiRwtJenisKelamin           string `json:"rwt_jenis_kelamin" validate:"omitempty"`
		AseskepBayiRwtTglKelahiranBayi       string `json:"rwt_kelahiran_bayi" validate:"omitempty"`
		AseskepBayiRwtMenangis               string `json:"rwt_menangis" validate:"omitempty"`
		AseskepBayiRwtKeterangan             string `json:"rwt_keterangan" validate:"omitempty"`
		AseskepBayiPrenatalUsiaPersalinan    string `json:"prenatal_usia_persalinan" validate:"omitempty"`
		AseskepBayiNatalPersalinan           string `json:"natal_persalinan" validate:"omitempty"`
		AseskepBayiNatalKpd                  string `json:"natal_kpd" validate:"omitempty"`
		AseskepBayiNatalKeadaan              string `json:"natal_keadaan" validate:"omitempty"`
		AseskepBayiNatalTindakanDiberikan    string `json:"natal_tindakan_diberikan" validate:"omitempty"`
		AseskepBayiNatalPost                 string `json:"natal_post" validate:"omitempty"`
		AseskepBayiNatalPresentasi           string `json:"natal_prestasi" validate:"omitempty"`
		AseskepBayiNatalDitolongOleh         string `json:"natal_ditolong_oleh" validate:"omitempty"`
		AseskepBayiNatalKetuban              string `json:"natal_ketuban" validate:"omitempty"`
		AseskepBayiNatalLetak                string `json:"natal_letak" validate:"omitempty"`
		AseskepBayiNatalLahirUmur            string `json:"natal_lahir_umur" validate:"omitempty"`
		NatalVolumen                         string `json:"natal_volume" validate:"omitempty"`
		NatalKomplikasi                      string `json:"natal_komplikasi" validate:"omitempty"`
		RwtObatObatanPrenatal                string `json:"obat_obatan_yang_dikomsumsi" validate:"omitempty"`
	}

	ReqGetPemeriksaanFisikBangsal struct {
		Noreg  string `json:"noreg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ResposeKeluhanUtamaIGD struct {
		RiwayatPenyakit []rme.KeluhanUtama            `json:"riwayat_terdahulu"`
		RiwayatKeluarga []rme.DAlergi                 `json:"riwayat_keluarga"`
		KeluhanUtama    rme.ResponseAsesemenDokterIGD `json:"asesmen"`
	}

	// OnSave Data Alergi Obat
	ReqOnSaveDataAlergiObat struct {
		NoRm       string `json:"no_rm" validate:"required"`
		Kelompok   string `json:"kelompok" validate:"required"`
		Alergi     string `json:"alergi" validate:"required"`
		InsertUser string `json:"nama_user" validate:"required"`
	}

	ReqOnSaveRiwayatPenyakitKeluarga struct {
		NoRm       string `json:"no_rm" validate:"required"`
		Alergi     string `json:"alergi" validate:"required"`
		InsertUser string `json:"nama_user" validate:"required"`
	}

	ReqOnDeleteDataAlergiObat struct {
		Nomor    int    `json:"nomor" validate:"required"`
		NoRm     string `json:"no_rm" validate:"omitempty"`
		Kelompok string `json:"kelompok" validate:"omitempty"`
	}

	ReqOnSaveDaskepSLKI struct {
		NoDaskep   string `json:"no_daskep" validate:"required"`
		KodeSLKI   string `json:"kode_slki" validate:"required"`
		IdKriteria int    `json:"id_kriteria" validate:"required"`
		Hasil      int    `json:"hasil" validate:"required"`
	}

	ReqOnDeleteAsuhanKeperawatan struct {
		NoDaskep string `json:"no_daskep" validate:"required"`
	}

	RegKodeSiki struct {
		KodeSiki string `json:"kode_siki" binding:"required"`
	}

	ResponseIntervensi struct {
		Judul     string `json:"judul"`
		NoUrut    int    `json:"no_urut"`
		Tanda     string `json:"tanda"`
		Menurun   string `json:"menurun"`
		Meningkat string `json:"meningkat"`
		Memburuk  string `json:"memburuk"`
	}

	MapperSLKI struct {
		Kode         string         `json:"kode"`
		Ekspektasi   string         `json:"ekspetasi"`
		Judul        string         `json:"judul"`
		ResponseSLKI []ResponseSLKI `json:"slki"`
	}

	//  ===================== DIAGNOSA KEPERAWATAN

	ReqAsesmenKeperawatan struct {
		Asesemen   Asesemen     `json:"asesmen" binding:"required"`
		Intervensi []Intervensi `json:"intervensi"  binding:"omitempty"`
		Sdki       string       `json:"sdki" binding:"omitempty"`
		Siki       string       `json:"siki" binding:"omitempty"`
	}

	// PERUBAHAN PADA ASUHAN KEPERAWATAN
	RegSaveAsuhanKeperawatan struct {
		Noreg      string       `json:"noreg" binding:"required"`
		Intervensi []Intervensi `json:"intervensi"  binding:"required"`
		Sdki       string       `json:"sdki" binding:"required"`
		Siki       string       `json:"siki" binding:"required"`
	}

	ReqVitalSignBangsal struct {
		DeviceID     string `json:"device_id" validate:"required"`
		Pelayanan    string `json:"pelayanan" validate:"required"`
		Noreg        string `json:"noreg" validate:"required"`
		Person       string `json:"person" validate:"required"`
		Kategori     string `json:"kategori" validate:"required"`
		TekananDarah string `json:"tekanan_darah" validate:"omitempty"`
		Nadi         string `json:"nadi" validate:"omitempty"`
		Pernapasan   string `json:"pernapasan" validate:"omitempty"`
		Suhu         string `json:"suhu" validate:"omitempty"`
		TinggiBadan  string `json:"tinggi_badan" validate:"omitempty"`
		BeratBadan   string `json:"berat_badan" validate:"omitempty"`
	}

	ReqKeadaanUmumBangsal struct {
		DeviceID        string `json:"device_id" validate:"required"`
		Pelayanan       string `json:"pelayanan" validate:"required"`
		Noreg           string `json:"noreg" validate:"required"`
		Person          string `json:"person" validate:"required"`
		KeadaanUmum     string `json:"keadaan_umum" validate:"omitempty"`
		Kesadaran       string `json:"kesadaran" validate:"omitempty"`
		KesadaranDetail string `json:"kesadaran_detail" validate:"omitempty"`
	}

	RegVitalSignGangguanPerilaku struct {
		DeviceID       string `json:"device_id" validate:"required"`
		Pelayanan      string `json:"pelayanan" validate:"required"`
		Noreg          string `json:"noreg" validate:"required"`
		Person         string `json:"person" validate:"required"`
		JalanNafas     string `json:"jalan_nafas" validate:"omitempty"`
		Td             string `json:"tekanan_darah" validate:"omitempty"`
		Pernafasan     string `json:"pernafasan" validate:"omitempty"`
		PupilKiri      string `json:"pupil_kiri" validate:"omitempty"`
		PupilKanan     string `json:"pupil_kanan" validate:"omitempty"`
		Nadi           string `json:"nadi" validate:"omitempty"`
		Spo2           string `json:"spo2" validate:"omitempty"`
		CahayaKanan    string `json:"cahaya_kanan" validate:"omitempty"`
		CahayaKiri     string `json:"cahaya_kiri" validate:"omitempty"`
		Suhu           string `json:"suhu" validate:"omitempty"`
		Akral          string `json:"akral" validate:"omitempty"`
		GcsE           string `json:"gcs_e" validate:"omitempty"`
		GcsM           string `json:"gcs_m" validate:"omitempty"`
		GcsV           string `json:"gcs_v" validate:"omitempty"`
		Gangguan       string `json:"gangguan" validate:"omitempty"`
		GangguanDetail string `json:"gangguan_detail" validate:"omitempty"`
	}

	ReqSkalaNyeriTriaseeIGD struct {
		DeviceID       string `json:"device_id" validate:"required"`
		Pelayanan      string `json:"pelayanan" validate:"required"`
		Noreg          string `json:"noreg" validate:"required"`
		Person         string `json:"person" validate:"required"`
		Nyeri          int    `json:"nyeri" validate:"omitempty"`
		NyeriP         string `json:"nyeri_p" validate:"omitempty"`
		NyeriQ         string `json:"nyeri_q" validate:"omitempty"`
		NyeriR         string `json:"nyeri_r" validate:"omitempty"`
		NyeriS         string `json:"nyeri_s" validate:"omitempty"`
		NyeriT         string `json:"nyeri_t" validate:"omitempty"`
		FlaccWajah     int    `json:"flacc_wajah" validate:"omitempty"`
		FlaccKaki      int    `json:"flacc_kaki" validate:"omitempty"`
		FlaccAktifitas int    `json:"flacc_aktifitas" validate:"omitempty"`
		FlaccMenangis  int    `json:"flacc_menangis" validate:"omitempty"`
		FlaccBersuara  int    `json:"flacc_bersuara" validate:"omitempty"`
		FlaccTotal     int    `json:"flacc_total" validate:"omitempty"`
		SkalaTriase    int    `json:"skala_triase" validate:"omitempty"`
	}

	// ============================== MAPPING DATA
	ResResikoDekkubitusIGD struct {
		Dekubitus1    string `json:"dekubitus1"`
		Dekubitus2    string `json:"dekubitus2"`
		Dekubitus3    string `json:"dekubitus3"`
		Dekubitus4    string `json:"dekubitus4"`
		DekubitusAnak string `json:"dekubitus_anak"`
	}

	ResInformasiDanKeluhanIGD struct {
		PerolehanInfo       string                 `json:"info"`
		PerolehanInfoDetail string                 `json:"info_detail"`
		CaraMasuk           string                 `json:"cara_masuk"`
		CaraMasukDetail     string                 `json:"cara_masuk_detail"`
		AsalMasuk           string                 `json:"asal_masuk"`
		AsalMasukDetail     string                 `json:"asal_masuk_detail"`
		Bb                  string                 `json:"berat_badan"`
		Tb                  string                 `json:"tinggi_badan"`
		RwytPnykt           string                 `json:"riwayat_penyakit"`
		RwytObat            string                 `json:"riwayat_obat"`
		AsesFungsional      string                 `json:"fungsional"`
		Rj1                 string                 `json:"resiko_jatuh1"`
		Rj2                 string                 `json:"resiko_jatuh2"`
		HslKajiRj           string                 `json:"hasil_kaji"`
		RiwayatPenyakit     []soap.RiwayatPenyakit `json:"riwayat"`
	}

	ResponseAsesmenAwalIGD struct {
		PerolehanInfo       string                 `json:"info"`
		PerolehanInfoDetail string                 `json:"info_detail"`
		CaraMasuk           string                 `json:"cara_masuk"`
		CaraMasukDetail     string                 `json:"cara_masuk_detail"`
		AsalMasuk           string                 `json:"asal_masuk"`
		AsalMasukDetail     string                 `json:"asal_masuk_detail"`
		AsesFungsional      string                 `json:"fungsional"`
		Riwayat             []soap.RiwayatPenyakit `json:"riwayat"`
	}

	ResSkalaNyeriTriaseIGD struct {
		Nyeri          int    `json:"nyeri"`
		NyeriP         string `json:"nyeri_p"`
		NyeriQ         string `json:"nyeri_q"`
		NyeriR         string `json:"nyeri_r"`
		NyeriS         string `json:"nyeri_s"`
		NyeriT         string `json:"nyeri_t"`
		FlaccWajah     int    `json:"flacc_wajah"`
		FlaccKaki      int    `json:"flacc_kaki"`
		FlaccAktifitas int    `json:"flacc_aktifitas"`
		FlaccMenangis  int    `json:"flacc_menangis"`
		FlaccBersuara  int    `json:"flacc_bersuara"`
		FlaccTotal     int    `json:"flacc_total"`
		SkalaTriase    int    `json:"skala_triase"`
	}

	ResVitalSignbangsal struct {
		KeadaanUmum  string `json:"keadaan_umum" validate:"omitempty"`
		Kesadaran    string `json:"kesadaran" validate:"omitempty"`
		TekananDarah string `json:"tekanan_darah" validate:"omitempty"`
		Nadi         string `json:"nadi" validate:"omitempty"`
		Pernapasan   string `json:"pernapasan" validate:"omitempty"`
		Suhu         string `json:"suhu" validate:"omitempty"`
		TinggiBadan  string `json:"tinggi_badan" validate:"omitempty"`
		BeratBadan   string `json:"berat_badan" validate:"omitempty"`
	}

	// RESPONSE IGD VITAL SIGN
	ResTriaseTandaVitalIGD struct {
		JalanNafas     string `json:"jalan_nafas"`
		Td             string `json:"tekanan_darah"`
		Pernafasan     string `json:"pernafasan"`
		PupilKiri      string `json:"pupil_kiri"`
		PupilKanan     string `json:"pupil_kanan"`
		Nadi           string `json:"nadi"`
		Spo2           string `json:"spo2"`
		CahayaKanan    string `json:"cahaya_kanan"`
		CahayaKiri     string `json:"cahaya_kiri"`
		Suhu           string `json:"suhu"`
		Akral          string `json:"akral"`
		GcsE           string `json:"gcs_e"`
		GcsM           string `json:"gcs_m"`
		GcsV           string `json:"gcs_v"`
		Gangguan       string `json:"gangguan"`
		GangguanDetail string `json:"gangguan_detail"`
	}

	ResponseVitalSignICU struct {
		TekananDarah string `json:"tekanan_darah"`
		Nadi         string `json:"nadi"`
		BeratBadan   string `json:"berat_badan"`
		Suhu         string `json:"suhu"`
		Pernapasan   string `json:"pernapasan"`
		TinggiBadan  string `json:"tinggi_badan"`
		Spo2         string `json:"spo2"`
	}

	RegNoRm struct {
		Norm string `json:"no_rm" binding:"required"`
	}

	ReqDeleteCPPT struct {
		No   int    `json:"no" binding:"required"`
		Norm string `json:"no_rm" binding:"required"`
	}

	ReqPemeriksaanFisikBangsal struct {
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
		Noreg            string `json:"noreg" validate:"required"`
		Person           string `json:"person" validate:"required"`
		Kategori         string `json:"kategori" validate:"required"`
		Kepala           string `json:"kepala" validate:"omitempty"`
		Leher            string `json:"leher" validate:"omitempty"`
		Dada             string `json:"dada" validate:"omitempty"`
		Abdomen          string `json:"abdomen" validate:"omitempty"`
		Punggung         string `json:"punggung" validate:"omitempty"`
		Genetalia        string `json:"genetalia" validate:"omitempty"`
		Ekstremitas      string `json:"ekstremitas" validate:"omitempty"`
		LainLain         string `json:"lain_lain" validate:"omitempty"`
		PemeriksaanFisik string `json:"pemeriksaan_fisik" validate:"omitempty"`
	}

	ReqPemeriksaanFisikAnak struct {
		DeviceID  string `json:"device_id" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`
		Kategori  string `json:"kategori" validate:"required"`

		// ================ PEMERIKSAAN FISIK ANAK  ==================== //
		Mata         string `json:"mata" validate:"omitempty"`
		Mulut        string `json:"mulut" validate:"omitempty"`
		Gigi         string `json:"gigi" validate:"omitempty"`
		Thyroid      string `json:"thyroid" validate:"omitempty"`
		Paru         string `json:"paru" validate:"omitempty"`
		Jantung      string `json:"jantung" validate:"omitempty"`
		DindingDada  string `json:"dinding_dada" validate:"omitempty"`
		Epigastrium  string `json:"epigatrium" validate:"omitempty"`
		Supratermal  string `json:"supratermal" validate:"omitempty"`
		Retraksi     string `json:"retraksi" validate:"omitempty"`
		Hepar        string `json:"hepar" validate:"omitempty"`
		HeparDetail  string `json:"hepar_detail" validate:"omitempty"`
		Limpa        string `json:"limpa" validate:"omitempty"`
		LimpaDetail  string `json:"limpa_detail" validate:"omitempty"`
		Ginjal       string `json:"ginjal" validate:"omitempty"`
		GinjalDetail string `json:"ginjal_detail" validate:"omitempty"`
		Genetalia    string `json:"genetalia" validate:"omitempty"`
		Ouf          string `json:"ouf" validate:"omitempty"`
		Ekstremitas  string `json:"ekstremitas" validate:"omitempty"`
		TugorKulit   string `json:"tugor_kulit" validate:"omitempty"`
		// ================ PEMERIKSAAN FISIK ANAK  ==================== //
	}

	//
	ReqPemeriksaanFisikIGD struct {
		DeviceID       string `json:"device_id" validate:"required"`
		Pelayanan      string `json:"pelayanan" validate:"required"`
		Noreg          string `json:"noreg" validate:"required"`
		Person         string `json:"person" validate:"required"`
		Mata           string `json:"mata" validate:"omitempty"`
		Tht            string `json:"tht" validate:"omitempty"`
		Mulut          string `json:"mulut" validate:"omitempty"`
		Gigi           string `json:"gigi" validate:"omitempty"`
		Thyroid        string `json:"thyroid" validate:"omitempty"`
		LeherLainnya   string `json:"leher_lainnya" validate:"omitempty"`
		DindingDada    string `json:"dinding_dada" validate:"omitempty"`
		SuaraJantung   string `json:"suara_jantung" validate:"omitempty"`
		SuaraParu      string `json:"suara_paru" validate:"omitempty"`
		DindingPerut   string `json:"dinding_perut" validate:"omitempty"`
		Hati           string `json:"hati" validate:"omitempty"`
		Lien           string `json:"lien" validate:"omitempty"`
		PeristatikUsus string `json:"peristatik_usus" validate:"omitempty"`
		AbdomenDetail  string `json:"abdomen_detail" validate:"omitempty"`
		Kulit          string `json:"kulit" validate:"omitempty"`
		Ginjal         string `json:"ginjal" validate:"omitempty"`
		Genetalia      string `json:"genetalia" validate:"omitempty"`
		Superior       string `json:"superior" validate:"omitempty"`
		Inferior       string `json:"inferior" validate:"omitempty"`
	}

	ReqInsertDcpptPasien struct {
		DeviceId     string `json:"device_id" binding:"required"`
		Kelompok     string `json:"kelompok" binding:"required"`
		Waktu        string `json:"waktu" binding:"omitempty"`
		Pelayanan    string `json:"pelayanan" binding:"required"`
		KdBagian     string `json:"kd_bagian" binding:"required"`
		Noreg        string `json:"noreg" binding:"required"`
		Dpjp         string `json:"dpjp" binding:"required"`
		Sujektif     string `json:"subjektif" binding:"required"`
		Objektif     string `json:"objektif" binding:"required"`
		Asesmen      string `json:"asesmen" binding:"required"`
		Plan         string `json:"plan" binding:"required"`
		InstruksiPpa string `json:"ppa" binding:"omitempty"`
	}

	///
	Asesemen struct {
		Noreg                         string `json:"noreg" binding:"required"`
		AseskepPerolehanInfo          string `json:"info" binding:"omitempty"`
		AseskepCaraMasuk              string `json:"cara_masuk" binding:"omitempty"`
		AseskepCaraMasukDetail        string `json:"cara_masuk_detail" binding:"omitempty"`
		AseskepAsalMasuk              string `json:"asal_masuk" binding:"omitempty"`
		AseskepAsalMasukDetail        string `json:"asal_masuk_detail" binding:"omitempty"`
		AseskepBb                     string `json:"bb" binding:"omitempty"`
		AseskepTb                     string `json:"tb" binding:"omitempty"`
		AseskepRwytPnykt              string `json:"rwt_penyakit" binding:"omitempty"`
		AseskepRwytObatDetail         string `json:"obat_detail" binding:"omitempty"`
		AseskepAsesFungsional         string `json:"fungsional" binding:"omitempty"`
		AseskepRj1                    string `json:"rj1" binding:"omitempty"`
		AseskepRj2                    string `json:"rj2" binding:"omitempty"`
		AseskepHslKajiRj              string `json:"kaji_rj" binding:"omitempty"`
		AseskepHslKajiRjTind          string `json:"kaji_rj_tindakan" binding:"omitempty"`
		AseskepSkalaNyeri             int    `json:"skala_nyeri" binding:"omitempty"`
		AseskepFrekuensiNyeri         string `json:"frekuensi_nyeri" binding:"omitempty"`
		AseskepLamaNyeri              string `json:"lama_nyeri" binding:"omitempty"`
		AseskepNyeriMenjalar          string `json:"nyeri_menjalar" binding:"omitempty"`
		AseskepNyeriMenjalarDetail    string `json:"menjalar_detail" binding:"omitempty"`
		AseskepKualitasNyeri          string `json:"kualitas_nyeri" binding:"omitempty"`
		AseskepNyeriPemicu            string `json:"nyeri_pemicu" binding:"omitempty"`
		AseskepNyeriPengurang         string `json:"nyeri_pengurang" binding:"omitempty"`
		AseskepKehamilan              string `json:"kehamilan" binding:"omitempty"`
		AseskepKehamilanGravida       string `json:"kehamilan_gravida" binding:"omitempty"`
		AseskepKehamilanPara          string `json:"kehamilan_para" binding:"omitempty"`
		AseskepKehamilanAbortus       string `json:"kehamilan_abortus" binding:"omitempty"`
		AseskepKehamilanHpht          string `json:"kehamilan_hpht" binding:"omitempty"`
		AseskepKehamilanTtp           string `json:"kehamilan_ttp" binding:"omitempty"`
		AseskepKehamilanLeopold1      string `json:"kehamilan_leopol1" binding:"omitempty"`
		AseskepKehamilanLeopold2      string `json:"kehamilan_leopol2" binding:"omitempty"`
		AseskepKehamilanLeopold3      string `json:"kehamilan_leopol3" binding:"omitempty"`
		AseskepKehamilanLeopold4      string `json:"kehamilan_leopol4" binding:"omitempty"`
		AseskepKehamilanDjj           string `json:"kehamilan_djj" binding:"omitempty"`
		AseskepKehamilanVt            string `json:"kehamilan_vt" binding:"omitempty"`
		AseskepDekubitus1             string `json:"dekubitus1" binding:"omitempty"`
		AseskepDekubitus2             string `json:"dekubitus2" binding:"omitempty"`
		AseskepDekubitus3             string `json:"dekubitus3" binding:"omitempty"`
		AseskepDekubitus4             string `json:"dekubitus4" binding:"omitempty"`
		AseskepDekubitusAnak          string `json:"dekubitus_anak" binding:"omitempty"`
		AseskepPulangKondisi          string `json:"pulang_kondisi" binding:"omitempty"`
		AseskepPulangTransportasi     string `json:"pulang_transportasi" binding:"omitempty"`
		AseskepPulangPendidikan       string `json:"pendidikan" binding:"omitempty"`
		AseskepPulangPendidikanDetail string `json:"pendidikan_detail" binding:"omitempty"`
	}

	IntervensiSelection struct {
		SelectionNumber int  `json:"selected_number" binding:"required"`
		Slki            Slki `json:"slki" binding:"required"`
	}

	Intervensi struct {
		Judul     string `json:"judul" binding:"required"`
		Kode      string `json:"kode" binding:"required"`
		Ekspetasi string `json:"ekspetasi" binding:"required"`
		Slki      []Slki `json:"slki" binding:"required"`
	}

	Slki struct {
		Judul           string `json:"judul" binding:"required"`
		NoUrut          int    `json:"no_urut" binding:"required"`
		Menurun         string `json:"menurun" binding:"omitempty"`
		Meningkat       string `json:"meningkat" binding:"omitempty"`
		Memburuk        string `json:"memburuk" binding:"omitempty"`
		Kode            string `json:"kode" binding:"required"`
		Ekspektasi      string `json:"ekspektasi" binding:"omitempty"`
		SelectionNumber int    `json:"selected_number" binding:"required"`
	}

	DaskepModelResponse struct {
		Tangal string `json:"tanggal"`
		NoUrut int    `json:"no_urut"`
		Nilai  int    `json:"nilai"`
		Hasil  string `json:"hasil"`
		Siki   string `json:"siki"`
		Slki   string `json:"slki"`
		Sdki   string `json:"sdki"`
	}

	// ================================== RESPONSE
	ResponseSLKI struct {
		Judul          string   `json:"judul"`
		Nomor          int      `json:"no"`
		NoUrut         int      `json:"no_urut"`
		Tanda          string   `json:"tanda"`
		Menurun        string   `json:"menurun"`
		Meningkat      string   `json:"meningkat"`
		Memburuk       string   `json:"memburuk"`
		Kode           string   `json:"kode"`
		Ekspektasi     string   `json:"ekspektasi"`
		Kriteria       []string `json:"kriteria"`
		SelectedNumber int      `json:"selected_number"`
	}

	ResponsePemeriksaanFisikBangsal struct {
		Kepala           string `json:"kepala"`
		Mata             string `json:"mata"`
		Tht              string `json:"tht"`
		Mulut            string `json:"mulut"`
		Gigi             string `json:"gigi"`
		Thyroid          string `json:"thyroid"`
		LeherLainnya     string `json:"leherlainnya"`
		DindingDada      string `json:"diding_dada"`
		SuaraJantung     string `json:"suara_jantung"`
		SuaraParu        string `json:"suara_paru"`
		DindingPerut     string `json:"dinding_perut"`
		Hati             string `json:"hati"`
		Lien             string `json:"lien"`
		PeristaltikUsus  string `json:"peristaltik_usus"`
		AbdomenLainnya   string `json:"abdomen_lainnya"`
		Kulit            string `json:"kulit"`
		Ginjal           string `json:"ginjal"`
		Leher            string `json:"leher"`
		Dada             string `json:"dada"`
		Abdomen          string `json:"abdomen"`
		Punggung         string `json:"punggung"`
		Genetalia        string `json:"genetalia"`
		Ekstremitas      string `json:"ekstremitas"`
		LainLain         string `json:"lain_lain"`
		PemeriksaanFisik string `json:"pemeriksaan_fisik"`
	}

	ResponsePemeriksaanAnak struct {
		Mata         string `json:"mata"`
		Mulut        string `json:"mulut"`
		Gigi         string `json:"gigi"`
		Thyroid      string `json:"thyroid"`
		Paru         string `json:"paru"`
		Jantung      string `json:"jantung"`
		DindingDada  string `json:"dinding_dada"`
		Epigastrium  string `json:"epigatrium"`
		Supratermal  string `json:"supratermal"`
		Retraksi     string `json:"retraksi"`
		Hepar        string `json:"hepar"`
		HeparDetail  string `json:"hepar_detail"`
		Limpa        string `json:"limpa"`
		LimpaDetail  string `json:"limpa_detail"`
		Ginjal       string `json:"ginjal"`
		GinjalDetail string `json:"ginjal_detail"`
		Genetalia    string `json:"genetalia"`
		Ouf          string `json:"ouf"`
		Ekstremitas  string `json:"ekstremitas"`
		TugorKulit   string `json:"tugor_kulit"`
	}

	ResponsePemeriksaanFisikGID struct {
		Mata           string `json:"mata" validate:"omitempty"`
		Tht            string `json:"tht" validate:"omitempty"`
		Mulut          string `json:"mulut" validate:"omitempty"`
		Gigi           string `json:"gigi" validate:"omitempty"`
		Thyroid        string `json:"thyroid" validate:"omitempty"`
		LeherLainnya   string `json:"leher_lainnya" validate:"omitempty"`
		DindingDada    string `json:"dinding_dada" validate:"omitempty"`
		SuaraJantung   string `json:"suara_jantung" validate:"omitempty"`
		SuaraParu      string `json:"suara_paru" validate:"omitempty"`
		DindingPerut   string `json:"dinding_perut" validate:"omitempty"`
		Hati           string `json:"hati" validate:"omitempty"`
		Lien           string `json:"lien" validate:"omitempty"`
		PeristatikUsus string `json:"peristatik_usus" validate:"omitempty"`
		AbdomenDetail  string `json:"abdomen_detail" validate:"omitempty"`
		Kulit          string `json:"kulit" validate:"omitempty"`
		Ginjal         string `json:"ginjal" validate:"omitempty"`
		Genetalia      string `json:"genetalia" validate:"omitempty"`
		Superior       string `json:"superior" validate:"omitempty"`
		Inferior       string `json:"inferior" validate:"omitempty"`
	}

	DiagnosaKeperawatanResponse struct {
		Kode        string `json:"kode"`
		Judul       string `json:"judul"`
		MappingSlki string `json:"slki"`
		MappingSiki string `json:"siki"`
	}

	CPPTResponse struct {
		IdCppt       int    `json:"id_cppt" validate:"required"`
		Bagian       string `json:"bagian" validate:"omitempty"`
		InsertDttm   string `json:"insert_dttm" validate:"omitempty"`
		Tanggal      string `json:"tanggal" validate:"omitempty"`
		Subjektif    string `json:"subjektif" validate:"omitempty"`
		Objectif     string `json:"objectif" validate:"omitempty"`
		Asesmen      string `json:"asesmen" validate:"omitempty"`
		Plan         string `json:"plan" validate:"omitempty"`
		Ppa          string `json:"ppa" validate:"omitempty"`
		InstruksiPpa string `json:"instruksi_ppa" validate:"omitempty"`
		NamaUser     string `json:"nama_user" validate:"omitempty"`
	}

	ReqCPPTUpdate struct {
		IDCppt       int    `json:"id_cppt" validate:"required"`
		Bagian       string `json:"bagian" validate:"required"`
		Subjektif    string `json:"subjektif" validate:"omitempty"`
		Objektif     string `json:"objektif" validate:"omitempty"`
		Asesmen      string `json:"asesmen" validate:"omitempty"`
		Plan         string `json:"plan" validate:"omitempty"`
		Ppa          string `json:"ppa" validate:"omitempty"`
		InstruksiPpa string `json:"instruksi_ppa" validate:"omitempty"`
	}

	ResikoJatuhResponse struct {
		IsShow   bool   `json:"is_show"`
		Hasil    string `json:"hasil"`
		Tindakan string `json:"tindakan"`
	}

	ResponseDeskripsiSiki struct {
		Kode        string          `json:"kode"`
		Judul       string          `json:"judul"`
		Observasi   []SikiDeskripsi `json:"observasi"`
		Terapetutik []SikiDeskripsi `json:"terapetutik"`
		Edukasi     []SikiDeskripsi `json:"edukasi"`
		Kolaborasi  []SikiDeskripsi `json:"kolaborasi"`
	}

	// ================================== RESPONSE SLKI DENGA METHODE MENURUN, MENINGKAT DAN LAIN LAIN
	ResponseDataSLKI struct {
		KodeSlki  string                  `json:"kode_slki"`
		Judul     string                  `json:"judul"`
		Defenisi  string                  `json:"defenisi"`
		Ekspetasi string                  `json:"ekspetasi"`
		Menurun   []ResponseKriteriaModel `json:"menurun"`
		Meningkat []ResponseKriteriaModel `json:"meningkat"`
		Memburuk  []ResponseKriteriaModel `json:"memburuk"`
	}

	// LAKUKAN MAPPER PADA RESPONSE KRITERIA MODEL
	ResponseKriteriaModel struct {
		IdKriteria   int    `json:"id_kriteria"`
		NamaKriteria string `json:"nama"`
		KodeSlki     string `json:"kode_slki"`
		Tanda        bool   `json:"tanda"`
		IsSelected   bool   `json:"is_selected"`
		Kategori     string `json:"kategori"`
		NoUrut       int    `json:"no_urut"`
		Waktu        int    `json:"waktu"`
		TargetJam    string `json:"lama_waktu"`
		Target       int    `json:"target"`
		Skor         []int  `json:"skor"`
	}

	SikiDeskripsi struct {
		IdSiki     int    `json:"id_siki"`
		NoUrut     int    `json:"no_urut"`
		KodeSiki   string `json:"kode_siki"`
		Deskripsi  string `json:"deskripsi"`
		IsSelected bool   `json:"is_selected"`
	}

	SikiResponse struct {
		SDKIResponse SDKIResponse            `json:"diagnosa"`
		SIKIResponse []ResponseDataSLKI      `json:"slki"`
		SikiResponse []ResponseDeskripsiSiki `json:"siki"`
	}

	ResponseAlergi struct {
		Dahulu rme.RiwayatPenyakitDahulu `json:"penyakit_dahulu"`
		Alergi []rme.DAlergi             `json:"alergi"`
	}

	SikiRequest struct {
		DeviceID     string                  `json:"device_id"   validate:"required"`
		Noreg        string                  `json:"noreg"   validate:"required"`
		Pelayanan    string                  `json:"pelayanan"   validate:"required"`
		Person       string                  `json:"person"   validate:"required"`
		SDKIResponse SDKIResponse            `json:"diagnosa"   validate:"required"`
		SLKIResponse []ResponseDataSLKI      `json:"slki"   validate:"required"`
		SikiResponse []ResponseDeskripsiSiki `json:"siki"   validate:"required"`
	}

	SDKIResponse struct {
		Kode     string `json:"kode"`
		Judul    string `json:"judul"`
		Defenisi string `json:"defenisi"`
		Slki     string `json:"slki"`
		Siki     string `json:"siki"`
	}

	ResponseSkalaNyeri struct {
		Karyawan   user.Karyawan      `json:"karyawan"`
		SkalaNyeri SkalaNyeriResponse `json:"skala_nyeri"`
	}

	// RESPONSE PEMERIKSAAN FISIK
	ResponsePemeriksaanFisik struct {
		AlatKelamin                string `json:"alat_kelamin"`
		AnggotaGerak               string `json:"anggota_gerak"`
		Refleks                    string `json:"refleks"`
		Otot                       string `json:"otot"`
		RtVt                       string `json:"rt_vt"`
		GcsE                       string `json:"gcs_e"`
		GcsM                       string `json:"gcs_m"`
		GcsV                       string `json:"gcs_v"`
		PupilKiri                  string `json:"pupil_kiri"`
		PupilKanan                 string `json:"pupil_kanan"`
		Isokor                     string `json:"isokor"`
		IsokorDetail               string `json:"isokor_detail"`
		Anisokor                   string `json:"anisokor"`
		CahayaKanan                string `json:"cahaya_kanan"`
		CahayaKiri                 string `json:"cahaya_kiri"`
		Akral                      string `json:"akral"`
		Pupil                      string `json:"pupil"`
		Kesadaran                  string `json:"kesadaran"`
		Kepala                     string `json:"kepala"`
		Rambut                     string `json:"rambut"`
		Wajah                      string `json:"wajah"`
		KeadaanUmum                string `json:"keadaan_umum"`
		JalanNafas                 string `json:"jalan_nafas"`
		Sirkulasi                  string `json:"sirkulasi"`
		Gangguan                   string `json:"gangguan"`
		Kulit                      string `json:"kulit"`
		Abdomen                    string `json:"abdomen"`
		Ginjal                     string `json:"ginjal"`
		AbdomenLainnya             string `json:"abdomen_lainnya"`
		PeristatikUsus             string `json:"peristatik_usus"`
		Thyroid                    string `json:"thyroid"`
		Hati                       string `json:"hati"`
		Paru                       string `json:"paru"`
		Mata                       string `json:"mata"`
		Tht                        string `json:"tht"`
		Telinga                    string `json:"telinga"`
		Hidung                     string `json:"hidung"`
		Mulut                      string `json:"mulut"`
		Gigi                       string `json:"gigi"`
		Lidah                      string `json:"lidah"`
		Tenggorokan                string `json:"tenggorokan"`
		Leher                      string `json:"lehe"`
		Lien                       string `json:"lien"`
		LeherLainnya               string `json:"leher_lainnya"`
		Dada                       string `json:"dada"`
		Respirasi                  string `json:"repirasi"`
		Perut                      string `json:"perut"`
		Jantung                    string `json:"jantung"`
		Integument                 string `json:"integument"`
		Ekstremitas                string `json:"ekstremitas"`
		EkstremitasSuperior        string `json:"ekstremitas_superior"`
		EkstremitasInferior        string `json:"ekstremitas_inferior"`
		KemampuanGenggam           string `json:"kemampuan_gemgam"`
		Genetalia                  string `json:"genetalia"`
		Anus                       string `json:"anus"`
		Punggung                   string `json:"punggung"`
		LainLain                   string `json:"lain_lain"`
		Dindingdada                string `json:"dinding_dada"`
		DindingdadaRetEpigastrium  string `json:"epigastrium"`
		DindingdadaRetSuprastermal string `json:"supratermal"`
		DindingdadaRetraksi        string `json:"retraksi"`
		Hepar                      string `json:"hepar"`
		HeparDetail                string `json:"hepar_detail"`
		Limpa                      string `json:"limpa"`
		LimpaDetail                string `json:"limpa_detail"`
		Ouf                        string `json:"ouf"`
		TugorKulit                 string `json:"tugor_kulit"`
		PemeriksaanFisik           string `json:"pemeriksaan_fisik"`
		SkalaNyeri                 int    `json:"skala_nyeri"`
		SkalaNyeriP                string `json:"nyeri_p"`
		SkalaNyeriQ                string `json:"nyeri_q"`
		SkalaNyeriR                string `json:"nyeri_r"`
		SkalaNyeriS                string `json:"nyeri_s"`
		SkalaNyeriT                string `json:"nyeri_t"`
		FlaccWajah                 int    `json:"flacc_wajah"`
		FlaccKaki                  int    `json:"flacc_kaki "`
		FlaccAktifitas             int    `json:"flacc_aktivitas"`
		FlaccMenangis              int    `json:"flacc_menangis"`
		FlaccBersuara              int    `json:"flacc_bersuara"`
		FlaccTotal                 int    `json:"flacc_total"`
		SkalaTriase                int    `json:"skala_triase"`
	}

	ResponseAsesmenBayiResponse struct {
		Dokter           []rme.DokterBayi               `json:"dokter"`
		RiwayatKehamilan []ResponseRiwayatKelahiranLalu `json:"riwayat_kehamilan"`
		AsesemenBayi     ResponseAsesmenKeperawatanBayi `json:"asesmen_bayi"`
	}

	ResponseReportAsesmenBayiResponse struct {
		AsesemenBayi    ResponseAsesmenKeperawatanBayi `json:"asesmen_bayi"`
		KelahiranLalu   []ResponseRiwayatKelahiranLalu `json:"riwayat_kelahiran"`
		ApgarScore      []ApgarScoreResponse           `json:"apgar_score"`
		DownScore       NeoNatusresponse               `json:"down_score"`
		VitalSignPerina ResponseDVitalSignPerina       `json:"vital_sign"`
		FisikPerina     ResponseFisikPerina            `json:"fisik"`
		User            ReponseKaryawan                `json:"karyawan"`
	}

	ResponseTindakLajutPerina struct {
		AsesmenTindakanOperasi string `json:"tindakan_operasi"`
		AsesmedTindakLanjut    string `json:"tindak_lanjut"`
		AsesmenTglKontrolUlang string `json:"kontrol_ulang"`
	}

	ResponseIdentitasBayi struct {
		KakiKananBayi            string `json:"kaki_kanan_bayi"`
		KakiKiriBayi             string `json:"kaki_kiri_bayi"`
		TanganKiriIbu            string `json:"tangan_kiri_ibu"`
		AseskepTtdPenentuJk      string `json:"ttd_jk"`
		AseskepTtdWali           string `json:"ttd_wali"`
		AseskepNamaPenentuJk     string `json:"nama_penentu_jk"`
		AseskepNamaWali          string `json:"nama_wali"`
		AseskepPemberiGelangBayi string `json:"nama_pemberi_gelang_bayi"`
		JamKelahiranBayi         string `json:"jam_kelahiran"`
	}

	VitalSignPerina struct {
		BeratLahir   string `json:"berat_lahir"`
		WarnatKulit  string `json:"warna_kulit"`
		PanjangBadan string `json:"panjang_badan"`
	}

	IdentitasBayiRes struct {
		ResponseIdentitasBayi ResponseIdentitasBayi `json:"gambar"`
		ProfilPasien          report.DProfilePasien `json:"pasien"`
		VitalSign             VitalSignPerina       `json:"vital_sign"`
	}

	ResIdentitasBayi struct {
		ImageURL string `json:"image_url"`
		NoReg    string `json:"no_reg"`
		Kategori string `json:"kategori"`
	}

	ResponseResumeMedisPerinatologi struct {
		IdentitasBayi IdentitasBayi `json:"identitas_bayi"`
	}

	IdentitasBayi struct {
		NamaBayi      string `json:"nama_bayi"`
		TanggalLahir  string `json:"tgl_lahir"`
		NoRM          string `json:"no_rm"`
		NomorRegister string `json:"no_reg"`
		Umur          string `json:"umur"`
		JenisKelamin  string `json:"jk"`
		TanggalMasuk  string `json:"tgl_masuk"`
		TanggalKeluar string `json:"tgl_keluar"`
		DokterAnak    string `json:"dokter_anak"`
		NamaIbu       string `json:"nama_ibu"`
		Ruang         string `json:"ruang"`
		Agama         string `json:"agama"`
		Alamat        string `json:"alamat"`
		DokterObgyn   string `json:"dokter_obgyn"`
	}

	// REPONSE RINGKASAN MASUK PASIEN
	ResponseRingkasanMasukPasien struct {
		Pasien report.DProfilePasien `json:"pasien"`
	}

	// ===== //
	ResponseEarlyWarningSystem struct {
		Waktu            string              `json:"waktu"`
		IdEws            int                 `json:"id_ews"`
		TingkatKesadaran string              `json:"tingkat_kesadaran"`
		Noreg            string              `json:"noreg"`
		Td               int                 `json:"td"`
		Td2              int                 `json:"td2"`
		TotalSkor        int                 `json:"total_skor"`
		Keterangan       string              `json:"keterangan"`
		Nadi             int                 `json:"nadi"`
		Pernapasan       int                 `json:"pernapasan"`
		ReaksiOtot       string              `json:"reaksi_otot"`
		Suhu             float64             `json:"suhu"`
		Spo2             int                 `json:"spo2"`
		Crt              int                 `json:"crt"`
		SkalaNyeri       int                 `json:"skala_nyeri"`
		Karyawan         rme.MutiaraPengajar `json:"karyawan"`
	}

	ResponseCairanIntake struct {
		ID         int                  `json:"id"`
		Noreg      string               `json:"no_reg"`
		InsertDttm time.Time            `json:"insert_dttm"`
		Ngt        string               `json:"ngt"`
		Minum      string               `json:"minum"`
		Infuse     string               `json:"infuse"`
		Total      string               `json:"total"`
		Perawat    rme.UserPerawatModel `json:"perawat"`
		Pelayanan  rme.KPelayanan       `json:"pelayanan"`
	}

	ResponseDcairanOutPut struct {
		ID         int                  `json:"id"`
		Noreg      string               `json:"no_reg"`
		InsertDttm time.Time            `json:"insert_dttm"`
		Urine      string               `json:"urine"`
		NGT        string               `json:"ngt"`
		WSD        string               `json:"wsd"`
		Drain      string               `json:"drain"`
		Muntah     string               `json:"muntah"`
		Pendarahan string               `json:"pendarahan"`
		Total      string               `json:"total"`
		Perawat    rme.UserPerawatModel `json:"perawat"`
		Pelayanan  rme.KPelayanan       `json:"pelayanan"`
	}

	ResponseMonitoringCairan struct {
		Intake []ResponseCairanIntake  `json:"intake"`
		OutPut []ResponseDcairanOutPut `json:"out_put"`
	}
)
