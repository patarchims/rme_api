package dto

type (
	ReqTransfusiDarah struct {
		NoReg         string `json:"no_reg" validate:"required"`
		JenisDarah    string `json:"jenis_darah" validate:"required"`
		TglKadaluarsa string `json:"tgl_kadaluarsa" validate:"required"`
		GolDarah      string `json:"gol_darah" validate:"required"`
		JamPemberian  string `json:"jam_pemberian" validate:"required"`
		NoKantung     string `json:"no_kantung" validate:"required"`
	}

	ReqOnGetTransfusiDarah struct {
		NoReg string `json:"no_reg" validate:"required"`
	}

	ReqOnChangedTransfusiDarah struct {
		IDTransfusi   int    `json:"id_transfusi" validate:"required"`
		JenisDarah    string `json:"jenis_darah" validate:"required"`
		TglKadaluarsa string `json:"tgl_kadaluarsa" validate:"required"`
		GolDarah      string `json:"gol_darah" validate:"required"`
		JamPemberian  string `json:"jam_pemberian" validate:"required"`
		NoKantung     string `json:"no_kantung" validate:"required"`
	}

	ReqReaksiTranfusiDarah struct {
		IDTransfusi int    `json:"id_transfusi" validate:"required"`
		Noreg       string `json:"noreg" validate:"required"`
		Reaksi      string `json:"reaksi" validate:"required"`
		Keterangan  string `json:"keterangan" validate:"required"`
	}

	ReqDeleteReaksiTranfusiDarah struct {
		IDReaksi int `json:"id_reaksi" validate:"required"`
	}

	VerifyTransfusiDarah struct {
		IDTransfusi    int    `json:"id_transfusi" validate:"required"`
		Noreg          string `json:"noreg" validate:"required"`
		NoKantung      int    `json:"no_kantung" validate:"omitempty"`
		JenisDarah     string `json:"jenis_darah" validate:"omitempty"`
		GolonganDarah  string `json:"golongan_darah" validate:"omitempty"`
		NoStock        string `json:"no_stok" validate:"omitempty"`
		TglKadaluwarsa string `json:"tgl_kadaluwarsa" validate:"omitempty"`
	}

	OnGetVerifyTransfusiDarah struct {
		IDTransfusi int `json:"id_transfusi" validate:"required"`
	}

	ReqDeleteTransfusiDarah struct {
		IDTransfusi int `json:"id_transfusi" validate:"required"`
	}

	ReqViewReaksiDarah struct {
		IDTransfusi int `json:"id_transfusi" validate:"required"`
	}
)
