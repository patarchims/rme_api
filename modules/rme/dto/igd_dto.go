package dto

import (
	"hms_api/modules/rme"
	"hms_api/modules/user"
)

type (
	// === //
	ResponseDaskepPerawat struct {
		Daskep  []rme.DasKepDiagnosaModel `json:"daskep"`
		Perawat user.UserPerawat          `json:"perawat"`
	}

	ResponseIGD struct {
		Kepala              string `json:"kepala"`
		Mata                string `json:"mata"`
		Tht                 string `json:"tht"`
		Mulut               string `json:"mulut"`
		Leher               string `json:"leher"`
		Dada                string `json:"dada"`
		Jantung             string `json:"jantung"`
		Paru                string `json:"paru"`
		Perut               string `json:"perut"`
		Hati                string `json:"hati"`
		Limpa               string `json:"limpa"`
		Ginjal              string `json:"ginjal"`
		AlatKelamin         string `json:"alat_kelamin"`
		AnggotaGerak        string `json:"anggota_gerak"`
		Refleks             string `json:"refleks"`
		KekuatanOtot        string `json:"kekuatan_otot"`
		Kulit               string `json:"kulit"`
		KelenjarGetahBening string `json:"getah_bening"`
		RtVt                string `json:"rtvt"`
		JalanNafas          string `json:"jalan_nafas"`
		Sirkulasi           string `json:"sirkulasi"`
		Kesadaran           string `json:"kesadaran"`
		GcsE                string `json:"gcs_e"`
		GcsM                string `json:"gcs_m"`
		GcsV                string `json:"gcs_v"`
		Gigi                string `json:"gigi"`
		Abdomen             string `json:"abdomen"`
		Hidung              string `json:"hidung"`
		Telinga             string `json:"telinga"`
		Usus                string `json:"usus"`
		EkstremitasSuperior string `json:"superior"`
		EkstremitasInferior string `json:"inferior"`
		Anus                string `json:"anus"`
		AbdomenLainnya      string `json:"abdomen_lainnya"`
	}

	ResponseFisikICU struct {
		GcsE        string `json:"gcs_e"`
		GcsM        string `json:"gcs_m"`
		GcsV        string `json:"gcs_V"`
		Kesadaran   string `json:"kesadaran"`
		Kepala      string `json:"kepala"`
		Rambut      string `json:"rambut"`
		Wajah       string `json:"wajah"`
		Mata        string `json:"mata"`
		Telinga     string `json:"telinga"`
		Hidung      string `json:"hidung"`
		Mulut       string `json:"mulut"`
		Gigi        string `json:"gigi"`
		Lidah       string `json:"lidah"`
		Tenggorokan string `json:"tenggorokan"`
		Leher       string `json:"leher"`
		Dada        string `json:"dada"`
		Respirasi   string `json:"respirasi"`
		Jantung     string `json:"jantung"`
		Integumen   string `json:"integumen"`
		Ekstremitas string `json:"ekstremitas"`
		Pupil       string `json:"pupil"`
	}

	ResponsePemeriksaanFisikAnak struct {
		GcsE              string `json:"gcs_e"`
		GcsM              string `json:"gcs_m"`
		GcsV              string `json:"gcs_V"`
		Mata              string `json:"mata"`
		Telinga           string `json:"telinga"`
		Hidung            string `json:"hidung"`
		Mulut             string `json:"mulut"`
		LeherDanBahu      string `json:"leher_dan_bahu"`
		Dada              string `json:"dada"`
		Abdomen           string `json:"abdomen"`
		Peristaltik       string `json:"peristaltik"`
		Punggung          string `json:"punggung"`
		NutrisiDanHidrasi string `json:"nutrisi_dan_hidrasi"`
	}

	ResponseIGDMethodist struct {
		Kepala              string `json:"kepala"`
		Mata                string `json:"mata"`
		Tht                 string `json:"tht"`
		Mulut               string `json:"mulut"`
		Gigi                string `json:"gigi"`
		Leher               string `json:"leher"`
		KelenjarGetahBening string `json:"getah_bening"`
		Dada                string `json:"dada"`
		Jantung             string `json:"jantung"`
		Paru                string `json:"paru"`
		Perut               string `json:"perut"`
		Hati                string `json:"hati"`
		Limpa               string `json:"limpa"`
		Usus                string `json:"usus"`
		AbdomenLainnya      string `json:"abdomen_lainnya"`
		Ginjal              string `json:"ginjal"`
		AlatKelamin         string `json:"alat_kelamin"`
		Anus                string `json:"anus"`
		Superior            string `json:"superior"`
		Inferior            string `json:"inferior"`
	}

	ResponsePemerikssanFisikAntonio struct {
		PemeriksaanFisik string `json:"pem_fisik"`
	}

	ResponseDVitalSignPerina struct {
		Td            string `json:"td"`
		HR            string `json:"hr"`
		RR            string `json:"rr"`
		Spo2          string `json:"spo2"`
		BB            string `json:"bb"`
		TB            string `json:"tb"`
		LingkarKepala string `json:"lingkar_kepala"`
		LingkarLengan string `json:"lingkar_lengan"`
		LingkarDada   string `json:"lingkar_dada"`
		LingkarPerut  string `json:"lingkar_perut"`
		WarnaKulit    string `json:"warna_kulit"`
		KeadaanUmum   string `json:"keadaan_umum"`
		// BabinSki          string `json:"babin_ski"`
		// RefleksRooting    string `json:"refleks_rooting"`
		// RefleksSwallowing string `json:"refleks_swallowing"`
		// RefleksSucking    string `json:"refleks_sucking"`
		// RefleksMoro       string `json:"refleks_moro"`
		// RefleksGraps      string `json:"refleks_graps"`
	}

	ResponseFisikPerina struct {
		GcsE              string `json:"gcs_e"`
		GcsV              string `json:"gcs_v"`
		GcsM              string `json:"gcs_m"`
		Kesadaran         string `json:"kesadaran"`
		Kepala            string `json:"kepala"`
		TonickNeck        string `json:"tonick_neck"`
		Kelainan          string `json:"kelainan"`
		Wajah             string `json:"wajah"`
		Telinga           string `json:"telinga"`
		Hidung            string `json:"hidung"`
		Mulut             string `json:"mulut"`
		Refleks           string `json:"refleks"`
		LeherDanBahu      string `json:"leher_dah_bahu"`
		Dada              string `json:"dada"`
		Abdomen           string `json:"abdomen"`
		Punggung          string `json:"punggung"`
		Integumen         string `json:"integumen"`
		Ekstremitas       string `json:"ekstremitas"`
		Genetalia         string `json:"genetalia"`
		Anus              string `json:"anus"`
		BabinSki          string `json:"babin_ski"`
		RefleksRooting    string `json:"refleks_rooting"`
		RefleksSwallowing string `json:"refleks_swallowing"`
		RefleksSucking    string `json:"refleks_sucking"`
		RefleksMoro       string `json:"refleks_moro"`
		RefleksGraps      string `json:"refleks_graps"`
	}

	ReqKeluhanUtamaIGD struct {
		NoRM            string `json:"no_rm" validate:"required"`
		NoReg           string `json:"no_reg" validate:"required"`
		Tanggal         string `json:"tanggal" validate:"required"`
		Person          string `json:"person" validate:"required"`
		KeluhanUtama    string `json:"keluhan_utama" validate:"required"`
		RiwayatSekarang string `json:"riwayat_sekarang" validate:"required"`
		RiwayatDahulu   string `json:"rwt_dahulu" validate:"omitempty"`
		Pelayanan       string `json:"pelayanan" validate:"required"`
		DeviceID        string `json:"device_id" validate:"required"`
	}

	ReqRiwayatKeluargaIGD struct {
		NoRM                string `json:"no_rm" validate:"required"`
		NoReg               string `json:"no_reg" validate:"required"`
		Tanggal             string `json:"tanggal" validate:"required"`
		Person              string `json:"person" validate:"required"`
		NamaRiwayatPenyakit string `json:"riwayat" validate:"omitempty"`
	}

	ReqReportAsesmenAwalMedis struct {
		NoRM      string `json:"no_rm" validate:"required"`
		NoReg     string `json:"no_reg" validate:"required"`
		Tanggal   string `json:"tanggal" validate:"omitempty"`
		Person    string `json:"person" validate:"omitempty"`
		KdBagian  string `json:"kd_bagian" validate:"omitempty"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
	}

	//=================================== REPORT

)
