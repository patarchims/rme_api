package dto

import (
	"hms_api/modules/igd"
	// igdDTO "hms_api/modules/igd/dto"
	"hms_api/modules/kebidanan"
	"hms_api/modules/nyeri/dto"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	soapDTO "hms_api/modules/soap/dto"

	"hms_api/modules/user"
	"time"
)

type (
	ResponsePengkajianKeperawatan struct {
		DPemFisik       rme.DPemFisikModel                    `json:"dpemfisik"`
		Persistem       igd.ResponseDPengkajianPersistenRANAP `json:"persistem"`
		Pengkajian      soapDTO.ResponsePengkajianRawatInap   `json:"asesmen_keperawatan"`
		RiwayatPenyakit []kebidanan.DRiwayatPengobatanDirumah `json:"riwayat"`
		Fisik           ResponsePemeriksaanFisik              `json:"fisik"`
		VitalSign       soap.DVitalSignIGDDokter              `json:"vital"`
		Funsional       kebidanan.DPengkajianFungsional       `json:"fungsional"`
		Nutrisi         dto.ToResponsePengkajianNutrisiDewasa `json:"nutrisi"`
		Asuhan          []rme.DasKepDiagnosaModelV2           `json:"asuhan"`
		Karu            rme.MutiaraPengajar                   `json:"karu"`
		Perawat         rme.MutiaraPengajar                   `json:"perawat"`
		Nyeri           dto.ToResponsePengkajianAwalNyeri     `json:"nyeri"`
		Resikojatuh     igd.DRisikoJatuh                      `json:"resiko_jatuh"`
		// rmeDTO.ResponsePengkajianNyeriIGD
		// Nyeris          dtoIGD.ResponsePengkajianNyeriIGD     `json:"kenyamanan"`
	}

	ReponseTerapiCairan struct {
		Cairan      []ResposeDPemberianCairan `json:"cairan"`
		UserProfile user.ProfilePasien        `json:"pasien"`
	}

	ResposeDPemberianCairan struct {
		InsertDttm      time.Time            `json:"insert_dttm"`
		JumlahInfuse    string               `json:"jumlah_infuse"`
		SisaInfuse      string               `json:"sisa_infuse"`
		Noreg           string               `json:"noreg"`
		NamaCairan      string               `json:"nama_cairan"`
		DosisMakro      string               `json:"dosis_makro"`
		DosisMikro      string               `json:"dosis_mikro"`
		FlsKe           int                  `json:"fls_ke"`
		ObatDitambahkan string               `json:"obat_ditambahkan"`
		Keterangan      string               `json:"keterangan"`
		Perawat         rme.UserPerawatModel `json:"perawat"`
		Pelayanan       rme.KPelayanan       `json:"pelayanan"`
	}
)
