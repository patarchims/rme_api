package dto

type (

	// PERUBAHAN PADA ASUHAN KEPERAWATAN
	ReqSaveAsesmenNyeri struct {
		Noreg          string `json:"noreg" validate:"required"`
		IDFarmakologi  int    `json:"id_farmakologi" validate:"required"`
		NamaObat       string `json:"nama_obat" validate:"required"`
		DosisFrekuensi string `json:"dosis_frekuensi" validate:"required"`
		Rute           string `json:"rute" validate:"required"`
		SkorNyeri      int    `json:"skor_nyeri" validate:"omitempty"`
		NonFarmakologi int    `json:"non_farmakologi" validate:"omitempty"`
		WaktuKaji      int    `json:"waktu_kaji" validate:"omitempty"`
	}

	ReqNoReg struct {
		Noreg string `json:"noreg" validate:"required"`
	}

	ReqIDUlangNyeri struct {
		ID string `json:"id_ulang_nyeri" validate:"required"`
	}

	OnUpdateUlangNyeri struct {
		ID             string `json:"id_ulang_nyeri" validate:"required"`
		SkorNyeri      int    `json:"skor_nyeri" validate:"required"`
		NonFarmakologi int    `json:"non_farmakologi" validate:"required"`
		WaktuKaji      int    `json:"waktu_kaji" validate:"required"`
	}
)
