package dto

type (
	ReqDownScoreNeoNatus struct {
		Noreg string `json:"noreg" validate:"required"`
	}

	ReqApgarScore struct {
		Noreg string `json:"noreg" validate:"required"`
	}

	ReqSaveDownScreNeoNatus struct {
		Noreg          string `json:"noreg" validate:"required"`
		Person         string `json:"person" validate:"required"`
		FrekwensiNafas int    `json:"nifas"`
		Sianosis       int    `json:"sianosis"`
		Retraksi       int    `json:"retraksi"`
		AirEntry       int    `json:"air_entry"`
		Merintih       int    `json:"merintih"`
		Total          int    `json:"total"`
	}

	ReqReportAsesmenBayi struct {
		Noreg  string `json:"no_reg" validate:"required"`
		NoRm   string `json:"no_rm" validate:"required"`
		Person string `json:"person" validate:"omitempty"`
	}

	ReqGetAnalisaData struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqGetAsesmenSkalaNyeri struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	// TODO : ON ASAVE SKALA NYERI DEWASA
	ReqAsesmenSkalaNyeri struct {
		Noreg      string `json:"no_reg" validate:"required"`
		SkalaNyeri int    `json:"skala_nyeri" validate:"required"`
		Person     string `json:"person" validate:"required"`
		Pelayanan  string `json:"pelayanan" validate:"required"`
		DevicesID  string `json:"device_id" validate:"required"`
		// TAMBAHKAN FREKUENSI NYERI PADA DEWASA
		FrekuensiNyeri string `json:"frekuensi_nyeri" validate:"omitempty"`
		LokasiNyeri    string `json:"lokasi_nyeri" validate:"omitempty"`
		KualitasNyeri  string `json:"kualitas_nyeri" validate:"omitempty"`
		Menjalar       string `json:"menjalar" validate:"omitempty"`
	}

	OnRegGetIdentitasBayi struct {
		NoReg string `json:"no_reg" validate:"required"`
		NoRM  string `json:"no_rm" validate:"required"`
	}

	OnDeleteEarningSystem struct {
		ID int `json:"id" validate:"required"`
	}

	OnUpdateData struct {
		ID      int    `json:"id" validate:"required"`
		Catatan string `json:"catatan" validate:"required"`
	}

	OnGetEarningSystem struct {
		NoReg string `json:"no_reg" validate:"required"`
	}

	OnGetNoRM struct {
		NoRM string `json:"no_rm" validate:"required"`
	}

	ReqOnSaveDataEdukasiTerintegrasi struct {
		NoRm              string `json:"no_rm" validate:"required"`
		NoReg             string `json:"no_reg" validate:"required"`
		Informasi         string `json:"informasi" validate:"required"`
		Metode            string `json:"metode" validate:"required"`
		PemberiInformasi  string `json:"pemberi_informasi" validate:"required"`
		PenerimaInformasi string `json:"penerima_informasi" validate:"required"`
		Evaluasi          string `json:"evaluasi" validate:"required"`
	}

	ReqOnUpdateEdukasiTerintegrasi struct {
		Id                int    `json:"id_edukasi" validate:"required"`
		Informasi         string `json:"informasi" validate:"required"`
		Metode            string `json:"metode" validate:"required"`
		PemberiInformasi  string `json:"pemberi_informasi" validate:"required"`
		PenerimaInformasi string `json:"penerima_informasi" validate:"required"`
		Evaluasi          string `json:"evaluasi" validate:"required"`
	}

	ReqIDEdukasiTerintegrasi struct {
		ID int `json:"ID" validate:"required"`
	}

	ReqGetDataEdukasiTerintegrasi struct {
		NoRm string `json:"no_rm" validate:"required"`
	}

	OnAddEdukasiTerintegrasi struct {
		NoRM string `json:"no_rm" validate:"required"`
	}

	OnSaveCatatanKeperawtan struct {
		Catatan string `json:"catatan" validate:"required"`
		NoReg   string `json:"no_reg" validate:"required"`
	}

	OnRegSaveEarlyWarningSystemAnak struct {
		Kategori       string  `json:"kategori" validate:"required"`
		Noreg          string  `json:"no_reg" validate:"required"`
		KeadaanUmum    int     `json:"keadaan_umum" validate:"required"`
		KardioVaskular int     `json:"kario_vaskular" validate:"required"`
		Respirasi      int     `json:"repirasi" validate:"required"`
		Suhu           float64 `json:"suhu" validate:"required"`
		Spo2           float64 `json:"spo2" validate:"required"`
		Score          int     `json:"score" validate:"required"`
	}

	OnRegSaveEarlyWarningSystem struct {
		Kesadaran       string  `json:"kesadaran" validate:"required"`
		NoReg           string  `json:"noreg" validate:"required"`
		Td              int     `json:"td" validate:"required"`
		Td2             int     `json:"td2" validate:"required"`
		Nadi            int     `json:"nadi" validate:"required"`
		Pernapasan      int     `json:"pernapasan" validate:"required"`
		ReaksiOtot      string  `json:"reaksi_otot" validate:"required"`
		Suhu            float64 `json:"suhu" validate:"required"`
		Spo2            int     `json:"spo2" validate:"required"`
		Kategori        string  `json:"kategori" validate:"required"`
		Crt             int     `json:"crt" validate:"required"`
		Keterangan      string  `json:"keterangan" validate:"omitempty"`
		SkalaNyeri      int     `json:"skala_nyeri" validate:"required"`
		TotalSkor       int     `json:"total_skor" validate:"omitempty"`
		ObsitenTambahan string  `json:"obsigen_tambahan" validate:"omitempty"`
	}

	ReqReportAsesmenNyeri struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqRingkasanMasukDanKeluar struct {
		Noreg string `json:"no_reg" validate:"required"`
		NoRM  string `json:"no_rm" validate:"required"`
	}

	ReqOnDeleteAnalisaData struct {
		KodeAnalisa string `json:"kode_analisa" validate:"required"`
	}

	OnSaveImplementasiKeperawatan struct {
		Noreg     string `json:"no_reg" validate:"omitempty"`
		NoDaskep  string `json:"no_daskep" validate:"required"`
		Deskripsi string `json:"deskripsi" validate:"required"`
	}

	ReqResumeMedisPerinatologi struct {
		Noreg string `json:"no_reg" validate:"required"`
		NoRM  string `json:"no_rm" validate:"required"`
	}

	ReqSaveTindakLajutPerinatologi struct {
		Noreg                  string `json:"noreg" validate:"required"`
		Pelayanan              string `json:"pelayanan" validate:"required"`
		DeviceID               string `json:"device_id"`
		AsesmenTindakanOperasi string `json:"tindakan_operasi"`
		AsesmedTindakLanjut    string `json:"tindak_lanjut"`
		AsesmenTglKontrolUlang string `json:"kontrol_ulang"`
		Person                 string `json:"person"`
	}

	ReqValidasiAnalisaData struct {
		Noreg        string `json:"no_reg" validate:"required"`
		Tanggal      string `json:"tanggal" validate:"required"`
		Jam          string `json:"jam" validate:"required"`
		KodeAnalisa  string `json:"kode_analisa" validate:"required"`
		JenisAnalisa string `json:"jenis_analisa" validate:"omitempty"`
	}

	ReqOnSaveRiwayatKelahiranYangLalu struct {
		Noreg                string `json:"noreg" validate:"required"`
		Tahun                string `json:"tahun"`
		JenisKelamin         string `json:"jenis_kelamin"`
		BeratBadan           string `json:"berat_badan"`
		KeadaanBayi          string `json:"keadaan_bayi"`
		KomplikasiKehamilan  string `json:"komplikasi_kehamilan"`
		KomplikasiPersalinan string `json:"komplikasi_persalinan"`
		TempatPersalinan     string `json:"termpat_persalinan"`
		JenisPersalinan      string `json:"jenis_persalinan"`
	}

	ResponseRiwayatKelahiranLalu struct {
		KdRiwayat            string `json:"kd_riwayat"`
		TahunPersalinan      string `json:"tahun_persalinan"`
		NoRm                 string `json:"no_rm"`
		TempatPersalinan     string `json:"tempat_persalinan"`
		Noreg                string `json:"noreg"`
		UmurKehamilan        string `json:"umur_kehamilan"`
		JenisPersalinan      string `json:"jenis_persalinan"`
		Penolong             string `json:"penolong"`
		Penyulit             string `json:"penyulit"`
		Nifas                string `json:"nifas"`
		Jk                   string `json:"jk"`
		Bb                   string `json:"bb"`
		KeadaanSekarang      string `json:"keadaan_sekarang"`
		KomplikasiHamil      string `json:"komplikasi_hamil"`
		KomplikasiPersalinan string `json:"komplikasi_persalinan"`
	}

	ReponseKaryawan struct {
		Nama         string `json:"nama"`
		JenisKelamin string `json:"jk"`
	}

	ResponseAsesmenKeperawatanBayi struct {
		Tanggal                           string `json:"tanggal"`
		ObatObatanYangDikomsumsi          string `json:"obat_obatan_yang_dikomsumsi"`
		AseskepBayiDokterObgyn            string `json:"dokter_obgyn"`
		PendarahanPrenatal                string `json:"pendarahan_prenatal"`
		AseskepBayiDokterAnak             string `json:"dokter_anak"`
		NamaAyah                          string `json:"nama_ayah"`
		RiwayatPenyakitAyah               string `json:"riwayat_penyakit_ayah"`
		AseskepBayiPekerjaanAyah          string `json:"pekerjaan_ayah"`
		AseskepBayiPerkawinanAyahKe       string `json:"perkawinan_ayah"`
		AseskepUsiaKehamilan              string `json:"usia_kehamilan"`
		AseskepBayiNamaIbu                string `json:"nama_ibu"`
		PekerjaanIbu                      string `json:"pekerjaan_ibu"`
		AseskepBayiPerkawinanIbuKe        string `json:"perkawinan_ibu"`
		AseskepBayiRwtPenyakitIbu         string `json:"penyakit_ibu"`
		AseskepBayiNamaPjawab             string `json:"nama_pjawab"`
		AseskepBayiUsiaPjawab             string `json:"usia_pjawab"`
		AseskepBayiPrenatalKebiasaanIbu   string `json:"prenatal_kebiasaan_ibu"`
		AseskepBayiPekerjaanPjawab        string `json:"pekerjaan_pjawab"`
		AseskepBayiUsiaPersalinan         string `json:"usia_persalinan"`
		AseskepBayiTglLahir               string `json:"tgl_lahir"`
		AseskepBayiLahirDgn               string `json:"lahir_dengan"`
		AseskepBayiMenangis               string `json:"menangis"`
		AseskepBayiJk                     string `json:"jenis_kelamin"`
		AseskepBayiPrenatalJumlahHari     string `json:"jumlah_hari"`
		AseskepBayiKeterangan             string `json:"keterangan"`
		AseskepBayiPrenatalUsiaKehamilan  string `json:"prenatal_usia_kehamilan"`
		AseskepBayiPrenatalKomplikasi     string `json:"prenatal_komplikasi"`
		AseskepBayiPrenatalHis            string `json:"prenatal_his"`
		AseskepBayiPrenatalTtp            string `json:"prenatal_ttp"`
		AseskepBayiPrenatalKetuban        string `json:"prenatal_ketuban"`
		AseskepBayiPrenatalJam            string `json:"prenatal_jam"`
		AseskepBayiRwtUsiaPersalinan      string `json:"rwt_usia_persalinan"`
		AseskepBayiRwtLahirDengan         string `json:"rwt_lahir_dengan"`
		AseskepBayiRwtJenisKelamin        string `json:"rwt_jenis_kelamin"`
		AseskepBayiRwtTglKelahiranBayi    string `json:"rwt_kelahiran_bayi"`
		AseskepBayiRwtMenangis            string `json:"rwt_menangis"`
		AseskepBayiRwtKeterangan          string `json:"rwt_keterangan"`
		AseskepBayiPrenatalUsiaPersalinan string `json:"prenatal_usia_persalinan"`
		AseskepBayiNatalPersalinan        string `json:"natal_persalinan"`
		AseskepBayiNatalKpd               string `json:"natal_kpd"`
		AseskepBayiNatalKeadaan           string `json:"natal_keadaan"`
		AseskepBayiNatalTindakanDiberikan string `json:"natal_tindakan_diberikan"`
		AseskepBayiNatalPost              string `json:"natal_post"`
		AseskepBayiNatalPresentasi        string `json:"natal_prestasi"`
		AseskepBayiNatalDitolongOleh      string `json:"natal_ditolong_oleh"`
		AseskepBayiNatalKetuban           string `json:"natal_ketuban"`
		AseskepBayiNatalLetak             string `json:"natal_letak"`
		AseskepBayiNatalLahirUmur         string `json:"natal_lahir_umur"`
		NatalVolume                       string `json:"natal_volume"`
		NatalKomplikasi                   string `json:"natal_komplikasi"`
		PrenatalPersalinan                string `json:"prenatal_riwayat_persalinan"`
	}

	ReqSaveApgarScore struct {
		NoReg      string `json:"no_reg"`
		Waktu      string `json:"waktu"`
		DJantung   int    `json:"jantung"`
		UNafas     int    `json:"nafas"`
		Otot       int    `json:"otot"`
		Refleksi   int    `json:"refleksi"`
		WarnaKulit int    `json:"warna_kulit"`
		Total      int    `json:"total"`
	}

	NeoNatusresponse struct {
		FrekwensiNafas int `json:"nifas"`
		Sianosis       int `json:"sianosis"`
		Retraksi       int `json:"retraksi"`
		AirEntry       int `json:"air_entry"`
		Merintih       int `json:"merintih"`
		Total          int `json:"total"`
	}
	ApgarScoreResponse struct {
		IdAgpar    int    `json:"id"`
		Waktu      string `json:"waktu"`
		DJantung   int    `json:"jantung"`
		UNafas     int    `json:"nafas"`
		Otot       int    `json:"otot"`
		Refleksi   int    `json:"refleksi"`
		WarnaKulit int    `json:"warna_kulit"`
		Total      int    `json:"total"`
	}

	SkalaNyeriResponse struct {
		SkalaNyeri     int    `json:"skala_nyeri"`
		FrekuensiNyeri string `json:"frekuensi_nyeri"`
		LokasiNyeri    string `json:"lokasi_nyeri"`
		KualitasNyeri  string `json:"kualitas_nyeri"`
		Menjalar       string `json:"menjalar"`
	}

	// ResponseSaveAnalisaData
	ReqSaveAnalisaData struct {
		NoReg    string                   `json:"no_reg"`
		Data     string                   `json:"data"`
		Diagnosa []DiagnosaKeperawatanRes `json:"diagnosa"`
	}

	// RESPONSE ANALISA DATA
	DiagnosaKeperawatanRes struct {
		Kode  string `json:"kode"`
		Judul string `json:"judul"`
	}
)
