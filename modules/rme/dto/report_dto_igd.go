package dto

import (
	"hms_api/modules/diagnosa"
	"hms_api/modules/his"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
)

type (
	ReportAsesmenIGD struct {
		Pasien                user.ProfilePasien               `json:"pasien"`
		AsesmenIGD            rme.AsesemenMedisIGD             `json:"asesmen"`
		RiwayatPenyakitDahulu []rme.KeluhanUtama               `json:"riwayat"`
		Alergi                []rme.DAlergi                    `json:"alergi"`
		RencanaTindakLanjut   soap.RencanaTindakLanjutModel    `json:"tindak_lanjut"`
		Diagnosa              []soap.DiagnosaResponse          `json:"diagnosa"`
		PemeriksaanFisik      ResponseIGD                      `json:"fisik"`
		VitalSign             soap.DVitalSignIGDDokter         `json:"vital_sign"`
		Labor                 []his.ResHasilLaborTableLama     `json:"labor"`
		Radiologi             []his.RegHasilRadiologiTabelLama `json:"radiologi"`
		DiagnosaBanding       []diagnosa.DDiagnosaBanding      `json:"diagnosa_banding"`
	}
)
