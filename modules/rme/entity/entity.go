package entity

import (
	"hms_api/modules/diagnosa"
	"hms_api/modules/his"
	"hms_api/modules/igd"
	dtoIGD "hms_api/modules/igd/dto"
	"hms_api/modules/kebidanan"
	dtoKebidanan "hms_api/modules/kebidanan/dto"
	"hms_api/modules/nyeri"
	dtoNyeri "hms_api/modules/nyeri/dto"

	// SoapDTO
	"hms_api/modules/report"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/modules/soap"

	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
	"mime/multipart"

	"github.com/gofiber/fiber/v2"
)

type RMERepository interface {
	GetPemeriksaanFisikAnakBangsalRepository(noReg string, KDBagian string) (res rme.PemeriksaanFisikModel, err error)
	OnUpdateDpenkajianPersistemRepository(data igd.DpengkajianPersistem, noReg string, pelayanan string) (res igd.DpengkajianPersistem, err error)
	OnSaveDpengkajianPersistemRepository(data igd.DpengkajianPersistem) (res igd.DpengkajianPersistem, err error)
	// ========================= NYERI REPOSITORY =====================
	OnUpdateEdukasiTerintegrasiRepository(idEdukasi int, data rme.DEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, err error)
	OnUpdateDataVerifikasiTransfusiDarahByIDAndNoregRepository(data rme.DverifyTransfusiDarah, idTransfusi int, noReg string) (res rme.DverifyTransfusiDarah, err error)
	OnGetVerifikasiTransfusiDarahByIDTransfusiRepository(idTransfusi int) (res rme.DverifyTransfusiDarah, err error)
	OnSaveVerifikasiTransfusiDrahRepository(data rme.DverifyTransfusiDarah) (res rme.DverifyTransfusiDarah, err error)
	OnChangedTransfusiDarahRepository(req dto.ReqOnChangedTransfusiDarah) (res rme.DTransfusiDarah, err error)
	OnSaveDAssesmenNyeriRepository(kdBagian string, userID string, req dto.ReqSaveAsesmenNyeri) (res rme.DasesmenUlangNyeri, err error)
	OnGetDAssemenUalngNyeriRepsoitory(req dto.ReqNoReg) (res []rme.DasesmenUlangNyeriV2, err error)
	OnDeleteAsesmenUlangNyeri(req dto.ReqIDUlangNyeri) (res rme.DasesmenUlangNyeri, err error)
	// DATA EDUKASI
	// GetReaksiTransfusiDarahRepository(noreg string, reaksi string) (res rme.DReaksiTransfusiDarah, err error)
	OnDeleteReaksiTransfusiDarahRepositoru(idReaksi int) (err error)
	OnGetDTransfusiDarahRepository(req dto.ReqOnGetTransfusiDarah, kdBagian string) (res []rme.DTransfusiDarah, err error)
	OnSaveReaksiTransufiDarahRepository(userID string, req dto.ReqReaksiTranfusiDarah) (res rme.DReaksiTransfusiDarah, err error)
	GetReaksiTransfusiDarahRepository(noreg string, reaksi string) (res rme.DReaksiTransfusiDarah, err error)
	InsertDtransfusiDarahRepository(req dto.ReqTransfusiDarah, kdbagian string, userID string) (res rme.DTransfusiDarah, err error)
	GetDataAsuhanKeperawatanRepositoryV4(noReg string, kdBagian string) (res []rme.DasKepDiagnosaModelV2, err error)
	OnDeleteEdukasiTerintegrasi(ID int) (res rme.DEdukasiTerintegrasi, err error)
	OnSaveDataEdukasiTerintegrasiRepository(userID string, kdBagian string, req dto.ReqOnSaveDataEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, err error)
	OnGetEdukasiTerintegrasi(noRm string) (res []rme.DEdukasiTerintegrasi, err error)

	GetHistoryRiwayatPenyakitRepository(noreg string) (res []rme.HistoryPenyakit, err error)
	// ===============
	OnUpdatePemeriksaanFisikAntonioDokterRepository(data rme.PemeriksaanFisikDokterAntonio, bagian string, person string, pelayanan string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)
	OnSavePemeriksaanFisikRepository(data rme.PemeriksaanFisikDokterAntonio) (res rme.PemeriksaanFisikDokterAntonio, err error)
	OnGetPemeriksaanFisikDokterRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)
	OnGetPemeriksaanFisikRawatInapRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	//======================= REPORT ASESMEN KEPERAWATAN
	OnGetReporAsesmenAwalKeperawatanIGDAntonioRepository(noReg string) (res rme.PengkajianPersistemIGD, errr error)
	// SKALA NYERI BIDAN
	OnGetSkalaNyeriBidanRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error)

	// ON DEELTE IMPLEMENTASI
	OnDeleteImplementasiTindakanRepository(idTindakan int) (err error)

	OnUpdatePengkajianPersistemIGDRepository(noReg string, kdbagian string, update rme.PengkajianPersistemIGD) (res rme.PengkajianPersistemIGD, err error)
	// TO MAPPING PENGKAJIAN PERSISTEM IGD
	OnSavePengkajianPersistemIGDRepository(kdBagian string, person string, userID string, req dto.ReqSaveAsesmenIGD) (res rme.PengkajianPersistemIGD, err error)

	// ============= REKEM MEDIS
	OnGetPengkajianAwalIGDRepository(noReg string, kdBagian string, person string) (res rme.PengkajianPersistemIGD, errr error)
	// HISTORY RME
	OnGetPemeriksaanFisikAnakRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	GetImplementasiTindakanRepository(noDaskep string) (res []rme.DImplementasiTindakanKeperawatan, err error)
	GetDataAsuhanKeperawatanRepositoryV3(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModelV3, err error)
	OnGeRiwayatKeluhanUtamaRepository(noRM string) (res []rme.RiwayatKeluhanModel, err error)
	InsertDpelaksanaKeperawatan(req dto.ReqOnSaveActionCpptFiberHandler, kdbagian string, userID string) (res rme.DPelaksanaKeperawatan, err error)
	UpdateDPelaksanaanKeperawatanRepository(req dto.ReqOnUpdateActionCpptFiberHandler) (res rme.DPelaksanaKeperawatan, err error)
	SaveDImplementasiKeperawatanRepository(req dto.OnSaveImplementasiKeperawatan, userID string, kdBagian string) (res rme.DImplementasiTindakanKeperawatan, err error)

	OnUpdateKartuCairanRepository(idKartu int, data rme.DKartuCairanObatObatan) (res rme.DKartuCairanObatObatan, err error)
	OnUpdateKartuObservasiRepository(idKartu int, data rme.DKartuObservasi) (res rme.DKartuObservasi, err error)
	OnDeleteKartuObservasiRepository(kodeObservasi int) (res rme.DKartuObservasi, err error)
	OnDeleteCairanRepository(kodeCairan int) (res rme.DKartuCairanObatObatan, err error)
	OnGetKartuCairanRepository(noReg string) (res []rme.DKartuCairanObatObatan, err error)
	OnSaveKartuCairanRepository(userID string, kdBagian string, req dto.OnSaveKartuCairan) (res rme.DKartuCairanObatObatan, message string, err error)
	OnSaveKartuObservasiRepository(userID string, kdBagian string, req dto.ReqOnSaveKartuObservasi) (err error)
	OnGetKartuObservasiRepository(noReg string) (res []rme.DKartuObservasi, err error)
	OnGetPemeriksaanFisikICURepository(noReg string) (res rme.PemeriksaanFisikModel, err error)
	// OnGetPengkajianPersistemICURepository(userID string, kdBagian string, person string, noReg string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)
	OnGetDVitalSignICURepository(noReg string, bagian string, pelayanan string, person string) (res rme.VitalSignDokter, err error)
	OnSavePengkajianPersistemICURepository(req dtoKebidanan.RequestPengkajianPersistemICU, userID string, kdBagian string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error)

	// ================================ INSERT IDENTIAS BAYI
	OnInsertKakiKananBayiRepository(data rme.KakiKananBayi) (res rme.KakiKananBayi, err error)
	OnSaveKakiKananBayiReporitory(data rme.KakiKananBayi) (res rme.KakiKananBayi, err error)
	OnUpdateKakiKananBayiRepository(data rme.KakiKananBayi, noReg string, kdBagian string) (res rme.KakiKananBayi, err error)
	OnGetKakiKananBayiRepository(noReg string, kdBagian string) (res rme.KakiKananBayi, err error)
	//================================= INSERT IDENTITAS BAYI
	OnGetApgarScoreNeoNatusRepository(noReg string) (res []rme.DApgarScoreNeoNatus, err error)
	OnGetAsesmenSkalaNyeriRepository(noReg string) (res rme.AsesmenSkalaNyeri, err error)
	OnDeleteCPPTRepository(nomor int) (err error)
	GetSIKIRepository(kode string) (res rme.SIKIModel, err error)
	GetSDKIRepository(judul string) (res []rme.SDKIModel, err error)
	GetSDKIByIDRepository(ID string) (res rme.SDKIModel, err error)
	GetSLKIRepository(judul string) (res []rme.SLKIModel, err error)
	InsertDaskepRepository(data rme.Daskep) (res rme.Daskep, err error)
	GetRiwayatKehamilanPerinaRepository(noRM string) (res []kebidanan.DRiwayatKehamilan, err error)
	OnGetTindakLanjutPerinaRepository(noReg string, kdBagian string) (res rme.TindakLajutPerina, err error)
	OnSaveTindakLanjutPerinaRepository(data rme.TindakLajutPerina) (res rme.TindakLajutPerina, err error)
	OnUpdateTindakLanjutPerinaRepository(data rme.TindakLajutPerina, noReg string, kdBagian string) (res rme.TindakLajutPerina, err error)

	//====
	// ASESMEN // KEPERAWATAN
	InsertAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqAsesmenKeperawatan) (res rme.AsesmedKeperawatanBidan, err error)
	InsertAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (res rme.AsesmedKeperawatanBidan, err error)
	UpdateAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqAsesmenKeperawatan) (res rme.AsesmedKeperawatanBidan, err error)
	UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (res rme.AsesmedKeperawatanBidan, err error)
	SearchDcpptSoapPasienRepository(noReg string, kdBagian string) (res rme.DcpptSoap, err error)
	GetAsesmenKeperawatanRepository(noReg string, kdBagian string) (res rme.AsesmedKeperawatanBidan, err error)
	GetDasekepRepository(noReg string, kdBagian string) (res []rme.DaskepModel, err error)
	InsertDeskripsiSikiModelRepository(data rme.DeskripsiSikiModel) (res rme.DeskripsiSikiModel, err error)
	OnSaveAlergiKeluargaRepository(alerti string, bagian string, userID string) (meesage string, err error)
	// =====

	// SIKI
	GetSikiByKodeRepository(kode string) (res rme.SIKIModel, err error)
	GetSlkiByKodeRepository(kode string) (res rme.SLKIModel, err error)
	GetSdkiByKodeRepository(kode string) (res rme.SDKIModel, err error)
	GetDaskepResponseRepository(noReg string, kdBagian string) (res []rme.RMEDaskep, err error)

	HapusDaskepRepository(kdBagian string, noreg string) (res rme.Daskep, err error)
	CariDataDaskepRepository(kdBagian string, noreg string) (res rme.Daskep, err error)
	GetAllSIKIRepository() (res []rme.SIKIModel, err error)
	GetAllSDKIRepository() (res []rme.SDKIModel, err error)
	SearchSikiRepository(kodeSiki string) (res []rme.DeskripsiSikiModel, err error)
	UpdateSKDIByKodeRepository(kode string, kodeSDKI string) (res rme.DSlki, err error)
	GetPemeriksaanFisikRepositoryIGD(userID string, ketKerson string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	// Deskripsi siki
	GetDeskripsiSikiRepository(kode string) (res rme.SikiDeskripsiModel, err error)
	CariCPPTRepository(noRm string) (res []rme.CpptModel, err error)
	SearchCPPTPasienRepository(kdBagian string, kelompok string, pelayanan string, noReg string, tanggal string, userId string) (res rme.CPPTPasienModel, err error)
	UpdateCPPTPasienRepository(kdBagian string, kelompok string, pelayanan string, noReg string, tanggal string, data dto.ReqInsertDcpptPasien) (res rme.CPPTPasienModel, err error)
	InsertCPPTPasienRepository(data rme.CPPTPasienModel) (res rme.CPPTPasienModel, err error)

	// ============================ PEMERIKSAAN FISIK
	CariPemeriksaanFisikByDateRepository(userID string, ketPerson string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	InsertPemeriksaanFisikBangsalRepository(userID string, kdBagian string, req dto.ReqPemeriksaanFisikBangsal) (res rme.PemeriksaanFisikModel, err error)
	UpdatePemeriksaanFisikBangsalRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikBangsal) (res rme.PemeriksaanFisikModel, err error)
	InsertPemeriksaanFisikRepository(userID string, kdBagian string, req dto.ReqPemeriksaanFisikIGD) (res rme.PemeriksaanFisikModel, err error)
	UpdatePemeriksaanFisikRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikIGD) (res rme.PemeriksaanFisikModel, err error)

	// ============================== VITAL SIGN REPOSITORY ============================== //
	// ============================== VITAL SIGN REPOSITORY ============================== //
	UpdateVitalSignBangsalRepository(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (res rme.DVitalSign, err error)
	InsertVitalSignBangsalRepository(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (res rme.DVitalSign, err error)
	UpdateTandaVitalGangguanPerilakuRepository(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (res rme.PemeriksaanFisikModel, err error)
	SavePemeriksaanFisikRepository(userID string, kdBagian string, data rme.PemeriksaanFisikModel) (res rme.PemeriksaanFisikModel, err error)
	SaveSkalaTriaseRepository(data rme.SkalaTriaseModel) (res rme.SkalaTriaseModel, err error)
	UpdateSkalaTriaseRepository(req dto.ReqSkalaNyeriTriaseeIGD, userID string, kdBagian string, noReg string) (res rme.SkalaTriaseModel, err error)
	GetSkalaTriaseRepository(noReg string, kdBagian string) (res dto.ResSkalaNyeriTriaseIGD, err error)
	CariDVitalSignRepository(userID string, kdBagian string, noReg string) (res rme.DVitalSign, err error)
	UpdatePemeriksaanFisikAnakRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikAnak) (res rme.PemeriksaanFisikModel, err error)

	// INTERVENSI //
	InsertIntervensiRisikoRespository(data rme.DIntervensiRisiko) (res rme.DIntervensiRisiko, err error)
	SearchIntervensiRisikoRepository(kdBagian string, noReg string, shift string) (res rme.DIntervensiRisiko, err error)
	UpdateIntervensiRisikoRepository(kdBagian string, req dto.ReqIntervensiResikoJatuh) (res rme.DIntervensiRisiko, err error)

	CariResikoJatuhPasienRepository(kdBagian string, noReg string, kategori string) (res rme.DRisikoJatuh, err error)
	InsertResikoJatuhPasienRepository(data rme.DRisikoJatuh) (res rme.DRisikoJatuh, err error)
	UpdateResikoJatuhPasienAnakRepositori(kdBagian string, noReg string, usia string, jenisKelamin string, diagnosis string, gangguan string, faktorLingkungan string, response string, penggunaanObat string, total int) (res rme.DRisikoJatuh, err error)
	// ========= //
	UpdateResikoJatuhPasienMorseRepositori(kdBagian string, noReg string, rJatuh string, diagnosis string, ambulasi string, terapi string, infuse string, gayaBerjalan string, mental string, total int, kategori string) (res rme.DRisikoJatuh, err error)
	UpdateResikoJatuhPasienDewasaRepository(kdBagian string, noReg string, kategori string, usia string, rJatuh string, aktivitas string, mobilisasi string, kognitif string, sensori string, pengobatan string, komorbiditas string, total int) (res rme.DRisikoJatuh, err error)
	GetHasilResikoJatuhRepository(noReg string) (res rme.HasilResikoJatuhPasien, err error)

	// INSERT DATA INTERVENSI
	InsertDataIntevensiRespository(data rme.DIntervensi) (res rme.DIntervensi, err error)
	GetALLSLKIRepository() (res []rme.SLKIModel, err error)
	InsertHasilSLKIRepository(data rme.DKriteriaHasilSLKI) (res rme.DKriteriaHasilSLKI, err error)
	InsertDataSLKIRespository(data rme.DSlki) (res rme.DSlki, err error)
	SearchDataSLKIRepository(kode string) (res rme.DSlki, err error)
	GetDeskripsiSLKIRespository(kodeSLKI string) (res []rme.DKriteriaSLKIModel, err error)

	//
	GetDataDSLKIRepository(kodeSLKI string) (res rme.DSlki, err error)
	OnSaveSkalaNyeriKeperawatanRepository(data rme.AsesmenSkalaNyeri) (res rme.AsesmenSkalaNyeri, err error)
	OnUpdateSkalaNyeriKeperawatanRepository(data rme.AsesmenSkalaNyeri, noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error)

	// DIAGNOSA DASKEP
	GetNomorDaskepDiagnosaRepository() (res rme.NoDaskep, err error)
	CariNomorDaskepDiagnosaRepository(noDaskep string) (res rme.DaskepDiagnosaModel, err error)
	SaveDataDaskepDiagnosaRespository(data rme.DaskepDiagnosaModel) (res rme.DaskepDiagnosaModel, err error)
	SaveDataDaskepSLKI(data rme.DaskepSLKIModel) (res rme.DaskepSLKIModel, err error)
	SaveDataDaskepSIKI(data rme.DaskepSikiModel) (res rme.DaskepSikiModel, err error)
	GetDataAsuhanKeperawatanRepository(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModel, err error)
	GetDataAsuhanKeperawatanRepositoryV2(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModelV2, err error)

	// UPDATE DATA HASIL INTERVENSI ASUHAN KEPERAWATAN
	UpdateDaskepSLKIRepository(hasil int, noDaskep string, kodeSLKI string, idKriteria int) (res rme.DaskepSLKIModel, err error)
	UpdateDasekepDiagnosaRepository(noDaskep string, noReg string) (res rme.DaskepSLKIModel, err error)

	// DATA ALERGI
	InsertDataAlergiRepository(data rme.DAlergi) (res rme.DAlergi, err error)
	GetPenyakitTerdahuluPerawatRepository(noRM string) (res rme.RiwayatPenyakitDahulu, err error)
	GetRiwayatAlergiRepository(noRM string) (res []rme.DAlergi, err error)
	GetDataIntervensiIntervensiResikoJatuhReporitory(kdBagian string, noReg string, shift string) (res rme.DIntervensiRisiko, err error)

	OnDeleteAlergiRepository(nomor int) (err error)

	//
	OnSaveRiwayatKeluargaRepository(noRm string, insertUser string, kdBagian string, alergi string) (res rme.DAlergi, err error)
	// GET ALERGI
	GetRiwayatAlergiKeluargaRepository(noRM string) (res []rme.DAlergi, err error)
	OnGetRiwayatAlergiPasienRepository(noRM string, kdBagian string) (res []rme.DAlergi, err error)
	CariPemeriksaanFisikByNoregKdBagianRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	GetBHPDializerRepository(noReg string) (res rme.DBhpDializerModel, err error)
	UpdateCPPTByCodeRepository(idCppt int, data dto.ReqCPPTUpdate) (res rme.CPPTPasienModel, err error)
	GetDVitalSignDokterRepository(noReg string, bagian string, pelayanan string, person string) (res rme.VitalSignDokter, err error)
	GetDemFisikDokterRepository(noReg string, person string, kdBagian string, pelayanan string) (res rme.DemfisikDokter, err error)

	// ==== //
	InserDemFisikDokterRepository(data rme.DemfisikDokter) (res rme.DemfisikDokter, err error)
	InsertDVitalSignDokterRepository(data rme.VitalSignDokter) (res rme.VitalSignDokter, err error)
	UpdateDVitalSignDokterRepository(ketPerson string, pelayanan string, kdBagian string, noReg string, update rme.VitalSignDokter) (res rme.VitalSignDokter, err error)
	UpdateDemFisikRepository(ketPerson string, pelayanan string, kdBagian string, noReg string, update rme.DemfisikDokter) (res rme.DemfisikDokter, err error)

	// keluhan utama
	OnGetRiwayatPenyakitDahulu(noRM string, tgl string) (res []rme.KeluhanUtama, erre error)
	GetAsesemenKeluhanUtamaIGDDokter(bagian string, noreg string, person string) (res rme.AsesemenDokterIGD, err error)

	OnSaveKeluhanUtamaIGDRepository(data rme.AsesemenDokterIGD) (res rme.AsesemenDokterIGD, err error)
	OnUpdateKeluhanUtamaIGDRepository(bagian string, req dto.ReqKeluhanUtamaIGD) (res rme.AsesemenDokterIGD, err error)
	GetPemeriksaanFisikIGDRepository(userID string, person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	SavePemeriksaanFisikIGDRepository(data rme.PemeriksaanFisikModel) (res rme.PemeriksaanFisikModel, err error)
	OnUpdatePemeriksaanFisikIGDRepository(data rme.PemeriksaanFisikModel, pelayanan string, person string, bagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	// ========================== REPORT IGD =====================
	GetAsesmenAwalMedisIGDDokter(noreg string) (res rme.AsesemenMedisIGD, err error)
	GetAsesmenAwalMedisIGDReportDokter(noreg string) (res rme.AsesemenMedisIGD, err error)
	GetPemeriksaanFisikReportIGDRepository(noReg string) (res rme.PemeriksaanFisikModel, err error)

	// ======================= SBAR CPPT
	OnGetDCPPTSBARRepository(noReg string) (res []rme.CPPTSBAR, err error)
	OnSaveDCPPTSBARRepository(data rme.DataCPPTSBAR) (res rme.DataCPPTSBAR, err error)
	OnUpdateCPPTSBARRepository(idCPpt int, update rme.DataCPPTSBAR) (res rme.DataCPPTSBAR, err error)
	OnDeteleCPTTSBARRepository(idCPPT int) (res rme.DataCPPTSBAR, err error)

	// SIMPAN RIWAYAT ALERGI KELUARGA
	OnSaveRiwayatPenyakitKeluargaRepository(alergi rme.DAlergi) (res rme.DAlergi, err error)
	OnGetRiwayatPenyakitDahuluPerawat(noRM string, tgl string) (res []rme.RiwayatPenyakitDahuluPerawat, erre error)

	// DELETE ASUHAN KEPERAWATAN
	OnDeleteDaskepSikiRepository(noDaskep string) (err error)
	OnDeleteDaskepSLKIRepository(noDaskep string) (err error)
	OnDeleteDaskepDiagnosaRepository(noDaskep string) (err error)
	//
	OnGetPemeriksaanFisikBangsalRepository(userID string, person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	// ASESMEN KEPERAWATAN BAYI
	OnGetDokterAnakDanObgynRepository() (res []rme.DokterBayi, err error)
	OnGetRiwayatKelahiranYangLaluRepository(noRM string) (res []rme.RiwayatKehamilanPerina, err error)

	// ==== VITAL SIGN PERINA
	GetDVitalSignRepository(noReg string, bagian string, pelayanan string, person string) (res rme.DVitalSignModel, err error)
	InsertTandaVitalPerinaRepository(data rme.DVitalSignModel) (res rme.DVitalSignModel, err error)
	UpdateTandaVitalPerinaRepository(kdBagian string, pelayanan string, noReg string, req dto.ReqDVitalSignPerina) (res rme.DVitalSignModel, err error)
	OnGetDownScroeNeoNatusRepository(noReg string) (res rme.DownScoreNeoNatusModel, err error)
	OnUpdateScoreNeoNatusRepository(data rme.DownScoreNeoNatusModel, noReg string) (res rme.DownScoreNeoNatusModel, err error)

	OnSaveScroeNeoNatusRepository(data rme.DownScoreNeoNatusModel) (res rme.DownScoreNeoNatusModel, err error)

	// APGAR SCORE NEONATUS
	OnCheckApgarScoreRepository(noReg string, waktu string) (res rme.DApgarScoreNeoNatus, err error)
	OnGetAsesmenKeperawatanBayiRepository(noReg string, kdBagian string) (res rme.AsesmenKeperawatanBayi, err error)
	OnInsertApgarScoreRepository(data rme.DApgarScoreNeoNatus) (res rme.DApgarScoreNeoNatus, err error)
	OnUpdateApgarSocreRepository(req dto.ReqSaveApgarScore, noReg string) (res dto.ApgarScoreResponse, err error)
	OnSaveRiwayatKehamilanLaluPerinaRepository(userID string, kdBagian string, req dto.ReqRiwayatKehamilanPerina) (res rme.RiwayatKehamilanPerina, message string, err error)
	OnDeleteRiwayatKehamilanYangLaluRepository(kdRiwayat string) (res rme.RiwayatKehamilanPerina, message string, err error)
	OnSaveDataAsesmenBayiRepository(data rme.AsesmenKeperawatanBayi) (res rme.AsesmenKeperawatanBayi, err error)
	OnUpdateDataAsesmenBayiRepository(data rme.AsesmenKeperawatanBayi, noReg string, kdBagian string) (res rme.AsesmenKeperawatanBayi, err error)
	OnGetAllDiagnosaKeperawatanRepository() (res []rme.DiagnosaKeperawatan, err error)
	GetNomorAnalisaDataRepository() (res rme.KodeAnalisa, err error)
	SaveAnalisaDataRepository(data rme.DAnalisaData) (res rme.DAnalisaData, err error)
	SaveDAnalisaProblemRepository(data rme.DAnalisaProblem) (res rme.DAnalisaProblem, err error)
	GetAnalisaDataRepository(noReg string) (res []rme.DAnalisaDataModel, err error)
	OnUpdateAnalisaDataRepository(kodeAnalisa string, update rme.DAnalisaDataModel) (res rme.DAnalisaDataModel, err error)
	OnDeleteAnalisaDataRepository(kodeAnalisa string) (message string, err error)

	// SKALA NYERI ANAK DAN DEWASA ================
	OnGetSkalaNyeriKeperawatanDewasaRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error)
	OnGetSkalaNyeriKeperawatanAnakRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error)
	// ================== END SKALA NYERI

	GetDVitalSignPerinaRepository(noReg string) (res rme.DVitalSignModel, err error)
	OnGetPemeriksaanFisikBangsalPerinaRepository(noReg string) (res rme.PemeriksaanFisikModel, err error)

	// DEWARNING EARNING SYSTEM
	OnInsertDewarningSystemRepository(data rme.DearlyWarningSystem) (res rme.DearlyWarningSystem, err error)
	OnDeleteDEWarningSystemRepository(ID int) (res rme.DearlyWarningSystem, err error)
	OnGetEaningWaningSystemRepository(noReg string) (res []rme.DearlyWarningSystem, err error)
	OnSaveCatatanKeperawatanRepository(data rme.DCatatanKeperawatan) (res rme.DCatatanKeperawatan, err error)
	OnDeleteCatatanKeperawatan(ID int) (res rme.DCatatanKeperawatan, err error)
	OnUpdateCatatanKeperawatanRepository(data rme.DCatatanKeperawatan, ID int, kdBagian string) (res rme.DCatatanKeperawatan, err error)
	OnGetCatatanKeperawatanRepository(noReg string) (res []rme.DCatatanKeperawatanModel, err error)

	//PEMERIKSAAN FISIK
	GetPemeriksaanFisikIGDPerawatRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	// VITAL SIGN
	UpdateVitalSignICURepository(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (res rme.DVitalSign, err error)
	InsertVitalSignICURepository(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (res rme.DVitalSign, err error)
	OnDeleteTransfusiDarahRepositoru(ID int) (err error)

	// ============ //
	OnGetReaksiByIDTransfusiDarahRepository(ID int) (res []rme.DReaksiTransfusiDarah, err error)
	OnViewReaksiDarahByIDTranfusiDarah(ID int) (res []rme.DReaksiTransfusiModelDarah, err error)
	OnGetDataKaruRepository(kdBagian string) (res rme.MutiaraPengajar, err error)
	OnGetDataPerawatRepository(idPegawai string) (res rme.MutiaraPengajar, err error)

	// PEMBERIAN CARIAN INFUSE
	OnSavePemberianTerapiCairanRepository(data rme.DPemberianCairan) (res rme.DPemberianCairan, err error)
	OnGetPemberianTerapiCairanRepository(noReg string) (res []rme.DPemberianCairan, err error)
	OnSaveDCairanIntakeRepository(data rme.CairanIntake) (res rme.CairanIntake, err error)

	//====//
	OnSaveDCairanOutPutRepository(data rme.DcairanOutPut) (res rme.DcairanOutPut, err error)
	OnGetDCairanInTakeRepository(noReg string) (res []rme.CairanIntake, err error)
	OnGetCairanOutPutRepository(noReg string) (res []rme.DcairanOutPut, err error)

	//===============
	OnDeleteCairanIntakeRepository(ID int) (res rme.CairanIntake, err error)
	OnDeleteOutPutRepository(ID int) (res rme.DcairanOutPut, err error)

	OnGetPengkajianDPemFIsikRepository(Kategori string, Person string, KDBagian string, Pelayanan string) (res rme.DPemFisikModel, err error)
	OnSaveDPemeriksaanFisikReporitory(data rme.DPemFisikModel) (res rme.DPemFisikModel, err error)
}

// Mapper REPOSITORY
type RMEMapper interface {
	ToMappingResponsePengkajianRANAP(sistem igd.DpengkajianPersistem) (res igd.ResponseDPengkajianPersistenRANAP)
	ToResponsePengkajianRANAP(pengkajian igd.PengkajianKeperawatan) (res soapDTO.ResponsePengkajianRawatInap)
	ToMappingPengkajianPersistemRanap(pemFisik rme.DPemFisikModel, persistem igd.DpengkajianPersistem) (res dto.PenkajianPersistemRANAP)
	ToMappingResponseDCairanPut(data []rme.DcairanOutPut) (res []dto.ResponseDcairanOutPut)
	ToMappingResponseDCairanOutPut(data []rme.CairanIntake) (res []dto.ResponseCairanIntake)
	ToMappingDataPemberianCairan(user user.ProfilePasien, cairan []rme.DPemberianCairan) (res dto.ReponseTerapiCairan)
	ToPemeriksaanFisikAntonioResponse(data rme.PemeriksaanFisikDokterAntonio) (res dto.ResponsePemerikssanFisikAntonio)
	ToMappingPengkajianAwalKeperawatanIGDAntonio(persistem rme.PengkajianPersistemIGD, nyeri soapDTO.ResSkriningNyeriIGD, asuhan []rme.DasKepDiagnosaModelV2, pemFisik dto.ResponsePemeriksaanFisik, asesmenIGD soap.AsesemenIGD, karyawan user.Karyawan) (res dto.ResponsePengkajianIGDKeperawatanAntonio)
	// =================
	ToMappingPengkajianPersistemIGD(data rme.PengkajianPersistemIGD, vital soapDTO.TandaVitalIGDResponse) (res dto.ResponseAsesmenIGD)
	// ======RME MAPPER
	ToResponseAsesmenAwalIGD(riwayat []soap.RiwayatPenyakit) (res dto.ResponseAsesmenAwalIGD)
	ToResponseCairanKartuMapper(data []rme.DKartuCairanObatObatan) (res []dto.ResponseCairanKartu)
	TOResponseDKartuObservasi(data []rme.DKartuObservasi) (res []dto.ResponseDKartuObservasi)
	ToNyeriICUResponseMapper(data rme.PengkajianNyeri) (res dto.AsesmenNyeiICU)
	ToResponsePemeriksaanFisikAnakMapper(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisikAnak)
	// VITAL SIGN ICU RESPONSE
	ToMapperVitalSignICUResponse(data rme.VitalSignDokter) (res dto.ResponseVitalSignICU)

	ToResponsePemeriksaanFisikICUMapper(data rme.PemeriksaanFisikModel) (res dto.ResponseFisikICU)
	ToMapperEarlyWarningSystemResponse(data []rme.DearlyWarningSystem) (res []dto.ResponseEarlyWarningSystem)
	ToMapperResponseRingkasanMasukPasien(pasien report.DProfilePasien) (res dto.ResponseRingkasanMasukPasien)
	ToMapperIdentitasBayiMapper(data dto.ResponseIdentitasBayi, profil report.DProfilePasien, vitalSign rme.DVitalSignModel) (res dto.IdentitasBayiRes)
	ToResponseTindakLajutMapper(data rme.TindakLajutPerina) (res dto.ResponseTindakLajutPerina)
	TOMapperResponseResumeMedisPerinatologi(data rme.AsesmenKeperawatanBayi, profil report.DProfilePasien, noReg string) (res dto.ResponseResumeMedisPerinatologi)
	ToMappingAsesmenNyeriResponse(userData user.Karyawan, nyeri dto.SkalaNyeriResponse) (res dto.ResponseSkalaNyeri)

	// SKALA NYERI DEWASA
	ToMapperSkalaNyeriDewasa(data rme.AsesmenSkalaNyeri) (response dto.SkalaNyeriResponse)
	ToResponseRiwayatKeperawatanBayi(data rme.AsesmenKeperawatanBayi, profil report.DProfilePasien) (res dto.ResponseAsesmenKeperawatanBayi)
	ToMapperResponseRiwayatKelahiranLalu(data []rme.RiwayatKehamilanPerina) (res []dto.ResponseRiwayatKelahiranLalu)
	ToMapperScoreNeoNatus(data []rme.DApgarScoreNeoNatus) (value []dto.ApgarScoreResponse)
	ToMapperSingleScoreNeoNatus(data rme.DApgarScoreNeoNatus) (value dto.ApgarScoreResponse)
	ToMapperResponseNeonatus(data rme.DownScoreNeoNatusModel) (value dto.NeoNatusresponse)

	// ==== MAPING DATA
	ToMapperResponseDVitalSignPerina(data rme.DVitalSignModel) (res dto.ResponseDVitalSignPerina)
	ToDiagnosaResponse(data []rme.SDKIModel) (value []dto.DiagnosaKeperawatanResponse)
	ToResponseSLKI(data rme.SLKIModel) (value dto.ResponseIntervensi)
	ToResponseReportAsesmenBayiMapper(data rme.AsesmenKeperawatanBayi, kelahiran []dto.ResponseRiwayatKelahiranLalu, apgar []dto.ApgarScoreResponse, downScore dto.NeoNatusresponse, vitalSign dto.ResponseDVitalSignPerina, fisik dto.ResponseFisikPerina, user user.MutiaraPengajar) (res dto.ResponseReportAsesmenBayiResponse)
	ToResponseAsesmenKebidanan(data rme.AsesmedKeperawatanBidan, value []rme.RMEDaskep, sdki rme.SDKIModel, siki rme.SIKIModel) (res rme.GetResponseAsesmen)

	// ============== MAPPER ASUHAN // ============== KEPERAWATAN
	ToResponseAsuhanKeperawatan(value []rme.RMEDaskep, sdki rme.SDKIModel, siki rme.SIKIModel) (res rme.GetResponseAsuhanKeperawatan)
	ToCPPTResponse(data []rme.CpptModel) (value []dto.CPPTResponse)
	ToResponsePemeriksaanFisikIGD(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanFisikGID)
	ToResponsePemeriksaanFisikBangsal(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanFisikBangsal)
	ToResponseVitalSignBangsal(data rme.PemeriksaanFisikModel) (value dto.ResVitalSignbangsal)
	ToResponseTriaseTandaVitalIGD(data rme.PemeriksaanFisikModel) (value dto.ResTriaseTandaVitalIGD)
	ToSkalaTriaseResponse(data soap.DcpptSoapDokter) (res dto.ResSkalaNyeriTriaseIGD)
	ToSkalaTriaseResponseFromModelTriase(data rme.SkalaTriaseModel) (res dto.ResSkalaNyeriTriaseIGD)
	ToResponsePemeriksaanFisikAnak(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanAnak)

	// TINDAKAN  HASIL RESIKO
	ToHasilResikoJatuhPasienMapper(data rme.HasilResikoJatuhPasien) (res dto.ResikoJatuhResponse)
	ToMapperIdentitasBayi(data rme.KakiKananBayi) (res dto.ResponseIdentitasBayi)

	ToResponseSikiDeskripsiMapper(data []rme.SikiDeskripsiModel) (res []dto.ResponseDeskripsiSiki)
	ToMapperDeskripsiSiki(data []rme.DeskripsiSikiModel) (res []dto.SikiDeskripsi)
	ToSDKIResponseMapper(skdi rme.SDKIModel, newSLKI []dto.ResponseDataSLKI, siki []dto.ResponseDeskripsiSiki) (res dto.SikiResponse)
	ToMapperResponseKriteriaModel(data []rme.DKriteriaSLKIModel) (res []dto.ResponseKriteriaModel)
	ToMappingAlergi(dahulu rme.RiwayatPenyakitDahulu, alergi []rme.DAlergi) (res dto.ResponseAlergi)
	ToReponseSkalaNyeri(data rme.PemeriksaanFisikModel) (res dto.ResSkalaNyeriTriaseIGD)

	// ===
	ToResponseTriaseVitalSignIGD(data1 rme.VitalSignDokter, demfisik rme.DemfisikDokter) (res dto.ResTriaseTandaVitalIGD)
	ToMappingKeluhanUtama(riwayatDahulu []rme.KeluhanUtama, alergi []rme.DAlergi, keluh rme.AsesemenDokterIGD, pengkajianKeperawatan soap.PengkajianAwalKeperawatan) (res dto.ResposeKeluhanUtamaIGD)
	ToMappingKeluhanUtamaV2(riwayatDahulu []rme.KeluhanUtama, alergi []rme.DAlergi, keluh rme.AsesemenDokterIGD) (res dto.ResposeKeluhanUtamaIGD)

	ToPemeriksaanFisikIGDResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGD)
	ToPemeriksaanFisikIGDMethodisResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGDMethodist)
	ToPemeriksaanFisikIGDAntonioResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGDMethodist)

	// =============== MAPPER REPORT
	ToMapperReportIGDResponse(data1 rme.AsesemenMedisIGD, riwayatPenyakit []rme.KeluhanUtama, alergi []rme.DAlergi, tindakLanjut soap.RencanaTindakLanjutModel, diagnosa []soap.DiagnosaResponse, fisik dto.ResponseIGD, vital soap.DVitalSignIGDDokter, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, userMapper user.ProfilePasien, diagnosaBanding []diagnosa.DDiagnosaBanding) (res dto.ReportAsesmenIGD)
	ToMapperAsuhanKeperawatanResponse(daskep []rme.DasKepDiagnosaModel, perawat user.UserPerawat) (res dto.ResponseDaskepPerawat)
	ToMapperPemeriksaanFisikReponse(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisik)

	// ================ MAPPING DATA KEPERWATAN BAYI
	ToMappingKeperawatanBayiResponse(dokter []rme.DokterBayi, riwayat []rme.RiwayatKehamilanPerina, asesmen rme.AsesmenKeperawatanBayi, profil report.DProfilePasien) (res dto.ResponseAsesmenBayiResponse)
	ToPemeriksaanFisikPerinaResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseFisikPerina)
	ToMappingPengkajianKeperawatan(pengkajian igd.PengkajianKeperawatan, riwayat []kebidanan.DRiwayatPengobatanDirumah, karu rme.MutiaraPengajar, perawat rme.MutiaraPengajar, mapperNutrisi dtoNyeri.ToResponsePengkajianNutrisiDewasa, asuhan []rme.DasKepDiagnosaModelV2, fisik dto.ResponsePemeriksaanFisik, vital soap.DVitalSignIGDDokter, funsional kebidanan.DPengkajianFungsional, nyeri nyeri.DasesmenUlangNyeri, resikoJatuh igd.DRisikoJatuh, persistem igd.DpengkajianPersistem, dpemFisik rme.DPemFisikModel, nyeris dtoIGD.ResponsePengkajianNyeriIGD) (res dto.ResponsePengkajianKeperawatan)
}

type RMEUseCase interface {
	OnSavePengkajianPersistemRANAPUseCase(req dto.OnSavePengkajianPersistemBangsal) (message string, err error)
	OnGetPengkajianPersistemRANAPUseCase(req dto.ReqOnGetPengkajianPersistem) (res dto.PenkajianPersistemRANAP)
	OnDeleteCairanOutPutUseCase(ID int) (message string, err error)
	OnDeleteDCairanIntakeUseCase(ID int) (message string, err error)
	OnGetMonitoringCairanUseCase(NoReg string) (res dto.ResponseMonitoringCairan, message string, err error)
	OnGetCairanOutPutUseCase(NoReg string) (res []dto.ResponseDcairanOutPut, message string, err error)
	OnGetCairanIntakeUseCase(noReg string) (res []dto.ResponseCairanIntake, message string, err error)
	OnSaveDCarianOutPutUseCase(req dto.ReqOnSaveCiaranOutPut, userID string, kdBagian string) (message string, err error)
	OnSaveDCairanIntakeUseCase(req dto.ReqOnSaveDCairanIntake, userID string, kdBagian string) (message string, err error)

	//
	OnGetReportPemberianTerapiCairanUseCase(noReg string, noRM string) (res dto.ReponseTerapiCairan, message string, err error)
	OnSavePemberianTerapiCairanUseCase(req dto.ReqOnSavePemberianTerapiCairan, userID string, kdBagian string) (message string, err error)
	OnGetReportPengkajianDewasaBangsalUseCase(req dto.ResPengkajianKeperawatan) (res dto.ResponsePengkajianKeperawatan, err error)
	OnVerifyTransfusiDarahUseCase(req dto.VerifyTransfusiDarah, kdBagian string, userID string) (messasge string, err error)
	OnUpdateTransfusiDarahUsecase(req dto.ReqOnChangedTransfusiDarah) (res rme.DTransfusiDarah, message string, err error)
	OnSaveNyeriUseCase(req dto.ReqSaveAsesmenNyeri, kdBagian string, userID string) (res rme.DasesmenUlangNyeri, message string, err error)
	OnDeleteTransfusiDarahUseCase(req dto.ReqDeleteTransfusiDarah) (message string, err error)
	OnDeleteReaksiTransfusiDarahUseCase(req dto.ReqDeleteReaksiTranfusiDarah) (message string, err error)
	OnSaveReaksiTransfusiDarahUseCase(userID string, req dto.ReqReaksiTranfusiDarah) (message string, err error)
	OnSaveDataEdukasiTerintegrasiUseCase(kdBagian string, userID string, req dto.ReqOnSaveDataEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, message string, err error)
	OnSavePemeriksaanFisikDokterAntonioUseCase(req dto.RegPemeriksaanFisikDokterAntonio, userID string, kdBagian string) (res dto.ResponsePemerikssanFisikAntonio, message string, err error)
	OnGetReportAsesmenDokterIGDAntonioUsecase(req dto.ReqReportAsesmenAwalMedis, bagian string) (res dto.ReportAsesmenIGD, err error)
	OnGetReportAsesmenAwalIGDDokterUseCase(reg dto.ReqReportAsesmenAwalMedis, bagian string) (res dto.ReportAsesmenIGD, err error)
	// OnGetReportAsesmenAwalIGDDokterUseCase(reg dto.ReqReportAsesmenAwalMedis, bagian string) (res dto.ReportAsesmenIGD, err error)
	// ANALISA DATA
	OnValidasiAnalisaDataEnumUseCase(req dto.ReqValidasiAnalisaData, userID string) (message string, err error)
	// END ANALISA DATA
	OnSavePengkajianIGDUseCase(noReg string, kdBagian string, person string, onSave dto.ReqSaveAsesmenIGD, userID string) (message string, err error)
	// === //
	OnSavePemeriksaanFisikAnakUseCase(req dto.ReqOnSavePemeriksaanFisikAnak, kdBagian string, userID string) (res dto.ResponsePemeriksaanFisikAnak, message string, err error)
	SaveVitalSignICUUseCase(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (message string, err error)

	OnSavePemeriksaanFisikICUFiberUsecase(req dto.ReqOnSavePemeriksaanFisikICU, userID string, kdBagian string) (res dto.ResponseFisikICU, mesage string, err error)
	OnSaveIdentitasPerinaUseCase(ttd1 *multipart.FileHeader, ttd2 *multipart.FileHeader, c *fiber.Ctx, noReg string, kdBagian string, kdDPJP string, userID string, namaPenentuJK string, nanaWali string, namPemberiGelang string, jamKelahiran string) (message string, err error)

	OnGetRingkasanMasuDanKeluarUseCase(noRM string) (res dto.ResponseRingkasanMasukPasien, err error)
	OnUploadIndentiasBayiPerinaUseCase(kategori string, noReg string, kdBagian string, files *multipart.FileHeader, c *fiber.Ctx, userID string, kdDpjp string) (res dto.ResponseIdentitasBayi, message string, err error)
	OnSaveTindakLanjutPerinaDokterUseCase(noReg string, kdBagian string, userID string, req dto.ReqSaveTindakLajutPerinatologi) (message string, err error)
	OnGetTindakLanjutPerinaDokterUseCase(noReg string, kdBagian string) (res dto.ResponseTindakLajutPerina, err error)
	OnGetReportResumeMedisPerinaUseCase(noRM string, noReg string) (response dto.ResponseResumeMedisPerinatologi)
	OnSaveSkalaNyeriKeperawatanDewasaUseCase(req dto.ReqAsesmenSkalaNyeri, kdBagian string, userID string) (message string, err error)
	OnValidasiAnalisaDataUseCase(req dto.ReqValidasiAnalisaData, userID string) (message string, ers error)
	OnGetReportAnalisaDataUseCase(noReg string) (res []rme.DAnalisaDataModel, err error)
	OnSaveAnalisaDataUseCase(kdBagian string, userID string, req dto.ReqSaveAnalisaData) (message string, err error)
	OnSaveAsesmenKeperawatanBayiUseCase(noReg string, noRM string, kdBagian string, userID string, req dto.ReqOnSaveAsesemenKeperawatanBayi) (res dto.ResponseAsesmenBayiResponse, message string, err error)
	OnSaveApgarScoreNeoNatus(noReg string, req dto.ReqSaveApgarScore, kdBagian string, userID string) (res dto.ApgarScoreResponse, message string, err error)
	OnSavePemeriksaanFisikIGDDokterMethodistFiberHandler(req dto.ReqPemeriksaanFisikIGDDokterMethodist, userID string, kdBagian string) (res dto.ResponseIGD, mesage string, err error)
	OnGetReportAsesmenKeperawatanPerinaUseCase(noReg string, noRm string) (response dto.ResponseReportAsesmenBayiResponse, err error)

	OnSaveDownScoreNeoNatusUseCase(noReg string, req dto.ReqSaveDownScreNeoNatus, kdBagian string, userID string, person string) (res dto.NeoNatusresponse, message string, err error)
	SaveVitalSignPerinaUseCase(userID string, kdBagian string, req dto.ReqDVitalSignPerina) (res dto.ResponseDVitalSignPerina, message string, err error)
	OnSavePemeriksaanFisikPerinaUseCase(req dto.ReqPemeriksaanFisikPerina, userID string, kdBagian string) (res dto.ResponseFisikPerina, mesage string, err error)
	SaveDataAllSKIKeperawatanUseCase(req dto.ReqDasKepDiagnosaModel) (message string, err error)
	GetIntervensiUseCase(req dto.RegIntervensi) (res []dto.MapperSLKI, err error)
	GetIntervensiUseCaseV2(req dto.RegIntervensiV2) (res []dto.MapperSLKI, err error)
	GetSIKIUseCase(siki string) (res []rme.SIKIModel, err error)
	SaveAsesmenKeperawatanUsecase(kdBagian string, req dto.ReqAsesmenKeperawatan) (message string, err error)
	SaveAsesmenKeperawatanUsecaseV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (message string, err error)
	GetAsesmenKeperawatanBidanUseCase(noReg string, kdBagian string) (res rme.GetResponseAsesmen, message string, err error)
	OnSavePemeriksaanFisikIGDDokterFiberHandler(req dto.ReqPemeriksaanFisikIGDDokter, userID string, kdBagian string) (res dto.ResponseIGD, mesage string, err error)

	// GET ASUHAN KEPERAWATAN
	GetAsuhanKeperawatanBidanUseCase(noReg string, kdBagian string) (res rme.GetResponseAsuhanKeperawatan, message string, err error)
	// SAVE ASUHAN KEPERAWATAN
	SaveAsuhanKeperawatanBidanUseCase(kdBagian string, req dto.RegSaveAsuhanKeperawatan) (message string, err error)
	CariDeskripsiSikiUsecase(siki string) (res []dto.ResponseDeskripsiSiki, err error)
	SaveCPPTPasienUsecase(req dto.ReqInsertDcpptPasien, userId string) (message string, res rme.CPPTPasienModel, err error)
	SavePemeriksaanFisikIGDUsecase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikIGD) (res dto.ResponsePemeriksaanFisikGID, message string, err error)
	SavePemeriksaanFisikBangsalUsecase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikBangsal) (message string, err error)

	// ON SAVE VITAL SIGN

	// VITAL SIGN
	SaveVitalSignBangsalUsecase(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (message string, err error)
	SaveTandaVitalGangguanPerilakuUsecase(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (message string, err error)
	SkalaNyeriTriaseIGDUsecase(userID string, kdBagian string, req dto.ReqSkalaNyeriTriaseeIGD) (message string, err error)

	// ====================
	SavePemeriksaanFisikAnakUseCase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikAnak) (message string, err error)

	SaveIntervensiResikoJatuhUseCase(userID string, kdBagian string, req dto.ReqIntervensiResikoJatuh) (message string, err error)
	SaveResikoJatuhPasienUseCase(userID string, kdBagian string, req dto.ReqResikoJatuhPasien) (message string, err error)
	GetAlertResikoJatuhPasienUseCase(kdBagian string, noReg string) (res dto.ResikoJatuhResponse, err error)
	GetDeskripsiLuaranSDKUseCase(kodeSDKi string) (res dto.SikiResponse, message string, err error)

	// DASKEP
	GetNomorDaskepUsecase() (nomr rme.NoDaskep, err error)
	SaveDeskripsiLuaranSDKIUserCase(kdBagian string, userID string, req dto.SikiRequest) (message string, err error)
	SaveDataSKIKeperawatanUseCase(noDaskep string, kodeSLKI string, idKriteria int, hasil int) (message string, err error)
	OnUpdateDataDaskepToClosedUseCase(noDaskep string, noReg string, modulID string, status string) (res []rme.DasKepDiagnosaModel, err error)
	// ===== //
	OnSaveDataAlergiUseCase(req dto.ReqOnSaveDataAlergiObat, kdBagian string) (res rme.DAlergi, message string, err error)
	OnDeleteDcpptUsecase(idCppt int, noRM string) (res map[string]interface{}, messag string, err error)
	UpdateCPPTPasienUsecase(req dto.ReqCPPTUpdate) (message string, res rme.CPPTPasienModel, err error)

	// ==== //
	GetVitalSignUseCase(noReg string, person string, bagian string, pelayanan string) (res dto.ResTriaseTandaVitalIGD, err error)
	SaveTandaVItalIGDUsecase(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (res dto.ResTriaseTandaVitalIGD, message string, err error)
	OnSaveKeluhanUtamaIGDUsecase(reg dto.ReqKeluhanUtamaIGD, userID string, bagian string) (res dto.ResposeKeluhanUtamaIGD, mesage string, err error)
	OnSaveRiwayatPenyakitKeluargaIGDUsecase(reg dto.ReqRiwayatKeluargaIGD, userID string, bagian string) (res dto.ResposeKeluhanUtamaIGD, mesage string, err error)

	//  ========================== REPORT ASESMEN
	OnGetReportAsesmenAwalMedisKelurgaUseCase(reg dto.ReqReportAsesmenAwalMedis) (res dto.ReportAsesmenIGD, err error)
	OnSaveCPPTSBARUsecase(req dto.ReqSaveCPPTSBar, userID string, kdBagian string) (message string, err error)
	OnUpdateCPTTSBARUsecase(req dto.ReqUpdateeCPPTSBar, userID string, kdBagisn string) (message string, err error)
	// OnUpdateCPPTSBARRepository(idCPpt int, update rme.DataCPPTSBAR) (res rme.DataCPPTSBAR, err error)

	// MAPINNG PEMERIKSAAN FISIK
	OnSavePemeriksaanFisikBangsalUseCase(req dto.ReqPemeriksaanFisik, userID string, kdBagian string) (res dto.ResponseIGD, mesage string, err error)
	OnGetAsesmenKeperawatanBayiUseCase(noReg string, noRM string, kdBagian string) (response dto.ResponseAsesmenBayiResponse, err error)
}
