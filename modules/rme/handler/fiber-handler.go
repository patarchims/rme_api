package handler

import (
	diagnosaRepository "hms_api/modules/diagnosa/entity"
	hisUseCase "hms_api/modules/his/entity"
	reporRepo "hms_api/modules/report/entity"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/modules/rme/entity"
	soapRepository "hms_api/modules/soap/entity"
	soapUsecase "hms_api/modules/soap/entity"
	userRepository "hms_api/modules/user/entity"

	"hms_api/pkg/helper"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

type RMEHandler struct {
	RMERepository      entity.RMERepository
	Logging            *logrus.Logger
	RMEMapper          entity.RMEMapper
	RMEUsecase         entity.RMEUseCase
	SoapUsecase        soapUsecase.SoapUseCase
	SoapRepository     soapRepository.SoapRepository
	UserMapper         userRepository.IUserMapper
	UserRepository     userRepository.UserRepository
	ReporRepository    reporRepo.ReportRepository
	HisUseCase         hisUseCase.HisUsecase
	DiagnosaRepository diagnosaRepository.DiagnosaRepository
}

func (rh *RMEHandler) OnSaveCairanOutPutFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveCiaranOutPut)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, ersave := rh.RMEUsecase.OnSaveDCarianOutPutUseCase(*payload, userID, modulID)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh RMEHandler) OnDeleteCairanOutPutFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteInTake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	message, ersave := rh.RMEUsecase.OnDeleteCairanOutPutUseCase(payload.ID)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetCairanIntakeFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetCairanInTake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("DATA INTAKE")
	rh.Logging.Info(*payload)

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, message, ersave := rh.RMEUsecase.OnGetCairanIntakeUseCase(payload.NoReg)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetMonitoringCairanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetCairanInTake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("DATA INTAKE")
	rh.Logging.Info(*payload)

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, message, ersave := rh.RMEUsecase.OnGetMonitoringCairanUseCase(payload.NoReg)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePengkajianPersistemDewasaRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSavePengkajianPersistemBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	message, er12 := rh.RMEUsecase.OnSavePengkajianPersistemRANAPUseCase(*payload)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnGetPengkajianAwalKeperawatanRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetPengkajianAwalKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// GET DATA PENGKAJIAN RAWAT INAP

	return nil
}

func (rh *RMEHandler) OnGetDataPengkajianPersistemDewasaRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetPengkajianPersistem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// === //
	data := rh.RMEUsecase.OnGetPengkajianPersistemRANAPUseCase(*payload)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetCairanOutPutFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetCairanInTake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, message, ersave := rh.RMEUsecase.OnGetCairanOutPutUseCase(payload.NoReg)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveCairanIntakeFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveDCairanIntake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, ersave := rh.RMEUsecase.OnSaveDCairanIntakeUseCase(*payload, userID, modulID)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteCairanIntakeFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteInTake)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	message, ersave := rh.RMEUsecase.OnDeleteDCairanIntakeUseCase(payload.ID)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePemberianTerapiCairanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSavePemberianTerapiCairan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, ersave := rh.RMEUsecase.OnSavePemberianTerapiCairanUseCase(*payload, userID, modulID)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportPemberianTerapiCairanInfuseFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetReportPemberianTerapiCairan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	data, message, ersave := rh.RMEUsecase.OnGetReportPemberianTerapiCairanUseCase(payload.Noreg, payload.NoRM)

	if ersave != nil {
		response := helper.APIResponse(message, http.StatusCreated, ersave.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetAsuhanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	modulID := c.Locals("modulID").(string)

	data, message, err := rh.RMEUsecase.GetAsuhanKeperawatanBidanUseCase(payload.Noreg, modulID)

	if err != nil {
		response := helper.APIResponse(message, http.StatusCreated, err.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if data.Siki.Judul == "" {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// INSERT DESKRIPSI SIKI
func (rh *RMEHandler) InsertDeskripsiSIKIFiberHandler(c *fiber.Ctx) error {
	// SELECT ALL SIKI

	data, err := rh.RMERepository.GetAllSIKIRepository()

	if err != nil {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if len(data) > 0 {
		for i := 0; i <= len(data)-1; i++ {
			// AMBIL OBSERVASI
			observasi := strings.Split(data[i].Observasi, "\n")

			for _, part := range observasi {
				if part != "" {

					var data1 = strings.Split(string(part[0:3]), ".")

					angka, _ := strconv.Atoi(data1[0])

					var des = strings.Split(string(part), ".")

					rh.Logging.Info("Angka  ")
					rh.Logging.Info(angka)
					// rh.Logging.Info(string(part[0:3]))
					// INSERT OBSERVASI
					var value = rme.DeskripsiSikiModel{
						NoUrut:    angka,
						KodeSiki:  data[i].Kode,
						Deskripsi: des[1],
						Kategori:  "Observasi",
					}
					_, err := rh.RMERepository.InsertDeskripsiSikiModelRepository(value)

					if err != nil {
						rh.Logging.Info("Error Insert " + err.Error())
					}

				}
			}

			// INSERT TERAPEUTIK
			terapeutik := strings.Split(data[i].Terapeutik, "\n")

			for _, parts := range terapeutik {
				if parts != "" {

					var data1 = strings.Split(string(parts[0:3]), ".")
					angka, _ := strconv.Atoi(data1[0])

					var des = strings.Split(string(parts), ".")

					rh.Logging.Info("Angka  ")
					rh.Logging.Info(angka)

					// INSERT OBSERVASI
					var value = rme.DeskripsiSikiModel{
						NoUrut:    angka,
						KodeSiki:  data[i].Kode,
						Deskripsi: des[1],
						Kategori:  "Terapeutik",
					}
					_, err := rh.RMERepository.InsertDeskripsiSikiModelRepository(value)

					if err != nil {
						rh.Logging.Info("Error Insert " + err.Error())
					}
					rh.Logging.Info(angka)
				}
			}

			// INSERT TERAPEUTIK
			edukasi := strings.Split(data[i].Edukasi, "\n")

			for _, parts1 := range edukasi {
				if parts1 != "" {
					// INSERT OBSERVASI
					var data1 = strings.Split(string(parts1[0:3]), ".")
					angka, _ := strconv.Atoi(data1[0])

					var des = strings.Split(string(parts1), ".")

					rh.Logging.Info("Angka  ")
					rh.Logging.Info(angka)

					var value = rme.DeskripsiSikiModel{
						NoUrut:    angka,
						KodeSiki:  data[i].Kode,
						Deskripsi: des[1],
						Kategori:  "Terapeutik",
					}

					_, err := rh.RMERepository.InsertDeskripsiSikiModelRepository(value)

					if err != nil {
						rh.Logging.Info("Error Insert " + err.Error())
					}
					rh.Logging.Info(angka)
				}
			}
			// INSERT KOLABORASI
			kolaborasi := strings.Split(data[i].Kolaborasi, "\n")

			for _, parts2 := range kolaborasi {
				if parts2 != "" {
					// INSERT OBSERVASI
					var data1 = strings.Split(string(parts2[0:3]), ".")

					angka, _ := strconv.Atoi(data1[0])

					var des = strings.Split(string(parts2), ".")

					rh.Logging.Info("Angka  ")
					rh.Logging.Info(angka)

					var value = rme.DeskripsiSikiModel{
						NoUrut:    angka,
						KodeSiki:  data[i].Kode,
						Deskripsi: des[1],
						Kategori:  "Kolaborasi",
					}
					_, err := rh.RMERepository.InsertDeskripsiSikiModelRepository(value)
					if err != nil {
						rh.Logging.Info("Error Insert " + err.Error())
					}
					rh.Logging.Info(angka)
				}
			}
		}
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) DeskripsiSikiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegKodeSiki)
	err := c.BodyParser(&payload)

	if err != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs1 := rh.RMERepository.SearchSikiRepository(payload.KodeSiki)

	if errs1 != nil {
		errors := helper.FormatValidationError(errs1)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ====== //
	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveAsuhanKeperawatanBidanFiberHandler(c *fiber.Ctx) error {
	// LAKUKAN SAVE ASUHAN KEPERAWATAN BIDAN
	payload := new(dto.RegSaveAsuhanKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET A

	modulID := c.Locals("modulID").(string)

	// USECASE SAVE ASUHAN KEPERAWATAN BIDAN
	message, errs := rh.RMEUsecase.SaveAsuhanKeperawatanBidanUseCase(modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(message, http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, payload)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) SaveVitalSignBangsal(c *fiber.Ctx) error {
	payload := new(dto.ReqVitalSignBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, errs := rh.RMEUsecase.SaveVitalSignBangsalUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(message, http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, payload)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// ========================================================= SKALA NYERI
// ====== SKALA TRIASE
func (rh RMEHandler) SaveTriaseSkalaNyeriHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSkalaNyeriTriaseeIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ================================== USECASE SKALA TRIASE IGD ============================= //
	message, errs := rh.RMEUsecase.SkalaNyeriTriaseIGDUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// SIMPAN SKALA TRIASE IGD HANDLER
func (rh RMEHandler) SaveSKalaTriaseIGDNyeriHandler(c *fiber.Ctx) error {
	return nil
}

func (rh RMEHandler) GetTriaseSkalaNyeriHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ================================ //CariDVitalSignRepository(userID string, kdBagian string, noReg string) (res rme.DVitalSign, err error)
	// data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, "", modulID, payload.Noreg)
	data, errs := rh.RMERepository.GetPemeriksaanFisikRepositoryIGD(userID, "", modulID, payload.Noreg)
	rh.Logging.Info("GET SKALA NYERI")
	rh.Logging.Info(data)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(errs.Error(), http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING TO SKALA NYERI
	mapper := rh.RMEMapper.ToReponseSkalaNyeri(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// ========================================================= END SKALA NYERI
func (rh RMEHandler) SaveGangguanPerilaku(c *fiber.Ctx) error {
	payload := new(dto.RegVitalSignGangguanPerilaku)
	errs := c.BodyParser(&payload)

	rh.Logging.Info("AMBIL DATA GANGGUAN PERILAKU")

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)
	rh.Logging.Info("NADI")
	rh.Logging.Info(payload.Nadi)

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	rh.Logging.Info(payload)

	message, errs := rh.RMEUsecase.SaveTandaVitalGangguanPerilakuUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(message, http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, payload)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh RMEHandler) SaveVitalSignDokter(c *fiber.Ctx) error {
	payload := new(dto.RegVitalSignGangguanPerilaku)
	errs := c.BodyParser(&payload)

	rh.Logging.Info("AMBIL DATA GANGGUAN PERILAKU")

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)
	rh.Logging.Info("NADI")
	rh.Logging.Info(payload.Nadi)

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	rh.Logging.Info(payload)

	resm, message, errs := rh.RMEUsecase.SaveTandaVItalIGDUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(message, http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, resm)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetGangguanPerilakuFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqVitalSign)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload.Noreg)
	rh.Logging.Info(payload.Person)
	rh.Logging.Info("Person")

	// data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, "", modulID, payload.Noreg)
	data, errs := rh.RMERepository.GetPemeriksaanFisikRepositoryIGD(userID, payload.Person, modulID, payload.Noreg)

	if errs != nil {
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPER PEMERIKSAAN FISIK KE VITAL SIGN
	rh.Logging.Info(data)
	rh.Logging.Info("DATA FISIK")
	mapper := rh.RMEMapper.ToResponseTriaseTandaVitalIGD(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetVitalSignBangsal(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	person := c.Locals("person").(string)
	// c.Locals("person", person)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload.Noreg)
	rh.Logging.Info(person)

	rh.Logging.Info("GET PEMERIKSAAN FISIK BANGSAL ")
	fisik, fiskErr := rh.RMERepository.CariDVitalSignRepository(userID, modulID, payload.Noreg)

	if fiskErr != nil {
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, fiskErr.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, fisik)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SavePemeriksaanFisikBangsal(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	message, errs := rh.RMEUsecase.SavePemeriksaanFisikBangsalUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SavePemeriksaanFisikAnak(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	message, errs := rh.RMEUsecase.SavePemeriksaanFisikAnakUseCase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikBangsal(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)

	data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, strings.ToUpper(person), modulID, payload.Noreg)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data.Noreg) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikBangsal(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikBangsalDokter(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(person)
	rh.Logging.Info(payload.Noreg)

	data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, "Dokter", modulID, payload.Noreg)

	rh.Logging.Info("Cari Pemeriksaan fisik")
	rh.Logging.Info(data)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data.Noreg) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(data)

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikBangsal(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanAnakHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload.Noreg)

	data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, "", modulID, payload.Noreg)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data.Noreg) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(data)

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikAnak(data)

	rh.Logging.Info("DATA FISIK ANAK")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveAsesmenPemeriksaanFisikIGD(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	_, message, errs := rh.RMEUsecase.SavePemeriksaanFisikIGDUsecase(userID, modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetAsesmenPemeriksaanFisikIGD(c *fiber.Ctx) error {
	payload := new(dto.ReqFisik)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload.Noreg)

	data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, payload.Person, modulID, payload.Noreg)

	rh.Logging.Info("DATA DITEMUKAN")
	rh.Logging.Info(data)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data.Noreg) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikIGD(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) CariDeskripsiSiki(c *fiber.Ctx) error {
	payload := new(dto.RegSikiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET SIKI
	data, errs := rh.RMEUsecase.CariDeskripsiSikiUsecase(payload.Siki)

	if errs != nil {
		response := helper.APIResponse("Error", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		rh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN MAPPING DATA
	// mapper := rh.RMEMapper.ToResponseSikiDeskripsiMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetVitalSignIGDHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqVitalSign)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)

	data, err12 := rh.RMEUsecase.GetVitalSignUseCase(payload.Noreg, payload.Person, modulID, payload.Pelayanan)

	if err12 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetVitalSignICUHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqVitalSign)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)
	rh.Logging.Info(payload)

	data, err12 := rh.RMERepository.OnGetDVitalSignICURepository(payload.Noreg, modulID, payload.Pelayanan, payload.Person)

	rh.Logging.Info("VITAL SIGN ICU REPOSITORY")
	rh.Logging.Info(data)
	rh.Logging.Info("============||========")

	mapper := rh.RMEMapper.ToMapperVitalSignICUResponse(data)

	if err12 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveVitalSignICUHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveVitalSignICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, err12 := rh.RMEUsecase.SaveVitalSignICUUseCase(userID, modulID, *payload)

	if err12 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnDeleteCPPTPasienHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteCPPT)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	res, message, errr := rh.RMEUsecase.OnDeleteDcpptUsecase(payload.No, payload.Norm)

	if errr != nil {
		rh.Logging.Error(errr.Error())
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetBHPHDHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	// CARI DPCCPT SOAP PASIEN

	rh.Logging.Info(userID)
	rh.Logging.Info(modulID)
	rh.Logging.Info(payload.Noreg)

	data, errs := rh.RMERepository.GetBHPDializerRepository(payload.Noreg)

	rh.Logging.Info("DATA DITEMUKAN")
	rh.Logging.Info(data)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) PengkajianKeperawatanBangsalPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ResPengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err12 := rh.RMEUsecase.OnGetReportPengkajianDewasaBangsalUseCase(*payload)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) RiwayatAsesmenPasienIGDHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	data, errs := rh.RMERepository.CariPemeriksaanFisikByDateRepository(userID, "", modulID, payload.Noreg)

	rh.Logging.Info("DATA DITEMUKAN")
	rh.Logging.Info(data)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data.Noreg) < 1 {
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikIGD(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) InsertCPPTPasien(c *fiber.Ctx) error {
	payload := new(dto.ReqInsertDcpptPasien)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	userID := c.Locals("userID").(string)

	// LAKUKAN INSERT DATA CPPT PASIEN
	message, res, errs := rh.RMEUsecase.SaveCPPTPasienUsecase(*payload, userID)

	if errs != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) UpdateCPPTFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqCPPTUpdate)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)

	message, update, er12 := rh.RMEUsecase.UpdateCPPTPasienUsecase(*payload)

	if er12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, update)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) RiwayatPenyakitDahulu(c *fiber.Ctx) error {
	payload := new(dto.RegSikiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	return nil
}

func (rh *RMEHandler) GetDaftarSDKIFiberHandler(c *fiber.Ctx) error {
	var judul = c.Params("judul")

	data, err := rh.RMERepository.GetSDKIRepository(judul)

	if err != nil {
		response := helper.APIResponse("Data", http.StatusCreated, err.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		rh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	// MAPPING DIAGNOSA KEPERAWATAN
	mapper := rh.RMEMapper.ToDiagnosaResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// TAMPILKAN JUDUL
func (rh *RMEHandler) GetIntervensiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegIntervensiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DATA INTERVENSI
	data, errs := rh.RMEUsecase.GetIntervensiUseCaseV2(*payload)

	if errs != nil {
		response := helper.APIResponse("Error", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusOK).JSON(response)
	}

	if len(data) < 1 {
		rh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	for i := 0; i <= len(data)-1; i++ {
		data[i].Ekspektasi = data[i].ResponseSLKI[0].Ekspektasi
		data[i].Judul = data[i].ResponseSLKI[0].Judul
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) GetSIKIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegSikiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET SIKI
	data, errs := rh.RMEUsecase.GetSIKIUseCase(payload.Siki)

	if errs != nil {
		response := helper.APIResponse("Error", http.StatusCreated, errs.Error())
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	if len(data) < 1 {
		rh.Logging.Error("Data Kosong")
		response := helper.APIResponseFailure("Data Kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveAsesmenKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenKeperawatanV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// USECASE SIMPAN ASESMEN KEPERAWATAN
	pesan, errs := rh.RMEUsecase.SaveAsesmenKeperawatanUsecaseV2(modulID, *payload)

	if errs != nil {
		rh.Logging.Error(errs.Error())
		response := helper.APIResponse(pesan, http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(pesan, http.StatusOK, "data")
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetAsesmenKeperawatanFiberHandler(c *fiber.Ctx) error {
	// todo: GET KEPERAWATAN
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, message, errs := rh.RMEUsecase.GetAsesmenKeperawatanBidanUseCase(payload.Noreg, modulID)

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveIntervensiResikoJatuhFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqIntervensiResikoJatuh)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	message, errs1 := rh.RMEUsecase.SaveIntervensiResikoJatuhUseCase(userID, modulID, *payload)
	rh.Logging.Info("DATA PAYLOAD")
	rh.Logging.Info(payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// TODO: GET RISIKO JATUH PASIEN
func (rh *RMEHandler) OnGetIntervensiResikoJatuhPasienFiberHandler(c *fiber.Ctx) error {
	return nil
}

func (rh *RMEHandler) SaveResikoJatuhPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResikoJatuhPasien)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	message, errs1 := rh.RMEUsecase.SaveResikoJatuhPasienUseCase(userID, modulID, *payload)

	rh.Logging.Info("DATA PAYLOAD")
	rh.Logging.Info(payload)

	if errs1 != nil {
		rh.Logging.Info(errs1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) DeteksiResikoJatuhPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)

	data, err1 := rh.RMEUsecase.GetAlertResikoJatuhPasienUseCase(modulID, payload.NoReg)

	if err1 != nil {
		var dat = dto.ResikoJatuhResponse{
			IsShow:   false,
			Hasil:    "",
			Tindakan: "",
		}

		response := helper.APIResponse("Data kosong", http.StatusOK, dat)
		rh.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("Data ditemukan", http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

// func generateIndex(fieldValue string) string {
// 	hasher := sha256.New()
// 	hasher.Write([]byte(fieldValue))
// 	hash := hex.EncodeToString(hasher.Sum(nil))

// 	// You can truncate the hash or use a specific part of it as an index
// 	// For example, using the first 8 characters of the hash
// 	index := hash[:8]

// 	return index
// }

// LAKUKAN GENERATE DATA SIKI
func (rh *RMEHandler) GenerateDataSIKIFiberHandler(c *fiber.Ctx) error {
	// GET ALL DATA SIKI
	data, errs := rh.RMERepository.GetAllSIKIRepository()

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}
	// times := time.Now()
	// PRINT ALL DATA
	for i := 0; i <= len(data)-1; i++ {
		// PARSING DATA OBSERVASI
		parts := strings.Split(data[i].Observasi, "\n")
		for a := 0; a <= len(parts)-1; a++ {

			if parts[a] != "" {
				rh.Logging.Info(a + 1)
				rh.Logging.Info(parts[a])
				// LAKUKAN INSERT OBSERVASI

				var data = rme.DIntervensi{
					KodeSiki:  data[i].Kode,
					NoUrut:    a + 1,
					Kategori:  "Observasi",
					Deskripsi: parts[a][3:],
				}

				rh.RMERepository.InsertDataIntevensiRespository(data)
			}

		}

		// PARSING DATA TERAPEUTIK
		tera := strings.Split(data[i].Terapeutik, "\n")
		for a := 0; a <= len(tera)-1; a++ {

			if tera[a] != "" {
				rh.Logging.Info(a + 1)
				rh.Logging.Info(tera[a])
				// LAKUKAN INSERT OBSERVASI
				var data = rme.DIntervensi{
					KodeSiki:  data[i].Kode,
					NoUrut:    a + 1,
					Kategori:  "Terapetutik",
					Deskripsi: tera[a][3:],
				}

				rh.RMERepository.InsertDataIntevensiRespository(data)
			}

		}
		// PARSING DATA EDUKASI
		edu := strings.Split(data[i].Edukasi, "\n")
		for a := 0; a <= len(edu)-1; a++ {

			if edu[a] != "" {
				rh.Logging.Info(a + 1)
				rh.Logging.Info(edu[a])
				// LAKUKAN INSERT OBSERVASI
				var data = rme.DIntervensi{
					KodeSiki:  data[i].Kode,
					NoUrut:    a + 1,
					Kategori:  "Edukasi",
					Deskripsi: edu[a][3:],
				}

				rh.RMERepository.InsertDataIntevensiRespository(data)
			}

		}

		// PARSING DATA KOLABORASI
		kola := strings.Split(data[i].Kolaborasi, "\n")
		for a := 0; a <= len(kola)-1; a++ {

			if kola[a] != "" {
				rh.Logging.Info(a + 1)
				rh.Logging.Info(kola[a])
				// LAKUKAN INSERT OBSERVASI
				var data = rme.DIntervensi{
					KodeSiki:  data[i].Kode,
					NoUrut:    a + 1,
					Kategori:  "Kolaborasi",
					Deskripsi: kola[a][3:],
				}

				rh.RMERepository.InsertDataIntevensiRespository(data)
			}

		}
	}

	response := helper.APIResponse("message", http.StatusOK, data)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// GET DESKRIPSI SIKI
func (rh *RMEHandler) GetDeskripsiSIKIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	return nil
}

func (rh *RMEHandler) GenerateKriteriaSLKIFIberHandler(c *fiber.Ctx) error {
	slki, errs := rh.RMERepository.GetALLSLKIRepository()

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// PRINT ALL DATA
	for i := 0; i <= len(slki)-1; i++ {
		rh.Logging.Info(slki[i].Defenisi)
		// INSERT DATA dkriteria_hasil_skli
		if len(slki[i].Menurun) > 1 {
			if slki[i].Tanda == "true" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Menurun,
					Kategori:     "Menurun",
					Tanda:        true,
					NoUrut:       slki[i].NoUrut,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

			if slki[i].Tanda == "false" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Menurun,
					Kategori:     "Menurun",
					Tanda:        false,
					NoUrut:       slki[i].NoUrut,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

		}

		if len(slki[i].Meningkat) > 1 {
			if slki[i].Tanda == "true" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Meningkat,
					Kategori:     "Meningkat",
					NoUrut:       slki[i].NoUrut,
					Tanda:        true,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

			if slki[i].Tanda == "false" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Meningkat,
					Kategori:     "Meningkat",
					NoUrut:       slki[i].NoUrut,
					Tanda:        false,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

		}

		if len(slki[i].Memburuk) > 1 {
			if slki[i].Tanda == "true" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Memburuk,
					Kategori:     "Memburuk",
					Tanda:        true,
					NoUrut:       slki[i].NoUrut,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

			if slki[i].Tanda == "false" {
				var data = rme.DKriteriaHasilSLKI{
					KodeSlki:     slki[i].Kode,
					NamaKriteria: slki[i].Memburuk,
					Kategori:     "Memburuk",
					Tanda:        true,
					NoUrut:       slki[i].NoUrut,
				}

				rh.RMERepository.InsertHasilSLKIRepository(data)
			}

		}
	}

	response := helper.APIResponse("message", http.StatusOK, slki)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GenerateSDKIFiberHandler(c *fiber.Ctx) error {
	// GET ALL DATA SLKI YANG LAMA
	slki, errs := rh.RMERepository.GetALLSLKIRepository()

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// PRINT ALL DATA
	for i := 0; i <= len(slki)-1; i++ {
		// CARI TERLEBIH DAHULU APAKAH SUDAH DI INPUT
		new, err1 := rh.RMERepository.SearchDataSLKIRepository(slki[i].Kode)

		if err1 != nil {
			response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		if new.KodeSlki == "" {
			rh.Logging.Info("SIMPAN DATA KODE SLKI")
			var data = rme.DSlki{
				KodeSlki:  slki[i].Kode,
				Judul:     slki[i].Judul,
				Defenisi:  slki[i].Defenisi,
				Ekspetasi: slki[i].Ekspektasi,
			}
			_, err2 := rh.RMERepository.InsertDataSLKIRespository(data)

			if err2 != nil {
				response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
				return c.Status(fiber.StatusCreated).JSON(response)
			}
		}
	}

	response := helper.APIResponse("message", http.StatusOK, slki)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) GenerateKodeSDKIFiberHandler(c *fiber.Ctx) error {
	// /GET ALL SDKI
	sdki, errs := rh.RMERepository.GetAllSDKIRepository()

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// PRINT ALL DATA
	for i := 0; i <= len(sdki)-1; i++ {

		// PARSING DATA OBSERVASI
		parts := strings.Split(sdki[i].MappingSlki, ",")

		// LOOPING
		// SLKI
		for a := 0; a <= len(parts)-1; a++ {
			// JIKA TIDAK KOSONG
			if parts[a] != "" {
				rh.Logging.Info(parts)
				//
				// Update with conditions
				rh.Logging.Info("LAKUKAN UPDATE")
				rh.RMERepository.UpdateSKDIByKodeRepository(parts[a], sdki[i].Kode)
			}
		}
	}

	response := helper.APIResponse("message", http.StatusOK, sdki)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

// DESKRIPSI LUARAN SLKI
func (rh *RMEHandler) GetDeskripsiLuaranSDKIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqKodeDiagnosa)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// USECASAE DESKRIPSI LUARAN SDKI
	data, message, err1 := rh.RMEUsecase.GetDeskripsiLuaranSDKUseCase(payload.KodeDiagnosa)

	if err1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)

	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveDeskripsiLuaranSDKIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.SikiRequest)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs1 := validate.Struct(payload); errs1 != nil {
		errors := helper.FormatValidationError(errs1)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(modulID)
	rh.Logging.Info(userID)

	// ==== //
	message, err5 := rh.RMEUsecase.SaveDeskripsiLuaranSDKIUserCase(modulID, userID, *payload)

	rh.Logging.Info(err5)
	rh.Logging.Info(message)

	if err5 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) GetDeskripsiAsuhanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.GetAsuhanKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// GET DESKRIPSI ASUHAN KEPERAWATAN USECASE
	rh.Logging.Info("LAKUKAN CARI ASUHAN KEPERAWATAN REPOSITORY")
	data, err5 := rh.RMERepository.GetDataAsuhanKeperawatanRepository(payload.Noreg, modulID, payload.Status)

	// AMBIL DATA USER YANG MELAKUKAN ASUHAN KEPERAWATN
	// TODO GET ASUHAN KEPERAWATAN

	if err5 != nil {
		response := helper.APIResponseFailure(err5.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("CARI ASUHAN KEPERAWATAN DI TEMUKAN")

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteAsuhanKeperawatanFiberhandler(c *fiber.Ctx) error {

	payload := new(dto.ReqOnDeleteAsuhanKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.RMERepository.OnDeleteDaskepSikiRepository(payload.NoDaskep)
	rh.RMERepository.OnDeleteDaskepSLKIRepository(payload.NoDaskep)
	rh.RMERepository.OnDeleteDaskepDiagnosaRepository(payload.NoDaskep)

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetDeskripsiAsuhanKeperawatanFiberHandlerV2(c *fiber.Ctx) error {
	payload := new(dto.GetAsuhanKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DESKRIPSI ASUHAN KEPERAWATAN USECASE
	modulID := c.Locals("modulID").(string)
	data, err5 := rh.RMERepository.GetDataAsuhanKeperawatanRepositoryV2(payload.Noreg, modulID, payload.Status)

	if err5 != nil {
		response := helper.APIResponseFailure(err5.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetDeskripsiAsuhanKeperawatanFiberHandlerV3(c *fiber.Ctx) error {
	payload := new(dto.GetAsuhanKeperawatan)
	errs := c.BodyParser(&payload)

	rh.Logging.Error("Dapatkan data")

	rh.Logging.Error(&payload)
	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DESKRIPSI ASUHAN KEPERAWATAN USECASE
	rh.Logging.Info("Data Asuhan keperawatan")

	modulID := c.Locals("modulID").(string)
	data, err5 := rh.RMERepository.GetDataAsuhanKeperawatanRepositoryV3(payload.Noreg, modulID, payload.Status)

	if err5 != nil {
		response := helper.APIResponseFailure(err5.Error(), http.StatusCreated)
		rh.Logging.Error(err5)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDaskepSLKIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveDaskepSLKI)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// USECASE ON SAVE DASKEP SLKI
	// SaveDataSKIKeperawatanUseCase(noDaskep string, kodeSLKI string, idKriteria int, hasil int) (message string, err error)
	message, err2 := rh.RMEUsecase.SaveDataSKIKeperawatanUseCase(payload.NoDaskep, payload.KodeSLKI, payload.IdKriteria, payload.Hasil)

	if err2 != nil {
		response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info(modulID)

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDataDaskepAllFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDasKepDiagnosaModel)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)

	rh.Logging.Info(payload)

	// USECASE ON SAVE DASKEP SLKI
	// SaveDataSKIKeperawatanUseCase(noDaskep string, kodeSLKI string, idKriteria int, hasil int) (message string, err error)
	message, err2 := rh.RMEUsecase.SaveDataAllSKIKeperawatanUseCase(*payload)

	if err2 != nil {
		response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnClosedDaskepSLKIFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.ReqOnUpdateDaskep)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// LAKUKAN UPDATE
	data, er2 := rh.RMEUsecase.OnUpdateDataDaskepToClosedUseCase(payload.NoDaskep, payload.Noreg, modulID, "Open")

	if er2 != nil {
		response := helper.APIResponseFailure(er2.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveAlergiObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveDataAlergiObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("SIMPAN ALERGI OBAT")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	simpan, message, err2 := rh.RMEUsecase.OnSaveDataAlergiUseCase(*payload, modulID)

	if err2 != nil {
		response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, simpan)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveRiwayatPenyakitKeluargahandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveRiwayatPenyakitKeluarga)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("SIMPPAN ALERGI KELUARGA")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	_, err12 := rh.RMERepository.OnSaveRiwayatKeluargaRepository(payload.NoRm, payload.InsertUser, modulID, payload.Alergi)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// GET DATA
	data, errqw := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRm)

	if errqw != nil {
		response := helper.APIResponseFailure(errqw.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetRiwayatPenyakitKeluargaHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatAlergi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DATA
	data, errs1 := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRM)

	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeletePenyakitKeluargaHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteDataAlergiObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	err2 := rh.RMERepository.OnDeleteAlergiRepository(payload.Nomor)

	if err2 != nil {
		response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data, _ := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRm)

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetAlergiObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatAlergi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	dahulu, _ := rh.RMERepository.GetPenyakitTerdahuluPerawatRepository(payload.NoRM)
	alergi, _ := rh.RMERepository.GetRiwayatAlergiRepository(payload.NoRM)

	mapper := rh.RMEMapper.ToMappingAlergi(dahulu, alergi)

	rh.Logging.Info("ALERGI PASIEN")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteAlergiObatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteDataAlergiObat)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info(payload)
	err2 := rh.RMERepository.OnDeleteAlergiRepository(payload.Nomor)

	if err2 != nil {
		response := helper.APIResponseFailure(err2.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

// KELUHAN UTAMA DOKTER
func (rh *RMEHandler) KeluhanUtamaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqKeluhanUtama)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	riwayatDahulu, _ := rh.RMERepository.OnGetRiwayatPenyakitDahulu(payload.NoRM, payload.Tanggal)
	rawatInap, _ := rh.SoapRepository.OnGetPengkajianKeperawatanDewasaRepository(payload.NoReg, modulID, userID)

	riwayatKeluarga, _ := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRM)
	keluUtama, _ := rh.RMERepository.GetAsesemenKeluhanUtamaIGDDokter(modulID, payload.NoReg, payload.Person)

	// MAPPER
	mapper := rh.RMEMapper.ToMappingKeluhanUtama(riwayatDahulu, riwayatKeluarga, keluUtama, rawatInap)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

// KELUHAN UTAMA DOKTER RAWAT INAP
func (rh *RMEHandler) OnGetKeluhanUtamaRANAP_FIBER_HANDLER(c *fiber.Ctx) error {
	payload := new(dto.ReqKeluhanUtama)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// riwayatDahulu, _ := rh.RMERepository.OnGetRiwayatPenyakitDahulu(payload.NoRM, payload.Tanggal)
	// rawatInap, _ := rh.SoapRepository.OnGetPengkajianKeperawatanDewasaRepository(payload.NoReg, modulID, userID)

	// riwayatKeluarga, _ := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRM)
	// keluUtama, _ := rh.RMERepository.GetAsesemenKeluhanUtamaIGDDokter(modulID, payload.NoReg, payload.Person)

	// mapper := rh.RMEMapper.ToMappingKeluhanUtama(riwayatDahulu, riwayatKeluarga, keluUtama, rawatInap)
	// response := helper.APIResponse("OK", http.StatusOK, mapper)

	// GET DATA KELUHAN UTAMA DOKTER RAWAT INAP

	return c.Status(fiber.StatusOK).JSON("response")
}

func (rh *RMEHandler) GetCPPTSBARPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqCPPTSBbar)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs1 := rh.RMERepository.OnGetDCPPTSBARRepository(payload.NoReg)

	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnSaveSBARFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveCPPTSBar)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// =============== //

	// OnSaveCPPTSBARUsecase
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er123 := rh.RMEUsecase.OnSaveCPPTSBARUsecase(*payload, userID, modulID)

	if er123 != nil {
		response := helper.APIResponseFailure(er123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnUpdateCPPTSBARFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqUpdateeCPPTSBar)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er123 := rh.RMEUsecase.OnUpdateCPTTSBARUsecase(*payload, userID, modulID)

	if er123 != nil {
		response := helper.APIResponseFailure(er123.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteCpptSBARFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteCPTTSBAR)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	_, err1 := rh.RMERepository.OnDeteleCPTTSBARRepository(payload.IdCppt)

	// message, er123 := rh.RMEUsecase.OnUpdateCPTTSBARUsecase(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveKartuObservasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveKartuObservasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// SAVE DATA
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	er12 := rh.RMERepository.OnSaveKartuObservasiRepository(userID, modulID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveKartuCairanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveKartuCairan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, messages, er12 := rh.RMERepository.OnSaveKartuCairanRepository(userID, modulID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messages, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetKartuCairanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetKartuObservasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	//
	res, er12 := rh.RMERepository.OnGetKartuCairanRepository(payload.Noreg)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("GET KARTU CAIRAN")

	mapper := rh.RMEMapper.ToResponseCairanKartuMapper(res)
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteKartuCairanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteKartuCairan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// DELETE KARTU CAIRAN FIBER HANDLER
	res, er12 := rh.RMERepository.OnDeleteCairanRepository(payload.IdKartu)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnUpdateKartuCairanFiberhandler(c *fiber.Ctx) error {
	payload := new(dto.OnUpdateKartuCairan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// LAKUKAN UPDATE KARTU CAIRAN
	var data = rme.DKartuCairanObatObatan{
		CairanMasuk1:      payload.CairanMasuk1,
		CairanMasuk2:      payload.CairanMasuk2,
		CairanMasuk3:      payload.CairanMasuk3,
		CairanMasukNgt:    payload.CairanMasukNgt,
		CairanKeluarUrine: payload.CairanKeluarUrine,
		CairanKeluarNgt:   payload.CairanKeluarNgt,
		NamaCairan:        payload.NamaCairan,
		Keterangan:        payload.Keterangan,
		DrainDll:          payload.Drain,
	}

	update, err12 := rh.RMERepository.OnUpdateKartuCairanRepository(payload.IdKartu, data)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil diubah", http.StatusOK, update)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetKartuObservasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetKartuObservasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// OnGetKartuObservasiRepository(noReg string) (res []rme.DKartuObservasi, err error)
	res, err12 := rh.RMERepository.OnGetKartuObservasiRepository(payload.Noreg)

	rh.Logging.Info(res)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.TOResponseDKartuObservasi(res)

	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteKartuObservasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteKartuObservasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// DELETE KARTU CAIRAN FIBER HANDLER
	res, er11 := rh.RMERepository.OnDeleteKartuObservasiRepository(payload.IDKartuObservasi)

	if er11 != nil {
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnUpdateKartuObservasiFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.ReqOnUpdateKartuObservasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// UPDATE KARTU OBSERVASI
	var kartu = rme.DKartuObservasi{
		T:            payload.T,
		N:            payload.N,
		P:            payload.P,
		S:            payload.S,
		Ekg:          payload.Ekg,
		Cvp:          payload.Cvp,
		PupilKi:      payload.PupilKi,
		RedaksiKa:    payload.RedaksiKa,
		PupilKa:      payload.PupilKa,
		AnggotaBadan: payload.AnggotaBadan,
		SputumWarna:  payload.SputumWarna,
		Keterangan:   payload.Keterangan,
		IsiCup:       payload.IsiCup,
		Kesadaran:    payload.Kesadaran,
		RedaksiKi:    payload.RedaksiKi,
	}

	updates, er1 := rh.RMERepository.OnUpdateKartuObservasiRepository(payload.IdObservasi, kartu)

	if er1 != nil {
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil diupdate", http.StatusOK, updates)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveRiwayatPenyakitKeluargaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveRiwayatPenyakitKelluarga)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("PAY LOAD")
	rh.Logging.Info(payload)

	// SIMPAN RIWAYAT PENYAKIT KELUARGA
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	times := time.Now()

	var alergi = rme.DAlergi{
		Id:         payload.NoRm,
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		Kelompok:   "keluarga",
		Alergi:     payload.Penyakit,
		InsertUser: userID,
		KdBagian:   modulID,
	}

	_, err12 := rh.RMERepository.OnSaveRiwayatPenyakitKeluargaRepository(alergi)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("OK", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveImplementasiCPPTFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveActionCpptFiberHandler)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// INSER DATA
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.RMERepository.InsertDpelaksanaKeperawatan(*payload, modulID, userID)

	response := helper.APIResponseFailure("OK", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveImplementasiTindakanSKLIFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveImplementasiKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	rh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, err12 := rh.RMERepository.SaveDImplementasiKeperawatanRepository(*payload, userID, modulID)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetPengkajianPersistemIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetAsesmenIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	vital, _ := rh.SoapUsecase.OnGetTandaVitalIGDPerawatUserCase(modulID, "Dokter", payload.Noreg, "rajal")
	res, er12 := rh.RMERepository.OnGetPengkajianAwalIGDRepository(payload.Noreg, modulID, payload.Person)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING DATA
	mapper := rh.RMEMapper.ToMappingPengkajianPersistemIGD(res, vital)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePengkajianPersistemIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveAsesmenIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	save, er123 := rh.RMEUsecase.OnSavePengkajianIGDUseCase(payload.Noreg, modulID, payload.Person, *payload, userID)

	if er123 != nil {
		response := helper.APIResponseFailure(er123.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(save, http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetTindakanImplementasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqImplementasiTindakan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	res, err12 := rh.RMERepository.GetImplementasiTindakanRepository(payload.NoAskep)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) ImplementasiTindakanAsuhanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTindakan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	res, err12 := rh.RMERepository.GetImplementasiTindakanRepository(payload.NoAskep)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteImplementasiTindakanAsuhanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteTindakan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// OnDeleteImplementasiTindakanRepository(idTindakan int) (err error)
	er1 := rh.RMERepository.OnDeleteImplementasiTindakanRepository(payload.IDTindakan)

	if er1 != nil {
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}
