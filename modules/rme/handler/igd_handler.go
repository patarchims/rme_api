package handler

import (
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (rh *RMEHandler) SavePemeriksaanFisikIGDDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(payload)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikIGDDokterFiberHandler(*payload, userID, modulID)

	rh.Logging.Info(message)
	rh.Logging.Info(err1)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePemeriksaanFisikICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSavePemeriksaanFisikICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// rh.Logging.Info(payload)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikICUFiberUsecase(*payload, userID, modulID)

	rh.Logging.Info(message)
	rh.Logging.Info(err1)

	if err1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SavePemeriksaanFisikIGDDokterMetodistFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikIGDDokterMethodist)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikIGDDokterMethodistFiberHandler(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePemeriksaanFisikDokterAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegPemeriksaanFisikDokterAntonio)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikDokterAntonioUseCase(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SavePemeriksaanFisikIGDDokterAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikIGDDokterMethodist)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikIGDDokterMethodistFiberHandler(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikIGDDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING

	mapper := rh.RMEMapper.ToPemeriksaanFisikIGDResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikIGDPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// GetPemeriksaanFisikIGDPerawatRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDPerawatRepository(payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING
	mapper := rh.RMEMapper.ToPemeriksaanFisikIGDResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikICUFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegGetPemeriksaanFisikICU)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikICUMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetPemeriksaanFisikAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// OnGetPemeriksaanFisikAnakRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	// data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	data, err1 := rh.RMERepository.OnGetPemeriksaanFisikAnakRepository(payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToResponsePemeriksaanFisikAnakMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePemeriksaanFisikAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSavePemeriksaanFisikAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// OnSavePemeriksaanFisikAnakUseCase(req dto.ReqOnSavePemeriksaanFisikAnak, kdBagian string, userID string) (res dto.ResponsePemeriksaanFisikAnak, message string, err error)

	res, messages, er12 := rh.RMEUsecase.OnSavePemeriksaanFisikAnakUseCase(*payload, modulID, userID)

	if er12 != nil {
		response := helper.APIResponseFailure(messages, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(messages, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikIGDDokterMethodistFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING

	mapper := rh.RMEMapper.ToPemeriksaanFisikIGDMethodisResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetPemeriksaanFisikDokterAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// OnUpdatePemeriksaanFisikAntonioDokterRepository(data rme.PemeriksaanFisikDokterAntonio, bagian string, person string, pelayanan string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)
	// OnGetPemeriksaanFisikDokterRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)

	data, err1 := rh.RMERepository.OnGetPemeriksaanFisikDokterRepository(payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING
	// ToPemeriksaanFisikAntonioResponse(data rme.PemeriksaanFisikDokterAntonio) (res dto.ResponsePemerikssanFisikAntonio)

	mapper := rh.RMEMapper.ToPemeriksaanFisikAntonioResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) GetPemeriksaanFisikIGDDokterAntonioFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING

	mapper := rh.RMEMapper.ToPemeriksaanFisikIGDAntonioResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetPemeriksaanFisikRawatInapDokterAntonioFiberHandler(c *fiber.Ctx) (err error) {
	// payload := new(dto.ReqGetPemeriksaanFisikRawatInapAntonio)
	// errs := c.BodyParser(&payload)

	// if errs != nil {
	// 	response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
	// 	rh.Logging.Error(response)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// validate := validator.New()

	// if errs := validate.Struct(payload); errs != nil {
	// 	errors := helper.FormatValidationError(errs)
	// 	response := helper.APIResponse("Error", http.StatusAccepted, errors)
	// 	rh.Logging.Error(response)
	// 	return c.Status(fiber.StatusAccepted).JSON(response)
	// }

	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// // data, err1 := rh.RMERepository.GetPemeriksaanFisikIGDRepository(userID, payload.Person, modulID, payload.Noreg)

	// if err1 != nil {
	// 	rh.Logging.Info(err1)
	// 	response := helper.APIResponseFailure("", http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// MAPPING

	// mapper := rh.RMEMapper.ToPemeriksaanFisikIGDAntonioResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, "mapper")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) SaveRiwayatPenyakitKeluargaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatKeluargaIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// SIMPAN KELUHAN UTAMA IGD DOKTER

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, mesage, err1 := rh.RMEUsecase.OnSaveRiwayatPenyakitKeluargaIGDUsecase(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponse(mesage, http.StatusCreated, data)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveKeluhanUtamaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqKeluhanUtamaIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// SIMPAN KELUHAN UTAMA IGD DOKTER
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, mesage, err1 := rh.RMEUsecase.OnSaveKeluhanUtamaIGDUsecase(*payload, userID, modulID)

	if err1 != nil {
		response := helper.APIResponse(mesage, http.StatusCreated, data)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) CPPTPasien(c *fiber.Ctx) error {
	payload := new(dto.RegNoRm)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := rh.RMERepository.CariCPPTRepository(payload.Norm)

	if errs != nil {
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, payload)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToCPPTResponse(data)

	m := map[string]any{}

	if len(mapper) > 0 {
		m["cppt"] = mapper
	} else {
		m["cppt"] = []string{}
	}

	response := helper.APIResponse("OK", http.StatusOK, m)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportAsesmenAwalMedisFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenAwalMedis)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ress, errr := rh.RMEUsecase.OnGetReportAsesmenAwalMedisKelurgaUseCase(*payload)

	if errr != nil {
		response := helper.APIResponse(errr.Error(), http.StatusCreated, ress)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, ress)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportAsesmenAwalHarapanMedisFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenAwalMedis)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	rh.Logging.Info("ASESMEN MEDIS DOKTER")
	rh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	ress, errr := rh.RMEUsecase.OnGetReportAsesmenAwalMedisKelurgaUseCase(*payload)

	if errr != nil {
		response := helper.APIResponse(errr.Error(), http.StatusCreated, ress)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, ress)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetAsesmenAwalMedisDokterFiberAntonioHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenAwalMedis)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	// GET DATA USER
	pasien, _ := rh.UserRepository.OnGetProfilePasienRepository(payload.NoRM)
	vital, _ := rh.SoapRepository.GetTandaVitalIGDReportdDokterRepository(payload.NoReg)
	data1, _ := rh.RMERepository.GetAsesmenAwalMedisIGDReportDokter(payload.NoReg)
	data2, _ := rh.RMERepository.OnGetRiwayatPenyakitDahulu(payload.NoRM, payload.Tanggal)
	data3, _ := rh.RMERepository.GetRiwayatAlergiKeluargaRepository(payload.NoRM)
	data4, _ := rh.SoapRepository.GetRencanaTindakLanjutReportRepository(payload.NoReg)
	data5, _ := rh.SoapRepository.GetDiagnosaRepositoryReportIGD(payload.NoReg)
	data6, _ := rh.RMERepository.GetPemeriksaanFisikReportIGDRepository(payload.NoReg)
	diagnosa, _ := rh.DiagnosaRepository.OnGetDianosaBandingRepository(payload.NoReg, payload.KdBagian, payload.Pelayanan)
	// diagBanding, _ := su.diagnosaRepository.OnGetDianosaBandingRepository(reg.NoReg, reg.KdBagian, reg.Pelayanan)

	// TAMBAHKAN PEMERIKSAAN PENUNJANG
	labor, _ := rh.HisUseCase.HistoryLaboratoriumUsecaseV2(payload.NoReg)
	radiologi, _ := rh.HisUseCase.HistoryRadiologiUsecaseV2(payload.NoReg)
	mapperFisik := rh.RMEMapper.ToPemeriksaanFisikIGDResponse(data6)
	mapperUser := rh.UserMapper.ToMappingProfilePasien(pasien)

	// mapperPasien := su.userMapper.ToMappingProfilePasien(pasien)

	mapper := rh.RMEMapper.ToMapperReportIGDResponse(data1, data2, data3, data4, data5, mapperFisik, vital, labor, radiologi, mapperUser, diagnosa)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) SavePemeriksaanFisikBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisik)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(payload)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikBangsalUseCase(*payload, userID, modulID)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SavePemeriksaanFisikPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		// rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikPerinaUseCase(*payload, userID, modulID)

	if err1 != nil {
		// rh.Logging.Info(err1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("PRINT DATA ")
	rh.Logging.Info(payload)

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetDVitalSignFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnReqVitalSign)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info(userID)

	data, err12 := rh.RMERepository.GetDVitalSignRepository(payload.Noreg, modulID, payload.Pelayanan, payload.Person)

	if err12 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperResponseDVitalSignPerina(data)

	rh.Logging.Info("PRINT MAPPER")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDVitalSignPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDVitalSignPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// rh.Logging.Info(userID)
	rh.Logging.Info("Keadaan Umum")
	rh.Logging.Info(payload.KeadaanUmum)

	data, message, err12 := rh.RMEUsecase.SaveVitalSignPerinaUseCase(userID, modulID, *payload)

	if err12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, err12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveDVitalSignPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPemeriksaanFisikPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err1 := rh.RMEUsecase.OnSavePemeriksaanFisikPerinaUseCase(*payload, userID, modulID)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetPemeriksaanFisikPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("PEMERIKSAAN FISIK PERINA")
	rh.Logging.Info(payload)
	rh.Logging.Info(modulID)

	data, err1 := rh.RMERepository.OnGetPemeriksaanFisikBangsalRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("Error", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING
	mapper := rh.RMEMapper.ToPemeriksaanFisikPerinaResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	rh.Logging.Info(response)

	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh RMEHandler) OnGetAsesmenBayiBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenBayiPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	data, err1 := rh.RMEUsecase.OnGetAsesmenKeperawatanBayiUseCase(payload.Noreg, payload.NoRm, modulID)

	rh.Logging.Info("DATA ASESMEN")
	rh.Logging.Info(data.AsesemenBayi)

	if err1 != nil {
		response := helper.APIResponse(err1.Error(), http.StatusCreated, data)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("KELAHIRAN BAYI")
	rh.Logging.Info(data.AsesemenBayi.AseskepBayiRwtTglKelahiranBayi)

	response := helper.APIResponse("OK", http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh RMEHandler) OnSaveAsesmenBayiBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveAsesemenKeperawatanBayi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	rh.Logging.Info("ASESMEN BANGSAL ")
	rh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("OBAT OBATAN REQ")
	rh.Logging.Info(payload.AseskepBayiPrenatalObatObatan)

	data, message, err1 := rh.RMEUsecase.OnSaveAsesmenKeperawatanBayiUseCase(payload.NoReg, payload.NoRm, modulID, userID, *payload)

	if err1 != nil {
		response := helper.APIResponse(message, http.StatusCreated, data)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("OBAT OBATAN DI KOMSUMSI")
	rh.Logging.Info(data.AsesemenBayi.ObatObatanYangDikomsumsi)

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) SaveRiwayatKelahiranLaluPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatKehamilanPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// INSERT DATA PEMERIKSAAN FISIK IGD DOKTER

	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)
	return nil
}

func (rh *RMEHandler) OnGetPemeriksaanFisikBangsal(c *fiber.Ctx) error {
	payload := new(dto.ReqGetPemeriksaanFisikBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, err1 := rh.RMERepository.OnGetPemeriksaanFisikBangsalRepository(userID, payload.Person, modulID, payload.Noreg)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING

	mapper := rh.RMEMapper.ToPemeriksaanFisikIGDResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetDownScoreNeoNatusFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDownScoreNeoNatus)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err13 := rh.RMERepository.OnGetDownScroeNeoNatusRepository(payload.Noreg)

	if err13 != nil {
		rh.Logging.Info(err13)
		response := helper.APIResponseFailure("", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperResponseNeonatus(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDownScoreNeoNatusFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveDownScreNeoNatus)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("DATA DOWN SCORE NEONATUS")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err13 := rh.RMEUsecase.OnSaveDownScoreNeoNatusUseCase(payload.Noreg, *payload, modulID, userID, payload.Person)

	if err13 != nil {
		rh.Logging.Info(err13)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveRiwayatKelahiranYangLaluPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRiwayatKehamilanPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	save, message, er12 := rh.RMERepository.OnSaveRiwayatKehamilanLaluPerinaRepository(userID, modulID, *payload)

	if er12 != nil {
		rh.Logging.Info(er12)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteRiwayatKelahiranYangLaluPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteRiwayatKehamilanPerina)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	_, mesage, err1 := rh.RMERepository.OnDeleteRiwayatKehamilanYangLaluRepository(payload.KdRiwayat)
	data, _ := rh.RMEUsecase.OnGetAsesmenKeperawatanBayiUseCase(payload.Noreg, payload.NoRm, modulID)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure(mesage, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDataApgarScoreFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveApgarScore)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	rh.Logging.Info("DATA DOWN SCORE NEONATUS")
	rh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, err13 := rh.RMEUsecase.OnSaveApgarScoreNeoNatus(payload.NoReg, *payload, modulID, userID)

	if err13 != nil {
		rh.Logging.Info(err13)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetDataApgarScoreNeoNatusFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqApgarScore)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err13 := rh.RMERepository.OnGetApgarScoreNeoNatusRepository(payload.Noreg)

	if err13 != nil {
		rh.Logging.Info(err13)
		response := helper.APIResponseFailure(err13.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperScoreNeoNatus(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportFormulirBayiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenBayi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs1 := rh.RMEUsecase.OnGetReportAsesmenKeperawatanPerinaUseCase(payload.Noreg, payload.NoRm)

	// REPORT
	if errs1 != nil {
		rh.Logging.Info(errs1)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnGetAllDiagnosaKeperawatan(c *fiber.Ctx) error {

	data, err12 := rh.RMERepository.OnGetAllDiagnosaKeperawatanRepository()

	// REPORT
	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveAnalisaDataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// data, errs1 := rh.RMEUsecase.OnGetReportAsesmenKeperawatanPerinaUseCase(payload.Noreg, payload.NoRm)
	message, er123 := rh.RMEUsecase.OnSaveAnalisaDataUseCase(modulID, userID, *payload)

	// REPORT
	if er123 != nil {
		rh.Logging.Info(er123)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetAnalisaDataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ANALISA DATA

	data, err12 := rh.RMERepository.GetAnalisaDataRepository(payload.Noreg)
	var analisa []rme.DAnalisaDataModel

	// REPORT
	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		analisa = make([]rme.DAnalisaDataModel, 0)
		response := helper.APIResponse("OK", http.StatusCreated, analisa)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnDeleteAnalisaDataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnDeleteAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// DELETE ANALISA DATA
	message, err12 := rh.RMERepository.OnDeleteAnalisaDataRepository(payload.KodeAnalisa)

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}
	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnValidaasiAnalisaDataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqValidasiAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	userID := c.Locals("userID").(string)
	//===FALIDASI DATA
	validasi, er11 := rh.RMEUsecase.OnValidasiAnalisaDataUseCase(*payload, userID)

	if er11 != nil {
		rh.Logging.Info(er11)
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, validasi)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnValidasiAnalisaDataEnumFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqValidasiAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	userID := c.Locals("userID").(string)
	//===FALIDASI DATA
	validasi, er11 := rh.RMEUsecase.OnValidasiAnalisaDataEnumUseCase(*payload, userID)

	if er11 != nil {
		rh.Logging.Info(er11)
		response := helper.APIResponseFailure(er11.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, validasi)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportAnalisaDataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetAnalisaData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ANALISA DATA
	data, err12 := rh.RMEUsecase.OnGetReportAnalisaDataUseCase(payload.Noreg)
	var analisa []rme.DAnalisaDataModel

	// REPORT
	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		analisa = make([]rme.DAnalisaDataModel, 0)
		response := helper.APIResponse("OK", http.StatusCreated, analisa)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnGetTindakLanjutPerinaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResumeMedisPerinatologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	data, err12 := rh.RMEUsecase.OnGetTindakLanjutPerinaDokterUseCase(payload.Noreg, modulID)

	// REPORT
	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveTindakLanjutPerinatologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveTindakLajutPerinatologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, err12 := rh.RMEUsecase.OnSaveTindakLanjutPerinaDokterUseCase(payload.Noreg, modulID, userID, *payload)

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ===//
func (rh *RMEHandler) OnGetReportResumeMedisPerinatologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResumeMedisPerinatologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data := rh.RMEUsecase.OnGetReportResumeMedisPerinaUseCase(payload.NoRM, payload.Noreg)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportIdentitasBayiPerinatologiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResumeMedisPerinatologi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data := rh.RMEUsecase.OnGetReportResumeMedisPerinaUseCase(payload.NoRM, payload.Noreg)

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveAsesmenSkalaNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenSkalaNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, err12 := rh.RMEUsecase.OnSaveSkalaNyeriKeperawatanDewasaUseCase(*payload, modulID, userID)

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetAsesmenNyeriDewasaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetAsesmenSkalaNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err12 := rh.RMERepository.OnGetSkalaNyeriKeperawatanDewasaRepository(payload.Noreg, modulID)

	// MAPPING DATA

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperSkalaNyeriDewasa(data)
	rh.Logging.Info("SKALA NYERI DEWASA")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetAsesmenSkalaNyeriBidanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqGetAsesmenSkalaNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// OnGetSkalaNyeriBidanRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error)

	data, err12 := rh.RMERepository.OnGetSkalaNyeriBidanRepository(payload.Noreg, modulID)

	// MAPPING DATA

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperSkalaNyeriDewasa(data)
	rh.Logging.Info("SKALA NYERI DEWASA")
	rh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportAsesmenNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	nyeri, errq := rh.RMERepository.OnGetAsesmenSkalaNyeriRepository(payload.Noreg)

	if errq != nil {
		rh.Logging.Info(errq)
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPER NYERI
	users, _ := rh.UserRepository.GetKaryawanRepository(nyeri.InsertUserId)

	mapperNyeri := rh.RMEMapper.ToMapperSkalaNyeriDewasa(nyeri)

	rh.Logging.Info("NYERI")
	rh.Logging.Info(nyeri)

	// MAPPING SKALA NYERI
	mapper := rh.RMEMapper.ToMappingAsesmenNyeriResponse(users, mapperNyeri)
	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportRingkasanMasukDanKeluarFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqRingkasanMasukDanKeluar)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET DATA DATA PROFILE PASIEN
	ringkasan, _ := rh.RMEUsecase.OnGetRingkasanMasuDanKeluarUseCase(payload.NoRM)
	response := helper.APIResponse("OK", http.StatusOK, ringkasan)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) UploadIdentitasBayiFiberHandler(c *fiber.Ctx) error {
	files, err := c.FormFile("imageUrl")

	if err != nil {
		rh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	var kategori = c.FormValue("kategori")
	var dpjp = c.FormValue("dpjp")

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, er12 := rh.RMEUsecase.OnUploadIndentiasBayiPerinaUseCase(kategori, noReg, modulID, files, c, userID, dpjp)

	if er12 != nil {
		rh.Logging.Info(er12)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	rh.Logging.Info("DATA IDENTITAS BAYI")
	rh.Logging.Info(data)

	response := helper.APIResponse(message, http.StatusOK, data)
	rh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetIdentitasBayiBayiHandler(c *fiber.Ctx) error {
	payload := new(dto.OnRegGetIdentitasBayi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err12 := rh.RMERepository.OnGetKakiKananBayiRepository(payload.NoReg, modulID)
	profil, _ := rh.ReporRepository.GetDataPasienRepository(payload.NoRM)
	vitalSign, _ := rh.RMERepository.GetDVitalSignPerinaRepository(payload.NoReg)

	if err12 != nil {
		rh.Logging.Info(err12)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := rh.RMEMapper.ToMapperIdentitasBayi(data)

	// MAPPING RESPONSE
	// TO RESPONE IDENTIAS
	bayi := rh.RMEMapper.ToMapperIdentitasBayiMapper(mapper, profil, vitalSign)

	response := helper.APIResponse("OK", http.StatusOK, bayi)
	rh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveIndentitasBayiHandler(c *fiber.Ctx) error {
	files1, err := c.FormFile("ttd1")

	if err != nil {
		rh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Tanda tangan penentu jenis kelamin tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	files2, err := c.FormFile("ttd2")

	if err != nil {
		rh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Tanda tangan wali/bapak/ibu tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var pemberiGelang = c.FormValue("pemberi-gelang")

	if pemberiGelang == "" {
		response := helper.APIResponseFailure("Nama Pemberi Gelang harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var namaPenentuJK = c.FormValue("nama-penentu-jk")

	if namaPenentuJK == "" {
		response := helper.APIResponseFailure("Nama Penentu Jenis Kelamin harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var namaWali = c.FormValue("nama-wali")

	if namaWali == "" {
		response := helper.APIResponseFailure("Nama Wali harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var kategori = c.FormValue("kategori")

	if kategori == "" {
		response := helper.APIResponseFailure("Kategori harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var dpjp = c.FormValue("dpjp")

	if dpjp == "" {
		response := helper.APIResponseFailure("DPJP harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}
	var jamKelahiran = c.FormValue("jam-kelahiran")

	if jamKelahiran == "" {
		response := helper.APIResponseFailure("DPJP harus diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ONSAVE DATA IDENTITAS BAYI PERINA
	rh.Logging.Info(files1)
	rh.Logging.Info(files2)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := rh.RMEUsecase.OnSaveIdentitasPerinaUseCase(files1, files2, c, noReg, modulID, dpjp, userID, namaPenentuJK, namaWali, pemberiGelang, jamKelahiran)

	if er12 != nil {
		rh.Logging.Info(er12)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	rh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveCatatanKeperawatanHandler(c *fiber.Ctx) error {
	payload := new(dto.OnSaveCatatanKeperawtan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ONSAVE CATATAN KEPERAWATAN

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	times := time.Now()

	var catatan = rme.DCatatanKeperawatan{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		KdBagian:   modulID,
		UserId:     userID,
		Noreg:      payload.NoReg,
		Catatan:    payload.Catatan,
	}

	save, er12 := rh.RMERepository.OnSaveCatatanKeperawatanRepository(catatan)

	if er12 != nil {
		rh.Logging.Info(er12)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteCatatanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteEarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	delete, err1 := rh.RMERepository.OnDeleteCatatanKeperawatan(payload.ID)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, delete)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (rh *RMEHandler) OnUpdateCatatanKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnUpdateData)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	var catatan = rme.DCatatanKeperawatan{
		Catatan: payload.Catatan,
	}

	modulID := c.Locals("modulID").(string)

	save, er12 := rh.RMERepository.OnUpdateCatatanKeperawatanRepository(catatan, payload.ID, modulID)

	if er12 != nil {
		rh.Logging.Info(er12)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil diubah", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportCatatanKeperawtanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetEarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er12 := rh.RMERepository.OnGetCatatanKeperawatanRepository(payload.NoReg)

	if er12 != nil {
		response := helper.APIResponseFailure(er12.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteEarlyWarningSystemHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteEarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// DELTE DATA
	// OnDeleteDEWarningSystemRepository(ID int) (res rme.DearlyWarningSystem, err error)
	data, err1 := rh.RMERepository.OnDeleteDEWarningSystemRepository(payload.ID)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveErlyWarningSystemAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnRegSaveEarlyWarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Info(errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON SAVE
	times := time.Now()

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("DATA SYSTEM")
	rh.Logging.Info(payload)

	var datas = rme.DearlyWarningSystem{
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
		UserId:           userID,
		KdBagian:         modulID,
		Keterangan:       payload.Keterangan,
		Td2:              payload.Td2,
		Kategori:         payload.Kategori,
		Noreg:            payload.NoReg,
		TingkatKesadaran: payload.Kesadaran,
		Td:               payload.Td,
		Nadi:             payload.Nadi,
		Pernapasan:       payload.Pernapasan,
		ReaksiOtot:       payload.ReaksiOtot,
		Suhu:             payload.Suhu,
		ObsigenTambahan:  payload.ObsitenTambahan,
		Spo2:             payload.Spo2,
		Crt:              payload.Crt,
		SkalaNyeri:       payload.SkalaNyeri,
		TotalSkor:        payload.TotalSkor,
	}

	save, err1 := rh.RMERepository.OnInsertDewarningSystemRepository(datas)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveEarlyWarningSystemHandler(c *fiber.Ctx) error {
	payload := new(dto.OnRegSaveEarlyWarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Info(errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON SAVE
	times := time.Now()

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	rh.Logging.Info("DATA SYSTEM")
	rh.Logging.Info(payload)

	var datas = rme.DearlyWarningSystem{
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
		UserId:           userID,
		KdBagian:         modulID,
		Keterangan:       payload.Keterangan,
		Td2:              payload.Td2,
		Kategori:         payload.Kategori,
		Noreg:            payload.NoReg,
		TingkatKesadaran: payload.Kesadaran,
		Td:               payload.Td,
		Nadi:             payload.Nadi,
		Pernapasan:       payload.Pernapasan,
		ReaksiOtot:       payload.ReaksiOtot,
		Suhu:             payload.Suhu,
		ObsigenTambahan:  payload.ObsitenTambahan,
		Spo2:             payload.Spo2,
		Crt:              payload.Crt,
		SkalaNyeri:       payload.SkalaNyeri,
		TotalSkor:        payload.TotalSkor,
	}

	save, err1 := rh.RMERepository.OnInsertDewarningSystemRepository(datas)

	if err1 != nil {
		rh.Logging.Info(err1)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveEarlyWarningSystemAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnRegSaveEarlyWarningSystemAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON SAVE
	// times := time.Now()

	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	rh.Logging.Info("DATA SYSTEM")
	rh.Logging.Info(payload)

	// var datas = rme.DearlyWarningSystem{
	// 	InsertDttm:       times.Format("2006-01-02 15:04:05"),
	// 	UserId:           userID,
	// 	KdBagian:         modulID,
	// 	Keterangan:       payload.Keterangan,
	// 	Td2:              payload.Td2,
	// 	Kategori:         payload.Kategori,
	// 	Noreg:            payload.NoReg,
	// 	TingkatKesadaran: payload.Kesadaran,
	// 	Td:               payload.Td,
	// 	Nadi:             payload.Nadi,
	// 	Pernapasan:       payload.Pernapasan,
	// 	ReaksiOtot:       payload.ReaksiOtot,
	// 	Suhu:             payload.Suhu,
	// 	ObsigenTambahan:  payload.ObsitenTambahan,
	// 	Spo2:             payload.Spo2,
	// 	Crt:              payload.Crt,
	// 	SkalaNyeri:       payload.SkalaNyeri,
	// 	TotalSkor:        payload.TotalSkor,
	// }

	// save, err1 := rh.RMERepository.OnInsertDewarningSystemRepository(datas)

	// if err1 != nil {
	// 	rh.Logging.Info(err1)
	// 	response := helper.APIResponseFailure("ERROR", http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, save)
	return c.Status(fiber.StatusOK).JSON("response")
}

func (rh *RMEHandler) OnGetEarlyWarningSystemHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetEarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	data, er1 := rh.RMERepository.OnGetEaningWaningSystemRepository(payload.NoReg)

	if er1 != nil {
		rh.Logging.Info(er1)
		response := helper.APIResponseFailure("ERROR", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING DATAa
	var ealry []dto.ResponseEarlyWarningSystem

	if len(data) == 0 {
		ealry = make([]dto.ResponseEarlyWarningSystem, 0)
	} else {
		ealry = rh.RMEMapper.ToMapperEarlyWarningSystemResponse(data)
	}

	response := helper.APIResponse("OK", http.StatusOK, ealry)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportEarlyWarningSystemHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetEarningSystem)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	// data, er1 := rh.RMERepository.OnGetEaningWaningSystemRepository(payload.NoReg)

	// if er1 != nil {
	// 	rh.Logging.Info(er1)
	// 	response := helper.APIResponseFailure("ERROR", http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }
	return nil
}

func (rh *RMEHandler) OnGetDataEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	data, er1 := rh.RMERepository.OnGetEdukasiTerintegrasi(payload.NoRM)

	rh.Logging.Info("DATA")
	rh.Logging.Info(data)

	if er1 != nil {
		rh.Logging.Info(er1)
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]rme.DEdukasiTerintegrasi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveDataEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveDataEdukasiTerintegrasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, mesage, er123 := rh.RMEUsecase.OnSaveDataEdukasiTerintegrasiUseCase(modulID, userID, *payload)

	if er123 != nil {
		rh.Logging.Info(er123)
		response := helper.APIResponseFailure(mesage, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetReportEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	data, er1 := rh.RMERepository.OnGetEdukasiTerintegrasi(payload.NoRM)

	if er1 != nil {
		rh.Logging.Info(er1)
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]rme.DEdukasiTerintegrasi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnUpdateEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnUpdateEdukasiTerintegrasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data := rme.DEdukasiTerintegrasi{
		Metode:            payload.Metode,
		Informasi:         payload.Informasi,
		PemberiInformasi:  payload.PemberiInformasi,
		PenerimaInformasi: payload.PemberiInformasi,
		Evaluasi:          payload.Evaluasi,
	}

	_, er123 := rh.RMERepository.OnUpdateEdukasiTerintegrasiRepository(payload.Id, data)

	if er123 != nil {
		response := helper.APIResponseFailure("Data gagal ubah", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil ubah", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteDataEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqIDEdukasiTerintegrasi)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	_, er123 := rh.RMERepository.OnDeleteEdukasiTerintegrasi(payload.ID)

	if er123 != nil {
		response := helper.APIResponseFailure("Data gagal dihapus", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetDataPemberiEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	data, er1 := rh.RMERepository.OnGetEdukasiTerintegrasi(payload.NoRM)

	if er1 != nil {
		rh.Logging.Info(er1)
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]rme.DEdukasiTerintegrasi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnAddDataEdukasiTerintegrasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetNoRM)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// ON GET EANINGN SYSTEM
	data, er1 := rh.RMERepository.OnGetEdukasiTerintegrasi(payload.NoRM)

	if er1 != nil {
		rh.Logging.Info(er1)
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]rme.DEdukasiTerintegrasi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
