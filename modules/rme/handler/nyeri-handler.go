package handler

import (
	"fmt"
	"hms_api/modules/rme/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (rh *RMEHandler) OnGetAsesmenUlangNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoReg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er12 := rh.RMERepository.OnGetDAssemenUalngNyeriRepsoitory(*payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data gagal didapat", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnReportAsesmenUlangNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoReg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er12 := rh.RMERepository.OnGetDAssemenUalngNyeriRepsoitory(*payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data gagal didapat", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnUpdateAsesmenUlangNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnUpdateUlangNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, "data")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteAssemenUlangNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqIDUlangNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er12 := rh.RMERepository.OnDeleteAsesmenUlangNyeri(*payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data gagal dihapus", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveAssesmenNyeriFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveAsesmenNyeri)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, er12 := rh.RMEUsecase.OnSaveNyeriUseCase(*payload, modulID, userID)

	if er12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)

}
