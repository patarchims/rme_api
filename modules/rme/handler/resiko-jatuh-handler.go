package handler

import (
	"hms_api/modules/rme/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (rh *RMEHandler) OnGetResikoJatuhFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoReg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er12 := rh.RMERepository.OnGetDAssemenUalngNyeriRepsoitory(*payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data gagal didapat", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSavePasienResikoJatuhDewasaFiberHandler(c *fiber.Ctx) error {
	// ON SAVE RESIKO JATUH PASIEN

	return nil
}

func (rh *RMEHandler) ON_GET_RESIKO_JATUH_RAWAT_INAP_DEWASA_FIBER_HANDLER(c *fiber.Ctx) error {
	return nil
}
