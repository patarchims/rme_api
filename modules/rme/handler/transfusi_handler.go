package handler

import (
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (rh *RMEHandler) OnChangedTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnChangedTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	update, message, er11 := rh.RMEUsecase.OnUpdateTransfusiDarahUsecase(*payload)

	if er11 != nil {
		response := helper.APIResponse(er11.Error(), http.StatusCreated, er11.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, update)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnGetTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	data, er12 := rh.RMERepository.OnGetDTransfusiDarahRepository(*payload, modulID)
	rh.Logging.Info("DATA ")
	rh.Logging.Info(data)

	if er12 != nil {
		response := helper.APIResponse(er12.Error(), http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]rme.DTransfusiDarah, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	_, er12 := rh.RMERepository.InsertDtransfusiDarahRepository(*payload, modulID, userID)

	if er12 != nil {
		response := helper.APIResponse(er12.Error(), http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnSaveReaksiTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReaksiTranfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	// modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	message, er121 := rh.RMEUsecase.OnSaveReaksiTransfusiDarahUseCase(userID, *payload)

	if er121 != nil {
		response := helper.APIResponse(er121.Error(), http.StatusCreated, er121.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnGetVerifyReaksiTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnGetVerifyTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat diproses", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er123 := rh.RMERepository.OnGetVerifikasiTransfusiDarahByIDTransfusiRepository(payload.IDTransfusi)

	if er123 != nil {
		response := helper.APIResponseFailure("Data gagal didapat", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnVerifyReaksiTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.VerifyTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, errr := rh.RMEUsecase.OnVerifyTransfusiDarahUseCase(*payload, modulID, userID)

	if errr != nil {
		response := helper.APIResponse(message, http.StatusCreated, message)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteTransfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	message, er12 := rh.RMEUsecase.OnDeleteTransfusiDarahUseCase(*payload)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, message)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteReaksiTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteReaksiTranfusiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASUHAN KEPERAWATAN
	// modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	message, er12 := rh.RMEUsecase.OnDeleteReaksiTransfusiDarahUseCase(*payload)

	if er12 != nil {
		response := helper.APIResponse(message, http.StatusCreated, message)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnDeleteReaskiTransfusiDarahFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqViewReaksiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er123 := rh.RMERepository.OnViewReaksiDarahByIDTranfusiDarah(payload.IDTransfusi)

	if er123 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, er123.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *RMEHandler) OnViewReaksiTranfusiDarahByNoTranfusiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqViewReaksiDarah)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", strings.Join(errors, "\n"))
		response := helper.APIResponse(message, http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, er123 := rh.RMERepository.OnViewReaksiDarahByIDTranfusiDarah(payload.IDTransfusi)
	rh.Logging.Info(data)

	if er123 != nil {
		response := helper.APIResponse("Error", http.StatusCreated, er123.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
