package mapper

import (
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

func (rm *RMEMapperImpl) ToMappingPengkajianAwalKeperawatanIGDAntonio(persistem rme.PengkajianPersistemIGD, nyeri soapDTO.ResSkriningNyeriIGD, asuhan []rme.DasKepDiagnosaModelV2, pemFisik dto.ResponsePemeriksaanFisik, asesmenIGD soap.AsesemenIGD, karyawan user.Karyawan) (res dto.ResponsePengkajianIGDKeperawatanAntonio) {

	if len(asuhan) == 0 {
		return dto.ResponsePengkajianIGDKeperawatanAntonio{
			PengkajiaPersistem: rm.ToResponsePengkajianKeperawatan(persistem),
			Nyeri:              nyeri,
			Asuhan:             make([]rme.DasKepDiagnosaModelV2, 0),
			Pemfisik:           pemFisik,
			AsesmenIGD:         asesmenIGD,
			User:               karyawan,
		}
	} else {
		return dto.ResponsePengkajianIGDKeperawatanAntonio{
			PengkajiaPersistem: rm.ToResponsePengkajianKeperawatan(persistem),
			Nyeri:              nyeri,
			Asuhan:             asuhan,
			Pemfisik:           pemFisik,
			AsesmenIGD:         asesmenIGD,
			User:               karyawan,
		}
	}

}

func (rm *RMEMapperImpl) ToResponsePengkajianKeperawatan(data rme.PengkajianPersistemIGD) (res dto.ResponsePengkajianPersistemIGD) {
	return dto.ResponsePengkajianPersistemIGD{
		TglMasuk:                       data.TglMasuk,
		TglKeluar:                      data.TglKeluar,
		JamCheckOut:                    data.JamCheckOut,
		JamCheckInRanap:                data.JamCheckInRanap,
		KdBagian:                       data.KdBagian,
		KdDpjp:                         data.KdBagian,
		AseskepSistemMerokok:           data.AseskepSistemMerokok,
		AseskepSistemMinumAlkohol:      data.AseskepSistemMinumAlkohol,
		AseskepSistemSpikologis:        data.AseskepSistemSpikologis,
		AseskepSistemGangguanJiwa:      data.AseskepSistemGangguanJiwa,
		AseskepSistemBunuhDiri:         data.AseskepSistemBunuhDiri,
		AseskepSistemTraumaPsikis:      data.AseskepSistemSpikologis,
		AseskepSistemHambatanSosial:    data.AseskepSistemHambatanSosial,
		AseskepSistemHambatanSpiritual: data.AseskepSistemHambatanSpiritual,
		AseskepSistemHambatanEkonomi:   data.AseskepSistemHambatanEkonomi,
		AseskepSistemPenghasilan:       data.AseskepSistemPenghasilan,
		AseskepSistemKultural:          data.AseskepSistemKultural,
		AseskepSistemAlatBantu:         data.AseskepSistemAlatBantu,
		AseskepSistemKebutuhanKhusus:   data.AseskepSistemKebutuhanKhusus,
	}
}
