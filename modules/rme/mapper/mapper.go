package mapper

import (
	"fmt"
	"hms_api/modules/diagnosa"
	"hms_api/modules/his"
	"hms_api/modules/igd"
	"hms_api/modules/kebidanan"
	"hms_api/modules/nyeri"
	dtoNutrisi "hms_api/modules/nyeri/dto"
	nyeriEntyty "hms_api/modules/nyeri/entity"
	"hms_api/modules/report"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"

	// rmeEntity "hms_api/modules/rme/entity"
	igdDto "hms_api/modules/igd/dto"
	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
	"strconv"

	"hms_api/modules/rme/entity"
	"hms_api/modules/soap"
)

type RMEMapperImpl struct {
	NyeriMapper nyeriEntyty.NyeriMapper
}

func NewLibMapperImple(yMapper nyeriEntyty.NyeriMapper) entity.RMEMapper {
	return &RMEMapperImpl{
		NyeriMapper: yMapper,
	}
}

func (rm *RMEMapperImpl) ToResponsePengkajianRANAP(pengkajian igd.PengkajianKeperawatan) (res soapDTO.ResponsePengkajianRawatInap) {
	return soapDTO.ResponsePengkajianRawatInap{
		TglPengkajian:            pengkajian.InsertDttm,
		KeluhanUtama:             pengkajian.KeluhanUtama,
		PenyakitSekarang:         pengkajian.RiwayatPenyakitSekarang,
		PenyakitDahulu:           pengkajian.RiwayatPenyakitDahulu,
		PenyakitKeluarga:         pengkajian.RiwayatPenyakitKeluarga,
		RiwayatPengobatanDirumah: pengkajian.RiwayatPengobatanDirumah,
		RiwayatAlergi:            pengkajian.RiwayatAlergi,
		ReaksiYangTimbul:         pengkajian.ReaksiAlergi,
	}
}

func (rm *RMEMapperImpl) ToMappingResponseDCairanPut(data []rme.DcairanOutPut) (res []dto.ResponseDcairanOutPut) {

	if len(data) < 1 {
		return make([]dto.ResponseDcairanOutPut, 0)
	}

	for _, V := range data {
		res = append(res, dto.ResponseDcairanOutPut{
			ID:         V.IDCairan,
			Noreg:      V.Noreg,
			Urine:      fmt.Sprintf("%s cc", strconv.Itoa(V.Urine)),
			NGT:        fmt.Sprintf("%s cc", strconv.Itoa(V.NGT)),
			WSD:        fmt.Sprintf("%s cc", strconv.Itoa(V.WSD)),
			Drain:      fmt.Sprintf("%s cc", strconv.Itoa(V.Drain)),
			Muntah:     fmt.Sprintf("%s cc", strconv.Itoa(V.Muntah)),
			Pendarahan: fmt.Sprintf("%s cc", strconv.Itoa(V.Pendarahan)),
			Total:      fmt.Sprintf("%s cc", strconv.Itoa(V.Urine+V.NGT+V.Drain+V.Muntah+V.WSD+V.Muntah)),
			Perawat:    V.Perawat,
			InsertDttm: V.InsertDttm,
			Pelayanan:  V.Pelayanan,
		})
	}

	return res
}

func (rm *RMEMapperImpl) ToMappingPengkajianPersistemRanap(pemFisik rme.DPemFisikModel, persistem igd.DpengkajianPersistem) (res dto.PenkajianPersistemRANAP) {
	return dto.PenkajianPersistemRANAP{
		DPemFIsik: pemFisik,
		Persistem: rm.ToMappingResponsePengkajianRANAP(igd.DpengkajianPersistem{
			IDPersistem:            persistem.IDPersistem,
			InsertDttm:             persistem.InsertDttm,
			KdBagian:               persistem.KdBagian,
			Pelayanan:              persistem.Pelayanan,
			Usia:                   persistem.Usia,
			NoReg:                  persistem.NoReg,
			NutrisiDanHidrasi:      toMessage(persistem.NutrisiDanHidrasi, ""),
			Eliminasi:              toMessage(persistem.Eliminasi, "Eliminasi BAK : \nUrea : \nCreatinin : \nEliminasi BAB : "),
			Aktivitas:              toMessage(persistem.Aktivitas, "Berjalan : \nSkor Aktivitas\n Mandi :\nBerpakaian\nMakan/Mandi : \nEliminasi : \nMobilisasi di TT :"),
			SistemKardioRepiratori: toMessage(persistem.SistemKardioRepiratori, "Kardiovaskuler :\nHilang Timbul : \nAKral :\nRespiratori : Batuk :\nSuara Napas :\nMerokok"),
			SistemPerfusiSecebral:  toMessage(persistem.SistemPerfusiSecebral, "Secebral :\n Sakit Kepala :\nPerubahan Status Mental :\nLemah anggota gerak :\n, Bicara : \nRiwayat Hipertensi : \n Pupil : \nRefleks Cahaya Kanan :..  / Kiri : ..\nKekuatan Otot : \n"),
			Thermoregulasi:         toMessage(persistem.Thermoregulasi, ""),
			SistemPerfusiPerifer:   toMessage(persistem.SistemPerfusiPerifer, ".... Lokasi : \nKesemutan/Kebas Lokasi :\nIkterus :"),
			SistemPencernaan:       toMessage(persistem.SistemPencernaan, "\nBising usus :"),
			Integumen:              toMessage(persistem.Integumen, "Dicubitus : \nYa, Lokasi :, Benjolan/Tumor di"),
			Proteksi:               toMessage(persistem.Proteksi, "Status Mental :"),
		}),
	}
}

func (rm *RMEMapperImpl) ToMappingResponsePengkajianRANAP(sistem igd.DpengkajianPersistem) (res igd.ResponseDPengkajianPersistenRANAP) {
	return igd.ResponseDPengkajianPersistenRANAP{
		NutrisiDanHidrasi:         toMessage(sistem.NutrisiDanHidrasi, ""),
		Eliminasi:                 toMessage(sistem.Eliminasi, "Eliminasi BAK : \nUrea : \nCreatinin : \nEliminasi BAB : "),
		Aktivitas:                 toMessage(sistem.Aktivitas, "Berjalan : \nSkor Aktivitas\n Mandi :\nBerpakaian\nMakan/Mandi : \nEliminasi : \nMobilisasi di TT :"),
		SistemKardioRepiratori:    toMessage(sistem.SistemKardioRepiratori, "Kardiovaskuler :\nHilang Timbul : \nAKral :\nRespiratori : Batuk :\nSuara Napas :\nMerokok"),
		SistemPerfusiSecebral:     toMessage(sistem.SistemPerfusiSecebral, "Secebral :\n Sakit Kepala :\nPerubahan Status Mental :\nLemah anggota gerak :\n, Bicara : \nRiwayat Hipertensi : \n Pupil : \nRefleks Cahaya Kanan :..  / Kiri : ..\nKekuatan Otot : \n"),
		Thermoregulasi:            toMessage(sistem.Thermoregulasi, ""),
		SistemPerfusiPerifer:      toMessage(sistem.SistemPerfusiPerifer, ".... Lokasi : \nKesemutan/Kebas Lokasi :\nIkterus :"),
		SistemPencernaan:          toMessage(sistem.SistemPencernaan, "\nBising usus :"),
		Integumen:                 toMessage(sistem.Integumen, "Dicubitus : \nYa, Lokasi :, Benjolan/Tumor di"),
		Proteksi:                  toMessage(sistem.Proteksi, "Dicubitus : \nYa, Lokasi :, Benjolan/Tumor di"),
		SeksualReproduksi:         toMessage(sistem.SeksualReproduksi, "Status Mental :"),
		PersistemKomunikasi:       toMessage(sistem.PersistemKomunikasi, "Hambatan Bahasa : \nPerlu Penerjemah :\nCara Belajar Yang disukai :\nBahasa Sehari Hari : \nPerlu Penerjemah : \n"),
		PersistemDataSpikologis:   toMessage(sistem.PersistemDataSpikologis, "Psikologis : \nHambatan Sosial :\nHambatan Ekonomi : \nHambatan Spirtual : \nResponse Emosi :\n"),
		PersistemNilaiKepercayaan: toMessage(sistem.PersistemNilaiKepercayaan, "Menjalankan Ibadah :\nPresepsi terhadap sakit :\nKunjungan Pemimpin Agama/Spiritual :\n Nilai/aturan Khusus dalam kepercayaan : \nJelaskan :"),
	}
}

func toMessage(data string, value string) (message string) {
	if data == "" {
		return value
	} else {
		return data
	}

}

func (rm *RMEMapperImpl) ToMappingResponseDCairanOutPut(data []rme.CairanIntake) (res []dto.ResponseCairanIntake) {

	if len(data) < 1 {
		return make([]dto.ResponseCairanIntake, 0)
	} else {
		for _, V := range data {
			res = append(res, dto.ResponseCairanIntake{
				ID:         V.IDCairan,
				Noreg:      V.Noreg,
				InsertDttm: V.InsertDttm,
				Ngt:        fmt.Sprintf("%s cc", strconv.Itoa(V.Ngt)),
				Minum:      fmt.Sprintf("%s cc", strconv.Itoa(V.Minum)),
				Infuse:     fmt.Sprintf("%s cc", strconv.Itoa(V.Infuse)),
				Total:      fmt.Sprintf("%s cc", strconv.Itoa(V.Minum+V.Ngt+V.Infuse)),
				Perawat:    V.Perawat,
				Pelayanan:  V.Pelayanan,
			})
		}

		return res
	}

}

func (rm *RMEMapperImpl) ToMappingPengkajianPersistemIGD(data rme.PengkajianPersistemIGD, vital soapDTO.TandaVitalIGDResponse) (res dto.ResponseAsesmenIGD) {
	return dto.ResponseAsesmenIGD{
		Noreg:                          data.Noreg,
		AseskepSistemMerokok:           data.AseskepSistemMerokok,
		AseskepSistemMinumAlkohol:      data.AseskepSistemMinumAlkohol,
		AseskepSistemSpikologis:        data.AseskepSistemSpikologis,
		AseskepSistemGangguanJiwa:      data.AseskepSistemGangguanJiwa,
		AseskepSistemBunuhDiri:         data.AseskepSistemBunuhDiri,
		AseskepSistemTraumaPsikis:      data.AseskepSistemTraumaPsikis,
		AseskepSistemHambatanSosial:    data.AseskepSistemHambatanSosial,
		AseskepSistemHambatanSpiritual: data.AseskepSistemHambatanSpiritual,
		AseskepSistemHambatanEkonomi:   data.AseskepSistemHambatanEkonomi,
		AseskepSistemPenghasilan:       data.AseskepSistemPenghasilan,
		AseskepSistemKultural:          data.AseskepSistemKultural,
		AseskepSistemAlatBantu:         data.AseskepSistemAlatBantu,
		AseskepSistemKebutuhanKhusus:   data.AseskepSistemKebutuhanKhusus,
	}
}

func (rm *RMEMapperImpl) ToMappingPengkajianKeperawatan(pengkajian igd.PengkajianKeperawatan, riwayat []kebidanan.DRiwayatPengobatanDirumah, karu rme.MutiaraPengajar, perawats rme.MutiaraPengajar, nutrisi dtoNutrisi.ToResponsePengkajianNutrisiDewasa, asuhan []rme.DasKepDiagnosaModelV2, fisik dto.ResponsePemeriksaanFisik, vital soap.DVitalSignIGDDokter, fungsional kebidanan.DPengkajianFungsional, nyeri nyeri.DasesmenUlangNyeri, resikoJatuh igd.DRisikoJatuh, persistem igd.DpengkajianPersistem, dpemFisik rme.DPemFisikModel, nyeris igdDto.ResponsePengkajianNyeriIGD) (res dto.ResponsePengkajianKeperawatan) {
	var riwayats = []kebidanan.DRiwayatPengobatanDirumah{}
	var asuhans = []rme.DasKepDiagnosaModelV2{}

	if len(riwayat) > 0 {
		riwayats = riwayat
	} else {
		riwayats = make([]kebidanan.DRiwayatPengobatanDirumah, 0)
	}

	if len(asuhan) > 0 {
		asuhans = asuhan
	} else {
		asuhans = make([]rme.DasKepDiagnosaModelV2, 0)
	}

	return dto.ResponsePengkajianKeperawatan{
		DPemFisik:       dpemFisik,
		Persistem:       rm.ToMappingResponsePengkajianRANAP(persistem),
		Pengkajian:      rm.ToResponsePengkajianRANAP(pengkajian),
		RiwayatPenyakit: riwayats,
		Karu:            karu,
		Perawat:         perawats,
		Nutrisi:         nutrisi,
		Asuhan:          asuhans,
		Fisik:           fisik,
		VitalSign:       vital,
		Funsional:       fungsional,
		Nyeri:           rm.NyeriMapper.ToMapperResponsePengkajianAwalNyeri(nyeri),
		Resikojatuh:     resikoJatuh,
	}
}

func (rm *RMEMapperImpl) ToMappingDataPemberianCairan(user user.ProfilePasien, cairan []rme.DPemberianCairan) (res dto.ReponseTerapiCairan) {
	var cairans = []rme.DPemberianCairan{}

	if len(cairan) > 0 {
		cairans = cairan
	} else {
		cairans = make([]rme.DPemberianCairan, 0)
	}

	return dto.ReponseTerapiCairan{
		Cairan:      rm.MappingCairan(cairans),
		UserProfile: user,
	}

}

func (rm *RMEMapperImpl) MappingCairan(carian []rme.DPemberianCairan) (res []dto.ResposeDPemberianCairan) {

	for _, V := range carian {
		res = append(res, dto.ResposeDPemberianCairan{
			InsertDttm:      V.InsertDttm,
			JumlahInfuse:    fmt.Sprintf("%s cc", strconv.Itoa(V.JumlahInfuse)),
			SisaInfuse:      fmt.Sprintf("%s cc", strconv.Itoa(V.SisaInfuse)),
			Noreg:           V.Noreg,
			NamaCairan:      V.NamaCairan,
			DosisMakro:      fmt.Sprintf("%s cc", strconv.Itoa(V.DosisMakro)),
			DosisMikro:      fmt.Sprintf("%s cc", strconv.Itoa(V.DosisMikro)),
			FlsKe:           V.FlsKe,
			ObatDitambahkan: V.ObatDitambahkan,
			Keterangan:      V.Keterangan,
			Perawat:         V.Perawat,
			Pelayanan:       V.Pelayanan,
		})
	}
	return res
}

func (rm *RMEMapperImpl) ToMappingKeluhanUtama(riwayatDahulu []rme.KeluhanUtama, alergi []rme.DAlergi, keluh rme.AsesemenDokterIGD, pengkajianKeperawatan soap.PengkajianAwalKeperawatan) (res dto.ResposeKeluhanUtamaIGD) {

	if pengkajianKeperawatan.AseskepKel != "" {
		data := rme.ResponseAsesemenDokterIGD{
			Noreg:             pengkajianKeperawatan.Noreg,
			AsesmedKeluhUtama: pengkajianKeperawatan.AseskepKel,
			AsesmedRwytSkrg:   pengkajianKeperawatan.AseskepRwytPnykt,
			RiwayatDahulu:     pengkajianKeperawatan.AseskepRwytPnyktDahulu,
		}

		if riwayatDahulu == nil {
			return dto.ResposeKeluhanUtamaIGD{
				KeluhanUtama:    data,
				RiwayatPenyakit: make([]rme.KeluhanUtama, 0),
				RiwayatKeluarga: alergi,
			}
		}

		if alergi == nil {
			return dto.ResposeKeluhanUtamaIGD{
				KeluhanUtama:    data,
				RiwayatPenyakit: riwayatDahulu,
				RiwayatKeluarga: make([]rme.DAlergi, 0),
			}
		}

		return dto.ResposeKeluhanUtamaIGD{
			KeluhanUtama:    data,
			RiwayatPenyakit: riwayatDahulu,
			RiwayatKeluarga: alergi,
		}
	} else {
		data := rme.ResponseAsesemenDokterIGD{
			Noreg:             keluh.Noreg,
			AsesmedKeluhUtama: keluh.AsesmedKeluhUtama,
			AsesmedRwytSkrg:   keluh.AsesmedRwytSkrg,
			RiwayatDahulu:     keluh.AsesmedRwytDahulu,
		}

		if riwayatDahulu == nil {
			return dto.ResposeKeluhanUtamaIGD{
				KeluhanUtama:    data,
				RiwayatPenyakit: make([]rme.KeluhanUtama, 0),
				RiwayatKeluarga: alergi,
			}
		}

		if alergi == nil {
			return dto.ResposeKeluhanUtamaIGD{
				KeluhanUtama:    data,
				RiwayatPenyakit: riwayatDahulu,
				RiwayatKeluarga: make([]rme.DAlergi, 0),
			}
		}

		return dto.ResposeKeluhanUtamaIGD{
			KeluhanUtama:    data,
			RiwayatPenyakit: riwayatDahulu,
			RiwayatKeluarga: alergi,
		}
	}

}

func (rm *RMEMapperImpl) ToMappingKeluhanUtamaV2(riwayatDahulu []rme.KeluhanUtama, alergi []rme.DAlergi, keluh rme.AsesemenDokterIGD) (res dto.ResposeKeluhanUtamaIGD) {

	data := rme.ResponseAsesemenDokterIGD{
		Noreg:             keluh.Noreg,
		AsesmedKeluhUtama: keluh.AsesmedKeluhUtama,
		AsesmedRwytSkrg:   keluh.AsesmedRwytSkrg,
		RiwayatDahulu:     keluh.AsesmedRwytDahulu,
	}

	if riwayatDahulu == nil {
		return dto.ResposeKeluhanUtamaIGD{
			KeluhanUtama:    data,
			RiwayatPenyakit: make([]rme.KeluhanUtama, 0),
			RiwayatKeluarga: alergi,
		}
	}

	if alergi == nil {
		return dto.ResposeKeluhanUtamaIGD{
			KeluhanUtama:    data,
			RiwayatPenyakit: riwayatDahulu,
			RiwayatKeluarga: make([]rme.DAlergi, 0),
		}
	}

	return dto.ResposeKeluhanUtamaIGD{
		KeluhanUtama:    data,
		RiwayatPenyakit: riwayatDahulu,
		RiwayatKeluarga: alergi,
	}

}

func (rm *RMEMapperImpl) ToDiagnosaResponse(data []rme.SDKIModel) (value []dto.DiagnosaKeperawatanResponse) {

	for _, V := range data {
		value = append(value, dto.DiagnosaKeperawatanResponse{
			Kode:        V.Kode,
			Judul:       V.Judul,
			MappingSlki: V.MappingSlki,
			MappingSiki: V.MappingSiki,
		})
	}

	return value
}

func (rm *RMEMapperImpl) ToCPPTResponse(data []rme.CpptModel) (value []dto.CPPTResponse) {

	for _, V := range data {

		var ppaString = ""
		// var profesi = ""

		if V.Namaperawat != "" {
			ppaString = V.Namaperawat
			// profesi = "Perawat"
		}

		if V.Namadokter != "" {
			ppaString = V.Namadokter
			// profesi = "Dokter"
		}

		value = append(value, dto.CPPTResponse{
			InstruksiPpa: V.InstruksiPpa,
			IdCppt:       V.IdCppt,
			InsertDttm:   V.InsertDttm,
			NamaUser:     ppaString,
			Bagian:       V.Namabagian,
			Tanggal:      (V.Tanggal[0:10]),
			Subjektif:    V.Subjektif,
			Objectif:     V.Objektif,
			Asesmen:      V.Asesmen,
			Plan:         V.Plan,
			Ppa:          ppaString,
		})
	}

	return value
}

func (rm *RMEMapperImpl) ToResponseSLKI(data rme.SLKIModel) (value dto.ResponseIntervensi) {
	return dto.ResponseIntervensi{
		Judul:     data.Judul,
		NoUrut:    data.NoUrut,
		Tanda:     data.Tanda,
		Menurun:   data.Menurun,
		Meningkat: data.Meningkat,
		Memburuk:  data.Memburuk,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikBangsal(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanFisikBangsal) {
	return dto.ResponsePemeriksaanFisikBangsal{
		Kepala:           data.Kepala,
		Leher:            data.Leher,
		Dada:             data.Dada,
		Abdomen:          data.Abdomen,
		Punggung:         data.Punggung,
		Genetalia:        data.Genetalia,
		Ekstremitas:      data.Ekstremitas,
		LainLain:         data.LainLain,
		PemeriksaanFisik: data.PemeriksaanFisik,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikBangsalDokter(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanFisikBangsal) {
	return dto.ResponsePemeriksaanFisikBangsal{
		Mata:             data.Mata,
		Tht:              data.Tht,
		Mulut:            data.Mulut,
		Gigi:             data.Gigi,
		Thyroid:          data.Thyroid,
		LeherLainnya:     data.LeherLainnya,
		DindingDada:      data.Dindingdada,
		SuaraJantung:     data.Jantung,
		SuaraParu:        data.Paru,
		DindingPerut:     data.Perut,
		Hati:             data.Hati,
		Lien:             data.Lien,
		PeristaltikUsus:  data.PeristatikUsus,
		AbdomenLainnya:   data.AbdomenLainnya,
		Kulit:            data.Kulit,
		Ginjal:           data.Ginjal,
		Kepala:           data.Kepala,
		Leher:            data.Leher,
		Dada:             data.Dada,
		Abdomen:          data.Abdomen,
		Punggung:         data.Punggung,
		Genetalia:        data.Genetalia,
		Ekstremitas:      data.Ekstremitas,
		LainLain:         data.LainLain,
		PemeriksaanFisik: data.PemeriksaanFisik,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikICUMapper(data rme.PemeriksaanFisikModel) (res dto.ResponseFisikICU) {
	return dto.ResponseFisikICU{
		GcsE:        data.GcsE,
		GcsM:        data.GcsM,
		GcsV:        data.GcsV,
		Kesadaran:   data.Kesadaran,
		Kepala:      data.Kepala,
		Rambut:      data.Rambut,
		Wajah:       data.Wajah,
		Mata:        data.Mata,
		Telinga:     data.Telinga,
		Hidung:      data.Hidung,
		Mulut:       data.Mulut,
		Gigi:        data.Gigi,
		Lidah:       data.Lidah,
		Tenggorokan: data.Tenggorokan,
		Leher:       data.Leher,
		Dada:        data.Dada,
		Respirasi:   data.Respirasi,
		Jantung:     data.Jantung,
		Integumen:   data.Integument,
		Ekstremitas: data.Ekstremitas,
		Pupil:       data.Pupil,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikAnakMapper(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisikAnak) {
	return dto.ResponsePemeriksaanFisikAnak{
		GcsE:              data.GcsE,
		GcsM:              data.GcsM,
		GcsV:              data.GcsV,
		Mata:              data.Mata,
		Telinga:           data.Telinga,
		Hidung:            data.Hidung,
		Mulut:             data.Mulut,
		LeherDanBahu:      data.Leher,
		Dada:              data.Dada,
		Abdomen:           data.Abdomen,
		Punggung:          data.Punggung,
		Peristaltik:       data.PeristatikUsus,
		NutrisiDanHidrasi: data.NutrisiDanHidrasi,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikAnak(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanAnak) {
	return dto.ResponsePemeriksaanAnak{
		Mata:         data.Mata,
		Mulut:        data.Mulut,
		Gigi:         data.Gigi,
		Thyroid:      data.Thyroid,
		Paru:         data.Paru,
		Jantung:      data.Jantung,
		DindingDada:  data.Dindingdada,
		Epigastrium:  data.DindingdadaRetEpigastrium,
		Supratermal:  data.DindingdadaRetSuprastermal,
		Retraksi:     data.Respirasi,
		Hepar:        data.Hepar,
		HeparDetail:  data.HeparDetail,
		Limpa:        data.Limpa,
		LimpaDetail:  data.LimpaDetail,
		Ginjal:       data.Ginjal,
		Ouf:          data.Ouf,
		TugorKulit:   data.TugorKulit,
		Genetalia:    data.Genetalia,
		GinjalDetail: data.Ginjal,
		Ekstremitas:  data.Ekstremitas,
	}
}

func (rm *RMEMapperImpl) ToResponseVitalSignBangsal(data rme.PemeriksaanFisikModel) (value dto.ResVitalSignbangsal) {
	return dto.ResVitalSignbangsal{
		KeadaanUmum: data.KeadaanUmum,
		Kesadaran:   data.Kesadaran,
	}
}

func (rm RMEMapperImpl) ToSkalaTriaseResponseFromModelTriase(data rme.SkalaTriaseModel) (res dto.ResSkalaNyeriTriaseIGD) {
	return dto.ResSkalaNyeriTriaseIGD{
		Nyeri:          data.SkalaNyeri,
		NyeriP:         data.SkalaNyeriP,
		NyeriQ:         data.SkalaNyeriQ,
		NyeriR:         data.SkalaNyeriR,
		NyeriS:         data.SkalaNyeriS,
		NyeriT:         data.SkalaNyeriT,
		FlaccWajah:     data.FlaccWajah,
		FlaccKaki:      data.FlaccKaki,
		FlaccAktifitas: data.FlaccAktifitas,
		FlaccMenangis:  data.FlaccMenangis,
		FlaccBersuara:  data.FlaccBersuara,
		FlaccTotal:     data.FlaccTotal,
		SkalaTriase:    data.SkalaTriase,
	}
}

func (rm RMEMapperImpl) ToSkalaTriaseResponse(data soap.DcpptSoapDokter) (res dto.ResSkalaNyeriTriaseIGD) {
	return dto.ResSkalaNyeriTriaseIGD{}
}

func (rm RMEMapperImpl) ToResponseTriaseTandaVitalIGD(data rme.PemeriksaanFisikModel) (value dto.ResTriaseTandaVitalIGD) {
	return dto.ResTriaseTandaVitalIGD{
		JalanNafas:  data.JalanNafas,
		PupilKiri:   data.PupilKiri,
		PupilKanan:  data.PupilKanan,
		CahayaKanan: data.CahayaKanan,
		CahayaKiri:  data.CahayaKiri,
		Akral:       data.Akral,
		GcsE:        data.GcsE,
		GcsM:        data.GcsM,
		GcsV:        data.GcsV,
		Gangguan:    data.Gangguan,
	}
}

func (rm RMEMapperImpl) ToResponseTriaseVitalSignIGD(data1 rme.VitalSignDokter, demfisik rme.DemfisikDokter) (res dto.ResTriaseTandaVitalIGD) {
	return dto.ResTriaseTandaVitalIGD{
		Td:             data1.Td,
		Pernafasan:     data1.Pernafasan,
		PupilKiri:      demfisik.PupilKiri,
		PupilKanan:     demfisik.PupilKanan,
		Gangguan:       demfisik.Gangguan,
		GangguanDetail: demfisik.GangguanDetail,
		JalanNafas:     demfisik.JalanNafas,
		Nadi:           data1.Nadi,
		Spo2:           data1.Spo2,
		CahayaKanan:    demfisik.CahayaKanan,
		CahayaKiri:     demfisik.CahayaKiri,
		Suhu:           data1.Suhu,
		Akral:          demfisik.Akral,
		GcsE:           demfisik.GcsE,
		GcsM:           demfisik.GcsM,
		GcsV:           demfisik.GcsV,
	}
}

func (rm *RMEMapperImpl) ToResponsePemeriksaanFisikIGD(data rme.PemeriksaanFisikModel) (value dto.ResponsePemeriksaanFisikGID) {
	return dto.ResponsePemeriksaanFisikGID{
		Mata:           data.Mata,
		Tht:            data.Tht,
		Mulut:          data.Mulut,
		Gigi:           data.Gigi,
		Thyroid:        data.Thyroid,
		LeherLainnya:   data.LeherLainnya,
		DindingDada:    data.Dada,
		SuaraJantung:   data.Jantung,
		SuaraParu:      data.Paru,
		DindingPerut:   data.Perut,
		Hati:           data.Hati,
		Lien:           data.Lien,
		PeristatikUsus: data.PeristatikUsus,
		AbdomenDetail:  data.AbdomenLainnya,
		Kulit:          data.Kulit,
		Ginjal:         data.Ginjal,
		Genetalia:      data.Genetalia,
		Superior:       data.EkstremitasSuperior,
		Inferior:       data.EkstremitasInferior,
	}
}

func (rm *RMEMapperImpl) ToResponseAsesmenKebidanan(data rme.AsesmedKeperawatanBidan,
	value []rme.RMEDaskep, sdki rme.SDKIModel, siki rme.SIKIModel) (res rme.GetResponseAsesmen) {
	if len(value) > 0 {
		return rme.GetResponseAsesmen{
			AsesmedKeperawatanBidan: data,
			Siki:                    siki,
			Sdki:                    sdki,
			Daskep:                  value,
		}
	} else {
		return rme.GetResponseAsesmen{
			AsesmedKeperawatanBidan: data,
			Siki:                    siki,
			Sdki:                    sdki,
			Daskep:                  []rme.RMEDaskep{},
		}
	}

}

// RESPONSE ASUHAN KEPERAWATAN
func (rm *RMEMapperImpl) ToResponseAsuhanKeperawatan(value []rme.RMEDaskep, sdki rme.SDKIModel, siki rme.SIKIModel) (res rme.GetResponseAsuhanKeperawatan) {
	if len(value) > 0 {
		return rme.GetResponseAsuhanKeperawatan{
			Siki:   siki,
			Sdki:   sdki,
			Daskep: value,
		}
	} else {
		return rme.GetResponseAsuhanKeperawatan{
			Siki:   siki,
			Sdki:   sdki,
			Daskep: []rme.RMEDaskep{},
		}
	}
}

// MAPPING HASIL RESIKO JATUH PASIEN
func (rm *RMEMapperImpl) ToHasilResikoJatuhPasienMapper(data rme.HasilResikoJatuhPasien) (res dto.ResikoJatuhResponse) {

	if data.Rj1 == "Ya" || data.Rj2 == "Ya" {
		return dto.ResikoJatuhResponse{
			IsShow:   true,
			Hasil:    data.Hasil,
			Tindakan: data.Tindakan,
		}
	}

	if data.Rj1 == "Tidak" && data.Rj2 == "Tidak" {
		return dto.ResikoJatuhResponse{
			IsShow:   false,
			Hasil:    data.Hasil,
			Tindakan: data.Tindakan,
		}
	}

	if data.Rj1 == "" && data.Rj2 == "" {
		return dto.ResikoJatuhResponse{
			IsShow:   false,
			Hasil:    data.Hasil,
			Tindakan: data.Tindakan,
		}
	}

	return dto.ResikoJatuhResponse{
		IsShow:   false,
		Hasil:    data.Hasil,
		Tindakan: data.Tindakan,
	}
}

// TO MAPPING DATA
func (rm *RMEMapperImpl) ToResponseSikiDeskripsiMapper(data []rme.SikiDeskripsiModel) (res []dto.ResponseDeskripsiSiki) {

	var siki dto.ResponseDeskripsiSiki
	var observasi []dto.SikiDeskripsi
	var terapetutik []dto.SikiDeskripsi
	var edukasi []dto.SikiDeskripsi
	var kolaborasi []dto.SikiDeskripsi

	for i := 0; i <= len(data)-1; i++ {

		for _, inter := range data[i].DeskripsiSikiModel {
			if inter.Kategori == "Observasi" {
				observasi = append(observasi, dto.SikiDeskripsi{
					NoUrut:     inter.NoUrut,
					KodeSiki:   inter.KodeSiki,
					Deskripsi:  inter.Deskripsi,
					IsSelected: false,
				})
			}

			if inter.Kategori == "Terapetutik" {
				terapetutik = append(terapetutik, dto.SikiDeskripsi{
					NoUrut:     inter.NoUrut,
					KodeSiki:   inter.KodeSiki,
					Deskripsi:  inter.Deskripsi,
					IsSelected: false,
				})
			}

			if inter.Kategori == "Edukasi" {
				edukasi = append(edukasi, dto.SikiDeskripsi{
					NoUrut:     inter.NoUrut,
					KodeSiki:   inter.KodeSiki,
					Deskripsi:  inter.Deskripsi,
					IsSelected: false,
				})
			}

			if inter.Kategori == "Kolaborasi" {
				kolaborasi = append(kolaborasi, dto.SikiDeskripsi{
					NoUrut:     inter.NoUrut,
					KodeSiki:   inter.KodeSiki,
					Deskripsi:  inter.Deskripsi,
					IsSelected: false,
				})
			}

			siki = dto.ResponseDeskripsiSiki{
				Kode:        data[i].Kode,
				Judul:       data[i].Judul,
				Observasi:   observasi,
				Terapetutik: terapetutik,
				Edukasi:     edukasi,
				Kolaborasi:  kolaborasi,
			}
		}

	}
	res = append(res, siki)

	return res

}

func (rm *RMEMapperImpl) ToNyeriICUResponseMapper(data rme.PengkajianNyeri) (res dto.AsesmenNyeiICU) {
	return dto.AsesmenNyeiICU{
		AseskepNyeri:          data.AseskepNyeri,
		AseskepLokasiNyeri:    data.AseskepLokasiNyeri,
		AseskepFrekuensiNyeri: data.AseskepFrekuensiNyeri,
		AseskepNyeriMenjalar:  data.AseskepNyeriMenjalar,
		AseskepKualitasNyeri:  data.AseskepKualitasNyeri,
	}
}

func (rm *RMEMapperImpl) ToMapperResponseKriteriaModel(data []rme.DKriteriaSLKIModel) (res []dto.ResponseKriteriaModel) {

	if len(data) == 0 {
		return make([]dto.ResponseKriteriaModel, 0)
	}

	if data == nil {
		return make([]dto.ResponseKriteriaModel, 0)
	}

	for _, V := range data {
		res = append(res, dto.ResponseKriteriaModel{
			NoUrut:       V.NoUrut,
			IsSelected:   false,
			IdKriteria:   V.IdKriteria,
			NamaKriteria: V.NamaKriteria,
			KodeSlki:     V.KodeSlki,
			Tanda:        V.Tanda,
			Kategori:     V.Kategori,
			Waktu:        0,
			Target:       0,
			Skor:         []int{1, 2, 3, 4, 5},
		})
	}

	return res

}

func (rm *RMEMapperImpl) ToMapperDeskripsiSiki(data []rme.DeskripsiSikiModel) (res []dto.SikiDeskripsi) {
	if len(data) == 0 {
		return make([]dto.SikiDeskripsi, 0)
	}

	if data == nil {
		return make([]dto.SikiDeskripsi, 0)
	}

	for _, V := range data {
		res = append(res, dto.SikiDeskripsi{
			IdSiki:     V.IdSiki,
			NoUrut:     V.NoUrut,
			KodeSiki:   V.KodeSiki,
			Deskripsi:  V.Deskripsi,
			IsSelected: false,
		})
	}

	return res
}

func (rm *RMEMapperImpl) ToSDKIResponseMapper(skdi rme.SDKIModel, newSLKI []dto.ResponseDataSLKI, siki []dto.ResponseDeskripsiSiki) (res dto.SikiResponse) {

	if len(siki) == 0 || siki == nil {
		return dto.SikiResponse{
			SDKIResponse: dto.SDKIResponse{
				Kode:     skdi.Kode,
				Judul:    skdi.Judul,
				Defenisi: skdi.Defenisi,
				Slki:     skdi.MappingSlki,
				Siki:     skdi.MappingSiki,
			},
			SIKIResponse: newSLKI,
			SikiResponse: make([]dto.ResponseDeskripsiSiki, 0),
		}
	}

	if len(newSLKI) == 0 || newSLKI == nil {

		return dto.SikiResponse{
			SDKIResponse: dto.SDKIResponse{
				Kode:     skdi.Kode,
				Judul:    skdi.Judul,
				Defenisi: skdi.Defenisi,
				Slki:     skdi.MappingSlki,
				Siki:     skdi.MappingSiki,
			},
			SIKIResponse: make([]dto.ResponseDataSLKI, 0),
			SikiResponse: siki,
		}
	}

	if len(newSLKI) == 0 && len(siki) == 0 {
		return dto.SikiResponse{
			SDKIResponse: dto.SDKIResponse{
				Kode:     skdi.Kode,
				Judul:    skdi.Judul,
				Defenisi: skdi.Defenisi,
				Slki:     skdi.MappingSlki,
				Siki:     skdi.MappingSiki,
			},
			SIKIResponse: make([]dto.ResponseDataSLKI, 0),
			SikiResponse: make([]dto.ResponseDeskripsiSiki, 0),
		}
	}

	return dto.SikiResponse{
		SDKIResponse: dto.SDKIResponse{
			Kode:     skdi.Kode,
			Judul:    skdi.Judul,
			Defenisi: skdi.Defenisi,
			Slki:     skdi.MappingSlki,
			Siki:     skdi.MappingSiki,
		},
		SIKIResponse: newSLKI,
		SikiResponse: siki,
	}
}

func (rm *RMEMapperImpl) ToMappingAlergi(dahulu rme.RiwayatPenyakitDahulu, alergi []rme.DAlergi) (res dto.ResponseAlergi) {
	var alergis []rme.DAlergi
	for _, V := range alergi {
		alergis = append(alergis, rme.DAlergi{
			Id:         V.Id,
			No:         V.No,
			Kelompok:   V.Kelompok,
			InsertDttm: V.InsertDttm[0:10],
			Alergi:     V.Alergi,
			InsertUser: V.InsertUser,
			KdBagian:   V.KdBagian,
		})
	}

	return dto.ResponseAlergi{
		Dahulu: dahulu,
		Alergi: alergis,
	}
}

func (rm RMEMapperImpl) ToReponseSkalaNyeri(data rme.PemeriksaanFisikModel) (res dto.ResSkalaNyeriTriaseIGD) {
	return dto.ResSkalaNyeriTriaseIGD{
		Nyeri:          data.SkalaNyeri,
		NyeriP:         data.SkalaNyeriP,
		NyeriQ:         data.SkalaNyeriQ,
		NyeriR:         data.SkalaNyeriR,
		NyeriS:         data.SkalaNyeriS,
		NyeriT:         data.SkalaNyeriT,
		FlaccWajah:     data.FlaccWajah,
		FlaccKaki:      data.FlaccKaki,
		FlaccAktifitas: data.FlaccAktifitas,
		FlaccMenangis:  data.FlaccMenangis,
		FlaccBersuara:  data.FlaccBersuara,
		FlaccTotal:     data.FlaccTotal,
		SkalaTriase:    data.SkalaTriase,
	}
}

func (rm RMEMapperImpl) ToPemeriksaanFisikIGDResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGD) {
	return dto.ResponseIGD{
		Kepala:              data.Kepala,
		Mata:                data.Mata,
		Tht:                 data.Tht,
		Mulut:               data.Mulut,
		Leher:               data.Leher,
		Dada:                data.Dada,
		Jantung:             data.Jantung,
		Paru:                data.Paru,
		Perut:               data.Perut,
		Hati:                data.Hati,
		Limpa:               data.Limpa,
		Ginjal:              data.Ginjal,
		AlatKelamin:         data.AlatKelamin,
		AnggotaGerak:        data.AnggotaGerak,
		Refleks:             data.Refleks,
		KekuatanOtot:        data.Otot,
		Kulit:               data.Kulit,
		KelenjarGetahBening: data.Thyroid,
		RtVt:                data.RtVt,
		JalanNafas:          data.JalanNafas,
		Sirkulasi:           data.Sirkulasi,
		GcsE:                data.GcsE,
		Kesadaran:           data.Kesadaran,
		GcsM:                data.GcsM,
		GcsV:                data.GcsV,
		Gigi:                data.Gigi,
		Abdomen:             data.Abdomen,
		Hidung:              data.Hidung,
		Telinga:             data.Telinga,
		Usus:                data.PeristatikUsus,
		EkstremitasSuperior: data.EkstremitasSuperior,
		EkstremitasInferior: data.EkstremitasInferior,
		Anus:                data.Anus,
		AbdomenLainnya:      data.AbdomenLainnya,
	}
}

func stringFunc(x string) string {
	returnValue := func() string {
		if x == "" {
			return "DBN"
		}
		return x
	}()
	return returnValue
}

func stringFuncAntonio(x string) string {
	returnValue := func() string {
		if x == "" {
			return "TAK"
		}
		return x
	}()
	return returnValue
}

func (rm RMEMapperImpl) ToPemeriksaanFisikIGDAntonioResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGDMethodist) {
	return dto.ResponseIGDMethodist{
		Kepala:              stringFuncAntonio(data.Kepala),
		Mata:                stringFuncAntonio(data.Mata),
		Tht:                 stringFuncAntonio(data.Tht),
		Mulut:               stringFuncAntonio(data.Mulut),
		Leher:               stringFuncAntonio(data.Leher),
		Dada:                stringFuncAntonio(data.Dada),
		Jantung:             stringFuncAntonio(data.Jantung),
		Paru:                stringFuncAntonio(data.Paru),
		Perut:               stringFuncAntonio(data.Perut),
		Hati:                stringFuncAntonio(data.Hati),
		Limpa:               stringFuncAntonio(data.Limpa),
		Ginjal:              stringFuncAntonio(data.Ginjal),
		AlatKelamin:         stringFuncAntonio(data.AlatKelamin),
		Gigi:                stringFuncAntonio(data.Gigi),
		KelenjarGetahBening: stringFuncAntonio(data.Thyroid),
		Usus:                stringFuncAntonio(data.PeristatikUsus),
		Anus:                stringFuncAntonio(data.Anus),
		AbdomenLainnya:      stringFuncAntonio(data.AbdomenLainnya),
		Superior:            stringFuncAntonio(data.EkstremitasSuperior),
		Inferior:            stringFuncAntonio(data.EkstremitasInferior),
	}
}

func (rm RMEMapperImpl) ToPemeriksaanFisikIGDMethodisResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseIGDMethodist) {
	return dto.ResponseIGDMethodist{
		Kepala:              stringFunc(data.Kepala),
		Mata:                stringFunc(data.Mata),
		Tht:                 stringFunc(data.Tht),
		Mulut:               stringFunc(data.Mulut),
		Leher:               stringFunc(data.Leher),
		Dada:                stringFunc(data.Dada),
		Jantung:             stringFunc(data.Jantung),
		Paru:                stringFunc(data.Paru),
		Perut:               stringFunc(data.Perut),
		Hati:                stringFunc(data.Hati),
		Limpa:               stringFunc(data.Limpa),
		Ginjal:              stringFunc(data.Ginjal),
		AlatKelamin:         stringFunc(data.AlatKelamin),
		Gigi:                stringFunc(data.Gigi),
		KelenjarGetahBening: stringFunc(data.Thyroid),
		Usus:                stringFunc(data.PeristatikUsus),
		Anus:                stringFunc(data.Anus),
		AbdomenLainnya:      stringFunc(data.AbdomenLainnya),
		Superior:            stringFunc(data.EkstremitasSuperior),
		Inferior:            stringFunc(data.EkstremitasInferior),
	}

}

func (rm RMEMapperImpl) ToPemeriksaanFisikAntonioResponse(data rme.PemeriksaanFisikDokterAntonio) (res dto.ResponsePemerikssanFisikAntonio) {

	if data.Keterangan == "" {
		return dto.ResponsePemerikssanFisikAntonio{
			PemeriksaanFisik: "1. Kepala dan Leher :\n\n\n2. Dada :\n\n\n3. Perut dan Pinggang :\n\n\n4. Anggota Gerak :\n\n\n5. Genetalia dan Anus :",
		}
	} else {
		return dto.ResponsePemerikssanFisikAntonio{
			PemeriksaanFisik: data.Keterangan,
		}
	}

}

func (rm RMEMapperImpl) ToPemeriksaanFisikPerinaResponse(data rme.PemeriksaanFisikModel) (res dto.ResponseFisikPerina) {
	return dto.ResponseFisikPerina{
		GcsE:              data.GcsE,
		GcsV:              data.GcsV,
		GcsM:              data.GcsM,
		Kesadaran:         data.Kesadaran,
		Abdomen:           data.Abdomen,
		Hidung:            data.Hidung,
		TonickNeck:        data.TonickNeck,
		Kelainan:          data.Kelainan,
		Telinga:           data.Telinga,
		Kepala:            data.Kepala,
		Wajah:             data.Wajah,
		Mulut:             data.Mulut,
		Refleks:           data.Refleks,
		LeherDanBahu:      data.Leher,
		Dada:              data.Dada,
		Punggung:          data.Punggung,
		Integumen:         data.Integument,
		Ekstremitas:       data.Ekstremitas,
		Genetalia:         data.Genetalia,
		Anus:              data.Anus,
		BabinSki:          data.Babinski,
		RefleksRooting:    data.RefleksRooting,
		RefleksSwallowing: data.RefleksSwallowing,
		RefleksSucking:    data.RefleksSucking,
		RefleksMoro:       data.RefleksMoro,
		RefleksGraps:      data.RefleksGraps,
	}
}

func (rm RMEMapperImpl) ToMapperReportIGDResponse(data1 rme.AsesemenMedisIGD, riwayatPenyakit []rme.KeluhanUtama, alergi []rme.DAlergi, tindakLanjut soap.RencanaTindakLanjutModel, diagnosa1 []soap.DiagnosaResponse, fisik dto.ResponseIGD, vital soap.DVitalSignIGDDokter, labor []his.ResHasilLaborTableLama, radiologi []his.RegHasilRadiologiTabelLama, userMapper user.ProfilePasien, diagnosabanding []diagnosa.DDiagnosaBanding) (res dto.ReportAsesmenIGD) {

	var newDianosa = []soap.DiagnosaResponse{}
	var labors = []his.ResHasilLaborTableLama{}
	var radiologis = []his.RegHasilRadiologiTabelLama{}
	var riwayatPenyakits []rme.KeluhanUtama
	var dignosaBandigs = []diagnosa.DDiagnosaBanding{}

	if len(diagnosa1) == 0 {
		newDianosa = make([]soap.DiagnosaResponse, 0)
	} else {
		newDianosa = diagnosa1
	}

	if len(riwayatPenyakit) == 0 {
		riwayatPenyakits = make([]rme.KeluhanUtama, 0)
	} else {
		riwayatPenyakits = riwayatPenyakit
	}

	if len(diagnosabanding) == 0 {
		dignosaBandigs = make([]diagnosa.DDiagnosaBanding, 0)
	} else {
		dignosaBandigs = diagnosabanding
	}

	if len(labor) == 0 {
		labors = make([]his.ResHasilLaborTableLama, 0)
	} else {
		labors = labor
	}

	if len(radiologi) == 0 {
		radiologis = make([]his.RegHasilRadiologiTabelLama, 0)
	} else {
		radiologis = radiologi
	}

	return dto.ReportAsesmenIGD{
		Pasien:                userMapper,
		AsesmenIGD:            data1,
		RiwayatPenyakitDahulu: riwayatPenyakits,
		Alergi:                alergi,
		RencanaTindakLanjut:   tindakLanjut,
		Diagnosa:              newDianosa,
		PemeriksaanFisik:      fisik,
		VitalSign:             vital,
		Labor:                 labors,
		Radiologi:             radiologis,
		DiagnosaBanding:       dignosaBandigs,
	}
}

// MAPPER DATA
func (rm RMEMapperImpl) ToMapperAsuhanKeperawatanResponse(daskep []rme.DasKepDiagnosaModel, perawat user.UserPerawat) (res dto.ResponseDaskepPerawat) {
	return dto.ResponseDaskepPerawat{
		Daskep:  daskep,
		Perawat: perawat,
	}
}

func (rm RMEMapperImpl) ToMapperResponseDVitalSignPerina(data rme.DVitalSignModel) (res dto.ResponseDVitalSignPerina) {
	return dto.ResponseDVitalSignPerina{
		Td:            data.Td,
		HR:            data.Hr,
		RR:            data.Pernafasan,
		Spo2:          data.Spo2,
		BB:            data.Bb,
		TB:            data.Tb,
		LingkarKepala: data.LingkarKepala,
		LingkarLengan: data.LingkarLengan,
		LingkarDada:   data.LingkarDada,
		LingkarPerut:  data.LingkarPerut,
		WarnaKulit:    data.WarnaKulit,
		KeadaanUmum:   data.KeadaanUmum,
	}
}

// MAPPING DATA
func (rm RMEMapperImpl) ToMappingKeperawatanBayiResponse(dokter []rme.DokterBayi, riwayat []rme.RiwayatKehamilanPerina, asesmen rme.AsesmenKeperawatanBayi, profil report.DProfilePasien) (res dto.ResponseAsesmenBayiResponse) {
	var dokters []rme.DokterBayi
	var riwayats []dto.ResponseRiwayatKelahiranLalu

	if len(dokter) == 0 || dokter == nil {
		dokters = make([]rme.DokterBayi, 0)
	} else {
		dokters = dokter
	}
	if len(riwayat) == 0 || riwayat == nil {
		riwayats = make([]dto.ResponseRiwayatKelahiranLalu, 0)
	} else {
		riwayats = rm.ToMapperResponseRiwayatKelahiranLalu(riwayat)
	}

	return dto.ResponseAsesmenBayiResponse{
		Dokter:           dokters,
		RiwayatKehamilan: riwayats,
		AsesemenBayi:     rm.ToResponseRiwayatKeperawatanBayi(asesmen, profil),
	}
}

func (rm RMEMapperImpl) ToMappingAsesmenNyeriResponse(userData user.Karyawan, nyeri dto.SkalaNyeriResponse) (res dto.ResponseSkalaNyeri) {
	return dto.ResponseSkalaNyeri{
		Karyawan:   userData,
		SkalaNyeri: nyeri,
	}
}

func (rm RMEMapperImpl) ToMapperPemeriksaanFisikReponse(data rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksaanFisik) {
	return dto.ResponsePemeriksaanFisik{
		AlatKelamin:                data.AlatKelamin,
		AnggotaGerak:               data.AnggotaGerak,
		Refleks:                    data.Refleks,
		Otot:                       data.Otot,
		RtVt:                       data.RtVt,
		GcsE:                       data.GcsE,
		GcsM:                       data.GcsM,
		GcsV:                       data.GcsV,
		PupilKiri:                  data.PupilKiri,
		PupilKanan:                 data.PupilKanan,
		Isokor:                     data.Isokor,
		IsokorDetail:               data.IsokorDetail,
		Anisokor:                   data.Anisokor,
		CahayaKanan:                data.CahayaKanan,
		CahayaKiri:                 data.CahayaKiri,
		Akral:                      data.Akral,
		Pupil:                      data.Pupil,
		Kesadaran:                  data.Kesadaran,
		Kepala:                     data.Kepala,
		Rambut:                     data.Rambut,
		Wajah:                      data.Wajah,
		KeadaanUmum:                data.KeadaanUmum,
		JalanNafas:                 data.JalanNafas,
		Sirkulasi:                  data.Sirkulasi,
		Gangguan:                   data.Gangguan,
		Kulit:                      data.Kulit,
		Abdomen:                    data.Abdomen,
		Ginjal:                     data.Ginjal,
		AbdomenLainnya:             data.AbdomenLainnya,
		PeristatikUsus:             data.PeristatikUsus,
		Thyroid:                    data.Thyroid,
		Hati:                       data.Hati,
		Paru:                       data.Paru,
		Mata:                       data.Mata,
		Tht:                        data.Tht,
		Telinga:                    data.Telinga,
		Hidung:                     data.Hidung,
		Mulut:                      data.Mulut,
		Gigi:                       data.Gigi,
		Lidah:                      data.Lidah,
		Tenggorokan:                data.Tenggorokan,
		Leher:                      data.Leher,
		Lien:                       data.Lien,
		LeherLainnya:               data.LeherLainnya,
		Dada:                       data.Dada,
		Respirasi:                  data.Respirasi,
		Perut:                      data.Perut,
		Jantung:                    data.Jantung,
		Integument:                 data.Integument,
		Ekstremitas:                data.Ekstremitas,
		EkstremitasSuperior:        data.EkstremitasSuperior,
		EkstremitasInferior:        data.EkstremitasInferior,
		KemampuanGenggam:           data.KemampuanGenggam,
		Genetalia:                  data.Genetalia,
		Anus:                       data.Anus,
		Punggung:                   data.Punggung,
		LainLain:                   data.LainLain,
		Dindingdada:                data.Dindingdada,
		DindingdadaRetEpigastrium:  data.DindingdadaRetEpigastrium,
		DindingdadaRetSuprastermal: data.DindingdadaRetSuprastermal,
		DindingdadaRetraksi:        data.DindingdadaRetraksi,
		Hepar:                      data.Hepar,
		HeparDetail:                data.HeparDetail,
		Limpa:                      data.Limpa,
		LimpaDetail:                data.LimpaDetail,
		Ouf:                        data.Ouf,
		TugorKulit:                 data.TugorKulit,
		PemeriksaanFisik:           data.PemeriksaanFisik,
		SkalaNyeri:                 data.SkalaNyeri,
		SkalaNyeriP:                data.SkalaNyeriP,
		SkalaNyeriQ:                data.SkalaNyeriQ,
		SkalaNyeriR:                data.SkalaNyeriR,
		SkalaNyeriS:                data.SkalaNyeriS,
		SkalaNyeriT:                data.SkalaNyeriT,
		FlaccWajah:                 data.FlaccWajah,
		FlaccKaki:                  data.FlaccKaki,
		FlaccAktifitas:             data.FlaccAktifitas,
		FlaccMenangis:              data.FlaccMenangis,
		FlaccBersuara:              data.FlaccBersuara,
		FlaccTotal:                 data.FlaccTotal,
		SkalaTriase:                data.SkalaTriase,
	}
}

func (rm RMEMapperImpl) ToMapperResponseRiwayatKelahiranLalu(data []rme.RiwayatKehamilanPerina) (res []dto.ResponseRiwayatKelahiranLalu) {
	var kelahiran []dto.ResponseRiwayatKelahiranLalu
	for _, V := range data {
		kelahiran = append(kelahiran, dto.ResponseRiwayatKelahiranLalu{
			KdRiwayat:            V.KdRiwayat,
			TahunPersalinan:      V.TahunPersalinan,
			NoRm:                 V.NoRm,
			TempatPersalinan:     V.TempatPersalinan,
			Noreg:                V.Noreg,
			UmurKehamilan:        V.UmurKehamilan,
			JenisPersalinan:      V.JenisPersalinan,
			Penolong:             V.Penolong,
			Penyulit:             V.Penyulit,
			Nifas:                V.Nifas,
			Jk:                   V.Jk,
			Bb:                   V.Bb,
			KeadaanSekarang:      V.KeadaanSekarang,
			KomplikasiHamil:      V.KomplikasiHamil,
			KomplikasiPersalinan: V.KomplikasiPersalinan,
		})
	}

	return kelahiran

}

func (rm RMEMapperImpl) ToResponseRiwayatKeperawatanBayi(data rme.AsesmenKeperawatanBayi, profil report.DProfilePasien) (res dto.ResponseAsesmenKeperawatanBayi) {
	var tanggals = ""

	if len(data.InsertDttm) > 10 {
		tanggals = data.InsertDttm[0:10]
	} else {
		tanggals = data.InsertDttm
	}

	return dto.ResponseAsesmenKeperawatanBayi{
		Tanggal:                           tanggals,
		AseskepBayiDokterObgyn:            data.AseskepBayiDokterObgyn,
		AseskepBayiDokterAnak:             data.AseskepBayiDokterAnak,
		AseskepBayiPekerjaanAyah:          profil.Pekerjaan,
		NamaAyah:                          profil.Namaayah,
		AseskepBayiPerkawinanAyahKe:       data.AseskepBayiPerkawinanAyahKe,
		AseskepUsiaKehamilan:              data.AseskepBayiPrenatalUsiaKehamilan,
		AseskepBayiNamaIbu:                profil.Namaibu,
		AseskepBayiPerkawinanIbuKe:        data.AseskepBayiPerkawinanIbuKe,
		AseskepBayiRwtPenyakitIbu:         data.AseskepBayiRwtPenyakitIbu,
		AseskepBayiNamaPjawab:             data.AseskepBayiNamaPjawab,
		AseskepBayiUsiaPjawab:             data.AseskepBayiUsiaPjawab,
		AseskepBayiPekerjaanPjawab:        data.AseskepBayiPekerjaanPjawab,
		AseskepBayiUsiaPersalinan:         data.AseskepBayiUsiaPersalinan,
		RiwayatPenyakitAyah:               data.AseskepBayiRwtPenyakitAyah,
		AseskepBayiTglLahir:               data.AseskepBayiTglLahir,
		AseskepBayiLahirDgn:               data.AseskepBayiLahirDgn,
		PekerjaanIbu:                      data.AseskepBayiPekerjaanIbu,
		AseskepBayiMenangis:               data.AseskepBayiMenangis,
		AseskepBayiJk:                     data.AseskepBayiJk,
		AseskepBayiKeterangan:             data.AseskepBayiKeterangan,
		PrenatalPersalinan:                data.AseskepBayiPrenatalPersalinanSebelumnya,
		AseskepBayiPrenatalUsiaKehamilan:  data.AseskepBayiPrenatalUsiaKehamilan,
		AseskepBayiPrenatalKomplikasi:     data.AseskepBayiPrenatalKomplikasi,
		AseskepBayiPrenatalHis:            data.AseskepBayiPrenatalHis,
		AseskepBayiPrenatalTtp:            data.AseskepBayiPrenatalTtp,
		AseskepBayiPrenatalKetuban:        data.AseskepBayiPrenatalKetuban,
		AseskepBayiPrenatalJam:            data.AseskepBayiPrenatalJam,
		AseskepBayiRwtUsiaPersalinan:      data.AseskepBayiRwtUsiaPersalinan,
		AseskepBayiRwtLahirDengan:         data.AseskepBayiRwtLahirDengan,
		AseskepBayiPrenatalKebiasaanIbu:   data.AseskepBayiPrenatalKebiasaanIbu,
		AseskepBayiRwtJenisKelamin:        data.AseskepBayiRwtJenisKelamin,
		AseskepBayiRwtTglKelahiranBayi:    data.AseskepBayiRwtTglKelahiranBayi,
		AseskepBayiRwtMenangis:            data.AseskepBayiRwtMenangis,
		AseskepBayiRwtKeterangan:          data.AseskepBayiRwtKeterangan,
		AseskepBayiPrenatalUsiaPersalinan: data.AseskepBayiPrenatalUsiaPersalinan,
		AseskepBayiNatalPersalinan:        data.AseskepBayiNatalPersalinan,
		AseskepBayiNatalKpd:               data.AseskepBayiNatalKpd,
		AseskepBayiNatalKeadaan:           data.AseskepBayiNatalKeadaan,
		AseskepBayiNatalTindakanDiberikan: data.AseskepBayiNatalTindakanDiberikan,
		AseskepBayiNatalPost:              data.AseskepBayiNatalPost,
		AseskepBayiNatalPresentasi:        data.AseskepBayiNatalPresentasi,
		AseskepBayiNatalDitolongOleh:      data.AseskepBayiNatalDitolongOleh,
		AseskepBayiNatalKetuban:           data.AseskepBayiNatalKetuban,
		AseskepBayiNatalLetak:             data.AseskepBayiNatalLetak,
		NatalVolume:                       data.AseskepBayiNatalVolume,
		AseskepBayiNatalLahirUmur:         data.AseskepBayiNatalLahirUmur,
		NatalKomplikasi:                   data.AseskepBayiNatalKomplikasi,
		AseskepBayiPrenatalJumlahHari:     data.AseskepBayiPrenatalJumlahHari,
		ObatObatanYangDikomsumsi:          data.AseskepBayiRwtPrenatalObatObatan,
		PendarahanPrenatal:                data.AseskepBayiPrenatalPendarahan,
	}
}

func (rm RMEMapperImpl) ToResponseRiwayatKeperawatanBayi2(data rme.AsesmenKeperawatanBayi) (res dto.ResponseAsesmenKeperawatanBayi) {
	var tanggals = ""

	if len(data.InsertDttm) > 10 {
		tanggals = data.InsertDttm[0:10]
	} else {
		tanggals = data.InsertDttm
	}

	return dto.ResponseAsesmenKeperawatanBayi{
		Tanggal:                           tanggals,
		AseskepBayiDokterObgyn:            data.AseskepBayiDokterObgyn,
		AseskepBayiDokterAnak:             data.AseskepBayiDokterAnak,
		AseskepBayiPekerjaanAyah:          data.AseskepBayiPekerjaanAyah,
		NamaAyah:                          data.AseskepBayiNamaAyah,
		AseskepBayiPerkawinanAyahKe:       data.AseskepBayiPerkawinanAyahKe,
		AseskepUsiaKehamilan:              data.AseskepBayiPrenatalUsiaKehamilan,
		AseskepBayiNamaIbu:                data.AseskepBayiNamaIbu,
		AseskepBayiPerkawinanIbuKe:        data.AseskepBayiPerkawinanIbuKe,
		AseskepBayiRwtPenyakitIbu:         data.AseskepBayiRwtPenyakitIbu,
		AseskepBayiNamaPjawab:             data.AseskepBayiNamaPjawab,
		AseskepBayiUsiaPjawab:             data.AseskepBayiUsiaPjawab,
		AseskepBayiPekerjaanPjawab:        data.AseskepBayiPekerjaanPjawab,
		AseskepBayiUsiaPersalinan:         data.AseskepBayiUsiaPersalinan,
		RiwayatPenyakitAyah:               data.AseskepBayiRwtPenyakitAyah,
		AseskepBayiTglLahir:               data.AseskepBayiTglLahir,
		AseskepBayiLahirDgn:               data.AseskepBayiLahirDgn,
		PekerjaanIbu:                      data.AseskepBayiPekerjaanIbu,
		AseskepBayiMenangis:               data.AseskepBayiMenangis,
		AseskepBayiJk:                     data.AseskepBayiJk,
		AseskepBayiKeterangan:             data.AseskepBayiKeterangan,
		PrenatalPersalinan:                data.AseskepBayiPrenatalPersalinanSebelumnya,
		AseskepBayiPrenatalUsiaKehamilan:  data.AseskepBayiPrenatalUsiaKehamilan,
		AseskepBayiPrenatalKomplikasi:     data.AseskepBayiPrenatalKomplikasi,
		AseskepBayiPrenatalHis:            data.AseskepBayiPrenatalHis,
		AseskepBayiPrenatalTtp:            data.AseskepBayiPrenatalTtp,
		AseskepBayiPrenatalKetuban:        data.AseskepBayiPrenatalKetuban,
		AseskepBayiPrenatalJam:            data.AseskepBayiPrenatalJam,
		AseskepBayiRwtUsiaPersalinan:      data.AseskepBayiRwtUsiaPersalinan,
		AseskepBayiRwtLahirDengan:         data.AseskepBayiRwtLahirDengan,
		AseskepBayiPrenatalKebiasaanIbu:   data.AseskepBayiPrenatalKebiasaanIbu,
		AseskepBayiRwtJenisKelamin:        data.AseskepBayiRwtJenisKelamin,
		AseskepBayiRwtTglKelahiranBayi:    data.AseskepBayiRwtTglKelahiranBayi,
		AseskepBayiRwtMenangis:            data.AseskepBayiRwtMenangis,
		AseskepBayiRwtKeterangan:          data.AseskepBayiRwtKeterangan,
		AseskepBayiPrenatalUsiaPersalinan: data.AseskepBayiPrenatalUsiaPersalinan,
		AseskepBayiNatalPersalinan:        data.AseskepBayiNatalPersalinan,
		AseskepBayiNatalKpd:               data.AseskepBayiNatalKpd,
		AseskepBayiNatalKeadaan:           data.AseskepBayiNatalKeadaan,
		AseskepBayiNatalTindakanDiberikan: data.AseskepBayiNatalTindakanDiberikan,
		AseskepBayiNatalPost:              data.AseskepBayiNatalPost,
		AseskepBayiNatalPresentasi:        data.AseskepBayiNatalPresentasi,
		AseskepBayiNatalDitolongOleh:      data.AseskepBayiNatalDitolongOleh,
		AseskepBayiNatalKetuban:           data.AseskepBayiNatalKetuban,
		AseskepBayiNatalLetak:             data.AseskepBayiNatalLetak,
		NatalVolume:                       data.AseskepBayiNatalVolume,
		AseskepBayiNatalLahirUmur:         data.AseskepBayiNatalLahirUmur,
		NatalKomplikasi:                   data.AseskepBayiNatalKomplikasi,
		AseskepBayiPrenatalJumlahHari:     data.AseskepBayiPrenatalJumlahHari,
		ObatObatanYangDikomsumsi:          data.AseskepBayiRwtPrenatalObatObatan,
		PendarahanPrenatal:                data.AseskepBayiPrenatalPendarahan,
	}
}

func (rm RMEMapperImpl) ToPengajarMapper(user user.MutiaraPengajar) (res dto.ReponseKaryawan) {
	return dto.ReponseKaryawan{
		Nama:         user.Nama,
		JenisKelamin: user.Jeniskelamin,
	}
}

func (rm RMEMapperImpl) ToResponseCairanKartuMapper(data []rme.DKartuCairanObatObatan) (res []dto.ResponseCairanKartu) {
	if len(data) == 0 {
		res = make([]dto.ResponseCairanKartu, 0)
	} else {
		for _, V := range data {

			res = append(res, dto.ResponseCairanKartu{
				IdKartu:           V.IdKartu,
				Jam:               V.InsertDttm,
				NamaCairan:        V.NamaCairan,
				CairanMasuk1:      V.CairanMasuk1,
				CairanMasuk2:      V.CairanMasuk2,
				CairanMasuk3:      V.CairanMasuk3,
				CairanMasukNgt:    V.CairanKeluarNgt,
				CairanKeluarUrine: V.CairanKeluarUrine,
				CairanKeluarNgt:   V.CairanKeluarNgt,
				DrainDll:          V.DrainDll,
				Keterangan:        V.Keterangan,
			})
		}
	}

	return res
}

func (rm RMEMapperImpl) ToResponseAsesmenAwalIGD(riwayat []soap.RiwayatPenyakit) (res dto.ResponseAsesmenAwalIGD) {
	var riwayats = []soap.RiwayatPenyakit{}

	if len(riwayat) == 0 {
		riwayats = make([]soap.RiwayatPenyakit, 0)
	} else {
		riwayats = riwayat
	}

	return dto.ResponseAsesmenAwalIGD{
		Riwayat: riwayats,
	}
}

func (rm RMEMapperImpl) TOResponseDKartuObservasi(data []rme.DKartuObservasi) (res []dto.ResponseDKartuObservasi) {

	if len(data) == 0 {
		res = make([]dto.ResponseDKartuObservasi, 0)
	} else {
		for _, V := range data {
			res = append(res, dto.ResponseDKartuObservasi{
				IdObservasi:  V.IdKartu,
				Jam:          V.InsertDttm,
				T:            V.T,
				N:            V.N,
				P:            V.P,
				S:            V.S,
				Cvp:          V.Cvp,
				PupilKi:      V.PupilKi,
				PupilKa:      V.PupilKa,
				RedaksiKi:    V.RedaksiKi,
				RedaksiKa:    V.RedaksiKa,
				AnggotaBadan: V.AnggotaBadan,
				Kesadaran:    V.Kesadaran,
				SputumWarna:  V.SputumWarna,
				IsiCup:       V.IsiCup,
				Ekg:          V.Ekg,
				Keterangan:   V.Keterangan,
			})
		}
	}

	return res

}

func (rm RMEMapperImpl) ToResponseReportAsesmenBayiMapper(data rme.AsesmenKeperawatanBayi, kelahiran []dto.ResponseRiwayatKelahiranLalu, apgarScore []dto.ApgarScoreResponse, downSocre dto.NeoNatusresponse, vitalSign dto.ResponseDVitalSignPerina, fisik dto.ResponseFisikPerina, user user.MutiaraPengajar) (res dto.ResponseReportAsesmenBayiResponse) {
	var kelahirans []dto.ResponseRiwayatKelahiranLalu
	var apgarScores []dto.ApgarScoreResponse

	if len(kelahiran) == 0 {
		kelahirans = make([]dto.ResponseRiwayatKelahiranLalu, 0)
	} else {
		kelahirans = kelahiran
	}

	if len(apgarScore) == 0 {
		apgarScores = make([]dto.ApgarScoreResponse, 0)
	} else {
		apgarScores = apgarScore
	}

	return dto.ResponseReportAsesmenBayiResponse{
		AsesemenBayi:    rm.ToResponseRiwayatKeperawatanBayi2(data),
		KelahiranLalu:   kelahirans,
		ApgarScore:      apgarScores,
		DownScore:       downSocre,
		VitalSignPerina: vitalSign,
		FisikPerina:     fisik,
		User:            rm.ToPengajarMapper(user),
	}
}

func (rm RMEMapperImpl) ToResponseTindakLajutMapper(data rme.TindakLajutPerina) (res dto.ResponseTindakLajutPerina) {
	var tindakLajut = ""

	if data.AsesmedTindakLanjut == "" {
		tindakLajut = "Minum ASI / Susu :\nObat / Tindakan Khusus :\n\nPulang :\n		Umur :\n		Berat Badan:\n		Keadaan Bayi:		"
	} else {
		tindakLajut = data.AsesmedTindakLanjut
	}
	return dto.ResponseTindakLajutPerina{
		AsesmenTindakanOperasi: data.AsesmenTindakanOperasi,
		AsesmedTindakLanjut:    tindakLajut,
		AsesmenTglKontrolUlang: data.AsesmenTglKontrolUlang,
	}
}

func (rm RMEMapperImpl) ToMapperIdentitasBayiRes(vitalSign rme.DVitalSignModel) (res dto.VitalSignPerina) {
	return dto.VitalSignPerina{
		BeratLahir:   vitalSign.Bb,
		WarnatKulit:  vitalSign.WarnaKulit,
		PanjangBadan: vitalSign.Td,
	}
}

func (rm RMEMapperImpl) ToMapperIdentitasBayiMapper(data dto.ResponseIdentitasBayi, pasien report.DProfilePasien, vitalSign rme.DVitalSignModel) (res dto.IdentitasBayiRes) {
	return dto.IdentitasBayiRes{
		ResponseIdentitasBayi: data,
		ProfilPasien:          pasien,
		VitalSign:             rm.ToMapperIdentitasBayiRes(vitalSign),
	}
}

func (rm RMEMapperImpl) ToMapperResponseRingkasanMasukPasien(pasien report.DProfilePasien) (res dto.ResponseRingkasanMasukPasien) {

	return dto.ResponseRingkasanMasukPasien{
		Pasien: pasien,
	}
}

func (rm RMEMapperImpl) ToMapperEarlyWarningSystemResponse(data []rme.DearlyWarningSystem) (res []dto.ResponseEarlyWarningSystem) {
	var kelahiran []dto.ResponseEarlyWarningSystem
	for _, V := range data {
		kelahiran = append(kelahiran, dto.ResponseEarlyWarningSystem{
			IdEws:            V.IdEws,
			Waktu:            V.InsertDttm,
			TingkatKesadaran: V.TingkatKesadaran,
			Noreg:            V.Noreg,
			Td:               V.Td,
			Nadi:             V.Nadi,
			Pernapasan:       V.Pernapasan,
			Keterangan:       V.Keterangan,
			Td2:              V.Td2,
			TotalSkor:        V.TotalSkor,
			ReaksiOtot:       V.ReaksiOtot,
			Suhu:             V.Suhu,
			Spo2:             V.Spo2,
			Crt:              V.Crt,
			SkalaNyeri:       V.SkalaNyeri,
			Karyawan:         V.MutiaraPengajar,
		})
	}

	return kelahiran
}

func (rm RMEMapperImpl) ToMapperVitalSignICUResponse(data rme.VitalSignDokter) (res dto.ResponseVitalSignICU) {
	return dto.ResponseVitalSignICU{
		TekananDarah: data.Td,
		Nadi:         data.Nadi,
		BeratBadan:   data.Bb,
		Suhu:         data.Suhu,
		Pernapasan:   data.Pernafasan,
		TinggiBadan:  data.Tb,
		Spo2:         data.Spo2,
	}
}
