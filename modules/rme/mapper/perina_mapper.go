package mapper

import (
	"hms_api/modules/report"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"strconv"
)

func (rm *RMEMapperImpl) ToMapperResponseNeonatus(data rme.DownScoreNeoNatusModel) (value dto.NeoNatusresponse) {
	return dto.NeoNatusresponse{
		FrekwensiNafas: data.FrekwensiNafas,
		Sianosis:       data.Sianosis,
		Retraksi:       data.Retraksi,
		AirEntry:       data.AirEntry,
		Merintih:       data.Merintih,
		Total:          data.Total,
	}
}

func (rm *RMEMapperImpl) ToMapperScoreNeoNatus(data []rme.DApgarScoreNeoNatus) (value []dto.ApgarScoreResponse) {

	for _, V := range data {
		value = append(value, dto.ApgarScoreResponse{
			IdAgpar:    V.IdAgpar,
			Waktu:      V.Waktu,
			DJantung:   V.DJantung,
			Otot:       V.Otot,
			UNafas:     V.UNafas,
			Refleksi:   V.Refleksi,
			WarnaKulit: V.WarnaKulit,
			Total:      V.Total,
		})
	}

	return value
}

func (rm *RMEMapperImpl) ToMapperSkalaNyeriDewasa(data rme.AsesmenSkalaNyeri) (response dto.SkalaNyeriResponse) {
	return dto.SkalaNyeriResponse{
		SkalaNyeri:     data.AseskepSkalaNyeri,
		FrekuensiNyeri: data.AseskepFrekuensiNyeri,
		LokasiNyeri:    data.AseskepLokasiNyeri,
		KualitasNyeri:  data.AseskepKualitasNyeri,
		Menjalar:       data.AseskepNyeriMenjalar,
	}
}

func (rm *RMEMapperImpl) ToMapperSingleScoreNeoNatus(data rme.DApgarScoreNeoNatus) (value dto.ApgarScoreResponse) {
	return dto.ApgarScoreResponse{
		Waktu:      data.Waktu,
		DJantung:   data.DJantung,
		UNafas:     data.UNafas,
		Otot:       data.Otot,
		Refleksi:   data.Refleksi,
		WarnaKulit: data.WarnaKulit,
		Total:      data.WarnaKulit,
	}
}

func (rm *RMEMapperImpl) ToIdentitasBayiMapper(data rme.AsesmenKeperawatanBayi, profil report.DProfilePasien, noReg string) (res dto.IdentitasBayi) {
	// profil.Umurbln
	var tglLahir = ""
	var tglMasuk = ""

	if len(profil.Tgllahir) > 10 {
		tglLahir = profil.Tgllahir[0:10]
	} else {
		tglLahir = profil.Tgllahir
	}

	if len(data.TglMasuk) > 10 {
		tglMasuk = data.TglMasuk[0:10]
	} else {
		tglMasuk = data.TglMasuk
	}

	return dto.IdentitasBayi{
		NamaBayi:      profil.Firstname,
		TanggalLahir:  tglLahir,
		NomorRegister: noReg,
		NoRM:          profil.Id,
		Umur:          strconv.Itoa(profil.Umurbln) + " Tahun,  " + strconv.Itoa(profil.Umurbln) + " Bulan",
		JenisKelamin:  profil.Jeniskelamin,
		TanggalMasuk:  tglMasuk,
		DokterAnak:    data.AseskepBayiDokterAnak,
		NamaIbu:       data.AseskepBayiNamaIbu,
		Agama:         profil.Agama,
		Alamat:        profil.Alamat,
		DokterObgyn:   data.AseskepBayiDokterObgyn,
	}
}

func (rm *RMEMapperImpl) TOMapperResponseResumeMedisPerinatologi(data rme.AsesmenKeperawatanBayi, profil report.DProfilePasien, noReg string) (res dto.ResponseResumeMedisPerinatologi) {
	return dto.ResponseResumeMedisPerinatologi{
		IdentitasBayi: rm.ToIdentitasBayiMapper(data, profil, noReg),
	}
}

func (rm *RMEMapperImpl) ToMapperIdentitasBayi(data rme.KakiKananBayi) (res dto.ResponseIdentitasBayi) {
	return dto.ResponseIdentitasBayi{
		KakiKananBayi:            data.AseskepImageSidikKakiKananBayi,
		KakiKiriBayi:             data.AseskepImageSidikKakiKiriBayi,
		TanganKiriIbu:            data.AseskepImageIbuJariKiriIbu,
		AseskepTtdPenentuJk:      data.AseskepTtdPenentuJk,
		AseskepNamaWali:          data.AseskepNamaWali,
		AseskepTtdWali:           data.AseskepTtdWali,
		AseskepPemberiGelangBayi: data.AseskepPemberiGelangBayi,
		AseskepNamaPenentuJk:     data.AseskepNamaPenentuJk,
		JamKelahiranBayi:         data.AseskepJamKelahiran,
	}

}
