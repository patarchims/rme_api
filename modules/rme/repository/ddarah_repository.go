package repository

import (
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"
)

func (sr *rmeRepository) OnGetDTransfusiDarahRepository(req dto.ReqOnGetTransfusiDarah, kdBagian string) (res []rme.DTransfusiDarah, err error) {
	errs := sr.DB.Where("noreg=? AND kd_bagian=?", req.NoReg, kdBagian).Preload("Reaksi").Preload("Perawat").Preload("Pelayanan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdateEdukasiTerintegrasiRepository(idEdukasi int, data rme.DEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, err error) {
	errs := sr.DB.Where("id_edukasi=?", idEdukasi).Updates(&data)

	if errs != nil {
		return data, errs.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnGetVerifikasiTransfusiDarahByIDTransfusiRepository(idTransfusi int) (res rme.DverifyTransfusiDarah, err error) {
	errs := sr.DB.Where("id_transfusi=?", idTransfusi).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdateDataVerifikasiTransfusiDarahByIDAndNoregRepository(data rme.DverifyTransfusiDarah, idTransfusi int, noReg string) (res rme.DverifyTransfusiDarah, err error) {

	result := sr.DB.Where(rme.DverifyTransfusiDarah{IDTransfusi: idTransfusi, Noreg: noReg}).Updates(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil

}

func (sr *rmeRepository) OnSaveVerifikasiTransfusiDrahRepository(data rme.DverifyTransfusiDarah) (res rme.DverifyTransfusiDarah, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnChangedTransfusiDarahRepository(req dto.ReqOnChangedTransfusiDarah) (res rme.DTransfusiDarah, err error) {
	data := rme.DTransfusiDarah{
		TglKadaluwarsa: req.TglKadaluarsa,
		JenisDarah:     req.JenisDarah,
		JamPemberian:   req.JamPemberian,
		NoKantung:      req.NoKantung,
		GolDarah:       req.GolDarah,
	}

	result := sr.DB.Where(rme.DTransfusiDarah{IDTransfusi: req.IDTransfusi}).Updates(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) InsertDtransfusiDarahRepository(req dto.ReqTransfusiDarah, kdbagian string, userID string) (res rme.DTransfusiDarah, err error) {
	data := rme.DTransfusiDarah{
		InsertDttm:     time.Now(),
		Noreg:          req.NoReg,
		UserID:         userID,
		KdBagian:       kdbagian,
		TglKadaluwarsa: req.TglKadaluarsa,
		JenisDarah:     req.JenisDarah,
		JamPemberian:   req.JamPemberian,
		NoKantung:      req.NoKantung,
		GolDarah:       req.GolDarah,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnGetReaksiByIDTransfusiDarahRepository(ID int) (res []rme.DReaksiTransfusiDarah, err error) {
	errs := sr.DB.Where("id_transfusi=?", ID).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnViewReaksiDarahByIDTranfusiDarah(ID int) (res []rme.DReaksiTransfusiModelDarah, err error) {
	errs := sr.DB.Where("id_transfusi=?", ID).Preload("Perawat").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) GetReaksiTransfusiDarahRepository(noreg string, reaksi string) (res rme.DReaksiTransfusiDarah, err error) {

	errs := sr.DB.Where("noreg=? AND reaksi=?", noreg, reaksi).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil

}

func (sr *rmeRepository) OnDeleteReaksiTransfusiDarahRepositoru(idReaksi int) (err error) {
	errs := sr.DB.Where("id_reaksi=?", idReaksi).Delete(&rme.DReaksiTransfusiDarah{})

	if errs != nil {
		return errs.Error
	}

	return nil
}
func (sr *rmeRepository) OnDeleteTransfusiDarahRepositoru(ID int) (err error) {
	errs := sr.DB.Where("id_transfusi=?", ID).Delete(&rme.DTransfusiDarah{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

func (sr *rmeRepository) OnSaveReaksiTransufiDarahRepository(userID string, req dto.ReqReaksiTranfusiDarah) (res rme.DReaksiTransfusiDarah, err error) {
	// insert_ddtm
	// insert_ddtm
	times := time.Now()

	data := rme.DReaksiTransfusiDarah{
		InsertDttm:  times.Format("2006-01-02 15:04:05"),
		IDTransfusi: req.IDTransfusi,
		Reaksi:      req.Reaksi,
		Keterangan:  req.Keterangan,
		Noreg:       req.Noreg,
		UserId:      userID,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}
