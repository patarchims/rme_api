package repository

import (
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"
)

func (sr *rmeRepository) InsertDpelaksanaKeperawatan(req dto.ReqOnSaveActionCpptFiberHandler, kdbagian string, userID string) (res rme.DPelaksanaKeperawatan, err error) {
	times := time.Now()

	data := rme.DPelaksanaKeperawatan{
		IdCppt: req.IdCppt, InsertDttm: times.Format("2006-01-02 15:04:05"), UserId: userID, Deskripsi: req.Deskripsi,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) UpdateDPelaksanaanKeperawatanRepository(req dto.ReqOnUpdateActionCpptFiberHandler) (res rme.DPelaksanaKeperawatan, err error) {
	errs := sr.DB.Where(rme.DPelaksanaKeperawatan{Id: req.IdPelaksanaan}).Updates(
		&rme.DPelaksanaKeperawatan{
			Deskripsi: req.Deskripsi,
		}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}
