package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"

	"gorm.io/gorm"
)

func (sr *rmeRepository) InsertAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (res rme.AsesmedKeperawatanBidan, err error) {
	times := time.Now()

	data := rme.AsesmedKeperawatanBidan{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: req.Asesemen.Noreg, AseskepPerolehanInfo: req.Asesemen.AseskepPerolehanInfo, AseskepCaraMasuk: req.Asesemen.AseskepAsalMasuk, AseskepCaraMasukDetail: req.Asesemen.AseskepAsalMasukDetail, AseskepAsalMasuk: req.Asesemen.AseskepAsalMasuk, AseskepAsalMasukDetail: req.Asesemen.AseskepAsalMasukDetail, AseskepBb: req.Asesemen.AseskepBb, AseskepTb: req.Asesemen.AseskepTb, AseskepRwytPnykt: req.Asesemen.AseskepRwytPnykt, AseskepRwytObatDetail: req.Asesemen.AseskepRwytObatDetail, AseskepAsesFungsional: req.Asesemen.AseskepAsesFungsional, AseskepRj1: req.Asesemen.AseskepRj1, AseskepRj2: req.Asesemen.AseskepRj2, AseskepHslKajiRj: req.Asesemen.AseskepHslKajiRj, AseskepHslKajiRjTind: req.Asesemen.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.Asesemen.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.Asesemen.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.Asesemen.AseskepLamaNyeri, AseskepNyeriMenjalar: req.Asesemen.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.Asesemen.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.Asesemen.AseskepKualitasNyeri, AseskepNyeriPemicu: req.Asesemen.AseskepNyeriPemicu, AseskepNyeriPengurang: req.Asesemen.AseskepNyeriPengurang, AseskepKehamilan: req.Asesemen.AseskepKehamilan, AseskepKehamilanGravida: req.Asesemen.AseskepKehamilanGravida, AseskepKehamilanPara: req.Asesemen.AseskepKehamilanPara, AseskepKehamilanAbortus: req.Asesemen.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.Asesemen.AseskepKehamilanHpht, AseskepKehamilanTtp: req.Asesemen.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.Asesemen.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.Asesemen.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.Asesemen.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.Asesemen.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.Asesemen.AseskepKehamilanDjj, AseskepKehamilanVt: req.Asesemen.AseskepKehamilanVt, AseskepDekubitus1: req.Asesemen.AseskepDekubitus1, AseskepDekubitus2: req.Asesemen.AseskepDekubitus2, AseskepDekubitus3: req.Asesemen.AseskepDekubitus3, AseskepDekubitus4: req.Asesemen.AseskepDekubitus4, AseskepDekubitusAnak: req.Asesemen.AseskepDekubitusAnak, AseskepPulangKondisi: req.Asesemen.AseskepPulangKondisi, AseskepPulangTransportasi: req.Asesemen.AseskepPulangTransportasi, AseskepPulangPendidikan: req.Asesemen.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.Asesemen.AseskepPulangPendidikanDetail,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (res rme.AsesmedKeperawatanBidan, err error) {

	errs := sr.DB.Where(rme.AsesmedKeperawatanBidan{Noreg: req.Asesemen.Noreg, KdBagian: kdBagian}).Updates(
		&rme.AsesmedKeperawatanBidan{
			KdBagian: kdBagian, Noreg: req.Asesemen.Noreg, AseskepPerolehanInfo: req.Asesemen.AseskepPerolehanInfo, AseskepCaraMasuk: req.Asesemen.AseskepCaraMasuk, AseskepCaraMasukDetail: req.Asesemen.AseskepAsalMasukDetail,
			AseskepAsalMasuk: req.Asesemen.AseskepAsalMasuk, AseskepAsalMasukDetail: req.Asesemen.AseskepAsalMasukDetail, AseskepBb: req.Asesemen.AseskepBb, AseskepTb: req.Asesemen.AseskepTb, AseskepRwytPnykt: req.Asesemen.AseskepRwytPnykt,
			AseskepRwytObatDetail: req.Asesemen.AseskepRwytObatDetail, AseskepAsesFungsional: req.Asesemen.AseskepAsesFungsional, AseskepRj1: req.Asesemen.AseskepRj1, AseskepRj2: req.Asesemen.AseskepRj2, AseskepHslKajiRj: req.Asesemen.AseskepHslKajiRj, AseskepHslKajiRjTind: req.Asesemen.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.Asesemen.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.Asesemen.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.Asesemen.AseskepLamaNyeri, AseskepNyeriMenjalar: req.Asesemen.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.Asesemen.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.Asesemen.AseskepKualitasNyeri, AseskepNyeriPemicu: req.Asesemen.AseskepNyeriPemicu, AseskepNyeriPengurang: req.Asesemen.AseskepNyeriPengurang, AseskepKehamilan: req.Asesemen.AseskepKehamilan, AseskepKehamilanGravida: req.Asesemen.AseskepKehamilanGravida, AseskepKehamilanPara: req.Asesemen.AseskepKehamilanPara, AseskepKehamilanAbortus: req.Asesemen.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.Asesemen.AseskepKehamilanHpht, AseskepKehamilanTtp: req.Asesemen.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.Asesemen.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.Asesemen.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.Asesemen.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.Asesemen.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.Asesemen.AseskepKehamilanDjj, AseskepKehamilanVt: req.Asesemen.AseskepKehamilanVt, AseskepDekubitus1: req.Asesemen.AseskepDekubitus1, AseskepDekubitus2: req.Asesemen.AseskepDekubitus2, AseskepDekubitus3: req.Asesemen.AseskepDekubitus3, AseskepDekubitus4: req.Asesemen.AseskepDekubitus4, AseskepDekubitusAnak: req.Asesemen.AseskepDekubitusAnak, AseskepPulangKondisi: req.Asesemen.AseskepPulangKondisi, AseskepPulangTransportasi: req.Asesemen.AseskepPulangTransportasi, AseskepPulangPendidikan: req.Asesemen.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.Asesemen.AseskepPulangPendidikanDetail,
		}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) CariCPPTRepository(noRm string) (res []rme.CpptModel, err error) {

	query := `SELECT dsp.*, ke.namadokter AS namadokter,  ke2.nama AS namaperawat, kp.bagian AS namabagian, dp.id FROM vicore_rme.dcppt AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id LEFT JOIN his.ktaripdokter AS ke ON dsp.ppa_finger_ttd=ke.iddokter LEFT JOIN mutiara.pengajar AS ke2 ON dsp.ppa_finger_ttd=ke2.id LEFT JOIN vicore_lib.kpelayanan AS kp ON dsp.kd_bagian=kp.kd_bag where dp.id=? AND dsp.subjektif!='' ORDER BY dsp.insert_dttm DESC`

	result := sr.DB.Raw(query, noRm).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteCPPTRepository(nomor int) (err error) {
	errs := sr.DB.Where("id_cppt=?", nomor).Delete(&rme.DccptModel{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

func (sr *rmeRepository) OnGetEdukasiTerintegrasi(noRm string) (res []rme.DEdukasiTerintegrasi, err error) {
	errs := sr.DB.Where("no_rm=?", noRm).Preload("Perawat").Preload("Pelayanan").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveDataEdukasiTerintegrasiRepository(userID string, kdBagian string, req dto.ReqOnSaveDataEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, err error) {
	times := time.Now()

	data := rme.DEdukasiTerintegrasi{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		InsertUser:        userID,
		KdBagian:          kdBagian,
		Metode:            req.Metode,
		Informasi:         req.Informasi,
		PemberiInformasi:  req.PemberiInformasi,
		PenerimaInformasi: req.PenerimaInformasi,
		Evaluasi:          req.Evaluasi,
		NoReg:             req.NoReg,
		NoRm:              req.NoRm,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

// =================================== PEMERIKSAAN FISIK
func (sr *rmeRepository) InsertPemeriksaanFisikRepository(userID string, kdBagian string, req dto.ReqPemeriksaanFisikIGD) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	data := rme.PemeriksaanFisikModel{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		Kategori:     req.Person,
		InsertDevice: req.DeviceID, InsertUserId: userID, KetPerson: req.Person, Noreg: req.Noreg, Mata: req.Mata, Tht: req.Tht, Mulut: req.Mulut, Gigi: req.Gigi, LeherLainnya: req.LeherLainnya, Thyroid: req.Thyroid, Dada: req.DindingDada, Jantung: req.SuaraJantung, Paru: req.SuaraParu, Perut: req.DindingPerut, Hati: req.Hati, Lien: req.Lien, PeristatikUsus: req.PeristatikUsus, AbdomenLainnya: req.AbdomenDetail, Kulit: req.Kulit, Ginjal: req.Ginjal, Genetalia: req.Genetalia, EkstremitasSuperior: req.Superior, EkstremitasInferior: req.Inferior, KdBagian: kdBagian,
		Pelayanan: req.Pelayanan,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) InsertPemeriksaanFisikBangsalRepository(userID string, kdBagian string, req dto.ReqPemeriksaanFisikBangsal) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	data := rme.PemeriksaanFisikModel{
		InsertDttm: times.Format("2006-01-02 15:04:05"), Kategori: req.Kategori, InsertDevice: req.DeviceID, InsertUserId: userID, KetPerson: req.Person, Noreg: req.Noreg, Genetalia: req.Genetalia, KdBagian: kdBagian, Kepala: req.Kepala, Leher: req.Leher, Dada: req.Dada, Abdomen: req.Abdomen, Punggung: req.Punggung, Ekstremitas: req.Ekstremitas, Pelayanan: req.Pelayanan, LainLain: req.LainLain, PemeriksaanFisik: req.PemeriksaanFisik,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) SavePemeriksaanFisikRepository(userID string, kdBagian string, data rme.PemeriksaanFisikModel) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) InsertVitalSignBangsalRepository(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (res rme.DVitalSign, err error) {
	times := time.Now()

	data := rme.DVitalSign{
		InsertDttm: times.Format("2006-01-02 15:04:05"), UpdDttm: times.Format("2006-01-02 15:04:05"), InsertDevice: req.DeviceID, InsertUserId: userID, Noreg: req.Noreg, KdBagian: kdBagian, Pelayanan: req.Pelayanan, Td: req.TekananDarah, Nadi: req.Nadi, Pernafasan: req.Pernapasan, Suhu: req.Suhu, Tb: req.TinggiBadan, Bb: req.BeratBadan, Kategori: req.Kategori, KetPerson: req.Person,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) InsertVitalSignICURepository(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (res rme.DVitalSign, err error) {
	times := time.Now()

	data := rme.DVitalSign{
		InsertDttm: times.Format("2006-01-02 15:04:05"), UpdDttm: times.Format("2006-01-02 15:04:05"), InsertDevice: req.DeviceID, InsertUserId: userID, Noreg: req.Noreg, KdBagian: kdBagian, Pelayanan: req.Pelayanan, Td: req.TekananDarah, Nadi: req.Nadi, Spo2: req.Spo2, Pernafasan: req.Pernapasan, Suhu: req.Suhu, Tb: req.TinggiBadan, Bb: req.BeratBadan, Kategori: req.Kategori, KetPerson: req.Person,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) UpdateTandaVitalGangguanPerilakuRepository(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	var data rme.PemeriksaanFisikModel

	query := `UPDATE  vicore_rme.dpem_fisik SET jalan_nafas=?,td=?, pernafasan=?, pupil_kiri=?, pupil_kanan=?, nadi=?,spo2=?, cahaya_kanan=?, cahaya_kiri=?,suhu=?, akral=?,gcs_e=?, gcs_m=?,gcs_v=?, gangguan=?,gangguan_detail=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.JalanNafas, req.Td, req.Pernafasan, req.PupilKiri, req.PupilKanan, req.Nadi, req.Spo2,
		req.CahayaKanan, req.CahayaKiri, req.Suhu, req.Akral, req.GcsE, req.GcsM, req.GcsV, req.Gangguan, req.GangguanDetail,
		times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, req.Noreg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdateVitalSignBangsalRepository(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (res rme.DVitalSign, err error) {
	times := time.Now()

	var data rme.DVitalSign

	query := `UPDATE  vicore_rme.dpem_fisik SET  nadi=?, pernafasan=?, suhu=?, tb=?, td=?, bb=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.Nadi, req.Pernapasan, req.Suhu, req.TinggiBadan, req.TekananDarah, req.BeratBadan,
		times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, req.Noreg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdateVitalSignICURepository(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (res rme.DVitalSign, err error) {
	times := time.Now()

	var data rme.DVitalSign

	query := `UPDATE  vicore_rme.dvital_sign SET  nadi=?, pernafasan=?, suhu=?, tb=?, td=?, bb=?, spo2=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.Nadi, req.Pernapasan, req.Suhu, req.TinggiBadan, req.TekananDarah, req.BeratBadan, req.Spo2, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, req.Noreg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdatePemeriksaanFisikBangsalRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikBangsal) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	var data rme.PemeriksaanFisikModel

	query := `UPDATE  vicore_rme.dpem_fisik SET pemeriksaan_fisik=?, kepala=?, leher=?, dada=?, abdomen=?, punggung=?, genetalia=?, ekstremitas=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.PemeriksaanFisik, req.Kepala, req.Leher, req.Dada, req.Abdomen, req.Punggung, req.Genetalia, req.Ekstremitas,
		times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdatePemeriksaanFisikAnakRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikAnak) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	var data rme.PemeriksaanFisikModel

	query := `UPDATE  vicore_rme.dpem_fisik SET mata=?, mulut=?, gigi=?, thyroid=?, paru=?, jantung=?, dindingdada=?,  dindingdada_ret_epigastrium=?, dindingdada_ret_suprastermal=?, dindingdada_retraksi=?, hepar=?, hepar_detail=?, limpa=?, limpa_detail=?, ginjal=?, ginjal_detail=?, genetalia=?, ouf=?, ekstremitas=?, tugor_kulit=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.Mata, req.Mulut, req.Gigi, req.Thyroid, req.Paru, req.Jantung, req.DindingDada, req.Epigastrium, req.Supratermal, req.Retraksi, req.Hepar, req.HeparDetail, req.Limpa, req.LimpaDetail, req.Ginjal, req.GinjalDetail, req.Genetalia, req.Ouf, req.Ekstremitas, req.TugorKulit, times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdatePemeriksaanFisikRepository(userID string, kdBagian string, noReg string, req dto.ReqPemeriksaanFisikIGD) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()

	var data rme.PemeriksaanFisikModel

	query := `UPDATE  vicore_rme.dpem_fisik SET mata=?, tht=?, mulut=?, gigi=?, thyroid=?, leher_lainnya=?, dada=?,  jantung=?, paru=?, perut=?, hati=?, lien=?, peristatik_usus=?, abdomen_lainnya=?, kulit=?, ginjal=?, genetalia=?, ekstremitas_superior=?, ekstremitas_inferior= ? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, req.Mata, req.Tht, req.Mulut, req.Gigi, req.Thyroid, req.LeherLainnya, req.DindingDada, req.SuaraJantung, req.SuaraParu, req.DindingPerut, req.Hati, req.Lien, req.PeristatikUsus, req.AbdomenDetail, req.Kulit, req.Ginjal, req.Genetalia, req.Superior, req.Inferior, times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

// =========  =========== //
func (sr *rmeRepository) CariPemeriksaanFisikByDateRepository(userID string, ketPerson string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {

	query := `SELECT * FROM vicore_rme.dpem_fisik AS a WHERE kd_bagian=? AND ket_person=?  AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, ketPerson, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) CariPemeriksaanFisikByNoregKdBagianRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {

	query := `SELECT * FROM vicore_rme.dpem_fisik AS a WHERE kd_bagian=?  AND noreg=?  ORDER BY insert_dttm DESC LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// GET BHP RE-USE DIALIZER HARAPAN
func (sr *rmeRepository) GetBHPDializerRepository(noReg string) (res rme.DBhpDializerModel, err error) {
	times := time.Now()
	query := `SELECT * FROM vicore_rme.dbhp_dializer AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=?  AND noreg=?  ORDER BY insert_dttm  DESC  LIMIT 1 `

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetPemeriksaanFisikRepositoryIGD(userID string, ketKerson string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	times := time.Now()
	query := `SELECT * FROM vicore_rme.dpem_fisik AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=? AND kd_bagian=?  AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// CARI DATA VITAL SIGN HARI INI
func (sr *rmeRepository) CariDVitalSignRepository(userID string, kdBagian string, noReg string) (res rme.DVitalSign, err error) {
	times := time.Now()

	query := `SELECT * FROM vicore_rme.dvital_sign AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=? AND kd_bagian=?  AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) InsertResikoJatuhPasienRepository(data rme.DRisikoJatuh) (res rme.DRisikoJatuh, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) UpdateResikoJatuhPasienAnakRepositori(kdBagian string, noReg string, usia string, jenisKelamin string, diagnosis string, gangguan string, faktorLingkungan string, response string, penggunaanObat string, total int) (res rme.DRisikoJatuh, err error) {
	times := time.Now()

	var data rme.DRisikoJatuh

	query := `UPDATE vicore_rme.drisiko_jatuh SET usia=?, jenis_kelamin=?, diagnosis=?, gangguan_kognitif=?, faktor_lingkungan=?, respon=?, penggunaan_obat=?, total=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := sr.DB.Raw(query, usia, jenisKelamin, diagnosis, gangguan, faktorLingkungan, response, penggunaanObat, total, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, ", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdateResikoJatuhPasienMorseRepositori(kdBagian string, noReg string, rJatuh string, diagnosis string, ambulasi string, terapi string, infuse string, gayaBerjalan string, mental string, total int, kategori string) (res rme.DRisikoJatuh, err error) {
	times := time.Now()

	var data rme.DRisikoJatuh

	query := `UPDATE vicore_rme.drisiko_jatuh SET r_jatuh=?, diagnosis=?, b_ambulasi=?, terapi=?, terpasang_infuse=?, gaya_berjalan=?, status_mental=?, total=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND kd_bagian=? AND noreg =? AND kategori =? LIMIT 1`

	result := sr.DB.Raw(query, rJatuh, diagnosis, ambulasi, terapi, infuse, gayaBerjalan, mental, total, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg, kategori).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s ", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) UpdateResikoJatuhPasienDewasaRepository(kdBagian string, noReg string, kategori string, usia string, rJatuh string, aktivitas string, mobilisasi string, kognitif string, sensori string, pengobatan string, komorbiditas string, total int) (res rme.DRisikoJatuh, err error) {
	times := time.Now()

	var data rme.DRisikoJatuh

	query := `UPDATE vicore_rme.drisiko_jatuh SET usia=?, r_jatuh=?, aktivitas=?, mobilisasi=?, kognitif=?, defisit_sensori=?, pengobatan=?, komorbiditas=?, total=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND kd_bagian=? AND noreg =? AND kategori =? LIMIT 1`

	result := sr.DB.Raw(query, usia, rJatuh, aktivitas, mobilisasi, kognitif, sensori, pengobatan, komorbiditas, total, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg, kategori).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s ", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) CariResikoJatuhPasienRepository(kdBagian string, noReg string, kategori string) (res rme.DRisikoJatuh, err error) {
	times := time.Now()

	query := `SELECT * FROM vicore_rme.drisiko_jatuh AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=? AND kd_bagian=?  AND noreg=?  AND kategori=? LIMIT 1`

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg, kategori).Scan(&res)

	sr.Logging.Info(times.Format("01"))
	sr.Logging.Info(times.Format("02"))
	sr.Logging.Info(times.Format("2006"))
	sr.Logging.Info(kdBagian)
	sr.Logging.Info(noReg)
	sr.Logging.Info(kategori)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	sr.Logging.Info(result)
	sr.Logging.Info(query)

	return res, nil

}

// INSERT DINTERVESSI
func (sr *rmeRepository) InsertDataIntevensiRespository(data rme.DIntervensi) (res rme.DIntervensi, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

// GET NOMOR DIAGNOSA
func (sr *rmeRepository) GetNomorDaskepDiagnosaRepository() (res rme.NoDaskep, err error) {
	query := `SELECT COALESCE(LPAD(CONVERT(@last_id :=MAX(no_daskep),SIGNED INTEGER)+1,8,0),'00000001') AS daskep FROM vicore_rme.daskep_diagnosa WHERE length(no_daskep)=8;`

	result := sr.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

// CARI NOMOR DASKEP
func (sr *rmeRepository) CariNomorDaskepDiagnosaRepository(noDaskep string) (res rme.DaskepDiagnosaModel, err error) {

	data := sr.DB.Where("no_daskep=?", noDaskep).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}
func (sr *rmeRepository) UpdateDVitalSignDokterRepository(ketPerson string, pelayanan string, kdBagian string, noReg string, update rme.VitalSignDokter) (res rme.VitalSignDokter, err error) {

	data := sr.DB.Where("ket_person=? AND pelayanan=? AND kd_bagian=? AND noreg=?", ketPerson, pelayanan, kdBagian, noReg).Updates(&update).Scan(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) UpdateDemFisikRepository(ketPerson string, pelayanan string, kdBagian string, noReg string, update rme.DemfisikDokter) (res rme.DemfisikDokter, err error) {

	data := sr.DB.Where("ket_person=? AND pelayanan=? AND kd_bagian=? AND noreg=?", ketPerson, pelayanan, kdBagian, noReg).Updates(&update).Scan(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetDVitalSignDokterRepository(noReg string, bagian string, pelayanan string, person string) (res rme.VitalSignDokter, err error) {
	data := sr.DB.Where("noreg=? AND pelayanan=? AND ket_person=? AND kd_bagian=?", noReg, pelayanan, person, bagian).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetDVitalSignICURepository(noReg string, bagian string, pelayanan string, person string) (res rme.VitalSignDokter, err error) {
	data := sr.DB.Where("noreg=?  AND kd_bagian=?", noReg, bagian).Limit(1).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

// === PEMERIKSAAN FISIK PERAWAT
func (sr *rmeRepository) GetDVitalSignRepository(noReg string, bagian string, pelayanan string, person string) (res rme.DVitalSignModel, err error) {
	data := sr.DB.Where("noreg=? AND pelayanan=? AND ket_person=? AND kd_bagian=?", noReg, pelayanan, person, bagian).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetDVitalSignPerinaRepository(noReg string) (res rme.DVitalSignModel, err error) {
	data := sr.DB.Where("noreg=? AND pelayanan=? AND ket_person=? AND kd_bagian=?", noReg, "ranap", "Perawat", "PERI").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) InsertDVitalSignDokterRepository(data rme.VitalSignDokter) (res rme.VitalSignDokter, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) GetDemFisikDokterRepository(noReg string, person string, kdBagian string, pelayanan string) (res rme.DemfisikDokter, err error) {
	data := sr.DB.Where("noreg=? AND ket_person=? AND pelayanan=? AND kd_bagian=?", noReg, person, pelayanan, kdBagian).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) InserDemFisikDokterRepository(data rme.DemfisikDokter) (res rme.DemfisikDokter, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

// SAVE DATA DASKEP
func (sr *rmeRepository) SaveDataDaskepDiagnosaRespository(data rme.DaskepDiagnosaModel) (res rme.DaskepDiagnosaModel, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) SaveDataDaskepSLKI(data rme.DaskepSLKIModel) (res rme.DaskepSLKIModel, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) SaveDataDaskepSIKI(data rme.DaskepSikiModel) (res rme.DaskepSikiModel, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

// SIMPAN DATA SLKI DARI DIAGNOSA YANG DIPILIH // GET DATA
func (sr *rmeRepository) GetDataAsuhanKeperawatanRepository(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModel, err error) {
	ersr := sr.DB.Where("kd_bagian=? AND noreg=? AND status=?", kdBagian, noReg, status).Preload("DaskepSLKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_slki.no_urut asc")
	}).Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Preload("SDKIModel").Order("insert_dttm asc").Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}

func (sr *rmeRepository) GetDataAsuhanKeperawatanRepositoryV2(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModelV2, err error) {
	ersr := sr.DB.Where("kd_bagian=? AND noreg=? AND status=?", kdBagian, noReg, status).Preload("DaskepSLKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_slki.no_urut asc")
	}).Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Preload("SDKIModel").Order("insert_dttm asc").Preload("Perawat").Preload("Pelayanan").Preload("Implementasi", func(db *gorm.DB) *gorm.DB {
		return db.Preload("Perawat")
	}).Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}
func (sr *rmeRepository) GetDataAsuhanKeperawatanRepositoryV4(noReg string, kdBagian string) (res []rme.DasKepDiagnosaModelV2, err error) {
	ersr := sr.DB.Where("kd_bagian=? AND noreg=? ", kdBagian, noReg).Preload("DaskepSLKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_slki.no_urut asc")
	}).Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Preload("SDKIModel").Order("insert_dttm asc").Preload("Perawat").Preload("Pelayanan").Preload("Implementasi", func(db *gorm.DB) *gorm.DB {
		return db.Preload("Perawat")
	}).Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}

func (sr *rmeRepository) GetDataAsuhanKeperawatanRepositoryV3(noReg string, kdBagian string, status string) (res []rme.DasKepDiagnosaModelV3, err error) {
	ersr := sr.DB.Where("kd_bagian=? AND noreg=? AND status=?", kdBagian, noReg, status).Preload("DaskepSLKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_slki.no_urut asc")
	}).Preload("DaskepSIKI", func(db *gorm.DB) *gorm.DB {
		return db.Order("daskep_siki.no_urut asc")
	}).Preload("SDKIModel").Order("insert_dttm asc").Preload("Perawat").Preload("Pelayanan").Preload("Tindakan", func(db *gorm.DB) *gorm.DB {
		return db.Order("dimplementasi_keperawatan.id asc").Preload("Perawat").Preload("Pelayanan")
	}).Find(&res).Error

	if ersr != nil {
		return res, ersr
	}

	return res, nil
}

func (sr *rmeRepository) GetImplementasiTindakanRepository(noDaskep string) (res []rme.DImplementasiTindakanKeperawatan, err error) {

	err12 := sr.DB.Where("no_daskep=?", noDaskep).Preload("Perawat").Preload("Pelayanan").Find(&res).Error

	if err12 != nil {
		return res, err12
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteImplementasiTindakanRepository(idTindakan int) (err error) {
	errs := sr.DB.Where("id=?", idTindakan).Delete(&rme.DImplementasiTindakanKeperawatan{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

// INSERT DATA ALERGI PASIEN
func (sr *rmeRepository) InsertDataAlergiRepository(data rme.DAlergi) (res rme.DAlergi, err error) {
	result := sr.DB.Create(&data).Scan(&res)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) GetPenyakitTerdahuluPerawatRepository(noRM string) (res rme.RiwayatPenyakitDahulu, err error) {

	times := time.Now()

	query := `SELECT dsp.tgl_masuk, dsp.aseskep_rwyt_pnykt FROM vicore_rme.dcppt_soap_pasien AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id WHERE dp.id=? AND dsp.pelayanan='rajal' AND dsp.tgl_masuk< ? ORDER BY dsp.insert_dttm DESC LIMIT 30`

	result := sr.DB.Raw(query, noRM, times.Format("2006-01-02")).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) GetRiwayatAlergiKeluargaRepository(noRM string) (res []rme.DAlergi, err error) {
	errs := sr.DB.Limit(5).Where(&rme.DAlergi{Id: noRM, Kelompok: "keluarga"}).Find(&res).Order("no DESC").Error

	if errs != nil {
		sr.Logging.Info(errs.Error())
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) OnGetRiwayatAlergiPasienRepository(noRM string, kdBagian string) (res []rme.DAlergi, err error) {
	errs := sr.DB.Limit(1).Where(&rme.DAlergi{Id: noRM, KdBagian: kdBagian}).Find(&res).Order("no DESC").Error

	if errs != nil {
		sr.Logging.Info(errs.Error())
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) GetRiwayatAlergiRepository(noRM string) (res []rme.DAlergi, err error) {
	errs := sr.DB.Where("id=? AND kelompok !=?", noRM, "keluarga").Find(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteAlergiRepository(nomor int) (err error) {
	errs := sr.DB.Where("no=?", nomor).Delete(&rme.DAlergi{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

func (sr *rmeRepository) OnGetRiwayatPenyakitDahulu(noRM string, tgl string) (res []rme.KeluhanUtama, erre error) {

	query := `SELECT dsp.tgl_masuk, dsp.asesmed_keluh_utama, dsp.asesmed_rwyt_skrg FROM vicore_rme.dcppt_soap_dokter AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id WHERE dp.id=? AND  dsp.asesmed_keluh_utama != ""  AND dsp.tgl_masuk < ? ORDER BY dsp.insert_dttm DESC LIMIT 30`

	result := sr.DB.Raw(query, noRM, tgl).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnGeRiwayatKeluhanUtamaRepository(noRM string) (res []rme.RiwayatKeluhanModel, err error) {
	query := `SELECT dsp.tgl_masuk, dsp.noreg, dp.id FROM vicore_rme.dcppt_soap_pasien AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id`

	result := sr.DB.Raw(query, noRM).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetRiwayatPenyakitDahuluPerawat(noRM string, tgl string) (res []rme.RiwayatPenyakitDahuluPerawat, erre error) {
	query := `SELECT dsp.tgl_masuk, dsp.aseskep_kel, dsp.aseskep_rwyt_pnykt, dp.id  FROM vicore_rme.dcppt_soap_pasien AS dsp LEFT JOIN rekam.dregister AS dr ON dsp.noreg=dr.noreg LEFT JOIN his.dprofilpasien AS dp ON dr.id=dp.id WHERE   dp.id=? AND dsp.tgl_masuk < ?  ORDER BY dsp.insert_dttm DESC LIMIT 30`

	result := sr.DB.Raw(query, noRM, tgl).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnSaveAlergiKeluargaRepository(alerti string, bagian string, userID string) (meesage string, err error) {

	datas := rme.DAlergi{
		Kelompok:   "keluarga",
		Alergi:     alerti,
		KdBagian:   bagian,
		InsertUser: userID,
	}

	result := sr.DB.Create(&datas).Scan(&datas)

	if result.Error != nil {
		return "Data gagal disimpan", result.Error
	}

	return "Data berhasil disimpan", nil
}

func (sr *rmeRepository) OnSaveKeluhanUtamaIGDRepository(data rme.AsesemenDokterIGD) (res rme.AsesemenDokterIGD, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateKeluhanUtamaIGDRepository(bagian string, req dto.ReqKeluhanUtamaIGD) (res rme.AsesemenDokterIGD, err error) {
	errs := sr.DB.Where(rme.AsesemenDokterIGD{
		Pelayanan:        req.Pelayanan,
		KeteranganPerson: req.Person,
		KdBagian:         bagian,
		Noreg:            req.NoReg,
	}).Updates(rme.AsesemenDokterIGD{
		AsesmedKeluhUtama: req.KeluhanUtama,
		AsesmedRwytSkrg:   req.RiwayatSekarang,
		AsesmedRwytDahulu: req.RiwayatDahulu,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveRiwayatPenyakitKeluargaRepository(alergi rme.DAlergi) (res rme.DAlergi, err error) {
	result := sr.DB.Create(&alergi).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return alergi, nil
}

// DELETE 	DASKEP SIKI
func (sr *rmeRepository) OnDeleteDaskepSikiRepository(noDaskep string) (err error) {
	errs := sr.DB.Where("no_daskep=?", noDaskep).Delete(&rme.DaskepSikiModel{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

// DELETE DASKEP SLKI
func (sr *rmeRepository) OnDeleteDaskepSLKIRepository(noDaskep string) (err error) {
	errs := sr.DB.Where("no_daskep=?", noDaskep).Delete(&rme.DaskepSLKIModel{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

// DELETE DASKEP DIAGNOSA
func (sr *rmeRepository) OnDeleteDaskepDiagnosaRepository(noDaskep string) (err error) {
	errs := sr.DB.Where("no_daskep=?", noDaskep).Delete(&rme.DasKepDiagnosaModel{})

	if errs != nil {
		return errs.Error
	}

	return nil
}

func (sr *rmeRepository) OnGetDokterAnakDanObgynRepository() (res []rme.DokterBayi, err error) {
	query := `SELECT iddokter AS id_dokter, spesialisasi, namadokter AS nama_dokter  FROM his.ktaripdokter WHERE spesialisasi LIKE "%Spesialis Kandungan ( Obgyn )%" OR   spesialisasi LIKE "%Spesialis Anak%"`

	result := sr.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnGetRiwayatKelahiranYangLaluRepository(noRM string) (res []rme.RiwayatKehamilanPerina, err error) {

	errs12 := sr.DB.Where(&rme.RiwayatKehamilanPerina{NoRm: noRM}).Find(&res).Error

	if errs12 != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", errs12.Error())
		return res, errors.New(message)
	}

	return res, nil
}
