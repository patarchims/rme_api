package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
)

func (sr *rmeRepository) OnGetPemeriksaanFisikAnakRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		Kategori:  "anak",
		KetPerson: person,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *rmeRepository) GetPemeriksaanFisikIGDRepository(userID string, person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {

	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: person,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetPemeriksaanFisikDokterRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikDokterAntonio{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: person,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *rmeRepository) OnGetPemeriksaanFisikRawatInapRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: "Perawat",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetPemeriksaanFisikICURepository(noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  "ICU",
		Noreg:     noReg,
		KetPerson: "Perawat",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetPemeriksaanFisikIGDPerawatRepository(person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {

	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: person,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetPemeriksaanFisikAnakBangsalRepository(noReg string, KDBagian string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  KDBagian,
		Noreg:     noReg,
		KetPerson: "Dokter",
		Pelayanan: "ranap",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetPemeriksaanFisikReportIGDRepository(noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  "IGD001",
		Noreg:     noReg,
		KetPerson: "Dokter",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdatePemeriksaanFisikIGDRepository(data rme.PemeriksaanFisikModel, pelayanan string,
	person string, bagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	errs := sr.DB.Where(rme.PemeriksaanFisikModel{
		Pelayanan: pelayanan,
		KetPerson: person,
		KdBagian:  bagian,
		Noreg:     noReg,
	}).Updates(&data).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdatePemeriksaanFisikAntonioDokterRepository(data rme.PemeriksaanFisikDokterAntonio, bagian string, person string, pelayanan string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error) {
	errs := sr.DB.Where(rme.PemeriksaanFisikDokterAntonio{
		Pelayanan: pelayanan,
		KetPerson: person,
		KdBagian:  bagian,
		Noreg:     noReg,
	}).Updates(&data).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) SavePemeriksaanFisikIGDRepository(data rme.PemeriksaanFisikModel) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnSavePemeriksaanFisikRepository(data rme.PemeriksaanFisikDokterAntonio) (res rme.PemeriksaanFisikDokterAntonio, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) GetAsesemenKeluhanUtamaIGDDokter(bagian string, noreg string, person string) (res rme.AsesemenDokterIGD, err error) {

	query := `SELECT ds.keterangan_person, ds.insert_user_id, ds.pelayanan, ds.kd_bagian, ds.noreg, ds.asesmed_keluh_utama, ds.asesmed_rwyt_skrg, ds.asesmed_rwyt_dahulu FROM vicore_rme.dcppt_soap_dokter AS ds where ds.kd_bagian=? AND ds.noreg=? AND ds.keterangan_person=?`

	result := sr.DB.Raw(query, bagian, noreg, person).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) GetAsesmenAwalMedisIGDDokter(noreg string) (res rme.AsesemenMedisIGD, err error) {

	query := `SELECT ds.keterangan_person, ds.insert_user_id, ds.pelayanan, ds.kd_bagian, ds.noreg, ds.asesmed_keluh_utama, ds.asesmed_rwyt_skrg, ds.asesmed_lokalis_image, ds.asesmen_prognosis, ds.asesmed_konsul_ke, ds.asesmed_alasan_opname, ds.asesmed_terapi, ds.asesmed_konsul_ke, kt.namadokter FROM vicore_rme.dcppt_soap_dokter AS ds LEFT JOIN his.ktaripdokter AS kt ON  ds.insert_user_id=kt.iddokter where ds.kd_bagian=? AND ds.noreg=? AND ds.keterangan_person=?`

	result := sr.DB.Raw(query, "IGD001", noreg, "Dokter").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) GetAsesmenAwalMedisIGDReportDokter(noreg string) (res rme.AsesemenMedisIGD, err error) {

	query := `SELECT ds.keterangan_person, ds.insert_user_id, ds.asesmed_diag_banding as diagnosa_banding, ds.pelayanan, ds.kd_bagian, ds.noreg, ds.asesmed_keluh_utama, ds.asesmed_rwyt_skrg, ds.asesmed_lokalis_image, ds.asesmen_prognosis,  ds.asesmed_rwyt_dahulu, ds.asesmed_konsul_ke, ds.asesmed_alasan_opname, ds.asesmed_terapi, ds.asesmed_konsul_ke, kt.namadokter, ds.jam_masuk, ds.tgl_masuk FROM vicore_rme.dcppt_soap_dokter AS ds LEFT JOIN his.ktaripdokter AS kt ON  ds.insert_user_id=kt.iddokter where ds.kd_bagian=? AND ds.noreg=? AND ds.keterangan_person=?`

	result := sr.DB.Raw(query, "IGD001", noreg, "Dokter").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetAllDiagnosaKeperawatanRepository() (res []rme.DiagnosaKeperawatan, err error) {

	query := `SELECT kode, judul from vicore_rme.sdki`

	result := sr.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetPemeriksaanFisikBangsalRepository(userID string, person string, kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: person,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetPemeriksaanFisikBangsalPerinaRepository(noReg string) (res rme.PemeriksaanFisikModel, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikModel{
		KdBagian:  "PERI",
		Noreg:     noReg,
		KetPerson: "Perawat",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetHistoryRiwayatPenyakitRepository(noreg string) (res []rme.HistoryPenyakit, err error) {

	ersr := sr.DB.Select("keterangan_person, insert_user_id, pelayanan, kd_bagian, noreg, asesmed_keluh_utama, asesmed_rwyt_skrg, asesmed_rwyt_dahulu").Where("noreg=?", noreg).Preload("KPelayanan").Find(&res).Error

	if ersr != nil {
		return res, ersr
	}

	return res, nil
}
