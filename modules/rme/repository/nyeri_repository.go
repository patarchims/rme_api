package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"
)

func (sr *rmeRepository) OnGetDAssemenUalngNyeriRepsoitory(req dto.ReqNoReg) (res []rme.DasesmenUlangNyeriV2, err error) {
	err12 := sr.DB.Where("noreg=?", req.Noreg).Preload("Perawat").Preload("Pelayanan").Find(&res).Error

	if err12 != nil {
		return res, err12
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteAsesmenUlangNyeri(req dto.ReqIDUlangNyeri) (res rme.DasesmenUlangNyeri, err error) {

	err12 := sr.DB.Where("id_ulang_nyeri=?", req.ID).Delete(&res).Find(&res).Error

	if err12 != nil {
		return res, err12
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveDAssesmenNyeriRepository(kdBagian string, userID string, req dto.ReqSaveAsesmenNyeri) (res rme.DasesmenUlangNyeri, err error) {

	datas := &rme.DasesmenUlangNyeri{
		KdBagian:       kdBagian,
		InsertDttm:     time.Now(),
		UserID:         userID,
		Noreg:          req.Noreg,
		NamaObat:       req.NamaObat,
		IDFarmakologi:  req.IDFarmakologi,
		Rute:           req.Rute,
		NonFarmakologi: req.NonFarmakologi,
		SkorNyeri:      req.SkorNyeri,
		WaktuKaji:      req.WaktuKaji,
		DosisFrekuensi: req.DosisFrekuensi,
	}

	result := sr.DB.Create(&datas).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data berhasil disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}
