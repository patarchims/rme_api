package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
)

func (sr *rmeRepository) OnGetDownScroeNeoNatusRepository(noReg string) (res rme.DownScoreNeoNatusModel, err error) {
	data := sr.DB.Where("noreg=?", noReg).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}
func (sr *rmeRepository) OnGetApgarScoreNeoNatusRepository(noReg string) (res []rme.DApgarScoreNeoNatus, err error) {
	data := sr.DB.Where("noreg=?", noReg).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetTindakLanjutPerinaRepository(noReg string, kdBagian string) (res rme.TindakLajutPerina, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, "Dokter").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveTindakLanjutPerinaRepository(data rme.TindakLajutPerina) (res rme.TindakLajutPerina, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateTindakLanjutPerinaRepository(data rme.TindakLajutPerina, noReg string, kdBagian string) (res rme.TindakLajutPerina, err error) {

	var datas rme.TindakLajutPerina

	query := `UPDATE  vicore_rme.dcppt_soap_dokter SET asesmen_tindakan_operasi=?, asesmed_tindak_lanjut=?, asesmen_tgl_kontrol_ulang=? WHERE  noreg =? AND kd_bagian =? LIMIT 1`

	result := sr.DB.Raw(query, data.AsesmenTindakanOperasi, data.AsesmedTindakLanjut, data.AsesmenTglKontrolUlang, noReg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return datas, nil
}

func (sr *rmeRepository) OnCheckApgarScoreRepository(noReg string, waktu string) (res rme.DApgarScoreNeoNatus, err error) {
	data := sr.DB.Where("noreg=? AND waktu=?", noReg, waktu).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetAsesmenKeperawatanBayiRepository(noReg string, kdBagian string) (res rme.AsesmenKeperawatanBayi, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=?", noReg, kdBagian).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

// OnGetSkalaNyeriKeperawatanRepository
// SKALA NYERI DEWASA
func (sr *rmeRepository) OnGetSkalaNyeriKeperawatanDewasaRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, "Dewasa").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetSkalaNyeriBidanRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, "Bidan").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetSkalaNyeriKeperawatanAnakRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=? AND keterangan_person=?", noReg, kdBagian, "Anak").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetSkalaNyeriRepository(noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error) {
	return res, nil
}

func (kb *rmeRepository) GetRiwayatKehamilanPerinaRepository(noRM string) (res []kebidanan.DRiwayatKehamilan, err error) {
	errs := kb.DB.Where("no_rm=?", noRM).Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnGetAsesmenSkalaNyeriRepository(noReg string) (res rme.AsesmenSkalaNyeri, err error) {
	data := sr.DB.Where("noreg=? AND pelayanan=?", noReg, "ranap").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveSkalaNyeriKeperawatanRepository(data rme.AsesmenSkalaNyeri) (res rme.AsesmenSkalaNyeri, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateSkalaNyeriKeperawatanRepository(data rme.AsesmenSkalaNyeri, noReg string, kdBagian string) (res rme.AsesmenSkalaNyeri, err error) {

	var datas rme.AsesmenSkalaNyeri

	query := `UPDATE  vicore_rme.dcppt_soap_pasien SET aseskep_skala_nyeri=?,  aseskep_lokasi_nyeri=?, aseskep_frekuensi_nyeri=?, aseskep_nyeri_menjalar=?, aseskep_kualitas_nyeri=? WHERE  noreg =? AND kd_bagian =? LIMIT 1`

	result := sr.DB.Raw(query, data.AseskepSkalaNyeri, data.AseskepLokasiNyeri, data.AseskepFrekuensiNyeri, data.AseskepNyeriMenjalar, data.AseskepKualitasNyeri, noReg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return datas, nil
}

func (sr *rmeRepository) OnDeleteRiwayatKehamilanYangLaluRepository(kdRiwayat string) (res rme.RiwayatKehamilanPerina, message string, err error) {
	result := sr.DB.Where(rme.RiwayatKehamilanPerina{KdRiwayat: kdRiwayat}).Delete(&res)

	if result.Error != nil {
		return res, "Data gagal dihapus", result.Error
	}
	return res, "Data berhasil dihapus", nil
}

func (sr *rmeRepository) OnInsertApgarScoreRepository(data rme.DApgarScoreNeoNatus) (res rme.DApgarScoreNeoNatus, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnSaveScroeNeoNatusRepository(data rme.DownScoreNeoNatusModel) (res rme.DownScoreNeoNatusModel, err error) {
	result := sr.DB.Create(&data).Scan(&data)
	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateScoreNeoNatusRepository(data rme.DownScoreNeoNatusModel, noReg string) (res rme.DownScoreNeoNatusModel, err error) {

	var datas rme.DownScoreNeoNatusModel

	query := `UPDATE  vicore_rme.ddown_score_neonatus SET frekwensi_nafas=?, sianosis=?, retraksi=?, air_entry=?, merintih=?, total=? WHERE  noreg =? LIMIT 1`

	result := sr.DB.Raw(query, data.FrekwensiNafas, data.Sianosis, data.Retraksi, data.AirEntry, data.Merintih, data.Total, noReg).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	errs := sr.DB.Where(rme.DownScoreNeoNatusModel{Noreg: noReg}).Updates(&datas).Scan(&datas).Error

	if errs != nil {
		return res, errs
	}

	return datas, nil
}

func (sr *rmeRepository) OnUpdateApgarSocreRepository(req dto.ReqSaveApgarScore, noReg string) (res dto.ApgarScoreResponse, err error) {

	var datas rme.DApgarScoreNeoNatus

	query := `UPDATE  vicore_rme.dapgar_score_neonatus SET d_jantung=?, u_nafas=?, otot=?, refleksi=?, warna_kulit=?, total=? WHERE noreg =? AND waktu=? LIMIT 1`

	result := sr.DB.Raw(query, req.DJantung, req.UNafas, req.Otot, req.Refleksi, req.WarnaKulit, req.Total, noReg, req.Waktu).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	mapper := sr.rmeMapper.ToMapperSingleScoreNeoNatus(datas)

	return mapper, nil
}

func (sr *rmeRepository) OnSaveDataAsesmenBayiRepository(data rme.AsesmenKeperawatanBayi) (res rme.AsesmenKeperawatanBayi, err error) {
	result := sr.DB.Create(&data).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateDataAsesmenBayiRepository(data rme.AsesmenKeperawatanBayi, noReg string, kdBagian string) (res rme.AsesmenKeperawatanBayi, err error) {
	errs := sr.DB.Where(rme.AsesmenKeperawatanBayi{
		KdBagian: kdBagian,
		Noreg:    noReg,
	}).Updates(&data).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *rmeRepository) GetNomorAnalisaDataRepository() (res rme.KodeAnalisa, err error) {
	query := `SELECT COALESCE(LPAD(CONVERT(@last_id :=MAX(kode_analisa),SIGNED INTEGER)+1,8,0),'00000001') AS kode_analisa FROM vicore_rme.danalisa_data  WHERE LENGTH(kode_analisa)=8;`

	result := sr.DB.Raw(query).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) SaveAnalisaDataRepository(data rme.DAnalisaData) (res rme.DAnalisaData, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) SaveDAnalisaProblemRepository(data rme.DAnalisaProblem) (res rme.DAnalisaProblem, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

// ==================== KAKI KANAN BAYI ======================== //
func (sr *rmeRepository) OnInsertKakiKananBayiRepository(data rme.KakiKananBayi) (res rme.KakiKananBayi, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnSaveKakiKananBayiReporitory(data rme.KakiKananBayi) (res rme.KakiKananBayi, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnUpdateKakiKananBayiRepository(data rme.KakiKananBayi, noReg string, kdBagian string) (res rme.KakiKananBayi, err error) {
	errs := sr.DB.Where(rme.KakiKananBayi{
		KdBagian: kdBagian,
		Noreg:    noReg,
	}).Updates(&data).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *rmeRepository) OnUpdateCatatanKeperawatanRepository(data rme.DCatatanKeperawatan, ID int, kdBagian string) (res rme.DCatatanKeperawatan, err error) {
	errs := sr.DB.Where(rme.DCatatanKeperawatan{
		KdBagian:  kdBagian,
		IdCatatan: ID,
	}).Updates(&data).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *rmeRepository) OnGetKakiKananBayiRepository(noReg string, kdBagian string) (res rme.KakiKananBayi, err error) {
	data := sr.DB.Where("noreg=? AND kd_bagian=?", noReg, kdBagian).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetEaningWaningSystemRepository(noReg string) (res []rme.DearlyWarningSystem, err error) {
	data := sr.DB.Where("noreg=?", noReg).Preload("MutiaraPengajar").Order("id_ews asc").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetDataKaruRepository(kdBagian string) (res rme.MutiaraPengajar, err error) {
	data := sr.DB.Where(rme.MutiaraPengajar{
		KdKaru: kdBagian,
	}).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnGetDataPerawatRepository(idPegawai string) (res rme.MutiaraPengajar, err error) {
	data := sr.DB.Where(rme.MutiaraPengajar{
		Id: idPegawai,
	}).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnDeleteCatatanKeperawatan(ID int) (res rme.DCatatanKeperawatan, err error) {
	result := sr.DB.Where(rme.DCatatanKeperawatan{IdCatatan: ID}).Delete(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnSaveCatatanKeperawatanRepository(data rme.DCatatanKeperawatan) (res rme.DCatatanKeperawatan, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnGetCatatanKeperawatanRepository(noReg string) (res []rme.DCatatanKeperawatanModel, err error) {
	result := sr.DB.Where(&rme.DCatatanKeperawatanModel{Noreg: noReg}).Preload("MutiaraPengajar").Order("id_catatan asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}
