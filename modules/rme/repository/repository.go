package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/igd"
	"hms_api/modules/kebidanan"
	dtoKebidanan "hms_api/modules/kebidanan/dto"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/modules/rme/entity"
	soapEntity "hms_api/modules/soap/entity"
	"time"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type rmeRepository struct {
	DB             *gorm.DB
	Logging        *logrus.Logger
	rmeMapper      entity.RMEMapper
	soapRepository soapEntity.SoapRepository
}

func NewRMERepository(db *gorm.DB, Logging *logrus.Logger, mapper entity.RMEMapper) entity.RMERepository {
	return &rmeRepository{
		DB:        db,
		Logging:   Logging,
		rmeMapper: mapper,
	}
}

// ============================================== SIMPAN SKALA TRIASE MODEL
func (rr *rmeRepository) SaveSkalaTriaseRepository(data rme.SkalaTriaseModel) (res rme.SkalaTriaseModel, err error) {
	result := rr.DB.Create(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return data, nil
}

func (rr *rmeRepository) UpdateSkalaTriaseRepository(req dto.ReqSkalaNyeriTriaseeIGD, userID string, kdBagian string, noReg string) (res rme.SkalaTriaseModel, err error) {
	times := time.Now()

	var data rme.SkalaTriaseModel

	query := `UPDATE vicore_rme.dpem_fisik SET skala_nyeri=?,skala_nyeri_p=?,skala_nyeri_q=?,
	skala_nyeri_r=?, skala_nyeri_s=?,skala_nyeri_t=?, flacc_wajah=?,flacc_kaki=?, flacc_aktifitas=?, flacc_menangis=?, flacc_bersuara=?,flacc_total=?,skala_triase=? WHERE YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? AND  insert_user_id=? AND kd_bagian=? AND noreg =? LIMIT 1`

	result := rr.DB.Raw(query, req.Nyeri, req.NyeriP, req.NyeriQ, req.NyeriR, req.NyeriS, req.NyeriT, req.FlaccWajah, req.FlaccKaki,
		req.FlaccAktifitas, req.FlaccMenangis, req.FlaccBersuara, req.FlaccTotal, req.SkalaTriase, times.Format("2006"), times.Format("01"), times.Format("02"), userID, kdBagian, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

// ONSAVE PENGKAJIAN PERSISTEM ICU FIBERHANDLER
func (rr *rmeRepository) OnSavePengkajianPersistemICURepository(req dtoKebidanan.RequestPengkajianPersistemICU, userID string, kdBagian string) (res kebidanan.AsesmenPengkajianPersistemICUBangsal, err error) {

	times := time.Now()

	tanggal, _ := rr.soapRepository.GetJamMasukPasienRepository(req.Noreg)

	var bidan = kebidanan.AsesmenPengkajianPersistemICUBangsal{
		InsertDttm:                 times.Format("2006-01-02 15:04:05"),
		UpdDttm:                    times.Format("2006-01-02 15:04:05"),
		KeteranganPerson:           req.Person,
		InsertUserId:               userID,
		InsertPc:                   req.InsertPC,
		TglMasuk:                   tanggal.Tanggal[0:10],
		Pelayanan:                  req.Pelayanan,
		KdBagian:                   kdBagian,
		Noreg:                      req.Noreg,
		KdDpjp:                     req.KodeDokter,
		AseskepSistemAirway:        req.Airway,
		AsekepSistemBreathing:      req.Breathing,
		AseskepSistemCirculation:   req.Circulation,
		AseskepSistemNutrisi:       req.Nutrisi,
		AseskepSistemEliminasiBak:  req.EliminasiBak,
		AseskepSistemEliminasiBab:  req.EliminasiBab,
		AseskepSistemMinum:         req.Minum,
		AseskepSistemMakan:         req.Makan,
		AseskepSistemAktivitas:     req.Aktivitas,
		AseskepSistemPadaBayi:      req.PadaBayi,
		AseskepSistemRefleksCahaya: req.RefleksCahaya,
		AseskepSistemPupil:         req.Pupil,
		AseskepSistemAbdomen:       req.Abdomen,
		// AseskepSistemTidurAtauIstirahat: req.tidu,
		AseskepSistemBerjalan:                    req.Berjalan,
		AseskepSistemThermoregulasi:              req.Thermoregulasi,
		AseskepSistemKenyamanan:                  req.Kenyamanan,
		AsekepSistemKualitas:                     req.Kualitas,
		AsekepSistemPola:                         req.Pola,
		AseskepSistemPerfusiGastrointestinal:     req.PerfusiGastrointestinal,
		AseskepSistemPerfusiRenal:                req.PerfusiRenal,
		AseskepSistemKejang:                      req.Kejang,
		AseskepSistemStatusMental:                req.StatusMental,
		AseskepSistemPenglihatan:                 req.Penglihatan,
		AseskepSistemPendengaran:                 req.Pendengaran,
		AseskepSistemHamil:                       req.Hamil,
		AseskepSistemCervixTerakhir:              req.PemeriksaanCervixTerakhir,
		AseskepSistemPerfusiSerebral:             req.PerfusiSerebral,
		AseskepSistemBicara:                      req.Bicara,
		AseskepSistemNyeriMempegaruhi:            req.NyeriMempengaruhi,
		AseskepBahasaSehariHari:                  req.BahasaSehariHari,
		AseskepHambatanBelajar:                   req.HambatanBelajar,
		AseskepBahasaIsyarat:                     req.BahasaIsyarat,
		AseskepPerluPenerjemah:                   req.PerluPenerjemah,
		AseskepSistemTidurAtauIstirahat:          req.AktivitasAtauIstirahat,
		AseskepSistemPenggunaanAlatBantu:         req.PenggunaanAlatBantu,
		AseskepReponseEmosi:                      req.ResponseEmosi,
		AseskepTingkatPendidikan:                 req.TingkatPendidikan,
		AseskepSistemPekerjaan:                   req.Pekerjaan,
		AseskepSistemBelMudahDijangkau:           req.BelMudaDijangkau,
		AseskepSistemTinggalBersama:              req.TingkatBersama,
		AseskepSistemPemimpinAgama:               req.KunjunganPemimpin,
		AseskepSistemPasangPengamananTempatTidur: req.PasangPengamanTempatTidur,
		AseskepKebutuhanPembelajaran:             req.PotensialKebutuhanPembelajaran,
		// AseskepSistemKamarMandi:                  req.AseskepSistemMandi,
		// AseskepSistemAksesMasuk: req.AseskepSistemAksesMasuk,
		AseskepCaraBelajarYangDisukai:      req.CaraBelajarDisukai,
		AseskepSistemPresepsiTerhadapSakit: req.PresepsiTerhadapSakit,
		AseskepSistemMenjalankanIbadah:     req.MenjalankanIbadah,
		AseskepSistemNilaiKepercayaan:      req.NilaiKepercayaan,
		AseskepKondisiLingkungan:           req.KondisiLingkunganDirumah,
	}

	result := rr.DB.Create(&bidan)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (rr rmeRepository) GetSkalaTriaseRepository(noReg string, kdBagian string) (res dto.ResSkalaNyeriTriaseIGD, err error) {
	var data rme.SkalaTriaseModel

	// GET SKALA NYERI DARI TABEL DPEM FISIK
	results := rr.DB.Where(&rme.SkalaTriaseModel{KdBagian: kdBagian, Noreg: noReg}).Find(&data)

	if results.Error != nil {
		return res, results.Error
	}

	rr.Logging.Info("Data ditemukan")
	rr.Logging.Info(data)

	mapper := rr.rmeMapper.ToSkalaTriaseResponseFromModelTriase(data)

	return mapper, nil
}

// ==============================================  UPDATE SKALA TIRASE MODEL

func (rr *rmeRepository) GetSDKIRepository(judul string) (res []rme.SDKIModel, err error) {
	var results []rme.SDKIModel
	search := "%" + judul + "%"
	data := rr.DB.Where("judul LIKE ? OR KODE LIKE ?", search, search).Find(&results)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		rr.Logging.Info(message)
		return results, errors.New(message)
	}

	return results, nil
}

// GET SDKI BY ID
func (rr *rmeRepository) GetSDKIByIDRepository(ID string) (res rme.SDKIModel, err error) {
	// GET SKALA NYERI DARI TABEL DPEM FISIK
	results := rr.DB.Where(&rme.SDKIModel{Kode: ID}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (rr *rmeRepository) GetSLKIRepository(judul string) (res []rme.SLKIModel, err error) {

	var results []rme.SLKIModel
	search := "%" + judul + "%"

	data := rr.DB.Where("kode LIKE ?", search).Order("no_urut asc").Find(&results)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		rr.Logging.Info(message)
		return results, errors.New(message)
	}

	return results, nil
}

func (rr *rmeRepository) GetSIKIRepository(kode string) (res rme.SIKIModel, err error) {
	var result rme.SIKIModel

	data := rr.DB.Where("kode=?", kode).Find(&result)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		rr.Logging.Info(message)
		return result, errors.New(message)
	}

	return result, nil
}

func (rr *rmeRepository) GetDeskripsiSikiRepository(kode string) (res rme.SikiDeskripsiModel, err error) {
	var data rme.SikiDeskripsiModel

	ersr := rr.DB.Where("kode=?", kode).Preload("DeskripsiSikiModel").Find(&data).Error
	if ersr != nil {
		return data, ersr
	}
	return data, nil

}

func (sr *rmeRepository) InsertDaskepRepository(data rme.Daskep) (res rme.Daskep, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) InsertTandaVitalPerinaRepository(data rme.DVitalSignModel) (res rme.DVitalSignModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) UpdateTandaVitalPerinaRepository(kdBagian string, pelayanan string, noReg string, req dto.ReqDVitalSignPerina) (res rme.DVitalSignModel, err error) {

	sr.DB.Where(rme.DVitalSignModel{Pelayanan: pelayanan,
		KdBagian: kdBagian, Noreg: noReg}).Updates(rme.DVitalSignModel{
		Td:            req.Td,
		Hr:            req.HR,
		Pernafasan:    req.RR,
		Tb:            req.Tb,
		LingkarDada:   req.LingkarDada,
		LingkarKepala: req.LingkarKepala,
		LingkarLengan: req.LingkarLengan,
		LingkarPerut:  req.LingkarPerut,
		Bb:            req.Bb,
		WarnaKulit:    req.WarnaKulit,
		Spo2:          req.Spo2,
		// Babinski:          req.Babinski,
		// RefleksSucking:    req.Sucking,
		// RefleksRooting:    req.Rooting,
		// RefleksMoro:       req.Moro,
		// RefleksGraps:      req.Graps,
		// RefleksSwallowing: req.Swallowing,
	}).Scan(&res)

	return res, nil
}

func (sr *rmeRepository) InsertCPPTPasienRepository(data rme.CPPTPasienModel) (res rme.CPPTPasienModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnDeleteDEWarningSystemRepository(ID int) (res rme.DearlyWarningSystem, err error) {
	sr.DB.Where(rme.DearlyWarningSystem{IdEws: ID}).Delete(&res)
	return res, nil
}

func (sr *rmeRepository) OnDeleteEdukasiTerintegrasi(ID int) (res rme.DEdukasiTerintegrasi, err error) {
	result := sr.DB.Where(rme.DEdukasiTerintegrasi{IdEdukasi: ID}).Delete(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnInsertDewarningSystemRepository(data rme.DearlyWarningSystem) (res rme.DearlyWarningSystem, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) SearchCPPTPasienRepository(kdBagian string, kelompok string, pelayanan string, noReg string, tanggal string, userId string) (res rme.CPPTPasienModel, err error) {

	sr.Logging.Debug("Cari CPPT Pasien")

	data := sr.DB.Where(rme.CPPTPasienModel{InsertUserId: userId, Kelompok: kelompok, Pelayanan: pelayanan,
		KdBagian: kdBagian, Noreg: noReg, Tanggal: tanggal}).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

// ============ UPDATE CPPT
func (sr *rmeRepository) UpdateCPPTPasienRepository(kdBagian string, kelompok string, pelayanan string, noReg string, tanggal string, data dto.ReqInsertDcpptPasien) (res rme.CPPTPasienModel, err error) {

	sr.DB.Where(rme.CPPTPasienModel{Pelayanan: pelayanan, Kelompok: kelompok,
		KdBagian: kdBagian, Noreg: noReg, Tanggal: tanggal}).Updates(rme.CPPTPasienModel{Subjektif: data.Sujektif, Objektif: data.Objektif, Asesmen: data.Asesmen, Plan: data.Plan, InstruksiPpa: data.InstruksiPpa}).Scan(&res)

	return res, nil
}

func (sr *rmeRepository) UpdateCPPTByCodeRepository(idCppt int, data dto.ReqCPPTUpdate) (res rme.CPPTPasienModel, err error) {
	sr.DB.Where(rme.CPPTPasienModel{IdCppt: idCppt}).Updates(rme.CPPTPasienModel{Subjektif: data.Subjektif, Objektif: data.Objektif, Asesmen: data.Asesmen, Plan: data.Plan, InstruksiPpa: data.InstruksiPpa}).Scan(&res)

	return res, nil
}

// HAPUS DATA DASKEP
func (sr *rmeRepository) HapusDaskepRepository(kdBagian string, noreg string) (res rme.Daskep, err error) {
	sr.Logging.Info("HAPUS DASKEP")
	sr.DB.Where(rme.Daskep{KdBagian: kdBagian, Noreg: noreg}).Delete(&res)
	return res, nil
}

func (sr *rmeRepository) CariDataDaskepRepository(kdBagian string, noreg string) (res rme.Daskep, err error) {
	data := sr.DB.Where(rme.Daskep{KdBagian: kdBagian, Noreg: noreg}).Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) InsertAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqAsesmenKeperawatan) (res rme.AsesmedKeperawatanBidan, err error) {
	times := time.Now()

	data := rme.AsesmedKeperawatanBidan{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: req.Asesemen.Noreg, AseskepPerolehanInfo: req.Asesemen.AseskepPerolehanInfo, AseskepCaraMasuk: req.Asesemen.AseskepAsalMasuk, AseskepCaraMasukDetail: req.Asesemen.AseskepAsalMasukDetail, AseskepAsalMasuk: req.Asesemen.AseskepAsalMasuk, AseskepAsalMasukDetail: req.Asesemen.AseskepAsalMasukDetail, AseskepBb: req.Asesemen.AseskepBb, AseskepTb: req.Asesemen.AseskepTb, AseskepRwytPnykt: req.Asesemen.AseskepRwytPnykt, AseskepRwytObatDetail: req.Asesemen.AseskepRwytObatDetail, AseskepAsesFungsional: req.Asesemen.AseskepAsesFungsional, AseskepRj1: req.Asesemen.AseskepRj1, AseskepRj2: req.Asesemen.AseskepRj2, AseskepHslKajiRj: req.Asesemen.AseskepHslKajiRj, AseskepHslKajiRjTind: req.Asesemen.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.Asesemen.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.Asesemen.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.Asesemen.AseskepLamaNyeri, AseskepNyeriMenjalar: req.Asesemen.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.Asesemen.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.Asesemen.AseskepKualitasNyeri, AseskepNyeriPemicu: req.Asesemen.AseskepNyeriPemicu, AseskepNyeriPengurang: req.Asesemen.AseskepNyeriPengurang, AseskepKehamilan: req.Asesemen.AseskepKehamilan, AseskepKehamilanGravida: req.Asesemen.AseskepKehamilanGravida, AseskepKehamilanPara: req.Asesemen.AseskepKehamilanPara, AseskepKehamilanAbortus: req.Asesemen.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.Asesemen.AseskepKehamilanHpht, AseskepKehamilanTtp: req.Asesemen.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.Asesemen.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.Asesemen.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.Asesemen.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.Asesemen.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.Asesemen.AseskepKehamilanDjj, AseskepKehamilanVt: req.Asesemen.AseskepKehamilanVt, AseskepDekubitus1: req.Asesemen.AseskepDekubitus1, AseskepDekubitus2: req.Asesemen.AseskepDekubitus2, AseskepDekubitus3: req.Asesemen.AseskepDekubitus3, AseskepDekubitus4: req.Asesemen.AseskepDekubitus4, AseskepDekubitusAnak: req.Asesemen.AseskepDekubitusAnak, AseskepPulangKondisi: req.Asesemen.AseskepPulangKondisi, AseskepPulangTransportasi: req.Asesemen.AseskepPulangTransportasi, AseskepPulangPendidikan: req.Asesemen.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.Asesemen.AseskepPulangPendidikanDetail,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) UpdateAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqAsesmenKeperawatan) (res rme.AsesmedKeperawatanBidan, err error) {

	errs := sr.DB.Where(rme.AsesmedKeperawatanBidan{Noreg: req.Asesemen.Noreg, KdBagian: kdBagian}).Updates(
		&rme.AsesmedKeperawatanBidan{
			KdBagian: kdBagian, Noreg: req.Asesemen.Noreg, AseskepPerolehanInfo: req.Asesemen.AseskepPerolehanInfo, AseskepCaraMasuk: req.Asesemen.AseskepCaraMasuk, AseskepCaraMasukDetail: req.Asesemen.AseskepAsalMasukDetail,
			AseskepAsalMasuk:       req.Asesemen.AseskepAsalMasuk,
			AseskepAsalMasukDetail: req.Asesemen.AseskepAsalMasukDetail,
			AseskepBb:              req.Asesemen.AseskepBb, AseskepTb: req.Asesemen.AseskepTb,
			AseskepRwytPnykt:      req.Asesemen.AseskepRwytPnykt,
			AseskepRwytObatDetail: req.Asesemen.AseskepRwytObatDetail,
			AseskepAsesFungsional: req.Asesemen.AseskepAsesFungsional,
			AseskepRj1:            req.Asesemen.AseskepRj1, AseskepRj2: req.Asesemen.AseskepRj2,
			AseskepHslKajiRj: req.Asesemen.AseskepHslKajiRj, AseskepHslKajiRjTind: req.Asesemen.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.Asesemen.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.Asesemen.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.Asesemen.AseskepLamaNyeri, AseskepNyeriMenjalar: req.Asesemen.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.Asesemen.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.Asesemen.AseskepKualitasNyeri, AseskepNyeriPemicu: req.Asesemen.AseskepNyeriPemicu, AseskepNyeriPengurang: req.Asesemen.AseskepNyeriPengurang, AseskepKehamilan: req.Asesemen.AseskepKehamilan, AseskepKehamilanGravida: req.Asesemen.AseskepKehamilanGravida, AseskepKehamilanPara: req.Asesemen.AseskepKehamilanPara, AseskepKehamilanAbortus: req.Asesemen.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.Asesemen.AseskepKehamilanHpht, AseskepKehamilanTtp: req.Asesemen.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.Asesemen.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.Asesemen.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.Asesemen.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.Asesemen.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.Asesemen.AseskepKehamilanDjj, AseskepKehamilanVt: req.Asesemen.AseskepKehamilanVt, AseskepDekubitus1: req.Asesemen.AseskepDekubitus1, AseskepDekubitus2: req.Asesemen.AseskepDekubitus2, AseskepDekubitus3: req.Asesemen.AseskepDekubitus3, AseskepDekubitus4: req.Asesemen.AseskepDekubitus4, AseskepDekubitusAnak: req.Asesemen.AseskepDekubitusAnak, AseskepPulangKondisi: req.Asesemen.AseskepPulangKondisi, AseskepPulangTransportasi: req.Asesemen.AseskepPulangTransportasi, AseskepPulangPendidikan: req.Asesemen.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.Asesemen.AseskepPulangPendidikanDetail,
		}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *rmeRepository) SearchDcpptSoapPasienRepository(noReg string, kdBagian string) (res rme.DcpptSoap, err error) {
	results := sr.DB.Select("kd_bagian, noreg").Where(&rme.DcpptSoap{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (sr *rmeRepository) GetAsesmenKeperawatanRepository(noReg string, kdBagian string) (res rme.AsesmedKeperawatanBidan, err error) {
	var result rme.AsesmedKeperawatanBidan

	errs := sr.DB.Select("insert_dttm", "kd_bagian", "noreg", "aseskep_perolehan_info", "aseskep_cara_masuk", "aseskep_cara_masuk_detail", "aseskep_asal_masuk", "aseskep_asal_masuk_detail",
		"aseskep_bb", "aseskep_tb", "aseskep_rwyt_pnykt", "aseskep_rwyt_obat_detail", "aseskep_ases_fungsional", "aseskep_rj1", "aseskep_rj2", "aseskep_hsl_kaji_rj",
		"aseskep_hsl_kaji_rj_tind", "aseskep_skala_nyeri", "aseskep_frekuensi_nyeri", "aseskep_lama_nyeri", "aseskep_nyeri_menjalar", "aseskep_nyeri_menjalar_detail",
		"aseskep_kualitas_nyeri", "aseskep_nyeri_pemicu", "aseskep_nyeri_pengurang", "aseskep_kehamilan", "aseskep_kehamilan_gravida", "aseskep_kehamilan_para",
		"aseskep_kehamilan_abortus", "aseskep_kehamilan_hpht", "aseskep_kehamilan_ttp", "aseskep_kehamilan_leopold1", "aseskep_kehamilan_leopold2", "aseskep_kehamilan_leopold3",
		"aseskep_kehamilan_leopold4", "aseskep_kehamilan_djj", "aseskep_kehamilan_vt", "aseskep_dekubitus1", "aseskep_dekubitus2", "aseskep_dekubitus3", "aseskep_dekubitus4",
		"aseskep_dekubitus_anak", "aseskep_pulang_kondisi", "aseskep_pulang_transportasi", "aseskep_pulang_pendidikan", "aseskep_pulang_pendidikan_detail").Where(&rme.AsesmedKeperawatanBidan{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&result).Error

	if errs != nil {
		return result, errs
	}

	return result, nil
}

func (sr *rmeRepository) GetSikiByKodeRepository(kode string) (res rme.SIKIModel, err error) {
	result := sr.DB.Where(&rme.SIKIModel{Kode: kode}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetSlkiByKodeRepository(kode string) (res rme.SLKIModel, err error) {
	result := sr.DB.Where(&rme.SLKIModel{Kode: kode}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetSdkiByKodeRepository(kode string) (res rme.SDKIModel, err error) {
	result := sr.DB.Where(&rme.SDKIModel{Kode: kode}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetDasekepRepository(noReg string, kdBagian string) (res []rme.DaskepModel, err error) {

	var data []rme.DaskepModel

	result := sr.DB.Where("kd_bagian = ? AND noreg = ? AND nilai >= ?", kdBagian, noReg, 1).Find(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return data, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) GetDaskepResponseRepository(noReg string, kdBagian string) (res []rme.RMEDaskep, err error) {

	query := `SELECT d.no_urut, d.nilai, ki.judul, ki.defenisi, ki.ekspektasi, ki.menurun, ki.meningkat, ki.memburuk,  d.kode_sdki AS sdki, d.kode_siki AS siki, d.tanggal, d.noreg  FROM vicore_rme.daskep AS d INNER JOIN vicore_rme.slki AS ki ON ki.kode = d.kode_slki  && d.no_urut = ki.no_urut WHERE d.noreg=? AND d.kd_bagian=? AND d.nilai >0`

	result := sr.DB.Raw(query, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// GET ALL SIKI
func (sr *rmeRepository) GetAllSIKIRepository() (res []rme.SIKIModel, err error) {

	result := sr.DB.Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetAllSDKIRepository() (res []rme.SDKIModel, err error) {

	result := sr.DB.Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetALLSLKIRepository() (res []rme.SLKIModel, err error) {

	result := sr.DB.Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// INSERT SIKI MODEL
func (sr *rmeRepository) InsertDeskripsiSikiModelRepository(data rme.DeskripsiSikiModel) (res rme.DeskripsiSikiModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

// INSERT DATA KRITERIA HASIL DESKRIPSI
func (sr *rmeRepository) InsertHasilSLKIRepository(data rme.DKriteriaHasilSLKI) (res rme.DKriteriaHasilSLKI, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) InsertDataSLKIRespository(data rme.DSlki) (res rme.DSlki, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) UpdateSKDIByKodeRepository(kode string, kodeSDKI string) (res rme.DSlki, err error) {
	// Update with conditions
	result := sr.DB.Model(&rme.DSlki{}).Where("kode_slki = ?", kode).Update("kode_sdki", kodeSDKI).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) SearchDataSLKIRepository(kode string) (res rme.DSlki, err error) {
	result := sr.DB.Where(&rme.DSlki{KodeSlki: kode}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) SearchSikiRepository(kodeSiki string) (res []rme.DeskripsiSikiModel, err error) {
	query := `SELECT * FROM vicore_rme.deskripsi_siki WHERE kode_siki=?  ORDER BY kategori asc`

	result := sr.DB.Raw(query, kodeSiki).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) InsertIntervensiRisikoRespository(data rme.DIntervensiRisiko) (res rme.DIntervensiRisiko, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnSavePemberianTerapiCairanRepository(data rme.DPemberianCairan) (res rme.DPemberianCairan, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnDeleteCairanIntakeRepository(ID int) (res rme.CairanIntake, err error) {
	result := sr.DB.Where(&rme.CairanIntake{IDCairan: ID}).Delete(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteOutPutRepository(ID int) (res rme.DcairanOutPut, err error) {

	result := sr.DB.Where(&rme.DcairanOutPut{IDCairan: ID}).Delete(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveDPemeriksaanFisikReporitory(data rme.DPemFisikModel) (res rme.DPemFisikModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnUpdateDpenkajianPersistemRepository(data igd.DpengkajianPersistem, noReg string, pelayanan string) (res igd.DpengkajianPersistem, err error) {
	result := sr.DB.Model(&igd.DpengkajianPersistem{}).Where(&igd.DpengkajianPersistem{NoReg: noReg, Pelayanan: pelayanan}).Updates(&data).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

// UPDATE DATA PEMERIKSAAN FISIK
func (sr *rmeRepository) OnUpdatePemeriksaanFisikRepository(update igd.DpengkajianPersistem, noReg string, pelayanan string) (res igd.DpengkajianPersistem, err error) {
	result := sr.DB.Model(&igd.DpengkajianPersistem{}).Where(&igd.DpengkajianPersistem{NoReg: noReg, Pelayanan: pelayanan}).Updates(&update).Scan(&update)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnSaveDpengkajianPersistemRepository(data igd.DpengkajianPersistem) (res igd.DpengkajianPersistem, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnGetPengkajianDPemFIsikRepository(Kategori string, Person string, KDBagian string, Pelayanan string) (res rme.DPemFisikModel, err error) {
	// TEKNIK SIPIL
	result := sr.DB.Where(&rme.DPemFisikModel{
		Kategori:  Kategori,
		KetPerson: Person,
		KdBagian:  KDBagian,
		Pelayanan: Pelayanan,
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}
	return res, nil
}

func (sr *rmeRepository) OnSaveDCairanIntakeRepository(data rme.CairanIntake) (res rme.CairanIntake, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *rmeRepository) OnGetDCairanInTakeRepository(noReg string) (res []rme.CairanIntake, err error) {
	result := sr.DB.Where(&rme.CairanIntake{Noreg: noReg}).Preload("Perawat").Preload("Pelayanan").Order("id_cairan asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetCairanOutPutRepository(noReg string) (res []rme.DcairanOutPut, err error) {
	result := sr.DB.Where(&rme.DcairanOutPut{Noreg: noReg}).Preload("Perawat").Preload("Pelayanan").Order("id_cairan asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveDCairanOutPutRepository(data rme.DcairanOutPut) (res rme.DcairanOutPut, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *rmeRepository) OnGetPemberianTerapiCairanRepository(noReg string) (res []rme.DPemberianCairan, err error) {
	result := sr.DB.Where(&rme.DPemberianCairan{Noreg: noReg}).Preload("Perawat").Preload("Pelayanan").Order("id asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) SearchIntervensiRisikoRepository(kdBagian string, noReg string, shift string) (res rme.DIntervensiRisiko, err error) {
	times := time.Now()
	result := sr.DB.Where("kd_bagian=? AND noreg=? AND shift=? AND YEAR(insert_dttm)=?  AND MONTH(insert_dttm)=?  AND DAY(insert_dttm)=?", kdBagian, noReg, shift, times.Format("2006"), times.Format("01"), times.Format("02")).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetDataIntervensiIntervensiResikoJatuhReporitory(kdBagian string, noReg string, shift string) (res rme.DIntervensiRisiko, err error) {

	times := time.Now()

	query := `SELECT * FROM vicore_rme.dintervensi_risiko AS a WHERE YEAR(a.insert_dttm)=? AND MONTH(a.insert_dttm)=? AND DAY(a.insert_dttm)=? AND kd_bagian=?  AND noreg=? AND shift=?`

	result := sr.DB.Raw(query, times.Format("2006"), times.Format("01"), times.Format("02"), kdBagian, noReg, shift).Find(&res)

	sr.Logging.Info("DATA INTERVENSI RESIKO")
	sr.Logging.Info(result.Statement)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *rmeRepository) UpdateIntervensiRisikoRepository(kdBagian string, req dto.ReqIntervensiResikoJatuh) (res rme.DIntervensiRisiko, err error) {
	times := time.Now()
	var data rme.DIntervensiRisiko

	query := `UPDATE vicore_rme.dintervensi_risiko SET resiko1=?, resiko2=?, resiko3=?, resiko4=?, resiko5=?, resiko6=?, resiko7=?, resiko8=? , resiko9=?, resiko10=?, resiko11=?, resiko12=?, resiko13=? , resiko14=? , resiko15=?, resiko16=?, resiko17=? WHERE shift=? AND kd_bagian=? AND noreg =? AND  YEAR(insert_dttm)=? AND MONTH(insert_dttm)=? AND DAY(insert_dttm)=? LIMIT 1`

	result := sr.DB.Raw(query,
		req.RisikoJatuh[0].KResikoJatuhPasien[0].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[1].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[2].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[3].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[4].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[5].IsEnable,
		req.RisikoJatuh[0].KResikoJatuhPasien[6].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[0].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[1].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[2].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[3].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[4].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[5].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[6].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[7].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[8].IsEnable,
		req.RisikoJatuh[1].KResikoJatuhPasien[9].IsEnable,
		req.Shift, kdBagian, req.Noreg, times.Format("2006"), times.Format("01"), times.Format("02")).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) GetHasilResikoJatuhRepository(noReg string) (res rme.HasilResikoJatuhPasien, err error) {
	query := `SELECT  aseskep_rj1 AS rj1, aseskep_rj2 AS rj2, aseskep_hsl_kaji_rj AS hasil, aseskep_hsl_kaji_rj_tind AS tindakan, noreg, kd_bagian FROM vicore_rme.dcppt_soap_pasien WHERE noreg=? AND kd_bagian="IGD001"`

	result := sr.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// GET DESKRIPSI SIKI RESPOSITORY
func (sr *rmeRepository) GetDeskripsiSLKIRespository(kodeSLKI string) (res []rme.DKriteriaSLKIModel, err error) {
	result := sr.DB.Where(&rme.DKriteriaSLKIModel{KodeSlki: kodeSLKI}).Order("no_urut asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) GetDataDSLKIRepository(kodeSLKI string) (res rme.DSlki, err error) {
	result := sr.DB.Where(&rme.DSlki{KodeSlki: kodeSLKI}).Preload("DKriteriaSLKIModel", func(db *gorm.DB) *gorm.DB {
		return db.Order("vicore_rme.dkriteria_hasil_slki.no_urut ASC")
	}).Order("kode_slki asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveDCPPTSBARRepository(data rme.DataCPPTSBAR) (res rme.DataCPPTSBAR, err error) {

	result := sr.DB.Create(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return data, nil
}

func (sr *rmeRepository) OnGetDCPPTSBARRepository(noReg string) (res []rme.CPPTSBAR, err error) {

	query := `SELECT id_cppt , insert_dttm, insert_user_id, kelompok, pelayanan, kd_bagian, tanggal, noreg,  kp.namaperawat, kd.namadokter, situation, background, recomendation, asesmen, instruksi_ppa  FROM vicore_rme.dcppt AS cp LEFT JOIN his.kperawat as kp ON kp.idperawat=cp.insert_user_id LEFT JOIN his.ktaripdokter AS kd ON kd.iddokter=cp.insert_user_id WHERE cp.noreg=? and cp.situation!=''`

	result := sr.DB.Raw(query, noReg).Scan(&res)

	sr.Logging.Info("DATA INTERVENSI RESIKO")
	sr.Logging.Info(result.Statement)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// UPATE DATA SLKI
func (sr *rmeRepository) UpdateDaskepSLKIRepository(hasil int, noDaskep string, kodeSLKI string, idKriteria int) (res rme.DaskepSLKIModel, err error) {
	times := time.Now()

	var data rme.SkalaTriaseModel

	query := `UPDATE vicore_rme.daskep_slki SET hasil=?, upd_dttm=?  WHERE  no_daskep=? AND kode_slki=? AND id_kriteria =?  LIMIT 1`

	result := sr.DB.Raw(query, hasil, times.Format("2006-01-02 15:04:05"), noDaskep, kodeSLKI, idKriteria).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

// UBAH STATUS DIAGNOSA DASKEP
func (sr *rmeRepository) UpdateDasekepDiagnosaRepository(noDaskep string, noReg string) (res rme.DaskepSLKIModel, err error) {
	times := time.Now()

	var data rme.SkalaTriaseModel

	query := `UPDATE vicore_rme.daskep_diagnosa SET status='Closed', upd_dttm=?  WHERE  no_daskep=? AND noreg=?  LIMIT 1`

	result := sr.DB.Raw(query, times.Format("2006-01-02 15:04:05"), noDaskep, noReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveRiwayatKeluargaRepository(noRm string, insertUser string, kdBagian string, alergi string) (res rme.DAlergi, err error) {

	times := time.Now()

	var dat = rme.DAlergi{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		Id:         noRm,
		Kelompok:   "keluarga",
		InsertUser: insertUser,
		Alergi:     alergi,
		KdBagian:   kdBagian,
	}

	result := sr.DB.Create(&dat)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return dat, nil
}

func (sr *rmeRepository) OnSaveKartuObservasiRepository(userID string, kdBagian string, req dto.ReqOnSaveKartuObservasi) (err error) {
	times := time.Now()

	result := sr.DB.Create(&rme.DKartuObservasi{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		KdBagian:     kdBagian,
		Noreg:        req.NoReg,
		T:            req.T,
		N:            req.N,
		P:            req.P,
		S:            req.S,
		Cvp:          req.CVP,
		Ekg:          req.EKG,
		Kesadaran:    req.Kesadaran,
		AnggotaBadan: req.AnggotaGerak,
		PupilKi:      req.UkuranKi,
		PupilKa:      req.UkuranKa,
		RedaksiKi:    req.RedaksiKi,
		RedaksiKa:    req.RedaksiKa,
		SputumWarna:  req.SputumWarna,
		IsiCup:       req.IsiCup,
		Keterangan:   req.Keterangan,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return errors.New(message)
	}

	return nil
}

func (sr *rmeRepository) OnGetKartuObservasiRepository(noReg string) (res []rme.DKartuObservasi, err error) {
	result := sr.DB.Where(&rme.DKartuObservasi{Noreg: noReg}).Order("id_kartu asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteCairanRepository(kodeCairan int) (res rme.DKartuCairanObatObatan, err error) {
	result := sr.DB.Where(&rme.DKartuCairanObatObatan{IdKartu: kodeCairan}).Delete(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteKartuObservasiRepository(kodeObservasi int) (res rme.DKartuObservasi, err error) {
	result := sr.DB.Where(&rme.DKartuObservasi{IdKartu: kodeObservasi}).Delete(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetKartuCairanRepository(noReg string) (res []rme.DKartuCairanObatObatan, err error) {
	result := sr.DB.Where(&rme.DKartuCairanObatObatan{Noreg: noReg}).Order("id_kartu asc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSaveKartuCairanRepository(userID string, kdBagian string, req dto.OnSaveKartuCairan) (res rme.DKartuCairanObatObatan, message string, err error) {
	times := time.Now()

	result := sr.DB.Create(&rme.DKartuCairanObatObatan{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		Noreg:             req.Noreg,
		UserId:            userID,
		CairanMasuk1:      req.CairanMasuk1,
		CairanMasuk2:      req.CairanMasuk2,
		CairanMasuk3:      req.CairanMasuk3,
		CairanMasukNgt:    req.CairanMasukNgt,
		NamaCairan:        req.NamaCairan,
		CairanKeluarUrine: req.CairanKeluarUrine,
		CairanKeluarNgt:   req.CairanKeluarNgt,
		KdBagian:          kdBagian,
		DrainDll:          req.Drain,
		Keterangan:        req.Keterangan,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, message, errors.New(message)
	}

	return res, "Data berhasil disimpan", nil

}

func (sr *rmeRepository) OnSaveRiwayatKehamilanLaluPerinaRepository(userID string, kdBagian string, req dto.ReqRiwayatKehamilanPerina) (res rme.RiwayatKehamilanPerina, message string, err error) {

	times := time.Now()

	result := sr.DB.Create(&rme.RiwayatKehamilanPerina{
		InsertDttm:           times.Format("2006-01-02 15:04:05"),
		KdRiwayat:            "RWT_" + uuid.NewString(),
		InsertPc:             req.DeviceID,
		InsertUserId:         userID,
		TahunPersalinan:      req.TahunKelahiran,
		KdBagian:             kdBagian,
		Jk:                   req.JenisKelamin,
		Bb:                   req.BeratBadan,
		NoRm:                 req.NoRm,
		Noreg:                req.Noreg,
		KeadaanSekarang:      req.Keadaan,
		TempatPersalinan:     req.TempatPersalinan,
		KomplikasiHamil:      req.KomplikasiKehamilan,
		KomplikasiPersalinan: req.KomplikasiPersalinan,
		JenisPersalinan:      req.JenisPersalinan,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, message, errors.New(message)
	}

	return res, "Data berhasil disimpan", nil
}

func (sr *rmeRepository) OnUpdateCPPTSBARRepository(idCPpt int, update rme.DataCPPTSBAR) (res rme.DataCPPTSBAR, err error) {
	result := sr.DB.Model(&rme.DataCPPTSBAR{}).Where("id_cppt = ?", idCPpt).Updates(&update).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) OnUpdateKartuObservasiRepository(idKartu int, data rme.DKartuObservasi) (res rme.DKartuObservasi, err error) {
	result := sr.DB.Where(&rme.DKartuObservasi{IdKartu: idKartu}).Updates(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdateKartuCairanRepository(idKartu int, data rme.DKartuCairanObatObatan) (res rme.DKartuCairanObatObatan, err error) {
	result := sr.DB.Where(&rme.DKartuCairanObatObatan{IdKartu: idKartu}).Updates(&data)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnDeteleCPTTSBARRepository(idCPPT int) (res rme.DataCPPTSBAR, err error) {
	result := sr.DB.Where(rme.DataCPPTSBAR{IdCppt: idCPPT}).Delete(&res)

	if result.Error != nil {
		return res, result.Error
	}
	return res, nil
}

func (sr *rmeRepository) GetAnalisaDataRepository(noReg string) (res []rme.DAnalisaDataModel, err error) {
	result := sr.DB.Where(&rme.DAnalisaDataModel{Noreg: noReg}).Preload("AnalisaData").Preload("Pengawai").Order("id_analisa desc").Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnGetReporAsesmenAwalKeperawatanIGDAntonioRepository(noReg string) (res rme.PengkajianPersistemIGD, errr error) {
	query := `SELECT insert_dttm, upd_dttm, keterangan_person, insert_user_id, insert_pc,tgl_masuk, tgl_keluar, jam_check_out, jam_check_in_ranap, kd_bagian, noreg, kd_dpjp, aseskep_sistem_merokok, aseskep_sistem_minum_alkohol, aseskep_sistem_spikologis, aseskep_sistem_gangguan_jiwa, aseskep_sistem_bunuh_diri, aseskep_sistem_trauma_psikis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_spiritual, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_penghasilan, aseskep_sistem_kultural, aseskep_sistem_alat_bantu, aseskep_sistem_kebutuhan_khusus FROM vicore_rme.dcppt_soap_pasien WHERE noreg=? AND kd_bagian=? AND keterangan_person=? LIMIT 1`

	result := sr.DB.Raw(query, noReg, "IGD001", "Perawat").Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *rmeRepository) OnGetPengkajianAwalIGDRepository(noReg string, kdBagian string, person string) (res rme.PengkajianPersistemIGD, errr error) {

	query := `SELECT insert_dttm, upd_dttm, keterangan_person, insert_user_id, insert_pc,tgl_masuk, tgl_keluar, jam_check_out, jam_check_in_ranap, kd_bagian, noreg, kd_dpjp, aseskep_sistem_merokok, aseskep_sistem_minum_alkohol, aseskep_sistem_spikologis, aseskep_sistem_gangguan_jiwa, aseskep_sistem_bunuh_diri, aseskep_sistem_trauma_psikis, aseskep_sistem_hambatan_sosial, aseskep_sistem_hambatan_spiritual, aseskep_sistem_hambatan_ekonomi, aseskep_sistem_penghasilan, aseskep_sistem_kultural, aseskep_sistem_alat_bantu, aseskep_sistem_kebutuhan_khusus FROM vicore_rme.dcppt_soap_pasien WHERE noreg=? AND kd_bagian=? AND keterangan_person=? LIMIT 1`

	result := sr.DB.Raw(query, noReg, kdBagian, person).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, data gagal didapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnSavePengkajianPersistemIGDRepository(kdBagian string, person string, userID string, req dto.ReqSaveAsesmenIGD) (res rme.PengkajianPersistemIGD, err error) {
	times := time.Now()
	sr.Logging.Info(times)

	tanggal, _ := sr.soapRepository.GetJamMasukPasienRepository(req.Noreg)

	sr.Logging.Info(req.Noreg)

	result := sr.DB.Create(&rme.PengkajianPersistemIGD{
		InsertDttm:                     times.Format("2006-01-02 15:04:05"),
		UpdDttm:                        times.Format("2006-01-02 15:04:05"),
		KeteranganPerson:               person,
		InsertUserId:                   userID,
		InsertPc:                       req.DeviceID,
		TglMasuk:                       tanggal.Tanggal[0:10],
		Noreg:                          req.Noreg,
		KdBagian:                       kdBagian,
		AseskepSistemMerokok:           req.AseskepSistemMerokok,
		AseskepSistemMinumAlkohol:      req.AseskepSistemMinumAlkohol,
		AseskepSistemSpikologis:        req.AseskepSistemSpikologis,
		AseskepSistemGangguanJiwa:      req.AseskepSistemGangguanJiwa,
		AseskepSistemBunuhDiri:         req.AseskepSistemBunuhDiri,
		AseskepSistemTraumaPsikis:      req.AseskepSistemTraumaPsikis,
		AseskepSistemHambatanSosial:    req.AseskepSistemHambatanSosial,
		AseskepSistemHambatanSpiritual: req.AseskepSistemHambatanSpiritual,
		AseskepSistemHambatanEkonomi:   req.AseskepSistemHambatanEkonomi,
		AseskepSistemPenghasilan:       req.AseskepSistemPenghasilan,
		AseskepSistemKultural:          req.AseskepSistemKultural,
		AseskepSistemAlatBantu:         req.AseskepSistemAlatBantu,
		AseskepSistemKebutuhanKhusus:   req.AseskepSistemKebutuhanKhusus,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdatePengkajianPersistemIGDRepository(noReg string, kdbagian string, update rme.PengkajianPersistemIGD) (res rme.PengkajianPersistemIGD, err error) {
	result := sr.DB.Model(&rme.PengkajianPersistemIGD{}).Where("noreg = ? AND kd_bagian=?", noReg, kdbagian).Updates(&update).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) OnUpdateAnalisaDataRepository(kodeAnalisa string, update rme.DAnalisaDataModel) (res rme.DAnalisaDataModel, err error) {
	result := sr.DB.Model(&rme.DAnalisaDataModel{}).Where("kode_analisa = ?", kodeAnalisa).Updates(&update).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *rmeRepository) SaveDImplementasiKeperawatanRepository(req dto.OnSaveImplementasiKeperawatan, userID string, kdBagian string) (res rme.DImplementasiTindakanKeperawatan, err error) {
	times := time.Now()

	result := sr.DB.Create(&rme.DImplementasiTindakanKeperawatan{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		NoDaskep:   req.NoDaskep,
		UserId:     userID,
		NoReg:      req.Noreg,
		KdBagian:   kdBagian,
		Deskripsi:  req.Deskripsi,
	})

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak dapat disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *rmeRepository) OnDeleteAnalisaDataRepository(kodeAnalisa string) (message string, err error) {
	// DELETE ANALISA DATA
	var problem = rme.DAnalisaProblem{}
	sr.DB.Where(rme.DAnalisaProblem{KodeAnalisa: kodeAnalisa}).Delete(&problem)

	var analisa = rme.DAnalisaData{}
	result := sr.DB.Where(rme.DAnalisaData{KodeAnalisa: kodeAnalisa}).Delete(&analisa)

	if result.Error != nil {
		return "Data gagal dihapus", result.Error
	}

	return "Data berhasil dihapus", nil
}

func (sr *rmeRepository) OnSaveVitalSignICURepository() (message string, err error) {
	return message, nil
}
