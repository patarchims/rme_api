package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
)

func (sr *rmeRepository) OnGetResikoJatuhPasienDewasaRepository(noReg string, kdBagian string) (res []rme.DRisikoJatuh, err error) {
	data := sr.DB.Where(rme.DRisikoJatuh{
		Noreg:    noReg,
		KdBagian: kdBagian,
	}).Preload("MutiaraPengajar").Order("id_ews asc").Find(&res)

	if data.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", data.Error.Error())
		sr.Logging.Info(message)
		return res, errors.New(message)
	}

	return res, nil
}
