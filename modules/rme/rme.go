package rme

import (
	"time"
)

type (
	DCPPT struct {
		TglMasuk     time.Time `json:"tglMasuk"`
		TglKeluar    time.Time `json:"tglKeluar"`
		JamCheckOut  time.Time `json:"jamCheckOut"`
		Noreg        string    `json:"noReg"`
		KdDokter     string    `json:"kdDokter"`
		Resep        string    `json:"resep"`
		PrimaryIcd   string    `json:"primaryICD"`
		SecondaryIcd string    `json:"secondaryICD"`
		Prosedur     string    `json:"prosedur"`
	}

	DCCPTSoapPasien struct {
		TglMasuk       time.Time `json:"tglMasuk"`
		TglKeluar      time.Time `json:"TglKeluar"`
		JamCheckOut    time.Time `json:"jamCheckOut"`
		KdBagian       string    `json:"kdBagian"`
		Noreg          string    `json:"noReg"`
		KdPpa          string    `json:"kodePpa"`
		KdDpjp         string    `json:"KdDpjp"`
		Subjectif      string    `json:"subjectif"`
		Objectif       string    `json:"objectif"`
		Asesmen        string    `json:"Asesmen"`
		Plan           string    `json:"plan"`
		PpaPascaBedah  string    `json:"ppaPascaBedah"`
		IcdKerja       string    `json:"IcdKerja"`
		IcdDeferensial string    `json:"IcdDeferensial"`
		Icd9           string    `json:"Icd9"`
		Note           string    `json:"note"`
	}

	DResumeMedik struct {
		TglMasuk         time.Time `json:"tglMasuk"`
		TglKeluar        time.Time `json:"TglKeluar"`
		JamCheckOut      time.Time `json:"jamCheckOut"`
		Id               string    `json:"id"`
		Noreg            string    `json:"noreg"`
		KdDokter         string    `json:"kodeDokter"`
		Resep            string    `json:"resep"`
		PrimaryIcd       string    `json:"primaryID"`
		SecondarIcd      string    `json:"secondaryID"`
		Procedure        string    `json:"procedure"`
		Kasus            string    `json:"kasus"`
		KasusMeninggal   string    `json:"kasusMeninggal"`
		Triase           string    `json:"Triase"`
		JenisKasus       string    `json:"jenisKasus"`
		TindakLanjut     string    `json:"tindakLajut"`
		ResponIGD        string    `json:"responIGD"`
		TipeKunjungan    string    `json:"tipeKunjungan"`
		GolonganPenyakit string    `json:"golonganPenyakit"`
		Emergency        string    `json:"emergency"`
	}

	// SLKI MODEL
	SLKIModel struct {
		Kode       string `json:"kode"`
		Judul      string `json:"judul"`
		Defenisi   string `json:"defenisi"`
		Ekspektasi string `json:"ekspektasi"`
		NoUrut     int    `json:"no_urut"`
		Tanda      string `json:"tanda"`
		Menurun    string `json:"menurun"`
		Meningkat  string `json:"meningkat"`
		Memburuk   string `json:"memburuk"`
	}

	//  SLKI DESKRIPSI MODEL
	DKriteriaHasilSLKI struct {
		IdKriteria   int
		KodeSlki     string
		NamaKriteria string
		Tanda        bool
		Kategori     string
		NoUrut       int
	}

	Daskep struct {
		InsertDttm string `json:"insert_dttm"`
		Tanggal    string `json:"tanggal"`
		KdBagian   string `json:"kd_bagian"`
		Noreg      string `json:"noreg"`
		NoUrut     int    `json:"no_urut"`
		Nilai      int    `json:"nilai"`
		KodeSdki   string `json:"kode_sdki"`
		KodeSlki   string `json:"kode_slki"`
		KodeSiki   string `json:"kode_siki"`
		Hasil      string `json:"hasil"`
	}

	SIKIModel struct {
		Kode       string `gorm:"column:kode;" json:"kode"`
		Judul      string `gorm:"column:judul" json:"judul"`
		Observasi  string `gorm:"column:observasi" json:"observasi"`
		Terapeutik string `gorm:"column:terapeutik" json:"terapeutik"`
		Edukasi    string `gorm:"column:edukasi" json:"edukasi"`
		Kolaborasi string `gorm:"column:kolaborasi" json:"kolaborasi"`
	}

	DeskripsiModel struct {
		KodeSiki  string `json:"kode_siki"`
		Deskripsi string `json:"deskripsi"`
		Kategori  string `json:"kategori"`
	}

	DaskepModel struct {
		InsertDttm string `json:"insert_dttm"`
		Tanggal    string `json:"tanggal"`
		KdBagian   string `json:"kd_bagian"`
		Noreg      string `json:"noreg"`
		NoUrut     int    `json:"no_urut"`
		Nilai      int    `json:"nilai"`
		Hasil      string `json:"hasil"`
		KodeSiki   string `gorm:"primaryKey:KodeSiki" json:"kode_siki"`
		KodeSlki   string `gorm:"primaryKey:KodeSlki" json:"kode_slki"`
		KodeSdki   string `gorm:"primaryKey:KodeSdki" json:"kode_sdki"`
	}

	RMEDaskep struct {
		NoUrut    int    `json:"no_urut"`
		Nilai     int    `json:"nilai"`
		Judul     string `json:"judul"`
		Defenisi  string `json:"defenisi"`
		Ekspetasi string `json:"ekspetasi"`
		Menurun   string `json:"menurun"`
		Meningkat string `json:"meningkat"`
		Memburuk  string `json:"memburuk"`
		Sdki      string `json:"sdki"`
		Siki      string `json:"siki"`
		Tanggal   string `json:"tanggal"`
	}

	// SDKI MODEL ===============
	SDKIModel struct {
		Kode                 string `gorm:"primaryKey:Kode" json:"kode"`
		Judul                string `json:"judul"`
		Defenisi             string `json:"defenisi"`
		Subjectif            string `json:"subjectif"`
		Objectif             string `json:"objectif"`
		Problem              string `json:"problem"`
		Etiologi             string `json:"etiologi"`
		EtiologiFisiologis   string `json:"etiologi_fisiologis"`
		EtiologiSituasional  string `json:"etiologi_situasional"`
		FaktorResiko         string `json:"faktor_resiko"`
		GejalaMayorSubjektif string `json:"gejala_mayor_subjektif"`
		GejalaMayorObjektif  string `json:"gejala_mayor_objectif"`
		GejalaMinorObjektif  string `json:"gejalan_minor_objectif"`
		KondisiKlinisTerkait string `json:"kondisi_klinis_terkait"`
		MappingSlki          string `json:"mapping_slki"`
		MappingSiki          string `json:"mapping_siki"`
	}

	AsesmedKeperawatanBidan struct {
		InsertDttm                    string `json:"insertDttm"`
		KdBagian                      string `json:"kdBagian"`
		Noreg                         string `json:"noreg"`
		AseskepPerolehanInfo          string `json:"info"`
		AseskepCaraMasuk              string `json:"cara_masuk"`
		AseskepCaraMasukDetail        string `json:"cara_masuk_detail"`
		AseskepAsalMasuk              string `json:"asal_masuk"`
		AseskepAsalMasukDetail        string `json:"asal_masuk_detail"`
		AseskepBb                     string `json:"bb"`
		AseskepTb                     string `json:"tb"`
		AseskepRwytPnykt              string `json:"rwt_penyakit"`
		AseskepRwytObatDetail         string `json:"obat_detail"`
		AseskepAsesFungsional         string `json:"fungsional"`
		AseskepRj1                    string `json:"rj1"`
		AseskepRj2                    string `json:"rj2"`
		AseskepHslKajiRj              string `json:"kaji_rj"`
		AseskepHslKajiRjTind          string `json:"kaji_rj_tindakan"`
		AseskepSkalaNyeri             int    `json:"skala_nyeri"`
		AseskepFrekuensiNyeri         string `json:"frekuensi_nyeri"`
		AseskepLamaNyeri              string `json:"lama_nyeri"`
		AseskepNyeriMenjalar          string `json:"nyeri_menjalar"`
		AseskepNyeriMenjalarDetail    string `json:"menjalar_detail"`
		AseskepKualitasNyeri          string `json:"kualitas_nyeri"`
		AseskepNyeriPemicu            string `json:"nyeri_pemicu"`
		AseskepNyeriPengurang         string `json:"nyeri_pengurang"`
		AseskepKehamilan              string `json:"kehamilan"`
		AseskepKehamilanGravida       string `json:"kehamilan_gravida"`
		AseskepKehamilanPara          string `json:"kehamilan_para"`
		AseskepKehamilanAbortus       string `json:"kehamilan_abortus"`
		AseskepKehamilanHpht          string `json:"kehamilan_hpht"`
		AseskepKehamilanTtp           string `json:"kehamilan_ttp"`
		AseskepKehamilanLeopold1      string `json:"kehamilan_leopol1"`
		AseskepKehamilanLeopold2      string `json:"kehamilan_leopol2"`
		AseskepKehamilanLeopold3      string `json:"kehamilan_leopol3"`
		AseskepKehamilanLeopold4      string `json:"kehamilan_leopol4"`
		AseskepKehamilanDjj           string `json:"kehamilan_djj"`
		AseskepKehamilanVt            string `json:"kehamilan_vt"`
		AseskepDekubitus1             string `json:"dekubitus1"`
		AseskepDekubitus2             string `json:"dekubitus2"`
		AseskepDekubitus3             string `json:"dekubitus3"`
		AseskepDekubitus4             string `json:"dekubitus4"`
		AseskepDekubitusAnak          string `json:"dekubitus_anak"`
		AseskepPulangKondisi          string `json:"pulang_kondisi"`
		AseskepPulangTransportasi     string `json:"pulang_transportasi"`
		AseskepPulangPendidikan       string `json:"pendidikan"`
		AseskepPulangPendidikanDetail string `json:"pendidikan_detail"`
	}

	CpptModel struct {
		InsertDttm   string
		UpdDttm      string
		InsertUserId string
		InsertPc     string
		IdCppt       int
		Kelompok     string

		KdBagian     string
		Tanggal      string
		Noreg        string
		Dpjp         string
		Subjektif    string
		Objektif     string
		Asesmen      string
		Plan         string
		PpaFingerTtd string
		PpaFingerJam string
		InstruksiPpa string

		Namadokter  string
		Namaperawat string
		Namabagian  string
		Id          string
	}

	CPPTPasienModel struct {
		InsertDttm   string
		UpdDttm      string
		InsertUserId string
		InsertPc     string
		Kelompok     string
		Pelayanan    string
		IdCppt       int

		KdBagian     string
		Tanggal      string
		Noreg        string
		Dpjp         string
		Subjektif    string
		Objektif     string
		Asesmen      string
		Plan         string
		PpaFingerTtd string
		PpaFingerJam string
		InstruksiPpa string
	}

	// =====

	DcpptSoap struct {
		KdBagian string `json:"kdBagian"`
		Noreg    string `json:"noreg"`
	}

	// GET RESPONSE
	GetResponseAsesmen struct {
		AsesmedKeperawatanBidan AsesmedKeperawatanBidan `json:"asemen"`
		Daskep                  []RMEDaskep             `json:"daskep"`
		Siki                    SIKIModel               `json:"siki"`
		Sdki                    SDKIModel               `json:"skdi"`
	}

	// REPAIR ASUHAN
	// KEPERAWATAN
	GetResponseAsuhanKeperawatan struct {
		Daskep []RMEDaskep `json:"daskep"`
		Siki   SIKIModel   `json:"siki"`
		Sdki   SDKIModel   `json:"skdi"`
	}

	SikiDeskripsiModel struct {
		Judul              string               `json:"judul"`
		Kode               string               `gorm:"primaryKey:Kode" json:"kode"`
		DeskripsiSikiModel []DeskripsiSikiModel `gorm:"foreignKey:KodeSiki" json:"deskripsi"`
	}

	// DESKRIPSI SIKI
	DeskripsiSikiModel struct {
		IdSiki    int    `json:"id_siki"`
		KodeSiki  string `json:"kode_siki"`
		NoUrut    int    `json:"no_urut"`
		Kategori  string `json:"kategori"`
		Deskripsi string `json:"deskripsi"`
	}

	SkalaTriaseModel struct {
		InsertDttm     string
		UpdateDttm     string
		InsertDevice   string
		InsertUserId   string
		KetPerson      string
		Pelayanan      string
		Kategori       string
		KdBagian       string
		Noreg          string
		SkalaNyeri     int
		SkalaNyeriP    string
		SkalaNyeriQ    string
		SkalaNyeriR    string
		SkalaNyeriS    string
		SkalaNyeriT    string
		FlaccWajah     int
		FlaccKaki      int
		FlaccAktifitas int
		FlaccMenangis  int
		FlaccBersuara  int
		FlaccTotal     int
		SkalaTriase    int
	}

	VitalSignDokter struct {
		InsertDttm       string
		UpdDttm          string
		InsertDevice     string
		InsertUserId     string
		KetPerson        string
		Pelayanan        string
		KdBagian         string
		Kategori         string
		Noreg            string
		Td               string
		Suhu             string
		Pernafasan       string
		PernafasanDetail string
		Nadi             string
		Tb               string
		Bb               string
		Spo2             string `gorm:"column:spo2"`
	}

	DVitalSignModel struct {
		InsertDttm        string
		UpdDttm           string
		InsertUserId      string
		InsertDevice      string
		Kategori          string
		KetPerson         string
		Pelayanan         string
		KdBagian          string
		Noreg             string
		Td                string
		Hr                string
		Suhu              string
		Pernafasan        string
		PernafasanDetail  string
		Nadi              string
		NadiDetail        string
		Tb                string
		Bb                string
		Spo2              string `gorm:"column:spo2"`
		LingkarKepala     string
		LingkarKepalaAtas string
		LingkarLengan     string
		LingkarDada       string
		LingkarPerut      string
		WarnaKulit        string
		KeadaanUmum       string
		// DVITAL SIGN PERINA
		// Babinski          string
		// RefleksRooting    string
		// RefleksSwallowing string
		// RefleksSucking    string
		// RefleksMoro       string
		// RefleksGraps      string
	}

	DemfisikDokter struct {
		InsertDttm   string
		UpdateDttm   string
		InsertDevice string
		InsertUserId string
		KetPerson    string
		Pelayanan    string
		KdBagian     string
		Kategori     string
		Noreg        string
		JalanNafas   string
		PupilKanan   string
		PupilKiri    string
		Akral        string
		// Refleks        string
		CahayaKanan    string
		CahayaKiri     string
		GcsE           string
		GcsM           string
		GcsV           string
		Gangguan       string
		GangguanDetail string
	}

	PemeriksaanFisikDokterAntonio struct {
		InsertDttm   string
		InsertDevice string
		InsertUserId string
		KetPerson    string
		Pelayanan    string
		KdBagian     string
		Kategori     string
		Noreg        string
		Keterangan   string
	}

	// ============================ PEMERIKSAAN
	// ============================ FISIK
	PemeriksaanFisikModel struct {
		InsertDttm                 string
		InsertDevice               string
		InsertUserId               string
		KetPerson                  string
		Pelayanan                  string
		KdBagian                   string
		Kategori                   string
		Noreg                      string
		AlatKelamin                string
		AnggotaGerak               string
		Refleks                    string
		Otot                       string
		RtVt                       string
		GcsE                       string
		GcsM                       string
		GcsV                       string
		PupilKiri                  string
		PupilKanan                 string
		Isokor                     string
		IsokorDetail               string
		Anisokor                   string
		CahayaKanan                string
		Peristaltik                string
		CahayaKiri                 string
		Akral                      string
		TonickNeck                 string
		Pupil                      string
		Kesadaran                  string
		Kepala                     string
		Rambut                     string
		Wajah                      string
		KeadaanUmum                string
		JalanNafas                 string
		Sirkulasi                  string
		Gangguan                   string
		Kulit                      string
		Abdomen                    string
		Kelainan                   string
		Ginjal                     string
		AbdomenLainnya             string
		PeristatikUsus             string
		Thyroid                    string
		Hati                       string
		Paru                       string
		Mata                       string
		Tht                        string
		Telinga                    string
		Hidung                     string
		Mulut                      string
		Gigi                       string
		Lidah                      string
		Tenggorokan                string
		Leher                      string
		Lien                       string
		LeherLainnya               string
		Dada                       string
		Respirasi                  string
		Perut                      string
		Jantung                    string
		Integument                 string
		Ekstremitas                string
		EkstremitasSuperior        string
		EkstremitasInferior        string
		KemampuanGenggam           string
		Genetalia                  string
		Anus                       string
		Punggung                   string
		LainLain                   string
		Dindingdada                string
		DindingdadaRetEpigastrium  string
		DindingdadaRetSuprastermal string
		DindingdadaRetraksi        string
		Hepar                      string
		HeparDetail                string
		NutrisiDanHidrasi          string
		Limpa                      string
		LimpaDetail                string
		Ouf                        string
		TugorKulit                 string
		PemeriksaanFisik           string
		SkalaNyeri                 int
		SkalaNyeriP                string
		SkalaNyeriQ                string
		SkalaNyeriR                string
		SkalaNyeriS                string
		SkalaNyeriT                string
		FlaccWajah                 int
		FlaccKaki                  int
		FlaccAktifitas             int
		FlaccMenangis              int
		FlaccBersuara              int
		FlaccTotal                 int
		SkalaTriase                int
		Babinski                   string // =========== PEMERIKSAAN FISIK
		RefleksRooting             string
		RefleksSucking             string
		RefleksSwallowing          string
		RefleksMoro                string
		RefleksGraps               string
	}

	// DVITAL SIGN
	DVitalSign struct {
		InsertDttm        string `json:"insert_dttm"`
		UpdDttm           string `json:"upd_dttm"`
		InsertUserId      string `json:"user_id"`
		InsertDevice      string `json:"device"`
		Pelayanan         string `json:"pelayanan"`
		Kategori          string `json:"kategori"`
		KetPerson         string `json:"person"`
		KdBagian          string `json:"kd_bagian"`
		Noreg             string `json:"noreg"`
		Td                string `json:"tekanan_darah"`
		Suhu              string `json:"suhu"`
		Pernafasan        string `json:"pernapasan"`
		PernafasanDetail  string `json:"pernapasan_detail"`
		Nadi              string `json:"nadi"`
		NadiDetail        string `json:"nadi_detail"`
		Tb                string `json:"tinggi_badan"`
		Bb                string `json:"berat_badan"`
		Spo2              string `gorm:"column:spo2" json:"spo2"`
		LingkarKepala     string `json:"lingkar_kepala"`
		LingkarLenganAtas string `json:"lingkar_lengan_atas"`
	}

	PengkajianNyeri struct {
		Noreg                 string
		InsertDttm            string
		KeteranganPerson      string
		InsertUserId          string
		InsertPc              string
		Pelayanan             string
		KdBagian              string
		KdDpjp                string
		TglMasuk              string
		TglKeluar             string
		AseskepNyeri          int
		AseskepLokasiNyeri    string
		AseskepFrekuensiNyeri string
		AseskepNyeriMenjalar  string
		AseskepKualitasNyeri  string
	}

	DVitalSignResponse struct {
		UpdDttm           string `json:"upd_dttm"`
		InsertUserId      string `json:"user_id"`
		InsertDevice      string `json:"device"`
		Pelayanan         string `json:"pelayanan"`
		Kategori          string `json:"kategori"`
		KetPerson         string `json:"person"`
		KdBagian          string `json:"kd_bagian"`
		Noreg             string `json:"noreg"`
		Td                string `json:"tekanan_darah"`
		Suhu              string `json:"suhu"`
		Pernafasan        string `json:"pernapasan"`
		PernafasanDetail  string `json:"pernapasan_detail"`
		Nadi              string `json:"nadi"`
		NadiDetail        string `json:"nadi_detail"`
		Tb                string `json:"tinggi_badan"`
		Bb                string `json:"berat_badan"`
		Spo2              string `gorm:"column:spo2" json:"spo_dua"`
		LingkarKepala     string `json:"lingkar_kepala"`
		LingkarLenganAtas string `json:"lingkar_lengan_atas"`
		Ddj               string `json:"ddj"`
		GcsE              string `json:"gcs_e"`
		GcsV              string `json:"gcs_v"`
		GcsM              string `json:"gcs_m"`
	}

	DPemfisikBidanGCS struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdateDttm   string `json:"upd_dttm"`
		InsertUserId string `json:"user_id"`
		InsertDevice string `json:"device"`
		Pelayanan    string `json:"pelayanan"`
		Kategori     string `json:"kategori"`
		KetPerson    string `json:"person"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		GcsE         string `json:"gcs_e"`
		GcsV         string `json:"gcs_v"`
		GcsM         string `json:"gcs_m"`
		Ddj          string `json:"ddj"`
		Tfu          string `json:"tfu"`
		Kesadaran    string `json:"kesadaran"`
		Pupil        string `json:"pupil"`
		Akral        string `json:"akral"`
	}

	// DATA RE-ASESMEN RISIKO JATUH PADA PASIEN DEWASA
	// DATA RISIKO
	DRisikoJatuh struct {
		InsertDttm   string
		UpdDttm      string
		InsertUserId string
		InsertPc     string
		KetPerson    string
		Pelayanan    string
		KdBagian     string
		Noreg        string
		Kategori     string
		Usia         string
		// ---- PADA ANAK
		JenisKelamin     string
		GangguanKognitif string
		FaktorLingkungan string
		Respon           string
		PenggunaanObat   string

		RJatuh         string
		Aktivitas      string
		Mobilisasi     string
		Kognitif       string
		DefisitSensori string
		Pengobatan     string
		Komorbiditas   string

		// ====
		Diagnosis       string
		BAmbulasi       string
		Terapi          string
		TerpasangInfuse string
		GayaBerjalan    string
		StatusMental    string
		// ====== MORS
		Total int
	}

	// DATA INTERVENSI RESIKO
	DIntervensiRisiko struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string
		InsertUserId string `json:"user_id"`
		InsertPc     string
		KetPerson    string `json:"person"`
		Pelayanan    string `json:"pelayanan"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		Shift        string `json:"shift"`
		// ===== DATA INTERVENSI RISIKO ====== //
		Resiko1  bool `json:"resiko1"`
		Resiko2  bool `json:"resiko2"`
		Resiko3  bool `json:"resiko3"`
		Resiko4  bool `json:"resiko4"`
		Resiko5  bool `json:"resiko5"`
		Resiko6  bool `json:"resiko6"`
		Resiko7  bool `json:"resiko7"`
		Resiko8  bool `json:"resiko8"`
		Resiko9  bool `json:"resiko9"`
		Resiko10 bool `json:"resiko10"`
		Resiko11 bool `json:"resiko11"`
		Resiko12 bool `json:"resiko12"`
		Resiko13 bool `json:"resiko13"`
		Resiko14 bool `json:"resiko14"`
		Resiko15 bool `json:"resiko15"`
		Resiko16 bool `json:"resiko16"`
		Resiko17 bool `json:"resiko17"`
		// Namaperawat string `json:"nama_perawat"`
		// ===== DATA INTERVENSI RISIKO  ====== //
	}

	DIntervensiResikoJatuh struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string
		InsertUserId string `json:"user_id"`
		InsertPc     string
		KetPerson    string `json:"person"`
		Pelayanan    string `json:"pelayanan"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		Shift        string `json:"shift"`
		// ===== DATA INTERVENSI RISIKO ====== //
		Resiko1     bool   `json:"resiko1"`
		Resiko2     bool   `json:"resiko2"`
		Resiko3     bool   `json:"resiko3"`
		Resiko4     bool   `json:"resiko4"`
		Resiko5     bool   `json:"resiko5"`
		Resiko6     bool   `json:"resiko6"`
		Resiko7     bool   `json:"resiko7"`
		Resiko8     bool   `json:"resiko8"`
		Resiko9     bool   `json:"resiko9"`
		Resiko10    bool   `json:"resiko10"`
		Resiko11    bool   `json:"resiko11"`
		Resiko12    bool   `json:"resiko12"`
		Resiko13    bool   `json:"resiko13"`
		Resiko14    bool   `json:"resiko14"`
		Resiko15    bool   `json:"resiko15"`
		Resiko16    bool   `json:"resiko16"`
		Resiko17    bool   `json:"resiko17"`
		Namaperawat string `json:"nama_perawat"`
	}

	DIntervensiRisikoResponse struct {
		InsertDttm   string `json:"insert_dttm"`
		UpdDttm      string
		InsertUserId string `json:"user_id"`
		InsertPc     string
		KetPerson    string `json:"person"`
		Pelayanan    string `json:"pelayanan"`
		KdBagian     string `json:"kd_bagian"`
		Noreg        string `json:"noreg"`
		Shift        string `json:"shift"`
		// ===== DATA INTERVENSI RISIKO ====== //
		Resiko1     bool `json:"resiko1"`
		Resiko2     bool `json:"resiko2"`
		Resiko3     bool `json:"resiko3"`
		Resiko4     bool `json:"resiko4"`
		Resiko5     bool `json:"resiko5"`
		Resiko6     bool `json:"resiko6"`
		Resiko7     bool `json:"resiko7"`
		Resiko8     bool `json:"resiko8"`
		Resiko9     bool `json:"resiko9"`
		Resiko10    bool `json:"resiko10"`
		Resiko11    bool `json:"resiko11"`
		Resiko12    bool `json:"resiko12"`
		Resiko13    bool `json:"resiko13"`
		Resiko14    bool `json:"resiko14"`
		Resiko15    bool `json:"resiko15"`
		Resiko16    bool `json:"resiko16"`
		Resiko17    bool `json:"resiko17"`
		Namaperawat string
		// ===== DATA INTERVENSI RISIKO  ====== //
	}

	// RESIKO JATUH
	HasilResikoJatuhPasien struct {
		Rj1      string
		Rj2      string
		Hasil    string
		Tindakan string
	}

	// DATA INTERVENSI
	DIntervensi struct {
		IdSiki    int
		KodeSiki  string
		NoUrut    int
		Deskripsi string
		Kategori  string
	}

	// DATA SDKI
	DSlki struct {
		KodeSlki           string `gorm:"primaryKey:KodeSlki" json:"kode_slki"`
		Judul              string
		Defenisi           string
		Ekspetasi          string
		DKriteriaSLKIModel []DKriteriaSLKIModel `gorm:"foreignKey:KodeSlki" json:"skli"`
	}

	// DATA HASIL SLKI
	DKriteriaSLKIModel struct {
		IdKriteria   int `gorm:"primaryKey:IdKriteria" json:"id_kriteria"`
		KodeSlki     string
		NamaKriteria string
		Tanda        bool
		Kategori     string
		NoUrut       int
	}

	DAnalisaProblemModel struct {
		Id           int    `gorm:"primaryKey:Id" json:"id"`
		Noreg        string `json:"noreg"`
		KodeAnalisa  string `json:"kode"`
		IdDiagnosa   string `json:"id_diagnosa"`
		NamaDiagnosa string `json:"nama_diagnosa"`
	}

	DAnalisaDataModel struct {
		IdAnalisa    int                    `json:"id"`
		InsertDttm   string                 `json:"insert_dttm"`
		KodeAnalisa  string                 `gorm:"primaryKey:KodeAnalisa" json:"kode_analisa"`
		KdBagian     string                 `json:"kd_bagian"`
		UserId       string                 `json:"user_id"`
		Noreg        string                 `json:"noreg"`
		Data         string                 `json:"data"`
		Hasil        string                 `json:"hasil"`
		UserTeratasi string                 `json:"user_teratasi"`
		TglTeratasi  string                 `json:"tgl"`
		JamTeratasi  string                 `json:"jam"`
		AnalisaData  []DAnalisaProblemModel `gorm:"foreignKey:KodeAnalisa" json:"analisa_data"`
		Pengawai     Karyawan               `gorm:"foreignKey:UserId" json:"pegawai"`
	}

	// DATA SLKI MAPPING KE DKRITERIA HASIL SLKI
	DSLKIModel struct {
		KodeSlki           string
		Judul              string
		Defenisi           string
		Ekspetasi          string
		DKriteriaSLKIModel []DKriteriaSLKIModel
	}

	// MUTIARA PENGAJAR
	Karyawan struct {
		Id           string `gorm:"primaryKey:Id" json:"id"`
		Nama         string `json:"nama"`
		Jeniskelamin string `json:"jenis_kelamin"`
		Tgllahir     string `json:"tgl_lahir"`
	}

	// NOMOR DASKEP
	NoDaskep struct {
		Daskep string
	}

	KodeAnalisa struct {
		KodeAnalisa string
	}

	DccptModel struct {
		InsertDttm string
		IdCppt     string
	}

	// DASKEP DIAGNOSA
	DaskepDiagnosaModel struct {
		InsertDttm string
		InsertPc   string
		KetPerson  string
		UserId     string
		Tanggal    string
		KdBagian   string
		Noreg      string
		NoDaskep   string
		KodeSdki   string
	}

	// DATA DASKEP SLKI
	DaskepSLKIModel struct {
		InsertDttm string `json:"insert_dttm"`
		NoDaskep   string `json:"no_daskep"`
		IdKriteria int    `gorm:"primaryKey:IdKriteria" json:"id_kriteria"`
		KodeSlki   string `json:"kode_slki"`
		NamaSlki   string `json:"nama_sllki"`
		Kategori   string `json:"kategori"`
		NoUrut     int    `json:"no_urut"`
		Hasil      int    `json:"hasil"`
		Target     int    `json:"target"`
		Waktu      int    `json:"waktu"`
	}

	// DATA SIKI
	DaskepSikiModel struct {
		InsertDttm string `json:"insert_dttm"`
		NoDaskep   string `json:"no_daskep"`
		IdSiki     int    `json:"id_siki"`
		KodeSiki   string `json:"kode_siki"`
		Nama       string `json:"nama_siki"`
		Kategori   string `json:"kategori"`
		NoUrut     int    `json:"no_urut"`
	}

	// DASKEP DIAGNOSA JOIN KE DASKEP SIKI DAN JOIN KE DASKEP SLKI
	DasKepDiagnosaModel struct {
		UserId     string            `json:"user_id"`
		Tanggal    string            `json:"tanggal"`
		Noreg      string            `json:"no_reg"`
		Hasil      string            `json:"hasil"`
		KodeSdki   string            `json:"kode_diagnosa"`
		NoDaskep   string            `gorm:"primaryKey:NoDaskep" json:"no_daskep"`
		SDKIModel  SDKIModel         `gorm:"foreignKey:KodeSdki" json:"diagnosa"`
		DaskepSLKI []DaskepSLKIModel `gorm:"foreignKey:NoDaskep" json:"deskripsi_slki"`
		DaskepSIKI []DaskepSikiModel `gorm:"foreignKey:NoDaskep" json:"deskripsi_siki"`
	}

	KPelayanan struct {
		KdBag     string `gorm:"primaryKey;column:kd_bag" json:"kd_bag"`
		Bagian    string `json:"bagian"`
		Pelayanan string `json:"pelayanan"`
	}

	// === // ===
	DasKepDiagnosaModelV2 struct {
		UserId       string                     `json:"user_id"`
		Tanggal      string                     `json:"tanggal"`
		InsertDttm   string                     `json:"insert_dttm"`
		KdBagian     string                     `json:"kd_bagian"`
		Noreg        string                     `json:"no_reg"`
		Hasil        string                     `json:"hasil"`
		KodeSdki     string                     `json:"kode_diagnosa"`
		NoDaskep     string                     `gorm:"primaryKey:NoDaskep" json:"no_daskep"`
		SDKIModel    SDKIModel                  `gorm:"foreignKey:KodeSdki" json:"diagnosa"`
		DaskepSLKI   []DaskepSLKIModel          `gorm:"foreignKey:NoDaskep" json:"deskripsi_slki"`
		DaskepSIKI   []DaskepSikiModel          `gorm:"foreignKey:NoDaskep" json:"deskripsi_siki"`
		Perawat      UserPerawatModel           `gorm:"foreignKey:UserId" json:"perawat"`
		Pelayanan    KPelayanan                 `gorm:"foreignKey:KdBagian" json:"bagian"`
		Implementasi []DImplementasiKeperawatan `gorm:"foreignKey:NoDaskep" json:"implementasi"`
	}

	DasKepDiagnosaTable struct {
		UserId     string `json:"user_id"`
		Tanggal    string `json:"tanggal"`
		InsertDttm string `json:"insert_dttm"`
		KdBagian   string `json:"kd_bagian"`
		Noreg      string `json:"no_reg"`
		Hasil      string `json:"hasil"`
		KodeSdki   string `json:"kode_diagnosa"`
		NoDaskep   string `gorm:"primaryKey:NoDaskep" json:"no_daskep"`
	}

	DImplementasiKeperawatan struct {
		Id         int              `json:"id"`
		InsertDttm string           `json:"insert_dttm"`
		NoDaskep   string           `json:"no_daskep"`
		UserId     string           `json:"user_id"`
		NoReg      string           `json:"noreg"`
		Perawat    UserPerawatModel `gorm:"foreignKey:UserId" json:"user"`
		KdBagian   string           `json:"kd_bagian"`
		Pelayanan  KPelayanan       `gorm:"foreignKey:KdBagian" json:"bagian"`
		Deskripsi  string           `json:"deskripsi"`
	}

	DasKepDiagnosaModelV3 struct {
		UserId     string                             `json:"user_id"`
		Tanggal    string                             `json:"tanggal"`
		InsertDttm string                             `json:"insert_dttm"`
		KdBagian   string                             `json:"kd_bagian"`
		Noreg      string                             `json:"no_reg"`
		Hasil      string                             `json:"hasil"`
		KodeSdki   string                             `json:"kode_diagnosa"`
		NoDaskep   string                             `gorm:"primaryKey:NoDaskep" json:"no_daskep"`
		SDKIModel  SDKIModel                          `gorm:"foreignKey:KodeSdki" json:"diagnosa"`
		DaskepSLKI []DaskepSLKIModel                  `gorm:"foreignKey:NoDaskep" json:"deskripsi_slki"`
		DaskepSIKI []DaskepSikiModel                  `gorm:"foreignKey:NoDaskep" json:"deskripsi_siki"`
		Perawat    UserPerawatModel                   `gorm:"foreignKey:UserId" json:"perawat"`
		Pelayanan  KPelayanan                         `gorm:"foreignKey:KdBagian" json:"bagian"`
		Tindakan   []DImplementasiTindakanKeperawatan `gorm:"foreignKey:NoDaskep" json:"tindakan"`
	}

	UserPerawatModel struct {
		Idperawat     string `gorm:"primaryKey;column:idperawat" json:"id_perawat"`
		Namaperawat   string `json:"nama"`
		Alamat        string `json:"alamat"`
		Jeniskelamin  string `json:"jenis_kelamin"`
		Statusperawat string `json:"status"`
	}

	// AMBIL DATA DASKEP SIKI
	DaskepSIKIModel struct {
		NoDaskep string `json:"no_daskep"`
		IdSiki   int    `gorm:"primaryKey:IdSiki" json:"id_siki"`
		KodeSiki string `json:"kode_siki"`
		Nama     string `json:"nama"`
		Kategori string `json:"kategori"`
		NoUrut   int    `json:"no_urut"`
	}

	// DESKRIPSI SIKI
	DeskripsiSIKIModel struct {
		IdSiki    int `gorm:"primaryKey:IdSiki" json:"id_siki"`
		KodeSiki  string
		Kategori  string
		NoUrut    int
		Deskripsi string
	}

	// DATAALERGI
	DAlergi struct {
		No         int    `json:"nomor"`
		Id         string `json:"no_rm"`
		Kelompok   string `json:"kelompok"`
		InsertDttm string `json:"insert_dttm"`
		Alergi     string `json:"alergi"`
		InsertUser string `json:"nama_user"`
		KdBagian   string `json:"bagian"`
	}

	// ----

	KeluhanUtama struct {
		TglMasuk          string `json:"tgl_masuk"`
		AsesmedKeluhUtama string `json:"keluh_utama"`
		AsesmedRwytSkrg   string `json:"riwayat_sekarang"`
	}

	RiwayatKeluhanModel struct {
		TglMasuk string `json:"tgl_masuk"`
		Noreg    string `json:"noreg"`
		Id       string `json:"id"`
	}

	RiwayatPenyakitDahuluPerawat struct {
		TglMasuk         string `json:"tgl_masuk"`
		AseskepKel       string `json:"keluh_utama"`
		AseskepRwytPnykt string `json:"riwayat_sekarang"`
	}

	// Riwayat Penyakit Dahulu
	RiwayatPenyakitDahulu struct {
		TglMasuk         string `json:"tgl_masuk"`
		AseskepRwytPnykt string `json:"riwayat_penyakit"`
	}

	ResponseAsesemenDokterIGD struct {
		Noreg             string `json:"noreg"`
		AsesmedKeluhUtama string `json:"keluh_utama"`
		AsesmedRwytSkrg   string `json:"rwt_sekarang"`
		RiwayatDahulu     string `json:"rwt_dahulu"`
	}

	DBhpDializerModel struct {
		InsertDttm   string `json:"insert_dttm"`
		InsertPc     string `json:"insert_pc"`
		InsertUserid string `json:"user_id"`
		KetPerson    string `json:"ket_person"`
		KdBagian     string `json:"kd_bagian"`
		KdDpjp       string `json:"dpjp"`
		Noreg        string `json:"noreg"`
		Item1a       bool   `json:"item_1a"`
		Item1b       bool   `json:"item_1b"`
		Item1c       bool   `json:"item_1c"`
		Item2a       bool   `json:"item_2a"`
		Item2b       bool   `json:"item_2b"`
		Item3        bool   `json:"item_3"`
		Item4        bool   `json:"item_4"`
		Item5        bool   `json:"item_5"`
		Item6        bool   `json:"item_6"`
	}

	// ============================= //
	CPPTSBAR struct {
		IdCppt        int    `json:"id_cppt"`
		InsertDttm    string `json:"insert_dttm"`
		InserUserId   string `json:"user_id"`
		Kelompok      string `json:"kelompok"`
		Pelayanan     string `json:"pelayanan"`
		KdBagian      string `json:"bagian"`
		Tanggal       string `json:"tanggal"`
		Noreg         string `json:"noreg"`
		Namaperawat   string `json:"nama_perawat"`
		Namadokter    string `json:"nama_dokter"`
		Situation     string `json:"situation"`
		Background    string `json:"background"`
		Asesmen       string `json:"asesmen"`
		Recomendation string `json:"recomendation"`
		InstruksiPpa  string `json:"instruksi_ppa"`
	}

	DataCPPTSBAR struct {
		IdCppt        int
		InsertDttm    string
		InsertUserId  string
		InsertPc      string
		Kelompok      string
		Tanggal       string
		KdBagian      string
		Noreg         string
		Situation     string
		Background    string
		Asesmen       string
		Recomendation string
		InstruksiPpa  string
		Pelayanan     string
		Dpjp          string
	}

	RiwayatKehamilanPerina struct {
		InsertDttm           string `json:"insert_dttm"`
		InsertPc             string `json:"insert_pc"`
		InsertUserId         string `json:"user_id"`
		KdBagian             string `json:"kd_bagian"`
		KdRiwayat            string `json:"kd_riwayat"`
		TahunPersalinan      string `json:"tahun_persalinan"`
		NoRm                 string `json:"no_rm"`
		TempatPersalinan     string `json:"tempat_persalinan"`
		Noreg                string `json:"noreg"`
		UmurKehamilan        string `json:"umur_kehamilan"`
		JenisPersalinan      string `json:"jenis_persalinan"`
		Penolong             string `json:"penolong"`
		Penyulit             string `json:"penyulit"`
		Nifas                string `json:"nifas"`
		Jk                   string `json:"jk"`
		Bb                   string `json:"bb"`
		KeadaanSekarang      string `json:"keadaan_sekarang"`
		KomplikasiHamil      string `json:"komplikasi_hamil"`
		KomplikasiPersalinan string `json:"komplikasi_persalinan"`
	}

	DokterBayi struct {
		IdDokter     string `json:"id_dokter"`
		Spesialisasi string `json:"spesialisasi"`
		NamaDokter   string `json:"nama_dokter"`
	}

	// TINDAK LANJUT PERINA
	TindakLajutPerina struct {
		InsertDttm             string
		KeteranganPerson       string
		InsertUserId           string
		InsertPc               string
		TglMasuk               string
		KdBagian               string
		Noreg                  string
		AsesmenTindakanOperasi string
		AsesmedTindakLanjut    string
		AsesmenTglKontrolUlang string
	}

	DKartuObservasi struct {
		InsertDttm   string
		IdKartu      int
		KdBagian     string
		Noreg        string
		T            string
		N            string
		P            string
		S            string
		Cvp          string
		Ekg          string
		PupilKi      string
		PupilKa      string
		RedaksiKi    string
		RedaksiKa    string
		AnggotaBadan string
		Kesadaran    string
		SputumWarna  string
		IsiCup       string
		Keterangan   string
	}

	// KARTU CAIRANOBAT
	DKartuCairanObatObatan struct {
		IdKartu           int
		InsertDttm        string
		UserId            string
		Noreg             string
		KdBagian          string
		CairanMasuk1      string
		CairanMasuk2      string
		CairanMasuk3      string
		CairanMasukNgt    string
		NamaCairan        string
		CairanKeluarUrine string
		CairanKeluarNgt   string
		DrainDll          string
		Keterangan        string
	}

	// DPELAKSANA KEPERAWATAN
	DPelaksanaKeperawatan struct {
		Id         int
		InsertDttm string
		IdCppt     int
		UserId     string
		Deskripsi  string
	}

	// IMPLEMENTASI=>TINDAKAN
	DImplementasiTindakanKeperawatan struct {
		Id         int              `gorm:"primaryKey;column:id" json:"id"`
		InsertDttm string           `json:"insert_dttm"`
		NoDaskep   string           `json:"no_daskep"`
		NoReg      string           `json:"no_reg"`
		UserId     string           `json:"user_id"`
		KdBagian   string           `json:"kd_bagian"`
		Deskripsi  string           `json:"deskripsi"`
		Perawat    UserPerawatModel `gorm:"foreignKey:UserId" json:"perawat"`
		Pelayanan  KPelayanan       `gorm:"foreignKey:KdBagian" json:"bagian"`
	}

	// INSTRUKSI FARMAKOLOGI
	DInstruksiMedisFarmakologi struct {
		Id            int
		InsertDttm    string
		KdBagian      string
		Noreg         string
		NamaObat      string
		InsertUser    string
		Dosis         string
		Frekuensi     int
		KodeObat      string
		JumlahObat    int
		CaraPemberian string
		SisaObat      string
	}

	// GET DATA EDUKASI TERINTEGRASI
	DEdukasiTerintegrasi struct {
		IdEdukasi         int              `json:"id_edukasi"`
		InsertDttm        string           `json:"insert_dttm"`
		InsertUser        string           `json:"user_id"`
		NoRm              string           `json:"no_rm"`
		NoReg             string           `json:"noreg"`
		KdBagian          string           `json:"kd_bagian"`
		Informasi         string           `json:"informasi"`
		Metode            string           `json:"metode"`
		PemberiInformasi  string           `json:"pemberi_informasi"`
		PenerimaInformasi string           `json:"penerima_informasi"`
		Evaluasi          string           `json:"evaluasi"`
		Perawat           UserPerawatModel `gorm:"foreignKey:InsertUser" json:"perawat"`
		Pelayanan         KPelayanan       `gorm:"foreignKey:KdBagian" json:"bagian"`
	}

	// TRANSUFIS DARAH
	DTransfusiDarah struct {
		IDTransfusi    int                     `gorm:"primaryKey;column:id_transfusi" db:"id_transfusi" json:"id_transfusi"`
		InsertDttm     time.Time               `db:"insert_dttm" json:"insert_dttm"`
		Noreg          string                  `db:"noreg" json:"noreg"`
		UserID         string                  `db:"user_id" json:"user_id"`
		KdBagian       string                  `db:"kd_bagian" json:"kd_bagian"`
		NoKantung      string                  `db:"no_kantung" json:"no_kantung"`
		JenisDarah     string                  `db:"jenis_darah" json:"jenis_darah"`
		TglKadaluwarsa string                  `db:"tgl_kadaluwarsa" json:"tgl_kadaluarsa"`
		GolDarah       string                  `db:"gol_darah" json:"gol_darah"`
		JamPemberian   string                  `db:"jam_pemberian" json:"jam_pemberi"`
		Reaksi         []DReaksiTransfusiDarah `gorm:"foreignKey:IDTransfusi" json:"reaksi"`
		Perawat        UserPerawatModel        `gorm:"foreignKey:UserID" json:"perawat"`
		Pelayanan      KPelayanan              `gorm:"foreignKey:KdBagian" json:"bagian"`
	}

	DReaksiTransfusiDarah struct {
		IDReaksi    int    `gorm:"primaryKey;column:id_reaksi" db:"id_reaksi"`
		IDTransfusi int    `db:"id_transfusi"`
		UserId      string `db:"user_id"`
		InsertDttm  string `db:"insert_ddtm"`
		Noreg       string `db:"noreg"`
		Reaksi      string `db:"reaksi"`
		Keterangan  string `db:"keterangan"`
	}

	DReaksiTransfusiModelDarah struct {
		IDReaksi    int              `gorm:"primaryKey;column:id_reaksi" db:"id_reaksi" json:"id_reaksi"`
		IDTransfusi int              `db:"id_transfusi" json:"id_tranfusi"`
		UserID      string           `db:"user_id" json:"user_id"`
		InsertDttm  string           `db:"insert_ddtm" json:"insert_dttm"`
		Noreg       string           `db:"noreg" json:"noreg"`
		Reaksi      string           `db:"reaksi" json:"reaksi"`
		Keterangan  string           `db:"keterangan" json:"keterangan"`
		Perawat     UserPerawatModel `gorm:"foreignKey:UserID" json:"perawat"`
	}

	// ASESMEN ULANG NYERI
	DasesmenUlangNyeri struct {
		IDUlangNyeri   int       `gorm:"column:id_ulang_nyeri;primaryKey" json:"id_ulang_nyer"`
		InsertDttm     time.Time `gorm:"column:insert_dttm;default:0000-00-00 00:00:00" json:"insert_dttm"`
		IDFarmakologi  int       `gorm:"column:id_farmakologi;default:0" json:"id_farmakologi"`
		UserID         string    `gorm:"column:user_id;size:50" json:"user_id"`
		Noreg          string    `gorm:"column:noreg;size:225" json:"noreg"`
		KdBagian       string    `gorm:"column:kd_bagian;size:50" json:"kd_bagian"`
		NamaObat       string    `gorm:"column:nama_obat;size:225" json:"nama_obat"`
		DosisFrekuensi string    `gorm:"column:dosis_frekuensi;size:225" json:"dosis_frekuensi"`
		Rute           string    `gorm:"column:rute;size:225" json:"rute"`
		SkorNyeri      int       `gorm:"column:skor_nyeri;default:0" json:"skor_nyeri"`
		NonFarmakologi int       `gorm:"column:non_farmakologi;default:0" json:"non_farmakologi"`
		WaktuKaji      int       `gorm:"column:waktu_kaji;default:0" json:"waktu_kaji"`
	}

	DasesmenUlangNyeriV2 struct {
		IDUlangNyeri   int              `gorm:"column:id_ulang_nyeri;primaryKey" json:"id_nyeri" `
		InsertDttm     time.Time        `gorm:"column:insert_dttm;default:0000-00-00 00:00:00" json:"insert_dttm"`
		IDFarmakologi  int              `gorm:"column:id_farmakologi;default:0" json:"id_farmakologi"`
		UserID         string           `gorm:"column:user_id;size:50" json:"user_id"`
		Noreg          string           `gorm:"column:noreg;size:225" json:"noreg"`
		KdBagian       string           `gorm:"column:kd_bagian;size:50" json:"bagian"`
		NamaObat       string           `gorm:"column:nama_obat;size:225" json:"nama_obat"`
		DosisFrekuensi string           `gorm:"column:dosis_frekuensi;size:225" json:"dosis_frekuensi"`
		Rute           string           `gorm:"column:rute;size:225" json:"rute"`
		SkorNyeri      int              `gorm:"column:skor_nyeri;default:0" json:"skor_nyeri"`
		NonFarmakologi int              `gorm:"column:non_farmakologi;default:0" json:"non_farmakologi"`
		WaktuKaji      int              `gorm:"column:waktu_kaji;default:0" json:"waktu_kaji"`
		Perawat        UserPerawatModel `gorm:"foreignKey:UserID" json:"perawat"`
		Pelayanan      KPelayanan       `gorm:"foreignKey:KdBagian" json:"pelayanan"`
	}

	// VERIFY TRANSFUSI DARAH
	DverifyTransfusiDarah struct {
		IDVerify      uint      `gorm:"primaryKey;autoIncrement;column:id_verify"`
		IDTransfusi   int       `gorm:"column:id_transfusi;default:0"`
		KdBagian      string    `gorm:"column:kd_bagian;size:50;default:null"`
		Noreg         string    `gorm:"column:noreg;size:225;default:null"`
		InsertDttm    time.Time `gorm:"column:insert_dttm;default:'0000-00-00 00:00:00'"`
		UserVerify    string    `gorm:"column:user_verify;size:50;default:null"`
		NoKantung     int       `gorm:"column:no_kantung;default:0"`
		JDarah        string    `gorm:"column:j_darah;type:enum('Sesuai','Tidak Sesuai');default:'Sesuai'"`
		GolDarah      string    `gorm:"column:gol_darah;type:enum('Sesuai','Tidak Sesuai');default:'Sesuai'"`
		NoStock       string    `gorm:"column:kantong_darah;type:enum('Sesuai','Tidak Sesuai');default:'Sesuai'"`
		TglKadaluarsa string    `gorm:"column:tgl_kadaluarsa;type:enum('Sesuai','Tidak Sesuai');default:'Sesuai'"`
	}

	// TableName overrides the table name used by Gorm to map this struct to the correct database table.
	DPemberianCairan struct {
		ID              int              `gorm:"primaryKey;column:id" json:"id"`
		InsertDttm      time.Time        `gorm:"column:insert_dttm" json:"insert_dttm"`
		UserID          string           `gorm:"column:user_id" json:"user_id"`
		KDBagian        string           `gorm:"column:kd_bagian" json:"kd_bagian"`
		JumlahInfuse    int              `gorm:"column:jumlah_infuse" json:"jumlah_infuse"`
		SisaInfuse      int              `gorm:"column:sisa_infuse" json:"sisa_infuse"`
		Noreg           string           `gorm:"column:noreg" json:"noreg"`
		NamaCairan      string           `gorm:"column:nama_cairan" json:"nama_cairan"`
		DosisMakro      int              `gorm:"column:dosis_makro" json:"dosis_makro"`
		DosisMikro      int              `gorm:"column:dosis_mikro" json:"dosis_mikro"`
		FlsKe           int              `gorm:"column:fls_ke" json:"fls_ke"`
		ObatDitambahkan string           `gorm:"column:obat_ditambahkan" json:"obat_ditambahkan"`
		Keterangan      string           `gorm:"column:keterangan" json:"keterangan"`
		Perawat         UserPerawatModel `gorm:"foreignKey:UserID" json:"perawat"`
		Pelayanan       KPelayanan       `gorm:"foreignKey:KDBagian" json:"pelayanan"`
	}

	CairanIntake struct {
		IDCairan   int              `gorm:"primary_key;auto_increment"`
		KdBagian   string           `gorm:"column:kd_bagian; not null;default:''"`
		UserID     string           `gorm:"column:user_id; default:''"`
		Noreg      string           `gorm:"column:no_reg; not null;default:''"`
		InsertDttm time.Time        `gorm:"column:insert_dttm; not null;default:'0000-00-00 00:00:00'"`
		Ngt        int              `gorm:"column:ngt; default:0"`
		Minum      int              `gorm:"column:minum; default:0"`
		Infuse     int              `gorm:"column:infuse; default:0"`
		Perawat    UserPerawatModel `gorm:"foreignKey:UserID" json:"perawat"`
		Pelayanan  KPelayanan       `gorm:"foreignKey:KdBagian" json:"pelayanan"`
	}

	DcairanOutPut struct {
		IDCairan   int              `gorm:"primary_key;auto_increment"`
		InsertDttm time.Time        `gorm:"column:insert_dttm; default:'0000-00-00 00:00:00'"`
		KdBagian   string           `gorm:"column:kd_bagian; default:''"`
		UserID     string           `gorm:"column:user_id; default:''"`
		Noreg      string           `gorm:"column:no_reg; not null;default:''"`
		Urine      int              `gorm:"column:urine; default:0"`
		NGT        int              `gorm:"column:ngt; default:0"`
		WSD        int              `gorm:"column:wsd; default:0"`
		Drain      int              `gorm:"column:drain; default:0"`
		Muntah     int              `gorm:"column:muntah; default:0"`
		Pendarahan int              `gorm:"column:pendarahan; default:0"`
		Perawat    UserPerawatModel `gorm:"foreignKey:UserID" json:"perawat"`
		Pelayanan  KPelayanan       `gorm:"foreignKey:KdBagian" json:"pelayanan"`
	}

	DanalisaDataKebidanan struct {
		UIDAnalisa   string    `gorm:"primaryKey;column:uid_analisa;type:varchar(225);not null"`
		InsertUser   string    `gorm:"column:insert_user;type:varchar(225)"`
		KodeAnalisa  string    `gorm:"column:kode_analisa;type:varchar(225)"`
		KdBagian     string    `gorm:"column:kd_bagian;type:varchar(25)"`
		Noreg        string    `gorm:"column:noreg;type:varchar(25)"`
		TglTeratasi  time.Time `gorm:"column:tgl_teratasi"`
		JamTeratasi  time.Time `gorm:"column:jam_teratasi"`
		UserTeratasi string    `gorm:"column:user_teratasi;type:varchar(25)"`
	}

	DPemFisikModel struct {
		InsertDttm   time.Time `json:"insert_dttm"`
		UpdateDttm   time.Time `json:"update_dttm"`
		InsertDevice string    `json:"insert_device"`
		Kategori     string    `json:"kategori"`
		Noreg        string    `json:"noreg"`
		KetPerson    string    `json:"ket_person"`
		Pelayanan    string    `json:"pelayanan"`
		KdBagian     string    `json:"kd_bagian"`
		Kepala       string    `json:"kepala"`
		Mata         string    `json:"mata"`
		Telinga      string    `json:"telinga"`
		LeherDanBahu string    `json:"leher_dan_bahu"`
		Hidung       string    `json:"hidung"`
		Mulut        string    `json:"mulut"`
		Gigi         string    `json:"gigi"`
		Dada         string    `json:"dada"`
		Abdomen      string    `json:"abdomen"`
	}
)

func (DverifyTransfusiDarah) TableName() string {
	return "vicore_rme.dverify_transfusi_darah"
}

func (DPemFisikModel) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (DanalisaDataKebidanan) TableName() string {
	return "vicore_rme.danalisa_data_kebidanan"
}

func (DcairanOutPut) TableName() string {
	return "vicore_rme.dcairan_out_put"
}

func (CairanIntake) TableName() string {
	return "vicore_rme.dcairan_in_take"
}

func (DPemberianCairan) TableName() string {
	return "vicore_rme.dpemberian_cairan"
}

func (DKartuObservasi) TableName() string {
	return "vicore_rme.dkartu_observasi"
}
func (DasesmenUlangNyeri) TableName() string {
	return "vicore_rme.dasesmen_ulang_nyeri"
}

func (DasesmenUlangNyeriV2) TableName() string {
	return "vicore_rme.dasesmen_ulang_nyeri"
}

func (DTransfusiDarah) TableName() string {
	return "vicore_rme.dtransfusi_darah"
}

func (DReaksiTransfusiDarah) TableName() string {
	return "vicore_rme.dreaksi_transfusi_darah"
}

func (DReaksiTransfusiModelDarah) TableName() string {
	return "vicore_rme.dreaksi_transfusi_darah"
}

func (DEdukasiTerintegrasi) TableName() string {
	return "vicore_rme.dedukasi_terintegrasi"
}

func (DInstruksiMedisFarmakologi) TableName() string {
	return "vicore_rme.dinstruksi_medis_farmakologi"
}

func (DImplementasiTindakanKeperawatan) TableName() string {
	return "vicore_rme.dimplementasi_keperawatan"
}

func (DPelaksanaKeperawatan) TableName() string {
	return "vicore_rme.dpelaksana_keperawatan"
}

func (DKartuCairanObatObatan) TableName() string {
	return "vicore_rme.dkartu_cairan"
}

func (Karyawan) TableName() string {
	return "mutiara.pengajar"
}

func (TindakLajutPerina) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (DeskripsiSIKIModel) TableName() string {
	return "vicore_rme.deskripsi_siki"
}

func (DVitalSignModel) TableName() string {
	return "vicore_rme.dvital_sign"
}

func (KPelayanan) TableName() string {
	return "vicore_lib.kpelayanan"
}

// USER PERAWAT

func (UserPerawatModel) TableName() string {
	return "his.kperawat"
}

func (RiwayatKehamilanPerina) TableName() string {
	return "vicore_rme.driwayat_kehamilan"
}

func (CPPTSBAR) TableName() string {
	return "vicore_rme.dcppt"
}

func (DataCPPTSBAR) TableName() string {
	return "vicore_rme.dcppt"
}

func (DPemfisikBidanGCS) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (VitalSignDokter) TableName() string {
	return "vicore_rme.dvital_sign"
}

func (DemfisikDokter) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (DccptModel) TableName() string {
	return "vicore_rme.dcppt"
}

func (DImplementasiKeperawatan) TableName() string {
	return "vicore_rme.dimplementasi_keperawatan"
}

func (DBhpDializerModel) TableName() string {
	return "vicore_rme.dbhp_dializer"
}

func (DAlergi) TableName() string {
	return "vicore_rme.dalergi"
}

func (DasKepDiagnosaModel) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}

func (DasKepDiagnosaTable) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}
func (DasKepDiagnosaModelV2) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}

func (DasKepDiagnosaModelV3) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}

func (SDKIModel) TableName() string {
	return "vicore_rme.sdki"
}

func (DaskepSikiModel) TableName() string {
	return "vicore_rme.daskep_siki"
}

// DASKEP SLKI9
func (DaskepSLKIModel) TableName() string {
	return "vicore_rme.daskep_slki"
}
func (DKriteriaSLKIModel) TableName() string {
	return "vicore_rme.dkriteria_hasil_slki"
}

func (DaskepDiagnosaModel) TableName() string {
	return "vicore_rme.daskep_diagnosa"
}

func (DSlki) TableName() string {
	return "vicore_rme.dslki"
}

func (DKriteriaHasilSLKI) TableName() string {
	return "vicore_rme.dkriteria_hasil_slki"
}

func (DIntervensi) TableName() string {
	return "vicore_rme.deskripsi_siki"
}

// ==== INTERVENSI RISIKO JATUH ==== //
func (DIntervensiRisiko) TableName() string {
	return "vicore_rme.dintervensi_risiko"
}

// ==== INTERVENSI RISIKO JATUH ==== //
func (DRisikoJatuh) TableName() string {
	return "vicore_rme.drisiko_jatuh"
}

func (DVitalSign) TableName() string {
	return "vicore_rme.dvital_sign"
}

// ==========
func (SkalaTriaseModel) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (PemeriksaanFisikModel) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (PemeriksaanFisikDokterAntonio) TableName() string {
	return "vicore_rme.dpem_fisik"
}

func (DcpptSoap) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
func (DeskripsiSikiModel) TableName() string {
	return "vicore_rme.deskripsi_siki"
}

func (SikiDeskripsiModel) TableName() string {
	return "vicore_rme.siki"
}

func (AsesmedKeperawatanBidan) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (Daskep) TableName() string {
	return "vicore_rme.daskep"
}

func (DaskepModel) TableName() string {
	return "vicore_rme.daskep"
}

func (SLKIModel) TableName() string {
	return "vicore_rme.slki"
}

func (SIKIModel) TableName() string {
	return "vicore_rme.siki"
}
func (CPPTPasienModel) TableName() string {
	return "vicore_rme.dcppt"
}
func (DAnalisaDataModel) TableName() string {
	return "vicore_rme.danalisa_data"
}
func (DAnalisaProblemModel) TableName() string {
	return "vicore_rme.danalisa_problem"
}
