package rme

type (
	PengkajianPersistemIGD struct {
		InsertDttm                     string
		UpdDttm                        string
		KeteranganPerson               string
		InsertUserId                   string
		InsertPc                       string
		TglMasuk                       string
		TglKeluar                      string
		JamCheckOut                    string
		JamCheckInRanap                string
		KdBagian                       string
		Noreg                          string
		KdDpjp                         string
		AseskepSistemMerokok           string
		AseskepSistemMinumAlkohol      string
		AseskepSistemSpikologis        string
		AseskepSistemGangguanJiwa      string
		AseskepSistemBunuhDiri         string
		AseskepSistemTraumaPsikis      string
		AseskepSistemHambatanSosial    string // KEBUTUHAN SPRITUAL
		AseskepSistemHambatanSpiritual string
		AseskepSistemHambatanEkonomi   string
		AseskepSistemPenghasilan       string
		AseskepSistemKultural          string
		AseskepSistemAlatBantu         string
		AseskepSistemKebutuhanKhusus   string
	}

	AsesemenMedisIGD struct {
		InsertDttm          string `json:"insert_dttm"`
		KeteranganPerson    string `json:"person"`
		InsertUserId        string `json:"user_id"`
		Pelayanan           string `json:"pelayanan"`
		InsertPc            string `json:"insert_pc"`
		KdBagian            string `json:"kd_bagian"`
		Noreg               string `json:"noreg"`
		AsesmedKeluhUtama   string `json:"keluh_utama"`
		AsesmedRwytDahulu   string `json:"rwt_dahulu"`
		AsesmedRwytSkrg     string `json:"rwt_sekarang"`
		TglMasuk            string `json:"tgl_masuk"`
		JamMasuk            string `json:"jam_masuk"`
		AsesmedLokalisImage string `json:"image_lokalis"`
		AsesmenPrognosis    string `json:"prognosis"`
		AsesmedAlasanOpname string `json:"alasan_opname"`
		AsesmedTerapi       string `json:"terapi"`
		AsesmedKonsulKe     string `json:"konsulke"`
		Namadokter          string `json:"nama_dokter"`
		DiagnosaBanding     string `json:"diagnosa_banding"`
	}

	DiagnosaKeperawatan struct {
		Kode  string `json:"kode"`
		Judul string `json:"judul"`
	}

	AsesmenPerawat struct {
		JenpelDetail                   string `json:"jenpel_detail"`
		Jenpel                         string `json:"jenpel"`
		InsertDttm                     string `json:"insert_dttm"`
		Person                         string `json:"person"`
		TglMasuk                       string `json:"tgl_masuk"`
		TglKeluar                      string `json:"tgl_keluar"`
		KdBagian                       string `json:"kd_bagian"`
		Noreg                          string `json:"noreg"`
		AseskepKel                     string `json:"keluhan"`
		UserId                         string `json:"user_id"`
		RwytPenyakit                   string `json:"riwayat_penyakit"`
		RwtPenyakitDahulu              string `json:"riwayat_penyakit_dahulu"`
		Nama                           string `json:"nama"`
		ReaksiAlergi                   string `json:"reaksi_alergi"`
		AseskepSistemEliminasiBak      string `json:"eliminasi_bak"`
		AseskepSistemEliminasiBab      string `json:"eliminasi_bab"`
		AseskepSistemIstirahat         string `json:"istirahat"`
		AseskepSistemAktivitas         string `json:"sistem_aktivitas"`
		AseskepSistemMandi             string `json:"sistem_mandi"`
		AseskepSistemBerpakaian        string `json:"sistem_berpakaian"`
		AseskepSistemMakan             string `json:"sistem_makan"`
		AseskepSistemEliminasi         string `json:"sistem_eliminasi"`
		AseskepSistemMobilisasi        string `json:"sistem_mobilisasi"`
		AseskepSistemKardiovaskuler    string `json:"sistem_kardiovaskuler"`
		AseskepSistemRespiratori       string `json:"sistem_respiratori"`
		AseskepSistemSecebral          string `json:"sistem_secebral"`
		AseskepSistemPerfusiPerifer    string `json:"sistem_perfusi_perifer"`
		AseskepSistemPencernaan        string `json:"sistem_pencernaan"`
		AseskepSistemIntegumen         string `json:"sistem_integumen"`
		AseskepSistemKenyamanan        string `json:"sistem_kenyamaman"`
		AseskepSistemProteksi          string `json:"sistem_proteksi"`
		AseskepSistemPapsSmer          string `json:"sitem_paps_smer"`
		AseskepSistemHamil             string `json:"sistem_hamil"`
		AseskepSistemPendarahan        string `json:"sistem_pendaharan"`
		AseskepSistemHambatanBahasa    string `json:"hambatan_bahasa"`
		AseskepSistemCaraBelajar       string `json:"cara_belajar"`
		AseskepSistemBahasaSehari      string `json:"bahasa_sehari"`
		AseskepSistemSpikologis        string `json:"spikologis"`
		AseskepSistemHambatanSosial    string `json:"hambatan_sosial"`
		AseskepSistemHambatanEkonomi   string `json:"hambatan_ekonomi"`
		AseskepSistemHambatanSpiritual string `json:"hambatan_spiritual"`
		AseskepSistemResponseEmosi     string `json:"response_emosi"`
		AseskepSistemNilaiKepercayaan  string `json:"nilai_kepercayaan"`
		AseskepSistemPresepsiSakit     string `json:"presepsi_sakit"`
		AseskepSistemThermoregulasi    string `json:"thermoregulasi"`
		AseskepSistemKhususKepercayaan string `json:"khusus_kepercayaan"`
		AseskepSistemSakitKepala       string `json:"sakit_kepala"`
		AseskepSistemPencernaanUsus    string `json:"sistem_usus"`
		AseskepSistemAkral             string `json:"akral"`
		AseskepSistemBatuk             string `json:"batuk"`
		AseskepSistemSuaraNapas        string `json:"suara_napas"`
		AseskepSistemMerokok           string `json:"merokok"`
		AseskepSistemLemahAnggotaGerak string `json:"anggota_gerak"`
		AseskepSistemStatusMental      string `json:"perubahan_status_mental"`
		AseskepSistemBicara            string `json:"bicara"`
		AseskepSistemRiwayatHipertensi string `json:"riwayat_hipertensi"`
		AseskepSistemKekuatanOtot      string `json:"kekuatan_otot"`
		AseskepSistemPenerjemah        string `json:"perlu_penerjemah"`
		AseskepSistemNutrisi           string `json:"nutrisi"`
		// AseskepSistemProteksi          string
	}

	//==============//
	AsesemenDokterIGD struct {
		AsesmedDokterTtd    string `json:"dokter_ttd"`
		AsesmedDokterTtdTgl string `json:"dokter_ttd_tgl"`
		AsesmedDokterTtdJam string `json:"dokter_ttd_jam"`
		InsertDttm          string `json:"insert_dttm"`
		KeteranganPerson    string `json:"person"`
		InsertUserId        string `json:"user_id"`
		Pelayanan           string `json:"pelayanan"`
		KdDpjp              string `json:"kd_dpjp"`
		InsertPc            string `json:"insert_pc"`
		KdBagian            string `json:"kd_bagian"`
		Noreg               string `json:"noreg"`
		AsesmedKeluhUtama   string `json:"keluh_utama"`
		AsesmedRwytSkrg     string `json:"rwt_sekarang"`
		AsesmedRwytDahulu   string `json:"rwt_dahulu"`
		TglMasuk            string `json:"tgl_masuk"`
		JamMasuk            string `json:"jam_masuk"`
	}

	// ==================== RIWAYAT PENYAKIT
	HistoryPenyakit struct {
		AsesmedDokterTtd    string     `json:"dokter_ttd"`
		AsesmedDokterTtdTgl string     `json:"dokter_ttd_tgl"`
		AsesmedDokterTtdJam string     `json:"dokter_ttd_jam"`
		InsertDttm          string     `json:"insert_dttm"`
		KeteranganPerson    string     `json:"person"`
		InsertUserId        string     `json:"user_id"`
		Pelayanan           string     `json:"pelayanan"`
		KdDpjp              string     `json:"kd_dpjp"`
		InsertPc            string     `json:"insert_pc"`
		KdBagian            string     `json:"kd_bagian"`
		Noreg               string     `json:"noreg"`
		AsesmedKeluhUtama   string     `json:"keluh_utama"`
		AsesmedRwytSkrg     string     `json:"rwt_sekarang"`
		AsesmedRwytDahulu   string     `json:"rwt_dahulu"`
		TglMasuk            string     `json:"tgl_masuk"`
		JamMasuk            string     `json:"jam_masuk"`
		KPelayanan          KPelayanan `gorm:"foreignKey:KdBagian" json:"bagian"`
	}
)

func (AsesemenDokterIGD) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (HistoryPenyakit) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (PengkajianPersistemIGD) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
