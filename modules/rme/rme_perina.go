package rme

type (
	DownScoreNeoNatusModel struct {
		InsertDttm     string
		Noreg          string
		FrekwensiNafas int
		Sianosis       int
		Retraksi       int
		AirEntry       int
		Merintih       int
		Total          int
		InsertUserId   string
		KdBagian       string
		Person         string
	}

	DApgarScoreNeoNatus struct {
		InsertDttm   string
		Noreg        string
		KdBagian     string
		IdAgpar      int
		InsertUserId string
		Waktu        string
		DJantung     int
		UNafas       int
		Otot         int
		Refleksi     int
		WarnaKulit   int
		Total        int
	}

	AsesmenSkalaNyeri struct {
		InsertDttm            string
		KeteranganPerson      string
		InsertUserId          string
		InsertPc              string
		AseskepSkalaNyeri     int
		TglMasuk              string
		Pelayanan             string
		KdBagian              string
		Noreg                 string
		KdDpjp                string
		TglKeluar             string
		AseskepNyeri          string
		AseskepLokasiNyeri    string
		AseskepFrekuensiNyeri string
		AseskepNyeriMenjalar  string
		AseskepKualitasNyeri  string
	}

	SkalaNyeri struct {
		InsertDttm        string
		KeteranganPerson  string
		InsertUserId      string
		InsertPc          string
		AseskepSkalaNyeri int
		TglMasuk          string
		Pelayanan         string
		KdBagian          string
		Noreg             string
	}

	// ASESMEN KEPERAWATAN BAYI
	AsesmenKeperawatanBayi struct {
		InsertDttm                              string
		KeteranganPerson                        string
		InsertUserId                            string
		InsertPc                                string
		Pelayanan                               string
		TglMasuk                                string
		KdBagian                                string
		Noreg                                   string
		KdDpjp                                  string
		AseskepBayiDokterObgyn                  string
		AseskepBayiDokterAnak                   string
		AseskepBayiNamaAyah                     string
		AseskepBayiPekerjaanAyah                string
		AseskepBayiPerkawinanAyahKe             string
		AseskepBayiPrenatalPendarahan           string
		AseskepUsiaKehamilan                    string
		AseskepBayiNamaIbu                      string
		AseskepBayiPekerjaanIbu                 string
		AseskepBayiRwtPenyakitAyah              string
		AseskepBayiPerkawinanIbuKe              string
		AseskepBayiRwtPenyakitIbu               string
		AseskepBayiPrenatalKebiasaanIbu         string
		AseskepBayiNamaPjawab                   string
		AseskepBayiUsiaPjawab                   string
		AseskepBayiPekerjaanPjawab              string
		AseskepBayiUsiaPersalinan               string
		AseskepBayiTglLahir                     string
		AseskepBayiLahirDgn                     string
		AseskepBayiMenangis                     string
		AseskepBayiJk                           string
		AseskepBayiPrenatalPersalinanSebelumnya string
		AseskepBayiKeterangan                   string
		AseskepBayiPrenatalUsiaKehamilan        string
		AseskepBayiPrenatalKomplikasi           string
		AseskepBayiPrenatalHis                  string
		AseskepBayiPrenatalTtp                  string
		AseskepBayiPrenatalKetuban              string
		AseskepBayiPrenatalJam                  string
		AseskepBayiRwtUsiaPersalinan            string
		AseskepBayiRwtLahirDengan               string
		AseskepBayiRwtJenisKelamin              string
		AseskepBayiRwtTglKelahiranBayi          string
		AseskepBayiRwtMenangis                  string
		AseskepBayiRwtKeterangan                string
		AseskepBayiRwtPrenatalObatObatan        string
		AseskepBayiPrenatalUsiaPersalinan       string
		AseskepBayiNatalPersalinan              string
		AseskepBayiNatalKpd                     string
		AseskepBayiNatalKeadaan                 string
		AseskepBayiNatalTindakanDiberikan       string
		AseskepBayiNatalPost                    string
		AseskepBayiNatalPresentasi              string
		AseskepBayiNatalDitolongOleh            string
		AseskepBayiNatalKetuban                 string
		AseskepBayiNatalLetak                   string
		AseskepBayiNatalVolume                  string
		AseskepBayiNatalLahirUmur               string
		AseskepBayiNatalKomplikasi              string
		AseskepBayiPrenatalJumlahHari           string
	}

	DAnalisaProblem struct {
		Id           int
		Noreg        string
		KodeAnalisa  string
		IdDiagnosa   string
		NamaDiagnosa string
	}

	DAnalisaData struct {
		IdAnalisa   int
		InsertDttm  string
		KodeAnalisa string
		KdBagian    string
		UserId      string
		Noreg       string
		Data        string
	}

	KakiKananBayi struct {
		InsertDttm                     string
		InsertUserId                   string
		KeteranganPerson               string
		Pelayanan                      string
		TglMasuk                       string
		KdBagian                       string
		KdDpjp                         string
		Noreg                          string
		AseskepImageSidikKakiKananBayi string
		AseskepImageSidikKakiKiriBayi  string
		AseskepImageIbuJariKiriIbu     string
		AseskepTtdPenentuJk            string
		AseskepTtdWali                 string
		AseskepNamaPenentuJk           string
		AseskepNamaWali                string
		AseskepJamKelahiran            string
		AseskepPemberiGelangBayi       string
	}

	// IDENTITAS BAYI
	IdentitasBayiModel struct {
		InsertDttm               string
		InsertUserId             string
		KeteranganPerson         string
		Pelayanan                string
		TglMasuk                 string
		KdBagian                 string
		KdDpjp                   string
		Noreg                    string
		AseskepTtdPenentuJk      string
		AseskepTtdWali           string
		AseskepNamaPenentuJk     string
		AseskepNamaWali          string
		AseskepPemberiGelangBayi string
	}

	// DEARLY WARNING SYSTEM
	DearlyWarningSystem struct {
		InsertDttm       string
		IdEws            int `gorm:"primaryKey:id_ews" json:"id_ews"`
		UserId           string
		Kategori         string
		KdBagian         string
		Keterangan       string
		TingkatKesadaran string
		Noreg            string
		ObsigenTambahan  string
		Td               int
		Td2              int
		Nadi             int
		Pernapasan       int
		ReaksiOtot       string
		Suhu             float64
		Spo2             int
		Crt              int
		SkalaNyeri       int
		TotalSkor        int
		MutiaraPengajar  MutiaraPengajar `gorm:"foreignKey:UserId" json:"karyawan"`
	}

	// CATATAN KEPERAWATAN
	DCatatanKeperawatan struct {
		IdCatatan  int
		InsertDttm string
		Noreg      string
		KdBagian   string
		UserId     string
		Catatan    string
	}

	MutiaraPengajar struct {
		Keterangan   string `json:"keterangan"`
		Id           string `gorm:"primaryKey:id" json:"id_pengajar"`
		Nama         string `json:"nama"`
		Jeniskelamin string `json:"jenis_kelamin"`
		KdKaru       string `json:"kd_karu"`
	}

	// `gorm:"foreignKey:NoDaskep" json:"deskripsi_siki"`
	DCatatanKeperawatanModel struct {
		IdCatatan       int             `gorm:"primaryKey:IdCatatan" json:"id_catatan"`
		InsertDttm      string          `json:"insert_dttm"`
		Noreg           string          `json:"noreg"`
		KdBagian        string          `json:"kd_bagian"`
		UserId          string          `gorm:"foreignKey:UserId" json:"user_id"`
		Catatan         string          `json:"catatan"`
		MutiaraPengajar MutiaraPengajar `gorm:"foreignKey:UserId" json:"karyawan"`
	}

	//===//

)

func (DownScoreNeoNatusModel) TableName() string {
	return "vicore_rme.dcatatan_keperawatan"
}

func (MutiaraPengajar) TableName() string {
	return "mutiara.pengajar"
}

func (DCatatanKeperawatan) TableName() string {
	return "vicore_rme.dcatatan_keperawatan"
}

func (DCatatanKeperawatanModel) TableName() string {
	return "vicore_rme.dcatatan_keperawatan"
}

func (DearlyWarningSystem) TableName() string {
	return "vicore_rme.dearly_warning_system"
}

func (IdentitasBayiModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (KakiKananBayi) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesmenSkalaNyeri) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DAnalisaProblem) TableName() string {
	return "vicore_rme.danalisa_problem"
}

func (DAnalisaData) TableName() string {
	return "vicore_rme.danalisa_data"
}

func (AsesmenKeperawatanBayi) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DApgarScoreNeoNatus) TableName() string {
	return "vicore_rme.dapgar_score_neonatus"
}
