package rme

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"strings"
	"time"
)

func (su *rmeUseCase) GetIntervensiUseCaseV2(req dto.RegIntervensiV2) (res []dto.MapperSLKI, err error) {

	parts := strings.Split(req.Slki, ",")

	var values []dto.MapperSLKI

	var dat []dto.ResponseSLKI

	var Kriteria = []string{"1", "2", "3", "4", "5"}

	for _, part := range parts {

		su.logging.Info(" GET INTERVENSI " + part)

		if part != "" {
			detail, errs := su.rmeRepository.GetSLKIRepository(part)

			if errs != nil {
				return res, errors.New(errs.Error())
			} else {
				for i := 0; i <= len(detail)-1; i++ {

					sat := dto.ResponseSLKI{
						Nomor:          i,
						NoUrut:         detail[i].NoUrut,
						Tanda:          detail[i].Tanda,
						Menurun:        detail[i].Menurun,
						Meningkat:      detail[i].Meningkat,
						Memburuk:       detail[i].Memburuk,
						Kode:           detail[i].Kode,
						Ekspektasi:     detail[i].Ekspektasi,
						Kriteria:       Kriteria,
						Judul:          detail[i].Judul,
						SelectedNumber: 0,
					}

					dat = append(dat, sat)
				}
			}

			data := dto.MapperSLKI{
				Kode:         part,
				ResponseSLKI: dat,
			}

			values = append(values, data)
		}

	}

	return values, nil
}

func (su *rmeUseCase) SaveAsesmenKeperawatanUsecaseV2(kdBagian string, req dto.ReqAsesmenKeperawatanV2) (message string, err error) {

	times := time.Now()
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap, err := su.rmeRepository.SearchDcpptSoapPasienRepository(req.Asesemen.Noreg, kdBagian)

	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA ASESKEP DAN DATA DASKEP")

		for i := 0; i <= len(req.Intervensi)-1; i++ {
			for _, inter := range req.Intervensi[i].Slki {

				daskep := rme.Daskep{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					Tanggal:    times.Format("2006-01-02"),
					KdBagian:   kdBagian,
					Noreg:      req.Asesemen.Noreg,
					NoUrut:     inter.NoUrut,
					Nilai:      inter.SelectionNumber,
					KodeSdki:   req.Sdki,
					KodeSlki:   inter.Kode,
					KodeSiki:   req.Siki,
					Hasil:      "",
				}

				_, err := su.rmeRepository.InsertDaskepRepository(daskep)

				if err != nil {
					return "", errors.New(err.Error())
				}
			}
		}

		// SIMPAN SOAP RESPOSITORY
		_, errs := su.rmeRepository.InsertAsesmenKeperawatanBidanRepositoryV2(kdBagian, req)
		if errs != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errs.Error())
			return message, errors.New(message)
		} else {
			return "Data berhasil disimpan", nil
		}

	} else {
		// SEHARUSNYA
		su.logging.Info("UPDATE DATA ASESKEP DAN DATA DASKEP")

		// CARI APAKAH ADA DATA DASKEP
		cari, errs := su.rmeRepository.CariDataDaskepRepository(kdBagian, req.Asesemen.Noreg)

		su.logging.Info(cari)

		if errs != nil || len(cari.Noreg) <= 0 {
			// INSERT DATA
			su.logging.Info("DATA NOREG  TIDAK DITEMUKAN")

			for i := 0; i <= len(req.Intervensi)-1; i++ {
				for _, inter := range req.Intervensi[i].Slki {

					daskep := rme.Daskep{
						InsertDttm: times.Format("2006-01-02 15:04:05"),
						Tanggal:    times.Format("2006-01-02"),
						KdBagian:   kdBagian,
						Noreg:      req.Asesemen.Noreg,
						NoUrut:     inter.NoUrut,
						Nilai:      inter.SelectionNumber,
						KodeSdki:   req.Sdki,
						KodeSlki:   inter.Kode,
						KodeSiki:   req.Siki,
						Hasil:      "",
					}

					_, err := su.rmeRepository.InsertDaskepRepository(daskep)

					if err != nil {
						return "", errors.New(err.Error())
					}
				}
			}
		} else {
			su.logging.Info("DATA NOREG DITEMUKAN")
			su.rmeRepository.HapusDaskepRepository(kdBagian, req.Asesemen.Noreg)
			su.logging.Info("LAKUKAN HAPUS DASKEP")

			if len(req.Intervensi) > 1 {
				// HAPUS DATA DASKEP

				// SIMPAN DATA
				// YANG BARU
				for i := 0; i <= len(req.Intervensi)-1; i++ {
					for _, inter := range req.Intervensi[i].Slki {

						daskep := rme.Daskep{
							InsertDttm: times.Format("2006-01-02 15:04:05"),
							Tanggal:    times.Format("2006-01-02"),
							KdBagian:   kdBagian,
							Noreg:      req.Asesemen.Noreg,
							NoUrut:     inter.NoUrut,
							Nilai:      inter.SelectionNumber,
							KodeSdki:   req.Sdki,
							KodeSlki:   inter.Kode,
							KodeSiki:   req.Siki,
							Hasil:      "",
						}

						_, err := su.rmeRepository.InsertDaskepRepository(daskep)

						if err != nil {
							return "", errors.New(err.Error())
						}
					}
				}

			}

		}

		_, errss := su.rmeRepository.UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian, req)

		if errss != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errss.Error())
			return message, errors.New(message)
		} else {
			return "Data berhasil diubah", nil
		}

	}
}

func (su *rmeUseCase) SavePemeriksaanFisikBangsalUsecase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikBangsal) (message string, err error) {
	// CHEK APAKAH DATA SUDAH ADA PADA HARI INI
	fisik, _ := su.rmeRepository.CariPemeriksaanFisikByDateRepository(userID, req.Person, kdBagian, req.Noreg)

	su.logging.Info("CARI DATA PEMERIKSAAN")
	su.logging.Info(userID)
	su.logging.Info(kdBagian)
	su.logging.Info(req)
	su.logging.Info(fisik)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {
		// LAKUKAN UPDATE DATE
		_, err1 := su.rmeRepository.UpdatePemeriksaanFisikBangsalRepository(userID, kdBagian, req.Noreg, req)

		su.logging.Info("LAKUKAN UPDATE")
		if err1 != nil {
			message := fmt.Sprintf("Error %s", err1.Error())
			return "Data gagal diubah", errors.New(message)
		}

		su.logging.Info("Data berhasil diubah")
		return "Data berhasil diubah", nil
	} else {
		// LAKUKAN INSERT DATA
		_, err1 := su.rmeRepository.InsertPemeriksaanFisikBangsalRepository(userID, kdBagian, req)
		su.logging.Info("LAKUKAN SIMPAN")

		if err1 != nil {
			message := fmt.Sprintf("Error %s", err1.Error())
			return "Data gagal di simpan", errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")
		return "Data berhasil disimpan", nil
	}
}

func (su *rmeUseCase) SavePemeriksaanFisikAnakUseCase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikAnak) (message string, err error) {
	// CHEK APAKAH DATA SUDAH ADA PADA HARI INI
	fisik, _ := su.rmeRepository.CariPemeriksaanFisikByDateRepository(userID, req.Person, kdBagian, req.Noreg)
	su.logging.Info("CARI DATA PEMERIKSAAN")
	su.logging.Info(fisik)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {
		// LAKUKAN UPDATE DATE
		_, err1 := su.rmeRepository.UpdatePemeriksaanFisikAnakRepository(userID, kdBagian, req.Noreg, req)

		su.logging.Info("LAKUKAN UPDATE")
		if err1 != nil {
			message := fmt.Sprintf("Error %s", err1.Error())
			return "Data gagal diubah", errors.New(message)
		}

		su.logging.Info("Data berhasil diubah")
		return "Data berhasil diubah", nil
	} else {

		times := time.Now()

		// LAKUKAN INSERT DATA
		data := rme.PemeriksaanFisikModel{
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			InsertDevice:               req.DeviceID,
			InsertUserId:               userID,
			KetPerson:                  req.Person,
			Pelayanan:                  req.Pelayanan,
			KdBagian:                   kdBagian,
			Noreg:                      req.Noreg,
			Mata:                       req.Mata,
			Mulut:                      req.Mulut,
			Gigi:                       req.Gigi,
			Thyroid:                    req.Thyroid,
			Paru:                       req.Paru,
			Jantung:                    req.Jantung,
			Dindingdada:                req.DindingDada,
			DindingdadaRetEpigastrium:  req.Epigastrium,
			DindingdadaRetSuprastermal: req.Supratermal,
			Kategori:                   req.Kategori,
			DindingdadaRetraksi:        req.Retraksi,
			Hepar:                      req.Hepar,
			HeparDetail:                req.HeparDetail,
			Limpa:                      req.Limpa,
			LimpaDetail:                req.LimpaDetail,
			Ginjal:                     req.Ginjal,
			// GinjalDetail:               req.GinjalDetail,
			Genetalia:   req.Genetalia,
			Ouf:         req.Ouf,
			Ekstremitas: req.Ekstremitas,
			TugorKulit:  req.TugorKulit,
		}

		_, err1 := su.rmeRepository.SavePemeriksaanFisikRepository(userID, kdBagian, data)
		su.logging.Info("LAKUKAN SIMPAN")

		if err1 != nil {
			message := fmt.Sprintf("Error %s", err1.Error())
			return "Data gagal di simpan", errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")
		return "Data berhasil disimpan", nil
	}
}

func (su *rmeUseCase) SkalaNyeriTriaseIGDUsecase(userID string, kdBagian string, req dto.ReqSkalaNyeriTriaseeIGD) (message string, err error) {
	// LAKUKAN CHEK APAKAH DOKTER ATAU PERAWAT
	// data, err := su.rmeRepository.CariPemeriksaanFisikByDateRepository(userID, req.Person, kdBagian, req.Noreg)
	data, err := su.rmeRepository.GetPemeriksaanFisikRepositoryIGD(userID, req.Person, kdBagian, req.Noreg)

	su.logging.Info(data)

	if err != nil || data.Noreg == "" {
		// AMBIL DATA PASIEN TERDAHULU
		// GET TANGGAL MASUK PASIEN
		su.logging.Info("CARI TANGGAL")
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return message, errors.New(message)
		}

		times := time.Now()

		data := rme.SkalaTriaseModel{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			UpdateDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DeviceID,
			InsertUserId:   userID,
			Kategori:       req.Person,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			SkalaNyeri:     req.Nyeri,
			SkalaNyeriP:    req.NyeriP,
			SkalaNyeriQ:    req.NyeriQ,
			SkalaNyeriR:    req.NyeriR,
			SkalaNyeriS:    req.NyeriS,
			SkalaNyeriT:    req.NyeriT,
			FlaccWajah:     req.FlaccWajah,
			FlaccKaki:      req.FlaccKaki,
			FlaccAktifitas: req.FlaccAktifitas,
			FlaccMenangis:  req.FlaccMenangis,
			FlaccBersuara:  req.SkalaTriase,
			FlaccTotal:     req.FlaccTotal,
			SkalaTriase:    req.SkalaTriase,
		}

		_, err1 := su.rmeRepository.SaveSkalaTriaseRepository(data)

		if err1 != nil {
			message := fmt.Sprintf("Data gagal disimpan,  %s", err1.Error())
			return message, err1
		}

		return "Data berhasil disimpan", nil

	} else {
		su.logging.Info("LAKUKAN UPDATE TRIASE")

		_, err := su.rmeRepository.UpdateSkalaTriaseRepository(req, userID, kdBagian, data.Noreg)

		if err != nil {
			message := fmt.Sprintf("Data gagal diubah,  %s", err.Error())
			return message, errors.New(message)
		}

		return "Data berhasil diubah", nil
	}

}

func (su *rmeUseCase) SaveTandaVitalGangguanPerilakuUsecase(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (message string, err error) {
	// CHEK APAKAH DATA // SUDAH ADA PADA HARI INI
	fisik, _ := su.rmeRepository.CariPemeriksaanFisikByDateRepository(userID, req.Person, kdBagian, req.Noreg)
	su.logging.Info("CARI DATA VITAL SIGN")
	su.logging.Info(fisik)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {
		// LAKUKAN UPDATE DATE
		_, err1 := su.rmeRepository.UpdateTandaVitalGangguanPerilakuRepository(userID, kdBagian, req)

		su.logging.Info("LAKUKAN UPDATE")
		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal diubah", errors.New(message)
		}

		su.logging.Info("Data berhasil diubah")
		return "Data berhasil diubah", nil
	} else {

		times := time.Now()

		// LAKUKAN INSERT DATA
		data := rme.PemeriksaanFisikModel{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			Kategori:     req.Person,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			JalanNafas:   req.JalanNafas,
			// Td:             req.Td,
			// Pernafasan:     req.Pernafasan,
			PupilKiri:  req.PupilKiri,
			PupilKanan: req.PupilKanan,
			// Nadi:           req.Nadi,
			// Spo2:           req.Spo2,
			CahayaKanan: req.CahayaKanan,
			CahayaKiri:  req.CahayaKiri,
			// Suhu:           req.Suhu,
			Akral:    req.Akral,
			GcsE:     req.GcsE,
			GcsM:     req.GcsM,
			GcsV:     req.GcsV,
			Gangguan: req.Gangguan,
			// GangguanDetail: req.GangguanDetail,
		}
		_, err1 := su.rmeRepository.SavePemeriksaanFisikRepository(userID, kdBagian, data)

		su.logging.Info("LAKUKAN SIMPAN")

		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal di simpan", errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")
		return "Data berhasil disimpan", nil
	}
}

func (su *rmeUseCase) SaveTandaVItalIGDUsecase(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (res dto.ResTriaseTandaVitalIGD, message string, err error) {
	// CEK APAKAH DATA SUDAH ADA
	vitalSign, err12 := su.rmeRepository.GetDVitalSignDokterRepository(req.Noreg, kdBagian, req.Pelayanan, req.Person)

	if err12 != nil || vitalSign.Noreg == "" {
		// SIMPAN DATA BARU
		times := time.Now()
		// return res, "Data gagal dihapus", errs
		data := rme.DemfisikDokter{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			UpdateDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DeviceID,
			InsertUserId:   userID,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			Kategori:       req.Person,
			JalanNafas:     req.JalanNafas,
			PupilKanan:     req.PupilKanan,
			PupilKiri:      req.PupilKiri,
			Akral:          req.Akral,
			CahayaKanan:    req.CahayaKanan,
			CahayaKiri:     req.CahayaKiri,
			GcsE:           req.GcsE,
			GcsM:           req.GcsM,
			GcsV:           req.GcsV,
			Gangguan:       req.Gangguan,
			GangguanDetail: req.GangguanDetail,
		}
		vitalSigns, _ := su.rmeRepository.InserDemFisikDokterRepository(data)

		datas := rme.VitalSignDokter{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Kategori:     kdBagian,
			Td:           req.Td,
			Suhu:         req.Suhu,
			Pernafasan:   req.Pernafasan,
			Nadi:         req.Nadi,
			Spo2:         req.Spo2,
		}

		pemFisik, _ := su.rmeRepository.InsertDVitalSignDokterRepository(datas)

		mapper := su.rmeMapper.ToResponseTriaseVitalSignIGD(pemFisik, vitalSigns)

		return mapper, "Data berhasil disimpan", nil
	} else {
		times := time.Now()
		data := rme.DemfisikDokter{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			UpdateDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DeviceID,
			InsertUserId:   userID,
			Kategori:       req.Person,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			JalanNafas:     req.JalanNafas,
			PupilKanan:     req.PupilKanan,
			PupilKiri:      req.PupilKiri,
			Akral:          req.Akral,
			CahayaKanan:    req.CahayaKanan,
			CahayaKiri:     req.CahayaKiri,
			GcsE:           req.GcsE,
			GcsM:           req.GcsM,
			GcsV:           req.GcsV,
			Gangguan:       req.Gangguan,
			GangguanDetail: req.GangguanDetail,
		}

		updateDemfisik, _ := su.rmeRepository.UpdateDemFisikRepository(req.Person, req.Pelayanan, kdBagian, req.Noreg, data)

		datas := rme.VitalSignDokter{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Kategori:     kdBagian,
			Td:           req.Td,
			Suhu:         req.Suhu,
			Pernafasan:   req.Pernafasan,
			Nadi:         req.Nadi,
			Spo2:         req.Spo2,
		}

		// UPDATE VITAL SIG DOKTER
		vital, _ := su.rmeRepository.UpdateDVitalSignDokterRepository(req.Person, req.Pelayanan, kdBagian, req.Noreg, datas)

		// UPDATE DATA
		mapper := su.rmeMapper.ToResponseTriaseVitalSignIGD(vital, updateDemfisik)
		return mapper, "Data berhasil diubah", nil
	}

}

func (su *rmeUseCase) OnSaveVitalSignICU(userID string, kdBagian string, req dto.RegVitalSignGangguanPerilaku) (res dto.ResTriaseTandaVitalIGD, message string, err error) {
	// CEK APAKAH DATA SUDAH ADA
	vitalSign, err12 := su.rmeRepository.GetDVitalSignDokterRepository(req.Noreg, kdBagian, req.Pelayanan, req.Person)

	if err12 != nil || vitalSign.Noreg == "" {
		// SIMPAN DATA BARU
		times := time.Now()
		// return res, "Data gagal dihapus", errs
		data := rme.DemfisikDokter{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			UpdateDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DeviceID,
			InsertUserId:   userID,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			Kategori:       req.Person,
			JalanNafas:     req.JalanNafas,
			PupilKanan:     req.PupilKanan,
			PupilKiri:      req.PupilKiri,
			Akral:          req.Akral,
			CahayaKanan:    req.CahayaKanan,
			CahayaKiri:     req.CahayaKiri,
			GcsE:           req.GcsE,
			GcsM:           req.GcsM,
			GcsV:           req.GcsV,
			Gangguan:       req.Gangguan,
			GangguanDetail: req.GangguanDetail,
		}
		vitalSigns, _ := su.rmeRepository.InserDemFisikDokterRepository(data)

		datas := rme.VitalSignDokter{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Kategori:     kdBagian,
			Td:           req.Td,
			Suhu:         req.Suhu,
			Pernafasan:   req.Pernafasan,
			Nadi:         req.Nadi,
			Spo2:         req.Spo2,
		}

		pemFisik, _ := su.rmeRepository.InsertDVitalSignDokterRepository(datas)

		mapper := su.rmeMapper.ToResponseTriaseVitalSignIGD(pemFisik, vitalSigns)

		return mapper, "Data berhasil disimpan", nil
	} else {
		times := time.Now()
		data := rme.DemfisikDokter{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			UpdateDttm:     times.Format("2006-01-02 15:04:05"),
			InsertDevice:   req.DeviceID,
			InsertUserId:   userID,
			Kategori:       req.Person,
			KetPerson:      req.Person,
			Pelayanan:      req.Pelayanan,
			KdBagian:       kdBagian,
			Noreg:          req.Noreg,
			JalanNafas:     req.JalanNafas,
			PupilKanan:     req.PupilKanan,
			PupilKiri:      req.PupilKiri,
			Akral:          req.Akral,
			CahayaKanan:    req.CahayaKanan,
			CahayaKiri:     req.CahayaKiri,
			GcsE:           req.GcsE,
			GcsM:           req.GcsM,
			GcsV:           req.GcsV,
			Gangguan:       req.Gangguan,
			GangguanDetail: req.GangguanDetail,
		}

		updateDemfisik, _ := su.rmeRepository.UpdateDemFisikRepository(req.Person, req.Pelayanan, kdBagian, req.Noreg, data)

		datas := rme.VitalSignDokter{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Kategori:     kdBagian,
			Td:           req.Td,
			Suhu:         req.Suhu,
			Pernafasan:   req.Pernafasan,
			Nadi:         req.Nadi,
			Spo2:         req.Spo2,
		}

		// UPDATE VITAL SIG DOKTER
		vital, _ := su.rmeRepository.UpdateDVitalSignDokterRepository(req.Person, req.Pelayanan, kdBagian, req.Noreg, datas)

		// UPDATE DATA
		mapper := su.rmeMapper.ToResponseTriaseVitalSignIGD(vital, updateDemfisik)
		return mapper, "Data berhasil diubah", nil
	}

}

func (su *rmeUseCase) SaveVitalSignPerinaUseCase(userID string, kdBagian string, req dto.ReqDVitalSignPerina) (res dto.ResponseDVitalSignPerina, message string, err error) {
	// CEK APAKAH DATA SUDAH ADA
	data, err12 := su.rmeRepository.GetDVitalSignRepository(req.Noreg, kdBagian, req.Pelayanan, req.Person)

	if data.Noreg == "" || err12 != nil {
		times := time.Now()

		vitalSign := rme.DVitalSignModel{
			InsertDttm:    times.Format("2006-01-02 15:04:05"),
			UpdDttm:       times.Format("2006-01-02 15:04:05"),
			InsertDevice:  req.DeviceID,
			InsertUserId:  userID,
			Kategori:      req.Kategori,
			KdBagian:      kdBagian,
			KetPerson:     req.Person,
			Noreg:         req.Noreg,
			Td:            req.Td,
			Pernafasan:    req.RR,
			Hr:            req.HR,
			Pelayanan:     req.Pelayanan,
			LingkarKepala: req.LingkarKepala,
			LingkarDada:   req.LingkarDada,
			LingkarLengan: req.LingkarLengan,
			LingkarPerut:  req.LingkarPerut,
			Bb:            req.Bb,
			Tb:            req.Tb,
			Spo2:          req.Spo2,
			WarnaKulit:    req.WarnaKulit,
			KeadaanUmum:   req.KeadaanUmum,
			// ======= //
		}

		insert, err122 := su.rmeRepository.InsertTandaVitalPerinaRepository(vitalSign)

		if err122 != nil {
			return res, "Data gagal di simpan", err122
		}

		mapper := su.rmeMapper.ToMapperResponseDVitalSignPerina(insert)

		return mapper, "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		update, er12 := su.rmeRepository.UpdateTandaVitalPerinaRepository(kdBagian, req.Pelayanan, req.Noreg, req)

		if er12 != nil {
			return res, "Data gagal di simpan", er12
		}

		mapper := su.rmeMapper.ToMapperResponseDVitalSignPerina(update)

		return mapper, "Data berhasil disimpan", nil
	}

}

func (su *rmeUseCase) OnDeleteDcpptUsecase(idCppt int, noRM string) (res map[string]interface{}, messag string, err error) {
	errs := su.rmeRepository.OnDeleteCPPTRepository(idCppt)

	if errs != nil {
		return res, "Data gagal dihapus", errs
	}

	cari, dass := su.rmeRepository.CariCPPTRepository(noRM)

	if dass != nil {
		su.logging.Info(dass.Error())
		return res, dass.Error(), dass
	}

	mapper := su.rmeMapper.ToCPPTResponse(cari)

	m := map[string]any{}
	if len(mapper) > 0 {
		m["cppt"] = mapper
	} else {
		m["cppt"] = []string{}
	}

	return m, "", nil

}

func (su *rmeUseCase) SaveVitalSignBangsalUsecase(userID string, kdBagian string, req dto.ReqVitalSignBangsal) (message string, err error) {
	// CHEK APAKAH DATA SUDAH ADA PADA HARI INI // CARI DATA DVITAL SIGN HARI INI
	fisik, _ := su.rmeRepository.CariDVitalSignRepository(userID, kdBagian, req.Noreg)
	su.logging.Info("CARI DATA VITAL SIGN")
	su.logging.Info(fisik)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {

		_, err1 := su.rmeRepository.UpdateVitalSignBangsalRepository(userID, kdBagian, req)

		su.logging.Info("LAKUKAN UPDATE")

		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal diubah", errors.New(message)
		}

		su.logging.Info("Data berhasil diubah")
		return "Data berhasil diubah", nil

	} else {
		// LAKUKAN INSERT DATA
		_, err1 := su.rmeRepository.InsertVitalSignBangsalRepository(userID, kdBagian, req)
		su.logging.Info("LAKUKAN SIMPAN")

		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal di simpan", errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")
		return "Data berhasil disimpan", nil
	}
}

func (su *rmeUseCase) SaveVitalSignICUUseCase(userID string, kdBagian string, req dto.OnSaveVitalSignICU) (message string, err error) {
	fisik, _ := su.rmeRepository.CariDVitalSignRepository(userID, kdBagian, req.Noreg)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {

		_, err1 := su.rmeRepository.UpdateVitalSignICURepository(userID, kdBagian, req)

		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal diubah", errors.New(message)
		}

		return "Data berhasil diubah", nil

	} else {

		_, err1 := su.rmeRepository.InsertVitalSignICURepository(userID, kdBagian, req)

		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return "Data gagal di simpan", errors.New(message)
		}

		return "Data berhasil disimpan", nil
	}
}

func (su *rmeUseCase) SavePemeriksaanFisikIGDUsecase(userID string, kdBagian string, req dto.ReqPemeriksaanFisikIGD) (res dto.ResponsePemeriksaanFisikGID, message string, err error) {
	// CHEK APAKAH DATA SUDAH ADA PADA HARI INI
	fisik, _ := su.rmeRepository.CariPemeriksaanFisikByDateRepository(userID, req.Person, kdBagian, req.Noreg)

	su.logging.Info("CARI DATA PEMERIKSAAN")
	su.logging.Info(fisik)

	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {
		// LAKUKAN UPDATE DATE
		update, err1 := su.rmeRepository.UpdatePemeriksaanFisikRepository(userID, kdBagian, req.Noreg, req)

		su.logging.Info("LAKUKAN UPDATE")
		if err1 != nil {
			message := fmt.Sprintf(" %s", err1.Error())
			return res, "Data gagal diubah", errors.New(message)
		}

		mapper := su.rmeMapper.ToResponsePemeriksaanFisikIGD(update)
		su.logging.Info("Data berhasil diubah")
		return mapper, "Data berhasil diubah", nil

	} else {
		// LAKUKAN INSERT DATA
		data, err1 := su.rmeRepository.InsertPemeriksaanFisikRepository(userID, kdBagian, req)

		if err1 != nil {
			message := fmt.Sprintf("Error %s", err1.Error())
			return res, "Data gagal di simpan", errors.New(message)
		}

		mapper := su.rmeMapper.ToResponsePemeriksaanFisikIGD(data)
		su.logging.Info("Data berhasil disimpan")
		return mapper, "Data berhasil disimpan", nil

	}
}

func (su *rmeUseCase) SaveIntervensiResikoJatuhUseCase(userID string, kdBagian string, req dto.ReqIntervensiResikoJatuh) (message string, err error) {
	times := time.Now()

	// CARI DATA INTERVENSI
	datas, errs := su.rmeRepository.GetDataIntervensiIntervensiResikoJatuhReporitory(kdBagian, req.Noreg, req.Shift)

	su.logging.Info("Data ditemukan")

	if datas.Noreg == "" || errs != nil {

		su.logging.Info("INSERT DATA")
		var data = rme.DIntervensiRisiko{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			InsertUserId: userID,
			KdBagian:     kdBagian,
			InsertPc:     req.DeviceId,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			Noreg:        req.Noreg,
			Shift:        req.Shift,
			Resiko1:      req.RisikoJatuh[0].KResikoJatuhPasien[0].IsEnable,
			Resiko2:      req.RisikoJatuh[0].KResikoJatuhPasien[1].IsEnable,
			Resiko3:      req.RisikoJatuh[0].KResikoJatuhPasien[2].IsEnable,
			Resiko4:      req.RisikoJatuh[0].KResikoJatuhPasien[3].IsEnable,
			Resiko5:      req.RisikoJatuh[0].KResikoJatuhPasien[4].IsEnable,
			Resiko6:      req.RisikoJatuh[0].KResikoJatuhPasien[5].IsEnable,
			Resiko7:      req.RisikoJatuh[0].KResikoJatuhPasien[6].IsEnable,
			Resiko8:      req.RisikoJatuh[1].KResikoJatuhPasien[0].IsEnable,
			Resiko9:      req.RisikoJatuh[1].KResikoJatuhPasien[1].IsEnable,
			Resiko10:     req.RisikoJatuh[1].KResikoJatuhPasien[2].IsEnable,
			Resiko11:     req.RisikoJatuh[1].KResikoJatuhPasien[3].IsEnable,
			Resiko12:     req.RisikoJatuh[1].KResikoJatuhPasien[4].IsEnable,
			Resiko13:     req.RisikoJatuh[1].KResikoJatuhPasien[5].IsEnable,
			Resiko14:     req.RisikoJatuh[1].KResikoJatuhPasien[6].IsEnable,
			Resiko15:     req.RisikoJatuh[1].KResikoJatuhPasien[7].IsEnable,
			Resiko16:     req.RisikoJatuh[1].KResikoJatuhPasien[8].IsEnable,
			Resiko17:     req.RisikoJatuh[1].KResikoJatuhPasien[9].IsEnable,
		}

		_, errs := su.rmeRepository.InsertIntervensiRisikoRespository(data)

		if errs != nil {
			message := fmt.Sprintf("Data gagal disimpan,  %s", errs.Error())
			return message, errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")

		return "Data berhasil disimpan", nil

	} else {

		_, err1s := su.rmeRepository.UpdateIntervensiRisikoRepository(kdBagian, req)

		if err1s != nil {
			message := fmt.Sprintf("Data gagal diupdate,  %s", err1s.Error())
			return message, errors.New(message)
		}

		su.logging.Info("Data berhasil diupdate")

		return "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) SaveResikoJatuhPasienUseCase(userID string, kdBagian string, req dto.ReqResikoJatuhPasien) (message string, err error) {
	// CARI TERLEBIH DAHULU APAKAH ADA DATA TERSEBUT
	datas, errs := su.rmeRepository.CariResikoJatuhPasienRepository(kdBagian, req.Noreg, req.Jenis)

	if datas.Noreg == "" || errs != nil {
		// LAKUKAN SIMPAN DATA // CHEK JENIS RESIKO
		su.logging.Info("LAKUKAN SIMPAN DATA")

		switch req.Jenis {
		case "Anak":
			var umur = ""
			var jk = ""
			var diagnosa = ""
			var gangguan = ""
			var faktor = ""
			var response = ""
			var penggunaan = ""
			// SIMPAN DATA UNTUK JENIS RESIKO ANAK
			su.logging.Info("SIMPAN JENIS RESIKO ANAK")

			for _, umurs := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if umurs.IsEnable {
					umur = umurs.KategoriFaktor
				}
			}

			for _, jks := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if jks.IsEnable {
					jk = jks.KategoriFaktor
				}
			}

			for _, diagnosas := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if diagnosas.IsEnable {
					diagnosa = diagnosas.KategoriFaktor
				}
			}

			for _, gangguans := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if gangguans.IsEnable {
					gangguan = gangguans.KategoriFaktor
				}
			}

			for _, faktors := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if faktors.IsEnable {
					faktor = faktors.KategoriFaktor
				}
			}

			for _, responses := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if responses.IsEnable {
					response = responses.KategoriFaktor
				}
			}
			for _, penggunaans := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if penggunaans.IsEnable {
					penggunaan = penggunaans.KategoriFaktor
				}
			}

			times := time.Now()

			var datas = rme.DRisikoJatuh{
				InsertDttm:       times.Format("2006-01-02 15:04:05"),
				UpdDttm:          times.Format("2006-01-02 15:04:05"),
				InsertUserId:     userID,
				KdBagian:         kdBagian,
				InsertPc:         req.DeviceID,
				KetPerson:        req.Person,
				Pelayanan:        req.Pelayanan,
				Noreg:            req.Noreg,
				Usia:             umur,
				JenisKelamin:     jk,
				Kategori:         "Anak",
				Diagnosis:        diagnosa,
				GangguanKognitif: gangguan,
				FaktorLingkungan: faktor,
				Respon:           response,
				PenggunaanObat:   penggunaan,
				Total:            req.Skor,
			}

			// LAKUKAN SIMPAN DATA
			_, err1 := su.rmeRepository.InsertResikoJatuhPasienRepository(datas)

			if err1 != nil {
				message := fmt.Sprintf("Data gagal disimpan,  %s", err1.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil disimpan")
			return "Data berhasil disimpan", nil
		case "Morse":
			su.logging.Info("SIMPAN RESIKO DEWASA MORSE")
			times := time.Now()

			// ==== //

			var jatuh = ""
			var diagnosa = ""
			var ambulasi = ""
			var terapi = ""
			var infuse = ""
			var berjalan = ""
			var mental = ""

			for _, jatuhs := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if jatuhs.IsEnable {
					jatuh = jatuhs.KategoriFaktor
				}
			}

			for _, diagnosas := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if diagnosas.IsEnable {
					diagnosa = diagnosas.KategoriFaktor
				}
			}

			for _, ambulasis := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if ambulasis.IsEnable {
					ambulasi = ambulasis.KategoriFaktor
				}
			}

			for _, terapis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if terapis.IsEnable {
					terapi = terapis.KategoriFaktor
				}
			}

			for _, infuses := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if infuses.IsEnable {
					infuse = infuses.KategoriFaktor
				}
			}

			for _, berjalans := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if berjalans.IsEnable {
					berjalan = berjalans.KategoriFaktor
				}
			}

			for _, mentals := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if mentals.IsEnable {
					mental = mentals.KategoriFaktor
				}
			}

			var datas = rme.DRisikoJatuh{
				InsertDttm:      times.Format("2006-01-02 15:04:05"),
				UpdDttm:         times.Format("2006-01-02 15:04:05"),
				InsertUserId:    userID,
				KdBagian:        kdBagian,
				InsertPc:        req.DeviceID,
				KetPerson:       req.Person,
				Pelayanan:       req.Pelayanan,
				Noreg:           req.Noreg,
				RJatuh:          jatuh,
				Kategori:        req.Jenis,
				Diagnosis:       diagnosa,
				BAmbulasi:       ambulasi,
				Terapi:          terapi,
				TerpasangInfuse: infuse,
				GayaBerjalan:    berjalan,
				StatusMental:    mental,
				Total:           req.Skor,
			}

			// LAKUKAN SIMPAN DATA
			_, err1 := su.rmeRepository.InsertResikoJatuhPasienRepository(datas)

			if err1 != nil {
				message := fmt.Sprintf("Data gagal disimpan,  %s", err1.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil disimpan")
			return "Data berhasil disimpan", nil
		case "ReAsesmen-Anak":
			su.logging.Info("SIMPAN RESIKO DEWASA MORSE")
			times := time.Now()

			// ==== //

			var jatuh = ""
			var diagnosa = ""
			var ambulasi = ""
			var terapi = ""
			var infuse = ""
			var berjalan = ""
			var mental = ""

			for _, jatuhs := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if jatuhs.IsEnable {
					jatuh = jatuhs.KategoriFaktor
				}
			}

			for _, diagnosas := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if diagnosas.IsEnable {
					diagnosa = diagnosas.KategoriFaktor
				}
			}

			for _, ambulasis := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if ambulasis.IsEnable {
					ambulasi = ambulasis.KategoriFaktor
				}
			}

			for _, terapis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if terapis.IsEnable {
					terapi = terapis.KategoriFaktor
				}
			}

			for _, infuses := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if infuses.IsEnable {
					infuse = infuses.KategoriFaktor
				}
			}

			for _, berjalans := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if berjalans.IsEnable {
					berjalan = berjalans.KategoriFaktor
				}
			}

			for _, mentals := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if mentals.IsEnable {
					mental = mentals.KategoriFaktor
				}
			}

			var datas = rme.DRisikoJatuh{
				InsertDttm:      times.Format("2006-01-02 15:04:05"),
				UpdDttm:         times.Format("2006-01-02 15:04:05"),
				InsertUserId:    userID,
				KdBagian:        kdBagian,
				InsertPc:        req.DeviceID,
				KetPerson:       req.Person,
				Pelayanan:       req.Pelayanan,
				Noreg:           req.Noreg,
				RJatuh:          jatuh,
				Kategori:        req.Jenis,
				Diagnosis:       diagnosa,
				BAmbulasi:       ambulasi,
				Terapi:          terapi,
				TerpasangInfuse: infuse,
				GayaBerjalan:    berjalan,
				StatusMental:    mental,
				Total:           req.Skor,
			}

			// LAKUKAN SIMPAN DATA
			_, err1 := su.rmeRepository.InsertResikoJatuhPasienRepository(datas)

			if err1 != nil {
				message := fmt.Sprintf("Data gagal disimpan,  %s", err1.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil disimpan")
			return "Data berhasil disimpan", nil
		case "Dewasa":
			// ===//=== //
			su.logging.Info("SIMPAN RESIKO DEWASA ")
			times := time.Now()
			// ==== //

			var usia = ""
			var riwayat = ""
			var aktivitas = ""
			var mobilisasi = ""
			var kognitif = ""
			var sensori = ""
			var pengobatan = ""
			var komorbiditas = ""

			for _, usias := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if usias.IsEnable {
					usia = usias.KategoriFaktor
				}
			}

			for _, riwayats := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if riwayats.IsEnable {
					riwayat = riwayats.KategoriFaktor
				}
			}

			for _, aktivitass := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if aktivitass.IsEnable {
					aktivitas = aktivitass.KategoriFaktor
				}
			}

			for _, mobilisasis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if mobilisasis.IsEnable {
					mobilisasi = mobilisasis.KategoriFaktor
				}
			}

			for _, kognitifs := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if kognitifs.IsEnable {
					kognitif = kognitifs.KategoriFaktor
				}
			}

			for _, sensoris := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if sensoris.IsEnable {
					sensori = sensoris.KategoriFaktor
				}
			}

			for _, pengobatans := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if pengobatans.IsEnable {
					pengobatan = pengobatans.KategoriFaktor
				}
			}
			for _, komorbiditass := range req.RisikoJatuh[7].KResikoJatuhPasien {
				if komorbiditass.IsEnable {
					komorbiditas = komorbiditass.KategoriFaktor
				}
			}

			var datas = rme.DRisikoJatuh{
				InsertDttm:     times.Format("2006-01-02 15:04:05"),
				UpdDttm:        times.Format("2006-01-02 15:04:05"),
				InsertUserId:   userID,
				KdBagian:       kdBagian,
				InsertPc:       req.DeviceID,
				KetPerson:      req.Person,
				Pelayanan:      req.Pelayanan,
				Noreg:          req.Noreg,
				Kategori:       req.Jenis,
				Usia:           usia,
				RJatuh:         riwayat,
				Aktivitas:      aktivitas,
				Mobilisasi:     mobilisasi,
				Kognitif:       kognitif,
				DefisitSensori: sensori,
				Pengobatan:     pengobatan,
				Komorbiditas:   komorbiditas,
				Total:          req.Skor,
			}

			// LAKUKAN SIMPAN DATA
			_, err1 := su.rmeRepository.InsertResikoJatuhPasienRepository(datas)

			if err1 != nil {
				message := fmt.Sprintf("Data gagal disimpan,  %s", err1.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil disimpan")
			return "Data berhasil disimpan", nil

		default:
			su.logging.Info("Data gagal disimpan")
			message := fmt.Sprintf("Data gagal disimpan, %s", "")
			return "Data gagal disimpan", errors.New(message)

		}

	} else {
		// ===================================== UPDATE RESIKO ANAK ===================== //
		switch req.Jenis {
		case "Anak":
			su.logging.Info("Update Resiko Anak")

			var umur = ""
			var jk = ""
			var diagnosa = ""
			var gangguan = ""
			var faktor = ""
			var response = ""
			var penggunaan = ""
			// SIMPAN DATA UNTUK JENIS RESIKO ANAK
			su.logging.Info("SIMPAN JENIS RESIKO ANAK")

			for _, umurs := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if umurs.IsEnable {
					umur = umurs.KategoriFaktor
				}
			}

			for _, jks := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if jks.IsEnable {
					jk = jks.KategoriFaktor
				}
			}

			for _, diagnosas := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if diagnosas.IsEnable {
					diagnosa = diagnosas.KategoriFaktor
				}
			}

			for _, gangguans := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if gangguans.IsEnable {
					gangguan = gangguans.KategoriFaktor
				}
			}

			for _, faktors := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if faktors.IsEnable {
					faktor = faktors.KategoriFaktor
				}
			}

			for _, responses := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if responses.IsEnable {
					response = responses.KategoriFaktor
				}
			}

			for _, penggunaans := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if penggunaans.IsEnable {
					penggunaan = penggunaans.KategoriFaktor
				}
			}

			// UPDATE DATA ANAK
			su.logging.Info("UPDATE DATA RESIKO ANAK")
			_, err2 := su.rmeRepository.UpdateResikoJatuhPasienAnakRepositori(kdBagian, req.Noreg, umur, jk, diagnosa, gangguan, faktor, response, penggunaan, req.Skor)

			if err2 != nil {
				message := fmt.Sprintf("Data gagal diubah,  %s", err2.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil diubah")
			return "Data berhasil diubah", nil
		// ===================================== UPDATE RESIKO MORIS DEWASA =========================== //
		case "Morse":
			su.logging.Info("SIMPAN RESIKO DEWASA MORSE")

			// ==== //

			var jatuh = ""
			var diagnosa = ""
			var ambulasi = ""
			var terapi = ""
			var infuse = ""
			var berjalan = ""
			var mental = ""

			for _, jatuhs := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if jatuhs.IsEnable {
					jatuh = jatuhs.KategoriFaktor
				}
			}

			for _, diagnosas := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if diagnosas.IsEnable {
					diagnosa = diagnosas.KategoriFaktor
				}
			}

			for _, ambulasis := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if ambulasis.IsEnable {
					ambulasi = ambulasis.KategoriFaktor
				}
			}

			for _, terapis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if terapis.IsEnable {
					terapi = terapis.KategoriFaktor
				}
			}

			for _, infuses := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if infuses.IsEnable {
					infuse = infuses.KategoriFaktor
				}
			}

			for _, berjalans := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if berjalans.IsEnable {
					berjalan = berjalans.KategoriFaktor
				}
			}

			for _, mentals := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if mentals.IsEnable {
					mental = mentals.KategoriFaktor
				}
			}

			su.logging.Info("UPDATE DATA RESIKO MORSE")
			_, err2 := su.rmeRepository.UpdateResikoJatuhPasienMorseRepositori(kdBagian, req.Noreg, jatuh, diagnosa, ambulasi, terapi, infuse, berjalan, mental, req.Skor, req.Jenis)

			if err2 != nil {
				message := fmt.Sprintf("Data gagal diubah,  %s", err2.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil diubah")
			return "Data berhasil diubah", nil
		//==================================  UPDATE JENIS DEWASA ===================== //
		case "Dewasa":
			var usia = ""
			var riwayat = ""
			var aktivitas = ""
			var mobilisasi = ""
			var kognitif = ""
			var sensori = ""
			var pengobatan = ""
			var komorbiditas = ""

			for _, usias := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if usias.IsEnable {
					usia = usias.KategoriFaktor
				}
			}

			for _, riwayats := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if riwayats.IsEnable {
					riwayat = riwayats.KategoriFaktor
				}
			}

			for _, aktivitass := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if aktivitass.IsEnable {
					aktivitas = aktivitass.KategoriFaktor
				}
			}

			for _, mobilisasis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if mobilisasis.IsEnable {
					mobilisasi = mobilisasis.KategoriFaktor
				}
			}

			for _, kognitifs := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if kognitifs.IsEnable {
					kognitif = kognitifs.KategoriFaktor
				}
			}

			for _, sensoris := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if sensoris.IsEnable {
					sensori = sensoris.KategoriFaktor
				}
			}

			for _, pengobatans := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if pengobatans.IsEnable {
					pengobatan = pengobatans.KategoriFaktor
				}
			}
			for _, komorbiditass := range req.RisikoJatuh[7].KResikoJatuhPasien {
				if komorbiditass.IsEnable {
					komorbiditas = komorbiditass.KategoriFaktor
				}
			}

			su.logging.Info("UPDATE DATA RESIKO MORSE")
			_, err2 := su.rmeRepository.UpdateResikoJatuhPasienDewasaRepository(kdBagian, req.Noreg, req.Jenis, usia, riwayat, aktivitas, mobilisasi, kognitif, sensori, pengobatan, komorbiditas, req.Skor)

			if err2 != nil {
				message := fmt.Sprintf("Data gagal diubah,  %s", err2.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil diubah")
			return "Data berhasil diubah", nil
			//==================================  UPDATE JENIS DEWASA ===================== //
		case "ReAsesmen-Anak":
			su.logging.Info("SIMPAN RESIKO DEWASA MORSE")

			var usia = ""
			var riwayat = ""
			var aktivitas = ""
			var mobilisasi = ""
			var kognitif = ""
			var sensori = ""
			var pengobatan = ""
			var komorbiditas = ""

			for _, usias := range req.RisikoJatuh[0].KResikoJatuhPasien {
				if usias.IsEnable {
					usia = usias.KategoriFaktor
				}
			}

			for _, riwayats := range req.RisikoJatuh[1].KResikoJatuhPasien {
				if riwayats.IsEnable {
					riwayat = riwayats.KategoriFaktor
				}
			}

			for _, aktivitass := range req.RisikoJatuh[2].KResikoJatuhPasien {
				if aktivitass.IsEnable {
					aktivitas = aktivitass.KategoriFaktor
				}
			}

			for _, mobilisasis := range req.RisikoJatuh[3].KResikoJatuhPasien {
				if mobilisasis.IsEnable {
					mobilisasi = mobilisasis.KategoriFaktor
				}
			}

			for _, kognitifs := range req.RisikoJatuh[4].KResikoJatuhPasien {
				if kognitifs.IsEnable {
					kognitif = kognitifs.KategoriFaktor
				}
			}

			for _, sensoris := range req.RisikoJatuh[5].KResikoJatuhPasien {
				if sensoris.IsEnable {
					sensori = sensoris.KategoriFaktor
				}
			}

			for _, pengobatans := range req.RisikoJatuh[6].KResikoJatuhPasien {
				if pengobatans.IsEnable {
					pengobatan = pengobatans.KategoriFaktor
				}
			}

			for _, komorbiditass := range req.RisikoJatuh[7].KResikoJatuhPasien {
				if komorbiditass.IsEnable {
					komorbiditas = komorbiditass.KategoriFaktor
				}
			}

			su.logging.Info("UPDATE DATA RESIKO MORSE")
			_, err2 := su.rmeRepository.UpdateResikoJatuhPasienDewasaRepository(kdBagian, req.Noreg, req.Jenis, usia, riwayat, aktivitas, mobilisasi, kognitif, sensori, pengobatan, komorbiditas, req.Skor)

			if err2 != nil {
				message := fmt.Sprintf("Data gagal diubah,  %s", err2.Error())
				return message, errors.New(message)
			}

			su.logging.Info("Data berhasil diubah")
			return "Data berhasil diubah", nil
		default:
			su.logging.Info("Data gagal disimpan")
			message := fmt.Sprintf("Data gagal disimpan, %s", "")
			return "Data gagal disimpan", errors.New(message)
		}

	}
}

// ====== GET ALERT RESIKO JATUH PASIEN ====== //
func (su *rmeUseCase) GetAlertResikoJatuhPasienUseCase(kdBagian string, noReg string) (res dto.ResikoJatuhResponse, err error) {

	// GET HASIL RESIKO
	data, errs := su.rmeRepository.GetHasilResikoJatuhRepository(noReg)

	if errs != nil {
		message := fmt.Sprintf("Error %s", errs.Error())
		return res, errors.New(message)
	}

	mapper := su.rmeMapper.ToHasilResikoJatuhPasienMapper(data)

	return mapper, nil
}

// USECASE LAKUKAN SDKI MAPPING
func (su *rmeUseCase) GetDeskripsiLuaranSDKUseCase(kodeSDKi string) (res dto.SikiResponse, message string, err error) {
	// LAKUKAN GET DIAGNOSA
	sdki, err1 := su.rmeRepository.GetSDKIByIDRepository(kodeSDKi)

	if err1 != nil {
		message := fmt.Sprintf("Error %s", err1.Error())
		return res, "Gagal mendapatkan data", errors.New(message)
	}

	if len(sdki.Kode) < 1 {
		return res, "Gagal ", errors.New("data tidak ditemukan")
	}

	// JIKA DATA DITEMUKAN LAKUKAN PARSING SLKI
	parts := strings.Split(sdki.MappingSlki, ",")

	var data []rme.DSlki

	// LAKUKAN PARSING PARTS
	for _, part := range parts {
		if len(part) > 0 {

			// LAKUKAN QUERY DATA DSLKI
			dat, err2 := su.rmeRepository.GetDataDSLKIRepository(part)

			if err2 != nil {
				return res, "Gagal ", errors.New("data tidak ditemukan")
			}

			data = append(data, dat)
		}
	}

	// LAKUKAN MAPPING
	var newSLKI []dto.ResponseDataSLKI

	for _, valu := range data {
		if len(valu.KodeSlki) > 0 {
			responseItem := dto.ResponseDataSLKI{
				KodeSlki:  valu.KodeSlki,
				Judul:     valu.Judul,
				Defenisi:  valu.Defenisi,
				Ekspetasi: valu.Ekspetasi,
				Menurun:   su.rmeMapper.ToMapperResponseKriteriaModel(filterKriteriaSLKIModel(valu.DKriteriaSLKIModel, "Menurun")),
				Meningkat: su.rmeMapper.ToMapperResponseKriteriaModel(filterKriteriaSLKIModel(valu.DKriteriaSLKIModel, "Meningkat")),
				Memburuk:  su.rmeMapper.ToMapperResponseKriteriaModel(filterKriteriaSLKIModel(valu.DKriteriaSLKIModel, "Memburuk")),
			}

			newSLKI = append(newSLKI, responseItem)
		}
	}

	su.logging.Info("MAPPING DATA SIKI")
	su.logging.Info(sdki.MappingSiki)

	// LAKUKAN CARI DESKRIPSI USECASE
	newData, err2 := su.CariDeskripsiSikiUsecase(sdki.MappingSiki)

	if err2 != nil {
		message := fmt.Sprintf("Error %s", err2.Error())
		return res, "Gagal mendapatkan data", errors.New(message)
	}

	mapper := su.rmeMapper.ToSDKIResponseMapper(sdki, newSLKI, newData)

	return mapper, "OK", nil
}

func (su *rmeUseCase) GetVitalSignUseCase(noReg string, person string, bagian string, pelayanan string) (res dto.ResTriaseTandaVitalIGD, err error) {
	// GET DPEMFISIK
	vitalSign, _ := su.rmeRepository.GetDVitalSignDokterRepository(noReg, bagian, pelayanan, person)
	pemFisik, _ := su.rmeRepository.GetDemFisikDokterRepository(noReg, person, bagian, pelayanan)

	// GET VITAL SIGN  MAPPING VITAL SIGN
	mapper := su.rmeMapper.ToResponseTriaseVitalSignIGD(vitalSign, pemFisik)
	su.logging.Info(mapper)
	su.logging.Info(vitalSign)
	su.logging.Info(pemFisik)
	return mapper, nil
}

func (su *rmeUseCase) OnSaveDataEdukasiTerintegrasiUseCase(kdBagian string, userID string, req dto.ReqOnSaveDataEdukasiTerintegrasi) (res rme.DEdukasiTerintegrasi, message string, err error) {
	// SAVE DATA
	save, er123 := su.rmeRepository.OnSaveDataEdukasiTerintegrasiRepository(userID, kdBagian, req)

	if er123 != nil {
		message := fmt.Sprintf("Error %s", er123.Error())
		return res, "Gagal disimpan", errors.New(message)
	}

	return save, "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnSaveVitalSignPerinaUseCase() (err error) {

	return nil
}

// LAKUKAN FILTER []DKriteriaSLKIModel KRITERIA
func filterKriteriaSLKIModel(items []rme.DKriteriaSLKIModel, kategori string) []rme.DKriteriaSLKIModel {

	var filteredItems []rme.DKriteriaSLKIModel

	for _, item := range items {
		if item.Kategori == kategori {
			filteredItems = append(filteredItems, item)
		}
	}

	return filteredItems
}

// PASTIKAN DATA DAPAT DI INSERT
func (su *rmeUseCase) GetNomorDaskepUsecase() (nomr rme.NoDaskep, err error) {
	// LAKUKAN CARI NOMOR DASKEP
	su.logging.Info("LAKUKAN GET DASKEP")
	nomor, err1 := su.rmeRepository.GetNomorDaskepDiagnosaRepository()

	if err1 != nil {
		su.logging.Info("ERROR GET DASKEP")
		message := fmt.Sprintf("Error %s", err1.Error())
		return nomor, errors.New(message)
	}

	// PASTIKAN NOMOR BELUM ADA
	data, err3 := su.rmeRepository.CariNomorDaskepDiagnosaRepository(nomor.Daskep)
	su.logging.Info("LAKUKAN CARI DASKEP")

	if err3 != nil {
		su.logging.Info("ERROR CARI DASKEP")
		message := fmt.Sprintf("Error %s", err3.Error())
		return nomor, errors.New(message)
	}

	if len(data.NoDaskep) > 1 {
		su.logging.Info("DASKEP DITEMUKAN")
		no2, err2 := su.GetNomorDaskepUsecase()
		return no2, err2

	}

	su.logging.Info("DASKEP KEADAAN KOSONG")
	return nomor, nil
}

func (su *rmeUseCase) SaveDeskripsiLuaranSDKIUserCase(kdBagian string, userID string, req dto.SikiRequest) (message string, err error) {
	// AMBIL NOMOR DASKEP
	times := time.Now()

	su.logging.Info("LAKUKAN GET NOMOR DASKEP")
	no, err2 := su.rmeRepository.GetNomorDaskepDiagnosaRepository()
	su.logging.Info(no.Daskep)

	if err2 != nil {
		message := fmt.Sprintf("Error %s", err2.Error())
		return "Data gagal disimpan", errors.New(message)
	}

	// SIMPAN DATA BARU KE DATA DASKEP DIAGNOSA
	// LOOPING ATA DASKEP
	var dataDaskep = rme.DaskepDiagnosaModel{
		KetPerson:  req.Person,
		UserId:     userID,
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		Tanggal:    times.Format("2006-01-02"),
		KdBagian:   kdBagian,
		KodeSdki:   req.SDKIResponse.Kode,
		NoDaskep:   no.Daskep,
		Noreg:      req.Noreg,
		InsertPc:   req.DeviceID,
	}

	// PERTAMA LAKUKAN SIMPAN DATA DIAGNOSA TERLEBIH DAHULU
	_, err4 := su.rmeRepository.SaveDataDaskepDiagnosaRespository(dataDaskep)

	if err4 != nil {
		message := fmt.Sprintf("Error %s", err4.Error())
		return "Data gagal disimpan", errors.New(message)
	}

	for i := 0; i <= len(req.SLKIResponse)-1; i++ {
		// FILTER MENURUN
		for _, memburuk := range req.SLKIResponse[i].Memburuk {

			if memburuk.IsSelected {
				newDaskepSLKI := rme.DaskepSLKIModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					IdKriteria: memburuk.IdKriteria,
					NoDaskep:   no.Daskep,
					Kategori:   "Memburuk",
					NamaSlki:   memburuk.NamaKriteria,
					NoUrut:     memburuk.NoUrut,
					KodeSlki:   memburuk.KodeSlki,
					Target:     memburuk.Target,
					Waktu:      memburuk.Waktu,
				}

				// LAKUKAN SIMPAN DATA
				su.logging.Info("LAKUKAN SIMPAN DASKEP")
				_, ee := su.rmeRepository.SaveDataDaskepSLKI(newDaskepSLKI)

				if ee != nil {
					message := fmt.Sprintf("Error %s", ee.Error())
					return "Gagal mendapatkan data", errors.New(message)
				}
			}
		}

		for _, meningkat := range req.SLKIResponse[i].Meningkat {
			if meningkat.IsSelected {
				newDaskepSLKI := rme.DaskepSLKIModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					IdKriteria: meningkat.IdKriteria,
					NamaSlki:   meningkat.NamaKriteria,
					NoUrut:     meningkat.NoUrut,
					Kategori:   "Meningkat",
					NoDaskep:   no.Daskep,
					KodeSlki:   meningkat.KodeSlki,
					Target:     meningkat.Target,
					Waktu:      meningkat.Waktu,
				}

				// LAKUKAN SIMPAN DATA
				su.logging.Info("LAKUKAN SIMPAN DASKEP")
				_, ee := su.rmeRepository.SaveDataDaskepSLKI(newDaskepSLKI)

				if ee != nil {
					message := fmt.Sprintf("Error %s", ee.Error())
					return "Gagal mendapatkan data", errors.New(message)
				}
			}
		}
		for _, menurun := range req.SLKIResponse[i].Menurun {
			if menurun.IsSelected {
				newDaskepSLKI := rme.DaskepSLKIModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					IdKriteria: menurun.IdKriteria,
					NamaSlki:   menurun.NamaKriteria,
					Kategori:   "Menurun",
					NoUrut:     menurun.NoUrut,
					NoDaskep:   no.Daskep,
					KodeSlki:   menurun.KodeSlki,
					Target:     menurun.Target,
					Waktu:      menurun.Waktu,
				}

				// LAKUKAN SIMPAN DATA
				su.logging.Info("LAKUKAN SIMPAN DASKEP")
				_, ee := su.rmeRepository.SaveDataDaskepSLKI(newDaskepSLKI)

				if ee != nil {
					message := fmt.Sprintf("Error %s", ee.Error())
					return "Gagal mendapatkan data", errors.New(message)
				}
			}
		}

	}

	// LAKUKAN SIMPAN DATA

	for ia := 0; ia <= len(req.SikiResponse)-1; ia++ {
		for _, edu := range req.SikiResponse[ia].Edukasi {
			if edu.IsSelected {
				newDataSiki := rme.DaskepSikiModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					NoDaskep:   no.Daskep,
					IdSiki:     edu.IdSiki,
					KodeSiki:   edu.KodeSiki,
					Nama:       edu.Deskripsi,
					Kategori:   "Edukasi",
					NoUrut:     edu.NoUrut,
				}
				su.rmeRepository.SaveDataDaskepSIKI(newDataSiki)
			}

		}

		for _, kola := range req.SikiResponse[ia].Kolaborasi {
			if kola.IsSelected {
				newDataSiki := rme.DaskepSikiModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					NoDaskep:   no.Daskep,
					IdSiki:     kola.IdSiki,
					KodeSiki:   kola.KodeSiki,
					Nama:       kola.Deskripsi,
					NoUrut:     kola.NoUrut,
					Kategori:   "Kolaborasi",
				}
				su.rmeRepository.SaveDataDaskepSIKI(newDataSiki)
			}
		}

		for _, tera := range req.SikiResponse[ia].Terapetutik {
			if tera.IsSelected {
				newDataSiki := rme.DaskepSikiModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					NoDaskep:   no.Daskep,
					IdSiki:     tera.IdSiki,
					KodeSiki:   tera.KodeSiki,
					Nama:       tera.Deskripsi,
					Kategori:   "Terapetutik",
					NoUrut:     tera.NoUrut,
				}
				su.rmeRepository.SaveDataDaskepSIKI(newDataSiki)
			}
		}

		for _, obser := range req.SikiResponse[ia].Observasi {
			if obser.IsSelected {
				newDataSiki := rme.DaskepSikiModel{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					NoDaskep:   no.Daskep,
					IdSiki:     obser.IdSiki,
					KodeSiki:   obser.KodeSiki,
					Nama:       obser.Deskripsi,
					Kategori:   "Observasi",
					NoUrut:     obser.NoUrut,
				}
				su.rmeRepository.SaveDataDaskepSIKI(newDataSiki)
			}
		}

	}

	return "Data berhasil disimpan", nil
}

// GET DATA
func (su *rmeUseCase) GetDataAsuhanKeperawatanUseCase(kdBagian string, userID string) (message string, err error) {

	return "", nil
}

// LAKUKAN SIMPAN PADA DASKEP SLKI
func (su *rmeUseCase) SaveDataSKIKeperawatanUseCase(noDaskep string, kodeSLKI string, idKriteria int, hasil int) (message string, err error) {
	// LAKUKAN UPADTE
	_, err2 := su.rmeRepository.UpdateDaskepSLKIRepository(hasil, noDaskep, kodeSLKI, idKriteria)

	if err2 != nil {
		message := fmt.Sprintf("Error %s", err2.Error())
		return "Gagal menyimpan data", errors.New(message)
	}

	return "Data berhasil disimpan", nil
}

// TODO :
func (su *rmeUseCase) SaveDataAllSKIKeperawatanUseCase(req dto.ReqDasKepDiagnosaModel) (message string, err error) {
	// UPDATE HASIL
	for ia := 0; ia <= len(req.DaskepSLKI)-1; ia++ {
		su.logging.Info(req.DaskepSLKI[ia].Hasil)
		_, err2 := su.rmeRepository.UpdateDaskepSLKIRepository(req.DaskepSLKI[ia].Hasil, req.NoDaskep, req.DaskepSLKI[ia].KodeSlki, req.DaskepSLKI[ia].IdKriteria)

		if err2 != nil {
			message := fmt.Sprintf("Error %s", err2.Error())
			return "Gagal menyimpan data", errors.New(message)
		}
	}

	_, err3 := su.rmeRepository.UpdateDasekepDiagnosaRepository(req.NoDaskep, req.Noreg)

	if err3 != nil {
		message := fmt.Sprintf("Error %s", err3.Error())
		return message, errors.New(message)

	}

	return "Data berhasil disimpan", nil
}

// ON UPDATE DATA DASKEP TO CLOSED
func (su *rmeUseCase) OnUpdateDataDaskepToClosedUseCase(noDaskep string, noReg string, modulID string, status string) (res []rme.DasKepDiagnosaModel, err error) {

	// LAKUKAN UPATE TERLEBIH DAHULU
	_, err3 := su.rmeRepository.UpdateDasekepDiagnosaRepository(noDaskep, noReg)

	if err3 != nil {
		message := fmt.Sprintf("Error %s", err3.Error())
		return res, errors.New(message)
	}

	data, err5 := su.rmeRepository.GetDataAsuhanKeperawatanRepository(noReg, modulID, status)

	return data, err5
}

func (su *rmeUseCase) OnSaveDataAlergiUseCase(req dto.ReqOnSaveDataAlergiObat, kdBagian string) (res rme.DAlergi, message string, err error) {
	// SIMPAN ALERGI USECASE
	times := time.Now()

	var data = rme.DAlergi{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		Id:         req.NoRm,
		Kelompok:   req.Kelompok,
		InsertUser: req.InsertUser,
		Alergi:     req.Alergi,
		KdBagian:   kdBagian,
	}

	data2, err2 := su.rmeRepository.InsertDataAlergiRepository(data)

	if err2 != nil {
		message := fmt.Sprintf("Error %s", err2.Error())
		return res, "Gagal menyimpan data", errors.New(message)
	}

	return data2, "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnSavePengkajianIGDUseCase(noReg string, kdBagian string, person string, onSave dto.ReqSaveAsesmenIGD, userID string) (message string, err error) {
	// CEK APAKAH ADA DATA TERSIMPAN SEBELUMNYA
	su.logging.Info(onSave.AseskepSistemPenghasilan)
	su.logging.Info(onSave.AseskepSistemAlatBantu)
	su.logging.Info(onSave.AseskepSistemKebutuhanKhusus)

	pengkajian, er11 := su.rmeRepository.OnGetPengkajianAwalIGDRepository(noReg, kdBagian, person)

	if er11 != nil || pengkajian.Noreg == "" {
		_, ersave := su.rmeRepository.OnSavePengkajianPersistemIGDRepository(kdBagian, person, userID, onSave)

		if ersave != nil {
			message := fmt.Sprintf("Error %s", "new")
			return "Data gagal di simpan", errors.New(message)
		}

		return "Data gagal di simpan", errors.New(message)

	} else {
		// UPDATE DATA
		data := rme.PengkajianPersistemIGD{
			AseskepSistemMerokok:           onSave.AseskepSistemMerokok,
			AseskepSistemMinumAlkohol:      onSave.AseskepSistemMinumAlkohol,
			AseskepSistemSpikologis:        onSave.AseskepSistemSpikologis,
			AseskepSistemGangguanJiwa:      onSave.AseskepSistemGangguanJiwa,
			AseskepSistemBunuhDiri:         onSave.AseskepSistemBunuhDiri,
			AseskepSistemTraumaPsikis:      onSave.AseskepSistemTraumaPsikis,
			AseskepSistemHambatanSosial:    onSave.AseskepSistemHambatanSosial,
			AseskepSistemHambatanSpiritual: onSave.AseskepSistemHambatanSpiritual,
			AseskepSistemHambatanEkonomi:   onSave.AseskepSistemHambatanEkonomi,
			AseskepSistemKebutuhanKhusus:   onSave.AseskepSistemKebutuhanKhusus,
			AseskepSistemAlatBantu:         onSave.AseskepSistemAlatBantu,
			AseskepSistemPenghasilan:       onSave.AseskepSistemPenghasilan,
			AseskepSistemKultural:          onSave.AseskepSistemKultural,
		}

		_, update := su.rmeRepository.OnUpdatePengkajianPersistemIGDRepository(noReg, kdBagian, data)

		if update != nil {
			message := fmt.Sprintf("Error %s", update.Error())
			return "Data gagal disimpan", errors.New(message)
		}

		return "Data berhasil diupdate", nil
	}

}

func (su *rmeUseCase) OnSavePemeriksaanFisikIGDDokterFiberHandler(req dto.ReqPemeriksaanFisikIGDDokter, userID string,
	kdBagian string) (res dto.ResponseIGD, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertDevice: req.DeviceID,
		InsertUserId: userID,
		Kategori:     req.Pelayanan,
		KetPerson:    req.Person,
		Pelayanan:    req.Pelayanan,
		KdBagian:     kdBagian,
		AlatKelamin:  req.AlatKelamin,
		Noreg:        req.Noreg,
		Mata:         req.Mata,
		Tht:          req.Tht,
		AnggotaGerak: req.AnggotaGerak,
		Otot:         req.KekuatanOtot,
		Refleks:      req.Refleks,
		RtVt:         req.RtVt,
		Thyroid:      req.KelenjarGetahBening,
		Kulit:        req.Kulit,
		Ginjal:       req.Ginjal,
		Limpa:        req.Limpa,
		Hati:         req.Hati,
		Perut:        req.Perut,
		Paru:         req.Paru,
		Jantung:      req.Jantung,
		Dada:         req.Dada,
		Leher:        req.Leher,
		Mulut:        req.Mulut,
		Kepala:       req.Kepala,
		JalanNafas:   req.JalanNafas,
		Sirkulasi:    req.Sirkulasi,
		Hidung:       req.Hidung,
		Telinga:      req.Telinga,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(data)
			message := fmt.Sprintf("Error: %s\nData gagal disimpan", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(fisik)
		return mapper, "Data berhasil disimpan", nil
	} else {
		updates := rme.PemeriksaanFisikModel{
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			AlatKelamin:  req.AlatKelamin,
			Gigi:         req.Gigi,
			Abdomen:      req.Abdomen,
			Noreg:        req.Noreg,
			Mata:         req.Mata,
			Tht:          req.Tht,
			AnggotaGerak: req.AnggotaGerak,
			Otot:         req.KekuatanOtot,
			Refleks:      req.Refleks,
			RtVt:         req.RtVt,
			Thyroid:      req.KelenjarGetahBening,
			Kulit:        req.Kulit,
			Ginjal:       req.Ginjal,
			Limpa:        req.Limpa,
			Hati:         req.Hati,
			Perut:        req.Perut,
			Paru:         req.Paru,
			Jantung:      req.Jantung,
			Dada:         req.Dada,
			Leher:        req.Leher,
			Mulut:        req.Mulut,
			Kepala:       req.Kepala,
			JalanNafas:   req.JalanNafas,
			Sirkulasi:    req.Sirkulasi,
			Hidung:       req.Hidung,
			Telinga:      req.Telinga,
		}

		su.logging.Info("UPDATE DATA PEMERIKSAAN FISIK")
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(updates)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", err12.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(update)
		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikICUFiberUsecase(req dto.ReqOnSavePemeriksaanFisikICU, userID string,
	kdBagian string) (res dto.ResponseFisikICU, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertDevice: req.DeviceID,
		InsertUserId: userID,
		Kategori:     req.Pelayanan,
		KetPerson:    req.Person,
		Pelayanan:    req.Pelayanan,
		KdBagian:     kdBagian,
		Noreg:        req.Noreg,
		GcsE:         req.GcsE,
		GcsM:         req.GcsM,
		GcsV:         req.GcsV,
		Kesadaran:    req.Kesadaran,
		Kepala:       req.Kepala,
		Rambut:       req.Rambut,
		Wajah:        req.Wajah,
		Mata:         req.Mata,
		Telinga:      req.Telinga,
		Hidung:       req.Hidung,
		Mulut:        req.Mulut,
		Gigi:         req.Gigi,
		Lidah:        req.Lidah,
		Tenggorokan:  req.Tenggorokan,
		Leher:        req.Leher,
		Respirasi:    req.Respirasi,
		Integument:   req.Integumen,
		Dada:         req.Dada,
		Jantung:      req.Jantung,
		Ekstremitas:  req.Ekstremitas,
		Pupil:        req.Pupil,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToResponsePemeriksaanFisikICUMapper(data)
			message := fmt.Sprintf("Error: %s\nData gagal disimpan", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToResponsePemeriksaanFisikICUMapper(fisik)
		return mapper, "Data berhasil disimpan", nil
	} else {

		updates := rme.PemeriksaanFisikModel{
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			GcsE:         req.GcsE,
			GcsM:         req.GcsM,
			GcsV:         req.GcsV,
			Kesadaran:    req.Kesadaran,
			Kepala:       req.Kepala,
			Rambut:       req.Rambut,
			Wajah:        req.Wajah,
			Mata:         req.Mata,
			Telinga:      req.Telinga,
			Hidung:       req.Hidung,
			Mulut:        req.Mulut,
			Gigi:         req.Gigi,
			Lidah:        req.Lidah,
			Tenggorokan:  req.Tenggorokan,
			Leher:        req.Leher,
			Respirasi:    req.Respirasi,
			Integument:   req.Integumen,
			Dada:         req.Dada,
			Jantung:      req.Jantung,
			Ekstremitas:  req.Ekstremitas,
			Pupil:        req.Pupil,
		}

		su.logging.Info("UPDATE DATA PEMERIKSAAN FISIK")
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToResponsePemeriksaanFisikICUMapper(updates)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", err12.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToResponsePemeriksaanFisikICUMapper(update)
		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikIGDDokterMethodistFiberHandler(req dto.ReqPemeriksaanFisikIGDDokterMethodist, userID string,
	kdBagian string) (res dto.ResponseIGD, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		InsertDevice:        req.DeviceID,
		InsertUserId:        userID,
		Kategori:            req.Pelayanan,
		KetPerson:           req.Person,
		Pelayanan:           req.Pelayanan,
		KdBagian:            kdBagian,
		AlatKelamin:         req.AlatKelamin,
		Noreg:               req.Noreg,
		Mata:                req.Mata,
		Tht:                 req.Tht,
		Thyroid:             req.KelenjarGetahBening,
		Ginjal:              req.Ginjal,
		Limpa:               req.Limpa,
		Hati:                req.Hati,
		Perut:               req.Perut,
		Paru:                req.Paru,
		Jantung:             req.Jantung,
		Dada:                req.Dada,
		Leher:               req.Leher,
		Mulut:               req.Mulut,
		Kepala:              req.Kepala,
		EkstremitasSuperior: req.Superior,
		EkstremitasInferior: req.Inferior,
		Anus:                req.Anus,
		AbdomenLainnya:      req.AbdomenLainnya,
		Gigi:                req.Gigi,
	}

	if err1 == nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(data)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(fisik)
		return mapper, "Data berhasil disimpan", nil
	} else {

		updates := rme.PemeriksaanFisikModel{
			InsertDevice:        req.DeviceID,
			InsertUserId:        userID,
			Kategori:            req.Pelayanan,
			KetPerson:           req.Person,
			Pelayanan:           req.Pelayanan,
			KdBagian:            kdBagian,
			AlatKelamin:         req.AlatKelamin,
			Noreg:               req.Noreg,
			Mata:                req.Mata,
			Tht:                 req.Tht,
			Thyroid:             req.KelenjarGetahBening,
			Ginjal:              req.Ginjal,
			Limpa:               req.Limpa,
			Hati:                req.Hati,
			Perut:               req.Perut,
			Paru:                req.Paru,
			Jantung:             req.Jantung,
			Dada:                req.Dada,
			Leher:               req.Leher,
			Mulut:               req.Mulut,
			Kepala:              req.Kepala,
			EkstremitasSuperior: req.Superior,
			EkstremitasInferior: req.Inferior,
			Anus:                req.Anus,
			AbdomenLainnya:      req.AbdomenLainnya,
			Gigi:                req.Gigi,
		}

		su.logging.Info("UPDATE DATA PEMERIKSAAN FISIK")
		// OnUpdatePemeriksaanFisikIGDRepository
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(updates)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", err12.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(update)
		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikDokterAntonioUseCase(req dto.RegPemeriksaanFisikDokterAntonio, userID string, kdBagian string) (res dto.ResponsePemerikssanFisikAntonio, message string, err error) {

	pemFisik, er12 := su.rmeRepository.OnGetPemeriksaanFisikDokterRepository("Dokter", kdBagian, req.Noreg)

	if er12 != nil || pemFisik.Noreg == "" {
		// INSERT DATA
		times := time.Now()

		datas := rme.PemeriksaanFisikDokterAntonio{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Keterangan:   req.PemeriksaanFisik,
		}

		fisik, errs := su.rmeRepository.OnSavePemeriksaanFisikRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikAntonioResponse(fisik)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikAntonioResponse(fisik)
		return mapper, "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		update := rme.PemeriksaanFisikDokterAntonio{
			Keterangan: req.PemeriksaanFisik,
		}

		update, er12 := su.rmeRepository.OnUpdatePemeriksaanFisikAntonioDokterRepository(update, kdBagian, req.Person, req.Pelayanan, req.Noreg)

		if er12 != nil {
			su.logging.Info(er12)
			mapper := su.rmeMapper.ToPemeriksaanFisikAntonioResponse(update)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", er12.Error())
			return mapper, message, er12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikAntonioResponse(update)
		return mapper, "Data berhasil disimpan", nil
	}

}

func (su *rmeUseCase) OnSavePemeriksaanFisikIGDDokterAntonioFiberHandler(req dto.ReqPemeriksaanFisikIGDDokterMethodist, userID string,
	kdBagian string) (res dto.ResponseIGD, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		InsertDevice:        req.DeviceID,
		InsertUserId:        userID,
		Kategori:            req.Pelayanan,
		KetPerson:           req.Person,
		Pelayanan:           req.Pelayanan,
		KdBagian:            kdBagian,
		AlatKelamin:         req.AlatKelamin,
		Noreg:               req.Noreg,
		Mata:                req.Mata,
		Tht:                 req.Tht,
		Thyroid:             req.KelenjarGetahBening,
		Ginjal:              req.Ginjal,
		Limpa:               req.Limpa,
		Hati:                req.Hati,
		Perut:               req.Perut,
		Paru:                req.Paru,
		Jantung:             req.Jantung,
		Dada:                req.Dada,
		Leher:               req.Leher,
		Mulut:               req.Mulut,
		Kepala:              req.Kepala,
		EkstremitasSuperior: req.Superior,
		EkstremitasInferior: req.Inferior,
		Anus:                req.Anus,
		AbdomenLainnya:      req.AbdomenLainnya,
		Gigi:                req.Gigi,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(data)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(fisik)
		return mapper, "Data berhasil disimpan", nil
	} else {

		updates := rme.PemeriksaanFisikModel{
			InsertDevice:        req.DeviceID,
			InsertUserId:        userID,
			Kategori:            req.Pelayanan,
			KetPerson:           req.Person,
			Pelayanan:           req.Pelayanan,
			KdBagian:            kdBagian,
			AlatKelamin:         req.AlatKelamin,
			Noreg:               req.Noreg,
			Mata:                req.Mata,
			Tht:                 req.Tht,
			Thyroid:             req.KelenjarGetahBening,
			Ginjal:              req.Ginjal,
			Limpa:               req.Limpa,
			Hati:                req.Hati,
			Perut:               req.Perut,
			Paru:                req.Paru,
			Jantung:             req.Jantung,
			Dada:                req.Dada,
			Leher:               req.Leher,
			Mulut:               req.Mulut,
			Kepala:              req.Kepala,
			EkstremitasSuperior: req.Superior,
			EkstremitasInferior: req.Inferior,
			Anus:                req.Anus,
			AbdomenLainnya:      req.AbdomenLainnya,
			Gigi:                req.Gigi,
		}

		su.logging.Info("UPDATE DATA PEMERIKSAAN FISIK")
		// OnUpdatePemeriksaanFisikIGDRepository
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(updates)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", err12.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(update)
		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikBangsalUseCase(req dto.ReqPemeriksaanFisik, userID string, kdBagian string) (res dto.ResponseIGD, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:   times.Format("2006-01-02 15:04:05"),
		InsertDevice: req.DeviceID,
		InsertUserId: userID,
		Kategori:     req.Pelayanan,
		Gigi:         req.Gigi,
		Abdomen:      req.Abdomen,
		KetPerson:    req.Person,
		Pelayanan:    req.Pelayanan,
		KdBagian:     kdBagian,
		AlatKelamin:  req.AlatKelamin,
		Noreg:        req.Noreg,
		Mata:         req.Mata,
		Tht:          req.Tht,
		AnggotaGerak: req.AnggotaGerak,
		Otot:         req.KekuatanOtot,
		Refleks:      req.Refleks,
		RtVt:         req.RtVt,
		Thyroid:      req.KelenjarGetahBening,
		Kulit:        req.Kulit,
		Ginjal:       req.Ginjal,
		Limpa:        req.Limpa,
		Hati:         req.Hati,
		Perut:        req.Perut,
		Paru:         req.Paru,
		Jantung:      req.Jantung,
		Dada:         req.Dada,
		Leher:        req.Leher,
		Mulut:        req.Mulut,
		Kepala:       req.Kepala,
		JalanNafas:   req.JalanNafas,
		Hidung:       req.Hidung,
		Telinga:      req.Telinga,
		Sirkulasi:    req.Sirkulasi,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(data)
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", errs.Error())
			return mapper, message, errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(fisik)
		return mapper, "Data berhasil disimpan", err1
	} else {

		updates := rme.PemeriksaanFisikModel{
			InsertDevice: req.DeviceID,
			InsertUserId: userID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     kdBagian,
			AlatKelamin:  req.AlatKelamin,
			Noreg:        req.Noreg,
			Mata:         req.Mata,
			Tht:          req.Tht,
			AnggotaGerak: req.AnggotaGerak,
			Otot:         req.KekuatanOtot,
			Refleks:      req.Refleks,
			RtVt:         req.RtVt,
			Thyroid:      req.KelenjarGetahBening,
			Kulit:        req.Kulit,
			Ginjal:       req.Ginjal,
			Limpa:        req.Limpa,
			Hati:         req.Hati,
			Perut:        req.Perut,
			Paru:         req.Paru,
			Jantung:      req.Jantung,
			Dada:         req.Dada,
			Leher:        req.Leher,
			Mulut:        req.Mulut,
			Kepala:       req.Kepala,
			Gigi:         req.Gigi,
			Abdomen:      req.Abdomen,
			JalanNafas:   req.JalanNafas,
			Hidung:       req.Hidung,
			Telinga:      req.Telinga,
			Sirkulasi:    req.Sirkulasi,
		}

		su.logging.Info("UPDATE DATA PEMERIKSAAN FISIK")
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(updates)
			return mapper, "Data gagal diupdate", err12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikIGDResponse(update)
		return mapper, "Data berhasil diupdate", err1
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikAnakUseCase(req dto.ReqOnSavePemeriksaanFisikAnak, kdBagian string, userID string) (res dto.ResponsePemeriksaanFisikAnak, message string, err error) {
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()

	datas := rme.PemeriksaanFisikModel{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		InsertDevice:      req.DeviceID,
		InsertUserId:      userID,
		Kategori:          "anak",
		Abdomen:           req.Abdomen,
		KetPerson:         req.Person,
		Pelayanan:         req.Pelayanan,
		KdBagian:          kdBagian,
		Noreg:             req.Noreg,
		Mata:              req.Mata,
		Dada:              req.Dada,
		Mulut:             req.Mulut,
		Hidung:            req.Hidung,
		Telinga:           req.Telinga,
		Leher:             req.LeherDanBahu,
		Punggung:          req.Punggung,
		PeristatikUsus:    req.Peristaltik,
		NutrisiDanHidrasi: req.NutrisiDanHidrasi,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			message := fmt.Sprintf("Error: %s\nData gagal diupdate", errs.Error())
			return res, message, errs
		}

		su.logging.Info(errs)
		mapper := su.rmeMapper.ToResponsePemeriksaanFisikAnakMapper(fisik)
		return mapper, "Data berhasil disimpan", err1

	} else {
		updates := rme.PemeriksaanFisikModel{
			Mata:              req.Mata,
			Telinga:           req.Telinga,
			Hidung:            req.Hidung,
			Mulut:             req.Mulut,
			Leher:             req.LeherDanBahu,
			Dada:              req.Dada,
			Abdomen:           req.Abdomen,
			PeristatikUsus:    req.Peristaltik,
			Punggung:          req.Punggung,
			NutrisiDanHidrasi: req.NutrisiDanHidrasi,
		}

		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)

		if err12 != nil {
			mapper := su.rmeMapper.ToResponsePemeriksaanFisikAnakMapper(updates)
			return mapper, "Data gagal diupdate", err12
		}

		mapper := su.rmeMapper.ToResponsePemeriksaanFisikAnakMapper(update)
		return mapper, "Data berhasil diupdate", err1
	}
}

func (su *rmeUseCase) OnGetAsesmenKeperawatanBayiUseCase(noReg string, noRM string, kdBagian string) (response dto.ResponseAsesmenBayiResponse, err error) {
	// GET ASESMEN KEPERAWATAN BAYI

	// profil, _ := rh.ReporRepository.GetDataPasienRepository(noRM)
	profil, _ := su.reportRepository.GetDataPasienRepository(noRM)
	asesmen, _ := su.rmeRepository.OnGetAsesmenKeperawatanBayiRepository(noReg, kdBagian)
	// GET IDENTITAS BAYI

	dokter, _ := su.rmeRepository.OnGetDokterAnakDanObgynRepository()
	riwayat, _ := su.rmeRepository.OnGetRiwayatKelahiranYangLaluRepository(noRM)

	// MAPPING DATA
	mapper := su.rmeMapper.ToMappingKeperawatanBayiResponse(dokter, riwayat, asesmen, profil)
	return mapper, nil
}

func (su *rmeUseCase) OnGetReportAsesmenKeperawatanPerinaUseCase(noReg string, noRm string) (response dto.ResponseReportAsesmenBayiResponse, err error) {
	asesmen, _ := su.rmeRepository.OnGetAsesmenKeperawatanBayiRepository(noReg, "PERI")

	// GET RIWAYAT KEHAMILAN
	su.logging.Info("INSERT USER ID")
	su.logging.Info(asesmen.InsertUserId)

	riwayat, _ := su.rmeRepository.OnGetRiwayatKelahiranYangLaluRepository(noRm)
	mapperRiwayat := su.rmeMapper.ToMapperResponseRiwayatKelahiranLalu(riwayat)
	apgar, _ := su.rmeRepository.OnGetApgarScoreNeoNatusRepository(noReg)
	downScore, _ := su.rmeRepository.OnGetDownScroeNeoNatusRepository(noReg)
	vital, _ := su.rmeRepository.GetDVitalSignPerinaRepository(noReg)
	mapperVital := su.rmeMapper.ToMapperResponseDVitalSignPerina(vital)
	pemFisik, _ := su.rmeRepository.OnGetPemeriksaanFisikBangsalPerinaRepository(noReg)
	mapperFisik := su.rmeMapper.ToPemeriksaanFisikPerinaResponse(pemFisik)

	mapperApgar := su.rmeMapper.ToMapperScoreNeoNatus(apgar)
	mapperNeo := su.rmeMapper.ToMapperResponseNeonatus(downScore)
	// GET USER
	user, _ := su.userRepository.GetDataPengajarRepository(asesmen.InsertUserId)

	mapper := su.rmeMapper.ToResponseReportAsesmenBayiMapper(asesmen, mapperRiwayat, mapperApgar, mapperNeo, mapperVital, mapperFisik, user)
	return mapper, nil
}

func (su *rmeUseCase) OnSaveAsesmenKeperawatanBayiUseCase(noReg string, noRM string, kdBagian string, userID string, req dto.ReqOnSaveAsesemenKeperawatanBayi) (res dto.ResponseAsesmenBayiResponse, message string, err error) {
	// CEK DATA TERLEBIH DAHULU
	soap, err12 := su.rmeRepository.SearchDcpptSoapPasienRepository(noReg, kdBagian)

	if err12 != nil || soap.Noreg == "" {
		// LAKUKAN INSERT DATA ASESEMEN KEPERAWATAN BAYI
		su.logging.Info("CARI DATA PADA REGISTER")
		su.logging.Info(noReg)
		tanggal, errs11 := su.soapRepository.GetJamMasukPasienRepository(noReg)

		if errs11 != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		times := time.Now()

		var bayi = rme.AsesmenKeperawatanBayi{
			InsertDttm:                              times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:                        req.Person,
			InsertUserId:                            userID,
			InsertPc:                                req.DeviceID,
			TglMasuk:                                tanggal.Tanggal[0:10],
			KdBagian:                                kdBagian,
			Pelayanan:                               req.Pelayanan,
			Noreg:                                   req.NoReg,
			AseskepBayiRwtPenyakitAyah:              req.RiwayatPenyakitAyah,
			AseskepBayiNamaAyah:                     req.NamaAyah,
			AseskepBayiNatalVolume:                  req.NatalVolumen,
			AseskepBayiNatalKomplikasi:              req.NatalKomplikasi,
			KdDpjp:                                  req.Dpjp,
			AseskepBayiPekerjaanIbu:                 req.PekerjaanIbu,
			AseskepBayiPrenatalPendarahan:           req.PrenatalPendarahan,
			AseskepBayiPerkawinanAyahKe:             req.AseskepBayiPerkawinanAyahKe,
			AseskepBayiDokterObgyn:                  req.AseskepBayiDokterObgyn,
			AseskepBayiDokterAnak:                   req.AseskepBayiDokterAnak,
			AseskepBayiPekerjaanAyah:                req.AseskepBayiPekerjaanAyah,
			AseskepBayiRwtPrenatalObatObatan:        req.AseskepBayiPrenatalObatObatan,
			AseskepUsiaKehamilan:                    req.AseskepUsiaKehamilan,
			AseskepBayiNamaIbu:                      req.AseskepBayiNamaIbu,
			AseskepBayiPerkawinanIbuKe:              req.AseskepBayiPerkawinanIbuKe,
			AseskepBayiRwtPenyakitIbu:               req.AseskepBayiRwtPenyakitIbu,
			AseskepBayiNamaPjawab:                   req.AseskepBayiNamaPjawab,
			AseskepBayiUsiaPjawab:                   req.AseskepBayiUsiaPjawab,
			AseskepBayiPekerjaanPjawab:              req.AseskepBayiPekerjaanPjawab,
			AseskepBayiUsiaPersalinan:               req.AseskepBayiPrenatalUsiaPersalinan,
			AseskepBayiTglLahir:                     req.AseskepBayiTglLahir,
			AseskepBayiMenangis:                     req.AseskepBayiMenangis,
			AseskepBayiJk:                           req.AseskepBayiJk,
			AseskepBayiKeterangan:                   req.AseskepBayiKeterangan,
			AseskepBayiPrenatalUsiaKehamilan:        req.AseskepBayiPrenatalUsiaKehamilan,
			AseskepBayiPrenatalKomplikasi:           req.AseskepBayiPrenatalKomplikasi,
			AseskepBayiPrenatalHis:                  req.AseskepBayiPrenatalHis,
			AseskepBayiPrenatalTtp:                  req.AseskepBayiPrenatalTtp,
			AseskepBayiPrenatalKetuban:              req.AseskepBayiPrenatalKetuban,
			AseskepBayiPrenatalJam:                  req.AseskepBayiPrenatalJam,
			AseskepBayiRwtUsiaPersalinan:            req.AseskepBayiUsiaPersalinan,
			AseskepBayiRwtLahirDengan:               req.AseskepBayiRwtLahirDengan,
			AseskepBayiRwtJenisKelamin:              req.AseskepBayiRwtJenisKelamin,
			AseskepBayiRwtTglKelahiranBayi:          req.AseskepBayiRwtTglKelahiranBayi,
			AseskepBayiRwtMenangis:                  req.AseskepBayiMenangis,
			AseskepBayiRwtKeterangan:                req.AseskepBayiKeterangan,
			AseskepBayiPrenatalUsiaPersalinan:       req.AseskepBayiPrenatalUsiaPersalinan,
			AseskepBayiNatalPersalinan:              req.AseskepBayiNatalPersalinan,
			AseskepBayiNatalKpd:                     req.AseskepBayiNatalKpd,
			AseskepBayiNatalKeadaan:                 req.AseskepBayiNatalKeadaan,
			AseskepBayiNatalTindakanDiberikan:       req.AseskepBayiNatalTindakanDiberikan,
			AseskepBayiNatalPost:                    req.AseskepBayiNatalPost,
			AseskepBayiNatalPresentasi:              req.AseskepBayiNatalPresentasi,
			AseskepBayiNatalDitolongOleh:            req.AseskepBayiNatalDitolongOleh,
			AseskepBayiNatalKetuban:                 req.AseskepBayiNatalKetuban,
			AseskepBayiNatalLetak:                   req.AseskepBayiNatalLetak,
			AseskepBayiNatalLahirUmur:               req.AseskepBayiNatalLahirUmur,
			AseskepBayiPrenatalJumlahHari:           req.AseskepBayiPrenatalJumlahHari,
			AseskepBayiPrenatalKebiasaanIbu:         req.AseskepBayiPrenatalKebiasaanIbu,
			AseskepBayiLahirDgn:                     req.AseskepBayiRwtLahirDengan,
			AseskepBayiPrenatalPersalinanSebelumnya: req.PrenatalPersalinan,
		}

		su.logging.Info("INSERT DATA")
		_, ersave := su.rmeRepository.OnSaveDataAsesmenBayiRepository(bayi)

		if ersave != nil {
			message := fmt.Sprintf("%s,", ersave)
			return res, message, errors.New(message)
		}

		return res, "Data berhasil disimpan", nil

	} else {

		var bayi = rme.AsesmenKeperawatanBayi{

			AseskepBayiDokterObgyn:                  req.AseskepBayiDokterObgyn,
			AseskepBayiDokterAnak:                   req.AseskepBayiDokterAnak,
			AseskepBayiPekerjaanAyah:                req.AseskepBayiPekerjaanAyah,
			AseskepBayiPerkawinanAyahKe:             req.AseskepBayiPerkawinanAyahKe,
			AseskepUsiaKehamilan:                    req.AseskepUsiaKehamilan,
			AseskepBayiRwtPenyakitAyah:              req.RiwayatPenyakitAyah,
			AseskepBayiNamaIbu:                      req.AseskepBayiNamaIbu,
			AseskepBayiPerkawinanIbuKe:              req.AseskepBayiPerkawinanIbuKe,
			AseskepBayiRwtPenyakitIbu:               req.AseskepBayiRwtPenyakitIbu,
			AseskepBayiPrenatalPendarahan:           req.PrenatalPendarahan,
			AseskepBayiPekerjaanIbu:                 req.PekerjaanIbu,
			AseskepBayiNamaPjawab:                   req.AseskepBayiNamaPjawab,
			AseskepBayiUsiaPjawab:                   req.AseskepBayiUsiaPjawab,
			AseskepBayiPekerjaanPjawab:              req.AseskepBayiPekerjaanPjawab,
			AseskepBayiUsiaPersalinan:               req.AseskepBayiPrenatalUsiaPersalinan,
			AseskepBayiTglLahir:                     req.AseskepBayiTglLahir,
			AseskepBayiNamaAyah:                     req.NamaAyah,
			AseskepBayiPrenatalJumlahHari:           req.AseskepBayiPrenatalJumlahHari,
			AseskepBayiPrenatalKebiasaanIbu:         req.AseskepBayiPrenatalKebiasaanIbu,
			AseskepBayiRwtPrenatalObatObatan:        req.AseskepBayiPrenatalObatObatan,
			AseskepBayiLahirDgn:                     req.AseskepBayiRwtLahirDengan,
			AseskepBayiMenangis:                     req.AseskepBayiMenangis,
			AseskepBayiJk:                           req.AseskepBayiJk,
			AseskepBayiKeterangan:                   req.AseskepBayiKeterangan,
			AseskepBayiPrenatalUsiaKehamilan:        req.AseskepBayiPrenatalUsiaKehamilan,
			AseskepBayiPrenatalKomplikasi:           req.AseskepBayiPrenatalKomplikasi,
			AseskepBayiPrenatalHis:                  req.AseskepBayiPrenatalHis,
			AseskepBayiPrenatalPersalinanSebelumnya: req.PrenatalPersalinan,
			AseskepBayiPrenatalTtp:                  req.AseskepBayiPrenatalTtp,
			AseskepBayiPrenatalKetuban:              req.AseskepBayiPrenatalKetuban,
			AseskepBayiPrenatalJam:                  req.AseskepBayiPrenatalJam,
			AseskepBayiRwtUsiaPersalinan:            req.AseskepBayiNatalPersalinan,
			AseskepBayiRwtLahirDengan:               req.AseskepBayiRwtLahirDengan,
			AseskepBayiRwtJenisKelamin:              req.AseskepBayiRwtJenisKelamin,
			AseskepBayiRwtTglKelahiranBayi:          req.AseskepBayiRwtTglKelahiranBayi,
			AseskepBayiRwtMenangis:                  req.AseskepBayiMenangis,
			AseskepBayiRwtKeterangan:                req.AseskepBayiKeterangan,
			AseskepBayiPrenatalUsiaPersalinan:       req.AseskepBayiPrenatalUsiaPersalinan,
			AseskepBayiNatalPersalinan:              req.AseskepBayiNatalPersalinan,
			AseskepBayiNatalKpd:                     req.AseskepBayiNatalKpd,
			AseskepBayiNatalKeadaan:                 req.AseskepBayiNatalKeadaan,
			AseskepBayiNatalTindakanDiberikan:       req.AseskepBayiNatalTindakanDiberikan,
			AseskepBayiNatalPost:                    req.AseskepBayiNatalPost,
			AseskepBayiNatalPresentasi:              req.AseskepBayiNatalPresentasi,
			AseskepBayiNatalDitolongOleh:            req.AseskepBayiNatalDitolongOleh,
			AseskepBayiNatalKetuban:                 req.AseskepBayiNatalKetuban,
			AseskepBayiNatalLetak:                   req.AseskepBayiNatalLetak,
			AseskepBayiNatalLahirUmur:               req.AseskepBayiNatalLahirUmur,
			AseskepBayiNatalVolume:                  req.NatalVolumen,
			AseskepBayiNatalKomplikasi:              req.NatalKomplikasi,
		}

		_, ersave := su.rmeRepository.OnUpdateDataAsesmenBayiRepository(bayi, req.NoReg, kdBagian)

		if ersave != nil {
			message := fmt.Sprintf("%s,", ersave)
			return res, message, errors.New(message)
		}

		return res, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSavePemeriksaanFisikPerinaUseCase(req dto.ReqPemeriksaanFisikPerina, userID string, kdBagian string) (res dto.ResponseFisikPerina, mesage string, err error) {
	// GET PEMERIKSAAN FISIK
	data, err1 := su.rmeRepository.GetPemeriksaanFisikIGDRepository(userID, req.Person, kdBagian, req.Noreg)

	times := time.Now()
	datas := rme.PemeriksaanFisikModel{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		Kesadaran:         req.Kesadaran,
		InsertDevice:      req.DeviceID,
		TonickNeck:        req.TonickNecK,
		InsertUserId:      userID,
		Kategori:          req.Pelayanan,
		KetPerson:         req.Person,
		Kelainan:          req.Kelainan,
		KdBagian:          kdBagian,
		Noreg:             req.Noreg,
		GcsE:              req.GcsE,
		GcsV:              req.GcsV,
		GcsM:              req.GcsM,
		Pelayanan:         req.Pelayanan,
		Kepala:            req.Kepala,
		Wajah:             req.Wajah,
		Telinga:           req.Telinga,
		Hidung:            req.Hidung,
		Mulut:             req.Mulut,
		Refleks:           req.Refleks,
		Leher:             req.LeherDanBahu,
		Ekstremitas:       req.Ekstremitas,
		Dada:              req.Dada,
		Punggung:          req.Punggung,
		Abdomen:           req.Abdomen,
		Integument:        req.Integumen,
		Anus:              req.Anus,
		Genetalia:         req.Genetalia,
		Babinski:          req.Babinski,
		RefleksRooting:    req.Rooting,
		RefleksSucking:    req.Sucking,
		RefleksSwallowing: req.Swallowing,
		RefleksMoro:       req.Moro,
		RefleksGraps:      req.Graps,
	}

	if err1 != nil || data.Noreg == "" {
		su.logging.Info("INSERT DATA PEMERIKSAAN FISIK")
		// SIMPAN DATA
		fisik, errs := su.rmeRepository.SavePemeriksaanFisikIGDRepository(datas)

		if errs != nil {
			su.logging.Info(errs)
			mapper := su.rmeMapper.ToPemeriksaanFisikPerinaResponse(data)
			return mapper, "Data gagal disimpan", errs
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikPerinaResponse(fisik)
		return mapper, "Data berhasil disimpan", err1
	} else {
		updates := rme.PemeriksaanFisikModel{
			InsertDevice:      req.DeviceID,
			InsertUserId:      userID,
			Kategori:          req.Pelayanan,
			KetPerson:         req.Person,
			Pelayanan:         req.Pelayanan,
			KdBagian:          kdBagian,
			Kesadaran:         req.Kesadaran,
			Kepala:            req.Kepala,
			GcsE:              req.GcsE,
			GcsV:              req.GcsV,
			GcsM:              req.GcsM,
			Wajah:             req.Wajah,
			TonickNeck:        req.TonickNecK,
			Kelainan:          req.Kelainan,
			Telinga:           req.Telinga,
			Hidung:            req.Hidung,
			Mulut:             req.Mulut,
			Refleks:           req.Refleks,
			Leher:             req.LeherDanBahu,
			Dada:              req.Dada,
			Ekstremitas:       req.Ekstremitas,
			Punggung:          req.Punggung,
			Abdomen:           req.Abdomen,
			Integument:        req.Integumen,
			Anus:              req.Anus,
			Genetalia:         req.Genetalia,
			Babinski:          req.Babinski,
			RefleksRooting:    req.Rooting,
			RefleksSucking:    req.Sucking,
			RefleksSwallowing: req.Swallowing,
			RefleksMoro:       req.Moro,
			RefleksGraps:      req.Graps,
		}

		// OnUpdatePemeriksaanFisikIGDRepository
		update, err12 := su.rmeRepository.OnUpdatePemeriksaanFisikIGDRepository(updates, req.Pelayanan, req.Person, kdBagian, req.Noreg)
		// UPDATE DATA

		if err12 != nil {
			su.logging.Info(err12)
			mapper := su.rmeMapper.ToPemeriksaanFisikPerinaResponse(updates)
			return mapper, "Data gagal diupdate", err12
		}

		mapper := su.rmeMapper.ToPemeriksaanFisikPerinaResponse(update)
		return mapper, "Data berhasil diupdate", err1
	}
}

func (su *rmeUseCase) OnGetReportAnalisaDataUseCase(noReg string) (res []rme.DAnalisaDataModel, err error) {

	data, err12 := su.rmeRepository.GetAnalisaDataRepository(noReg)

	if err12 != nil {
		return res, nil
	}

	return data, nil
}

func (su *rmeUseCase) OnValidasiAnalisaDataUseCase(req dto.ReqValidasiAnalisaData, userID string) (message string, ers error) {

	var data = rme.DAnalisaDataModel{
		JamTeratasi:  req.Jam,
		KodeAnalisa:  req.KodeAnalisa,
		TglTeratasi:  req.Tanggal,
		UserTeratasi: userID,
		Hasil:        "Teratasi",
	}
	_, er11 := su.rmeRepository.OnUpdateAnalisaDataRepository(req.KodeAnalisa, data)

	if er11 != nil {
		return "Data gagal diupdate", nil
	}
	return "Problem teratasi", nil
}

func (su *rmeUseCase) OnValidasiAnalisaDataEnumUseCase(req dto.ReqValidasiAnalisaData, userID string) (message string, err error) {
	var data = rme.DAnalisaDataModel{
		JamTeratasi:  req.Jam,
		KodeAnalisa:  req.KodeAnalisa,
		TglTeratasi:  req.Tanggal,
		UserTeratasi: userID,
		Hasil:        req.JenisAnalisa,
	}
	_, er11 := su.rmeRepository.OnUpdateAnalisaDataRepository(req.KodeAnalisa, data)

	if er11 != nil {
		return "Data gagal diupdate", nil
	}

	return req.JenisAnalisa, nil
}
