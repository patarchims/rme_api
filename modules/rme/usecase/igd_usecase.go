package rme

import (
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"
)

func (su *rmeUseCase) OnSaveKeluhanUtamaIGDUsecase(reg dto.ReqKeluhanUtamaIGD, userID string, bagian string) (res dto.ResposeKeluhanUtamaIGD, mesage string, err error) {

	// CEK APAKAH DATA SUDAH TERSIMPAN ATAU BELUM
	keluhanUtama, err12 := su.rmeRepository.GetAsesemenKeluhanUtamaIGDDokter(bagian, reg.NoReg, reg.Person)
	times := time.Now()
	if err12 != nil || keluhanUtama.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(reg.NoReg)

		data := rme.AsesemenDokterIGD{
			InsertDttm:          times.Format("2006-01-02 15:04:05"),
			TglMasuk:            tanggal.Tanggal[0:10],
			AsesmedDokterTtd:    userID,
			AsesmedDokterTtdTgl: times.Format("2006-01-02"),
			AsesmedDokterTtdJam: times.Format("15:04:05"),
			JamMasuk:            tanggal.Jam,
			KeteranganPerson:    reg.Person,
			KdDpjp:              userID,
			InsertUserId:        userID,
			Pelayanan:           reg.Pelayanan,
			KdBagian:            bagian,
			InsertPc:            reg.DeviceID,
			Noreg:               reg.NoReg,
			AsesmedKeluhUtama:   reg.KeluhanUtama,
			AsesmedRwytDahulu:   reg.RiwayatDahulu,
			AsesmedRwytSkrg:     reg.RiwayatSekarang,
		}

		riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(reg.NoRM, reg.Tanggal)
		riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(reg.NoRM)

		save, err12 := su.rmeRepository.OnSaveKeluhanUtamaIGDRepository(data)

		if err12 != nil {
			mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, data)
			message := fmt.Sprintf("Error: %s\nData gagal disimpan", err12.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, save)
		return mapper, "Data berhasil disimpan", err12

	} else {
		// UPDATE DATA
		data := rme.AsesemenDokterIGD{
			KeteranganPerson:  reg.Person,
			Pelayanan:         reg.Pelayanan,
			KdBagian:          bagian,
			Noreg:             reg.NoReg,
			AsesmedKeluhUtama: reg.KeluhanUtama,
			AsesmedRwytDahulu: reg.RiwayatDahulu,
			AsesmedRwytSkrg:   reg.RiwayatSekarang,
		}

		udpate, errs1 := su.rmeRepository.OnUpdateKeluhanUtamaIGDRepository(bagian, reg)

		riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(reg.NoRM, reg.Tanggal)
		riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(reg.NoRM)

		if errs1 != nil {
			mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, data)
			message := fmt.Sprintf("Error: %s\nData gagal disimpan", errs1.Error())
			return mapper, message, err12
		}

		mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, udpate)
		return mapper, "Data berhasil diubah", nil
	}
}

func (su *rmeUseCase) OnSaveRiwayatPenyakitKeluargaIGDUsecase(reg dto.ReqRiwayatKeluargaIGD, userID string, bagian string) (res dto.ResposeKeluhanUtamaIGD, mesage string, err error) {

	_, err1 := su.rmeRepository.OnSaveRiwayatKeluargaRepository(reg.NoRM, userID, bagian, reg.NamaRiwayatPenyakit)

	if err1 != nil {
		keluhanUtama, _ := su.rmeRepository.GetAsesemenKeluhanUtamaIGDDokter(bagian, reg.NoReg, reg.Person)
		riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(reg.NoRM, reg.Tanggal)
		riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(reg.NoRM)
		mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, keluhanUtama)
		message := fmt.Sprintf("Error: %s\nData gagal disimpan", err1.Error())
		return mapper, message, err1
	}

	// CEK APAKAH DATA SUDAH TERSIMPAN ATAU BELUM
	keluhanUtama, _ := su.rmeRepository.GetAsesemenKeluhanUtamaIGDDokter(bagian, reg.NoReg, reg.Person)
	riwayatDahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(reg.NoRM, reg.Tanggal)
	riwayatKeluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(reg.NoRM)
	mapper := su.rmeMapper.ToMappingKeluhanUtamaV2(riwayatDahulu, riwayatKeluarga, keluhanUtama)
	return mapper, "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnGetReportAsesmenAwalMedisKelurgaUseCase(reg dto.ReqReportAsesmenAwalMedis) (res dto.ReportAsesmenIGD, err error) {
	pasien, _ := su.userRepository.OnGetProfilePasienRepository(reg.NoRM)
	mapperPasien := su.userMapper.ToMappingProfilePasien(pasien)
	data1, _ := su.rmeRepository.GetAsesmenAwalMedisIGDReportDokter(reg.NoReg)
	data2, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(reg.NoRM, reg.Tanggal)
	data3, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(reg.NoRM)
	data4, _ := su.soapRepository.GetRencanaTindakLanjutReportRepository(reg.NoReg)
	data5, _ := su.soapRepository.GetDiagnosaRepositoryReportIGD(reg.NoReg)

	vital, _ := su.soapRepository.GetTandaVitalIGDReportdDokterRepository(reg.NoReg)
	data6, _ := su.rmeRepository.GetPemeriksaanFisikReportIGDRepository(reg.NoReg)
	mapperFisik := su.rmeMapper.ToPemeriksaanFisikIGDResponse(data6)

	labor, _ := su.hisUseCase.HistoryLaboratoriumUsecaseV2(reg.NoReg)
	radiologi, _ := su.hisUseCase.HistoryRadiologiUsecaseV2(reg.NoReg)
	diagBanding, _ := su.diagnosaRepository.OnGetDianosaBandingRepository(reg.NoReg, reg.KdBagian, reg.Pelayanan)

	mapper := su.rmeMapper.ToMapperReportIGDResponse(data1, data2, data3, data4, data5, mapperFisik, vital, labor, radiologi, mapperPasien, diagBanding)

	return mapper, nil
}

func (su *rmeUseCase) OnGetReportAsesmenDokterIGDAntonioUsecase(req dto.ReqReportAsesmenAwalMedis, bagian string) (res dto.ReportAsesmenIGD, err error) {
	data1, _ := su.rmeRepository.GetAsesmenAwalMedisIGDReportDokter(req.NoReg)

	su.logging.Info(data1)
	return res, nil
}

func (su *rmeUseCase) OnGetReportAsesmenAwalIGDDokterUseCase(reg dto.ReqReportAsesmenAwalMedis, bagian string) (res dto.ReportAsesmenIGD, err error) {
	data1, _ := su.rmeRepository.GetAsesmenAwalMedisIGDReportDokter(reg.NoReg)

	su.logging.Info(data1)
	return res, nil
}

func (su *rmeUseCase) OnSaveCPPTSBARUsecase(req dto.ReqSaveCPPTSBar, userID string, kdBagian string) (message string, err error) {
	times := time.Now()

	var datas = rme.DataCPPTSBAR{
		Noreg:         req.NoReg,
		Situation:     req.Situation,
		Background:    req.Background,
		KdBagian:      kdBagian,
		Asesmen:       req.Asesmen,
		Kelompok:      req.Kelompok,
		Recomendation: req.Recomendation,
		InsertDttm:    times.Format("2006-01-02 15:04:05"),
		Tanggal:       times.Format("2006-01-02"),
		InsertUserId:  userID,
		InstruksiPpa:  req.Ppa,
		InsertPc:      req.DeviceID,
		Pelayanan:     req.Pelayanan,
		Dpjp:          req.Dpjp,
	}

	_, e123 := su.rmeRepository.OnSaveDCPPTSBARRepository(datas)

	if e123 != nil {
		message := fmt.Sprintf("Error: %s\nData gagal disimpan", e123.Error())
		return message, e123
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnUpdateCPTTSBARUsecase(req dto.ReqUpdateeCPPTSBar, userID string, kdBagisn string) (message string, err error) {
	var datas = rme.DataCPPTSBAR{
		Situation:     req.Situation,
		Background:    req.Background,
		Asesmen:       req.Asesmen,
		Recomendation: req.Recomendation,
		InsertUserId:  userID,
	}
	_, err12 := su.rmeRepository.OnUpdateCPPTSBARRepository(req.IdCppt, datas)

	if err12 != nil {
		message := fmt.Sprintf("Error: %s\nData gagal disimpan", err12.Error())
		return message, err12
	}

	return "Data berhasil diubah", nil
}
