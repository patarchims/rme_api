package rme

import (
	igdDto "hms_api/modules/igd/dto"
	"hms_api/modules/rme/dto"
)

func (su *rmeUseCase) OnGetReportPengkajianDewasaBangsalUseCase(req dto.ResPengkajianKeperawatan) (res dto.ResponsePengkajianKeperawatan, err error) {
	pengkajian, _ := su.igdRepository.GetPengkajianKeperawatanRepository(req.KdBagian, req.Pelayanan, req.Usia, req.NoReg)
	persistem, _ := su.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.NoReg, req.Usia, req.Pelayanan)
	dPemfisik, _ := su.rmeRepository.OnGetPengkajianDPemFIsikRepository(req.Kategori, req.Person, req.KdBagian, req.Pelayanan)

	riwayat, _ := su.kebidananRepository.OnGetRiwayatPengobatanDirumahRepository(req.NoReg)
	karu, _ := su.rmeRepository.OnGetDataKaruRepository(req.KdBagian)
	perawat, _ := su.rmeRepository.OnGetDataPerawatRepository(pengkajian.UserID)

	nutrisi, _ := su.nyeriRepository.OnGetPengkajianNutrisiRepository(req.NoReg, req.KdBagian, req.Pelayanan, "DEWASA")
	mapperNutrisi := su.nyeriMapper.ToMappingResponsePengkajianNutrisi(nutrisi)
	asuhan, _ := su.rmeRepository.GetDataAsuhanKeperawatanRepositoryV4(req.NoReg, req.KdBagian)
	pemFisik, _ := su.rmeRepository.GetPemeriksaanFisikIGDPerawatRepository("Dokter", req.KdBagian, req.NoReg)
	fisiks := su.rmeMapper.ToMapperPemeriksaanFisikReponse(pemFisik)
	vital, _ := su.soapRepository.GetTandaVitalBangsalRepository(req.NoReg, req.KdBagian)
	funsional, _ := su.kebidananRepository.OnGetPengkajianFungsionalRepository(req.NoReg)
	nyeri, _ := su.nyeriRepository.OnGetAsesmenUlangNyeriRepository(req.NoReg, req.KdBagian, req.Pelayanan, "AWAL")
	resikoJatuh, _ := su.igdRepository.OnGetResikoJatuhRANA_DEWASA_Repository(req.KdBagian, req.NoReg)

	dto := igdDto.OnGetPengkajianIGD{
		Noreg:     req.NoReg,
		KdBagian:  req.KdBagian,
		Pelayanan: req.Pelayanan,
		Usia:      req.Usia,
	}

	nyeris, _ := su.igdUseCase.OnGetPengkajianNyeriIGDAnakUseCase(dto)

	mapper := su.rmeMapper.ToMappingPengkajianKeperawatan(pengkajian, riwayat, karu, perawat, mapperNutrisi, asuhan, fisiks, vital, funsional, nyeri, resikoJatuh, persistem, dPemfisik, nyeris)
	return mapper, nil
}
