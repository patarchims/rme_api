package rme

import (
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
)

func (su *rmeUseCase) OnSaveNyeriUseCase(req dto.ReqSaveAsesmenNyeri, kdBagian string, userID string) (res rme.DasesmenUlangNyeri, message string, err error) {

	// SAVE DATA ASESMEN NYERI
	res, er123 := su.rmeRepository.OnSaveDAssesmenNyeriRepository(kdBagian, userID, req)

	if er123 != nil {
		return res, "Data gagal disimpan", er123
	}

	return res, "Data berhasil disimpan", nil
}
