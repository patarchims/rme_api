package rme

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"mime/multipart"
	"os"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

func (su *rmeUseCase) OnSaveDownScoreNeoNatusUseCase(noReg string, req dto.ReqSaveDownScreNeoNatus, kdBagian string, userID string, person string) (res dto.NeoNatusresponse, message string, err error) {
	neo, err12 := su.rmeRepository.OnGetDownScroeNeoNatusRepository(noReg)

	if err12 != nil || neo.Noreg == "" {
		su.logging.Info("SAVE NEONATUS REPOSITORY")
		times := time.Now()
		// SIMPAN DATA
		var neoNatus = rme.DownScoreNeoNatusModel{
			InsertDttm:     times.Format("2006-01-02 15:04:05"),
			Noreg:          noReg,
			KdBagian:       kdBagian,
			InsertUserId:   userID,
			Person:         person,
			FrekwensiNafas: req.FrekwensiNafas,
			Sianosis:       req.Sianosis,
			Retraksi:       req.Retraksi,
			AirEntry:       req.AirEntry,
			Merintih:       req.Merintih,
			Total:          req.Total,
		}

		save, er11 := su.rmeRepository.OnSaveScroeNeoNatusRepository(neoNatus)

		if er11 != nil {
			return res, "Data gagal disimpan", err12
		}

		mapper := su.rmeMapper.ToMapperResponseNeonatus(save)

		return mapper, "Data berhasil disimpan", nil

	} else {
		su.logging.Info("UPDATE NEONATUS REPOSITORY")

		var neoNatus = rme.DownScoreNeoNatusModel{
			Noreg:          noReg,
			FrekwensiNafas: req.FrekwensiNafas,
			Sianosis:       req.Sianosis,
			Retraksi:       req.Retraksi,
			AirEntry:       req.AirEntry,
			Merintih:       req.Merintih,
			Total:          req.Total,
		}

		update, e11 := su.rmeRepository.OnUpdateScoreNeoNatusRepository(neoNatus, req.Noreg)

		if e11 != nil {
			return res, "Data gagal diupdate", e11
		}

		mapper := su.rmeMapper.ToMapperResponseNeonatus(update)

		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSaveApgarScoreNeoNatus(noReg string, req dto.ReqSaveApgarScore, kdBagian string, userID string) (res dto.ApgarScoreResponse, message string, err error) {
	apgar, err12 := su.rmeRepository.OnCheckApgarScoreRepository(noReg, req.Waktu)

	if err12 != nil || apgar.Noreg == "" {
		su.logging.Info("LAKUKAN INSERT DATA APGAR SCORE")
		times := time.Now()
		var score = rme.DApgarScoreNeoNatus{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			Noreg:        noReg,
			KdBagian:     kdBagian,
			InsertUserId: userID,
			Waktu:        req.Waktu,
			DJantung:     req.DJantung,
			UNafas:       req.UNafas,
			Refleksi:     req.Refleksi,
			Otot:         req.Otot,
			WarnaKulit:   req.WarnaKulit,
			Total:        req.Total,
		}
		save, er12 := su.rmeRepository.OnInsertApgarScoreRepository(score)

		if er12 != nil {
			return res, "Data gagal disimpan", er12
		}

		mapper := su.rmeMapper.ToMapperSingleScoreNeoNatus(save)

		return mapper, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("LAKUKAN UPDATE PADA APGAR SCORE")
		update, er12 := su.rmeRepository.OnUpdateApgarSocreRepository(req, noReg)

		if er12 != nil {
			return res, "Data gagal diupdate", er12
		}

		return update, "Data berhasil diupdate", nil

	}

}

func (su *rmeUseCase) OnSaveAnalisaDataUseCase(kdBagian string, userID string, req dto.ReqSaveAnalisaData) (message string, err error) {
	times := time.Now()
	kode, err12 := su.rmeRepository.GetNomorAnalisaDataRepository()
	if err12 != nil {
		message := fmt.Sprintf("Error %s", err12.Error())
		return "Data gagal disimpan", errors.New(message)
	}

	var analisaData = rme.DAnalisaData{
		InsertDttm:  times.Format("2006-01-02 15:04:05"),
		KodeAnalisa: kode.KodeAnalisa,
		KdBagian:    kdBagian,
		UserId:      userID,
		Noreg:       req.NoReg,
		Data:        req.Data,
	}

	// INSERT ANALISA DATA
	su.rmeRepository.SaveAnalisaDataRepository(analisaData)

	for i := 0; i <= len(req.Diagnosa)-1; i++ {
		//==//
		var problem = rme.DAnalisaProblem{
			Noreg:        req.NoReg,
			KodeAnalisa:  kode.KodeAnalisa,
			IdDiagnosa:   req.Diagnosa[i].Kode,
			NamaDiagnosa: req.Diagnosa[i].Judul,
		}

		su.rmeRepository.SaveDAnalisaProblemRepository(problem)
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnGetReportResumeMedisPerinaUseCase(noRM string, noReg string) (response dto.ResponseResumeMedisPerinatologi) {
	asesmen, _ := su.rmeRepository.OnGetAsesmenKeperawatanBayiRepository(noReg, "PERI")
	profil, _ := su.reportRepository.GetProfilePasienRepository(noRM)

	mapper := su.rmeMapper.TOMapperResponseResumeMedisPerinatologi(asesmen, profil, noReg)
	return mapper
}

func (su *rmeUseCase) OnGetTindakLanjutPerinaDokterUseCase(noReg string, kdBagian string) (res dto.ResponseTindakLajutPerina, err error) {
	tindakLanjut, _ := su.rmeRepository.OnGetTindakLanjutPerinaRepository(noReg, kdBagian)

	mapper := su.rmeMapper.ToResponseTindakLajutMapper(tindakLanjut)
	return mapper, nil
}

func (su *rmeUseCase) OnSaveTindakLanjutPerinaDokterUseCase(noReg string, kdBagian string, userID string, req dto.ReqSaveTindakLajutPerinatologi) (message string, err error) {
	// CEK ASESMEN TERLEBIH DAHULU
	tindak, err12 := su.rmeRepository.OnGetTindakLanjutPerinaRepository(noReg, kdBagian)

	if err12 != nil || tindak.Noreg == "" {
		// SIMPAN DATA TINDAK LANJUT PERINA REPOSITO
		tanggals, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		times := time.Now()
		var tindaks = rme.TindakLajutPerina{
			InsertDttm:             times.Format("2006-01-02 15:04:05"),
			InsertPc:               req.DeviceID,
			InsertUserId:           userID,
			KeteranganPerson:       req.Person,
			TglMasuk:               tanggals.Tanggal[0:10],
			KdBagian:               kdBagian,
			Noreg:                  req.Noreg,
			AsesmenTindakanOperasi: req.AsesmenTindakanOperasi,
			AsesmedTindakLanjut:    req.AsesmedTindakLanjut,
			AsesmenTglKontrolUlang: req.AsesmenTglKontrolUlang,
		}

		su.rmeRepository.OnSaveTindakLanjutPerinaRepository(tindaks)

		return "Data berhasil disimpan", nil

	} else {
		// UPDATE DATA TINDAK LAJUT PERINA

		var tindaks = rme.TindakLajutPerina{
			InsertPc:               req.DeviceID,
			InsertUserId:           userID,
			KeteranganPerson:       req.Person,
			KdBagian:               kdBagian,
			Noreg:                  req.Noreg,
			AsesmenTindakanOperasi: req.AsesmenTindakanOperasi,
			AsesmedTindakLanjut:    req.AsesmedTindakLanjut,
			AsesmenTglKontrolUlang: req.AsesmenTglKontrolUlang,
		}

		su.rmeRepository.OnUpdateTindakLanjutPerinaRepository(tindaks, noReg, kdBagian)

		return "Data berhasil diupdate", nil
	}

}

func (su *rmeUseCase) OnUploadIndentiasBayiPerinaUseCase(kategori string, noReg string, kdBagian string, files *multipart.FileHeader, c *fiber.Ctx, userID string, kdDpjp string) (res dto.ResponseIdentitasBayi, message string, err error) {
	switch kategori {
	case "KAKI-KANAN-BAYI":
		su.logging.Info("UPLOAD KAKI KANAN BAYI")
		// GET DATA TERLEBIH DAHULU
		data, err12 := su.rmeRepository.OnGetKakiKananBayiRepository(noReg, kdBagian)

		if err12 != nil || data.Noreg == "" {
			su.logging.Info("LAKUKAN UPLOAD DATA")
			tipe := strings.Split(files.Filename, ".")

			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "kaki_kanan_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				return res, "Gagal upload image, error: " + errs12.Error(), errs12
			}

			tanggals, _ := su.soapRepository.GetJamMasukPasienRepository(noReg)

			var kaki = rme.KakiKananBayi{
				InsertDttm:                     times.Format("2006-01-02 15:04:05"),
				InsertUserId:                   userID,
				Pelayanan:                      "ranap",
				KeteranganPerson:               "Perawat",
				TglMasuk:                       tanggals.Tanggal[0:10],
				KdBagian:                       kdBagian,
				KdDpjp:                         kdDpjp,
				Noreg:                          noReg,
				AseskepImageSidikKakiKananBayi: fileName,
			}

			// INSERT DATA
			kakis, _ := su.rmeRepository.OnInsertKakiKananBayiRepository(kaki)

			mapper := su.rmeMapper.ToMapperIdentitasBayi(kakis)

			return mapper, "Gambar berhasil diupload", nil
		} else {

			// check apakah ada data sebelumnya
			if data.AseskepImageSidikKakiKananBayi != "" {
				su.logging.Info("GAGAL HAPUS FILE KAKI KANAN")
				errs1 := tryRemoveFile("images/identitas-bayi/" + data.AseskepImageSidikKakiKananBayi)

				if errs1 != nil {
					fmt.Printf("Error removing file: %v\n", err)
					// return res, errs1.Error(), errs1
				}

			}

			tipe := strings.Split(files.Filename, ".")
			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "kaki_kanan_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				su.logging.Info("GAGAL SIMPAN FILE")
				return res, "GAGAL SIMPAN FILE :" + errs12.Error(), errs12
			}

			var kaki = rme.KakiKananBayi{
				AseskepImageSidikKakiKananBayi: fileName,
			}

			update, er1 := su.rmeRepository.OnUpdateKakiKananBayiRepository(kaki, noReg, kdBagian)

			if er1 != nil {
				message := fmt.Sprintf("Error %s, Data gagal di update", er1.Error())
				return res, message, errors.New(message)
			}

			mapper := su.rmeMapper.ToMapperIdentitasBayi(update)
			return mapper, "Gambar berhasil diubah", nil
		}

	case "KAKI-KIRI-BAYI":
		su.logging.Info("UPLOAD KAKI KIRI BAYI")
		data, err12 := su.rmeRepository.OnGetKakiKananBayiRepository(noReg, kdBagian)
		if err12 != nil || data.Noreg == "" {
			su.logging.Info("LAKUKAN UPLOAD DATA")
			tipe := strings.Split(files.Filename, ".")

			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "kaki_kiri_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				return res, "Gagal upload image, error: " + errs12.Error(), errs12
			}

			tanggals, _ := su.soapRepository.GetJamMasukPasienRepository(noReg)

			var kaki = rme.KakiKananBayi{
				InsertDttm:                    times.Format("2006-01-02 15:04:05"),
				InsertUserId:                  userID,
				Pelayanan:                     "ranap",
				KeteranganPerson:              "Perawat",
				TglMasuk:                      tanggals.Tanggal[0:10],
				KdBagian:                      kdBagian,
				KdDpjp:                        kdDpjp,
				Noreg:                         noReg,
				AseskepImageSidikKakiKiriBayi: fileName,
			}

			// INSERT DATA
			kakis, _ := su.rmeRepository.OnInsertKakiKananBayiRepository(kaki)

			mapper := su.rmeMapper.ToMapperIdentitasBayi(kakis)

			return mapper, "Gambar berhasil diupload", nil
		} else {
			// check apakah ada data sebelumnya
			if data.AseskepImageSidikKakiKananBayi != "" {
				su.logging.Info("GAGAL HAPUS FILE KAKI KANAN")
				errs1 := tryRemoveFile("images/identitas-bayi/" + data.AseskepImageSidikKakiKananBayi)

				if errs1 != nil {
					fmt.Printf("Error removing file: %v\n", err)
					// return res, errs1.Error(), errs1
				}

			}

			tipe := strings.Split(files.Filename, ".")
			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "kaki_kiri_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				su.logging.Info("GAGAL SIMPAN FILE")
				return res, "GAGAL SIMPAN FILE :" + errs12.Error(), errs12
			}

			var kaki = rme.KakiKananBayi{
				AseskepImageSidikKakiKiriBayi: fileName,
			}

			update, er1 := su.rmeRepository.OnUpdateKakiKananBayiRepository(kaki, noReg, kdBagian)

			if er1 != nil {
				message := fmt.Sprintf("Error %s, Data gagal di update", er1.Error())
				return res, message, errors.New(message)
			}

			mapper := su.rmeMapper.ToMapperIdentitasBayi(update)
			return mapper, "Gambar berhasil diubah", nil
		}

	case "TANGAN-KIRI-IBU":
		su.logging.Info("UPLOAD KAKI KIRI BAYI")
		data, err12 := su.rmeRepository.OnGetKakiKananBayiRepository(noReg, kdBagian)
		if err12 != nil || data.Noreg == "" {
			su.logging.Info("LAKUKAN UPLOAD DATA")
			tipe := strings.Split(files.Filename, ".")

			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "tangan_kiri_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				return res, "Gagal upload image, error: " + errs12.Error(), errs12
			}

			tanggals, _ := su.soapRepository.GetJamMasukPasienRepository(noReg)

			var kaki = rme.KakiKananBayi{
				InsertDttm:                 times.Format("2006-01-02 15:04:05"),
				InsertUserId:               userID,
				Pelayanan:                  "ranap",
				KeteranganPerson:           "Perawat",
				TglMasuk:                   tanggals.Tanggal[0:10],
				KdBagian:                   kdBagian,
				KdDpjp:                     kdDpjp,
				Noreg:                      noReg,
				AseskepImageIbuJariKiriIbu: fileName,
			}

			// INSERT DATA
			kakis, _ := su.rmeRepository.OnInsertKakiKananBayiRepository(kaki)

			mapper := su.rmeMapper.ToMapperIdentitasBayi(kakis)

			return mapper, "Gambar berhasil diupload", nil
		} else {
			// check apakah ada data sebelumnya
			if data.AseskepImageSidikKakiKananBayi != "" {
				su.logging.Info("GAGAL HAPUS FILE KAKI KANAN")
				errs1 := tryRemoveFile("images/identitas-bayi/" + data.AseskepImageSidikKakiKananBayi)

				if errs1 != nil {
					fmt.Printf("Error removing file: %v\n", err)
				}

			}

			tipe := strings.Split(files.Filename, ".")
			times := time.Now()

			fileName := fmt.Sprintf("%s.%s", "tangan_kiri_"+times.Format("20060102150405")+noReg, tipe[1])
			path := fmt.Sprintf("images/identitas-bayi/%s", fileName)

			// UPLOAD FILE
			errs12 := c.SaveFile(files, path)

			if errs12 != nil {
				su.logging.Info("GAGAL SIMPAN FILE")
				return res, "GAGAL SIMPAN FILE :" + errs12.Error(), errs12
			}

			var kaki = rme.KakiKananBayi{
				AseskepImageIbuJariKiriIbu: fileName,
			}

			update, er1 := su.rmeRepository.OnUpdateKakiKananBayiRepository(kaki, noReg, kdBagian)

			if er1 != nil {
				message := fmt.Sprintf("Error %s, Data gagal di update", er1.Error())
				return res, message, errors.New(message)
			}

			mapper := su.rmeMapper.ToMapperIdentitasBayi(update)
			return mapper, "Gambar berhasil diubah", nil
		}

	default:
		su.logging.Info("TIDAK DAPAT DIKATEGORIKAN")
		return res, "Gambar tidak dapat disimpan", errors.New("Error")
	}

}

func tryRemoveFile(filePath string) error {
	logrus.Info("Attempt to open the file with write permissions to ensure it's not in use")
	file, err := os.OpenFile(filePath, os.O_WRONLY, 0)
	if err != nil {
		return err
	}

	defer file.Close()

	logrus.Info("Now that the file is open, attempt to remove it")
	err = os.Remove(filePath)
	if err != nil {
		return err
	}

	return nil
}

func (su *rmeUseCase) OnGetRingkasanMasuDanKeluarUseCase(noRM string) (res dto.ResponseRingkasanMasukPasien, err error) {
	// GET PROFIL PASIEN
	profil, _ := su.reportRepository.GetDataPasienRepository(noRM)
	// MAPPING PROFILE PASIEN
	mapper := su.rmeMapper.ToMapperResponseRingkasanMasukPasien(profil)
	return mapper, nil
}

func (su *rmeUseCase) OnSaveIdentitasPerinaUseCase(ttd1 *multipart.FileHeader, ttd2 *multipart.FileHeader, c *fiber.Ctx, noReg string, kdBagian string, kdDPJP string, userID string, namaPenentuJK string, nanaWali string, namPemberiGelang string, jamKelahiran string) (message string, err error) {
	// PERIKSA DATA TERLEBIH DAHULU
	// data, err12 := rh.RMERepository.OnGetKakiKananBayiRepository(payload.NoReg, modulID)
	data, er12 := su.rmeRepository.OnGetKakiKananBayiRepository(noReg, kdBagian)

	if data.Noreg == "" || er12 != nil {
		// SIMPAN DATA
		tanggals, _ := su.soapRepository.GetJamMasukPasienRepository(noReg)
		times := time.Now()

		tipe1 := strings.Split(ttd1.Filename, ".")
		tipe2 := strings.Split(ttd2.Filename, ".")

		fileName1 := fmt.Sprintf("%s.%s", "ttd_p_jk"+times.Format("20060102150405")+noReg, tipe1[1])
		path1 := fmt.Sprintf("images/identitas-bayi/%s", fileName1)

		fileName2 := fmt.Sprintf("%s.%s", "ttd_wali"+times.Format("20060102150405")+noReg, tipe2[1])
		path2 := fmt.Sprintf("images/identitas-bayi/%s", fileName2)

		// UPLOAD FILE
		errs12 := c.SaveFile(ttd1, path1)

		if errs12 != nil {
			su.logging.Info("GAGAL SIMPAN FILE")
			return "GAGAL SIMPAN FILE :" + errs12.Error(), errs12
		}

		errs122 := c.SaveFile(ttd2, path2)

		if errs122 != nil {
			su.logging.Info("GAGAL SIMPAN FILE")
			return "GAGAL SIMPAN FILE :" + errs122.Error(), errs122
		}

		var kaki = rme.KakiKananBayi{
			InsertDttm:               times.Format("2006-01-02 15:04:05"),
			InsertUserId:             userID,
			Pelayanan:                "ranap",
			KeteranganPerson:         "Perawat",
			TglMasuk:                 tanggals.Tanggal[0:10],
			KdBagian:                 kdBagian,
			KdDpjp:                   kdDPJP,
			Noreg:                    noReg,
			AseskepNamaPenentuJk:     namaPenentuJK,
			AseskepNamaWali:          nanaWali,
			AseskepPemberiGelangBayi: namPemberiGelang,
			AseskepTtdPenentuJk:      fileName1,
			AseskepTtdWali:           fileName2,
			AseskepJamKelahiran:      jamKelahiran,
		}

		su.rmeRepository.OnInsertKakiKananBayiRepository(kaki)

		return "Data berhasil disimpan", nil

	} else {
		// UPDATE DATA

		if data.AseskepTtdWali != "" {
			su.logging.Info("HAPUS TTD WALI")
			errs1 := tryRemoveFile("images/identitas-bayi/" + data.AseskepImageSidikKakiKananBayi)

			if errs1 != nil {
				fmt.Printf("Error removing file: %v\n", err)
			}
		}

		if data.AseskepTtdPenentuJk != "" {
			su.logging.Info("HAPUS TTD WALI")
			errs1 := tryRemoveFile("images/identitas-bayi/" + data.AseskepTtdPenentuJk)

			if errs1 != nil {
				fmt.Printf("Error removing file: %v\n", err)
			}
		}

		times := time.Now()

		tipe1 := strings.Split(ttd1.Filename, ".")
		tipe2 := strings.Split(ttd2.Filename, ".")

		fileName1 := fmt.Sprintf("%s.%s", "ttd_p_jk"+times.Format("20060102150405")+noReg, tipe1[1])
		path1 := fmt.Sprintf("images/identitas-bayi/%s", fileName1)

		fileName2 := fmt.Sprintf("%s.%s", "ttd_wali"+times.Format("20060102150405")+noReg, tipe2[1])
		path2 := fmt.Sprintf("images/identitas-bayi/%s", fileName2)

		// UPLOAD FILE
		errs12 := c.SaveFile(ttd1, path1)

		if errs12 != nil {
			su.logging.Info("GAGAL SIMPAN FILE")
			return "GAGAL SIMPAN FILE :" + errs12.Error(), errs12
		}

		errs122 := c.SaveFile(ttd2, path2)

		if errs122 != nil {
			su.logging.Info("GAGAL SIMPAN FILE")
			return "GAGAL SIMPAN FILE :" + errs122.Error(), errs122
		}

		var kaki = rme.KakiKananBayi{
			AseskepNamaPenentuJk:     namaPenentuJK,
			AseskepNamaWali:          nanaWali,
			AseskepPemberiGelangBayi: namPemberiGelang,
			AseskepTtdPenentuJk:      fileName1,
			AseskepTtdWali:           fileName2,
			AseskepJamKelahiran:      jamKelahiran,
		}

		_, er1 := su.rmeRepository.OnUpdateKakiKananBayiRepository(kaki, noReg, kdBagian)

		if er1 != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", er1.Error())
			return message, errors.New(message)
		}

		return "Data berhasil diubah", nil
	}

}

func (su *rmeUseCase) OnReportEarlyWarningSystemUseCase(noReg string) (err error) {
	return nil
}
