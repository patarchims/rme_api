package rme

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"time"
)

func (su *rmeUseCase) OnSaveReaksiTranfusiDarahUseCase(req dto.ReqReaksiTranfusiDarah) (message string, err error) {
	// CEK DATA TERLBIH DAHLU
	su.rmeRepository.GetReaksiTransfusiDarahRepository(req.Noreg, req.Reaksi)
	return message, nil
}

func (su *rmeUseCase) OnDeleteReaksiTransfusiDarahUseCase(req dto.ReqDeleteReaksiTranfusiDarah) (message string, err error) {
	// LAKUKAN DELETE DATA
	er12 := su.rmeRepository.OnDeleteReaksiTransfusiDarahRepositoru(req.IDReaksi)

	if er12 != nil {
		message := fmt.Sprintf("Data tidak dapat dihapus, error : %s", er12.Error())
		return message, errors.New(message)
	}
	return "Data berhasil di hapus", nil
}

func (su *rmeUseCase) OnDeleteTransfusiDarahUseCase(req dto.ReqDeleteTransfusiDarah) (message string, err error) {
	data, _ := su.rmeRepository.OnGetReaksiByIDTransfusiDarahRepository(req.IDTransfusi)

	if len(data) > 0 {
		message := fmt.Sprintf("Data tidak dapat dihapus, reaksi sudah ada. %s", "")
		return message, errors.New(message)
	}

	er12 := su.rmeRepository.OnDeleteTransfusiDarahRepositoru(req.IDTransfusi)

	if er12 != nil {
		message := fmt.Sprintf("Data tidak dapat dihapus,\nerror : %s", er12.Error())
		return message, errors.New(message)
	}

	return "Data berhasil di hapus", nil
}

func (su *rmeUseCase) OnUpdateTransfusiDarahUsecase(req dto.ReqOnChangedTransfusiDarah) (res rme.DTransfusiDarah, message string, err error) {
	update, er12 := su.rmeRepository.OnChangedTransfusiDarahRepository(req)

	if er12 != nil {
		return res, "Data gagal diubah", nil
	}

	return update, "Data berhasi diubah", nil
}

func (su *rmeUseCase) OnVerifyTransfusiDarahUseCase(req dto.VerifyTransfusiDarah, kdBagian string, userID string) (messasge string, err error) {
	// GET GERLEBIH DAHULU DATA VERIFY SUDAH ADA ATAU BELUM
	data, _ := su.rmeRepository.OnGetVerifikasiTransfusiDarahByIDTransfusiRepository(req.IDTransfusi)

	if data.Noreg == "" {
		datVeri := rme.DverifyTransfusiDarah{
			IDTransfusi:   req.IDTransfusi,
			KdBagian:      kdBagian,
			Noreg:         req.Noreg,
			NoKantung:     req.NoKantung,
			InsertDttm:    time.Now(),
			UserVerify:    userID,
			JDarah:        req.JenisDarah,
			GolDarah:      req.GolonganDarah,
			NoStock:       req.NoStock,
			TglKadaluarsa: req.TglKadaluwarsa,
		}
		_, er123 := su.rmeRepository.OnSaveVerifikasiTransfusiDrahRepository(datVeri)

		if er123 != nil {
			message := fmt.Sprintf("Data tidak dapat disimpan, error : %s", er123)
			return message, er123
		}

		return "Data berhasil disimpan", nil
	}

	if data.Noreg != "" {
		update := rme.DverifyTransfusiDarah{
			IDTransfusi:   req.IDTransfusi,
			KdBagian:      kdBagian,
			Noreg:         req.Noreg,
			NoKantung:     req.NoKantung,
			InsertDttm:    time.Now(),
			UserVerify:    userID,
			JDarah:        req.JenisDarah,
			GolDarah:      req.GolonganDarah,
			NoStock:       req.NoStock,
			TglKadaluarsa: req.TglKadaluwarsa,
		}

		_, e1111 := su.rmeRepository.OnUpdateDataVerifikasiTransfusiDarahByIDAndNoregRepository(update, req.IDTransfusi, req.Noreg)

		if e1111 != nil {

			message := fmt.Sprintf("Data tidak dapat disimpan, error : %s", e1111)
			return message, e1111
		}
		return "Data berhasil diubah", nil
	}

	return "Data gagal diprose", errors.New("data gagal diproses")
}

func (su *rmeUseCase) OnSaveReaksiTransfusiDarahUseCase(userID string, req dto.ReqReaksiTranfusiDarah) (message string, err error) {

	// CEK APAKAH SUDAH INPUT REAKSI TRANFUSI DARAH
	reaksi, _ := su.rmeRepository.GetReaksiTransfusiDarahRepository(req.Noreg, req.Reaksi)

	if reaksi.Reaksi == "" {
		_, er12 := su.rmeRepository.OnSaveReaksiTransufiDarahRepository(userID, req)

		if er12 != nil {
			message := fmt.Sprintf("Data tidak dapat disimpan, error : %s", er12.Error())
			return message, errors.New(message)
		}

		return "Data berhasil disimpan", nil
	} else {
		message := fmt.Sprintf("Data  %s, sudah di entri", req.Reaksi)
		return message, errors.New(message)
	}

}
