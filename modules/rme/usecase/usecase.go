package rme

import (
	"errors"
	"fmt"
	diagnosaEntity "hms_api/modules/diagnosa/entity"
	hs "hms_api/modules/his/entity"
	"hms_api/modules/igd"
	igdEntity "hms_api/modules/igd/entity"
	kebidananEntity "hms_api/modules/kebidanan/entity"
	nyeriEntity "hms_api/modules/nyeri/entity"
	rp "hms_api/modules/report/entity"
	"hms_api/modules/rme"
	"hms_api/modules/rme/dto"
	"hms_api/modules/rme/entity"
	en "hms_api/modules/soap/entity"
	us "hms_api/modules/user/entity"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type rmeUseCase struct {
	rmeRepository       entity.RMERepository
	rmeMapper           entity.RMEMapper
	logging             *logrus.Logger
	soapUsece           en.SoapUseCase
	soapRepository      en.SoapRepository
	hisUseCase          hs.HisUsecase
	userRepository      us.UserRepository
	userMapper          us.IUserMapper
	reportRepository    rp.ReportRepository
	diagnosaRepository  diagnosaEntity.DiagnosaRepository
	kebidananRepository kebidananEntity.KebidananRepository
	igdRepository       igdEntity.IGDRepository
	nyeriRepository     nyeriEntity.NyeriRepository
	nyeriMapper         nyeriEntity.NyeriMapper
	igdUseCase          igdEntity.IGDUseCase
}

func NewRMEUseCase(rr entity.RMERepository, rm entity.RMEMapper, loggging *logrus.Logger, su en.SoapUseCase, sr en.SoapRepository, hu hs.HisUsecase, userRepo us.UserRepository, reportRepository rp.ReportRepository, um us.IUserMapper, dr diagnosaEntity.DiagnosaRepository, kebidananRepository kebidananEntity.KebidananRepository, igdRepository igdEntity.IGDRepository, nyeriRepository nyeriEntity.NyeriRepository, nyeriMapper nyeriEntity.NyeriMapper, igdUseCase igdEntity.IGDUseCase) entity.RMEUseCase {
	return &rmeUseCase{
		rmeRepository:       rr,
		rmeMapper:           rm,
		logging:             loggging,
		soapUsece:           su,
		soapRepository:      sr,
		hisUseCase:          hu,
		reportRepository:    reportRepository,
		userRepository:      userRepo,
		userMapper:          um,
		diagnosaRepository:  dr,
		kebidananRepository: kebidananRepository,
		igdRepository:       igdRepository,
		nyeriRepository:     nyeriRepository,
		nyeriMapper:         nyeriMapper,
		igdUseCase:          igdUseCase,
	}
}

func (su *rmeUseCase) GetIntervensiUseCase(req dto.RegIntervensi) (res []dto.MapperSLKI, err error) {

	parts := strings.Split(req.Slki, "\n")

	var values []dto.MapperSLKI

	var dat []dto.ResponseSLKI

	var Kriteria = []string{"1", "2", "3", "4", "5"}

	for _, part := range parts {

		su.logging.Info(" GET INTERVENSI " + part)

		if part != "" {
			detail, errs := su.rmeRepository.GetSLKIRepository(part)

			if errs != nil {
				return res, errors.New(errs.Error())
			} else {
				for i := 0; i <= len(detail)-1; i++ {

					sat := dto.ResponseSLKI{
						Nomor:          i,
						NoUrut:         detail[i].NoUrut,
						Tanda:          detail[i].Tanda,
						Menurun:        detail[i].Menurun,
						Meningkat:      detail[i].Meningkat,
						Memburuk:       detail[i].Memburuk,
						Kode:           detail[i].Kode,
						Ekspektasi:     detail[i].Ekspektasi,
						Kriteria:       Kriteria,
						Judul:          detail[i].Judul,
						SelectedNumber: 0,
					}

					dat = append(dat, sat)
				}
			}

			data := dto.MapperSLKI{
				Kode:         part,
				ResponseSLKI: dat,
			}

			values = append(values, data)
		}

	}

	return values, nil
}

func (su *rmeUseCase) GetSIKIUseCase(siki string) (res []rme.SIKIModel, err error) {
	// =====
	if len(siki) > 0 {
		parts := strings.Split(siki, ",")

		var values []rme.SIKIModel
		// GET SIKI
		for _, part := range parts {
			su.logging.Info(" GET SIKI " + part)

			if part != "" {
				// GET SIKI
				detail, errs := su.rmeRepository.GetSIKIRepository(part)

				if errs != nil {
					return res, errors.New(errs.Error())
				}

				if detail.Judul != "" {
					values = append(values, detail)
				}

			}
		}
		return values, nil
	}

	return res, nil
}

func (su *rmeUseCase) CariDeskripsiSikiUsecase(siki string) (res []dto.ResponseDeskripsiSiki, err error) {

	if len(siki) > 0 {
		parts := strings.Split(siki, ",")

		var values []rme.SikiDeskripsiModel

		// GET SIKI

		for _, part := range parts {

			if part != "" {
				detail, errs := su.rmeRepository.GetDeskripsiSikiRepository(part)

				if errs != nil {
					su.logging.Info(errs.Error())
					return res, errors.New(errs.Error())
				}
				if detail.Judul != "" {
					values = append(values, detail)
				}

			}
		}

		var responseItems []dto.ResponseDeskripsiSiki

		if len(values) > 1 {
			for _, valu := range values {
				responseItem := dto.ResponseDeskripsiSiki{
					Kode:        valu.Kode,
					Judul:       valu.Judul,
					Observasi:   su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(valu.DeskripsiSikiModel, "Observasi")),
					Terapetutik: su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(valu.DeskripsiSikiModel, "Terapetutik")),
					Edukasi:     su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(valu.DeskripsiSikiModel, "Edukasi")),
					Kolaborasi:  su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(valu.DeskripsiSikiModel, "Kolaborasi")),
				}

				responseItems = append(responseItems, responseItem)
			}
		}

		if len(values) == 1 {
			responseItem := dto.ResponseDeskripsiSiki{
				Kode:        values[0].Kode,
				Judul:       values[0].Judul,
				Observasi:   su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(values[0].DeskripsiSikiModel, "Observasi")),
				Terapetutik: su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(values[0].DeskripsiSikiModel, "Terapetutik")),
				Edukasi:     su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(values[0].DeskripsiSikiModel, "Edukasi")),
				Kolaborasi:  su.rmeMapper.ToMapperDeskripsiSiki(filterDeskripsi(values[0].DeskripsiSikiModel, "Kolaborasi")),
			}

			responseItems = append(responseItems, responseItem)
		}

		return responseItems, nil
	}

	return res, nil
}

func filterDeskripsi(items []rme.DeskripsiSikiModel, kategori string) []rme.DeskripsiSikiModel {

	var filteredItems []rme.DeskripsiSikiModel

	for _, item := range items {
		if item.Kategori == kategori {
			filteredItems = append(filteredItems, item)
		}
	}

	return filteredItems
}

func (su *rmeUseCase) SaveAsesmenKeperawatanUsecase(kdBagian string, req dto.ReqAsesmenKeperawatan) (message string, err error) {
	times := time.Now()
	soap, err := su.rmeRepository.SearchDcpptSoapPasienRepository(req.Asesemen.Noreg, kdBagian)

	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA ASESKEP DAN DATA DASKEP")

		for i := 0; i <= len(req.Intervensi)-1; i++ {
			for _, inter := range req.Intervensi[i].Slki {

				daskep := rme.Daskep{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					Tanggal:    times.Format("2006-01-02"),
					KdBagian:   kdBagian,
					Noreg:      req.Asesemen.Noreg,
					NoUrut:     inter.NoUrut,
					Nilai:      inter.SelectionNumber,
					KodeSdki:   req.Sdki,
					KodeSlki:   inter.Kode,
					KodeSiki:   req.Siki,
					Hasil:      "",
				}

				_, err := su.rmeRepository.InsertDaskepRepository(daskep)

				if err != nil {
					return "", errors.New(err.Error())
				}
			}
		}

		// SIMPAN SOAP RESPOSITORY
		_, errs := su.rmeRepository.InsertAsesmenKeperawatanBidanRepository(kdBagian, req)
		if errs != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errs.Error())
			return message, errors.New(message)
		} else {
			return "Data berhasil disimpan", nil
		}
	} else {
		// SEHARUSNYA
		su.logging.Info("UPDATE DATA ASESKEP DAN DATA DASKEP")

		// CARI APAKAH ADA DATA DASKEP
		cari, errs := su.rmeRepository.CariDataDaskepRepository(kdBagian, req.Asesemen.Noreg)

		su.logging.Info(cari)

		if errs != nil || len(cari.Noreg) <= 0 {
			// INSERT DATA
			su.logging.Info("DATA NOREG  TIDAK DITEMUKAN")

			for i := 0; i <= len(req.Intervensi)-1; i++ {
				for _, inter := range req.Intervensi[i].Slki {

					daskep := rme.Daskep{
						InsertDttm: times.Format("2006-01-02 15:04:05"),
						Tanggal:    times.Format("2006-01-02"),
						KdBagian:   kdBagian,
						Noreg:      req.Asesemen.Noreg,
						NoUrut:     inter.NoUrut,
						Nilai:      inter.SelectionNumber,
						KodeSdki:   req.Sdki,
						KodeSlki:   inter.Kode,
						KodeSiki:   req.Siki,
						Hasil:      "",
					}

					_, err := su.rmeRepository.InsertDaskepRepository(daskep)

					if err != nil {
						return "", errors.New(err.Error())
					}
				}
			}
		} else {
			su.logging.Info("DATA NOREG DITEMUKAN")
			su.rmeRepository.HapusDaskepRepository(kdBagian, req.Asesemen.Noreg)
			su.logging.Info("LAKUKAN HAPUS DASKEP")

			if len(req.Intervensi) > 1 {
				// HAPUS DATA DASKEP

				// SIMPAN DATA
				// YANG BARU
				for i := 0; i <= len(req.Intervensi)-1; i++ {
					for _, inter := range req.Intervensi[i].Slki {

						daskep := rme.Daskep{
							InsertDttm: times.Format("2006-01-02 15:04:05"),
							Tanggal:    times.Format("2006-01-02"),
							KdBagian:   kdBagian,
							Noreg:      req.Asesemen.Noreg,
							NoUrut:     inter.NoUrut,
							Nilai:      inter.SelectionNumber,
							KodeSdki:   req.Sdki,
							KodeSlki:   inter.Kode,
							KodeSiki:   req.Siki,
							Hasil:      "",
						}

						_, err := su.rmeRepository.InsertDaskepRepository(daskep)

						if err != nil {
							return "", errors.New(err.Error())
						}
					}
				}

			}

		}

		_, errss := su.rmeRepository.UpdateAsesmenKeperawatanBidanRepository(kdBagian, req)

		if errss != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errss.Error())
			return message, errors.New(message)
		} else {
			return "Data berhasil diubah", nil
		}
	}
}

func (su *rmeUseCase) GetAsesmenKeperawatanBidanUseCase(noReg string, kdBagian string) (res rme.GetResponseAsesmen, message string, err error) {

	data, errs := su.rmeRepository.GetAsesmenKeperawatanRepository(noReg, kdBagian)

	if errs != nil {
		return res, "data gagal didapat", err
	}

	// GET DATA SDKI
	var dataSiki rme.SIKIModel
	var dataSdki rme.SDKIModel

	// GET DATA DASKEP daskep, ers := su.rmeRepository.GetDasekep( noReg, kdBagian)
	daskep, err := su.rmeRepository.GetDaskepResponseRepository(noReg, kdBagian)

	if err != nil {
		return res, "data gagal didapat", err
	}

	// GET DATA DASKEP
	if len(daskep) > 0 {
		// GET DATA SIKI
		siki, _ := su.rmeRepository.GetSikiByKodeRepository(daskep[0].Siki)
		dataSiki = siki

		// GET DATA SLKI GET SDKI
		sdki, _ := su.rmeRepository.GetSdkiByKodeRepository(daskep[0].Sdki)
		dataSdki = sdki
	}

	mapper := su.rmeMapper.ToResponseAsesmenKebidanan(data, daskep, dataSdki, dataSiki)

	return mapper, "Data berhasil didapat", nil
}

func (su *rmeUseCase) GetAsuhanKeperawatanBidanUseCase(noReg string, kdBagian string) (res rme.GetResponseAsuhanKeperawatan, message string, err error) {

	// GET DATA SDKI
	var dataSiki rme.SIKIModel
	var dataSdki rme.SDKIModel

	daskep, err := su.rmeRepository.GetDaskepResponseRepository(noReg, kdBagian)

	if err != nil {
		return res, "data gagal didapat", err
	}

	// GET DATA DASKEP
	if len(daskep) > 0 {
		// GET DATA SIKI
		siki, _ := su.rmeRepository.GetSikiByKodeRepository(daskep[0].Siki)
		dataSiki = siki

		// GET DATA SLKI GET SDKI
		sdki, _ := su.rmeRepository.GetSdkiByKodeRepository(daskep[0].Sdki)
		dataSdki = sdki
	}

	mapper := su.rmeMapper.ToResponseAsuhanKeperawatan(daskep, dataSdki, dataSiki)

	return mapper, "Data berhasil didapat", nil
}

func (su *rmeUseCase) OnSaveDCarianOutPutUseCase(req dto.ReqOnSaveCiaranOutPut, userID string, kdBagian string) (message string, err error) {
	cairan := rme.DcairanOutPut{
		InsertDttm: time.Now(),
		KdBagian:   kdBagian,
		UserID:     userID,
		Noreg:      req.Noreg,
		Urine:      req.Urine,
		NGT:        req.NGT,
		WSD:        req.WSD,
		Drain:      req.Drain,
		Muntah:     req.Muntah,
		Pendarahan: req.Pendarahan,
	}

	_, err11 := su.rmeRepository.OnSaveDCairanOutPutRepository(cairan)

	if err11 != nil {
		return "Data gagal disimpan", err11
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnGetMonitoringCairanUseCase(NoReg string) (res dto.ResponseMonitoringCairan, message string, err error) {
	output, _ := su.rmeRepository.OnGetCairanOutPutRepository(NoReg)
	intake, _ := su.rmeRepository.OnGetDCairanInTakeRepository(NoReg)

	// MAPPING DATA
	return dto.ResponseMonitoringCairan{
		Intake: su.rmeMapper.ToMappingResponseDCairanOutPut(intake),
		OutPut: su.rmeMapper.ToMappingResponseDCairanPut(output),
	}, "OK", nil
}

func (su *rmeUseCase) OnSavePengkajianPersistemRANAPUseCase(req dto.OnSavePengkajianPersistemBangsal) (message string, err error) {
	// GET PEMERIKSAAN FISIK RANAP
	dPemfisik, _ := su.rmeRepository.OnGetPengkajianDPemFIsikRepository(req.Kategori, req.Person, req.KdBagian, req.Pelayanan)
	persistem, _ := su.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.NoReg, req.Usia, req.Pelayanan)

	// SIMPAN DATA DPEMFISIK JIKA DATA KOSONG,
	if dPemfisik.Noreg == "" {
		pemfisik := rme.DPemFisikModel{
			InsertDttm:   time.Now(),
			UpdateDttm:   time.Now(),
			InsertDevice: req.DevicesID,
			Kategori:     req.Kategori,
			Noreg:        req.NoReg,
			KetPerson:    req.Person,
			Pelayanan:    req.Pelayanan,
			KdBagian:     req.KdBagian,
			Kepala:       req.Kepala,
			Mata:         req.Mata,
			Telinga:      req.Telinga,
			LeherDanBahu: req.LeherDanBahu,
			Hidung:       req.Hidung,
			Mulut:        req.Mulut,
			Gigi:         req.Gigi,
			Dada:         req.Dada,
			Abdomen:      req.Abdomen,
		}

		su.rmeRepository.OnSaveDPemeriksaanFisikReporitory(pemfisik)
	}

	if persistem.NoReg == "" {
		persistem := igd.DpengkajianPersistem{
			InsertDttm:             time.Now(),
			KdBagian:               req.KdBagian,
			Pelayanan:              req.Pelayanan,
			Usia:                   req.Usia,
			NutrisiDanHidrasi:      req.NutrisiDanHidrasi,
			Eliminasi:              req.Eliminasi,
			Aktivitas:              req.Aktivitas,
			SistemKardioRepiratori: req.SistemKardioRepiratori,
			SistemPerfusiSecebral:  req.SistemPerfusiSecebral,
			Thermoregulasi:         req.Thermoregulasi,
			SistemPerfusiPerifer:   req.SistemPerfusiPerifer,
			SistemPencernaan:       req.SistemPencernaan,
			Integumen:              req.Integumen,
			Proteksi:               req.Proteksi,
			SeksualReproduksi:      req.SeksualReproduksi,
		}

		su.rmeRepository.OnSaveDpengkajianPersistemRepository(persistem)
	}

	if persistem.NoReg != "" {
		persistem := igd.DpengkajianPersistem{
			NutrisiDanHidrasi:      req.NutrisiDanHidrasi,
			Eliminasi:              req.Eliminasi,
			Aktivitas:              req.Aktivitas,
			SistemKardioRepiratori: req.SistemKardioRepiratori,
			SistemPerfusiSecebral:  req.SistemPerfusiSecebral,
			Thermoregulasi:         req.Thermoregulasi,
			SistemPerfusiPerifer:   req.SistemPerfusiPerifer,
			SistemPencernaan:       req.SistemPencernaan,
			Integumen:              req.Integumen,
			Proteksi:               req.Proteksi,
			SeksualReproduksi:      req.SeksualReproduksi,
		}

		su.rmeRepository.OnUpdateDpenkajianPersistemRepository(persistem, req.NoReg, req.Pelayanan)
	}

	// UPDATE DPEMFISIK
	if dPemfisik.Noreg != "" {
		persistem := igd.DpengkajianPersistem{
			NutrisiDanHidrasi:      req.NutrisiDanHidrasi,
			Eliminasi:              req.Eliminasi,
			Aktivitas:              req.Aktivitas,
			SistemKardioRepiratori: req.SistemKardioRepiratori,
			SistemPerfusiSecebral:  req.SistemPerfusiSecebral,
			Thermoregulasi:         req.Thermoregulasi,
			SistemPerfusiPerifer:   req.SistemPerfusiPerifer,
			SistemPencernaan:       req.SistemPencernaan,
			Integumen:              req.Integumen,
			Proteksi:               req.Proteksi,
			SeksualReproduksi:      req.SeksualReproduksi,
		}
		su.rmeRepository.OnUpdateDpenkajianPersistemRepository(persistem, req.NoReg, req.Pelayanan)
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnGetPengkajianPersistemRANAPUseCase(req dto.ReqOnGetPengkajianPersistem) (res dto.PenkajianPersistemRANAP) {
	dPemfisik, _ := su.rmeRepository.OnGetPengkajianDPemFIsikRepository(req.Kategori, req.Person, req.KdBagian, req.Pelayanan)
	persistem, _ := su.igdRepository.OnGetPengkajianPersistemIGDRepository(req.KdBagian, req.NoReg, req.Usia, req.Pelayanan)

	mapper := su.rmeMapper.ToMappingPengkajianPersistemRanap(dPemfisik, persistem)
	return mapper
}

func (su *rmeUseCase) OnGetCairanOutPutUseCase(NoReg string) (res []dto.ResponseDcairanOutPut, message string, err error) {
	data, err123 := su.rmeRepository.OnGetCairanOutPutRepository(NoReg)

	if err123 != nil {
		return make([]dto.ResponseDcairanOutPut, 0), "Data gagal didapat", err123
	}

	mapping := su.rmeMapper.ToMappingResponseDCairanPut(data)

	return mapping, "Data ebrhasil didapat", nil
}

func (su *rmeUseCase) OnGetCairanIntakeUseCase(noReg string) (res []dto.ResponseCairanIntake, message string, err error) {
	// ONGET DATA
	data, err12 := su.rmeRepository.OnGetDCairanInTakeRepository(noReg)

	if err12 != nil {
		return make([]dto.ResponseCairanIntake, 0), "Data gagal disimpan", err12
	}

	// LAKUKAN MAPPING DATA
	mapper := su.rmeMapper.ToMappingResponseDCairanOutPut(data)

	return mapper, message, nil
}

func (su *rmeUseCase) OnDeleteCairanOutPutUseCase(ID int) (message string, err error) {
	_, er122 := su.rmeRepository.OnDeleteOutPutRepository(ID)

	if er122 != nil {
		return "Data gagal dihapus", er122
	}
	return "Data berhasil dihapus", nil
}

func (su *rmeUseCase) OnDeleteDCairanIntakeUseCase(ID int) (message string, err error) {
	_, er123 := su.rmeRepository.OnDeleteCairanIntakeRepository(ID)

	if er123 != nil {
		return "Data gagal dihapus", er123
	}

	return "Data berhasil dihapus", nil
}

func (su *rmeUseCase) OnSaveDCairanIntakeUseCase(req dto.ReqOnSaveDCairanIntake, userID string, kdBagian string) (message string, err error) {
	intake := rme.CairanIntake{
		InsertDttm: time.Now(),
		KdBagian:   kdBagian,
		UserID:     userID,
		Noreg:      req.Noreg,
		Minum:      req.Minum,
		Ngt:        req.NGT,
		Infuse:     req.Infuse,
	}

	_, er123 := su.rmeRepository.OnSaveDCairanIntakeRepository(intake)

	if er123 != nil {
		return "Data gagal disimpan", er123
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnSavePemberianTerapiCairanUseCase(req dto.ReqOnSavePemberianTerapiCairan, userID string, kdBagian string) (message string, err error) {
	cairan := rme.DPemberianCairan{
		InsertDttm:      time.Now(),
		UserID:          userID,
		Noreg:           req.Noreg,
		NamaCairan:      req.NamaCairan,
		DosisMakro:      req.DosisMakro,
		DosisMikro:      req.DosisMikro,
		FlsKe:           req.FlsKe,
		ObatDitambahkan: req.Obat,
		Keterangan:      req.Keterangan,
		KDBagian:        kdBagian,
		JumlahInfuse:    req.JumlahInfuse,
		SisaInfuse:      req.SisaInfuse,
	}

	_, ersave := su.rmeRepository.OnSavePemberianTerapiCairanRepository(cairan)

	if ersave != nil {
		return "Data gagal disimpan", ersave
	}

	return "Data berhasil disimpan", nil
}

func (su *rmeUseCase) OnGetReportPemberianTerapiCairanUseCase(noReg string, noRM string) (res dto.ReponseTerapiCairan, message string, err error) {
	data, _ := su.rmeRepository.OnGetPemberianTerapiCairanRepository(noReg)
	user, _ := su.userRepository.OnGetDProfilePasienRepository(noRM)

	mapper := su.rmeMapper.ToMappingDataPemberianCairan(user, data)

	return mapper, message, nil
}

func (su *rmeUseCase) SaveAsuhanKeperawatanBidanUseCase(kdBagian string, req dto.RegSaveAsuhanKeperawatan) (message string, err error) {
	// CARI APAKAH DATA
	// SUDAH ADA PADA DATABASE
	data, errs := su.rmeRepository.CariDataDaskepRepository(kdBagian, req.Noreg)
	times := time.Now()

	if errs != nil {
		su.logging.Info("ERROR DATA")
	}

	if len(data.Noreg) < 1 {
		// JIKA ERROR, DAN DATA KOSONG,.
		// SIMPAN DATA BARU
		su.logging.Info("DATA NOREG  TIDAK DITEMUKAN && TAMBAHKAN DATA BARU")
		if len(req.Intervensi) > 0 {
			for i := 0; i <= len(req.Intervensi)-1; i++ {
				for _, inter := range req.Intervensi[i].Slki {

					daskep := rme.Daskep{
						InsertDttm: times.Format("2006-01-02 15:04:05"),
						Tanggal:    times.Format("2006-01-02"),
						KdBagian:   kdBagian,
						Noreg:      req.Noreg,
						NoUrut:     inter.NoUrut,
						Nilai:      inter.SelectionNumber,
						KodeSdki:   req.Sdki,
						KodeSlki:   inter.Kode,
						KodeSiki:   req.Siki,
						Hasil:      "",
					}

					_, err := su.rmeRepository.InsertDaskepRepository(daskep)

					if err != nil {
						return "data gagal disimpan", errors.New(err.Error())
					}
				}
			}
		}

	}

	if len(data.Noreg) > 1 {
		su.logging.Info("DATA NOREG DITEMUKAN")
		data, err := su.rmeRepository.HapusDaskepRepository(kdBagian, req.Noreg)

		if err != nil {
			return "gagal hapus data", errors.New(err.Error())
		}

		su.logging.Info(data)

		// INSERT DATA
		for i := 0; i <= len(req.Intervensi)-1; i++ {
			for _, inter := range req.Intervensi[i].Slki {

				daskep := rme.Daskep{
					InsertDttm: times.Format("2006-01-02 15:04:05"),
					Tanggal:    times.Format("2006-01-02"),
					KdBagian:   kdBagian,
					Noreg:      req.Noreg,
					NoUrut:     inter.NoUrut,
					Nilai:      inter.SelectionNumber,
					KodeSdki:   req.Sdki,
					KodeSlki:   inter.Kode,
					KodeSiki:   req.Siki,
					Hasil:      "",
				}

				_, err := su.rmeRepository.InsertDaskepRepository(daskep)

				if err != nil {
					return "data gagal disimpan", errors.New(err.Error())
				}
			}
		}

	}

	return "Data berhasil diubah", nil
}

func (su *rmeUseCase) SaveCPPTPasienUsecase(req dto.ReqInsertDcpptPasien, userId string) (message string, res rme.CPPTPasienModel, err error) {

	//  =============================== //
	times := time.Now()

	// ================== JIKA REQUEST ADALAH RAWAT JALAN
	if req.Pelayanan == "rajal" {
		dataBefore, _ := su.rmeRepository.SearchCPPTPasienRepository(req.KdBagian, req.Kelompok, req.Pelayanan, req.Noreg, times.Format("2006-01-02"), userId)

		// JIKA DATA DITEMUKAN
		if len(dataBefore.Noreg) > 1 {
			// LAKUKAN UPDATE DATA
			update, errs := su.rmeRepository.UpdateCPPTPasienRepository(req.KdBagian, req.Kelompok, req.Pelayanan, req.Noreg, times.Format("2006-01-02"), req)

			if errs != nil {
				return "data gagal diubah", res, errors.New(errs.Error())
			}

			return "data berhasil diubah", update, nil
		} else {

			data := rme.CPPTPasienModel{
				InsertDttm:   times.Format("2006-01-02 15:04:05"),
				Tanggal:      times.Format("2006-01-02"),
				InsertUserId: userId,
				UpdDttm:      times.Format("2006-01-02 15:04:05"),
				Kelompok:     req.Kelompok,
				KdBagian:     req.KdBagian,
				InstruksiPpa: req.InstruksiPpa,
				Noreg:        req.Noreg,
				Dpjp:         req.Dpjp,
				Pelayanan:    req.Pelayanan,
				InsertPc:     req.DeviceId,
				Subjektif:    req.Sujektif,
				Objektif:     req.Objektif,
				Asesmen:      req.Asesmen,
				Plan:         req.Plan,
				PpaFingerTtd: userId,
				PpaFingerJam: times.Format("15:04:05"),
			}

			datas, errs := su.rmeRepository.InsertCPPTPasienRepository(data)

			if errs != nil {
				return "data gagal disimpan", datas, errors.New(errs.Error())
			}

			return "Data berhasil disimpan", datas, nil
		}
	}

	if req.Pelayanan == "ranap" {

		data := rme.CPPTPasienModel{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			Tanggal:      times.Format("2006-01-02"),
			InsertUserId: userId,
			UpdDttm:      times.Format("2006-01-02 15:04:05"),
			Kelompok:     req.Kelompok,
			Pelayanan:    req.Pelayanan,
			KdBagian:     req.KdBagian,
			Noreg:        req.Noreg,
			Dpjp:         req.Dpjp,
			InsertPc:     req.DeviceId,
			Subjektif:    req.Sujektif,
			Objektif:     req.Objektif,
			Asesmen:      req.Asesmen,
			Plan:         req.Plan,
			InstruksiPpa: req.InstruksiPpa,
			PpaFingerTtd: userId,
			PpaFingerJam: times.Format("15:04:05"),
		}

		datas, errs := su.rmeRepository.InsertCPPTPasienRepository(data)

		if errs != nil {
			return "data gagal disimpan", datas, errors.New(errs.Error())
		}

		return "Data berhasil disimpan", datas, nil
	}

	return "data tidak dapat diproses, di luar pelayanan", res, errors.New("data gagal di lakukan")
}

func (su *rmeUseCase) UpdateCPPTPasienUsecase(req dto.ReqCPPTUpdate) (message string, res rme.CPPTPasienModel, err error) {
	udpate, err2 := su.rmeRepository.UpdateCPPTByCodeRepository(req.IDCppt, req)

	if err2 != nil {
		return "Data gagal diubah", res, err2
	}

	return "Data berhasil diubah", udpate, nil
}

func (su *rmeUseCase) OnGetMasalahKeperawatanUseCase(noReg string, kdBagian string) (res rme.GetResponseAsuhanKeperawatan, message string, err error) {

	// GET DATA SDKI
	var dataSiki rme.SIKIModel
	var dataSdki rme.SDKIModel

	daskep, err := su.rmeRepository.GetDaskepResponseRepository(noReg, kdBagian)

	if err != nil {
		return res, "data gagal didapat", err
	}

	// GET DATA DASKEP
	if len(daskep) > 0 {
		// GET DATA SIKI
		siki, _ := su.rmeRepository.GetSikiByKodeRepository(daskep[0].Siki)
		dataSiki = siki

		// GET DATA SLKI GET SDKI
		sdki, _ := su.rmeRepository.GetSdkiByKodeRepository(daskep[0].Sdki)
		dataSdki = sdki
	}

	mapper := su.rmeMapper.ToResponseAsuhanKeperawatan(daskep, dataSdki, dataSiki)

	return mapper, "Data berhasil didapat", nil
}

func (su *rmeUseCase) OnSaveSkalaNyeriKeperawatanDewasaUseCase(req dto.ReqAsesmenSkalaNyeri, kdBagian string, userID string) (message string, err error) {
	getSkakala, err1 := su.rmeRepository.OnGetSkalaNyeriKeperawatanDewasaRepository(req.Noreg, kdBagian)

	// =============== //
	times := time.Now()
	// =============== //
	if err1 != nil || getSkakala.Noreg == "" {
		// GET TANGGAL MASUK
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		var nyeri = rme.AsesmenSkalaNyeri{
			InsertDttm:            times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:      "Dewasa",
			InsertUserId:          userID,
			InsertPc:              req.DevicesID,
			TglMasuk:              tanggal.Tanggal[0:10],
			Pelayanan:             req.Pelayanan,
			AseskepSkalaNyeri:     req.SkalaNyeri,
			KdBagian:              kdBagian,
			Noreg:                 req.Noreg,
			AseskepFrekuensiNyeri: req.FrekuensiNyeri,
			AseskepLokasiNyeri:    req.LokasiNyeri,
			AseskepNyeriMenjalar:  req.Menjalar,
			AseskepKualitasNyeri:  req.KualitasNyeri,
		}

		su.rmeRepository.OnSaveSkalaNyeriKeperawatanRepository(nyeri)

		return "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		var nyeri = rme.AsesmenSkalaNyeri{
			InsertDttm:            times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:      "Dewasa",
			InsertUserId:          userID,
			InsertPc:              req.DevicesID,
			AseskepSkalaNyeri:     req.SkalaNyeri,
			KdBagian:              kdBagian,
			Noreg:                 req.Noreg,
			AseskepFrekuensiNyeri: req.FrekuensiNyeri,
			AseskepLokasiNyeri:    req.LokasiNyeri,
			AseskepNyeriMenjalar:  req.Menjalar,
			AseskepKualitasNyeri:  req.KualitasNyeri,
		}

		su.rmeRepository.OnUpdateSkalaNyeriKeperawatanRepository(nyeri, req.Noreg, kdBagian)

		return "Data berhasil diupdate", nil
	}
}

func (su *rmeUseCase) OnSaveKartuObservasiUseCase() (res rme.DKartuObservasi, message string, err error) {
	return res, message, nil
}
