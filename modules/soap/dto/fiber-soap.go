package dto

import (
	"hms_api/modules/his"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
	"mime/multipart"
)

type (
	RequestGetDataV2 struct {
		NoReg string `json:"noReg" binding:"required" bson:"noReg"`
	}

	ReqNoReg struct {
		NoReg string `json:"no_reg" binding:"required" bson:"noReg"`
	}

	InsertOdontogramV2 struct {
		NoReg      string `json:"noReg" validate:"required" bson:"noReg"`
		Number     int    `json:"number" validate:"required" bson:"number"`
		Assets     string `json:"assets" validate:"required" bson:"assets"`
		Keterangan string `json:"keterangan" validate:"required" bson:"keterangan"`
	}

	RequestDeleteOdontogramV2 struct {
		NoReg  string `json:"noReg" validate:"required" bson:"noReg"`
		NoGigi string `json:"noGigi" validate:"required" bson:"noGigi"`
	}

	RequestSaveAssementRawatJalanDokterV2 struct {
		soap.DataMedik            `json:"dataMedik" bson:"dataMedik"`
		soap.IntraOral            `json:"intraOral" bson:"intaOral"`
		soap.AssesPemeriksaanGigi `json:"pemeriksaanGigi" bson:"pemeriksaanGigi"`
		soap.AssesRiwayat         `json:"riwayat" bson:"riwayat"`
		NoReg                     string `json:"noReg" binding:"validate" bson:"noReg"`
		KdBagian                  string `json:"kdBagian" bson:"kdBagian"`
		KeluhanUtama              string `json:"kelUtama" bson:"kelUtama"`
		Pelayanan                 string `json:"pelayanan" bson:"pelayanan"`
	}

	RequestSaveOdontogramV2 struct {
		NoReg      string `json:"noReg" validate:"required" bson:"noReg"`
		NoGigi     string `json:"noGigi" validate:"required" bson:"noGigi"`
		Keterangan string `json:"keterangan" validate:"required" bson:"keterangan"`
	}

	RequestSaveAssementRawatJalanPerawatV2 struct {
		KodeBagian               string `json:"kdBagian" validate:"omitempty" bson:"kdBagian"`
		NoReg                    string `json:"noReg" validate:"required" bson:"noReg"`
		KelUtama                 string `json:"kelUtama" validate:"omitempty" bson:"kelUtama"`
		RiwayatPenyakit          string `json:"riwayatPenyakit" validate:"omitempty" bson:"riwayatPenyakit"`
		RiwayatPenyakitDetail    string `json:"riwayatPenyakitDetail" validate:"omitempty" bson:"RiwayatPenyakitDetail"`
		RiwayatObat              string `json:"riwayatObat" validate:"omitempty" bson:"riwayatObat"`
		RiwayatSaatDirumah       string `json:"riwayatSaatDirumah" validate:"omitempty" bson:"riwayatSaatDirumah"`
		RiwayatObatDetail        string `json:"riwayatObatDetail" validate:"omitempty" bson:"riwayatObatDetail"`
		TekananDarah             string `json:"tekananDarah" validate:"omitempty" bson:"tekananDarah"`
		Nadi                     string `json:"nadi" validate:"omitempty" bson:"nadi"`
		Suhu                     string `json:"suhu" validate:"omitempty" bson:"suhu"`
		Pernapasan               string `json:"pernapasan" validate:"omitempty" bson:"pernapasan"`
		BeratBadan               string `json:"beratBadan" validate:"omitempty" bson:"beratBadan"`
		TinggiBadan              string `json:"tinggiBadan" validate:"omitempty" bson:"tinggiBadan"`
		SkriningNyeri            string `json:"skriningNyeri" validate:"omitempty" bson:"skriningNyeri"`
		Psikologis               string `json:"psikologis" validate:"omitempty" bson:"psikologis"`
		PsikologisDetail         string `json:"psikologisDetail" validate:"omitempty" bson:"psikologisDetail"`
		Fungsional               string `json:"fungsional" validate:"omitempty" bson:"fungsional"`
		FungsionalDetail         string `json:"fungsionalDetail" validate:"omitempty" bson:"fungsionalDetail"`
		AseskepRj1               string `json:"aseskepRj1" validate:"omitempty" bson:"AseskepRj1"`
		AseskepRj2               string `json:"aseskepRj2" validate:"omitempty" bson:"AseskepRj2"`
		AseskepHasilKajiRj       string `json:"aseskepHasilKajiRj" validate:"omitempty" bson:"aseskepHasilKajiRj"`
		AseskepHslKajiRjTind     string `json:"aseskepHslKajiRjTind" validate:"omitempty" bson:"aseskepHasilKajiRjTindakan"`
		AseskepNyeri             string `json:"aseskepNyeri" validate:"omitempty" bson:"aseskepNyeri"`
		AseskepHasilKajiRjDetail string `json:"aseskepHasilKajiRjDetail" validate:"omitempty" bson:"aseskepHasilKajiRjDetail"`
		AseskepPulang1           string `json:"aseskepPulang1" validate:"omitempty" bson:"aseskepPulang1"`
		AseskepPulang1Detail     string `json:"aseskepPulang1Detail" validate:"omitempty" bson:"aseskepPulang1Detail"`
		AseskepPulang2           string `json:"aseskepPulang2" validate:"omitempty" bson:"aseskepPulang2"`
		AseskepPulang2Detail     string `json:"aseskepPulang2Detail" validate:"omitempty" bson:"aseskepPulang2Detail"`
		AseskepPulang3           string `json:"aseskepPulang3" validate:"omitempty" bson:"aseskepPulang3"`
		AseskepPulang3Detail     string `json:"aseskepPulang3Detail" validate:"omitempty" bson:"aseskepPulang3Detail"`
		MasalahKeperawatan       string `json:"masalahKeperawatan" validate:"omitempty" bson:"masalahKeperawatan"`
		RencanaKeperawatan       string `json:"rencanaKeperawatan" validate:"omitempty" bson:"rencanaKeperawatan"`
	}

	RequestGetSkriningV2 struct {
		NoReg string `json:"noReg" validate:"required" bson:"noReg"`
	}

	RequestSaveSkriningV2 struct {
		KodeBagian string `json:"kdBagian" validate:"required" bson:"kdBagian"`
		NoReg      string `json:"noReg" validate:"required" bson:"noReg"`
		K1         string `json:"k1" validate:"omitempty" bson:"k1"`
		K2         string `json:"k2" validate:"omitempty" bson:"k2"`
		K3         string `json:"k3" validate:"omitempty" bson:"k3"`
		K4         string `json:"k4" validate:"omitempty" bson:"k4"`
		K5         string `json:"k5" validate:"omitempty" bson:"k5"`
		K6         string `json:"k6" validate:"omitempty" bson:"k6"`
		KF1        string `json:"KF1" validate:"omitempty" bson:"KF1"`
		KF2        string `json:"KF2" validate:"omitempty" bson:"KF2"`
		KF3        string `json:"KF3" validate:"omitempty" bson:"KF3"`
		KF4        string `json:"KF4" validate:"omitempty" bson:"KF4"`
		B1         string `json:"B1" validate:"omitempty" bson:"B1"`
		B2         string `json:"B2" validate:"omitempty" bson:"B2"`
		RJ         string `json:"RJ" validate:"required" bson:"RJ"`
		IV1        string `json:"IV1" validate:"omitempty" bson:"IV1"`
		IV2        string `json:"IV2" validate:"omitempty" bson:"IV2"`
		IV3        string `json:"IV3" validate:"omitempty" bson:"IV3"`
		IV4        string `json:"IV4" validate:"omitempty" bson:"IV4"`
		User       string `json:"user" bson:"user"`
		Tanggal    string `json:"tanggal" bson:"tanggal"`
		Jam        string `json:"jam" bson:"jam"`
	}
	RequestSaveAnamnesaV2 struct {
		NoReg                      string `json:"noReg" validate:"required" bson:"noReg"`
		AsesmedKeluhUtama          string `json:"keluhUtama" validate:"omitempty" bson:"keluhUtama"`
		AsesmedJenpel              string `json:"jenisPelayanan"  validate:"omitempty"  bson:"jenisPelayanan"`
		AsesmenRwtSkrg             string `json:"rwtSekarang"  validate:"omitempty"  bson:"rwtSekarang"`
		AsesmenRwtDulu             string `json:"rwtDulu"  validate:"omitempty"  bson:"rwtDulu"`
		AsesmenRwtObat             string `json:"rwtObat"  validate:"omitempty"  bson:"rwtObat"`
		AsesmenRwtPenyKlrg         string `json:"rwtPenyKeluarga"  validate:"omitempty"  bson:"rwtPenyKeluarga"`
		AsesmenRwtPenyAlergi       string `json:"rwtPenyAlergi"  validate:"omitempty"  bson:"rwtPenyAlergi"`
		AsesmenRwtPenyAlergiDetail string `json:"rwtPenyAlergiDetail"  validate:"omitempty"  bson:"rwtPenyAlergiDetail"`
		AsesmenKhusPemGigi1        string `json:"khusPemGigi1"  validate:"omitempty"  bson:"khusPemGigi1"`
		AsesmenKhusPemGigi2        string `json:"khusPemGigi2"  validate:"omitempty"  bson:"khusPemGigi2"`
		AsesmenKhusPemGigi3        string `json:"khusPemGigi3"  validate:"omitempty"  bson:"khusPemGigi3"`
		AsesmenKhusPemGigi4        string `json:"khusPemGigi4"  validate:"omitempty"  bson:"khusPemGigi4"`
		AsesmenKhusPemGigi5        string `json:"khusPemGigi5"  validate:"omitempty"  bson:"khusPemGigi5"`
		AsesmenKhusPemGigi5Detail  string `json:"khusPemGigi5Detail"  validate:"omitempty"  bson:"khusPemGigi5Detail"`
	}

	RequestSaveAnatomiV2 struct {
		Nama       string                `form:"nama" bson:"nama" validate:"required"`
		Norm       string                `form:"norm" bson:"norm" validate:"required"`
		Keterangan string                `form:"keterangan" bson:"keterangan"  validate:"required"`
		File       *multipart.FileHeader `form:"imageUrl" bson:"imageUrl"`
	}

	InsertDiagnosaV2 struct {
		NoReg      string `json:"noReg" validate:"required" bson:"noReg"`
		Number     int    `json:"number" validate:"required" bson:"number"`
		Assets     string `json:"assets" validate:"required" bson:"assets"`
		Keterangan string `json:"keterangan" validate:"required" bson:"keterangan"`
	}

	ReqKebEdukasiV2 struct {
		Edukasi         KebEdukasiRes      `json:"edukasi"`
		Hambatan        HabatanPasienRes   `json:"hambatan"`
		Hasil           HasilRes           `json:"hasil"`
		IntervensiRes   IntervensiRes      `json:"intervensi"`
		KempBelajar     KempBelajarRes     `json:"kemampuanBelajar"`
		MotivasiBelajar MotivasiBelajarRes `json:"motivasiBelajar"`
		NilaiPasien     PilihanPasienRes   `json:"nilaiPasien"`
		RencanaEdukasi  string             `json:"rencanaEdukasi"`
		AsseEduJam      string             `json:"aseseduJam"`
		AsseEduTgl      string             `json:"aseseduTgl"`
		AsseEduUser     string             `json:"aseseduUser"`
		NoReg           string             `json:"noReg" validate:"required"`
		Tujuan          TujuanRes          `json:"tujuan"`
	}

	ReqNoregV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqReportAsesmenDokterDewasa struct {
		NoRM    string `json:"no_rm" validate:"required"`
		NoReg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
		// Person  string `json:"person" validate:"required"`
	}

	ResposeReportAsesmenDokterAntonio struct {
		RiwayatPenyakit  []rme.KeluhanUtama               `json:"riwayat_terdahulu"`
		RiwayatKeluarga  []rme.DAlergi                    `json:"riwayat_keluarga"`
		KeluhanUtama     rme.ResponseAsesemenDokterIGD    `json:"asesmen"`
		PemeriksaanFisik string                           `json:"pem_fisik"`
		Karyawan         user.Karyawan                    `json:"karyawan"`
		Tindakan         []soap.TindakanResponse          `json:"tindakan"`
		HasilLabor       []his.ResHasilLaborTableLama     `json:"labor"`
		Radiologi        []his.RegHasilRadiologiTabelLama `json:"radiologi"`
	}

	OnDeleteTindakanICD9 struct {
		Noreg string `json:"no_reg" validate:"required"`
		Index string `json:"index" validate:"required"`
	}

	ReqInfoKeluh struct {
		Noreg string `json:"no_reg" validate:"required"`
		NoRm  string `json:"no_rm" validate:"required"`
	}

	ReqAsesmenIGD struct {
		Noreg   string `json:"no_reg" validate:"required"`
		NoRm    string `json:"no_rm" validate:"required"`
		Tanggal string `json:"tanggal" validate:"required"`
		Person  string `json:"person"`
	}

	ReqOnSaveAsesmenIGD struct {
		Noreg                   string `json:"no_reg" validate:"required"`
		NoRm                    string `json:"no_rm" validate:"required"`
		Tanggal                 string `json:"tanggal" validate:"required"`
		Person                  string `json:"person"`
		DevicesID               string `json:"device_id"`
		Pelayanan               string `json:"pelayanan"`
		PerolehanInfo           string `json:"perolehan_info"`
		PerolehanInfoDetail     string `json:"perolehan_info_detail"`
		CaraMasuk               string `json:"cara_masuk"`
		AsalMasuk               string `json:"asal_masuk"`
		FungsiAktivitas         string `json:"fungsi_aktivitas"`
		KeluhanUtama            string `json:"keluhan_utama"`
		RiwayatPenyakitSekarang string `json:"penyakit_sekarang"`
		RiwayatPenyakitDahulu   string `json:"penyakit_dahulu"`
		ReaksiAlergi            string `json:"reaksi_alergi"`
	}

	ReqResikoJatuhGoUpGoTest struct {
		Noreg string `json:"no_reg" validate:"required"`
		NoRm  string `json:"no_rm" validate:"required"`
	}

	ReqSaveTriaseRiwayatAlergi struct {
		Noreg              string `json:"no_reg" validate:"required"`
		KeluhanUtama       string `json:"keluhan_utama" validate:"omitempty"`
		StatusAlergi       string `json:"status_alergi" validate:"omitempty"`
		StatusAlergiDetail string `json:"status_alergi_detail" validate:"omitempty"`
		KeteranganPerson   string `json:"person" validate:"required"`
		InsertUserId       string `json:"user_id" validate:"required"`
		DeviceID           string `json:"device_id" validate:"required"`
		Pelayanan          string `json:"pelayanan" validate:"required"`
	}

	RegSaveKeluhanUtamaIGD struct {
		Noreg            string `json:"no_reg" validate:"required"`
		KeluhanUtama     string `json:"keluhan_utama" validate:"omitempty"`
		KeteranganPerson string `json:"person" validate:"required"`
		InsertUserId     string `json:"user_id" validate:"required"`
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
	}

	ReqInputSingleDiagnosaV2 struct {
		Noreg            string `json:"noReg" validate:"required"`
		Code             string `json:"code" validate:"required"`
		Type             string `json:"type" validate:"required"`
		Table            string `json:"table" validate:"required"`
		KeteranganPerson string `json:"person" validate:"required"`
		InsertUserId     string `json:"user_id" validate:"required"`
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
	}

	RegInputTindakan struct {
		Noreg            string `json:"noReg" validate:"required"`
		Code             string `json:"code" validate:"required"`
		Index            string `json:"index" validate:"required"`
		KeteranganPerson string `json:"person" validate:"required"`
		InsertUserId     string `json:"user_id" validate:"required"`
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
	}

	ReqDiagnosaBanding struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	ReqDeleteDiagnosaV2 struct {
		Noreg string `json:"no_reg" validate:"required"`
		Table string `json:"table" validate:"required"`
	}

	ReqOnGetPengkajianAnak struct {
		Person  string `json:"person" validate:"required"`
		NoReg   string `json:"no_reg" validate:"required"`
		Tanggal string `json:"tanggal" validate:"omitempty"`
	}

	ReqDataPasienV2 struct {
		Noreg    string `json:"no_reg" validate:"required"`
		KdBagian string `json:"kd_bagian" validate:"required"`
	}

	// Request Masalah Medis
	ReqInformasiMedisV2 struct {
		Noreg            string `json:"no_reg" validate:"required"`
		KdBagian         string `json:"kd_bagian" validate:"required"`
		MasalahMedis     string `json:"masalah_medis" validate:"omitempty"`
		Terapi           string `json:"terapi" validate:"omitempty"`
		PemeriksaanFisik string `json:"pemeriksaan_fisik" validate:"omitempty"`
		Anjuran          string `json:"anjuran" validate:"omitempty"`
	}
	//

	// SAVE ANAMNESA
	ReqSaveAsesmedAnamnesaV2 struct {
		Noreg                     string `json:"noReg" validate:"required"`
		AsesmedKeluhUtama         string `json:"kelUtama" validate:"omitempty"`
		AsesmedRwytSkrg           string `json:"riwayatSekarang" validate:"omitempty"`
		AsesmedRwytPenyKlrg       string `json:"penyakitKeluarga" validate:"omitempty"`
		AsesmedRwytAlergi         string `json:"riwayatAlergi" validate:"omitempty"`
		AsesmedRwytAlergiDetail   string `json:"riwayatAlergiDetail" validate:"omitempty"`
		AsesmedJenpel             string `json:"jenisPelayanan" validate:"omitempty"`
		AsesmedKhusPemGigi1       string `json:"gigi1" validate:"omitempty"`
		AsesmedKhusPemGigi2       string `json:"gigi2" validate:"omitempty"`
		AsesmedKhusPemGigi3       string `json:"gigi3" validate:"omitempty"`
		AsesmedKhusPemGigi4       string `json:"gigi4" validate:"omitempty"`
		AsesmedKhusPemGigi5       string `json:"gigi5" validate:"omitempty"`
		AsesmedKhusPemGigi5Detail string `json:"gigi5Detail" validate:"omitempty"`
	}

	//  ===== ANAMNESA IGD ======
	AnamnesaIGDV2 struct {
		NoReg               string `json:"no_reg" validate:"required"`
		AsesmedKeluhUtama   string `json:"keluhan_utama"`
		AsesmedKesadaran    string `json:"kesadaran"`
		AsesmendTelaah      string `json:"telaah"`
		AsesmendMslhMedis   string `json:"mslh_medis"`
		AsesmedRwytSkrg     string `json:"rwyt_skrg"`
		AsesmedRwytDulu     string `json:"rwyt_dulu"`
		AsesmedRwytObat     string `json:"rwyt_obat"`
		AsesmedRwytPenyKlrg string `json:"rwyt_klrg"`
		AsesmedJenpel       string `json:"jenpel"`
		KeteranganPerson    string `json:"person" validate:"required"`
		InsertUserId        string `json:"user_id" validate:"required"`
		DeviceID            string `json:"device_id" validate:"required"`
		Pelayanan           string `json:"pelayanan" validate:"required"`
	}

	ReqDataMedikV2 struct {
		Noreg                    string `json:"noReg" validate:"required"`
		AsesmedGolDarah          string `json:"golDarah" validate:"omitempty"`
		AsesmedTd                string `json:"tekananDarah" validate:"omitempty"`
		AsesmedTdDetail          string `json:"tekananDarahDetail" validate:"omitempty"`
		AsesmedPenyJantung       string `json:"penyJantung" validate:"omitempty"`
		AsesmedPenyJantungDetail string `json:"penyJantungDetail" validate:"omitempty"`
		AsesmedDiabet            string `json:"diabet" validate:"omitempty"`
		AsesmedDiabetDetail      string `json:"diabetDetail" validate:"omitempty"`
		AsesmedHaemop            string `json:"haemop" validate:"omitempty"`
		AsesmedHaemopDetail      string `json:"haemopDetail" validate:"omitempty"`
		AsesmedHepat             string `json:"hepat" validate:"omitempty"`
		AsesmedHepatDetail       string `json:"hepatDetail" validate:"omitempty"`
		AsesmedPenyLain          string `json:"penyLain" validate:"omitempty"`
		AsesmedPenyLainDetail    string `json:"penyLainDetail" validate:"omitempty"`
		AsesmedAlergiObat        string `json:"alergiObat" validate:"omitempty"`
		AsesmedAlergiObatDetail  string `json:"alergiObatDetail" validate:"omitempty"`
		AsesmedAlergiMknan       string `json:"alergiMakanan" validate:"omitempty"`
		AsesmedAlergiMknanDetail string `json:"alergiMakananDetail" validate:"omitempty"`
		AsesmedKebiasaanBuruk    string `json:"kebiasaanBuruk" validate:"omitempty"`
	}

	ReqDataIntraOralV2 struct {
		Noreg                        string `json:"noReg"  validate:"required"`
		AsesmedKhusOkAnterior        string `json:"anterior" validate:"omitempty"`
		AsesmedKhusOkPosterior       string `json:"posterior" validate:"omitempty"`
		AsesmedKhusOkMolar           string `json:"molar" validate:"omitempty"`
		AsesmedKhusPalatum           string `json:"palatum" validate:"omitempty"`
		AsesmedKhusTorusP            string `json:"torusP" validate:"omitempty"`
		AsesmedKhusTorusM            string `json:"torusM" validate:"omitempty"`
		AsesmedKhusTorusMDetail      string `json:"torusMDetail" validate:"omitempty"`
		AsesmedKhusSuperTeeth        string `json:"superTeeth" validate:"omitempty"`
		AsesmedKhusSuperTeethDetail  string `json:"superTeethDetail" validate:"omitempty"`
		AsesmedKhusDiastema          string `json:"diastema" validate:"omitempty"`
		AsesmedKhusDiastemaDetail    string `json:"diastemaDetail" validate:"omitempty"`
		AsesmedKhusGigiAnomali       string `json:"gigiAnomali" validate:"omitempty"`
		AsesmedKhusGigiAnomaliDetail string `json:"gigiAnomaliDetail" validate:"omitempty"`
		AsesmedKhusOralLain          string `json:"khusOralLain" validate:"omitempty"`
	}

	ReqPascaOperasiV1 struct {
		Noreg                       string `json:"no_reg" validate:"required"`
		KdBagian                    string `json:"kd_bagian" validate:"required"`
		ObsanslokKhusPostKeadUmum   string `json:"keadaanUmum" validate:"omitempty"`
		ObsanslokKhusPostKeluhUtama string `json:"keluhUmum" validate:"omitempty"`
		ObsanslokKhusPostTd         string `json:"tekananDarah" validate:"omitempty"`
		ObsanslokKhusPostNadi       string `json:"nadi" validate:"omitempty"`
		ObsanslokKhusPostSuhu       string `json:"suhu" validate:"omitempty"`
		ObsanslokKhusPostRr         string `json:"pernapasan" validate:"omitempty"`
	}

	RegTriaseV2 struct {
		NoReg                   string `json:"noreg" validate:"required"`
		IgdTriaseKeluh          string `json:"keluhan" validate:"omitempty"`
		IgdTriaseAlergi         string `json:"alergi" validate:"omitempty"`
		IgdTriaseAlergiDetail   string `json:"alergi_detail" validate:"omitempty"`
		IgdTriaseNafas          string `json:"nafas" validate:"omitempty"`
		IgdTriaseTd             string `json:"tekanan_darah" validate:"omitempty"`
		IgdTriaseRr             string `json:"pernapasan" validate:"omitempty"`
		IgdTriasePupil          string `json:"pupil" validate:"omitempty"`
		IgdTriaseNadi           string `json:"nadi" validate:"omitempty"`
		IgdTriaseSpo2           string `json:"spo" validate:"omitempty"`
		IgdTriaseSuhu           string `json:"suhu" validate:"omitempty"`
		IgdTriaseAkral          string `json:"akral" validate:"omitempty"`
		IgdTriaseCahaya         string `json:"reflek_cahaya" validate:"omitempty"`
		IgdTriaseGcsE           string `json:"gcs_e" validate:"omitempty"`
		IgdTriaseGcsV           string `json:"gcs_v" validate:"omitempty"`
		IgdTriaseGcsM           string `json:"gcs_m" validate:"omitempty"`
		IgdTriaseGangguan       string `json:"gangguan" validate:"omitempty"`
		IgdTriaseGangguanDetail string `json:"gangguan_detail" validate:"omitempty"`
		IgdTriaseSkalaNyeri     int    `json:"nyeri" validate:"omitempty"`
		IgdTriaseSkalaNyeriP    string `json:"nyeri_p" validate:"omitempty"`
		IgdTriaseSkalaNyeriQ    string `json:"nyeri_q" validate:"omitempty"`
		IgdTriaseSkalaNyeriR    string `json:"nyeri_r" validate:"omitempty"`
		IgdTriaseSkalaNyeriS    string `json:"nyeri_s" validate:"omitempty"`
		IgdTriaseSkalaNyeriT    string `json:"nyeri_t" validate:"omitempty"`
		IgdTriaseFlaccWajah     int    `json:"wajah" validate:"omitempty"`
		IgdTriaseFlaccKaki      int    `json:"kaki" validate:"omitempty"`
		IgdTriaseFlaccAktifitas int    `json:"aktifitas" validate:"omitempty"`
		IgdTriaseFlaccMenangis  int    `json:"menangis" validate:"omitempty"`
		IgdTriaseFlaccBersuara  int    `json:"bersuara" validate:"omitempty"`
		IgdTriaseFlaccTotal     int    `json:"total" validate:"omitempty"`
		IgdTriaseSkalaTriase    int    `json:"triase" validate:"omitempty"`
		IgdTriaseFingerUser     string `json:"user" validate:"omitempty"`
		IgdTriaseFingerTgl      string `json:"tanggal" validate:"omitempty"`
		IgdTriaseFingerJam      string `json:"jam" validate:"omitempty"`
	}

	ReqPascaOperasiV2 struct {
		Noreg                       string `json:"no_reg" validate:"required"`
		KdBagian                    string `json:"kd_bagian" validate:"required"`
		ObsanslokKhusPostKeadUmum   string `json:"keadaanUmum" validate:"omitempty"`
		ObsanslokKhusPostKeluhUtama string `json:"keluhUmum" validate:"omitempty"`
		ObsanslokKhusPostTd         string `json:"tekananDarah" validate:"omitempty"`
		ObsanslokKhusPostNadi       string `json:"nadi" validate:"omitempty"`
		ObsanslokKhusPostSuhu       string `json:"suhu" validate:"omitempty"`
		ObsanslokKhusPostRr         string `json:"pernapasan" validate:"omitempty"`
	}

	RequestSavePemeriksaanFisikV2 struct {
		Noreg                          string `json:"noreg" vaidate:"required" bson:"noreg"`
		AsesmedPemfisKepala            string `json:"kepala" vaidate:"omitempty" bson:"kepala"`
		AsesmedPemfisKepalaDetail      string `json:"kepala_detail" vaidate:"omitempty" bson:"kepala_detail"`
		AsesmedPemfisLeher             string `json:"leher" vaidate:"omitempty" bson:"leher"`
		AsesmedPemfisLeherDetail       string `json:"leher_detail" vaidate:"omitempty" bson:"leher_detail"`
		AsesmedPemfisDada              string `json:"data" vaidate:"omitempty" bson:"data"`
		AsesmedPemfisDadaDetail        string `json:"data_detail" vaidate:"omitempty" bson:"data_detail"`
		AsesmedPemfisAbdomen           string `json:"abdomen" vaidate:"omitempty" bson:"abdomen"`
		AsesmedPemfisAbdomenDetail     string `json:"abdomen_detail" vaidate:"omitempty" bson:"abdomen_detail"`
		AsesmedPemfisPunggung          string `json:"punggung" vaidate:"omitempty" bson:"punggung"`
		AsesmedPemfisPunggungDetail    string `json:"punggung_detail" vaidate:"omitempty" bson:"punggung_detail"`
		AsesmedPemfisGenetalia         string `json:"genetalia" vaidate:"omitempty" bson:"genetalia"`
		AsesmedPemfisGenetaliaDetail   string `json:"genetalia_detail" vaidate:"omitempty" bson:"genetalia_detail"`
		AsesmedPemfisEkstremitas       string `json:"ekstremitas" vaidate:"omitempty" bson:"ekstremitas"`
		AsesmedPemfisEkstremitasDetail string `json:"ekstremitas_detail" vaidate:"omitempty" bson:"ekstremitas_detail"`
		AsesmedPemfisLain              string `json:"lain" vaidate:"omitempty" bson:"lain"`
		// ===========  ============= //
		KeteranganPerson string `json:"person" validate:"required"`
		InsertUserId     string `json:"user_id" validate:"required"`
		DeviceID         string `json:"device_id" validate:"required"`
		Pelayanan        string `json:"pelayanan" validate:"required"`
	}

	RequestPilihPemeriksaanLaborV2 struct {
		NameGrup string `json:"name_grup"  validate:"required"  bson:"name_grup"`
	}

	RequestRencanaTindakLanjutV2 struct {
		NoReg                string `json:"noreg"  validate:"required"  bson:"noreg"`
		RencanaAnjuranTerapi string `json:"anjuran"  validate:"omitempty"  bson:"anjuran"`
		KonsulKe             string `json:"konsul_ke"  validate:"omitempty"  bson:"konsul_ke"`
		AlasanOpname         string `json:"alasan_opname"  validate:"omitempty"  bson:"alasan_opname"`
		AlasanKonsul         string `json:"alasan_konsul"  validate:"omitempty"  bson:"alasan_opname"`
		Prognosis            string `json:"prognosis"  validate:"omitempty"  bson:"prognosis"`
		KeteranganPerson     string `json:"person" validate:"required"`
		InsertUserId         string `json:"user_id" validate:"required"`
		DeviceID             string `json:"device_id" validate:"required"`
		Pelayanan            string `json:"pelayanan" validate:"required"`
	}

	ReqTindakLanjutV2 struct {
		Noreg string `json:"noReg" validate:"required"`
	}

	// REQUEST ASESMEN AWAL KEPERAWATAN BIDAN DI RUANGAN IGD
	ReqAsesmenInformasiKeluhanIGD struct {
		DeviceID  string `json:"device_id" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`
		// ==============================
		AseskepPerolehanInfo       string `json:"info" validate:"omitempty"`
		AseskepPerolehanInfoDetail string `json:"info_detail" validate:"omitempty"`
		AseskepCaraMasuk           string `json:"cara_masuk" validate:"omitempty"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail" validate:"omitempty"`
		AseskepAsalMasuk           string `json:"asal_masuk" validate:"omitempty"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail" validate:"omitempty"`
		Aseskepbb                  string `json:"berat_badan" validate:"omitempty"`
		AseskepTb                  string `json:"tinggi_badan" validate:"omitempty"`
		AseskepRwytPnykt           string `json:"riwayat_penyakit" validate:"omitempty"`
		AseskepRwytObat            string `json:"riwayat_obat" validate:"omitempty"`
		AseskepAsesFungsional      string `json:"fungsional" validate:"omitempty"`
		AseskepRj1                 string `json:"resiko_jatuh1" validate:"omitempty"`
		AseskepRj2                 string `json:"resiko_jatuh2" validate:"omitempty"`
		AseskepHslKajiRj           string `json:"hasil_kaji" validate:"omitempty"`
	}

	ReqSkriningDekubitusIGD struct {
		DeviceID      string `json:"device_id" validate:"required"`
		Pelayanan     string `json:"pelayanan" validate:"required"`
		Noreg         string `json:"noreg" validate:"required"`
		Person        string `json:"person" validate:"required"`
		Dekubitus1    string `json:"dekubitus1" validate:"omitempty"`
		Dekubitus2    string `json:"dekubitus2" validate:"omitempty"`
		Dekubitus3    string `json:"dekubitus3" validate:"omitempty"`
		Dekubitus4    string `json:"dekubitus4" validate:"omitempty"`
		DekubitusAnak string `json:"dekubitus_anak" validate:"omitempty"`
	}

	// ReqSave Asesmen
	// Keperawatan Bidan
	ReqSaveAsesmenKeperawatanBidanV2 struct {
		Noreg                         string `json:"noreg" validate:"required"`
		AseskepPerolehanInfo          string `json:"info" validate:"omitempty"`
		AseskepCaraMasuk              string `json:"cara_masuk" validate:"omitempty"`
		AseskepCaraMasukDetail        string `json:"cara_masuk_detail" validate:"omitempty"`
		AseskepAsalMasuk              string `json:"asal_masuk" validate:"omitempty"`
		AseskepAsalMasukDetail        string `json:"asal_masuk_detail" validate:"omitempty"`
		AseskepBb                     string `json:"bb" validate:"omitempty"`
		AseskepTb                     string `json:"tb" validate:"omitempty"`
		AseskepRwytPnykt              string `json:"rwt_penyakit" validate:"omitempty"`
		AseskepRwytObatDetail         string `json:"obat_detail" validate:"omitempty"`
		AseskepAsesFungsional         string `json:"fungsional" validate:"omitempty"`
		AseskepRj1                    string `json:"rj1" validate:"omitempty"`
		AseskepRj2                    string `json:"rj2" validate:"omitempty"`
		AseskepHslKajiRj              string `json:"kaji_rj" validate:"omitempty"`
		AseskepHslKajiRjTind          string `json:"kaji_rj_tindakan" validate:"omitempty"`
		AseskepSkalaNyeri             int    `json:"skala_nyeri" validate:"omitempty"`
		AseskepFrekuensiNyeri         string `json:"frekuensi_nyeri" validate:"omitempty"`
		AseskepLamaNyeri              string `json:"lama_nyeri" validate:"omitempty"`
		AseskepNyeriMenjalar          string `json:"nyeri_menjalar" validate:"omitempty"`
		AseskepNyeriMenjalarDetail    string `json:"menjalar_detail" validate:"omitempty"`
		AseskepKualitasNyeri          string `json:"kualitas_nyeri" validate:"omitempty"`
		AseskepNyeriPemicu            string `json:"nyeri_pemicu" validate:"omitempty"`
		AseskepNyeriPengurang         string `json:"nyeri_pengurang" validate:"omitempty"`
		AseskepKehamilan              string `json:"kehamilan" validate:"omitempty"`
		AseskepKehamilanGravida       string `json:"kehamilan_gravida" validate:"omitempty"`
		AseskepKehamilanPara          string `json:"kehamilan_para" validate:"omitempty"`
		AseskepKehamilanAbortus       string `json:"kehamilan_abortus" validate:"omitempty"`
		AseskepKehamilanHpht          string `json:"kehamilan_hpht" validate:"omitempty"`
		AseskepKehamilanTtp           string `json:"kehamilan_ttp" validate:"omitempty"`
		AseskepKehamilanLeopold1      string `json:"kehamilan_leopol1" validate:"omitempty"`
		AseskepKehamilanLeopold2      string `json:"kehamilan_leopol2" validate:"omitempty"`
		AseskepKehamilanLeopold3      string `json:"kehamilan_leopol3" validate:"omitempty"`
		AseskepKehamilanLeopold4      string `json:"kehamilan_leopol4" validate:"omitempty"`
		AseskepKehamilanDjj           string `json:"kehamilan_djj" validate:"omitempty"`
		AseskepKehamilanVt            string `json:"kehamilan_vt" validate:"omitempty"`
		AseskepDekubitus1             string `json:"dekubitus1" validate:"omitempty"`
		AseskepDekubitus2             string `json:"dekubitus2" validate:"omitempty"`
		AseskepDekubitus3             string `json:"dekubitus3" validate:"omitempty"`
		AseskepDekubitus4             string `json:"dekubitus4" validate:"omitempty"`
		AseskepDekubitusAnak          string `json:"dekubitus_anak" validate:"omitempty"`
		AseskepPulangKondisi          string `json:"pulang_kondisi" validate:"omitempty"`
		AseskepPulangTransportasi     string `json:"pulang_transportasi" validate:"omitempty"`
		AseskepPulangPendidikan       string `json:"pendidikan" validate:"omitempty"`
		AseskepPulangPendidikanDetail string `json:"pendidikan_detail" validate:"omitempty"`
	}

	RequestKehamilanIGD struct {
		DeviceID  string `json:"device_id" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`

		Kehamilan         string `json:"kehamilan" validate:"omitempty"`
		KehamilanGravida  string `json:"gravida" validate:"omitempty"`
		KehamilanPara     string `json:"para" validate:"omitempty"`
		KehamilanAbortus  string `json:"abortus" validate:"omitempty"`
		KehamilanHpht     string `json:"hpht" validate:"omitempty"`
		KehamilanTtp      string `json:"ttp" validate:"omitempty"`
		KehamilanLeopold1 string `json:"leopold1" validate:"omitempty"`
		KehamilanLeopold2 string `json:"leopold2" validate:"omitempty"`
		KehamilanLeopold3 string `json:"leopold3" validate:"omitempty"`
		KehamilanLeopold4 string `json:"leopold4" validate:"omitempty"`
		KehamilanDjj      string `json:"ddj" validate:"omitempty"`
		KehamilanVt       string `json:"vt" validate:"omitempty"`
	}

	ResRiwayatKehamilanIGD struct {
		Kehamilan         bool   `json:"kehamilan"`
		KehamilanGravida  string `json:"gravida"`
		KehamilanPara     string `json:"para"`
		KehamilanAbortus  string `json:"abortus"`
		KehamilanHpht     string `json:"hpht"`
		KehamilanTtp      string `json:"ttp"`
		KehamilanLeopold1 string `json:"leopold1"`
		KehamilanLeopold2 string `json:"leopold2"`
		KehamilanLeopold3 string `json:"leopold3"`
		KehamilanLeopold4 string `json:"leopold4"`
		KehamilanDjj      string `json:"ddj"`
		KehamilanVt       string `json:"vt"`
	}

	// ===

	RequestSkriningNyeriIGD struct {
		DeviceID  string `json:"device_id" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`

		SkalaNyeri          int    `json:"skala_nyeri" validate:"omitempty"`
		FrekuensiNyeri      string `json:"frekuensi_nyeri" validate:"omitempty"`
		LamaNyeri           string `json:"lama_nyeri" validate:"omitempty"`
		NyeriMenjalar       string `json:"nyeri_menjalar" validate:"omitempty"`
		NyeriMenjalarDetail string `json:"manjalar_detail" validate:"omitempty"`
		Kualitasnyeri       string `json:"kualitas_nyeri" validate:"omitempty"`
		NyeriPemicu         string `json:"nyeri_pemicu" validate:"omitempty"`
		NyeriPengurang      string `json:"nyeri_pengurang" validate:"omitempty"`
	}

	// ======================== TO MAPPER SKRINING NYERI =========== //
	ResSkriningNyeriIGD struct {
		SkalaNyeri          int    `json:"skala_nyeri" validate:"omitempty"`
		FrekuensiNyeri      string `json:"frekuensi_nyeri" validate:"omitempty"`
		LamaNyeri           string `json:"lama_nyeri" validate:"omitempty"`
		NyeriMenjalar       string `json:"nyeri_menjalar" validate:"omitempty"`
		NyeriMenjalarDetail string `json:"manjalar_detail" validate:"omitempty"`
		Kualitasnyeri       string `json:"kualitas_nyeri" validate:"omitempty"`
		NyeriPemicu         string `json:"nyeri_pemicu" validate:"omitempty"`
		NyeriPengurang      string `json:"nyeri_pengurang" validate:"omitempty"`
	}

	// ======================= REQUEST TINDAK LANJUUT
	RequestInputTindakLanjutIGD struct {
		DeviceID             string `json:"device_id" validate:"required"`
		Pelayanan            string `json:"pelayanan" validate:"required"`
		Noreg                string `json:"noreg" validate:"required"`
		Person               string `json:"person" validate:"required"`
		AseskepPulang1       string `json:"pulang1" validate:"omitempty"`
		AseskepPulang1Detail string `json:"pulang1_detail" validate:"omitempty"`
		AseskepPulang2       string `json:"pulang2" validate:"omitempty"`
		AseskepPulang2Detail string `json:"pulang2_detail" validate:"omitempty"`
		AseskepPulang3       string `json:"pulang3" validate:"omitempty"`
		AseskepPulang3Detail string `json:"pulang3_detail" validate:"omitempty"`
		Jam                  string `json:"jam" validate:"required"`
		Menit                string `json:"menit" validate:"required"`
		CaraKeluar           string `json:"cara_keluar" validate:"required"`
	}

	ResponseTindakLanjutIGD struct {
		JamCheckOut          string `json:"jam"`
		AseskepPulang1       string `json:"pulang1"`
		AseskepPulang1Detail string `json:"pulang1_detail"`
		AseskepPulang2       string `json:"pulang2"`
		AseskepPulang2Detail string `json:"pulang2_detail"`
		AseskepPulang3       string `json:"pulang3"`
		AseskepPulang3Detail string `json:"pulang3_detail"`
		CaraKeluar           string `json:"cara_keluar"`
	}

	ResponseVitalSignBangsal struct {
		KeadaanUmum     string `json:"keadaan_umum"`
		Kesadaran       string `json:"kesadaran"`
		KesadaranDetail string `json:"kesadaran_detail"`
		TekananDarah    string `json:"tekanan_darah"`
		Nadi            string `json:"nadi"`
		Pernapasan      string `json:"pernapasan"`
		TinggiBadan     string `json:"tinggi_badan"`
		BeratBadan      string `json:"berat_badan"`
	}

	// REQUEST ASESMEN DOKTER
	RequestAsesmenDokter struct {
		DeviceID  string `json:"device_id" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"required"`
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`

		// ======================== ASESMEN DOKTER ===================== //
		AsesmedJenpel           string `json:"jenpel" validate:"omitempty"`
		AsesmedKeluhUtama       string `json:"keluhan_utama" validate:"omitempty"`
		AsesmedKeluhTambahan    string `json:"keluhan_tambahan" validate:"omitempty"`
		AsesmedTelaah           string `json:"telaah" validate:"omitempty"`
		AsesmedMslhMedis        string `json:"masalah_medis" validate:"omitempty"`
		AsesmedRwytSkrg         string `json:"riwayat_sekarang" validate:"omitempty"`
		AsesmedRwytDulu         string `json:"riwayat_dulu" validate:"omitempty"`
		AsesmedRwytObat         string `json:"riwayat_obat" validate:"omitempty"`
		AsesmedRwytPenyKlrg     string `json:"riwayat_keluarga" validate:"omitempty"`
		AsesmedRwytAlergi       string `json:"riwayat_alergi" validate:"omitempty"`
		AsesmedRwytAlergiDetail string `json:"riwayat_detail" validate:"omitempty"`
		AsesmedAnamnesa         string `json:"anamnesa" validate:"omitempty"`
	}

	//  ======================= //
	ResponseAsesmenDokter struct {
		AsesmedJenpel     string `json:"jenpel"`
		AsesmedKeluhUtama string `json:"keluhan_utama"`
		AsesmedTelaah     string `json:"telaah"`
		AsesmedMslhMedis  string `json:"masalah_medis"`
	}

	ResponseToKeadaanUmum struct {
		KeadaanUmum     string `json:"keadaan_umum"`
		Kesadaran       string `json:"kesadaran"`
		KesadaranDetail string `json:"kesadaran_detail"`
	}

	ToResponseSkalaNyeri struct {
		SkalaNyeri     int    `json:"skala_nyeri"`
		FrekuensiNyeri string `json:"frekuensi_nyeri"`
		LokasiNyeri    string `json:"lokasi_nyeri"`
		KualitasNyeri  string `json:"kualitas_nyeri"`
		Menjalar       string `json:"menjalar"`
	}

	ResponseKebidananModel struct {
		Noreg   string `json:"noreg"`
		N1      string `json:"n1"`
		N2      string `json:"n2"`
		Nilai   int    `json:"nilai"`
		NilaiN1 int    `json:"nilai_n1"`
		NilaiN2 int    `json:"nilai_n2"`
	}

	ResDPengkajianFungsionalModel struct {
		Noreg    string `json:"noreg"`
		F1       string `json:"f1"`
		NilaiF1  int    `json:"nilai_f1"`
		F2       string `json:"f2"`
		NilaiF2  int    `json:"nilai_f2"`
		F3       string `json:"f3"`
		NilaiF3  int    `json:"nilai_f3"`
		F4       string `json:"f4"`
		NilaiF4  int    `json:"nilai_f4"`
		F5       string `json:"f5"`
		NilaiF5  int    `json:"nilai_f5"`
		F6       string `json:"f6"`
		NilaiF6  int    `json:"nilai_f6"`
		F7       string `json:"f7"`
		NilaiF7  int    `json:"nilai_f7"`
		F8       string `json:"f8"`
		NilaiF8  int    `json:"nilai_f8"`
		F9       string `json:"f9"`
		NilaiF9  int    `json:"nilai_f9"`
		F10      string `json:"f10"`
		NilaiF10 int    `json:"nilai_f10"`
		Nilai    int    `json:"nilai"`
	}

	ResponsePemeriksasanFisikAntonio struct {
		Kepala              string `json:"kepala"`
		Mata                string `json:"mata"`
		Tht                 string `json:"tht"`
		Mulut               string `json:"mulut"`
		Gigi                string `json:"gigi"`
		Leher               string `json:"leher"`
		KelenjarGetahBening string `json:"getah_bening"`
		Dada                string `json:"dada"`
		Jantung             string `json:"jantung"`
		Paru                string `json:"paru"`
		Perut               string `json:"perut"`
		Hati                string `json:"hati"`
		Limpa               string `json:"limpa"`
		Usus                string `json:"usus"`
		AbdomenLainnya      string `json:"abdomen_lainnya"`
		Ginjal              string `json:"ginjal"`
		AlatKelamin         string `json:"alat_kelamin"`
		Anus                string `json:"anus"`
		Superior            string `json:"superior"`
		Inferior            string `json:"inferior"`
	}

	TandaVitalIGDResponse struct {
		GCSE       string `json:"gcs_e"`
		GCSV       string `json:"gcs_v"`
		GCSM       string `json:"gcs_m"`
		Td         string `json:"td"`
		Nadi       string `json:"nadi"`
		Suhu       string `json:"suhu"`
		Kesadaran  string `json:"kesadaran"`
		Pernafasan string `json:"pernafasan"`
		Spo2       string `json:"spo2"`
		Tb         string `json:"tinggi_badan"`
		Bb         string `json:"berat_badan"`
		Akral      string `json:"akral"`
		Pupil      string `json:"pupil"`
	}

	ReqTandaVitalIGDDokter struct {
		Noreg     string `json:"noreg" validate:"required"`
		Person    string `json:"person" validate:"required"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
	}

	ResponseDoubleCheck struct {
		IDDoubleCheck      int    `json:"id_double_check"`
		PasienPemberi      bool   `json:"pasien_pemberi"`
		PasienVerify       bool   `json:"pasien_verify"`
		ObatPemberi        bool   `json:"obat_pemberi"`
		ObatVerify         bool   `json:"obat_verify"`
		DosisPemberi       bool   `json:"dosis_pemberi"`
		DosisVerify        bool   `json:"dosis_verify"`
		CaraPemberi        bool   `json:"cara_pemberi"`
		CaraVerify         bool   `json:"cara_verify"`
		WaktuPemberi       bool   `json:"waktu_pemberi"`
		WaktuVerify        bool   `json:"waktu_verify"`
		InformasiPemberi   bool   `json:"informasi_pemberi"`
		InformasiVerify    bool   `json:"informasi_verify"`
		DokumentasiPemberi bool   `json:"dokumentasi_pemberi"`
		DokumentasiVerify  bool   `json:"dokumentasi_verify"`
		Keterangan         string `json:"keterangan"`
	}

	ReqSaveTandaVitalIGDDokter struct {
		Noreg      string `json:"noreg" validate:"required"`
		Person     string `json:"person" validate:"required"`
		Pelayanan  string `json:"pelayanan" validate:"omitempty"`
		DeviceID   string `json:"device_id" validate:"required"`
		GCSE       string `json:"gcs_e"  validate:"omitempty"`
		GCSV       string `json:"gcs_v"  validate:"omitempty"`
		GCSM       string `json:"gcs_m"  validate:"omitempty"`
		Td         string `json:"td"  validate:"omitempty"`
		Nadi       string `json:"nadi"  validate:"omitempty"`
		Suhu       string `json:"suhu"  validate:"omitempty"`
		Kesadaran  string `json:"kesadaran"  validate:"omitempty"`
		Pernafasan string `json:"pernafasan"  validate:"omitempty"`
		Spo2       string `json:"spo2"  validate:"omitempty"`
		Tb         string `json:"tinggi_badan"  validate:"omitempty"`
		Bb         string `json:"berat_badan"  validate:"omitempty"`
		Akral      string `json:"akral" validate:"omitempty"`
		Pupil      string `json:"pupil" validate:"omitempty"`
	}

	ResponseTindakLanjutIGDResponse struct {
		Noreg        string `json:"noreg"`
		Prognosis    string `json:"prognosis"`
		KdBagian     string `json:"kdBagian"`
		Terapi       string `json:"terapi"`
		AlasanOpname string `json:"alasan_opname"`
		KonsulKe     string `json:"konsul_ke"`
		AlasanKonsul string `json:"alasan_konsul"`
	}

	ResponseResikoJatuhGoUpGoTest struct {
		ResikoJatuh1 string `json:"resiko_jatuh1"`
		ResikoJatuh2 string `json:"resiko_jatuh2"`
		Tindakan     string `json:"tindakan"`
	}

	ResponseAsesmenAwalIGD struct {
		PerolehanInfo       string                 `json:"info"`
		PerolehanInfoDetail string                 `json:"info_detail"`
		CaraMasuk           string                 `json:"cara_masuk"`
		CaraMasukDetail     string                 `json:"cara_masuk_detail"`
		AsalMasuk           string                 `json:"asal_masuk"`
		AsalMasukDetail     string                 `json:"asal_masuk_detail"`
		AsesFungsional      string                 `json:"fungsional"`
		Riwayat             []soap.RiwayatPenyakit `json:"riwayat"`
	}

	ReqResikoJatuhGoUpAndGoTest struct {
		DeviceID     string `json:"device_id"`
		Pelayanan    string `json:"pelayanan"`
		Person       string `json:"person"`
		Noreg        string `json:"noreg"`
		ResikoJatuh1 string `json:"resiko_jatuh1"`
		ResikoJatuh2 string `json:"resiko_jatuh2"`
		Tindakan     string `json:"tindakan"`
	}

	ReqNoreg struct {
		Noreg string `json:"no_reg" binding:"required"`
	}

	ReqTindakLanjut struct {
		Noreg string `json:"noReg" binding:"required"`
	}

	ReqDeleteDiagnosa struct {
		Noreg string `json:"no_reg" binding:"required"`
		Table string `json:"table" binding:"required"`
	}

	ReqInputSingleDiagnosa struct {
		Noreg string `json:"noReg" binding:"required"`
		Code  string `json:"code" binding:"required"`
		Type  string `json:"type" binding:"required"`
		Table string `json:"table" binding:"required"`
	}

	ReqInputDiagnosa struct {
		Noreg    string     `json:"noReg" binding:"required"`
		KdBagian string     `json:"kdBagian" binding:"required"`
		Diagnosa []Diagnosa `json:"diagnosa"`
	}

	Diagnosa struct {
		Code        string `json:"code"`
		Description string `json:"description"`
		Type        string `json:"type"`
	}

	ReqKebEdukasi struct {
		Edukasi         KebEdukasiRes      `json:"edukasi"`
		Hambatan        HabatanPasienRes   `json:"hambatan"`
		Hasil           HasilRes           `json:"hasil"`
		IntervensiRes   IntervensiRes      `json:"intervensi"`
		KempBelajar     KempBelajarRes     `json:"kemampuanBelajar"`
		MotivasiBelajar MotivasiBelajarRes `json:"motivasiBelajar"`
		NilaiPasien     PilihanPasienRes   `json:"nilaiPasien"`
		RencanaEdukasi  string             `json:"rencanaEdukasi"`
		AsseEduJam      string             `json:"aseseduJam"`
		AsseEduTgl      string             `json:"aseseduTgl"`
		AsseEduUser     string             `json:"aseseduUser"`
		NoReg           string             `json:"noReg" binding:"required"`
		Tujuan          TujuanRes          `json:"tujuan"`
	}

	ReqSaveRawatJalanDokter struct {
	}

	KebEdukasiRes struct {
		KebEdu1        string `json:"kebEdu1"`
		KebEdu2        string `json:"kebEdu2"`
		KebEdu3        string `json:"kebEdu3"`
		KebEdu4        string `json:"kebEdu4"`
		KebEdu5        string `json:"kebEdu5"`
		KebEdu6        string `json:"kebEdu6"`
		KebEdu7        string `json:"kebEdu7"`
		KebEdu8        string `json:"kebEdu8"`
		KebEdu9        string `json:"kebEdu9"`
		KebEdu10       string `json:"kebEdu10"`
		KebEdu11       string `json:"kebEdu11"`
		KebEdu11Detail string `json:"kebEdu11Detail"`
	}

	TujuanRes struct {
		Tujuan1 string `json:"tujuan1"`
		Tujuan2 string `json:"tujuan2"`
	}

	MotivasiBelajarRes struct {
		MotivasiBel1 string `json:"motivasiBelajar1"`
		MotivasiBel2 string `json:"motivasiBelajar2"`
		MotivasiBel3 string `json:"motivasiBelajar3"`
	}

	KempBelajarRes struct {
		KempBelajar1 string `json:"kempBelajar1"`
		KempBelajar2 string `json:"kempBelajar2"`
		KempBelajar3 string `json:"kempBelajar3"`
	}

	PilihanPasienRes struct {
		NilaiPasien1       string `json:"nilaiPasien1"`
		NilaiPasien2       string `json:"nilaiPasien2"`
		NilaiPasien3       string `json:"nilaiPasien3"`
		NilaiPasien4       string `json:"nilaiPasien4"`
		NilaiPasien5       string `json:"nilaiPasien5"`
		NilaiPasien6       string `json:"nilaiPasien6"`
		NilaiPasien6Detail string `json:"nilaiPasien6Detail"`
	}

	HabatanPasienRes struct {
		Hambatan1       string `json:"hambatan1"`
		Hambatan2       string `json:"hambatan2"`
		Hambatan3       string `json:"hambatan3"`
		Hambatan4       string `json:"hambatan4"`
		Hambatan5       string `json:"hambatan5"`
		Hambatan6       string `json:"hambatan6"`
		Hambatan7       string `json:"hambatan7"`
		Hambatan8       string `json:"hambatan8"`
		Hambatan9       string `json:"hambatan9"`
		Hambatan9Detail string `json:"hambatan9Detail"`
	}

	HasilRes struct {
		Hasil1 string `json:"hasil1"`
		Hasil2 string `json:"hasil2"`
	}

	ResUploadOdontogram struct {
		ImageUrl string `form:"imageUrl" binding:"required"`
		NoReg    string `form:"noReg"  binding:"required"`
	}

	IntervensiRes struct {
		Intervensi1 string `json:"intervensi1"`
		Intervensi2 string `json:"intervensi2"`
		Intervensi3 string `json:"intervensi3"`
		Intervensi4 string `json:"intervensi4"`
		Intervensi5 string `json:"intervensi5"`
	}

	RequestGetData struct {
		NoReg string `json:"noReg" binding:"required" bson:"noReg"`
	}

	InsertOdontogram struct {
		NoReg      string `json:"noReg" binding:"required" bson:"noReg"`
		Number     int    `json:"number" binding:"required" bson:"number"`
		Assets     string `json:"assets" binding:"required" bson:"assets"`
		Keterangan string `json:"keterangan" binding:"required" bson:"keterangan"`
	}

	InsertDiagnosa struct {
		NoReg      string `json:"noReg" binding:"required" bson:"noReg"`
		Number     int    `json:"number" binding:"required" bson:"number"`
		Assets     string `json:"assets" binding:"required" bson:"assets"`
		Keterangan string `json:"keterangan" binding:"required" bson:"keterangan"`
	}

	RequestSaveAnamnesa struct {
		NoReg                      string `json:"noReg" binding:"required" bson:"noReg"`
		AsesmedKeluhUtama          string `json:"keluhUtama" binding:"omitempty" bson:"keluhUtama"`
		AsesmedJenpel              string `json:"jenisPelayanan"  binding:"omitempty"  bson:"jenisPelayanan"`
		AsesmenRwtSkrg             string `json:"rwtSekarang"  binding:"omitempty"  bson:"rwtSekarang"`
		AsesmenRwtDulu             string `json:"rwtDulu"  binding:"omitempty"  bson:"rwtDulu"`
		AsesmenRwtObat             string `json:"rwtObat"  binding:"omitempty"  bson:"rwtObat"`
		AsesmenRwtPenyKlrg         string `json:"rwtPenyKeluarga"  binding:"omitempty"  bson:"rwtPenyKeluarga"`
		AsesmenRwtPenyAlergi       string `json:"rwtPenyAlergi"  binding:"omitempty"  bson:"rwtPenyAlergi"`
		AsesmenRwtPenyAlergiDetail string `json:"rwtPenyAlergiDetail"  binding:"omitempty"  bson:"rwtPenyAlergiDetail"`
		AsesmenKhusPemGigi1        string `json:"khusPemGigi1"  binding:"omitempty"  bson:"khusPemGigi1"`
		AsesmenKhusPemGigi2        string `json:"khusPemGigi2"  binding:"omitempty"  bson:"khusPemGigi2"`
		AsesmenKhusPemGigi3        string `json:"khusPemGigi3"  binding:"omitempty"  bson:"khusPemGigi3"`
		AsesmenKhusPemGigi4        string `json:"khusPemGigi4"  binding:"omitempty"  bson:"khusPemGigi4"`
		AsesmenKhusPemGigi5        string `json:"khusPemGigi5"  binding:"omitempty"  bson:"khusPemGigi5"`
		AsesmenKhusPemGigi5Detail  string `json:"khusPemGigi5Detail"  binding:"omitempty"  bson:"khusPemGigi5Detail"`
	}

	RequestSaveAnatomi struct {
		Nama       string                `form:"nama" bson:"nama" binding:"required"`
		Norm       string                `form:"norm" bson:"norm" binding:"required"`
		Keterangan string                `form:"keterangan" bson:"keterangan"  binding:"required"`
		File       *multipart.FileHeader `form:"imageUrl" bson:"imageUrl"`
	}

	RequstSaveImageOdontogram struct {
		File  *multipart.FileHeader `form:"imageUrl" binding:"required" bson:"imageUrl"`
		NoReg string                `form:"noReg"  binding:"required" bson:"noReg"`
	}

	RequestSavePublishOdontogram struct {
		File     *multipart.FileHeader `form:"imageUrl" binding:"required"  bson:"imageUrl"`
		NoReg    string                `form:"noReg"  binding:"required" bson:"noReg"`
		KdBagian string                `form:"kdBagian" bson:"kdBagian"`
	}

	RequestSaveLokalis struct {
		File  *multipart.FileHeader `form:"imageUrl" binding:"required"  bson:"imageUrl"`
		NoReg string                `form:"noReg"  binding:"required" bson:"noReg"`
	}

	RequestUploadLokalisPublic struct {
		File     *multipart.FileHeader `form:"imageUrl" binding:"required"  bson:"imageUrl"`
		NoReg    string                `form:"noReg"  binding:"required" bson:"noReg"`
		KdBagian string                `form:"kd_bagian"  binding:"required" bson:"kd_bagian"`
	}

	RequestRencanaTindakLanjut struct {
		NoReg                string `json:"noreg"  binding:"required"  bson:"noreg"`
		KetPerson            string `json:"person"`
		RencanaAnjuranTerapi string `json:"anjuran"  binding:"omitempty"  bson:"anjuran"`
		KonsulKe             string `json:"konsul_ke"  binding:"omitempty"  bson:"konsul_ke"`
		AlasanOpname         string `json:"alasan_opname"  binding:"omitempty"  bson:"alasan_opname"`
	}

	RequestPilihPemeriksaanLabor struct {
		NameGrup string `json:"name_grup"  binding:"required"  bson:"name_grup"`
	}

	RequestSavePemeriksaanFisik struct {
		Noreg                          string `json:"noreg" binding:"required" bson:"noreg"`
		AsesmedPemfisKepala            string `json:"kepala" binding:"omitempty" bson:"kepala"`
		AsesmedPemfisKepalaDetail      string `json:"kepala_detail" binding:"omitempty" bson:"kepala_detail"`
		AsesmedPemfisLeher             string `json:"leher" binding:"omitempty" bson:"leher"`
		AsesmedPemfisLeherDetail       string `json:"leher_detail" binding:"omitempty" bson:"leher_detail"`
		AsesmedPemfisDada              string `json:"data" binding:"omitempty" bson:"data"`
		AsesmedPemfisDadaDetail        string `json:"data_detail" binding:"omitempty" bson:"data_detail"`
		AsesmedPemfisAbdomen           string `json:"abdomen" binding:"omitempty" bson:"abdomen"`
		AsesmedPemfisAbdomenDetail     string `json:"abdomen_detail" binding:"omitempty" bson:"abdomen_detail"`
		AsesmedPemfisPunggung          string `json:"punggung" binding:"omitempty" bson:"punggung"`
		AsesmedPemfisPunggungDetail    string `json:"punggung_detail" binding:"omitempty" bson:"punggung_detail"`
		AsesmedPemfisGenetalia         string `json:"genetalia" binding:"omitempty" bson:"genetalia"`
		AsesmedPemfisGenetaliaDetail   string `json:"genetalia_detail" binding:"omitempty" bson:"genetalia_detail"`
		AsesmedPemfisEkstremitas       string `json:"ekstremitas" binding:"omitempty" bson:"ekstremitas"`
		AsesmedPemfisEkstremitasDetail string `json:"ekstremitas_detail" binding:"omitempty" bson:"ekstremitas_detail"`
	}

	RequestUploadOdontogram struct {
		File  *multipart.FileHeader `form:"imageUrl" binding:"required"  bson:"imageUrl"`
		NoReg string                `form:"noReg"  binding:"required" bson:"noReg"`
	}

	RequestSaveOdontogram struct {
		NoReg      string `json:"noReg" binding:"required" bson:"noReg"`
		NoGigi     string `json:"noGigi" binding:"required" bson:"noGigi"`
		Keterangan string `json:"keterangan" binding:"required" bson:"keterangan"`
	}

	RequestDeleteOdontogram struct {
		NoReg  string `json:"noReg" binding:"required" bson:"noReg"`
		NoGigi string `json:"noGigi" binding:"required" bson:"noGigi"`
	}

	RequestSaveAssementRawatJalanDokter struct {
		soap.DataMedik            `json:"dataMedik" bson:"dataMedik"`
		soap.IntraOral            `json:"intraOral" bson:"intaOral"`
		soap.AssesPemeriksaanGigi `json:"pemeriksaanGigi" bson:"pemeriksaanGigi"`
		soap.AssesRiwayat         `json:"riwayat" bson:"riwayat"`
		NoReg                     string `json:"noReg" binding:"required" bson:"noReg"`
		KdBagian                  string `json:"kdBagian" bson:"kdBagian"`
		KeluhanUtama              string `json:"kelUtama" bson:"kelUtama"`
		Pelayanan                 string `json:"pelayanan" bson:"pelayanan"`
	}

	RequestSaveAssementRawatJalanPerawat struct {
		KodeBagian               string `json:"kdBagian" binding:"omitempty" bson:"kdBagian"`
		NoReg                    string `json:"noReg" binding:"required" bson:"noReg"`
		KelUtama                 string `json:"kelUtama" binding:"omitempty" bson:"kelUtama"`
		RiwayatPenyakit          string `json:"riwayatPenyakit" binding:"omitempty" bson:"riwayatPenyakit"`
		RiwayatPenyakitDetail    string `json:"riwayatPenyakitDetail" binding:"omitempty" bson:"RiwayatPenyakitDetail"`
		RiwayatObat              string `json:"riwayatObat" binding:"omitempty" bson:"riwayatObat"`
		RiwayatSaatDirumah       string `json:"riwayatSaatDirumah" binding:"omitempty" bson:"riwayatSaatDirumah"`
		RiwayatObatDetail        string `json:"riwayatObatDetail" binding:"omitempty" bson:"riwayatObatDetail"`
		TekananDarah             string `json:"tekananDarah" binding:"omitempty" bson:"tekananDarah"`
		Nadi                     string `json:"nadi" binding:"omitempty" bson:"nadi"`
		Suhu                     string `json:"suhu" binding:"omitempty" bson:"suhu"`
		Pernapasan               string `json:"pernapasan" binding:"omitempty" bson:"pernapasan"`
		BeratBadan               string `json:"beratBadan" binding:"omitempty" bson:"beratBadan"`
		TinggiBadan              string `json:"tinggiBadan" binding:"omitempty" bson:"tinggiBadan"`
		SkriningNyeri            string `json:"skriningNyeri" binding:"omitempty" bson:"skriningNyeri"`
		Psikologis               string `json:"psikologis" binding:"omitempty" bson:"psikologis"`
		PsikologisDetail         string `json:"psikologisDetail" binding:"omitempty" bson:"psikologisDetail"`
		Fungsional               string `json:"fungsional" binding:"omitempty" bson:"fungsional"`
		FungsionalDetail         string `json:"fungsionalDetail" binding:"omitempty" bson:"fungsionalDetail"`
		AseskepRj1               string `json:"aseskepRj1" binding:"omitempty" bson:"AseskepRj1"`
		AseskepRj2               string `json:"aseskepRj2" binding:"omitempty" bson:"AseskepRj2"`
		AseskepHasilKajiRj       string `json:"aseskepHasilKajiRj" binding:"omitempty" bson:"aseskepHasilKajiRj"`
		AseskepHslKajiRjTind     string `json:"aseskepHslKajiRjTind" binding:"omitempty" bson:"aseskepHasilKajiRjTindakan"`
		AseskepNyeri             string `json:"aseskepNyeri" binding:"omitempty" bson:"aseskepNyeri"`
		AseskepHasilKajiRjDetail string `json:"aseskepHasilKajiRjDetail" binding:"omitempty" bson:"aseskepHasilKajiRjDetail"`
		AseskepPulang1           string `json:"aseskepPulang1" binding:"omitempty" bson:"aseskepPulang1"`
		AseskepPulang1Detail     string `json:"aseskepPulang1Detail" binding:"omitempty" bson:"aseskepPulang1Detail"`
		AseskepPulang2           string `json:"aseskepPulang2" binding:"omitempty" bson:"aseskepPulang2"`
		AseskepPulang2Detail     string `json:"aseskepPulang2Detail" binding:"omitempty" bson:"aseskepPulang2Detail"`
		AseskepPulang3           string `json:"aseskepPulang3" binding:"omitempty" bson:"aseskepPulang3"`
		AseskepPulang3Detail     string `json:"aseskepPulang3Detail" binding:"omitempty" bson:"aseskepPulang3Detail"`
		MasalahKeperawatan       string `json:"masalahKeperawatan" binding:"omitempty" bson:"masalahKeperawatan"`
		RencanaKeperawatan       string `json:"rencanaKeperawatan" binding:"omitempty" bson:"rencanaKeperawatan"`
	}

	RequestGetSkrining struct {
		NoReg string `json:"noReg" binding:"required" bson:"noReg"`
	}

	RequestSaveSkrining struct {
		KodeBagian string `json:"kdBagian" binding:"required" bson:"kdBagian"`
		NoReg      string `json:"noReg" binding:"required" bson:"noReg"`
		K1         string `json:"k1" binding:"omitempty" bson:"k1"`
		K2         string `json:"k2" binding:"omitempty" bson:"k2"`
		K3         string `json:"k3" binding:"omitempty" bson:"k3"`
		K4         string `json:"k4" binding:"omitempty" bson:"k4"`
		K5         string `json:"k5" binding:"omitempty" bson:"k5"`
		K6         string `json:"k6" binding:"omitempty" bson:"k6"`
		KF1        string `json:"KF1" binding:"omitempty" bson:"KF1"`
		KF2        string `json:"KF2" binding:"omitempty" bson:"KF2"`
		KF3        string `json:"KF3" binding:"omitempty" bson:"KF3"`
		KF4        string `json:"KF4" binding:"omitempty" bson:"KF4"`
		B1         string `json:"B1" binding:"omitempty" bson:"B1"`
		B2         string `json:"B2" binding:"omitempty" bson:"B2"`
		RJ         string `json:"RJ" binding:"required" bson:"RJ"`
		IV1        string `json:"IV1" binding:"omitempty" bson:"IV1"`
		IV2        string `json:"IV2" binding:"omitempty" bson:"IV2"`
		IV3        string `json:"IV3" binding:"omitempty" bson:"IV3"`
		IV4        string `json:"IV4" binding:"omitempty" bson:"IV4"`
		User       string `json:"user" bson:"user"`
		Tanggal    string `json:"tanggal" bson:"tanggal"`
		Jam        string `json:"jam" bson:"jam"`
	}

	ResponseTriaseRiwayatAlergi struct {
		IgdTriaseKeluh        string `json:"keluhan_utama"`
		IgdTriaseAlergi       string `json:"alergi"`
		IgdTriaseAlergiDetail string `json:"alergi_detail"`
	}

	ResDcpptSoap struct {
		KdBagian string `json:"kdBagian"`
		Noreg    string `json:"noreg"`
	}

	ResponseAssesRawatJalanPerawat struct {
		KelUtama                     string `json:"kelUtama" bson:"kelUtama"`
		RiwayatPenyakit              string `json:"riwayatPenyakit" bson:"riwayatPenyakit"`
		RiwayatObat                  string `json:"riwayatObat" bson:"riwayatObat"`
		RiwayatObatDetail            string `json:"riwayatObatDetail" bson:"riwayatObatDetail"`
		TekananDarah                 string `json:"tekananDarah" bson:"tekananDarah"`
		Nadi                         string `json:"nadi" bson:"nadi"`
		Suhu                         string `json:"suhu" bson:"suhu"`
		Pernapasan                   string `json:"pernapasan" bson:"pernapasan"`
		BeratBadan                   string `json:"beratBadan" bson:"beratBadan"`
		TinggiBadan                  string `json:"tinggiBadan" bson:"tinggiBadan"`
		Fungsional                   string `json:"fungsional" bson:"fungsional"`
		FungsionalDetail             string `json:"fungsionalDetail" bson:"fungsionalDetail"`
		ResikoJatuh1                 string `json:"resikoJatuh1" bson:"resikoJatuh1"`
		ResikoJatuh2                 string `json:"resikoJatuh2" bson:"resikoJatuh2"`
		HasilKajiResikoJatuh         string `json:"hasilKajiResikoJatuh" bson:"hasilKajiResikoJatuh"`
		HasilKajiResikoJatuhTindakan string `json:"hasilKajiResikoJatuhTindakan" bson:"hasilKajiResikoJatuhTindakan"`
		Nyeri                        string `json:"nyeri" bson:"nyeri"`
		Psikologis                   string `json:"psikologis" bson:"psikologis"`
		PsikologisDetail             string `json:"psikologisDetail" bson:"psikologisDetail"`
		Pulang1                      string `json:"pulang1" bson:"pulang1"`
		Pulang1Detail                string `json:"pulang1Detail" bson:"pulang1Detail"`
		Pulang2                      string `json:"pulang2" bson:"pulang2"`
		Pulang2Detail                string `json:"pulang2Detail" bson:"pulang2Detail"`
		Pulang3                      string `json:"pulang3" bson:"pulang3"`
		Pulang3Detail                string `json:"pulang3Detail" bson:"pulang3Detail"`
		MasalahKeperawatan           string `json:"masalahKeperawatan" bson:"masalahKeperawatan"`
		RencanaKeperawatan           string `json:"rencanaKeperawatan" bson:"rencanaKeperawatan"`
		AseskepUser                  string `json:"aseskepUser" bson:"aseskepUser"`
		AseskepTgl                   string `json:"aseskepTgl" bson:"aseskepTgl"`
		AseskepJam                   string `json:"aseskepJam" bson:"aseskepJam"`
	}

	ReqDataPasien struct {
		Noreg    string `json:"no_reg" binding:"required"`
		KdBagian string `json:"kd_bagian" binding:"required"`
	}

	ResImageLokalis struct {
		Image string `json:"image_lokalis" binding:"required"`
	}

	// Request Masalah Medis
	ReqInformasiMedis struct {
		Noreg            string `json:"no_reg" binding:"required"`
		KdBagian         string `json:"kd_bagian" binding:"required"`
		MasalahMedis     string `json:"masalah_medis" binding:"omitempty"`
		Terapi           string `json:"terapi" binding:"omitempty"`
		PemeriksaanFisik string `json:"pemeriksaan_fisik" binding:"omitempty"`
		Anjuran          string `json:"anjuran" binding:"omitempty"`
	}

	ReqPascaOperasi struct {
		Noreg                       string `json:"no_reg" binding:"required"`
		KdBagian                    string `json:"kd_bagian" binding:"required"`
		ObsanslokKhusPostKeadUmum   string `json:"keadaanUmum" binding:"omitempty"`
		ObsanslokKhusPostKeluhUtama string `json:"keluhUmum" binding:"omitempty"`
		ObsanslokKhusPostTd         string `json:"tekananDarah" binding:"omitempty"`
		ObsanslokKhusPostNadi       string `json:"nadi" binding:"omitempty"`
		ObsanslokKhusPostSuhu       string `json:"suhu" binding:"omitempty"`
		ObsanslokKhusPostRr         string `json:"pernapasan" binding:"omitempty"`
	}

	ReqIntraOpreasi struct {
		KdBagian           string `json:"kdBagian"`
		Noreg              string `json:"noreg" binding:"omitempty"`
		ObsansloKhusInTd   string `json:"tekananDarah" binding:"omitempty"`
		ObsansloKhusInNadi string `json:"nadi" binding:"omitempty"`
		ObsansloKhusInSuhu string `json:"suhu" binding:"omitempty"`
		ObsansloKhusInRr   string `json:"pernapasan" binding:"omitempty"`
	}

	// SAVE ANAMNESA
	ReqSaveAsesmedAnamnesa struct {
		Noreg                     string `json:"noReg" binding:"required"`
		AsesmedKeluhUtama         string `json:"kelUtama" binding:"omitempty"`
		AsesmedRwytSkrg           string `json:"riwayatSekarang" binding:"omitempty"`
		AsesmedRwytPenyKlrg       string `json:"penyakitKeluarga" binding:"omitempty"`
		AsesmedRwytAlergi         string `json:"riwayatAlergi" binding:"omitempty"`
		AsesmedRwytAlergiDetail   string `json:"riwayatAlergiDetail" binding:"omitempty"`
		AsesmedJenpel             string `json:"jenisPelayanan" binding:"omitempty"`
		AsesmedKhusPemGigi1       string `json:"gigi1" binding:"omitempty"`
		AsesmedKhusPemGigi2       string `json:"gigi2" binding:"omitempty"`
		AsesmedKhusPemGigi3       string `json:"gigi3" binding:"omitempty"`
		AsesmedKhusPemGigi4       string `json:"gigi4" binding:"omitempty"`
		AsesmedKhusPemGigi5       string `json:"gigi5" binding:"omitempty"`
		AsesmedKhusPemGigi5Detail string `json:"gigi5Detail" binding:"omitempty"`
	}

	ReqDataIntraOral struct {
		Noreg                        string `json:"noReg"  binding:"required"`
		AsesmedKhusOkAnterior        string `json:"anterior" binding:"omitempty"`
		AsesmedKhusOkPosterior       string `json:"posterior" binding:"omitempty"`
		AsesmedKhusOkMolar           string `json:"molar" binding:"omitempty"`
		AsesmedKhusPalatum           string `json:"palatum" binding:"omitempty"`
		AsesmedKhusTorusP            string `json:"torusP" binding:"omitempty"`
		AsesmedKhusTorusM            string `json:"torusM" binding:"omitempty"`
		AsesmedKhusTorusMDetail      string `json:"torusMDetail" binding:"omitempty"`
		AsesmedKhusSuperTeeth        string `json:"superTeeth" binding:"omitempty"`
		AsesmedKhusSuperTeethDetail  string `json:"superTeethDetail" binding:"omitempty"`
		AsesmedKhusDiastema          string `json:"diastema" binding:"omitempty"`
		AsesmedKhusDiastemaDetail    string `json:"diastemaDetail" binding:"omitempty"`
		AsesmedKhusGigiAnomali       string `json:"gigiAnomali" binding:"omitempty"`
		AsesmedKhusGigiAnomaliDetail string `json:"gigiAnomaliDetail" binding:"omitempty"`
		AsesmedKhusOralLain          string `json:"khusOralLain" binding:"omitempty"`
	}

	ReqDataMedik struct {
		Noreg                    string `json:"noReg" binding:"required"`
		AsesmedGolDarah          string `json:"golDarah" binding:"omitempty"`
		AsesmedTd                string `json:"tekananDarah" binding:"omitempty"`
		AsesmedTdDetail          string `json:"tekananDarahDetail" binding:"omitempty"`
		AsesmedPenyJantung       string `json:"penyJantung" binding:"omitempty"`
		AsesmedPenyJantungDetail string `json:"penyJantungDetail" binding:"omitempty"`
		AsesmedDiabet            string `json:"diabet" binding:"omitempty"`
		AsesmedDiabetDetail      string `json:"diabetDetail" binding:"omitempty"`
		AsesmedHaemop            string `json:"haemop" binding:"omitempty"`
		AsesmedHaemopDetail      string `json:"haemopDetail" binding:"omitempty"`
		AsesmedHepat             string `json:"hepat" binding:"omitempty"`
		AsesmedHepatDetail       string `json:"hepatDetail" binding:"omitempty"`
		AsesmedPenyLain          string `json:"penyLain" binding:"omitempty"`
		AsesmedPenyLainDetail    string `json:"penyLainDetail" binding:"omitempty"`
		AsesmedAlergiObat        string `json:"alergiObat" binding:"omitempty"`
		AsesmedAlergiObatDetail  string `json:"alergiObatDetail" binding:"omitempty"`
		AsesmedAlergiMknan       string `json:"alergiMakanan" binding:"omitempty"`
		AsesmedAlergiMknanDetail string `json:"alergiMakananDetail" binding:"omitempty"`
		AsesmedKebiasaanBuruk    string `json:"kebiasaanBuruk" binding:"omitempty"`
	}

	// == TRIASE ==
	RegTriase struct {
		NoReg                   string `json:"noreg" binding:"required"`
		IgdTriaseKeluh          string `json:"keluhan" binding:"omitempty"`
		IgdTriaseAlergi         string `json:"alergi" binding:"omitempty"`
		IgdTriaseAlergiDetail   string `json:"alergi_detail" binding:"omitempty"`
		IgdTriaseNafas          string `json:"nafas" binding:"omitempty"`
		IgdTriaseTd             string `json:"tekanan_darah" binding:"omitempty"`
		IgdTriaseRr             string `json:"pernapasan" binding:"omitempty"`
		IgdTriasePupil          string `json:"pupil" binding:"omitempty"`
		IgdTriaseNadi           string `json:"nadi" binding:"omitempty"`
		IgdTriaseSpo2           string `json:"spo" binding:"omitempty"`
		IgdTriaseSuhu           string `json:"suhu" binding:"omitempty"`
		IgdTriaseAkral          string `json:"akral" binding:"omitempty"`
		IgdTriaseCahaya         string `json:"reflek_cahaya" binding:"omitempty"`
		IgdTriaseGcsE           string `json:"gcs_e" binding:"omitempty"`
		IgdTriaseGcsV           string `json:"gcs_v" binding:"omitempty"`
		IgdTriaseGcsM           string `json:"gcs_m" binding:"omitempty"`
		IgdTriaseGangguan       string `json:"gangguan" binding:"omitempty"`
		IgdTriaseGangguanDetail string `json:"gangguan_detail" binding:"omitempty"`
		IgdTriaseSkalaNyeri     int    `json:"nyeri" binding:"omitempty"`
		IgdTriaseSkalaNyeriP    string `json:"nyeri_p" binding:"omitempty"`
		IgdTriaseSkalaNyeriQ    string `json:"nyeri_q" binding:"omitempty"`
		IgdTriaseSkalaNyeriR    string `json:"nyeri_r" binding:"omitempty"`
		IgdTriaseSkalaNyeriS    string `json:"nyeri_s" binding:"omitempty"`
		IgdTriaseSkalaNyeriT    string `json:"nyeri_t" binding:"omitempty"`
		IgdTriaseFlaccWajah     int    `json:"wajah" binding:"omitempty"`
		IgdTriaseFlaccKaki      int    `json:"kaki" binding:"omitempty"`
		IgdTriaseFlaccAktifitas int    `json:"aktifitas" binding:"omitempty"`
		IgdTriaseFlaccMenangis  int    `json:"menangis" binding:"omitempty"`
		IgdTriaseFlaccBersuara  int    `json:"bersuara" binding:"omitempty"`
		IgdTriaseFlaccTotal     int    `json:"total" binding:"omitempty"`
		IgdTriaseSkalaTriase    int    `json:"triase" binding:"omitempty"`
		IgdTriaseFingerUser     string `json:"user" binding:"omitempty"`
		IgdTriaseFingerTgl      string `json:"tanggal" binding:"omitempty"`
		IgdTriaseFingerJam      string `json:"jam" binding:"omitempty"`
	}

	//  ===== ANAMNESA IGD ======
	AnamnesaIGD struct {
		NoReg               string `json:"no_reg" binding:"required"`
		AsesmedKeluhUtama   string `json:"keluhan_utama"`
		AsesmedKesadaran    string `json:"kesadaran"`
		AsesmendTelaah      string `json:"telaah"`
		AsesmendMslhMedis   string `json:"mslh_medis"`
		AsesmedRwytSkrg     string `json:"rwyt_skrg"`
		AsesmedRwytDulu     string `json:"rwyt_dulu"`
		AsesmedRwytObat     string `json:"rwyt_obat"`
		AsesmedRwytPenyKlrg string `json:"rwyt_klrg"`
		AsesmedJenpel       string `json:"jenpel"`
	}

	// ReqSave Asesmen
	// Keperawatan Bidan
	ReqSaveAsesmenKeperawatanBidan struct {
		Noreg                         string `json:"noreg" binding:"required"`
		AseskepPerolehanInfo          string `json:"info" binding:"omitempty"`
		AseskepCaraMasuk              string `json:"cara_masuk" binding:"omitempty"`
		AseskepCaraMasukDetail        string `json:"cara_masuk_detail" binding:"omitempty"`
		AseskepAsalMasuk              string `json:"asal_masuk" binding:"omitempty"`
		AseskepAsalMasukDetail        string `json:"asal_masuk_detail" binding:"omitempty"`
		AseskepBb                     string `json:"bb" binding:"omitempty"`
		AseskepTb                     string `json:"tb" binding:"omitempty"`
		AseskepRwytPnykt              string `json:"rwt_penyakit" binding:"omitempty"`
		AseskepRwytObatDetail         string `json:"obat_detail" binding:"omitempty"`
		AseskepAsesFungsional         string `json:"fungsional" binding:"omitempty"`
		AseskepRj1                    string `json:"rj1" binding:"omitempty"`
		AseskepRj2                    string `json:"rj2" binding:"omitempty"`
		AseskepHslKajiRj              string `json:"kaji_rj" binding:"omitempty"`
		AseskepHslKajiRjTind          string `json:"kaji_rj_tindakan" binding:"omitempty"`
		AseskepSkalaNyeri             int    `json:"skala_nyeri" binding:"omitempty"`
		AseskepFrekuensiNyeri         string `json:"frekuensi_nyeri" binding:"omitempty"`
		AseskepLamaNyeri              string `json:"lama_nyeri" binding:"omitempty"`
		AseskepNyeriMenjalar          string `json:"nyeri_menjalar" binding:"omitempty"`
		AseskepNyeriMenjalarDetail    string `json:"menjalar_detail" binding:"omitempty"`
		AseskepKualitasNyeri          string `json:"kualitas_nyeri" binding:"omitempty"`
		AseskepNyeriPemicu            string `json:"nyeri_pemicu" binding:"omitempty"`
		AseskepNyeriPengurang         string `json:"nyeri_pengurang" binding:"omitempty"`
		AseskepKehamilan              string `json:"kehamilan" binding:"omitempty"`
		AseskepKehamilanGravida       string `json:"kehamilan_gravida" binding:"omitempty"`
		AseskepKehamilanPara          string `json:"kehamilan_para" binding:"omitempty"`
		AseskepKehamilanAbortus       string `json:"kehamilan_abortus" binding:"omitempty"`
		AseskepKehamilanHpht          string `json:"kehamilan_hpht" binding:"omitempty"`
		AseskepKehamilanTtp           string `json:"kehamilan_ttp" binding:"omitempty"`
		AseskepKehamilanLeopold1      string `json:"kehamilan_leopol1" binding:"omitempty"`
		AseskepKehamilanLeopold2      string `json:"kehamilan_leopol2" binding:"omitempty"`
		AseskepKehamilanLeopold3      string `json:"kehamilan_leopol3" binding:"omitempty"`
		AseskepKehamilanLeopold4      string `json:"kehamilan_leopol4" binding:"omitempty"`
		AseskepKehamilanDjj           string `json:"kehamilan_djj" binding:"omitempty"`
		AseskepKehamilanVt            string `json:"kehamilan_vt" binding:"omitempty"`
		AseskepDekubitus1             string `json:"dekubitus1" binding:"omitempty"`
		AseskepDekubitus2             string `json:"dekubitus2" binding:"omitempty"`
		AseskepDekubitus3             string `json:"dekubitus3" binding:"omitempty"`
		AseskepDekubitus4             string `json:"dekubitus4" binding:"omitempty"`
		AseskepDekubitusAnak          string `json:"dekubitus_anak" binding:"omitempty"`
		AseskepPulangKondisi          string `json:"pulang_kondisi" binding:"omitempty"`
		AseskepPulangTransportasi     string `json:"pulang_transportasi" binding:"omitempty"`
		AseskepPulangPendidikan       string `json:"pendidikan" binding:"omitempty"`
		AseskepPulangPendidikanDetail string `json:"pendidikan_detail" binding:"omitempty"`
	}
)
