package dto

import (
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/user"
	"time"
)

type (
	// ====
	ResponsePengkajianAwalKeperawatanDEWASA_RANAP struct {
		Pengkajian        ResponsePengkajianRawatInap           `json:"pengkajian"`
		PengobatanDirumah []kebidanan.DRiwayatPengobatanDirumah `json:"pengobatan_dirumah"`
	}

	ResponsePengkajianRawatInap struct {
		TglPengkajian            time.Time `json:"tgl_pengkajian"`
		KeluhanUtama             string    `json:"keluhan_utama"`
		PenyakitSekarang         string    `json:"penyakit_sekarang"`
		PenyakitDahulu           string    `json:"penyakit_dahulu"`
		PenyakitKeluarga         string    `json:"penyakit_keluarga"`
		RiwayatPengobatanDirumah string    `json:"pengobatan_dirumah"`
		RiwayatAlergi            string    `json:"riwayat_alergi"`
		ReaksiYangTimbul         string    `json:"reaksi_alergi"`
	}

	//====//
	ResDPengkajianFungsionalMapper struct {
		Noreg    string `json:"noreg"`
		F1       string `json:"f1"`
		NilaiF1  int    `json:"nilai_f1"`
		F2       string `json:"f2"`
		NilaiF2  int    `json:"nilai_f2"`
		F3       string `json:"f3"`
		NilaiF3  int    `json:"nilai_f3"`
		F4       string `json:"f4"`
		NilaiF4  int    `json:"nilai_f4"`
		F5       string `json:"f5"`
		NilaiF5  int    `json:"nilai_f5"`
		F6       string `json:"f6"`
		NilaiF6  int    `json:"nilai_f6"`
		F7       string `json:"f7"`
		NilaiF7  int    `json:"nilai_f7"`
		F8       string `json:"f8"`
		NilaiF8  int    `json:"nilai_f8"`
		F9       string `json:"f9"`
		NilaiF9  int    `json:"nilai_f9"`
		F10      string `json:"f10"`
		NilaiF10 int    `json:"nilai_f10"`
		Nilai    int    `json:"nilai"`
	}

	ResponsePengkajianKeperawatanRawatInap struct {
		PengkajianKeperawatan    soap.PengkajianAwalKeperawatan        `json:"pengkajian"`
		Karyawan                 user.Karyawan                         `json:"karyawan"`
		PemFisik                 ResponsePemeriksasanFisikAntonio      `json:"pem_fisik"`
		SkalaNyeri               ToResponseSkalaNyeri                  `json:"skala_nyeri"`
		Fungsional               ResDPengkajianFungsionalMapper        `json:"fungsional"`
		ResikoJatuh              kebidanan.DRisikoJatuhKebidanan       `json:"resiko_jatuh"`
		PengkajianNutrisi        ResponseKebidananModel                `json:"nutrisi"`
		AsuhanKeperawatan        []rme.DasKepDiagnosaModelV2           `json:"asuhan_keperawatan"`
		RiwayatPengobatanDirumah []kebidanan.DRiwayatPengobatanDirumah `json:"pengobatan_dirumah"`
	}

	ResponseAsesmenKeperawatan struct {
		Pengkajian PenkajianResponse                  `json:"pengkajian"`
		Alergi     []rme.DAlergi                      `json:"riwayat_keluarga"`
		Riwayat    []rme.RiwayatPenyakitDahuluPerawat `json:"riwayat_terdahulu"`
	}

	ResponseAsesmenIGD struct {
		Penkajian IGDResponse                        `json:"pengkajian"`
		Alergi    []rme.DAlergi                      `json:"riwayat_keluarga"`
		Riwayat   []rme.RiwayatPenyakitDahuluPerawat `json:"riwayat_terdahulu"`
	}

	IGDResponse struct {
		AsesmedKeluhUtama          string `json:"keluhan_utama"`
		JamCheckOut                string `json:"jam_check_out"`
		JamCheckInRanap            string `json:"jam_check_in_ranap"`
		CaraKeluar                 string `json:"cara_keluar"`
		AseskepPerolehanInfo       string `json:"info"`
		AseskepPerolehanInfoDetail string `json:"info_detail"`
		AseskepCaraMasuk           string `json:"cara_masuk"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail"`
		AseskepAsalMasuk           string `json:"asal_masuk"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail"`
		AseskepRwytObat            string `json:"rwt_obat"`
		AseskepAsesFungsional      string `json:"fungsional"`
		ReaksiAlergi               string `json:"reaksi_alergi"`
		AseskepRwytPnykt           string `json:"rwt_penyakit"`
		AsesmedRwytSkrg            string `json:"rwt_sekarang"`
	}

	ResponseAsesmenAnak struct {
		PengkajianAnak PenkajianAnakResponse              `json:"pengkajian_anak"`
		Riwayat        []rme.RiwayatPenyakitDahuluPerawat `json:"riwayat_terdahulu"`
		Alergi         []rme.DAlergi                      `json:"riwayat_keluarga"`
	}

	PenkajianAnakResponse struct {
		Noreg             string `json:"noreg"`
		Person            string `json:"person"`
		KdDPJP            string `json:"kd_dpjp"`
		Jenpel            string `json:"jenpel"`
		JenpelDetail      string `json:"jenpel_detail"`
		KeluhanUtama      string `json:"keluhan_utama"`
		RwtPenyakit       string `json:"rwt_penyakit"`
		ReaksiAlergi      string `json:"reaksi_alergi"`
		RwtPenyakitDahulu string `json:"rwt_penyakit_dahulu"`
		RwtImunisasi      string `json:"rwt_imunisasi"`
		RwtKelahiran      string `json:"rwt_kelahiran"`
	}

	PenkajianResponse struct {
		Noreg                 string `json:"noreg"`
		Person                string `json:"person"`
		KdDpjp                string `json:"dpjp"`
		AseskepJenpel         string `json:"jenpel"`
		AseskepJenpelDetail   string `json:"detail_jenpel"`
		AseskepKel            string `json:"keluhan_utama"`
		AseskepRwytPnykt      string `json:"rwyt_penyakit"`
		AseskepReaksiAlergi   string `json:"reaksi_alergi"`
		RiwayatPenyakitDahulu string `json:"rwyt_penyakit_dahulu"`
	}

	ReqAsesmenKeperawatan struct {
		Noreg   string `json:"no_reg" validate:"required"`
		NoRm    string `json:"no_rm" validate:"required"`
		Person  string `json:"person" validate:"required"`
		Tanggal string `json:"tanggal"`
	}

	ReqReportPengkajianAwalKeperawatan struct {
		Noreg   string `json:"no_reg" validate:"required"`
		NoRm    string `json:"no_rm" validate:"required"`
		Tanggal string `json:"tanggal"`
	}

	ReqAsesmenAnak struct {
		Noreg   string `json:"no_reg" validate:"required"`
		NoRm    string `json:"no_rm" validate:"required"`
		Person  string `json:"person" validate:"required"`
		Tanggal string `json:"tanggal"`
	}

	ReqReportAnak struct {
		Noreg     string `json:"no_reg" validate:"required"`
		Tanggal   string `json:"tanggal" validate:"omitempty"`
		NoRM      string `json:"no_rm" validate:"omitempty"`
		Person    string `json:"person" validate:"omitempty"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
	}

	ReqDoubleCheck struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqSaveDoubleCheck struct {
		Noreg       string `json:"no_reg" validate:"required"`
		Devices     string `json:"device_id" validate:"omitempty"`
		Pasien      bool   `json:"pasien" validate:"omitempty"`
		Obat        bool   `json:"obat" validate:"omitempty"`
		Dosis       bool   `json:"dosis" validate:"omitempty"`
		Cara        bool   `json:"cara" validate:"omitempty"`
		Waktu       bool   `json:"waktu" validate:"omitempty"`
		Informasi   bool   `json:"informasi" validate:"omitempty"`
		Dokumentasi bool   `json:"dokumentasi" validate:"omitempty"`
		Keterangan  string `json:"keterangan" validate:"omitempty"`
		NamaObat    string `json:"nama_obat" validate:"omitempty"`
		KodeObat    string `json:"kode_obat" validate:"omitempty"`
		PemberiObat string `json:"pemberi_obat" validate:"omitempty"`
	}

	ReqOnVerifyDoubleCheck struct {
		Noreg       string `json:"no_reg" validate:"required"`
		Devices     string `json:"device_id" validate:"omitempty"`
		Pasien      bool   `json:"pasien" validate:"omitempty"`
		Obat        bool   `json:"obat" validate:"omitempty"`
		Dosis       bool   `json:"dosis" validate:"omitempty"`
		Cara        bool   `json:"cara" validate:"omitempty"`
		Waktu       bool   `json:"waktu" validate:"omitempty"`
		Informasi   bool   `json:"informasi" validate:"omitempty"`
		Dokumentasi bool   `json:"dokumentasi" validate:"omitempty"`
		IDCheck     int    `json:"ID" validate:"omitempty"`
		NamaVerify  string `json:"nama_verify" validate:"omitempty"`
	}

	OnDeleteDoubleCheck struct {
		IdDoubleCheck int `json:"id_double_check" validate:"required"`
	}

	ReqOnSaveDoubleCheckVerify struct {
		Noreg string `json:"no_reg" validate:"required"`
	}

	ReqReportAsesmenKeperawatan struct {
		Noreg     string `json:"no_reg" validate:"required"`
		Person    string `json:"person" validate:"omitempty"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
	}

	ReqReportAsesmenKeperawatanRANAP struct {
		Noreg     string `json:"no_reg" validate:"required"`
		Person    string `json:"person" validate:"omitempty"`
		Pelayanan string `json:"pelayanan" validate:"omitempty"`
		Usia      string `json:"usia" validate:"omitempty"`
	}

	ReqSavePengkajianKeperawatan struct {
		Noreg                 string `json:"no_reg" validate:"required"`
		NoRm                  string `json:"no_rm" validate:"required"`
		Person                string `json:"person" validate:"required"`
		Pelayanan             string `json:"pelayanan" validate:"required"`
		DeviceID              string `json:"device_id" validate:"required"`
		Dpjp                  string `json:"dpjp"`
		AseskepJenpel         string `json:"jenpel"`
		AseskepJenpelDetail   string `json:"detail_jenpel"`
		AseskepKel            string `json:"keluhan_utama"`
		AseskepRwytPnykt      string `json:"rwyt_penyakit"`
		RiwayatPenyakitDahulu string `json:"rwyt_penyakit_dahulu"`
		AseskepReaksiAlergi   string `json:"reaksi_alergi"`
		Tanggal               string `json:"tanggal"`
	}

	ReqSavePengkajianAnak struct {
		Noreg                 string `json:"no_reg" validate:"required"`
		NoRm                  string `json:"no_rm" validate:"required"`
		Person                string `json:"person" validate:"required"`
		Pelayanan             string `json:"pelayanan" validate:"required"`
		DeviceID              string `json:"device_id" validate:"required"`
		Dpjp                  string `json:"dpjp"`
		AseskepJenpel         string `json:"jenpel"`
		AseskepJenpelDetail   string `json:"detail_jenpel"`
		AseskepKel            string `json:"keluhan_utama"`
		AseskepRwytPnykt      string `json:"rwyt_penyakit"`
		RiwayatPenyakitDahulu string `json:"rwyt_penyakit_dahulu"`
		AseskepReaksiAlergi   string `json:"reaksi_alergi"`
		Tanggal               string `json:"tanggal"`
		RwtImunisasi          string `json:"rwt_imunisasi"`
		RwtKelahiran          string `json:"rwt_kelahiran"`
	}

	ReqSaveVitalSignKeperawatanBangsal struct {
		Pelayanan   string `json:"pelayanan" validate:"required"`
		Noreg       string `json:"noreg" validate:"required"`
		Person      string `json:"person" validate:"required"`
		DeviceID    string `json:"device_id" validate:"required"`
		GCSE        string `json:"gcs_e"`
		GCSV        string `json:"gcs_v"`
		GCSM        string `json:"gcs_m"`
		Td          string `json:"td"`
		Pernafasan  string `json:"pernafasan"`
		Spo2        string `json:"spo2"`
		TinggiBadan string `json:"tinggi_badan"`
		BeratBadan  string `json:"berat_badan"`
		Nadi        string `json:"nadi"`
		Suhu        string `json:"suhu"`
		Kesadaran   string `json:"kesadaran"`
		Akral       string `json:"akral"`
		Pupil       string `json:"pupil"`
	}

	TandaVitalBangsalKeperawatanResponse struct {
		GCSE         string `json:"gcs_e"`
		GCSV         string `json:"gcs_v"`
		GCSM         string `json:"gcs_m"`
		Td           string `json:"td"`
		BeratBadan   string `json:"berat_badan"`
		TinggiBandan string `json:"tinggi_badan"`
		Pernafasan   string `json:"pernafasan"`
		Spo2         string `json:"spo2"`
		Nadi         string `json:"nadi"`
		Suhu         string `json:"suhu"`
		Kesadaran    string `json:"kesadaran"`
		Akral        string `json:"akral"`
		Pupil        string `json:"pupil"`
	}

	ReqTandaVitalKeperawatanBangsal struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}

	RegTandaVitalSignAnakBansal struct {
		Noreg  string `json:"no_reg" validate:"required"`
		Person string `json:"person" validate:"required"`
	}
)
