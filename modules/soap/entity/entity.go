package entity

import (
	"hms_api/modules/his"
	"hms_api/modules/igd"
	"hms_api/modules/kebidanan"
	kebidananDTO "hms_api/modules/kebidanan/dto"
	"hms_api/modules/rme"
	rmeDto "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/modules/user"
	"reflect"

	"github.com/gofiber/fiber/v2"
)

/*
Dalam Clean Architecture, "usecase" dan "interactor" memiliki arti yang sama: ini adalah komponen yang berisi logika bisnis. Presenter dalam arsitektur ini tidak mengandung logika bisnis apa pun. Tugas presenter adalah memetakan struktur data yang dikembalikan oleh interaksi use case ke dalam struktur data yang paling nyaman untuk dilihat.
*/
type SoapUseCase interface {
	// REPORT PENGKAJIAN AWAL KEPERATAN
	OnGetReportPengkajianAwalKeperawatanRawatInapUseCase(req dto.ReqReportPengkajianAwalKeperawatan, kdBagian string, userID string) (res dto.ResponsePengkajianKeperawatanRawatInap, err error)
	// ASESMEN IGD USECASE
	OnSaveAsesmenIGDUseCase(userID string, kdBagian string, req dto.ReqOnSaveAsesmenIGD) (res dto.ResponseAsesmenIGD, message string, err error)
	OnGetAsesmenIGDUseCase(userID string, kdBagian string, req dto.ReqAsesmenIGD) (res dto.ResponseAsesmenIGD)
	OnSaveResikoJatuhGoUpAndGoTestIGDUseCase(kdBagian string, userID string, req dto.ReqResikoJatuhGoUpAndGoTest) (res dto.ResponseResikoJatuhGoUpGoTest, message string, err error)
	DoubleCheckHighUseCase(userID string, kdBagian string, req dto.ReqSaveDoubleCheck) (message string, err error)
	OnGetReportPengkajianAwalMedisDokterUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res kebidananDTO.ReportPengkajianAwalDokterResponse, err error)
	OnSavePengkajianAwalAnakUseCase(userID string, kdBagian string, req dto.ReqSavePengkajianAnak) (res dto.ResponseAsesmenAnak, message string, err error)
	OnGetAsesmenAnakUseCase(userID string, kdBagian string, req dto.ReqAsesmenAnak) (res dto.ResponseAsesmenAnak, err error)
	OnGetPengkajianAwalKeperawatanAnakUseCase(userID string, kdBagian string, req dto.ReqAsesmenKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error)

	PublishImageOdontogramUseCase(noReg string, imgFile string, kdBagian string) (res soap.DcpptOdontogram, err error)
	InsertOdontogramUseCase(reg dto.RequestSaveOdontogram) (err error)
	UploadOdontogramUseCase(kdBagian string, noReg string, imgFile string) (res soap.DcpptOdontogram, err error)
	GetAssessRawatJalanUseCase(noRm string) (res map[string]interface{}, err error)
	GetKebutuhanEdukasiUseCase(noreg string) (res map[string]interface{}, err error)
	DetailPemeriksaanLaborUseCase(namaGrup string) (res soap.DetailPemeriksaanLaborModel, err error)
	InputRencanaTindakLanjutUseCase(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, message string, err error)

	// ============================ USECASE VERSI 2
	InsertOdontogramUseCaseV2(reg dto.RequestSaveOdontogramV2) (err error)
	DeleteOdontogramUsecaseV2(req dto.RequestDeleteOdontogramV2) (err error)
	SaveAssesRawatJalanDokterUsecaseV2(userID string, req dto.RequestSaveAssementRawatJalanDokterV2) (data soap.DcpptOdontogram, err error)
	SaveAssementRawatJalanPerawatUseCaseV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error)
	SimpanAnamnesaIGDUsecaseV2(kdBagian string, req dto.AnamnesaIGDV2) (res soap.AsesmedAwalPasienIGDModel, message string, err error)
	SaveImageLokalisUseCase(kdBagian string, path string, noReg string) (res soap.ImageLokalisModel, message string, err error)
	SimpanTriaseUseCaseV2(kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, message string, err error)
	SimpanTriaseUseCase(kdBagian string, req dto.RegTriase) (res soap.TriaseModel, message string, err error)
	SaveImageLokalisPublicUsecase(kdBagian string, noReg string, path string,
		keteranganPerson string, insertUserId string, deviceId string, pelayanan string,
	) (res soap.ImageLokalisModel, message string, err error)
	SaveImageLokalisToPublicUsecase(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, message string, err error)
	SavePemeriksaanFisikUseCaseV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, message string, err error)
	PilihPemeriksaanLaborUsecase(namaGrup string) (res []soap.PilihPemeriksaanLaborModel, err error)
	InputRencanaTindakLanjutUseCaseV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string, userID string) (res soap.RencanaTindakLanjutModel, message string, err error)
	SaveKeperawatanBidanUseCase(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, message string, err error)
	SaveKeperawatanBidanUseCaseV2(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, message string, err error)

	SaveTriaseRiwayatAlergiUsecase(kdBagian string, req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, message string, err error)
	SearchDcpptSoapPasienDokterUsecase(noReg string, kdBagian string, person string) (res dto.ResDcpptSoap, err error)
	SaveAsesmenInfoKeluarUsecase(kdBagian string, userID string, req dto.ReqAsesmenInformasiKeluhanIGD) (
		data soap.DcpptSoapPasienModel, message string, err error)
	SaveSkriningResikoDekubitusUsecase(kdBagian string, userID string, req dto.ReqSkriningDekubitusIGD) (data soap.DcpptSoapPasienModel, message string, err error)
	SaveRiwayatKehamilanUsecase(kdBagian string, userID string, req dto.RequestKehamilanIGD) (data soap.DcpptSoapPasienModel, message string, err error)
	SaveSkriningNyeriUseCase(userID string, kdBagian string, req dto.RequestSkriningNyeriIGD) (data soap.DcpptSoapPasienModel, message string, err error)
	SaveTindakLanjutUseCase(userID string, kdBagian string, req dto.RequestInputTindakLanjutIGD) (data soap.DcpptSoapPasienModel, message string, err error)
	GetVitalSignBangsalUsecase(kdBagian string, noReg string) (res dto.ResponseVitalSignBangsal)

	// =============================== ASESMEN DOKTER
	SaveAsesmenDokterUseCase(userID string, kdBagian string, req dto.RequestAsesmenDokter) (data soap.DcpptSoapDokter, message string, err error)
	SaveKeadaanUmumUseCase(kdBagian string, noReg string, userID string, req rmeDto.ReqKeadaanUmumBangsal) (datas soap.DcpptSoapPasienModel, message string, err error)
	SaveKeluhanUtamaIGDUseCase(kdBagian string, data dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, message string, err error)
	SaveImageLokalisUsecase(kdBagian string, noReg string, context *fiber.Ctx) (res soap.ImageLokalisModel, message string, err error)
	OnGetTandaVitalIGDDokterUserCase(userID string, kdBagian string, person string, noReg string, pelayanan string) (res dto.TandaVitalIGDResponse, err error)
	OnSaveTandaVitalIGDDokterUseCase(userID string, kdBagian string, person string, noReg string, deviceID string, req dto.ReqSaveTandaVitalIGDDokter) (res dto.TandaVitalIGDResponse, message string, err error)
	GetPengkajianAwalUsecase(userID string, kdBagian string, req dto.ReqAsesmenKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error)

	// ============================================== ASESMEN KEPERAWATAN BANGSAL  ============================ //
	OnSavePengkajianAwalKeperawatanUseCase(userID string, kdBagian string, req dto.ReqSavePengkajianKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error)
	OnSaveTandaVitalKeperawatanUseCase(userID string, kdBagian string, person string, noReg string, deviceID string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res dto.TandaVitalBangsalKeperawatanResponse, message string, err error)
	OnGetTandaVitalKeperawatanBangsalUseCase(userID string, kdBagian string, person string, noReg string) (res dto.TandaVitalBangsalKeperawatanResponse, err error)

	// REPORT TRIASE IGD
	OnGetTandaReportVitalIGDDokterUserCase(kdBagian string, person string, noReg string) (res dto.TandaVitalIGDResponse, err error)
	OnGetTandaVitalIGDPerawatUserCase(kdBagian string, person string, noReg string, pelayanan string) (res dto.TandaVitalIGDResponse, err error)
}

/*
Dalam konteks Clean Architecture, sebuah repository adalah sebuah pola desain yang bertanggung jawab untuk berkomunikasi antara domain atau use case dengan sumber daya data yang ada, seperti database, sistem file, atau sumber daya eksternal lainnya. Dalam struktur Clean Architecture, repository berada di antara domain atau use case dengan implementasi konkrit dari sumber daya data.

Tujuan utama dari penggunaan repository dalam Clean Architecture adalah untuk memisahkan logika bisnis dari logika akses data. Dengan demikian, struktur aplikasi dapat tetap bersih dan terorganisir, memungkinkan penggantian atau pembaruan sumber daya data tanpa memengaruhi logika bisnis di dalamnya.

Repository biasanya berisi operasi-operasi dasar seperti menyimpan data, mengambil data, menghapus data, dan memperbarui data. Dengan menggunakan pola desain ini, kode aplikasi menjadi lebih modular dan dapat diuji dengan lebih baik karena logika bisnis tidak terikat langsung dengan sumber daya data yang spesifik.

Dengan demikian, repository merupakan komponen penting dalam Clean Architecture karena membantu dalam mencapai prinsip-prinsip penting seperti pemisahan kepentingan dan ketergantungan, pengujian yang lebih mudah, dan kemampuan untuk mengganti sumber daya data tanpa mempengaruhi logika bisnis secara langsung.
*/
type SoapRepository interface {
	GetTandaVitalBangsalRepository(noreg string, kdBagian string) (res soap.DVitalSignIGDDokter, err error)
	// OnGetReportPemeriksaanFisikDokterAntonio(kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)
	OnGetReportPemeriksaanFisikDokterAntonio(kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error)
	InputSingleTindakanRepository(req dto.RegInputTindakan, kdBagian string) (res soap.DiagnosaModelOne, err error)
	OnGetTindakanICD9RepositoryBangsalDokter(noReg string, kdBagian string) (res []soap.TindakanResponse, err error)
	OnDeleteTindakanICD9Repository(noReg string, index string, kdBagian string) (err error)
	InsertSingleFirstTindakanRepository(req dto.RegInputTindakan, jamMasuk string, tglMasuk string, kdBagian string, kdDpjp string) (res soap.DiagnosaModelOne, err error)

	OnGetReportPengkajianKeperawatanDewasaRepository(noReg string, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error)
	UpdateTindakLanjutRepositoryDipulangkan(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD, jam string) (res soap.DcpptSoapPasienModel, err error)
	UpdateTindakLanjutRepositoryDirawatInap(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD, jam string) (res soap.DcpptSoapPasienModel, err error)
	// GET IMAGE LOKALIS MATA
	GetImageLokalisMataRepository(noReg string, kdBagian string) (res soap.ImageLokalisModel, err error)
	// GET IMAGE LOKALIS MATA
	OnGetAsesmenIGDRepository(kdBagian string, noReg string) (res soap.AsesemenIGD, err error)
	UpdateResikoJatuhRepository(noReg string, kdBagian string, req dto.ReqResikoJatuhGoUpAndGoTest) (res soap.ResikoJatuhGetUpGoTest, err error)
	GetResikoJatuhGoUpGoTestRepository(noReg string, kdBagian string) (res soap.ResikoJatuhGetUpGoTest, err error)
	//===//
	OnSaveDobleCheckHighReporitory(userID string, kdBagian string, devicesID string, req dto.ReqSaveDoubleCheck) (res soap.DDoubleCheck, err error)
	//===//
	OnUpdatePengkajianAnakRepository(req dto.ReqSavePengkajianAnak, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error)
	OnGetPengkajianAnakRepository(noReg string, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error)
	GetAnamnesaRepository(noreg string) (res soap.Anamnesa, err error)
	GetAssementRawatJalanPerawatRepository(noReg string) (res soap.AssementRawatJalanPerawat, err error)
	SaveAnamnesaRepository(req dto.RequestSaveAnamnesa) (err error)
	SaveAnamnesaRepositoryV2(req dto.RequestSaveAnamnesaV2) (err error)
	GetSkriningRepository(noreg string) (res soap.DcpptSoapPasien, err error)
	UpdateAssesRawatJalanDokterRepository(req dto.RequestSaveAssementRawatJalanDokter) (res soap.DcpptOdontogram, err error)
	GetRecordOdontogramRepository(noReg string) (res soap.DcpptOdontogram, err error)
	DeleteOdontogramRepository(req dto.RequestDeleteOdontogram) (res soap.DcpptSoapPasien, err error)
	InsertAssesRawatJalanDokterRepositoryV2(userID string, req dto.RequestSaveAssementRawatJalanDokterV2) (res soap.DcpptOdontogram, err error)
	SaveAnatomiRepository(req dto.RequestSaveAnatomi, urlImage string, dpjp string) (res soap.Anatomi, err error)
	SaveSkriningRepository(req dto.RequestSaveSkrining) (res soap.DcpptSoapPasien, err error)
	DetailSkriningRepository(noreg string) (res soap.DcpptSoapPasien, err error)
	UpdateSkriningRepository(req dto.RequestSaveSkrining) (res soap.DcpptSoapPasien, err error)
	InsertImageOdontogramRepository(kdBagian string, noReg string, imgFile string) (res soap.DcpptOdontogram, err error)
	UpdateImageOdontogramRepository(noReg string, imgFile string) (res soap.DcpptOdontogram, err error)
	UpdateOdontogramRepository(req dto.RequestSaveOdontogram) (res soap.DcpptSoapPasien, err error)
	InsertDataOdontogramRepository(kdBagian string, NoReg string, imgFile string) (res soap.DcpptOdontogram, err error)
	UpdateDataOdontogramRepository(noReg string, imgFile string) (res soap.DcpptOdontogram, err error)
	GetAsesmedAnamnesaRepository(noReg string, kdBagian string) (res soap.AsesmedAnamnesa, err error)
	GetOdontogramRepository(noReg string) (res []soap.Odontograms, err error)
	InsertOndontogramRepository(req dto.InsertOdontogram) (res soap.Odontograms, err error)
	InsertOndontogramRepositoryV2(req dto.InsertOdontogramV2) (res soap.Odontograms, err error)
	InsertAssesRawatJalanDokterRepository(userID string, req dto.RequestSaveAssementRawatJalanDokter) (res soap.DcpptOdontogram, err error)
	GetKebutuhanEdukasiRespository(noReg string) (res soap.KebEdukasi, err error)
	SaveAssesKebEdukasiRepositoryV2(req dto.ReqKebEdukasiV2) (res soap.KebEdukasi, err error)
	GetIntraOperasiRepository(noReg string, kdBagian string) (res soap.IntraOperasiModel, err error)
	InputSingleDiagnosaRepositoryV2(req dto.ReqInputSingleDiagnosaV2, kdBagian string) (res soap.DiagnosaModelOne, err error)
	GetDiagnosaRepository(noreg string, kdBagian string, kdDpjp string) (res []soap.DiagnosaResponse, err error)
	GetInformasiMedisRepository(req dto.ReqDataPasienV2) (res soap.InformasiMedis, err error)
	GetDataMedikRepository(noReg string, kdBagian string) (res soap.DataMedikModel, err error)
	SaveDataMedikRepositoryV2(req dto.ReqDataMedikV2, kdBagian string) (res soap.DataMedikModel, err error)
	GetDataIntraOralRepository(noReg string, kdBagian string) (res soap.DataIntraOralModel, err error)
	SavePascaOperasiRepository(req dto.ReqPascaOperasi, kdBagian string) (res soap.PascaOperasiModel, err error)
	SavePascaOperasiRepositoryV2(req dto.ReqPascaOperasiV2, kdBagian string) (res soap.PascaOperasiModel, err error)
	InsertPemeriksaanFisikRepository(kdBagian string, req dto.RequestSavePemeriksaanFisik) (res soap.PemeriksaanFisikModel, err error)
	UpdatePemeriksaanFisikRepository(kdBagian string, req dto.RequestSavePemeriksaanFisik) (res soap.PemeriksaanFisikModel, err error)

	// GetPemeriksaanFisikRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)
	GetTotalKTaripRepository(namaGrup string) (res soap.KtaripTotal, err error)
	GetPemeriksaanLaborRepository(namaGrup string) (res []soap.PemeriksaanLaborModel, err error)
	InsertRencanaTindakLanjutRepository(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, err error)
	UpdateRencanaTindakLanjutRepository(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, err error)
	UpdateAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, err error)

	// =============VERSION 2
	SaveAnatomiRepositoryV2(nama string, norm string, keterangan string, urlImage string, dpjp string) (res soap.Anatomi, err error)
	SaveSkriningRepositoryV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error)
	UpdateSkriningRepositoryV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error)
	UpdateOdontogramRepositoryV2(req dto.RequestSaveOdontogramV2) (res soap.DcpptSoapPasien, err error)
	DeleteOdontogramRepositoryV2(req dto.RequestDeleteOdontogramV2) (res soap.DcpptSoapPasien, err error)
	UpdateAssesRawatJalanDokterRepositoryV2(req dto.RequestSaveAssementRawatJalanDokterV2) (res soap.DcpptOdontogram, err error)
	UpdateDcpptSoapPasienRepositoryV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error)
	InsertDcpptSoapPasienRepositoryV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error)
	InsertSingleDiagnosaFirstRepositoryV2(req dto.ReqInputSingleDiagnosaV2, jamMasuk string, tglMasuk string, kdBagian string, kdDpjp string) (res soap.DiagnosaModelOne, err error)
	GetHasilPenunjangMedikRepository(noreg string) (res []soap.HasilPenunjangMedik, err error)
	DeleteDiagnosaRepositoryV2(req dto.ReqDeleteDiagnosaV2, kdBagian string) (err error)
	InsertInformasiMedisRepositoryV2(req dto.ReqInformasiMedisV2) (res soap.InformasiMedis, err error)
	UpdateInformasiMedisRepositoryV2(req dto.ReqInformasiMedisV2) (res soap.InformasiMedis, err error)
	SaveAsesmedAnamnesaRepositoryV2(req dto.ReqSaveAsesmedAnamnesaV2, kdBagian string) (res soap.AsesmedAnamnesa, err error)
	UpdateDataAnamnesaRepositoryV2(req dto.ReqSaveAsesmedAnamnesaV2, kdBagian string) (res soap.AsesmedAnamnesa, err error)
	GetAnamnesaIGDRepository(noReg string, kdBagian string) (res soap.AsesmedAwalPasienIGDModel, err error)
	SaveAnamnesaIGDRepositoryV2(kdBagian string, req dto.AnamnesaIGDV2, tanggalMasuk string, jamMasuk string) (res soap.AsesmedAwalPasienIGDModel, err error)
	UpdateAnamnesaIGDRepositoryV2(kdBagian string, req dto.AnamnesaIGDV2) (res soap.AsesmedAwalPasienIGDModel, err error)
	UpdateDataMedikRepositoryV2(req dto.ReqDataMedikV2, kdBagian string) (res soap.DataMedikModel, err error)
	InsertDataIntraOralRepositoryV2(req dto.ReqDataIntraOralV2, kdBagian string) (res soap.DataIntraOralModel, err error)
	UpdateDataIntraOralRepositoryV2(req dto.ReqDataIntraOralV2, kdBagian string) (res soap.DataIntraOralModel, err error)
	InsertPascaOperasiRepository(req dto.ReqPascaOperasi, kdBagian string) (res soap.PascaOperasiModel, err error)
	InsertPascaOperasiRepositoryV2(req dto.ReqPascaOperasiV2, kdBagian string) (res soap.PascaOperasiModel, err error)
	GetTriaseRepository(noReg string, kdBagian string) (res soap.TriaseModel, err error)
	InsertTriaseRepository(noReg string, kdBagian string, req dto.RegTriase) (res soap.TriaseModel, err error)
	UpdateTriasRepository(kdBagian string, req dto.RegTriase) (res soap.TriaseModel, err error)

	UpdateTriasRepositoryV2(kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error)
	InsertTriaseRepositoryV2(noReg string, kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error)
	GetImageLokalisRepository(noReg string, kdBagian string) (res soap.ImageLokalisModel, err error)
	InsertImageLokalisRepository(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, err error)
	UpdateImageLokalisRepository(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, err error)
	InsertPemeriksaanFisikRepositoryV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, err error)
	UpdatePemeriksaanFisikRepositoryV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, err error)
	GetAsesmedKeperawatanBidanRepository(noReg string, kdBagian string) (res soap.AsesmedKeperawatanBidan, err error)
	InsertRencanaTindakLanjutRepositoryV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string, tglMasuk string, jamMasuk string, KdDpjp string) (res soap.RencanaTindakLanjutModel, err error)
	UpdateRencanaTindakLanjutRepositoryV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string) (res soap.RencanaTindakLanjutModel, err error)
	GetRencanaTindakLanjutRepository(noReg string, kdBagian string, kdDpjp string) (res soap.RencanaTindakLanjutModel, err error)
	GetRencanaTindakLanjutReportRepository(noReg string) (res soap.RencanaTindakLanjutModel, err error)
	InsertAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, err error)
	InsertAsesmenKeperawatanBidanRepositoryV2(UpdateAsesmenKeperawatanBidanRepositorykdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, err error)
	UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, err error)

	InsertImageToLokasisRepositoryV2(kdBagian string, noReg string, path string, keteranganPerson string, insertUserId string, devicesID string, pelayanan string, tglMasuk string, jamMasuk string) (res soap.ImageLokalisModel, err error)
	GetTriaseRiwayatAlergi(noReg string, kdBagian string) (res soap.TriaseRiwayatAlergi, err error)
	SaveTriaseRiwayatAlergiRepository(noReg string, kdBagian string, tglMasuk string, jamMasuk string, req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, err error)
	UpdateTriaseRiwayatAlergiRepository(noReg string, kdBagian string, req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, err error)
	GetJamMasukPasienRepository(noReg string) (res soap.DRegister, err error)
	SearchDcpptSoapPasienRepository(noReg string, kdBagian string) (res dto.ResDcpptSoap, err error)
	SearchDcpptSoapDokterRepository(noReg string, kdBagian string) (res dto.ResDcpptSoap, err error)
	GetDcpptSoapPasien(kdBagian string, noReg string) (res soap.DcpptSoapPasienModel, err error)
	SaveDcpptSoapPasien(data soap.DcpptSoapPasienModel) (res soap.DcpptSoapPasienModel, err error)
	UpdateInformasiKeluhan(noReg string, kdBagian string, req dto.ReqAsesmenInformasiKeluhanIGD) (res soap.DcpptSoapPasienModel, err error)
	UpdateSkriningResikoDekubitus(noReg string, kdBagian string, req dto.ReqSkriningDekubitusIGD) (res soap.DcpptSoapPasienModel, err error)
	UpdateRiwayatKehamilanRepository(noReg string, kdBagian string, req dto.RequestKehamilanIGD) (res soap.DcpptSoapPasienModel, err error)
	UpdateSkriningNyeriRepository(noReg string, kdBagian string, req dto.RequestSkriningNyeriIGD) (res soap.DcpptSoapPasienModel, err error)
	UpdateTindakLanjutRepository(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD) (res soap.DcpptSoapPasienModel, err error)
	GetTindakLanjutRepository(kdBagian string, noReg string) (res soap.TindakLanjutModel, err error)
	GetPemeriksaanFisikRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error)

	SaveDcpptSoapDokter(data soap.DcpptSoapDokter) (res soap.DcpptSoapDokter, err error)

	// ============================== ASESMEN DOKTER
	UpdateAsesmenDokterRepository(noReg string, kdBagian string, userID string, req dto.RequestAsesmenDokter) (res soap.DcpptSoapDokter, err error)
	GetDccptSoapDokterRepository(kdBagian string, noReg string) (res soap.DcpptSoapDokter, err error)
	GetDccptSoapDokterAsesmenRepository(kdBagian string, userID string, noReg string) (res soap.DcpptSoapDokter, err error)
	GetDccptSoapDokterAsesmenForPerawatRepository(kdBagian string, noReg string) (res soap.DcpptSoapDokter, err error)
	SearchDcpptSoapDokterForAsesmentRepository(userID string, noReg string, kdBagian string) (res dto.ResDcpptSoap, err error)
	UpdateKeadaanUmumBangsalRepository(noReg string, kdBagian string, userID string, req rmeDto.ReqKeadaanUmumBangsal) (res soap.DcpptSoapPasienModel, err error)

	SearchDcpptSoapPasienBangsalRepository(noReg string, userID string, kdBagian string) (res dto.ResDcpptSoap, err error)
	GetDcpptSoapPasienRepository(kdBagian string, noReg string, userID string) (res soap.DcpptSoapPasienModel, err error)
	GetKeluhanUtamaIGDRepository(noReg string, kdBagian string) (res soap.KeluhanUtamaPerawatIGD, err error)

	// KELUHAN UTAMA
	SaveKeluhanUtamaIGDRepository(kdBagian string, tglMasuk string, req dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, err error)
	UpdateKeluhanUtamaIGDRepository(noReg string, kdBagian string, data dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, err error)

	// ===
	GetRiwayatPenyakitSebelumnya(noRm string) (res []soap.RiwayatPenyakit, err error)
	GetDiagnosaRepositoryBangsal(noreg string, kdBagian string) (res []soap.DiagnosaResponse, err error)
	GetDiagnosaBandingRepositoryBangsal(noreg string, kdBagian string) (res soap.DiagnosaBandingResponse, err error)
	GetDiagnosaRepositoryReportIGD(noreg string) (res []soap.DiagnosaResponse, err error)
	GetDiagnosaRepositoryReportBangsal(noreg string, bagian string) (res []soap.DiagnosaResponse, err error)

	// =================================== IGD ======================================== //
	GetTandaVitalIGDRepository(userID string, kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error)
	GetTandaVitalIGDPerawatRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error)

	//
	GetTandaVitalIGDReportdDokterRepository(noreg string) (res soap.DVitalSignIGDDokter, err error)
	//PEMERIKSAAN FISIK
	GetPemFisikIGDDokterRepository(userID string, kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error)
	GetPemFisikIGDPerawatRepository(kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error)

	InsertTandaVitalIGDRepository(data soap.DVitalSignIGDDokter) (res soap.DVitalSignIGDDokter, err error)
	InsertPemFisikIGDDokterIGDRepository(data soap.DPemFisikIGDDokterRepository) (res soap.DPemFisikIGDDokterRepository, err error)
	UpdatePemFisikIGDDokterRespository(kdBagian string, req dto.ReqSaveTandaVitalIGDDokter) (res soap.DPemFisikIGDDokterRepository, err error)
	UpdateTandaVitalIGDDokterRespository(userID string, kdBagian string, req dto.ReqSaveTandaVitalIGDDokter) (res soap.DVitalSignIGDDokter, err error)

	OnUpdatePengkajianKeperawatanDewasaRepository(req dto.ReqSavePengkajianKeperawatan, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error)

	// =================================== KEPERAWATAN ======================================== //
	OnGetPengkajianKeperawatanDewasaRepository(noReg string, kdBagian string, userID string) (res soap.PengkajianAwalKeperawatan, err error)
	OnSavePengkajianKeperawatanRepository(data soap.PengkajianAwalKeperawatan) (res soap.PengkajianAwalKeperawatan, err error)
	OnUpdatePengkajianKeperawatanRepository(req dto.ReqSavePengkajianKeperawatan, kdBagian string, person string) (res soap.PengkajianAwalKeperawatan, err error)
	GetVitalSignKeperawatanBangsalRepository(userID string, kdBagian string, person string, noreg string) (res soap.DVitalSignKeperawatanBangsal, err error)
	InsertVitalSignKeperawatanBangsalRepository(data soap.DVitalSignKeperawatanBangsal) (res soap.DVitalSignKeperawatanBangsal, err error)
	InsertPemFisikKeperawatanBangsalRepository(data soap.DPemFisikKeperawatanBangsalRepository) (res soap.DPemFisikKeperawatanBangsalRepository, err error)
	UpdatePemFisikKeperawatanBangsalRespository(userID string, kdBagian string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res soap.DPemFisikKeperawatanBangsalRepository, err error)
	UpdateTandaVitalIKeperawatanBangsalRespository(userID string, kdBagian string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res soap.DVitalSignKeperawatanBangsal, err error)
	GetPemFisikKeperawatanRepository(userID string, kdBagian string, person string, noreg string) (res soap.DPemFisikKeperawatanBangsalRepository, err error)

	//  USER REPOSITORY
	GetTandaVitalReportIGDDokterRepository(kdBagian string, person string, noreg string) (res soap.DVitalSignIGDDokter, err error)
	GetReportPemFisikIGDDokterRepository(kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error)
	GetTandaVitalReportReportDokterRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error)

	// ====
	OnGetDoubleCheckHightReporitory(noReg string, kdBagian string) (res []soap.DDoubleCheck, err error)
	OnUpdateDcpptSoapPasien(data soap.DcpptSoapPasienModel, kdBagian string, req dto.ReqOnSaveAsesmenIGD) (res soap.DcpptSoapPasienModel, err error)

	// DELETE DATA
	OnDeleteDoubleCheckRepository(idDoubleCheck int) (res soap.DDoubleCheck, err error)
	OnVerfyDoubleCheckHighRepository(req dto.ReqOnVerifyDoubleCheck, userID string) (res soap.DDoubleCheck, err error)
	OnGetViewDoubleCheckRepository(noReg string) (res []soap.DDoubleCheck, err error)
}

/*
Dalam konteks Clean Architecture, mapper biasanya merujuk pada sebuah komponen yang bertanggung jawab untuk mengonversi data dari satu bentuk ke bentuk lainnya. Mapper ini digunakan untuk memetakan data antara berbagai layer dalam aplikasi, seperti antara layer domain dan layer data, atau antara objek domain dengan representasi data yang disimpan di database.

Salah satu tujuan utama dari penggunaan mapper dalam Clean Architecture adalah untuk memisahkan struktur data yang digunakan di dalam domain dengan struktur data yang digunakan di luar domain, misalnya dalam database atau antarmuka pengguna. Dengan adanya mapper, kita dapat dengan mudah mengubah format atau struktur data tanpa perlu memodifikasi langsung objek domain atau struktur bisnis utama, memastikan bahwa perubahan pada representasi data tidak memengaruhi logika bisnis.

Mapper dalam Clean Architecture juga membantu dalam memastikan bahwa perubahan pada entitas atau objek domain tidak secara langsung mempengaruhi representasi data yang tersimpan di sumber daya data eksternal. Ini memungkinkan fleksibilitas dalam pengembangan aplikasi, memungkinkan perubahan pada struktur data tanpa memengaruhi operasi bisnis yang mendasarinya.

Secara keseluruhan, mapper dalam Clean Architecture membantu dalam menjaga pemisahan antara berbagai layer dalam aplikasi, memungkinkan fleksibilitas, dan mempermudah pemeliharaan dan pengembangan kode yang lebih terorganisir.dto.ResponseIGDMethodist
*/

type SoapMapper interface {
	ToResponsePengkajianAwalKeperawatanRANAP(pengkajian igd.PengkajianKeperawatan, pengobatan []kebidanan.DRiwayatPengobatanDirumah) (res dto.ResponsePengkajianAwalKeperawatanDEWASA_RANAP)
	ToMappingReportAsesmenDokterAntonio(riwayatDahulu []rme.KeluhanUtama, keluh rme.AsesemenDokterIGD, pengkajianKeperawatan soap.PengkajianAwalKeperawatan, pemFisik rme.PemeriksaanFisikDokterAntonio, user user.Karyawan, tindakan []soap.TindakanResponse, labor []his.ResHasilLaborTableLama, radiologin []his.RegHasilRadiologiTabelLama) (res dto.ResposeReportAsesmenDokterAntonio)
	ToPengkajianNutrisiMapperSoap(data kebidanan.DPengkajianNutrisi) (res dto.ResponseKebidananModel)
	ToFungsionalKebidananMapper(data kebidanan.DPengkajianFungsional) (res dto.ResDPengkajianFungsionalMapper)
	ToResponseSkalaNyeri(nyeri rme.AsesmenSkalaNyeri) (res dto.ToResponseSkalaNyeri)
	ToMapperPemeriksaanFisik(pemFisik rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksasanFisikAntonio)
	ToMappingResponsePengkajiaKeperawatanRawatInap(pengkajian soap.PengkajianAwalKeperawatan, karyawan user.Karyawan, pemeriksaanFisik dto.ResponsePemeriksasanFisikAntonio, skalaNyeri dto.ToResponseSkalaNyeri, fungsionalMapepr dto.ResDPengkajianFungsionalMapper, resikoJatuh kebidanan.DRisikoJatuhKebidanan, nutrisi dto.ResponseKebidananModel, asuhan []rme.DasKepDiagnosaModelV2, pengobatanDirumah []kebidanan.DRiwayatPengobatanDirumah) (res dto.ResponsePengkajianKeperawatanRawatInap)
	// =============== //
	ToMappingResponseAsesmenIGD(alergi []rme.DAlergi, riwayat []rme.RiwayatPenyakitDahuluPerawat, pengkajian soap.AsesemenIGD) (res dto.ResponseAsesmenIGD)
	ToResponseAsesmenAwalIGD(riwayat []soap.RiwayatPenyakit) (res dto.ResponseAsesmenAwalIGD)
	ToResikoJatuhMapper(data soap.DcpptSoapPasienModel) (res dto.ResponseResikoJatuhGoUpGoTest)
	ToMappingResikoJatuhGoUpAndGoTest(data soap.ResikoJatuhGetUpGoTest) (res dto.ResponseResikoJatuhGoUpGoTest)
	ToResponseDoubleCheckMapper(data []soap.DDoubleCheck) (res []dto.ResponseDoubleCheck)
	ToMapperResponseAsesmenAnak(alergi []rme.DAlergi, perawat soap.PengkajianAwalKeperawatan, riwayat []rme.RiwayatPenyakitDahuluPerawat) (res dto.ResponseAsesmenAnak)
	ToMappingAlasanOpname(data soap.RencanaTindakLanjutModel) (value dto.ResponseTindakLanjutIGDResponse)
	ToSkriningMapper(data soap.DcpptSoapPasien) (value dto.RequestSaveSkrining)
	ToAssesRawatJalanPerawatMapper(data soap.AssementRawatJalanPerawat) (value dto.ResponseAssesRawatJalanPerawat)
	ToOdontogramModel(data reflect.Value, tipe reflect.Type) (value []soap.Odontograms)
	ToOdontogramModel2(data reflect.Value, tipe reflect.Type) (value []soap.Odontograms2)

	ToPemeriksaanGigi(data soap.DcpptOdontogram) (value soap.AssesPemeriksaanGigi)
	ToRiwayat(data soap.DcpptOdontogram) (value soap.AssesRiwayat)
	ToDataMedik(data soap.DcpptOdontogram) (value soap.DataMedik)
	ToIntraOral(data soap.DcpptOdontogram) (value soap.IntraOral)

	// ASSEMENT EDUKASI
	ToKebEdukasi(data reflect.Value) (value []soap.ResAssKebEdukasi)
	KebEdukasi(data soap.KebEdukasi) (value dto.KebEdukasiRes)
	TujuanEdukasi(data soap.KebEdukasi) (value dto.TujuanRes)
	KemBelajar(data soap.KebEdukasi) (value dto.KempBelajarRes)
	MotivasiBelajar(data soap.KebEdukasi) (value dto.MotivasiBelajarRes)
	PilihanPasien(data soap.KebEdukasi) (value dto.PilihanPasienRes)
	ToHambatan(data soap.KebEdukasi) (value dto.HabatanPasienRes)
	ToIntervensi(data soap.KebEdukasi) (value dto.IntervensiRes)
	ToHasil(data soap.KebEdukasi) (value dto.HasilRes)

	// LOKALIS IMAGE
	ToImageLokalis(data soap.ImageLokalisModel) (value dto.ResImageLokalis)
	ToImageLokalisMata(data soap.ImageLokalisModel) (value dto.ResImageLokalis)
	// ==== END LOKALIS

	ToTriaseRiwayatAlergiMapper(data soap.TriaseRiwayatAlergi) (value dto.ResponseTriaseRiwayatAlergi)
	ToResDcpptSoapMapper(kdBagian string, noReg string) (value dto.ResDcpptSoap)
	ToPemeriksaanMapper(data rme.PemeriksaanFisikModel) (value soap.PemeriksaanFisikModel)
	ToMappingPasienInformasiKeluhanIGDResponse(data soap.DcpptSoapPasienModel, riwayat []soap.RiwayatPenyakit) (res rmeDto.ResInformasiDanKeluhanIGD)
	ToMappingResikoDekubitusIGDResponse(data soap.DcpptSoapPasienModel) (res rmeDto.ResResikoDekkubitusIGD)
	ToResRiwayatKehamilan(data soap.DcpptSoapPasienModel) (res dto.ResRiwayatKehamilanIGD)
	ToResSkriningNyeri(data soap.DcpptSoapPasienModel) (res dto.ResSkriningNyeriIGD)
	ToMapperTindakLanjut(data soap.DcpptSoapPasienModel) (res dto.ResponseTindakLanjutIGD)
	ToMapperVitalSignBangsal(data soap.DcpptSoapPasienModel, dpemFisik rme.PemeriksaanFisikModel) (res dto.ResponseVitalSignBangsal)

	// ===== MAPPER
	ToMapperResponseAsesmenDokter(data soap.DcpptSoapDokter) (res dto.ResponseAsesmenDokter)
	ToMapperResponseKeadaanUmum(data soap.DcpptSoapPasienModel) (res dto.ResponseToKeadaanUmum)
	ToMapperResponseIGDDokter(data1 soap.DVitalSignIGDDokter, data2 soap.DPemFisikIGDDokterRepository) (value dto.TandaVitalIGDResponse)

	// ================================ MAPPER KEPERAWATAN ==========================
	TOMappingResponsePengkajianKeperawatan(data soap.PengkajianAwalKeperawatan, alergi []rme.DAlergi, riwayat []rme.RiwayatPenyakitDahuluPerawat) (value dto.ResponseAsesmenKeperawatan)
	ToMapperResponseKeperawatanBangsal(data1 soap.DVitalSignKeperawatanBangsal, data2 soap.DPemFisikKeperawatanBangsalRepository) (value dto.TandaVitalBangsalKeperawatanResponse)
}
