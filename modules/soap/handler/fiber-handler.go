package handler

import (
	"errors"
	"fmt"
	rmeDto "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/pkg/helper"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

/*
INSERT ASSEMENT
RAWAT JALAN PERAWAT
*/

func (sh *SoapHandler) SaveAssesmentRawatJalanPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSaveAssementRawatJalanPerawatV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	_, errs = sh.SoapUseCase.SaveAssementRawatJalanPerawatUseCaseV2(*payload)
	if errs != nil {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		return c.Status(fiber.StatusCreated).JSON(errorMessage)
	}

	get, _ := sh.SoapRepository.GetAssementRawatJalanPerawatRepository(payload.NoReg)

	mapper := sh.SoapMapper.ToAssesRawatJalanPerawatMapper(get)

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetSkriningFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestGetSkriningV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	get, errs := sh.SoapRepository.GetSkriningRepository(payload.NoReg)

	if errs != nil || get.Noreg == "" || len(get.Noreg) == 0 {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusAccepted, errorMessage)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data := sh.SoapMapper.ToSkriningMapper(get)

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
List Odontogram, mendapatkan data odontogram dalam bentuk list
*/
func (sh *SoapHandler) ListOdontogramFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestGetDataV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// GET ODONTOGRAM
	m := map[string]any{}
	odon, err := sh.SoapRepository.GetOdontogramRepository(payload.NoReg)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusAccepted, errorMessage)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if len(odon) == 0 {
		sh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	m["list"] = odon
	response := helper.APIResponse("Ok", http.StatusOK, m)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveAssmentRawatJalanDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSaveAssementRawatJalanDokterV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID").(string)

	_, errs = sh.SoapUseCase.SaveAssesRawatJalanDokterUsecaseV2(userID, *payload)

	if errs != nil {
		response := helper.APIResponse("Data gagal diproses", http.StatusCreated, errs.Error())
		c.Status(fiber.StatusCreated).JSON(response)
	}

	data, _ := sh.SoapUseCase.GetAssessRawatJalanUseCase(payload.NoReg)

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusCreated).JSON(response)
}

func (sh *SoapHandler) SaveAnamnesaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSaveAnamnesaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	errs = sh.SoapRepository.SaveAnamnesaRepositoryV2(*payload)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
Insert Odontogram, berfungsi untuk mendapatkan data odontogram
*/
func (sh *SoapHandler) InsertOdontogramFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.InsertOdontogramV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	insert, errs := sh.SoapRepository.InsertOndontogramRepositoryV2(*payload)

	if errs != nil {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, errorMessage)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, insert)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
Fungsi untuk Mendpatkan Data Asesmen Rawat Jalan Dokter
*/
func (sh *SoapHandler) GetAssesRawatJalanDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestGetDataV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err := sh.SoapUseCase.GetAssessRawatJalanUseCase(payload.NoReg)

	if err != nil {
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("Data berhasil disimpan")
	return c.JSON(response)
}

/*
Delete Odontogram, Fungsi yang dibuat untuk mengapus data odontogram
*/
func (sh *SoapHandler) DeleteOdontogramV2(c *fiber.Ctx) error {
	payload := new(dto.RequestDeleteOdontogramV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	errs = sh.SoapUseCase.DeleteOdontogramUsecaseV2(*payload)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data gagal disimpan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, payload)
	sh.Logging.Info("Data berhasil disimpan")
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
Save Single Odontogram
*/
func (sh *SoapHandler) SaveSingleOdontogramFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.RequestSaveOdontogramV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	errs = sh.SoapUseCase.InsertOdontogramUseCaseV2(*payload)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data gagal disimpan", http.StatusCreated)
		return c.JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, payload)
	sh.Logging.Info("Data berhasil disimpan")
	return c.JSON(response)
}

/*
Save Skrining
*/
func (sh *SoapHandler) SaveSkriningFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSaveSkriningV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	detail, errs := sh.SoapRepository.DetailSkriningRepository(payload.NoReg)

	if errs != nil || detail.Noreg == "" {
		// SIMPAN DATA BARU
		save, errs1 := sh.SoapRepository.SaveSkriningRepositoryV2(*payload)

		if errs1 != nil {
			sh.Logging.Info(errs1.Error())
			response := helper.APIResponseFailure("Data gagal disimpan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		data := sh.SoapMapper.ToSkriningMapper(save)
		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		c.Status(fiber.StatusOK).JSON(response)
	}

	if len(detail.Noreg) > 1 {
		// UPDATE DATA
		_, err := sh.SoapRepository.UpdateSkriningRepositoryV2(*payload)

		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		get, _ := sh.SoapRepository.GetSkriningRepository(payload.NoReg)

		data := sh.SoapMapper.ToSkriningMapper(get)

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		return c.Status(fiber.StatusOK).JSON(response)
	} else {
		response := helper.APIResponseFailure("Data gagal diproses", http.StatusCreated)
		sh.Logging.Info("Data gagal diproses")
		return c.Status(fiber.StatusCreated).JSON(response)
	}
}

func (sh *SoapHandler) UploadImageOdontogramFiberHandler(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	tipe := strings.Split(files.Filename, ".")

	path := fmt.Sprintf("images_odon/%s.%s", noReg, tipe[1])

	// UPLOAD FILE
	errs := c.SaveFile(files, path)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	// SIMPAN DATA
	data := dto.ResUploadOdontogram{ImageUrl: path, NoReg: noReg}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveAnatomiPublicUploadFiberHandler(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var nama = c.FormValue("nama")
	var norm = c.FormValue("norm")
	var keterangan = c.FormValue("keterangan")
	var userID = c.FormValue("userid")

	if nama == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if norm == "" {
		response := helper.APIResponseFailure("Norm harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if keterangan == "" {
		response := helper.APIResponseFailure("Keterangan harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if userID == "" {
		response := helper.APIResponseFailure("Keterangan harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	now := time.Now().Unix()
	s := strconv.Itoa(int(now))
	path := fmt.Sprintf("images/anatomi/%s-%s", s, files.Filename)

	errs := c.SaveFile(files, path)

	if errs != nil {
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	an, errs1 := sh.SoapRepository.SaveAnatomiRepositoryV2(nama, norm, keterangan, path, userID)

	if errs1 != nil {
		message := fmt.Sprintf("Gambar berhasil diupload, tetapi data tidak dapat disimpan %s", errs1)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, an)
	sh.Logging.Info("Data berhasil disimpan")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveAnatomiFiberHandler(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var nama = c.FormValue("nama")
	var norm = c.FormValue("norm")
	var keterangan = c.FormValue("keterangan")

	if nama == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if norm == "" {
		response := helper.APIResponseFailure("Norm harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	if keterangan == "" {
		response := helper.APIResponseFailure("Keterangan harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	userID := c.Locals("userID")

	now := time.Now().Unix()
	s := strconv.Itoa(int(now))
	path := fmt.Sprintf("images/anatomi/%s-%s", s, files.Filename)

	errs := c.SaveFile(files, path)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	an, errs := sh.SoapRepository.SaveAnatomiRepositoryV2(nama, norm, keterangan, path, userID.(string))

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, an)
	sh.Logging.Info("Data berhasil disimpan")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetAnamnesaFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.RequestGetDataV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, errs := sh.SoapRepository.GetAnamnesaRepository(payload.NoReg)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetRawatJalanPerawatFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.RequestGetDataV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	get, errs := sh.SoapRepository.GetAssementRawatJalanPerawatRepository(payload.NoReg)

	if errs != nil || get.Noreg == "" {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, errorMessage)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	sh.Logging.Info(get)

	mapper := sh.SoapMapper.ToAssesRawatJalanPerawatMapper(get)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) InsertDiagnosaFiberHandler(c *fiber.Ctx) error {

	payload := new(dto.InsertDiagnosaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, payload)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
Get Asesesmen Kebidanan Edukasi V2
*/
func (sh *SoapHandler) GetAssesKebEdukasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestGetDataV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	edu, err := sh.SoapUseCase.GetKebutuhanEdukasiUseCase(payload.NoReg)

	if err != nil {
		sh.Logging.Error(err.Error())
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, "")
		return c.Status(fiber.StatusAccepted).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, edu)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) SaveAssesKebEdukasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqKebEdukasiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// UPDATE ASSESMENT KEBUTUHAN EDUKASI
	_, errs = sh.SoapRepository.SaveAssesKebEdukasiRepositoryV2(*payload)

	if errs != nil {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse(errs.Error(), http.StatusCreated, errorMessage)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data Berhasil Disimpan", http.StatusOK, payload)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusCreated).JSON(response)
}

// GET INTRA OPERASI
func (sh *SoapHandler) GetIntraOperasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err := sh.SoapRepository.GetIntraOperasiRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// TODO: INI ADALAH ENDPOINT BARU
func (sh *SoapHandler) HasilPenunjangMedikFiberHandler(c *fiber.Ctx) error {
	// ENDPOINT YANG DIGUNAKAN UNTUK MENAMPILKAN HASIL PENUNJANG MEDIK
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err := sh.SoapRepository.GetHasilPenunjangMedikRepository(payload.Noreg)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		sh.Logging.Info("Data kosong")
		response := helper.APIResponseFailure("Data kosong", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) InputSingleDiagnosaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInputSingleDiagnosaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	data, _ := sh.SoapRepository.SearchDcpptSoapDokterRepository(payload.Noreg, modulID)

	if len(data.Noreg) < 1 {
		tanggal, errs := sh.SoapRepository.GetJamMasukPasienRepository(payload.Noreg)

		if errs != nil || tanggal.Tanggal == "" {
			sh.Logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			response := helper.APIResponseFailure(message, http.StatusCreated)
			return c.JSON(response)
		}

		_, err1 := sh.SoapRepository.InsertSingleDiagnosaFirstRepositoryV2(*payload, tanggal.Jam, tanggal.Tanggal[0:10], modulID, userID)

		if err1 != nil {
			sh.Logging.Error("ERROR INPUT SINGLE DIAGNOSA < 1")
			sh.Logging.Error(err1.Error())
			response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
			return c.JSON(response)
		}
	}

	// INPUT SINGLE DIAGNOSA
	_, errs = sh.SoapRepository.InputSingleDiagnosaRepositoryV2(*payload, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	value, errs := sh.SoapRepository.GetDiagnosaRepository(payload.Noreg, modulID, userID)

	sh.Logging.Info(value)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	values, _ := sh.SoapRepository.GetDiagnosaRepositoryBangsal(payload.Noreg, modulID)

	if len(values) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, values)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetDiagnosaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info("GET DATA DIAGNOSA")
	sh.Logging.Info(userID)

	value, err := sh.SoapRepository.GetDiagnosaRepositoryBangsal(payload.Noreg, modulID)

	// GetDiagnosaRepositoryBangsal(noreg string, kdBagian string) (res []soap.DiagnosaResponse, err error)
	sh.Logging.Info(value)

	if err != nil || len(value) == 0 {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, value)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetDiagnoabandingFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDiagnosaBanding)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info(modulID)
	sh.Logging.Info(userID)

	value, err := sh.SoapRepository.GetDiagnosaBandingRepositoryBangsal(payload.Noreg, modulID)

	if err != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	sh.Logging.Info("DIAGNOSA BANDING")
	sh.Logging.Info(value)

	response := helper.APIResponse("OK", http.StatusOK, value)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) DeleteDiagnosaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDeleteDiagnosaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// LAKUKAN DELETE DATA
	errs = sh.SoapRepository.DeleteDiagnosaRepositoryV2(*payload, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data gagal dihapus", http.StatusCreated)
		c.JSON(response)

	}

	value, _ := sh.SoapRepository.GetDiagnosaRepository(payload.Noreg, modulID, userID)

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data kosong", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil dihapus", http.StatusOK, value)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusCreated).JSON(response)
}

func (sh *SoapHandler) GetKeluhanUtamaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err1 := sh.SoapRepository.GetKeluhanUtamaIGDRepository(payload.Noreg, modulID)

	sh.Logging.Info("GET KELUHAN UTAMA IGD FIBER HANDLER")

	if err1 != nil {
		sh.Logging.Info(err1.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveKeluhanUtamaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegSaveKeluhanUtamaIGD)
	errs := c.BodyParser(&payload)

	sh.Logging.Info("Receive payload")
	sh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	simpan, message, err2 := sh.SoapUseCase.SaveKeluhanUtamaIGDUseCase(modulID, *payload)

	if err2 != nil || simpan.Noreg == "" {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("message", http.StatusOK, simpan)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

// TODO : GET INFORMASI MEDIS // GET DATA INFROMASI MEDIS
func (sh *SoapHandler) GetInformasiMedisFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDataPasienV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// LAKUKAN PENCARIAN DATA PASIEN
	data, errs := sh.SoapRepository.GetInformasiMedisRepository(*payload)
	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveInformasiMedisFiberHandler(c *fiber.Ctx) error {
	// GET DATA INFROMASI MEDIS
	payload := new(dto.ReqInformasiMedisV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// CARI TERBELIH DAHULU APAKAH SUDAH PERNAH DI INPUT?
	value, err := sh.SoapRepository.SearchDcpptSoapPasienRepository(payload.Noreg, payload.KdBagian)
	if err != nil || value.Noreg == "" {
		// SIMPAN DATA
		// LAKUKAN PERINTAH INSERT
		data, err := sh.SoapRepository.InsertInformasiMedisRepositoryV2(*payload)
		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak dapat disimpan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)

	}

	// UPDATE DATA
	data, err := sh.SoapRepository.UpdateInformasiMedisRepositoryV2(*payload)
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak dapat disimpan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// SIMPAN INFORMASI MEDIS

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) UploadOdontogramFiberHandler(c *fiber.Ctx) error {
	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	tipe := strings.Split(files.Filename, ".")

	path := fmt.Sprintf("images/odontogram/%s.%s", noReg, tipe[1])
	_ = c.SaveFile(files, path)

	_, errs := sh.SoapUseCase.UploadOdontogramUseCase(modulID, noReg, path)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	sh.Logging.Info("Data berhasil disimpan")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) PublisImageOdontogramV2(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("Nama harus diisi", http.StatusAccepted)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	tipe := strings.Split(files.Filename, ".")

	modulID := c.Locals("modulID").(string)

	path := fmt.Sprintf("images_odon/%s.%s", noReg, tipe[1])
	_ = c.SaveFile(files, path)

	data, errs := sh.SoapUseCase.PublishImageOdontogramUseCase(noReg, path, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info("Data berhasil disimpan")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetAsesmedAnamnesaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// GET DATA ASEMEDANAMNESA
	data, err := sh.SoapRepository.GetAsesmedAnamnesaRepository(payload.Noreg, modulID)
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

/*
SIMPAN ANAMNESA
*/
func (sh *SoapHandler) SaveAsesmedAnamnesaFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveAsesmedAnamnesaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	value, _ := sh.SoapRepository.SearchDcpptSoapPasienRepository(payload.Noreg, modulID)

	if len(value.Noreg) < 1 {
		// GET DATA ASEMEDANAMNESA
		sh.Logging.Info("Simpan data anamnesa")
		data, err := sh.SoapRepository.SaveAsesmedAnamnesaRepositoryV2(*payload, modulID)

		if err != nil {
			sh.Logging.Error(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		return c.Status(fiber.StatusOK).JSON(response)

	} else {
		sh.Logging.Info("Update data anamnesa")
		data, err := sh.SoapRepository.UpdateDataAnamnesaRepositoryV2(*payload, modulID)
		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.JSON(response)
		}

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		return c.Status(fiber.StatusOK).JSON(response)

	}
}

func (sh *SoapHandler) GetAnamnesaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	sh.Logging.Info(modulID)

	data, err := sh.SoapRepository.GetAnamnesaIGDRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)

}

// ============================ SIMPAN ANAMNESA IGD
func (sh *SoapHandler) SaveAnamnesaIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.AnamnesaIGDV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	sh.Logging.Info(modulID)

	data, message, err := sh.SoapUseCase.SimpanAnamnesaIGDUsecaseV2(modulID, *payload)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	sh.Logging.Info(message)
	return c.JSON(response)
}

// DATA MEDIK YANG DIPERLUKAN
func (sh *SoapHandler) GetDataMedikYangDiperlukanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err := sh.SoapRepository.GetDataMedikRepository(payload.Noreg, modulID)
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveDataMedikYangDiperlukanV2(c *fiber.Ctx) error {
	payload := new(dto.ReqDataMedikV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	modulID := c.Locals("modulID").(string)

	// cek apakah data sudah ada
	value, _ := sh.SoapRepository.SearchDcpptSoapPasienRepository(payload.Noreg, modulID)

	if len(value.Noreg) < 1 {

		data, err := sh.SoapRepository.SaveDataMedikRepositoryV2(*payload, modulID)

		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("OK", http.StatusOK, data)
		sh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)
	} else {
		data, err := sh.SoapRepository.UpdateDataMedikRepositoryV2(*payload, modulID)

		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.JSON(response)

		}

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		return c.Status(fiber.StatusOK).JSON(response)
	}

}

func (sh *SoapHandler) GetDataIntraOralFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// Get data
	data, err := sh.SoapRepository.GetDataIntraOralRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveIntraOralFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDataIntraOralV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	value, _ := sh.SoapRepository.SearchDcpptSoapPasienRepository(payload.Noreg, modulID)

	sh.Logging.Info(value)

	if len(value.Noreg) < 1 {
		// Simpan
		data, err := sh.SoapRepository.InsertDataIntraOralRepositoryV2(*payload, modulID)
		sh.Logging.Info("INSERT DATA INTRA ORAL")
		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		sh.Logging.Info("Data berhasil disimpan")
		return c.Status(fiber.StatusOK).JSON(response)

	} else {
		// Update
		data, err := sh.SoapRepository.UpdateDataIntraOralRepositoryV2(*payload, modulID)
		sh.Logging.Info("Update data intra oral")

		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)
		}

		response := helper.APIResponse("Data berhasil diubah", http.StatusOK, data)
		sh.Logging.Info("Data berhasil diubah")
		return c.Status(fiber.StatusOK).JSON(response)
	}
}

func (sh *SoapHandler) SavePascaOperasiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqPascaOperasiV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, _ := sh.SoapRepository.SearchDcpptSoapPasienRepository(payload.Noreg, modulID)

	if len(data.Noreg) < 1 {
		// Insert Single
		data, err := sh.SoapRepository.InsertPascaOperasiRepositoryV2(*payload, modulID)

		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure("Data tidak dapat disimpan", http.StatusCreated)
			return c.JSON(response)

		}

		response := helper.APIResponse("OK", http.StatusOK, data)
		sh.Logging.Info("OK")
		c.JSON(response)

	} else {
		// Update
		data, errs := sh.SoapRepository.SavePascaOperasiRepositoryV2(*payload, modulID)

		if errs != nil {
			sh.Logging.Info(errs.Error())
			response := helper.APIResponseFailure("Data tidak dapat disimpan", http.StatusCreated)
			return c.Status(fiber.StatusCreated).JSON(response)

		}

		response := helper.APIResponse("Data berhasi disimpan", http.StatusOK, data)
		sh.Logging.Info(response)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponseFailure("Data gagal diproses", http.StatusCreated)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusCreated).JSON(response)
}

func (sh *SoapHandler) SaveIntraOperasiFiberHandler(c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON("")
}

func (sh *SoapHandler) GetTriaseFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// -III- //
	data, err := sh.SoapRepository.GetTriaseRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetRiwayatAlergiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err1 := sh.SoapRepository.GetTriaseRiwayatAlergi(payload.Noreg, modulID)

	if err1 != nil {
		sh.Logging.Info(err1.Error())
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	// MAPPER DATA
	mapper := sh.SoapMapper.ToTriaseRiwayatAlergiMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) SaveRiwayatAlergiFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveTriaseRiwayatAlergi)
	errs := c.BodyParser(&payload)

	sh.Logging.Info("Receive payload")
	sh.Logging.Info(payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, message, errs1 := sh.SoapUseCase.SaveTriaseRiwayatAlergiUsecase(modulID, *payload)

	sh.Logging.Info("Data save triase")
	sh.Logging.Info(data)

	if errs1 != nil || data.Noreg == "" {
		sh.Logging.Info(errs1.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPER DATA
	sh.Logging.Info(data)
	mapper := sh.SoapMapper.ToTriaseRiwayatAlergiMapper(data)

	response := helper.APIResponse(message, http.StatusOK, mapper)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveTriaseFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegTriaseV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// USECASE TRIASE
	data, message, err := sh.SoapUseCase.SimpanTriaseUseCaseV2(modulID, *payload)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) UploadImageLokalisFiberHandler(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("noReg tidak diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	tipe := strings.Split(files.Filename, ".")

	times := time.Now()

	modulID := c.Locals("modulID").(string)

	path := fmt.Sprintf("%s-%s-%s.%s", times.Format("20060102150405"), noReg, modulID, tipe[1])

	_ = c.SaveFile(files, "images/lokalis/"+path)

	data, message, err := sh.SoapUseCase.SaveImageLokalisUseCase(modulID, noReg, path)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusOK).JSON(response)
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse(message, http.StatusOK, image)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) GetImageLokalisFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err := sh.SoapRepository.GetImageLokalisRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse("OK", http.StatusOK, image)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OngetLokalisImageMataFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, err := sh.SoapRepository.GetImageLokalisRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	image := sh.SoapMapper.ToImageLokalisMata(data)

	response := helper.APIResponse("OK", http.StatusOK, image)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) UploadImageLokalisFiberHandlerPublic(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")
	var keteranganPerson = c.FormValue("person")
	var insertUserId = c.FormValue("user_id")
	var deviceId = c.FormValue("device_id")
	var pelayanan = c.FormValue("pelayanan")
	var bagian = c.FormValue("bagian")
	// TANGKAP DATA USER ID

	if noReg == "" {
		response := helper.APIResponseFailure("noReg tidak diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)

	// file := payload.File
	tipe := strings.Split(files.Filename, ".")

	times := time.Now()

	path := fmt.Sprintf("%s-%s-%s.%s", times.Format("20060102150405"), noReg, bagian, tipe[1])

	_ = c.SaveFile(files, "images/lokalis/"+path)

	// simpan data
	sh.Logging.Info("Simpan data")
	data, message, err := sh.SoapUseCase.SaveImageLokalisPublicUsecase(bagian, noReg, path,
		keteranganPerson, insertUserId, deviceId, pelayanan)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse(message, http.StatusOK, image)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) UploadImageLokalisPublicFiberHandler(c *fiber.Ctx) error {

	var noReg = c.FormValue("noReg")

	if noReg == "" {
		response := helper.APIResponseFailure("noReg tidak diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var kdBagian = c.FormValue("kdBagian")

	if kdBagian == "" {
		response := helper.APIResponseFailure("kode bagian tidak diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	sh.Logging.Info("LAKUKAN UPLOAD IMAGE LOKALIS")
	data, message, err := sh.SoapUseCase.SaveImageLokalisUsecase(kdBagian, noReg, c)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse(message, http.StatusOK, image)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) UploadImageLokalisFiberHandlerPrivate(c *fiber.Ctx) error {

	files, err := c.FormFile("imageUrl")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	var noReg = c.FormValue("noReg")
	var keteranganPerson = c.FormValue("person")
	var insertUserId = c.FormValue("user_id")
	var deviceId = c.FormValue("device_id")
	var pelayanan = c.FormValue("pelayanan")
	modulID := c.Locals("modulID").(string)

	if noReg == "" {
		response := helper.APIResponseFailure("noReg tidak diisi", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// file := payload.File
	tipe := strings.Split(files.Filename, ".")

	times := time.Now()

	path := fmt.Sprintf("%s-%s-%s.%s", times.Format("20060102150405"), noReg, modulID, tipe[1])

	_ = c.SaveFile(files, "images/lokalis/"+path)

	// simpan data
	sh.Logging.Info("Simpan data")
	data, message, err := sh.SoapUseCase.SaveImageLokalisPublicUsecase(modulID, noReg, path,
		keteranganPerson, insertUserId, deviceId, pelayanan)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse(message, http.StatusOK, image)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) SimpanPemeriksaanFisikFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSavePemeriksaanFisikV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	//  SAVE PEMERIKSAAN FISIK
	data, message, err := sh.SoapUseCase.SavePemeriksaanFisikUseCaseV2(modulID, *payload)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetPemeriksaanFisikFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// modulID := c.Locals("modulID").(string)

	// // GET PEMERIKSAAN FISIK
	// data, err := sh.SoapRepository.GetPemeriksaanFisikRepository(modulID, payload.Noreg)

	// if err != nil {
	// 	sh.Logging.Info(err.Error())
	// 	response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// if data.Noreg == "" {
	// 	response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
	// 	return c.Status(fiber.StatusCreated).JSON(response)
	// }

	// // MAPPER TO soap.PemeriksaanFisikModel
	// mapper := sh.SoapMapper.ToPemeriksaanMapper(data)

	response := helper.APIResponse("OK", http.StatusOK, "mapper")
	sh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) PilihPemeriksaanLaborFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestPilihPemeriksaanLaborV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET PEMERIKSAAN FISIK
	data, err := sh.SoapUseCase.PilihPemeriksaanLaborUsecase(payload.NameGrup)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) DetailPemeriksaanLaborFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestPilihPemeriksaanLaborV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET PEMERIKSAAN FISIK
	data, err := sh.SoapUseCase.DetailPemeriksaanLaborUseCase(payload.NameGrup)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetAsesmedKeperawatanBidanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// GET ASESMEN KEPERAWATAN
	data, errs := sh.SoapRepository.GetAsesmedKeperawatanBidanRepository(payload.Noreg, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

// MAPPING
func (sh *SoapHandler) GetAsesmenInfoKeluhanIGdHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInfoKeluh)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// LAKUKAN GET INFORMASI & KELUHAN KEPERAWATAN

	data, errs1 := sh.SoapRepository.GetDcpptSoapPasien(modulID, payload.Noreg)
	riwayat, _ := sh.SoapRepository.GetRiwayatPenyakitSebelumnya(payload.NoRm)

	// GET RIWAYAT PENYAKIT SEBELUMNYA

	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING HASIL KAJI
	mapper := sh.SoapMapper.ToMappingPasienInformasiKeluhanIGDResponse(data, riwayat)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetAsesmenAwalIGDHangler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res := sh.SoapUseCase.OnGetAsesmenIGDUseCase(userID, modulID, *payload)

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveAsesmenAwalIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnSaveAsesmenIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// sh.Logging.Info("SAVE DATA")
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info("SAVE DATA")
	sh.Logging.Info(payload)
	sh.Logging.Info("KELUHAN UTAMA")
	sh.Logging.Info(payload.KeluhanUtama)

	res, message, er123 := sh.SoapUseCase.OnSaveAsesmenIGDUseCase(userID, modulID, *payload)

	if er123 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetAsesmenResikoJatuhGoUpAndGoTestFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResikoJatuhGoUpGoTest)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// GET ASESMEN RESIKO JATUH GO UP AND GO TEST
	modulID := c.Locals("modulID").(string)

	resiko, er123 := sh.SoapRepository.GetResikoJatuhGoUpGoTestRepository(payload.Noreg, modulID)

	if er123 != nil {
		response := helper.APIResponseFailure(er123.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING RESIKO JATUH GO UP AND GO TEST
	mapper := sh.SoapMapper.ToMappingResikoJatuhGoUpAndGoTest(resiko)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveAsesmenResikoJatuhGoUpAndGoTestFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqResikoJatuhGoUpAndGoTest)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	// ON SAVE ASESMEN RESIK JATUH GO UP AND GO TEST

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	res, message, er12 := sh.SoapUseCase.OnSaveResikoJatuhGoUpAndGoTestIGDUseCase(modulID, userID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ASESMEN INFO KELUHAN IGD HANDLER
func (sh *SoapHandler) SaveAsesmenInfoKeluhanIGdHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenInformasiKeluhanIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, errs1 := sh.SoapUseCase.SaveAsesmenInfoKeluarUsecase(modulID, userID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

// SIMPAN DATA ASESMEND KEPERAWATAN BIDAN
func (sh *SoapHandler) SaveAsesmedKeperawatanBidanFiberHandler(c *fiber.Ctx) error {
	// SAVE KEPERAWATAN BIDAN
	payload := new(dto.ReqSaveAsesmenKeperawatanBidanV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// USECASE SAVE DATA
	modulID := c.Locals("modulID").(string)

	save, message, err := sh.SoapUseCase.SaveKeperawatanBidanUseCaseV2(modulID, *payload)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, save)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveSkriningResikoDekubituIGDHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSkriningDekubitusIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}
	sh.Logging.Info("Data objek json di dapat")

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ====================== USECASE SAVE INFORMASI DEKUBITUS IGD HANDLLER
	// SAVE ASESMEN INFO // KELUHAN IGD USECASE
	data, message, errs1 := sh.SoapUseCase.SaveSkriningResikoDekubitusUsecase(modulID, userID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetSkriningResikoDekubitusHandler(c *fiber.Ctx) error {

	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// ============ LAKUKAN GET INFORMASI & KELUHAN KEPERAWATAN  ============ //

	sh.Logging.Info("Cari data informasi keluhan pasien")
	data, errs1 := sh.SoapRepository.GetDcpptSoapPasien(modulID, payload.Noreg)
	sh.Logging.Info(data)

	if errs1 != nil {
		sh.Logging.Info(errs1.Error())
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING HASIL KAJI
	mapper := sh.SoapMapper.ToMappingResikoDekubitusIGDResponse(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetRiwayatKehamilanHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// ============ LAKUKAN GET INFORMASI & KELUHAN KEPERAWATAN  ============ //
	sh.Logging.Info("Cari data informasi keluhan pasien")
	data, errs1 := sh.SoapRepository.GetDcpptSoapPasien(modulID, payload.Noreg)
	sh.Logging.Info(data)

	if errs1 != nil {
		sh.Logging.Info(errs1.Error())
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING HASIL KAJI
	mapper := sh.SoapMapper.ToResRiwayatKehamilan(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveRiwayatKehamilanHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestKehamilanIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()
	sh.Logging.Info("FALIDASI PAYLOAD RIWAYAT KEHAMILAN")
	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info(payload)
	// SAVE RIWAYAT KEHAMILAN USECASE HANDLER
	sh.Logging.Info("SAVE DATA INFORMAI KEHAMILAN")
	data, mesage, err1 := sh.SoapUseCase.SaveRiwayatKehamilanUsecase(modulID, userID, *payload)

	if err1 != nil {
		sh.Logging.Info(err1.Error())
		response := helper.APIResponseFailure(mesage, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(mesage, http.StatusOK, data)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// ======================================== GET SKRINING NYERI ================================== //
func (sh *SoapHandler) GetSkriningNyeriHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, errs1 := sh.SoapRepository.GetDcpptSoapPasien(modulID, payload.Noreg)

	if errs1 != nil {
		sh.Logging.Info(errs1.Error())
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING HASIL KAJI
	mapper := sh.SoapMapper.ToResSkriningNyeri(data)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ====================================== SAVE SKRINING NYERI ================================== //
func (sh *SoapHandler) SaveSkriningNyeriHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestSkriningNyeriIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ====================== USECASE SAVE INFORMASI DEKUBITUS IGD HANDLLER
	// SAVE ASESMEN INFO // KELUHAN IGD USECASE
	data, message, errs1 := sh.SoapUseCase.SaveSkriningNyeriUseCase(userID, modulID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToResSkriningNyeri(data)

	response := helper.APIResponse(message, http.StatusOK, mapper)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTindakLanjutHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	data, errs1 := sh.SoapRepository.GetDcpptSoapPasien(modulID, payload.Noreg)

	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := sh.SoapMapper.ToMapperTindakLanjut(data)

	sh.Logging.Info("GET DATA TINDAK LANJUT")
	sh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) SaveTindakLanjuthandler(c *fiber.Ctx) error {
	payload := new(dto.RequestInputTindakLanjutIGD)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info("PAY DATA")
	sh.Logging.Info(payload)

	sh.Logging.Info(payload.Jam)
	sh.Logging.Info(payload.Menit)

	// SAVE ASESMEN INFO // KELUHAN IGD USECASE
	data, message, errs1 := sh.SoapUseCase.SaveTindakLanjutUseCase(userID, modulID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToMapperTindakLanjut(data)

	response := helper.APIResponse(message, http.StatusOK, mapper)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *SoapHandler) GetVitalSignBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoreg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// CARI DPCCPT SOAP PASIEN

	mapper := rh.SoapUseCase.GetVitalSignBangsalUsecase(modulID, payload.Noreg)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (rh *SoapHandler) SaveKeadaanUmumBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(rmeDto.ReqKeadaanUmumBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// VITAL SIGN USECASE //
	data, mesage, errs := rh.SoapUseCase.SaveKeadaanUmumUseCase(modulID, payload.Noreg, userID, *payload)

	if errs != nil {
		rh.Logging.Info("DATA GAGAL DI DAPAT")
		response := helper.APIResponseFailure(mesage, http.StatusCreated)
		rh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := rh.SoapMapper.ToMapperResponseKeadaanUmum(data)
	rh.Logging.Info("MAPPING DATA")
	rh.Logging.Info(mapper)

	response := helper.APIResponse(mesage, http.StatusOK, mapper)
	rh.Logging.Info(mesage)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetKeadaanUmumBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// SAVE ASESMEN INFO // KELUHAN IGD USECASE

	data, ers := sh.SoapRepository.GetDcpptSoapPasienRepository(modulID, payload.Noreg, userID)

	if ers != nil {
		sh.Logging.Info("DATA GAGAL DI DAPAT")
		response := helper.APIResponseFailure("Data gagal diperoleh", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToMapperResponseKeadaanUmum(data)
	sh.Logging.Info("MAPPING DATA")
	sh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// ============================================ ASESMEN DOKTER ======================================== //
func (sh *SoapHandler) SaveAsesmenDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestAsesmenDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// ====================== USECASE SAVE INFORMASI DEKUBITUS IGD HANDLLER
	// SAVE ASESMEN INFO // KELUHAN IGD USECASE
	data, message, err2 := sh.SoapUseCase.SaveAsesmenDokterUseCase(userID, modulID, *payload)

	if err2 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING KE ANEMNESA
	mapper := sh.SoapMapper.ToMapperResponseAsesmenDokter(data)

	response := helper.APIResponse(message, http.StatusOK, mapper)
	sh.Logging.Info("message")
	return c.Status(fiber.StatusOK).JSON(response)
}

// ===============================  GET ASESMEN DOKTER ============================ //
func (sh *SoapHandler) GetAsesmenDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// SAVE ASESMEN INFO // KELUHAN IGD USECASE

	data, ers := sh.SoapRepository.GetDccptSoapDokterAsesmenRepository(modulID, userID, payload.Noreg)

	if ers != nil {
		sh.Logging.Info("DATA GAGAL DI DAPAT")
		response := helper.APIResponseFailure("Data gagal diperoleh", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToMapperResponseAsesmenDokter(data)
	sh.Logging.Info("MAPPING DATA")
	sh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) FindAsesmenDokterToPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// SAVE ASESMEN INFO // KELUHAN IGD USECASE

	data, ers := sh.SoapRepository.GetDccptSoapDokterAsesmenForPerawatRepository(modulID, payload.Noreg)

	if ers != nil {
		sh.Logging.Info("DATA GAGAL DI DAPAT")
		response := helper.APIResponseFailure("Data gagal diperoleh", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToMapperResponseAsesmenDokter(data)
	sh.Logging.Info("MAPPING DATA")
	sh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) HistoryAsesmenPasienFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// GET HISTORY ASESMEN PASIEN DOKTER

	data, ers := sh.SoapRepository.GetDccptSoapDokterAsesmenRepository(modulID, userID, payload.Noreg)

	if ers != nil {
		sh.Logging.Info("DATA GAGAL DI DAPAT")
		response := helper.APIResponseFailure("Data gagal diperoleh", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// MAPPING SKRINING NYERI
	mapper := sh.SoapMapper.ToMapperResponseAsesmenDokter(data)
	sh.Logging.Info("MAPPING DATA")
	sh.Logging.Info(mapper)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) InputTindkaanICD9FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqInputSingleDiagnosaV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)
	data, _ := sh.SoapRepository.SearchDcpptSoapDokterRepository(payload.Noreg, modulID)

	if len(data.Noreg) < 1 {
		tanggal, errs := sh.SoapRepository.GetJamMasukPasienRepository(payload.Noreg)

		if errs != nil || tanggal.Tanggal == "" {
			sh.Logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			response := helper.APIResponseFailure(message, http.StatusCreated)
			return c.JSON(response)
		}

		_, err1 := sh.SoapRepository.InsertSingleDiagnosaFirstRepositoryV2(*payload, tanggal.Jam, tanggal.Tanggal[0:10], modulID, userID)

		if err1 != nil {
			sh.Logging.Error("ERROR INPUT SINGLE DIAGNOSA < 1")
			sh.Logging.Error(err1.Error())
			response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
			return c.JSON(response)
		}
	}

	// INPUT SINGLE DIAGNOSA
	_, errs = sh.SoapRepository.InputSingleDiagnosaRepositoryV2(*payload, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	value, errs := sh.SoapRepository.GetDiagnosaRepository(payload.Noreg, modulID, userID)

	sh.Logging.Info(value)

	if errs != nil {
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	values, err := sh.SoapRepository.GetDiagnosaRepositoryBangsal(payload.Noreg, modulID)

	sh.Logging.Info(values)

	if err != nil || len(values) == 0 {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(values) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, values)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTindakanICD9FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoregV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	// OnGetTindakanICD9RepositoryBangsalDokter(noReg string, kdBagian string) (res []soap.TindakanResponse, err error)
	value, err := sh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.Noreg, modulID)

	sh.Logging.Info(value)

	if err != nil || len(value) == 0 {
		value = make([]soap.TindakanResponse, 0)
		response := helper.APIResponse("Data kosong", http.StatusCreated, value)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnDeleteTindakanICD9FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteTindakanICD9)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	er12 := sh.SoapRepository.OnDeleteTindakanICD9Repository(payload.Noreg, payload.Index, modulID)

	if er12 != nil {
		sh.Logging.Info(er12.Error())
		response := helper.APIResponseFailure("Data gagal dihapus", http.StatusCreated)
		c.JSON(response)

	}

	value, _ := sh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.Noreg, modulID)

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnAddTindakanICD9FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegInputTindakan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	sh.Logging.Info("TAMBAH DATA TINDAKAN")
	sh.Logging.Info(payload)

	data, _ := sh.SoapRepository.SearchDcpptSoapDokterRepository(payload.Noreg, modulID)

	if len(data.Noreg) < 1 {
		tanggal, errs := sh.SoapRepository.GetJamMasukPasienRepository(payload.Noreg)

		if errs != nil || tanggal.Tanggal == "" {
			sh.Logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			response := helper.APIResponseFailure(message, http.StatusCreated)
			return c.JSON(response)
		}

		_, err1 := sh.SoapRepository.InsertSingleFirstTindakanRepository(*payload, tanggal.Jam, tanggal.Tanggal[0:10], modulID, userID)

		if err1 != nil {
			sh.Logging.Error("ERROR INPUT SINGLE DIAGNOSA < 1")
			response := helper.APIResponseFailure(err1.Error(), http.StatusCreated)
			return c.JSON(response)
		}
	}

	// INPUT SINGLE DIAGNOSA
	_, errs = sh.SoapRepository.InputSingleTindakanRepository(*payload, modulID)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	value, err := sh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.Noreg, modulID)

	if err != nil || len(value) == 0 {
		value = make([]soap.TindakanResponse, 0)
		response := helper.APIResponse("Data kosong", http.StatusCreated, value)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(value) == 0 {
		response := helper.APIResponseFailure("Data tidak ada", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, value)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetAsesmenReportAwalRawatInalDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenDokterDewasa)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	// SOAP DOKTER DAN PASIEN
	riwayatDahulu, _ := sh.RMEReporitory.OnGetRiwayatPenyakitDahulu(payload.NoRM, payload.Tanggal)
	rawatInap, _ := sh.SoapRepository.OnGetPengkajianKeperawatanDewasaRepository(payload.NoReg, modulID, userID)
	pemFisik, _ := sh.SoapRepository.OnGetReportPemeriksaanFisikDokterAntonio(modulID, payload.NoReg)
	tindakan, _ := sh.SoapRepository.OnGetTindakanICD9RepositoryBangsalDokter(payload.NoReg, modulID)
	labor, _ := sh.HisUsecase.HistoryLaboratoriumUsecaseV2(payload.NoReg)
	radiologi, _ := sh.HisUsecase.HistoryRadiologiUsecaseV2(payload.NoReg)

	// KELUHAN UTAMA
	keluh, _ := sh.RMEReporitory.GetAsesemenKeluhanUtamaIGDDokter(modulID, payload.NoReg, "Dokter")
	karyawan, _ := sh.UserRepository.GetKaryawanRepository(keluh.InsertUserId)
	mapper := sh.SoapMapper.ToMappingReportAsesmenDokterAntonio(riwayatDahulu, keluh, rawatInap, pemFisik, karyawan, tindakan, labor, radiologi)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}
