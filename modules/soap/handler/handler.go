package handler

import (
	"fmt"
	hisEntity "hms_api/modules/his/entity"
	igdEntity "hms_api/modules/igd/entity"
	kebidananEntity "hms_api/modules/kebidanan/entity"
	rmeEntity "hms_api/modules/rme/entity"
	"hms_api/modules/soap/dto"
	"hms_api/modules/soap/entity"
	userEntity "hms_api/modules/user/entity"
	"hms_api/pkg/helper"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type SoapHandler struct {
	SoapUseCase         entity.SoapUseCase
	SoapRepository      entity.SoapRepository
	SoapMapper          entity.SoapMapper
	Logging             *logrus.Logger
	UserRepository      userEntity.UserRepository
	RMEReporitory       rmeEntity.RMERepository
	RMEMapper           rmeEntity.RMEMapper
	KebidananRepository kebidananEntity.KebidananRepository
	KebidananMaper      kebidananEntity.KebidananMapper
	HisUsecase          hisEntity.HisUsecase
	IGDRepository       igdEntity.IGDRepository
}

func (sh *SoapHandler) GetSkriningHandler(c *gin.Context) {
	payload := new(dto.RequestGetSkrining)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	get, errs := sh.SoapRepository.GetSkriningRepository(payload.NoReg)

	if errs != nil || get.Noreg == "" || len(get.Noreg) == 0 {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	data := sh.SoapMapper.ToSkriningMapper(get)

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) GetRawatJalanPerawat(c *gin.Context) {
	payload := new(dto.RequestGetData)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	get, errs := sh.SoapRepository.GetAssementRawatJalanPerawatRepository(payload.NoReg)

	if errs != nil || get.Noreg == "" {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data tidak ditemukan", http.StatusCreated, errorMessage)
		c.JSON(http.StatusCreated, response)
		return
	}

	sh.Logging.Info(get)

	mapper := sh.SoapMapper.ToAssesRawatJalanPerawatMapper(get)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	sh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (sh *SoapHandler) SaveSingleOdontogram(c *gin.Context) {
	payload := new(dto.RequestSaveOdontogram)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	errs := sh.SoapUseCase.InsertOdontogramUseCase(*payload)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure("Data gagal disimpan", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, payload)
	sh.Logging.Info("Data berhasil disimpan")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) SaveSkriningHandler(c *gin.Context) {
	payload := new(dto.RequestSaveSkrining)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	detail, errs1 := sh.SoapRepository.DetailSkriningRepository(payload.NoReg)

	if errs1 != nil || detail.Noreg == "" {
		save, errs := sh.SoapRepository.SaveSkriningRepository(*payload)
		if errs != nil {
			sh.Logging.Info(errs.Error())
			response := helper.APIResponseFailure("Data gagal disimpan", http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		data := sh.SoapMapper.ToSkriningMapper(save)
		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		c.JSON(http.StatusOK, response)
	}

	if len(detail.Noreg) > 1 {
		// UPDATE DATA
		_, err := sh.SoapRepository.UpdateSkriningRepository(*payload)
		if err != nil {
			sh.Logging.Info(err.Error())
			response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
			c.JSON(http.StatusCreated, response)
			return
		}

		get, _ := sh.SoapRepository.GetSkriningRepository(payload.NoReg)

		data := sh.SoapMapper.ToSkriningMapper(get)

		response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
		c.JSON(http.StatusOK, response)
	}
}

func (sh *SoapHandler) PublisImageOdontogramHandler(c *gin.Context) {
	payload := new(dto.RequestSavePublishOdontogram)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	file := payload.File
	tipe := strings.Split(file.Filename, ".")

	modulID := c.MustGet("modulID").(string)

	path := fmt.Sprintf("images_odon/%s.%s", payload.NoReg, tipe[1])
	_ = c.SaveUploadedFile(file, path)

	data, err := sh.SoapUseCase.PublishImageOdontogramUseCase(payload.NoReg, path, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info("Data berhasil disimpan")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) GetPemeriksaanFisikHandler(c *gin.Context) {
	// payload := new(dto.ReqNoreg)
	// err := c.ShouldBindJSON(&payload)

	// modulID := c.MustGet("modulID").(string)

	// if err != nil {
	// 	sh.Logging.Error(err.Error())
	// 	errors := helper.FormatValidationError(err)
	// 	errorMessage := gin.H{"errors": errors}
	// 	response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
	// 	c.JSON(http.StatusAccepted, response)
	// 	return
	// }

	// // GET PEMERIKSAAN FISIK
	// // data, err := sh.SoapRepository.GetPemeriksaanFisikRepository(modulID, payload.Noreg)

	// if err != nil {
	// 	sh.Logging.Info(err.Error())
	// 	response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
	// 	c.JSON(http.StatusCreated, response)
	// 	return
	// }

	// response := helper.APIResponse("OK", http.StatusOK, data)
	// sh.Logging.Info("OK")
	// c.JSON(http.StatusOK, response)

}

func (sh *SoapHandler) DetailPemeriksaanLabor(c *gin.Context) {
	payload := new(dto.RequestPilihPemeriksaanLabor)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	// GET PEMERIKSAAN FISIK
	data, err := sh.SoapUseCase.DetailPemeriksaanLaborUseCase(payload.NameGrup)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) PilihPemeriksaanLaborHandler(c *gin.Context) {
	payload := new(dto.RequestPilihPemeriksaanLabor)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	// GET PEMERIKSAAN FISIK
	data, err := sh.SoapUseCase.PilihPemeriksaanLaborUsecase(payload.NameGrup)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) InsertRencanaTindakLanjut(c *gin.Context) {
	payload := new(dto.RequestRencanaTindakLanjut)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	modulID := c.MustGet("modulID").(string)

	value, message, errs := sh.SoapUseCase.InputRencanaTindakLanjutUseCase(*payload, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse(message, http.StatusOK, value)
	sh.Logging.Info(message)
	c.JSON(http.StatusOK, response)

}

func (sh *SoapHandler) GetImageLokalisHandler(c *gin.Context) {
	payload := new(dto.ReqNoreg)
	err := c.ShouldBindJSON(&payload)

	modulID := c.MustGet("modulID").(string)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	data, err := sh.SoapRepository.GetImageLokalisRepository(payload.Noreg, modulID)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("OK", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse("OK", http.StatusOK, image)
	sh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) UploadImageLokalisPublic(c *gin.Context) {

	// File     *multipart.FileHeader `form:"imageUrl" binding:"required"  bson:"imageUrl"`
	// NoReg    string                `form:"noReg"  binding:"required" bson:"noReg"`
	// KdBagian string                `form:"kd_bagian"  binding:"required" bson:"kd_bagian"`

	payload := new(dto.RequestUploadLokalisPublic)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		helper.PaylayotHandler(c, errorMessage)
	}

	// file := payload.File
	// tipe := strings.Split(file.Filename, ".")

	// times := time.Now()

	// path := fmt.Sprintf("%s-%s-%s.%s", times.Format("20060102150405"), payload.NoReg, payload.KdBagian, tipe[1])

	// _ = c.SaveUploadedFile(file, "images/lokalis/"+path)

	data, message, err := sh.SoapUseCase.SaveImageLokalisToPublicUsecase(payload.KdBagian, payload.NoReg, "path")

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	image := sh.SoapMapper.ToImageLokalis(data)

	response := helper.APIResponse(message, http.StatusOK, image)
	sh.Logging.Info(message)
	c.JSON(http.StatusOK, response)

}

func (sh *SoapHandler) UploadOdontogramHandler(c *gin.Context) {
	payload := new(dto.RequestUploadOdontogram)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	modulID := c.MustGet("modulID").(string)

	file := payload.File
	tipe := strings.Split(file.Filename, ".")

	path := fmt.Sprintf("images/odontogram/%s.%s", payload.NoReg, tipe[1])
	_ = c.SaveUploadedFile(file, path)

	_, err = sh.SoapUseCase.UploadOdontogramUseCase(modulID, payload.NoReg, path)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponseFailure("Data berhasil disimpan", http.StatusOK)
	sh.Logging.Info("Data berhasil disimpan")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) UploadImageOdontogramHandler(c *gin.Context) {
	payload := new(dto.RequstSaveImageOdontogram)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	file := payload.File
	tipe := strings.Split(file.Filename, ".")

	path := fmt.Sprintf("images_odon/%s.%s", payload.NoReg, tipe[1])

	// UPLOAD FILE
	err = c.SaveUploadedFile(file, path)
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// SIMPAN DATA
	data := dto.ResUploadOdontogram{ImageUrl: path, NoReg: payload.NoReg}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, data)
	sh.Logging.Info("Data berhasil disimpan")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) SaveAnatomiHandler(c *gin.Context) {
	payload := new(dto.RequestSaveAnatomi)
	err := c.ShouldBind(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	userID := c.MustGet("userID")

	now := time.Now().Unix()
	s := strconv.Itoa(int(now))
	file := payload.File
	path := fmt.Sprintf("images/anatomi/%s-%s", s, file.Filename)

	err = c.SaveUploadedFile(file, path)
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// SIMPAN DATA KE DATABASE
	an, err := sh.SoapRepository.SaveAnatomiRepository(*payload, path, userID.(string))
	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure("Gambar gagal diupload", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Data berhasil disimpan", http.StatusOK, an)
	sh.Logging.Info("Data berhasil disimpan")
	c.JSON(http.StatusOK, response)
}

// SIMPAN DATA ASESMEND KEPERAWATAN BIDAN
func (sh *SoapHandler) SaveAsesmedKeperawatanBidan(c *gin.Context) {
	// SAVE KEPERAWATAN BIDAN
	payload := new(dto.ReqSaveAsesmenKeperawatanBidan)
	errs := c.ShouldBindJSON(&payload)

	if errs != nil {
		sh.Logging.Error(errs.Error())
		errors := helper.FormatValidationError(errs)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	// USECASE SAVE DATA
	modulID := c.MustGet("modulID").(string)
	save, message, err := sh.SoapUseCase.SaveKeperawatanBidanUseCase(modulID, *payload)

	if err != nil {
		sh.Logging.Info(err.Error())
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse(message, http.StatusOK, save)
	sh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (sh *SoapHandler) GetAsesmedKeperawatanBidan(c *gin.Context) {
	payload := new(dto.ReqNoreg)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		sh.Logging.Error(err.Error())
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	modulID := c.MustGet("modulID").(string)

	// GET ASESMEN KEPERAWATAN
	data, errs := sh.SoapRepository.GetAsesmedKeperawatanBidanRepository(payload.Noreg, modulID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	sh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}
