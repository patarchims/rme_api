package handler

import (
	"hms_api/modules/soap/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (sh *SoapHandler) InsertRencanaTindakLanjutFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestRencanaTindakLanjutV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	sh.Logging.Info("RENCANA TINDAK LANJUT")
	sh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	value, message, errs := sh.SoapUseCase.InputRencanaTindakLanjutUseCaseV2(*payload, modulID, userID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.JSON(response)

	}

	response := helper.APIResponse(message, http.StatusOK, value)
	sh.Logging.Info(message)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) GetRencanaTindakLanjutFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTindakLanjutV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	sh.Logging.Info("GET NOREG")
	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info(payload.Noreg)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	value, errs := sh.SoapRepository.GetRencanaTindakLanjutRepository(payload.Noreg, modulID, userID)

	if errs != nil {
		sh.Logging.Info(errs.Error())
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := sh.SoapMapper.ToMappingAlasanOpname(value)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTandaVitalIGDDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTandaVitalIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	sh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, errs1 := sh.SoapUseCase.OnGetTandaVitalIGDDokterUserCase(userID, modulID, payload.Person, payload.Noreg, payload.Pelayanan)

	// APATIS
	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTandaVitalPerawatIGDFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTandaVitalIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	sh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	// SAVE ASESMEN INFO // KELUHAN IGD USECASE
	data, errs1 := sh.SoapUseCase.OnGetTandaVitalIGDPerawatUserCase(modulID, payload.Person, payload.Noreg, payload.Pelayanan)

	if errs1 != nil {
		response := helper.APIResponseFailure(errs1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveTandaVitalIGDDokterFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveTandaVitalIGDDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	sh.Logging.Info("DATA PAYLOAD DIDAPAT")
	sh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, errs1 := sh.SoapUseCase.OnSaveTandaVitalIGDDokterUseCase(userID, modulID, payload.Person, payload.Noreg, payload.DeviceID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
