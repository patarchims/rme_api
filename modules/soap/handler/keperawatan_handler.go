package handler

import (
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func (sh *SoapHandler) OnSavePengkajianAwalPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, message, errs12 := sh.SoapUseCase.OnSavePengkajianAwalKeperawatanUseCase(userID, modulID, *payload)

	if errs12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnReportPengkajianAwalKeperawatanRANAPFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenKeperawatanRANAP)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)

	pengkajian, _ := sh.IGDRepository.GetPengkajianKeperawatanRepository(modulID, payload.Pelayanan, payload.Usia, payload.Noreg)
	riwayatPengobatan, _ := sh.KebidananRepository.OnGetRiwayatPengobatanDirumahRepository(payload.Noreg)

	mapper := sh.SoapMapper.ToResponsePengkajianAwalKeperawatanRANAP(pengkajian, riwayatPengobatan)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)

}

func (sh *SoapHandler) OnReportPengkajianAwalKeperawatanFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAsesmenKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	// userID := c.Locals("userID").(string)

	pengkajian, _ := sh.SoapRepository.OnGetReportPengkajianKeperawatanDewasaRepository(payload.Noreg, modulID)
	karyawan, _ := sh.UserRepository.GetKaryawanRepository(pengkajian.InsertUserId)
	pemFisik, _ := sh.RMEReporitory.OnGetPemeriksaanFisikRawatInapRepository(modulID, payload.Noreg)
	maperPemFisik := sh.SoapMapper.ToMapperPemeriksaanFisik(pemFisik)

	nyeri, _ := sh.RMEReporitory.OnGetSkalaNyeriKeperawatanDewasaRepository(payload.Noreg, modulID)
	mapperNyeri := sh.SoapMapper.ToResponseSkalaNyeri(nyeri)

	fungsional, _ := sh.KebidananRepository.OnGetPengkajianFungsionalRepository(pemFisik.Noreg)
	resikoJatuh, _ := sh.KebidananRepository.OnGetResikoJatuhKebidananRepository(payload.Noreg, payload.Person)

	nutrisi, _ := sh.KebidananRepository.OnGetPengkajianNutrisiRepository(payload.Noreg)
	nutrisiMapper := sh.SoapMapper.ToPengkajianNutrisiMapperSoap(nutrisi)
	asuhan, _ := sh.RMEReporitory.GetDataAsuhanKeperawatanRepositoryV2(payload.Noreg, modulID, "Open")
	riwayatPengobatan, _ := sh.KebidananRepository.OnGetRiwayatPengobatanDirumahRepository(payload.Noreg)

	mapper := sh.SoapMapper.ToMappingResponsePengkajiaKeperawatanRawatInap(pengkajian, karyawan, maperPemFisik, mapperNyeri, sh.SoapMapper.ToFungsionalKebidananMapper(fungsional), resikoJatuh, nutrisiMapper, asuhan, riwayatPengobatan)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSavePengkajianAwalAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSavePengkajianAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	sh.Logging.Info("Data objek json di dapat")
	sh.Logging.Info(payload)

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, message, er12 := sh.SoapUseCase.OnSavePengkajianAwalAnakUseCase(userID, modulID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

// GET PENGKAJIAN PERAWAT FIBER HANDLER
func (sh *SoapHandler) GetPengkajianPerawatFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, message, errs1 := sh.SoapUseCase.GetPengkajianAwalUsecase(userID, modulID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetAsesmenAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, er12 := sh.SoapUseCase.OnGetAsesmenAnakUseCase(userID, modulID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetDoubleCheckFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	res, er12 := sh.SoapRepository.OnGetDoubleCheckHightReporitory(payload.Noreg, modulID)

	// == //
	sh.Logging.Info("DOUBLE CHECK FIBER HANDLER")
	sh.Logging.Info(res)

	if er12 != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapper := sh.SoapMapper.ToResponseDoubleCheckMapper(res)

	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveDoubleCheckFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ON SAVE DOUBLE CHECK USECASE
	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	message, er12 := sh.SoapUseCase.DoubleCheckHighUseCase(userID, modulID, *payload)

	if er12 != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure(message, http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnDeleteCheckFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.OnDeleteDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ON SAVE DOUBLE CHECK USECASE
	_, er12 := sh.SoapRepository.OnDeleteDoubleCheckRepository(payload.IdDoubleCheck)

	if er12 != nil {
		response := helper.APIResponseFailure("Data tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("Data berhasil dihapus", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveDoubleCheckVerifyFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnVerifyDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ONSAVE VERIFY
	userID := c.Locals("userID").(string)

	message, er12 := sh.SoapRepository.OnVerfyDoubleCheckHighRepository(*payload, userID)

	if er12 != nil {
		response := helper.APIResponseFailure("Gagal verify", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Berhasil verify", http.StatusOK, message)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ====//
func (sh *SoapHandler) OnSaveDoubleCheckVerifyV2FiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqOnVerifyDoubleCheck)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// ONSAVE VERIFY
	userID := c.Locals("userID").(string)

	message, er12 := sh.SoapRepository.OnVerfyDoubleCheckHighRepository(*payload, userID)

	if er12 != nil {
		response := helper.APIResponseFailure("Gagal verify", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Berhasil verify", http.StatusOK, message)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetReportDoubleCheckFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqNoReg)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	// userID := c.Locals("userID").(string)
	// modulID := c.Locals("modulID").(string)
	// OnGetViewDoubleCheckRepository(noReg string) (res []soap.DDoubleCheck, err error)

	data, er12 := sh.SoapRepository.OnGetViewDoubleCheckRepository(payload.NoReg)

	if er12 != nil {
		response := helper.APIResponse(er12.Error(), http.StatusCreated, er12.Error())
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(data) < 1 {
		response := helper.APIResponse("OK", http.StatusOK, make([]soap.DDoubleCheck, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

// ==== //
func (sh *SoapHandler) OnReportAssesmenAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqReportAnak)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	//=== GET REPORT PENGKAJIAN ANAK =====//
	modulID := c.Locals("modulID").(string)

	res, err12 := sh.SoapUseCase.OnGetReportPengkajianAwalMedisDokterUseCase(payload.Noreg, modulID, payload.Person, payload.Pelayanan, payload.Tanggal, payload.NoRM)

	if err12 != nil {
		response := helper.APIResponseFailure("data gagal", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnGetPengkajianKeperawatanAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAsesmenKeperawatan)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	res, message, errs1 := sh.SoapUseCase.OnGetPengkajianAwalKeperawatanAnakUseCase(userID, modulID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, res)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) OnSaveTandaKeperawatanBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqSaveVitalSignKeperawatanBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		sh.Logging.Info(errors)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, message, errs1 := sh.SoapUseCase.OnSaveTandaVitalKeperawatanUseCase(userID, modulID, payload.Person, payload.Noreg, payload.DeviceID, *payload)

	if errs1 != nil {
		response := helper.APIResponseFailure(message, http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse(message, http.StatusOK, data)
	sh.Logging.Info(data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTandaVitalKeperawatanBangsalFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqTandaVitalKeperawatanBangsal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		sh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	sh.Logging.Info(payload)

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, errs1 := sh.SoapUseCase.OnGetTandaVitalKeperawatanBangsalUseCase(userID, modulID, payload.Person, payload.Noreg)

	if errs1 != nil {
		response := helper.APIResponseFailure("Error", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (sh *SoapHandler) GetTandaVitalSignBangsalAnakFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.RegTandaVitalSignAnakBansal)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	modulID := c.Locals("modulID").(string)
	userID := c.Locals("userID").(string)

	data, errs1 := sh.SoapUseCase.OnGetTandaVitalKeperawatanBangsalUseCase(userID, modulID, payload.Person, payload.Noreg)

	if errs1 != nil {
		response := helper.APIResponseFailure("Error", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}
