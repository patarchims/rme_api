package mapper

import (
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
)

func (a *SoapMapperImpl) ToMapperResponseKeperawatanBangsal(data1 soap.DVitalSignKeperawatanBangsal, data2 soap.DPemFisikKeperawatanBangsalRepository) (value dto.TandaVitalBangsalKeperawatanResponse) {
	var e = ""
	var v = ""
	var m = ""

	if data2.GcsE == "" || data2.GcsE == " " {
		e = " "
	} else {
		e = data2.GcsE
	}

	if data2.GcsM == "" || data2.GcsM == " " {
		m = " "
	} else {
		m = data2.GcsM
	}

	if data2.GcsV == "" || data2.GcsV == " " {
		v = " "
	} else {
		v = data2.GcsV
	}

	return dto.TandaVitalBangsalKeperawatanResponse{
		GCSE:         e,
		GCSV:         v,
		GCSM:         m,
		Nadi:         data1.Nadi,
		Suhu:         data1.Suhu,
		TinggiBandan: data1.Tb,
		Td:           data1.Td,
		BeratBadan:   data1.Bb,
		Pernafasan:   data1.Pernafasan,
		Spo2:         data1.Spo2,
		Kesadaran:    data2.Kesadaran,
		Akral:        data2.Akral,
		Pupil:        data2.Pupil,
	}

}

func (a *SoapMapperImpl) ToMapperResponseAsesmenAnak(alergi []rme.DAlergi, perawat soap.PengkajianAwalKeperawatan, riwayat []rme.RiwayatPenyakitDahuluPerawat) (res dto.ResponseAsesmenAnak) {
	var alergis = []rme.DAlergi{}
	var riwayats = []rme.RiwayatPenyakitDahuluPerawat{}

	if len(alergi) == 0 || alergi == nil {
		alergis = make([]rme.DAlergi, 0)
	} else {
		alergis = alergi
	}

	if len(riwayat) == 0 || riwayat == nil {
		riwayats = make([]rme.RiwayatPenyakitDahuluPerawat, 0)
	} else {
		riwayats = riwayat
	}

	return dto.ResponseAsesmenAnak{
		Alergi:         alergis,
		PengkajianAnak: a.ToResponseAsesmenAnak(perawat),
		Riwayat:        riwayats,
	}

}

func (rm *SoapMapperImpl) ToResponseAsesmenAwalIGD(riwayat []soap.RiwayatPenyakit) (res dto.ResponseAsesmenAwalIGD) {
	var riwayats = []soap.RiwayatPenyakit{}

	if len(riwayat) == 0 {
		riwayats = make([]soap.RiwayatPenyakit, 0)
	} else {
		riwayats = riwayat
	}

	return dto.ResponseAsesmenAwalIGD{
		Riwayat: riwayats,
	}
}

func (a *SoapMapperImpl) ToResponseAsesmenAnak(data soap.PengkajianAwalKeperawatan) (res dto.PenkajianAnakResponse) {
	return dto.PenkajianAnakResponse{
		Noreg:             data.Noreg,
		Person:            data.KeteranganPerson,
		KdDPJP:            data.KdDpjp,
		Jenpel:            data.AseskepPerolehanInfo,
		JenpelDetail:      data.AseskepPerolehanInfoDetail,
		KeluhanUtama:      data.AseskepKel,
		RwtPenyakitDahulu: data.AseskepRwytPnyktDahulu,
		ReaksiAlergi:      data.AseskepReaksiAlergi,
		RwtPenyakit:       data.AseskepRwytPnykt,
		RwtImunisasi:      data.AseskepRwtImunisasi,
		RwtKelahiran:      data.AseskepRwtKelahiran,
	}
}

func (a *SoapMapperImpl) ToResikoJatuhMapper(data soap.DcpptSoapPasienModel) (res dto.ResponseResikoJatuhGoUpGoTest) {
	return dto.ResponseResikoJatuhGoUpGoTest{
		ResikoJatuh1: data.AseskepRj1,
		ResikoJatuh2: data.AseskepRj2,
		Tindakan:     data.AseskepHslKajiRjTind,
	}
}

func (a *SoapMapperImpl) ToPengkajianNutrisiMapperSoap(data kebidanan.DPengkajianNutrisi) (res dto.ResponseKebidananModel) {
	return dto.ResponseKebidananModel{
		Noreg:   data.Noreg,
		N1:      data.N1,
		N2:      data.N2,
		Nilai:   data.Nilai,
		NilaiN1: 0,
		NilaiN2: 0,
	}
}
