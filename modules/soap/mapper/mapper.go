package mapper

import (
	"hms_api/modules/igd"
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	rmeDto "hms_api/modules/rme/dto"
	rmeMapper "hms_api/modules/rme/mapper"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/modules/soap/entity"
	"reflect"
	"strconv"
	"strings"
)

type SoapMapperImpl struct {
	RMEMapper rmeMapper.RMEMapperImpl
}

func NewSoapMapperImple(rmeMapper rmeMapper.RMEMapperImpl) entity.SoapMapper {
	return &SoapMapperImpl{
		RMEMapper: rmeMapper,
	}
}

func (a *SoapMapperImpl) ToSkriningMapper(data soap.DcpptSoapPasien) (value dto.RequestSaveSkrining) {
	return dto.RequestSaveSkrining{
		KodeBagian: data.KdBagian,
		NoReg:      data.Noreg,
		K1:         data.SkriningK1,
		K2:         data.SkriningK2,
		K3:         data.SkriningK3,
		K4:         data.SkriningK4,
		K5:         data.SkriningK5,
		K6:         data.SkriningK6,
		KF1:        data.SkriningF1,
		KF2:        data.SkriningF2,
		KF3:        data.SkriningF3,
		KF4:        data.SkriningF4,
		B1:         data.SkriningB1,
		B2:         data.SkriningB2,
		RJ:         data.SkriningRj,
		IV1:        data.SkriningR1,
		IV2:        data.SkriningR2,
		IV3:        data.SkriningR3,
		IV4:        data.SkriningR4,
	}
}

func (a *SoapMapperImpl) ToPemeriksaanGigi(data soap.DcpptOdontogram) (value soap.AssesPemeriksaanGigi) {
	return soap.AssesPemeriksaanGigi{
		AsesmedKhusPemGigi1:       data.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:       data.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:       data.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:       data.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:       data.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail: data.AsesmedKhusPemGigi5Detail,
	}
}

func (a *SoapMapperImpl) ToRiwayat(data soap.DcpptOdontogram) (value soap.AssesRiwayat) {
	return soap.AssesRiwayat{
		AsesmedRwytSkrg:         data.AsesmedRwytSkrg,
		AsesmedRwytDulu:         data.AsesmedRwytDulu,
		AsesmedRwytObat:         data.AsesmedRwytObat,
		AsesmedRwytPenyKlrg:     data.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergi:       data.AsesmedRwytAlergi,
		AsesmedRwytAlergiDetail: data.AsesmedRwytAlergiDetail,
	}
}

func (a *SoapMapperImpl) ToIntraOral(data soap.DcpptOdontogram) (value soap.IntraOral) {
	return soap.IntraOral{
		AsesmedKhusOkAnterior:        data.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       data.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           data.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           data.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            data.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            data.AsesmedKhusTorusM,
		AsesmedKhusTorusMDetail:      data.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        data.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  data.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          data.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    data.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       data.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: data.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          data.AsesmedPenyLain,
	}
}

func (a *SoapMapperImpl) ToDataMedik(data soap.DcpptOdontogram) (value soap.DataMedik) {
	return soap.DataMedik{
		AsesmedGolDarah:          data.AsesmedGolDarah,
		AsesmedTd:                data.AsesmedTd,
		AsesmedTdDetail:          data.AsesmedTdDetail,
		AsesmedPenyJantung:       data.AsesmedPenyJantung,
		AsesmedPenyJantungDetail: data.AsesmedPenyJantungDetail,
		AsesmedDiabet:            data.AsesmedDiabet,
		AsesmedDiabetDetail:      data.AsesmedDiabetDetail,
		AsesmedHaemop:            data.AsesmedHaemop,
		AsesmedHaemopDetail:      data.AsesmedHaemopDetail,
		AsesmedHepat:             data.AsesmedHepat,
		AsesmedHepatDetail:       data.AsesmedHepatDetail,
		AsesmedPenyLain:          data.AsesmedPenyLain,
		AsesmedPenyLainDetail:    data.AsesmedPenyLainDetail,
		AsesmedAlergiObat:        data.AsesmedAlergiObat,
		AsesmedAlergiObatDetail:  data.AsesmedAlergiObatDetail,
		AsesmedAlergiMknan:       data.AsesmedAlergiMknan,
		AsesmedAlergiMknanDetail: data.AsesmedAlergiMknanDetail,
		AsesmedKebiasaanBuruk:    data.AsesmedKebiasaanBuruk,
	}
}

func (a *SoapMapperImpl) ToOdontogramModel(data reflect.Value, tipe reflect.Type) (value []soap.Odontograms) {

	for i := 51; i < data.NumField(); i++ {
		if len(data.Field(i).Interface().(string)) > 1 {

			val := strings.Split(tipe.Field(i).Name, "_")
			tabs := val[1]
			intVar, _ := strconv.Atoi(tabs)

			kets := strings.Split(data.Field(i).Interface().(string), ",")
			ket1 := kets[0]
			ket2 := kets[1]

			value = append(value, soap.Odontograms{
				Number:     intVar,
				Assets:     ket1,
				Keterangan: ket2,
			})
		}
	}

	return value
}

func (a *SoapMapperImpl) ToOdontogramModel2(data reflect.Value, tipe reflect.Type) (value []soap.Odontograms2) {

	for i := 31; i < data.NumField(); i++ {
		if len(data.Field(i).Interface().(string)) > 1 {

			val := strings.Split(tipe.Field(i).Name, "_")
			tabs := val[1]
			intVar, _ := strconv.Atoi(tabs)

			value = append(value, soap.Odontograms2{
				Number:     intVar,
				Keterangan: data.Field(i).Interface().(string),
			})
		}
	}

	return value
}

func (a *SoapMapperImpl) ToKebEdukasi(data reflect.Value) (value []soap.ResAssKebEdukasi) {
	for i := 0; i < 11; i++ {

		if len(data.Field(i).Interface().(string)) > 1 {
			value = append(value, soap.ResAssKebEdukasi{
				Name: data.Field(i).Interface().(string),
			})
		}

	}
	return value
}

func (a *SoapMapperImpl) KebEdukasi(data soap.KebEdukasi) (value dto.KebEdukasiRes) {
	return dto.KebEdukasiRes{
		KebEdu1:        data.AseseduKe1,
		KebEdu2:        data.AseseduKe2,
		KebEdu3:        data.AseseduKe3,
		KebEdu4:        data.AseseduKe4,
		KebEdu5:        data.AseseduKe5,
		KebEdu6:        data.AseseduKe6,
		KebEdu7:        data.AseseduKe7,
		KebEdu8:        data.AseseduKe8,
		KebEdu9:        data.AseseduKe9,
		KebEdu10:       data.AseseduKe10,
		KebEdu11:       data.AseseduKe11,
		KebEdu11Detail: data.AseseduKe11Detail,
	}
}

func (a *SoapMapperImpl) TujuanEdukasi(data soap.KebEdukasi) (value dto.TujuanRes) {
	return dto.TujuanRes{
		Tujuan1: data.AseseduTu1,
		Tujuan2: data.AseseduTu2,
	}
}

func (a *SoapMapperImpl) KemBelajar(data soap.KebEdukasi) (value dto.KempBelajarRes) {
	return dto.KempBelajarRes{
		KempBelajar1: data.AseseduKb1,
		KempBelajar2: data.AseseduKb2,
		KempBelajar3: data.AseseduKb3,
	}
}

func (a *SoapMapperImpl) MotivasiBelajar(data soap.KebEdukasi) (value dto.MotivasiBelajarRes) {
	return dto.MotivasiBelajarRes{
		MotivasiBel1: data.AseseduMb1,
		MotivasiBel2: data.AseseduMb2,
		MotivasiBel3: data.AseseduMb3,
	}
}

func (a *SoapMapperImpl) PilihanPasien(data soap.KebEdukasi) (value dto.PilihanPasienRes) {
	return dto.PilihanPasienRes{
		NilaiPasien1:       data.AseseduNp1,
		NilaiPasien2:       data.AseseduNp2,
		NilaiPasien3:       data.AseseduNp3,
		NilaiPasien4:       data.AseseduNp4,
		NilaiPasien5:       data.AseseduNp5,
		NilaiPasien6:       data.AseseduNp6,
		NilaiPasien6Detail: data.AseseduNp6Detail,
	}
}

func (a *SoapMapperImpl) ToHambatan(data soap.KebEdukasi) (value dto.HabatanPasienRes) {
	return dto.HabatanPasienRes{
		Hambatan1:       data.AseseduHa1,
		Hambatan2:       data.AseseduHa2,
		Hambatan3:       data.AseseduHa3,
		Hambatan4:       data.AseseduHa4,
		Hambatan5:       data.AseseduHa5,
		Hambatan6:       data.AseseduHa6,
		Hambatan7:       data.AseseduHa7,
		Hambatan8:       data.AseseduHa8,
		Hambatan9:       data.AseseduHa9,
		Hambatan9Detail: data.AseseduHa9Detail,
	}
}

func (a *SoapMapperImpl) ToIntervensi(data soap.KebEdukasi) (value dto.IntervensiRes) {
	return dto.IntervensiRes{
		Intervensi1: data.AseseduIn1,
		Intervensi2: data.AseseduIn2,
		Intervensi3: data.AseseduIn3,
		Intervensi4: data.AseseduIn4,
		Intervensi5: data.AseseduIn5,
	}
}

func (a *SoapMapperImpl) ToHasil(data soap.KebEdukasi) (value dto.HasilRes) {
	return dto.HasilRes{
		Hasil1: data.AseseduHasil1,
		Hasil2: data.AseseduHasil2,
	}
}

func (a *SoapMapperImpl) ToAssesRawatJalanPerawatMapper(data soap.AssementRawatJalanPerawat) (value dto.ResponseAssesRawatJalanPerawat) {
	return dto.ResponseAssesRawatJalanPerawat{
		KelUtama:                     data.AseskepKel,
		RiwayatPenyakit:              data.AseskepRwytPnykt,
		RiwayatObat:                  data.AseskepRwytObat,
		RiwayatObatDetail:            data.AseskepRwytObatDetail,
		TekananDarah:                 data.AseskepTd,
		Nadi:                         data.AseskepNadi,
		Suhu:                         data.AseskepSuhu,
		Pernapasan:                   data.AseskepRr,
		BeratBadan:                   data.AseskepBb,
		TinggiBadan:                  data.AseskepTb,
		Fungsional:                   data.AseskepAsesFungsional,
		FungsionalDetail:             data.AseskepAsesFungsionalDetail,
		ResikoJatuh1:                 data.AseskepRj1,
		ResikoJatuh2:                 data.AseskepRj2,
		HasilKajiResikoJatuh:         data.AseskepHslKajiRj,
		HasilKajiResikoJatuhTindakan: data.AseskepHslKajiRjTind,
		Nyeri:                        data.AseskepAsesNyeri,
		Psikologis:                   data.AseskepPsiko,
		PsikologisDetail:             data.AseskepPsikoDetail,
		Pulang1:                      data.AseskepPulang1,
		Pulang1Detail:                data.AseskepPulang1Detail,
		Pulang2:                      data.AseskepPulang2,
		Pulang2Detail:                data.AseskepPulang2Detail,
		Pulang3:                      data.AseskepPulang3,
		Pulang3Detail:                data.AseskepPulang3Detail,
		MasalahKeperawatan:           data.AseskepMslhKprwtan,
		RencanaKeperawatan:           data.AseskepRencKprwtan,
	}
}

func (a *SoapMapperImpl) ToTriaseRiwayatAlergiMapper(data soap.TriaseRiwayatAlergi) (value dto.ResponseTriaseRiwayatAlergi) {
	return dto.ResponseTriaseRiwayatAlergi{
		IgdTriaseKeluh: data.AsesmedKeluhUtama,
	}
}

func (a *SoapMapperImpl) ToResDcpptSoapMapper(kdBagian string, noReg string) (value dto.ResDcpptSoap) {
	return dto.ResDcpptSoap{
		KdBagian: kdBagian,
		Noreg:    noReg,
	}
}

// TO PEMERIKSAAN MAPPER
func (a *SoapMapperImpl) ToPemeriksaanMapper(data rme.PemeriksaanFisikModel) (value soap.PemeriksaanFisikModel) {
	return soap.PemeriksaanFisikModel{
		InsertDttm:                   data.InsertDttm,
		KdBagian:                     data.KdBagian,
		Noreg:                        data.Noreg,
		AsesmedPemfisKepala:          data.Kepala,
		AsesmedPemfisLeher:           data.Leher,
		AsesmedPemfisLeherDetail:     data.LeherLainnya,
		AsesmedPemfisGenetalia:       data.Genetalia,
		AsesmedPemfisAbdomen:         data.Abdomen,
		AsesmedPemfisGenetaliaDetail: data.Genetalia,
		AsesmedPemfisEkstremitas:     data.Ekstremitas,
		AsesmedPemfisDada:            data.Dada,
		AsesmedPemfisPunggung:        data.Punggung,
		AsesmedPemfisLain:            data.LainLain,
		AsesmedPemfisPunggungDetail:  data.Punggung,
	}
}

func (a *SoapMapperImpl) ToImageLokalis(data soap.ImageLokalisModel) (value dto.ResImageLokalis) {
	if data.AsesmedLokalisImage != "" {
		return dto.ResImageLokalis{
			Image: "images/lokalis/" + data.AsesmedLokalisImage,
		}
	} else {
		return dto.ResImageLokalis{
			Image: "images/lokalis/lokalis_default.png",
		}
	}
}

func (a *SoapMapperImpl) ToImageLokalisMata(data soap.ImageLokalisModel) (value dto.ResImageLokalis) {
	if data.AsesmedLokalisImage != "" {
		return dto.ResImageLokalis{
			Image: "images/lokalis/" + data.AsesmedLokalisImage,
		}
	} else {
		return dto.ResImageLokalis{
			Image: "images/lokalis/lokalis_mata_default.png",
		}
	}
}

// MAPPING DCPPTSOAP PASIEN TO INFORMASI DAN KELUHAN IGD RESPONS
func (a *SoapMapperImpl) ToMappingPasienInformasiKeluhanIGDResponse(data soap.DcpptSoapPasienModel, riwayat []soap.RiwayatPenyakit) (res rmeDto.ResInformasiDanKeluhanIGD) {
	return rmeDto.ResInformasiDanKeluhanIGD{
		PerolehanInfo:       data.AseskepPerolehanInfo,
		PerolehanInfoDetail: data.AseskepPerolehanInfoDetail,
		CaraMasuk:           data.AseskepCaraMasuk,
		CaraMasukDetail:     data.AseskepCaraMasukDetail,
		AsalMasuk:           data.AseskepAsalMasuk,
		AsalMasukDetail:     data.AseskepAsalMasukDetail,
		Bb:                  data.AseskepBb,
		Tb:                  data.AseskepTb,
		RwytPnykt:           data.AseskepRwytPnykt,
		RwytObat:            data.AseskepRwytObat,
		AsesFungsional:      data.AseskepAsesFungsional,
		Rj1:                 data.AseskepRj1,
		Rj2:                 data.AseskepRj2,
		HslKajiRj:           data.AseskepHslKajiRj + " - " + data.AseskepHslKajiRjTind,
		RiwayatPenyakit:     riwayat,
	}
}

func (a *SoapMapperImpl) ToMappingResikoDekubitusIGDResponse(data soap.DcpptSoapPasienModel) (res rmeDto.ResResikoDekkubitusIGD) {
	return rmeDto.ResResikoDekkubitusIGD{
		Dekubitus1:    data.AseskepDekubitus1,
		Dekubitus2:    data.AseskepDekubitus2,
		Dekubitus3:    data.AseskepDekubitus3,
		Dekubitus4:    data.AseskepDekubitus4,
		DekubitusAnak: data.AseskepDekubitusAnak,
	}
}

// === MAPPING TO RIWAYAT KEHAMILAN
func (rm *SoapMapperImpl) ToResRiwayatKehamilan(data soap.DcpptSoapPasienModel) (res dto.ResRiwayatKehamilanIGD) {
	var isHamil bool

	if data.AseskepKehamilan == "" {
		isHamil = false
	}

	if data.AseskepKehamilan == "true" {
		isHamil = true
	}

	if data.AseskepKehamilan == "false" {
		isHamil = false
	}

	return dto.ResRiwayatKehamilanIGD{
		Kehamilan:         isHamil,
		KehamilanGravida:  data.AseskepKehamilanGravida,
		KehamilanPara:     data.AseskepKehamilanPara,
		KehamilanAbortus:  data.AseskepKehamilanAbortus,
		KehamilanHpht:     data.AseskepKehamilanHpht,
		KehamilanTtp:      data.AseskepKehamilanTtp,
		KehamilanLeopold1: data.AseskepKehamilanLeopold1,
		KehamilanLeopold2: data.AseskepKehamilanLeopold2,
		KehamilanLeopold3: data.AseskepKehamilanLeopold3,
		KehamilanLeopold4: data.AseskepKehamilanLeopold4,
		KehamilanDjj:      data.AseskepKehamilanDjj,
		KehamilanVt:       data.AseskepKehamilanVt,
	}
}

// ====================== TO MAPPER SKRINGIN NYERI  ================================ //
func (rm *SoapMapperImpl) ToResSkriningNyeri(data soap.DcpptSoapPasienModel) (res dto.ResSkriningNyeriIGD) {
	return dto.ResSkriningNyeriIGD{
		SkalaNyeri:          data.AseskepSkalaNyeri,
		FrekuensiNyeri:      data.AseskepFrekuensiNyeri,
		LamaNyeri:           data.AseskepLamaNyeri,
		NyeriMenjalar:       data.AseskepNyeriMenjalar,
		NyeriMenjalarDetail: data.AseskepNyeriMenjalarDetail,
		Kualitasnyeri:       data.AseskepKualitasNyeri,
		NyeriPemicu:         data.AseskepNyeriPemicu,
		NyeriPengurang:      data.AseskepNyeriPengurang,
	}
}

func (rm *SoapMapperImpl) ToResponseDoubleCheckMapper(data []soap.DDoubleCheck) (res []dto.ResponseDoubleCheck) {

	if len(data) == 0 {
		return make([]dto.ResponseDoubleCheck, 0)
	}

	for _, V := range data {
		res = append(res, dto.ResponseDoubleCheck{
			IDDoubleCheck:      V.IdDoubleCheck,
			PasienPemberi:      V.PasienPemberi,
			PasienVerify:       V.PasienVerify,
			ObatPemberi:        V.ObatPemberi,
			ObatVerify:         V.ObatVerify,
			DosisPemberi:       V.DosisPemberi,
			DosisVerify:        V.DosisVerify,
			CaraPemberi:        V.CaraPemberi,
			CaraVerify:         V.CaraVerify,
			WaktuPemberi:       V.WaktuPemberi,
			WaktuVerify:        V.WaktuVerify,
			InformasiPemberi:   V.InformasiPemberi,
			InformasiVerify:    V.InformasiVerify,
			DokumentasiPemberi: V.DokumentasiPemberi,
			DokumentasiVerify:  V.DokumentasiVerify,
			Keterangan:         V.Keterangan,
		})
	}

	return res
}

// ======================= TO MAPPER TINDAK LANJUT
func (rm *SoapMapperImpl) ToMapperTindakLanjut(data soap.DcpptSoapPasienModel) (res dto.ResponseTindakLanjutIGD) {

	var jam = ""

	if data.JamCheckOut != "" {
		jam = data.JamCheckOut
	}

	if data.JamCheckInRanap != "" {
		jam = data.JamCheckInRanap
	}

	return dto.ResponseTindakLanjutIGD{
		JamCheckOut:          jam,
		AseskepPulang1:       data.AseskepPulang1,
		AseskepPulang1Detail: data.AseskepPulang1Detail,
		AseskepPulang2:       data.AseskepPulang2,
		AseskepPulang2Detail: data.AseskepPulang2Detail,
		AseskepPulang3:       data.AseskepPulang3,
		AseskepPulang3Detail: data.AseskepPulang3Detail,
		CaraKeluar:           data.CaraKeluar,
	}
}

// ========================= TO MAPPER VITAL SIGN BANGSAL
func (rm *SoapMapperImpl) ToMapperVitalSignBangsal(data soap.DcpptSoapPasienModel, dpemFisik rme.PemeriksaanFisikModel) (res dto.ResponseVitalSignBangsal) {
	return dto.ResponseVitalSignBangsal{
		KeadaanUmum:     data.AseskepKeadaanUmum,
		Kesadaran:       data.AseskepKesadaran,
		KesadaranDetail: data.AseskepKesadaranDetail,
	}
}

// TO MAPPING DATA KE RESPONSE
func (rm *SoapMapperImpl) ToMapperResponseAsesmenDokter(data soap.DcpptSoapDokter) (res dto.ResponseAsesmenDokter) {
	return dto.ResponseAsesmenDokter{
		AsesmedJenpel:     data.AsesmedJenpel,
		AsesmedKeluhUtama: data.AsesmedKeluhUtama,
		AsesmedTelaah:     data.AsesmedTelaah,
		AsesmedMslhMedis:  data.AsesmedMslhMedis,
	}
}

func (rm *SoapMapperImpl) ToMapperResponseKeadaanUmum(data soap.DcpptSoapPasienModel) (res dto.ResponseToKeadaanUmum) {
	return dto.ResponseToKeadaanUmum{
		KeadaanUmum:     data.AseskepKeadaanUmum,
		Kesadaran:       data.AseskepKesadaran,
		KesadaranDetail: data.AseskepKesadaranDetail,
	}
}

// ToMapperPemeriksaanFisik(pemFisik rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksasanFisikAntonio)
func (rm *SoapMapperImpl) ToMapperPemeriksaanFisik(pemFisik rme.PemeriksaanFisikModel) (res dto.ResponsePemeriksasanFisikAntonio) {
	return dto.ResponsePemeriksasanFisikAntonio{
		Kepala:         pemFisik.Kepala,
		Mata:           pemFisik.Mata,
		Tht:            pemFisik.Tht,
		Mulut:          pemFisik.Mulut,
		Gigi:           pemFisik.Gigi,
		Leher:          pemFisik.Leher,
		Dada:           pemFisik.Dada,
		Jantung:        pemFisik.Jantung,
		Paru:           pemFisik.Paru,
		Perut:          pemFisik.Perut,
		Hati:           pemFisik.Hati,
		Limpa:          pemFisik.Limpa,
		Usus:           pemFisik.PeristatikUsus,
		Ginjal:         pemFisik.Ginjal,
		AbdomenLainnya: pemFisik.AbdomenLainnya,
		AlatKelamin:    pemFisik.AlatKelamin,
		Anus:           pemFisik.Anus,
		Superior:       pemFisik.EkstremitasSuperior,
		Inferior:       pemFisik.EkstremitasInferior,
	}
}

func (rm *SoapMapperImpl) ToResponseSkalaNyeri(nyeri rme.AsesmenSkalaNyeri) (res dto.ToResponseSkalaNyeri) {
	return dto.ToResponseSkalaNyeri{
		SkalaNyeri:     nyeri.AseskepSkalaNyeri,
		FrekuensiNyeri: nyeri.AseskepFrekuensiNyeri,
		LokasiNyeri:    nyeri.AseskepLokasiNyeri,
		KualitasNyeri:  nyeri.AseskepKualitasNyeri,
		Menjalar:       nyeri.AseskepNyeriMenjalar,
	}
}

func (rm *SoapMapperImpl) ToResponsePengkajianAwalKeperawatanRANAP(pengkajian igd.PengkajianKeperawatan, pengobatan []kebidanan.DRiwayatPengobatanDirumah) (res dto.ResponsePengkajianAwalKeperawatanDEWASA_RANAP) {
	var pengobatans = []kebidanan.DRiwayatPengobatanDirumah{}

	if len(pengobatan) > 0 {
		pengobatans = pengobatan
	} else {
		pengobatans = make([]kebidanan.DRiwayatPengobatanDirumah, 0)
	}

	return dto.ResponsePengkajianAwalKeperawatanDEWASA_RANAP{
		Pengkajian:        rm.RMEMapper.ToResponsePengkajianRANAP(pengkajian),
		PengobatanDirumah: pengobatans,
	}
}

func (km *SoapMapperImpl) ToFungsionalKebidananMapper(data kebidanan.DPengkajianFungsional) (res dto.ResDPengkajianFungsionalMapper) {
	var angka1 = 0
	var angka2 = 0
	var angka3 = 0
	var angka4 = 0
	var angka5 = 0
	var angka6 = 0
	var angka7 = 0
	var angka8 = 0
	var angka9 = 0
	var angka10 = 0

	if data.F8 == "Dengan Bantuan" {
		angka8 = 5
	}

	if data.F8 == "Mandiri" {
		angka8 = 10
	}

	if data.F9 == "Dengan Bantuan" {
		angka9 = 5
	}

	if data.F9 == "Mandiri" {
		angka9 = 10
	}

	if data.F10 == "Dengan Bantuan" {
		angka10 = 5
	}

	if data.F10 == "Mandiri" {
		angka10 = 10
	}

	if data.F1 == "Dengan Bantuan" {
		angka1 = 5
	}

	if data.F1 == "Mandiri" {
		angka1 = 10
	}

	if data.F2 == "Dengan Bantuan" {
		angka2 = 5
	}

	if data.F2 == "Mandiri" {
		angka2 = 10
	}

	if data.F3 == "Dengan Bantuan" {
		angka3 = 10
	}

	if data.F3 == "Mandiri" {
		angka3 = 15
	}

	if data.F4 == "Dengan Bantuan" {
		angka4 = 0
	}

	if data.F4 == "Mandiri" {
		angka4 = 5
	}

	if data.F5 == "Dengan Bantuan" {
		angka5 = 0
	}

	if data.F5 == "Mandiri" {
		angka5 = 5
	}

	if data.F6 == "Dengan Bantuan" {
		angka6 = 10
	}

	if data.F6 == "Mandiri" {
		angka6 = 15
	}

	if data.F7 == "Dengan Bantuan" {
		angka7 = 5
	}

	if data.F7 == "Mandiri" {
		angka7 = 10
	}

	return dto.ResDPengkajianFungsionalMapper{
		Noreg:    data.Noreg,
		F1:       data.F1,
		NilaiF1:  angka1,
		NilaiF2:  angka2,
		NilaiF3:  angka3,
		NilaiF4:  angka4,
		NilaiF5:  angka5,
		NilaiF6:  angka6,
		NilaiF7:  angka7,
		NilaiF8:  angka8,
		NilaiF9:  angka9,
		NilaiF10: angka10,
		F2:       data.F2,
		F3:       data.F3,
		F4:       data.F4,
		F5:       data.F5,
		F6:       data.F6,
		F7:       data.F7,
		F8:       data.F8,
		F9:       data.F9,
		F10:      data.F10,
		Nilai:    data.Nilai,
	}
}
