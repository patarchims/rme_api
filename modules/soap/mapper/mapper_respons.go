package mapper

import (
	"hms_api/modules/his"
	"hms_api/modules/kebidanan"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	soapDTO "hms_api/modules/soap/dto"
	"hms_api/modules/user"
)

func (a *SoapMapperImpl) ToMapperResponseIGDDokter(data1 soap.DVitalSignIGDDokter, data2 soap.DPemFisikIGDDokterRepository) (value soapDTO.TandaVitalIGDResponse) {
	var e = ""
	var v = ""
	var m = ""

	if data2.GcsE == "" || data2.GcsE == " " {
		e = " "
	} else {
		e = data2.GcsE
	}

	if data2.GcsV == "" || data2.GcsV == " " {
		v = " "
	} else {
		v = data2.GcsV
	}

	if data2.GcsM == "" || data2.GcsM == " " {
		m = " "
	} else {
		m = data2.GcsM
	}

	return soapDTO.TandaVitalIGDResponse{
		GCSE:       e,
		GCSV:       v,
		GCSM:       m,
		Td:         data1.Td,
		Nadi:       data1.Nadi,
		Suhu:       data1.Suhu,
		Kesadaran:  data2.Kesadaran,
		Pernafasan: data1.Pernafasan,
		Spo2:       data1.Spo2,
		Tb:         data1.Tb,
		Bb:         data1.Bb,
		Akral:      data2.Akral,
		Pupil:      data2.Pupil,
	}

}

func (a *SoapMapperImpl) ToMappingAlasanOpname(data soap.RencanaTindakLanjutModel) (value soapDTO.ResponseTindakLanjutIGDResponse) {
	return soapDTO.ResponseTindakLanjutIGDResponse{
		Noreg:        data.Noreg,
		Prognosis:    data.AsesmenPrognosis,
		KdBagian:     data.KdBagian,
		AlasanKonsul: data.AsesmedKonsulKe,
		Terapi:       data.AsesmedTerapi,
		KonsulKe:     data.AsesmedKonsulKe,
		AlasanOpname: data.AsesmedAlasanOpname,
	}
}

func (a *SoapMapperImpl) ToMappingResikoJatuhGoUpAndGoTest(data soap.ResikoJatuhGetUpGoTest) (res soapDTO.ResponseResikoJatuhGoUpGoTest) {
	return soapDTO.ResponseResikoJatuhGoUpGoTest{
		ResikoJatuh1: data.AseskepRj1,
		ResikoJatuh2: data.AseskepRj2,
		Tindakan:     data.AseskepHslKajiRj + data.AseskepHslKajiRjTind,
	}
}

func (a *SoapMapperImpl) ToMappingResponseAsesmenIGD(alergi []rme.DAlergi, riwayat []rme.RiwayatPenyakitDahuluPerawat, pengkajian soap.AsesemenIGD) (res soapDTO.ResponseAsesmenIGD) {
	var alergis = []rme.DAlergi{}
	var riwayats = []rme.RiwayatPenyakitDahuluPerawat{}

	if len(alergi) == 0 {
		alergis = make([]rme.DAlergi, 0)
	} else {
		alergis = alergi
	}

	if len(riwayat) == 0 {
		riwayats = make([]rme.RiwayatPenyakitDahuluPerawat, 0)
	} else {
		riwayats = riwayat
	}

	return soapDTO.ResponseAsesmenIGD{
		Penkajian: a.ToResponseIGDMapepr(pengkajian),
		Alergi:    alergis,
		Riwayat:   riwayats,
	}
}

func (a *SoapMapperImpl) ToResponseIGDMapepr(data soap.AsesemenIGD) (res soapDTO.IGDResponse) {
	return soapDTO.IGDResponse{
		AsesmedKeluhUtama:          data.AseskepKel,
		AsesmedRwytSkrg:            data.AseskepRwytPnykt,
		CaraKeluar:                 data.CaraKeluar,
		AseskepPerolehanInfo:       data.AseskepPerolehanInfo,
		AseskepRwytPnykt:           data.AseskepRwytPnyktDahulu,
		AseskepPerolehanInfoDetail: data.AseskepPerolehanInfoDetail,
		AseskepCaraMasuk:           data.AseskepCaraMasuk,
		AseskepCaraMasukDetail:     data.AseskepCaraMasukDetail,
		AseskepAsalMasuk:           data.AseskepAsalMasuk,
		AseskepAsalMasukDetail:     data.AseskepAsalMasukDetail,
		AseskepRwytObat:            data.AseskepRwytObat,
		AseskepAsesFungsional:      data.AseskepAsesFungsional,
		ReaksiAlergi:               data.AseskepReaksiAlergi,
	}
}

func (a *SoapMapperImpl) TOMappingResponsePengkajianKeperawatan(data soap.PengkajianAwalKeperawatan, alergi []rme.DAlergi, riwayat []rme.RiwayatPenyakitDahuluPerawat) (value soapDTO.ResponseAsesmenKeperawatan) {

	if len(alergi) == 0 {
		return soapDTO.ResponseAsesmenKeperawatan{
			Riwayat: riwayat,
			Alergi:  make([]rme.DAlergi, 0),
			Pengkajian: soapDTO.PenkajianResponse{
				Noreg:                 data.Noreg,
				Person:                data.KeteranganPerson,
				KdDpjp:                data.KdDpjp,
				AseskepJenpel:         data.AseskepPerolehanInfo,
				AseskepJenpelDetail:   data.AseskepPerolehanInfoDetail,
				AseskepKel:            data.AseskepKel,
				AseskepRwytPnykt:      data.AseskepRwytPnykt,
				AseskepReaksiAlergi:   data.AseskepReaksiAlergi,
				RiwayatPenyakitDahulu: data.AseskepRwytPnyktDahulu,
			},
		}
	}

	if len(riwayat) == 0 {
		return soapDTO.ResponseAsesmenKeperawatan{
			Riwayat: make([]rme.RiwayatPenyakitDahuluPerawat, 0),
			Alergi:  alergi,
			Pengkajian: soapDTO.PenkajianResponse{
				Noreg:                 data.Noreg,
				Person:                data.KeteranganPerson,
				KdDpjp:                data.KdDpjp,
				AseskepJenpel:         data.AseskepPerolehanInfo,
				AseskepJenpelDetail:   data.AseskepPerolehanInfoDetail,
				AseskepKel:            data.AseskepKel,
				AseskepRwytPnykt:      data.AseskepRwytPnykt,
				AseskepReaksiAlergi:   data.AseskepReaksiAlergi,
				RiwayatPenyakitDahulu: data.AseskepRwytPnyktDahulu,
			},
		}
	}

	if len(riwayat) == 0 && len(alergi) == 0 {
		return soapDTO.ResponseAsesmenKeperawatan{
			Riwayat: make([]rme.RiwayatPenyakitDahuluPerawat, 0),
			Alergi:  make([]rme.DAlergi, 0),
			Pengkajian: soapDTO.PenkajianResponse{
				Noreg:                 data.Noreg,
				Person:                data.KeteranganPerson,
				KdDpjp:                data.KdDpjp,
				AseskepJenpel:         data.AseskepPerolehanInfo,
				AseskepJenpelDetail:   data.AseskepPerolehanInfoDetail,
				AseskepKel:            data.AseskepKel,
				AseskepRwytPnykt:      data.AseskepRwytPnykt,
				AseskepReaksiAlergi:   data.AseskepReaksiAlergi,
				RiwayatPenyakitDahulu: data.AseskepRwytPnyktDahulu,
			},
		}
	}

	return soapDTO.ResponseAsesmenKeperawatan{
		Alergi:  alergi,
		Riwayat: riwayat,
		Pengkajian: soapDTO.PenkajianResponse{
			Noreg:                 data.Noreg,
			Person:                data.KeteranganPerson,
			KdDpjp:                data.KdDpjp,
			AseskepJenpel:         data.AseskepPerolehanInfo,
			AseskepJenpelDetail:   data.AseskepPerolehanInfoDetail,
			AseskepKel:            data.AseskepKel,
			AseskepRwytPnykt:      data.AseskepRwytPnykt,
			AseskepReaksiAlergi:   data.AseskepReaksiAlergi,
			RiwayatPenyakitDahulu: data.AseskepRwytPnyktDahulu,
		},
	}

}

func (a *SoapMapperImpl) ToMappingResponsePengkajiaKeperawatanRawatInap(pengkajian soap.PengkajianAwalKeperawatan, karyawan user.Karyawan, pemeriksaanFisik soapDTO.ResponsePemeriksasanFisikAntonio, skalaNyeri soapDTO.ToResponseSkalaNyeri, fungsionalMapepr dto.ResDPengkajianFungsionalMapper, resikoJatuh kebidanan.DRisikoJatuhKebidanan, nutrisi dto.ResponseKebidananModel, asuhan []rme.DasKepDiagnosaModelV2, pengobataDirumah []kebidanan.DRiwayatPengobatanDirumah) (res soapDTO.ResponsePengkajianKeperawatanRawatInap) {
	return soapDTO.ResponsePengkajianKeperawatanRawatInap{
		PengkajianKeperawatan:    pengkajian,
		Karyawan:                 karyawan,
		PemFisik:                 pemeriksaanFisik,
		SkalaNyeri:               skalaNyeri,
		Fungsional:               fungsionalMapepr,
		ResikoJatuh:              resikoJatuh,
		PengkajianNutrisi:        nutrisi,
		AsuhanKeperawatan:        asuhan,
		RiwayatPengobatanDirumah: pengobataDirumah,
	}
}

func (rm *SoapMapperImpl) ToMappingReportAsesmenDokterAntonio(riwayatDahulu []rme.KeluhanUtama, keluh rme.AsesemenDokterIGD, pengkajianKeperawatan soap.PengkajianAwalKeperawatan, pemFisik rme.PemeriksaanFisikDokterAntonio, karyawan user.Karyawan, tindakan []soap.TindakanResponse, labor []his.ResHasilLaborTableLama, radiologin []his.RegHasilRadiologiTabelLama) (res dto.ResposeReportAsesmenDokterAntonio) {

	if pengkajianKeperawatan.AseskepKel != "" {

		data := rme.ResponseAsesemenDokterIGD{
			Noreg:             pengkajianKeperawatan.Noreg,
			AsesmedKeluhUtama: pengkajianKeperawatan.AseskepKel,
			AsesmedRwytSkrg:   pengkajianKeperawatan.AseskepRwytPnykt,
			RiwayatDahulu:     pengkajianKeperawatan.AseskepRwytPnyktDahulu,
		}

		return dto.ResposeReportAsesmenDokterAntonio{
			KeluhanUtama:     data,
			PemeriksaanFisik: pemFisik.Keterangan,
			Karyawan:         karyawan,
			Tindakan:         tindakan,
			HasilLabor:       labor,
			Radiologi:        radiologin,
		}

	} else {

		data := rme.ResponseAsesemenDokterIGD{
			Noreg:             keluh.Noreg,
			AsesmedKeluhUtama: keluh.AsesmedKeluhUtama,
			AsesmedRwytSkrg:   keluh.AsesmedRwytSkrg,
			RiwayatDahulu:     keluh.AsesmedRwytDahulu,
		}

		return dto.ResposeReportAsesmenDokterAntonio{
			KeluhanUtama:     data,
			PemeriksaanFisik: pemFisik.Keterangan,
			Karyawan:         karyawan,
			Tindakan:         tindakan,
			HasilLabor:       labor,
			Radiologi:        radiologin,
		}
	}
}
