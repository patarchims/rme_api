package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/rme"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"time"
)

func (sr *soapRepository) GetTriaseRiwayatAlergi(noReg string, kdBagian string) (res soap.TriaseRiwayatAlergi, err error) {

	errs := sr.DB.Select("asesmed_keluh_utama", "noreg", "kd_bagian").Where(&soap.TriaseRiwayatAlergi{Noreg: noReg, KdBagian: kdBagian}).First(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) GetKeluhanUtamaIGDRepository(noReg string, kdBagian string) (res soap.KeluhanUtamaPerawatIGD, err error) {

	errs := sr.DB.Select("aseskep_kel", "noreg", "kd_bagian").Where(&soap.KeluhanUtamaPerawatIGD{Noreg: noReg, KdBagian: kdBagian}).First(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) SaveKeluhanUtamaIGDRepository(kdBagian string, tglMasuk string, req dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, err error) {

	times := time.Now()

	data := soap.KeluhanUtamaPerawatIGD{
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
		UpdDttm:          times.Format("2006-01-02 15:04:05"),
		AseskepKel:       req.KeluhanUtama,
		Noreg:            req.Noreg,
		KdBagian:         kdBagian,
		TglMasuk:         tglMasuk,
		KeteranganPerson: req.KeteranganPerson,
		InsertUserId:     req.InsertUserId,
		InsertPc:         req.DeviceID,
		Pelayanan:        req.Pelayanan,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

// INSERT TRIASE RIWAYAT ALERGI
func (sr *soapRepository) InsertTriaseRiwayatAlergi(noReg string, kdBagian string) (res soap.TriaseRiwayatAlergi, err error) {
	data := soap.TriaseRiwayatAlergi{Noreg: noReg}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil

}

func (sr *soapRepository) InsertOndontogramUseCaseV2(req dto.InsertOdontogramV2) (res soap.Odontograms, err error) {
	data := soap.Odontograms{Number: req.Number, Keterangan: req.Keterangan}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) SaveSkriningV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error) {

	var datetime = time.Now().UTC()
	dt := datetime.Format("2006-01-02 15:04:05")

	skrining := soap.DcpptSoapPasien{InsertDttm: dt, Noreg: req.NoReg, KdBagian: req.KodeBagian, SkriningK1: req.K1, SkriningK2: req.K2,
		SkriningK3: req.K3, SkriningK4: req.K4, SkriningK5: req.K5, SkriningK6: req.K6, SkriningF1: req.KF1, SkriningF2: req.KF2, SkriningF3: req.KF3,
		SkriningF4: req.KF4, SkriningB1: req.B1, SkriningB2: req.B2, SkriningRj: req.RJ, SkriningR1: req.IV1, SkriningR2: req.IV2, SkriningR3: req.IV3,
		SkriningR4: req.IV4, SkriningTgl: dt, AseskepTgl: dt,
	}

	result := sr.DB.Create(&skrining)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error.Error())
		return skrining, errors.New(message)
	}

	return skrining, nil
}

func (sr *soapRepository) SaveAnatomiRepositoryV2(nama string, norm string, keterangan string, urlImage string, dpjp string) (res soap.Anatomi, err error) {
	anatomi := soap.Anatomi{Norm: norm, Nama: nama, Keterangan: keterangan, UrlImage: urlImage, Dpjp: dpjp}
	result := sr.DB.Create(&anatomi)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error.Error())
		return res, errors.New(message)
	}

	return anatomi, nil
}

func (sr *soapRepository) SaveAssesKebEdukasiV2(req dto.ReqKebEdukasiV2) (res soap.KebEdukasi, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesedu_ke1=? , asesedu_ke2=?,asesedu_ke3=?, asesedu_ke4=?, asesedu_ke5=?, asesedu_ke6=?, asesedu_ke7=?, asesedu_ke8=?,
				asesedu_ke9=?, asesedu_ke10=?, asesedu_ke11=?, asesedu_ke11_detail=?, asesedu_tu1=?, asesedu_tu2=?,
				asesedu_kb1=?, asesedu_kb2=?, asesedu_kb3=?, asesedu_mb1=?,asesedu_mb2=?, asesedu_mb3=?, asesedu_np1=?,
				asesedu_np2=?, asesedu_np3=?, asesedu_np4=?, asesedu_np5=?, asesedu_np6=?, asesedu_np6_detail=?,
				asesedu_ha1=?, asesedu_ha2=?, asesedu_ha3=?, asesedu_ha4=?, asesedu_ha5=?, asesedu_ha6=?, asesedu_ha7=?,
				asesedu_ha8=?, asesedu_ha9=?, asesedu_ha9_detail=?, asesedu_in1=?, asesedu_in2=?, asesedu_in3=?, asesedu_in4=?,
				asesedu_in5=?, asesedu_hasil1=?, asesedu_hasil2=?, asesedu_rencana_edukasi=?, asesedu_tgl=?, asesedu_jam=?, asesedu_user=?
				WHERE noreg=?;`

	result := sr.DB.Raw(query, req.Edukasi.KebEdu1, req.Edukasi.KebEdu2, req.Edukasi.KebEdu3, req.Edukasi.KebEdu4, req.Edukasi.KebEdu5,
		req.Edukasi.KebEdu6, req.Edukasi.KebEdu7, req.Edukasi.KebEdu8, req.Edukasi.KebEdu9, req.Edukasi.KebEdu10, req.Edukasi.KebEdu11, req.Edukasi.KebEdu11Detail, req.Tujuan.Tujuan1, req.Tujuan.Tujuan2, req.KempBelajar.KempBelajar1, req.KempBelajar.KempBelajar2, req.KempBelajar.KempBelajar3,
		req.MotivasiBelajar.MotivasiBel1, req.MotivasiBelajar.MotivasiBel2, req.MotivasiBelajar.MotivasiBel3, req.NilaiPasien.NilaiPasien1, req.NilaiPasien.NilaiPasien2, req.NilaiPasien.NilaiPasien3, req.NilaiPasien.NilaiPasien4, req.NilaiPasien.NilaiPasien5, req.NilaiPasien.NilaiPasien6, req.NilaiPasien.NilaiPasien6Detail, req.Hambatan.Hambatan1, req.Hambatan.Hambatan2, req.Hambatan.Hambatan3, req.Hambatan.Hambatan4, req.Hambatan.Hambatan5,
		req.Hambatan.Hambatan6, req.Hambatan.Hambatan7, req.Hambatan.Hambatan8, req.Hambatan.Hambatan9, req.Hambatan.Hambatan9Detail,
		req.IntervensiRes.Intervensi1, req.IntervensiRes.Intervensi2, req.IntervensiRes.Intervensi3, req.IntervensiRes.Intervensi4, req.IntervensiRes.Intervensi5,
		req.Hasil.Hasil1, req.Hasil.Hasil2, req.RencanaEdukasi, req.AsseEduTgl, req.AsseEduJam, req.AsseEduUser, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) DeleteDiagnosaRepositoryV2(req dto.ReqDeleteDiagnosaV2, kdBagian string) (err error) {

	var datas soap.DiagnosaModelOne
	value := "asesmed_diag" + req.Table

	query := `UPDATE vicore_rme.dcppt_soap_dokter  SET ` + value + ` =""  WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.Noreg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return errors.New(message)
	}

	return nil
}

func (sr *soapRepository) GetInformasiMedisRepository(req dto.ReqDataPasienV2) (res soap.InformasiMedis, err error) {
	errs := sr.DB.Select("insert_dttm", "kd_bagian", "noreg", "asesmed_mslh_medis", "asesmed_terapi", "asesmed_pem_fisik", "asesmed_anjuran").Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).First(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) SearchDcpptSoapPasienV2(noReg string, kdBagian string) (res soap.DcpptSoap, err error) {
	results := sr.DB.Select("kd_bagian, noreg").Where(&soap.DcpptSoap{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (sr *soapRepository) SaveAnamnesaV2(req dto.RequestSaveAnamnesaV2) (err error) {
	data := soap.Anamnesa{}
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_keluh_utama=?, asesmed_jenpel=?, asesmed_rwyt_skrg=?, asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?, asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.AsesmedKeluhUtama, req.AsesmedJenpel,
		req.AsesmenRwtSkrg, req.AsesmenRwtDulu, req.AsesmenRwtObat, req.AsesmenRwtPenyKlrg, req.AsesmenRwtPenyAlergi,
		req.AsesmenRwtPenyAlergiDetail, req.AsesmenKhusPemGigi1, req.AsesmenKhusPemGigi2, req.AsesmenKhusPemGigi3,
		req.AsesmenKhusPemGigi4, req.AsesmenKhusPemGigi5, req.AsesmenKhusPemGigi5Detail, req.NoReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return errors.New(message)
	}

	return nil
}

func (sr *soapRepository) InsertSingleDiagnosaFirstRepositoryV2(req dto.ReqInputSingleDiagnosaV2, jamMasuk string, tglMasuk string, kdBagian string, kdDpjp string) (res soap.DiagnosaModelOne, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian, KdDpjp: kdDpjp}).Save(soap.DiagnosaModelOne{
		AsesmedDiagp:        req.Code,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		KeteranganPerson:    req.KeteranganPerson,
		TglMasuk:            tglMasuk,
		KdDpjp:              kdDpjp,
		JamMasuk:            jamMasuk,
		InsertUserId:        req.InsertUserId,
		InsertPc:            req.DeviceID,
		Pelayanan:           req.Pelayanan,
		KdBagian:            kdBagian,
		Noreg:               req.Noreg,
		AsesmedDokterTtd:    req.InsertUserId,
		AsesmedDokterTtdTgl: times.Format("2006-01-02"),
		AsesmedDokterTtdJam: times.Format("15:04:05"),
	}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertSingleFirstTindakanRepository(req dto.RegInputTindakan, jamMasuk string, tglMasuk string, kdBagian string, kdDpjp string) (res soap.DiagnosaModelOne, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian, KdDpjp: kdDpjp}).Save(soap.DiagnosaModelOne{
		AsesmedPros1:        req.Code,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		KeteranganPerson:    req.KeteranganPerson,
		TglMasuk:            tglMasuk,
		KdDpjp:              kdDpjp,
		JamMasuk:            jamMasuk,
		InsertUserId:        req.InsertUserId,
		InsertPc:            req.DeviceID,
		Pelayanan:           req.Pelayanan,
		KdBagian:            kdBagian,
		Noreg:               req.Noreg,
		AsesmedDokterTtd:    req.InsertUserId,
		AsesmedDokterTtdTgl: times.Format("2006-01-02"),
		AsesmedDokterTtdJam: times.Format("15:04:05"),
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) InputSingleDiagnosaRepositoryV2(req dto.ReqInputSingleDiagnosaV2, kdBagian string) (res soap.DiagnosaModelOne, err error) {

	if req.Type == "primer" {
		errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
			AsesmedDiagp: req.Code}).Scan(&res).Error
		if errs != nil {
			return res, errs
		}
		return res, nil
	}

	if req.Type == "sekunder" {
		switch req.Table {
		case "S1":

			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags1: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		case "S2":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags2: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S3":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags3: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S4":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags4: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S5":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags5: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		case "S6":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags6: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		}
	}

	return res, nil
}

func (sr *soapRepository) InputSingleTindakanRepository(req dto.RegInputTindakan, kdBagian string) (res soap.DiagnosaModelOne, err error) {

	var datas soap.DiagnosaModelOne

	// var code = req.Code

	value := "asesmed_pros" + req.Index

	query := `UPDATE vicore_rme.dcppt_soap_dokter  SET ` + value + ` = '` + req.Code + `'  WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.Noreg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, errors.New(message)
	}

	return datas, nil
}

func (sr *soapRepository) UpdateOdontogramV2(req dto.RequestSaveOdontogramV2) (res soap.DcpptSoapPasien, err error) {

	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =?  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.Keterangan, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) InsertDcpptSoapPasienRepositoryV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error) {
	times := time.Now()
	data := soap.AssementRawatJalanPerawat{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		AseskepKel: req.KelUtama, AseskepRwytPnykt: req.RiwayatPenyakit,
		AseskepTd: req.TekananDarah, AseskepNadi: req.Nadi, AseskepSuhu: req.Suhu, AseskepRr: req.Pernapasan,
		AseskepBb: req.BeratBadan, AseskepTb: req.TinggiBadan, AseskepAsesFungsional: req.Fungsional,
		AseskepMslhKprwtan: req.MasalahKeperawatan, AseskepRencKprwtan: req.RencanaKeperawatan, Noreg: req.NoReg, KdBagian: req.KodeBagian,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil

}

func (sr *soapRepository) UpdateDcpptSoapPasienRepositoryV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_kel=?, aseskep_rwyt_pnykt=?, aseskep_rwyt_obat=?, aseskep_rwyt_obat_detail=?, aseskep_td=?, aseskep_nadi=?, aseskep_suhu=?, aseskep_rr=?, aseskep_bb=?, aseskep_tb=?, aseskep_ases_fungsional=?, aseskep_ases_fungsional_detail=?, aseskep_rj1=?, aseskep_rj2=?, aseskep_hsl_kaji_rj=?, aseskep_hsl_kaji_rj_tind=?,aseskep_ases_nyeri=?, aseskep_psiko=?, aseskep_psiko_detail=?, aseskep_pulang1=?, aseskep_pulang1_detail=?, aseskep_pulang2=?, aseskep_pulang2_detail=?, aseskep_pulang3=?, aseskep_pulang3_detail=?, aseskep_mslh_kprwtan=?, aseskep_renc_kprwtan=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.KelUtama, req.RiwayatPenyakit, req.RiwayatObat, req.RiwayatObatDetail, req.TekananDarah, req.Nadi, req.Suhu, req.Pernapasan, req.BeratBadan, req.TinggiBadan, req.Fungsional, req.FungsionalDetail, req.AseskepRj1, req.AseskepRj2, req.AseskepHasilKajiRj, req.AseskepHslKajiRjTind, req.AseskepNyeri, req.Psikologis, req.PsikologisDetail, req.AseskepPulang1, req.AseskepPulang1Detail, req.AseskepPulang2, req.AseskepPulang2Detail, req.AseskepPulang3, req.AseskepPulang3Detail, req.MasalahKeperawatan, req.RencanaKeperawatan, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) UpdateAssesRawatJalanDokterV2(req dto.RequestSaveAssementRawatJalanDokterV2) (res soap.DcpptOdontogram, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET asesmed_jenpel=?, asesmed_keluh_utama=?, asesmed_rwyt_skrg=?,
	asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?,
	asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=?, asesmed_gol_darah=?,
	asesmed_td=?, asesmed_td_detail=?, asesmed_peny_jantung=?, asesmed_peny_jantung_detail=?, asesmed_diabet=?, asesmed_diabet_detail=?, asesmed_haemop=?,
	asesmed_haemop_detail=?, asesmed_hepat=?, asesmed_hepat_detail=?, asesmed_peny_lain=?, asesmed_peny_lain_detail=?, asesmed_alergi_obat=?, asesmed_alergi_obat_detail=?, 
	asesmed_alergi_mknan=?, asesmed_alergi_mknan_detail=?, asesmed_kebiasaan_buruk=?, asesmed_khus_ok_anterior=?, asesmed_khus_ok_posterior=?, asesmed_khus_ok_molar=?, 
	asesmed_khus_palatum=?, asesmed_khus_torus_P=?, asesmed_khus_torus_M=?, asesmed_khus_torus_M_detail=?, asesmed_khus_super_teeth=?, asesmed_khus_super_teeth_detail=?, 
	asesmed_khus_diastema=?, asesmed_khus_diastema_detail=?, asesmed_khus_gigi_anomali=?, asesmed_khus_gigi_anomali_detail=?, asesmed_khus_oral_lain=?
	WHERE noreg=?;`

	result := sr.DB.Raw(query, req.Pelayanan, req.KeluhanUtama, req.AsesmedRwytSkrg, req.AsesmedRwytDulu,
		req.AssesRiwayat.AsesmedRwytObat, req.AssesRiwayat.AsesmedRwytPenyKlrg, req.AssesRiwayat.AsesmedRwytAlergi, req.AssesRiwayat.AsesmedRwytAlergiDetail,
		req.AsesmedKhusPemGigi1, req.AsesmedKhusPemGigi2, req.AsesmedKhusPemGigi3, req.AsesmedKhusPemGigi4, req.AsesmedKhusPemGigi5, req.AsesmedKhusPemGigi5Detail,
		req.AsesmedGolDarah, req.AsesmedTd, req.AsesmedTdDetail, req.AsesmedPenyJantung, req.AsesmedPenyJantungDetail, req.AsesmedDiabet, req.AsesmedDiabetDetail, req.AsesmedHaemop, req.AsesmedHaemopDetail, req.AsesmedHepat, req.AsesmedHepatDetail, req.AsesmedPenyLain, req.AsesmedPenyLainDetail, req.AsesmedAlergiObat, req.AsesmedAlergiObatDetail,
		req.AsesmedAlergiMknan, req.AsesmedAlergiMknanDetail, req.AsesmedKebiasaanBuruk, req.AsesmedKhusOkAnterior, req.AsesmedKhusOkPosterior, req.AsesmedKhusOkMolar, req.AsesmedKhusPalatum,
		req.AsesmedKhusTorusP, req.AsesmedKhusTorusM, req.AsesmedKhusTorusMDetail, req.AsesmedKhusSuperTeeth, req.AsesmedKhusSuperTeethDetail,
		req.AsesmedKhusDiastema, req.AsesmedDiabetDetail, req.AsesmedKhusGigiAnomali, req.AsesmedKhusGigiAnomaliDetail, req.AsesmedKhusOralLain,
		req.NoReg).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) InsertAssesRawatJalanDokterRepositoryV2(userID string, req dto.RequestSaveAssementRawatJalanDokterV2) (res soap.DcpptOdontogram, err error) {
	times := time.Now()
	data := soap.DcpptOdontogram{
		InsertDttm:                   times.Format("2006-01-02 15:04:05"),
		InsertUserID:                 userID,
		KdBagian:                     req.KdBagian,
		Noreg:                        req.NoReg,
		AsesmedJenpel:                req.Pelayanan,
		AsesmedKeluhUtama:            req.KeluhanUtama,
		AsesmedRwytSkrg:              req.AssesRiwayat.AsesmedRwytSkrg,
		AsesmedRwytDulu:              req.AssesRiwayat.AsesmedRwytDulu,
		AsesmedRwytObat:              req.AssesRiwayat.AsesmedRwytObat,
		AsesmedRwytPenyKlrg:          req.AssesRiwayat.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergi:            req.AssesRiwayat.AsesmedRwytAlergi,
		AsesmedRwytAlergiDetail:      req.AssesRiwayat.AsesmedRwytAlergiDetail,
		AsesmedKhusPemGigi1:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail:    req.AssesPemeriksaanGigi.AsesmedKhusPemGigi5Detail,
		AsesmedGolDarah:              req.AsesmedGolDarah,
		AsesmedTd:                    req.AsesmedTd,
		AsesmedTdDetail:              req.AsesmedTdDetail,
		AsesmedPenyJantung:           req.AsesmedPenyJantung,
		AsesmedPenyJantungDetail:     req.AsesmedPenyJantungDetail,
		AsesmedDiabet:                req.AsesmedDiabet,
		AsesmedDiabetDetail:          req.AsesmedDiabetDetail,
		AsesmedHaemop:                req.AsesmedHaemop,
		AsesmedHaemopDetail:          req.AsesmedAlergiMknanDetail,
		AsesmedHepat:                 req.AsesmedHepat,
		AsesmedHepatDetail:           req.AsesmedHepatDetail,
		AsesmedPenyLain:              req.AsesmedPenyLain,
		AsesmedPenyLainDetail:        req.AsesmedPenyLainDetail,
		AsesmedAlergiObat:            req.AsesmedAlergiObat,
		AsesmedAlergiObatDetail:      req.AsesmedAlergiObatDetail,
		AsesmedAlergiMknan:           req.AsesmedAlergiMknan,
		AsesmedAlergiMknanDetail:     req.AsesmedAlergiMknanDetail,
		AsesmedKebiasaanBuruk:        req.AsesmedKebiasaanBuruk,
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusMDetail:      req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

// INSERT DATA INFORMASI MEDIS
func (sr *soapRepository) InsertInformasiMedisRepositoryV2(req dto.ReqInformasiMedisV2) (res soap.InformasiMedis, err error) {
	times := time.Now()
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).Save(soap.InformasiMedis{
		AsesmedMslhMedis: req.MasalahMedis,
		AsesmedTerapi:    req.Terapi,
		AsesmedPemFisik:  req.PemeriksaanFisik,
		AsesmedAnjuran:   req.Anjuran,
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
	}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

/*
UPDATE INFORMASI MEDIS
*/
func (sr *soapRepository) UpdateInformasiMedisRepositoryV2(req dto.ReqInformasiMedisV2) (res soap.InformasiMedis, err error) {
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).Updates(soap.InformasiMedis{
		AsesmedMslhMedis: req.MasalahMedis,
		AsesmedTerapi:    req.Terapi,
		AsesmedPemFisik:  req.PemeriksaanFisik,
		AsesmedAnjuran:   req.Anjuran,
	}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) DeleteOdontogramV2(req dto.RequestDeleteOdontogramV2) (res soap.DcpptSoapPasien, err error) {
	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =""  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, result.AddError(errors.New(message))
	}

	return res, nil
}

func (sr *soapRepository) UpdateSkriningV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error) {

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET skrining_k1=?, skrining_k2=?, skrining_k3=?,   skrining_k4=? , skrining_k5=? ,
	skrining_k6=?, skrining_f1=?, skrining_f2=?, skrining_f3=?, skrining_f4=?, skrining_b1=?, skrining_b2=?, skrining_rj=?, skrining_r1=?,
	skrining_r2=?, skrining_r3=?, skrining_r4=?, skrining_user=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.K1, req.K2, req.K3, req.K4, req.K5, req.K6, req.KF1, req.KF2, req.KF3, req.KF4, req.B1, req.B2, req.RJ,
		req.IV1, req.IV2, req.IV3, req.IV4, req.User, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) SaveAsesmedAnamnesaRepositoryV2(req dto.ReqSaveAsesmedAnamnesaV2, kdBagian string) (res soap.AsesmedAnamnesa, err error) {
	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.Noreg}).Save(&soap.AsesmedAnamnesa{
		AsesmedKeluhUtama:         req.AsesmedKeluhUtama,
		AsesmedRwytSkrg:           req.AsesmedRwytSkrg,
		AsesmedRwytPenyKlrg:       req.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergiDetail:   req.AsesmedRwytAlergiDetail,
		AsesmedJenpel:             req.AsesmedJenpel,
		AsesmedKhusPemGigi1:       req.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:       req.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:       req.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:       req.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:       req.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail: req.AsesmedKhusPemGigi5Detail,
		AsesmedRwytAlergi:         req.AsesmedRwytAlergi,
		KdBagian:                  kdBagian,
		Noreg:                     req.Noreg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateDataAnamnesaRepositoryV2(req dto.ReqSaveAsesmedAnamnesaV2, kdBagian string) (res soap.AsesmedAnamnesa, err error) {
	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.AsesmedAnamnesa{
		AsesmedKeluhUtama:         req.AsesmedKeluhUtama,
		AsesmedRwytSkrg:           req.AsesmedRwytSkrg,
		AsesmedRwytPenyKlrg:       req.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergiDetail:   req.AsesmedRwytAlergiDetail,
		AsesmedJenpel:             req.AsesmedJenpel,
		AsesmedKhusPemGigi1:       req.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:       req.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:       req.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:       req.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:       req.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail: req.AsesmedKhusPemGigi5Detail,
		AsesmedRwytAlergi:         req.AsesmedRwytAlergi,
		KdBagian:                  kdBagian,
		Noreg:                     req.Noreg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) SaveAnamnesaIGDRepositoryV2(kdBagian string, req dto.AnamnesaIGDV2, tanggalMasuk string, jamMasuk string) (res soap.AsesmedAwalPasienIGDModel, err error) {
	times := time.Now()

	data := soap.AsesmedAwalPasienIGDModel{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: req.NoReg, AsesmedKeluhUtama: req.AsesmedKeluhUtama, AsesmedTelaah: req.AsesmendTelaah, AsesmedMslhMedis: req.AsesmendMslhMedis, AsesmedRwytSkrg: req.AsesmedRwytSkrg, AsesmedRwytObat: req.AsesmedRwytObat, AsesmedJenpel: req.AsesmedJenpel, KeteranganPerson: req.KeteranganPerson, InsertUserId: req.InsertUserId,
		InsertPc: req.DeviceID, Pelayanan: req.Pelayanan, TglMasuk: tanggalMasuk, JamMasuk: jamMasuk,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) UpdateAnamnesaIGDRepositoryV2(kdBagian string, req dto.AnamnesaIGDV2) (res soap.AsesmedAwalPasienIGDModel, err error) {

	times := time.Now()

	errs := sr.DB.Where(&soap.AsesmedAwalPasienIGDModel{KdBagian: kdBagian, Noreg: req.NoReg}).Updates(&soap.AsesmedAwalPasienIGDModel{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		AsesmedKeluhUtama: req.AsesmedKeluhUtama, AsesmedTelaah: req.AsesmendTelaah, AsesmedMslhMedis: req.AsesmendMslhMedis, AsesmedJenpel: req.AsesmedJenpel,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) SaveDataMedikRepositoryV2(req dto.ReqDataMedikV2, kdBagian string) (res soap.DataMedikModel, err error) {
	errs := sr.DB.Select("asesmed_gol_darah", "asesmed_td", "asesmed_td_detail", "asesmed_peny_jantung",
		"asesmed_peny_jantung_detail", "asesmed_diabet", "asesmed_diabet_detail", "asesmed_haemop", "asesmed_haemop_detail", "asesmed_hepat", "asesmed_hepat_detail", "asesmed_peny_lain",
		"asesmed_peny_lain_detail", "asesmed_alergi_obat", "asesmed_alergi_obat_detail", "asesmed_alergi_mknan", "asesmed_alergi_mknan_detail", "asesmed_kebiasaan_buruk").Where(&soap.DataMedikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Save(&soap.DataMedikModel{AsesmedGolDarah: req.AsesmedGolDarah, AsesmedTd: req.AsesmedTd, AsesmedTdDetail: req.AsesmedTdDetail, AsesmedPenyJantung: req.AsesmedPenyJantung, AsesmedPenyJantungDetail: req.AsesmedPenyJantungDetail, AsesmedDiabet: req.AsesmedDiabet, AsesmedDiabetDetail: req.AsesmedDiabetDetail, AsesmedHaemop: req.AsesmedHaemop, AsesmedHaemopDetail: req.AsesmedHaemopDetail, AsesmedHepat: req.AsesmedHepat,
		AsesmedHepatDetail: req.AsesmedHepatDetail, AsesmedPenyLain: req.AsesmedPenyLain, AsesmedPenyLainDetail: req.AsesmedPenyLainDetail, AsesmedAlergiObat: req.AsesmedAlergiObat, AsesmedAlergiObatDetail: req.AsesmedAlergiObatDetail, AsesmedAlergiMknan: req.AsesmedAlergiMknan, AsesmedAlergiMknanDetail: req.AsesmedAlergiMknanDetail, AsesmedKebiasaanBuruk: req.AsesmedKebiasaanBuruk,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateDataMedikRepositoryV2(req dto.ReqDataMedikV2, kdBagian string) (res soap.DataMedikModel, err error) {
	errs := sr.DB.Select(
		"asesmed_gol_darah", "asesmed_td", "asesmed_td_detail", "asesmed_peny_jantung", "asesmed_peny_jantung_detail",
		"asesmed_diabet", "asesmed_diabet_detail", "asesmed_haemop", "asesmed_haemop_detail", "asesmed_hepat", "asesmed_hepat_detail", "asesmed_peny_lain", "asesmed_peny_lain_detail", "asesmed_alergi_obat",
		"asesmed_alergi_obat_detail", "asesmed_alergi_mknan", "asesmed_alergi_mknan_detail", "asesmed_kebiasaan_buruk").Where(&soap.DataMedikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.DataMedikModel{AsesmedGolDarah: req.AsesmedGolDarah, AsesmedTd: req.AsesmedTd, AsesmedTdDetail: req.AsesmedTdDetail, AsesmedPenyJantung: req.AsesmedPenyJantung, AsesmedPenyJantungDetail: req.AsesmedPenyJantungDetail, AsesmedDiabet: req.AsesmedDiabet, AsesmedDiabetDetail: req.AsesmedDiabetDetail, AsesmedHaemop: req.AsesmedHaemop, AsesmedHaemopDetail: req.AsesmedHaemopDetail, AsesmedHepat: req.AsesmedHepat,
		AsesmedHepatDetail: req.AsesmedHepatDetail, AsesmedPenyLain: req.AsesmedPenyLain, AsesmedPenyLainDetail: req.AsesmedPenyLainDetail, AsesmedAlergiObat: req.AsesmedAlergiObat, AsesmedAlergiObatDetail: req.AsesmedAlergiObatDetail, AsesmedAlergiMknan: req.AsesmedAlergiMknan, AsesmedAlergiMknanDetail: req.AsesmedAlergiMknanDetail, AsesmedKebiasaanBuruk: req.AsesmedKebiasaanBuruk,
	}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertDataIntraOralRepositoryV2(req dto.ReqDataIntraOralV2, kdBagian string) (res soap.DataIntraOralModel, err error) {
	times := time.Now()
	errs := sr.DB.Where(&soap.DataIntraOralModel{
		Noreg: req.Noreg, KdBagian: kdBagian}).Save(&soap.DataIntraOralModel{
		InsertDttm:                   times.Format("2006-01-02 15:04:05"),
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusDetail:       req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
		Noreg:                        req.Noreg,
		KdBagian:                     kdBagian,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) UpdateDataIntraOralRepositoryV2(req dto.ReqDataIntraOralV2, kdBagian string) (res soap.DataIntraOralModel, err error) {

	errs := sr.DB.Where(&soap.DataIntraOralModel{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(&soap.DataIntraOralModel{
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusDetail:       req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertTriaseV2(noReg string, kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error) {
	times := time.Now()

	data := soap.TriaseModel{
		KdBagian: kdBagian, Noreg: noReg,
		InsertDttm: times.Format("2006-01-02 15:04:05"), IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseAkral: req.IgdTriaseAkral,
		//  IgdTriaseCahaya: req.IgdTriaseCahaya,
		IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseGangguanDetail: req.IgdTriaseAlergiDetail, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), IgdTriaseFingerUser: req.IgdTriaseFingerUser, IgdTriaseFingerJam: times.Format("15:04:05"),
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, errors.New(result.Error.Error())
	}

	return data, nil

}

func (sr *soapRepository) UpdateTriaseV2(kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error) {

	times := time.Now()

	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.NoReg}).Updates(&soap.TriaseModel{
		IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseAkral: req.IgdTriaseAkral,
		//  IgdTriaseCahaya: req.IgdTriaseCahaya,
		IgdTriaseGangguanDetail: req.IgdTriaseGangguanDetail, IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), KdBagian: kdBagian, Noreg: req.NoReg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) SaveAnamnesaRepositoryV2(req dto.RequestSaveAnamnesaV2) (err error) {
	data := soap.Anamnesa{}
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_keluh_utama=?, asesmed_jenpel=?, asesmed_rwyt_skrg=?, asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?, asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.AsesmedKeluhUtama, req.AsesmedJenpel,
		req.AsesmenRwtSkrg, req.AsesmenRwtDulu, req.AsesmenRwtObat, req.AsesmenRwtPenyKlrg, req.AsesmenRwtPenyAlergi,
		req.AsesmenRwtPenyAlergiDetail, req.AsesmenKhusPemGigi1, req.AsesmenKhusPemGigi2, req.AsesmenKhusPemGigi3,
		req.AsesmenKhusPemGigi4, req.AsesmenKhusPemGigi5, req.AsesmenKhusPemGigi5Detail, req.NoReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return errors.New(message)
	}

	return nil
}

func (sr *soapRepository) SaveSkriningRepositoryV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error) {

	var datetime = time.Now().UTC()
	dt := datetime.Format("2006-01-02 15:04:05")

	skrining := soap.DcpptSoapPasien{InsertDttm: dt, Noreg: req.NoReg, KdBagian: req.KodeBagian, SkriningK1: req.K1, SkriningK2: req.K2,
		SkriningK3: req.K3, SkriningK4: req.K4, SkriningK5: req.K5, SkriningK6: req.K6, SkriningF1: req.KF1, SkriningF2: req.KF2, SkriningF3: req.KF3,
		SkriningF4: req.KF4, SkriningB1: req.B1, SkriningB2: req.B2, SkriningRj: req.RJ, SkriningR1: req.IV1, SkriningR2: req.IV2, SkriningR3: req.IV3,
		SkriningR4: req.IV4, SkriningTgl: dt, AseskepTgl: dt,
	}

	result := sr.DB.Create(&skrining)

	if result.Error != nil {

		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error.Error())
		return skrining, errors.New(message)
	}

	return skrining, nil
}

func (sr *soapRepository) UpdateSkriningRepositoryV2(req dto.RequestSaveSkriningV2) (res soap.DcpptSoapPasien, err error) {

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET skrining_k1=?, skrining_k2=?, skrining_k3=?,   skrining_k4=? , skrining_k5=? ,
	skrining_k6=?, skrining_f1=?, skrining_f2=?, skrining_f3=?, skrining_f4=?, skrining_b1=?, skrining_b2=?, skrining_rj=?, skrining_r1=?,
	skrining_r2=?, skrining_r3=?, skrining_r4=?, skrining_user=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.K1, req.K2, req.K3, req.K4, req.K5, req.K6, req.KF1, req.KF2, req.KF3, req.KF4, req.B1, req.B2, req.RJ,
		req.IV1, req.IV2, req.IV3, req.IV4, req.User, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *soapRepository) UpdateOdontogramRepositoryV2(req dto.RequestSaveOdontogramV2) (res soap.DcpptSoapPasien, err error) {

	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =?  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.Keterangan, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) DeleteOdontogramRepositoryV2(req dto.RequestDeleteOdontogramV2) (res soap.DcpptSoapPasien, err error) {
	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =""  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, result.AddError(errors.New(message))
	}

	return res, nil
}

func (sr *soapRepository) InsertOndontogramRepositoryV2(req dto.InsertOdontogramV2) (res soap.Odontograms, err error) {
	data := soap.Odontograms{
		Number: req.Number, Keterangan: req.Keterangan,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, err
	}

	return data, nil
}

func (sr *soapRepository) UpdateAssesRawatJalanDokterRepositoryV2(req dto.RequestSaveAssementRawatJalanDokterV2) (res soap.DcpptOdontogram, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET asesmed_jenpel=?, asesmed_keluh_utama=?, asesmed_rwyt_skrg=?,
	asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?,
	asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=?, asesmed_gol_darah=?,
	asesmed_td=?, asesmed_td_detail=?, asesmed_peny_jantung=?, asesmed_peny_jantung_detail=?, asesmed_diabet=?, asesmed_diabet_detail=?, asesmed_haemop=?,
	asesmed_haemop_detail=?, asesmed_hepat=?, asesmed_hepat_detail=?, asesmed_peny_lain=?, asesmed_peny_lain_detail=?, asesmed_alergi_obat=?, asesmed_alergi_obat_detail=?, 
	asesmed_alergi_mknan=?, asesmed_alergi_mknan_detail=?, asesmed_kebiasaan_buruk=?, asesmed_khus_ok_anterior=?, asesmed_khus_ok_posterior=?, asesmed_khus_ok_molar=?, 
	asesmed_khus_palatum=?, asesmed_khus_torus_P=?, asesmed_khus_torus_M=?, asesmed_khus_torus_M_detail=?, asesmed_khus_super_teeth=?, asesmed_khus_super_teeth_detail=?, 
	asesmed_khus_diastema=?, asesmed_khus_diastema_detail=?, asesmed_khus_gigi_anomali=?, asesmed_khus_gigi_anomali_detail=?, asesmed_khus_oral_lain=?
	WHERE noreg=?;`

	result := sr.DB.Raw(query, req.Pelayanan, req.KeluhanUtama, req.AsesmedRwytSkrg, req.AsesmedRwytDulu,
		req.AssesRiwayat.AsesmedRwytObat, req.AssesRiwayat.AsesmedRwytPenyKlrg, req.AssesRiwayat.AsesmedRwytAlergi, req.AssesRiwayat.AsesmedRwytAlergiDetail,
		req.AsesmedKhusPemGigi1, req.AsesmedKhusPemGigi2, req.AsesmedKhusPemGigi3, req.AsesmedKhusPemGigi4, req.AsesmedKhusPemGigi5, req.AsesmedKhusPemGigi5Detail,
		req.AsesmedGolDarah, req.AsesmedTd, req.AsesmedTdDetail, req.AsesmedPenyJantung, req.AsesmedPenyJantungDetail, req.AsesmedDiabet, req.AsesmedDiabetDetail, req.AsesmedHaemop, req.AsesmedHaemopDetail, req.AsesmedHepat, req.AsesmedHepatDetail, req.AsesmedPenyLain, req.AsesmedPenyLainDetail, req.AsesmedAlergiObat, req.AsesmedAlergiObatDetail,
		req.AsesmedAlergiMknan, req.AsesmedAlergiMknanDetail, req.AsesmedKebiasaanBuruk, req.AsesmedKhusOkAnterior, req.AsesmedKhusOkPosterior, req.AsesmedKhusOkMolar, req.AsesmedKhusPalatum,
		req.AsesmedKhusTorusP, req.AsesmedKhusTorusM, req.AsesmedKhusTorusMDetail, req.AsesmedKhusSuperTeeth, req.AsesmedKhusSuperTeethDetail,
		req.AsesmedKhusDiastema, req.AsesmedDiabetDetail, req.AsesmedKhusGigiAnomali, req.AsesmedKhusGigiAnomaliDetail, req.AsesmedKhusOralLain,
		req.NoReg).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) SaveAssesKebEdukasiRepositoryV2(req dto.ReqKebEdukasiV2) (res soap.KebEdukasi, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesedu_ke1=? , asesedu_ke2=?,
				asesedu_ke3=?, asesedu_ke4=?, asesedu_ke5=?, asesedu_ke6=?, asesedu_ke7=?, asesedu_ke8=?,
				asesedu_ke9=?, asesedu_ke10=?, asesedu_ke11=?, asesedu_ke11_detail=?, asesedu_tu1=?, asesedu_tu2=?,
				asesedu_kb1=?, asesedu_kb2=?, asesedu_kb3=?, asesedu_mb1=?,asesedu_mb2=?, asesedu_mb3=?, asesedu_np1=?,
				asesedu_np2=?, asesedu_np3=?, asesedu_np4=?, asesedu_np5=?, asesedu_np6=?, asesedu_np6_detail=?,
				asesedu_ha1=?, asesedu_ha2=?, asesedu_ha3=?, asesedu_ha4=?, asesedu_ha5=?, asesedu_ha6=?, asesedu_ha7=?,
				asesedu_ha8=?, asesedu_ha9=?, asesedu_ha9_detail=?, asesedu_in1=?, asesedu_in2=?, asesedu_in3=?, asesedu_in4=?,
				asesedu_in5=?, asesedu_hasil1=?, asesedu_hasil2=?, asesedu_rencana_edukasi=?, asesedu_tgl=?, asesedu_jam=?, asesedu_user=? 
				WHERE noreg=?;`

	result := sr.DB.Raw(query, req.Edukasi.KebEdu1, req.Edukasi.KebEdu2, req.Edukasi.KebEdu3, req.Edukasi.KebEdu4, req.Edukasi.KebEdu5,
		req.Edukasi.KebEdu6, req.Edukasi.KebEdu7, req.Edukasi.KebEdu8, req.Edukasi.KebEdu9, req.Edukasi.KebEdu10, req.Edukasi.KebEdu11, req.Edukasi.KebEdu11Detail, req.Tujuan.Tujuan1, req.Tujuan.Tujuan2, req.KempBelajar.KempBelajar1, req.KempBelajar.KempBelajar2, req.KempBelajar.KempBelajar3,
		req.MotivasiBelajar.MotivasiBel1, req.MotivasiBelajar.MotivasiBel2, req.MotivasiBelajar.MotivasiBel3, req.NilaiPasien.NilaiPasien1, req.NilaiPasien.NilaiPasien2, req.NilaiPasien.NilaiPasien3, req.NilaiPasien.NilaiPasien4, req.NilaiPasien.NilaiPasien5, req.NilaiPasien.NilaiPasien6, req.NilaiPasien.NilaiPasien6Detail, req.Hambatan.Hambatan1, req.Hambatan.Hambatan2, req.Hambatan.Hambatan3, req.Hambatan.Hambatan4, req.Hambatan.Hambatan5,
		req.Hambatan.Hambatan6, req.Hambatan.Hambatan7, req.Hambatan.Hambatan8, req.Hambatan.Hambatan9, req.Hambatan.Hambatan9Detail,
		req.IntervensiRes.Intervensi1, req.IntervensiRes.Intervensi2, req.IntervensiRes.Intervensi3, req.IntervensiRes.Intervensi4, req.IntervensiRes.Intervensi5,
		req.Hasil.Hasil1, req.Hasil.Hasil2, req.RencanaEdukasi, req.AsseEduTgl, req.AsseEduJam, req.AsseEduUser, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

// Save Pasca Operasi
func (sr *soapRepository) InsertPascaOperasiRepositoryV2(req dto.ReqPascaOperasiV2, kdBagian string) (res soap.PascaOperasiModel, err error) {
	errs := sr.DB.Where(&soap.PascaOperasiModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
	}).Save(&soap.PascaOperasiModel{
		ObsanslokKhusPostKeadUmum:   req.ObsanslokKhusPostKeadUmum,
		ObsanslokKhusPostKeluhUtama: req.ObsanslokKhusPostKeluhUtama, ObsanslokKhusPostTd: req.ObsanslokKhusPostTd,
		ObsanslokKhusPostNadi: req.ObsanslokKhusPostNadi, ObsanslokKhusPostSuhu: req.ObsanslokKhusPostSuhu,
		ObsanslokKhusPostRr: req.ObsanslokKhusPostRr,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

// UPDATE PASCA OPERASI
func (sr *soapRepository) SavePascaOperasiRepositoryV2(req dto.ReqPascaOperasiV2, kdBagian string) (res soap.PascaOperasiModel, err error) {
	errs := sr.DB.Select("obsanslok_khus_post_kead_umum", "obsanslok_khus_post_keluh_utama", "obsanslok_khus_post_td", "obsanslok_khus_post_nadi", "obsanslok_khus_post_suhu", "obsanslok_khus_post_rr", "kd_bagian", "noreg").Where(&soap.PascaOperasiModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
	}).Updates(&soap.PascaOperasiModel{
		ObsanslokKhusPostKeadUmum:   req.ObsanslokKhusPostKeadUmum,
		ObsanslokKhusPostKeluhUtama: req.ObsanslokKhusPostKeluhUtama, ObsanslokKhusPostTd: req.ObsanslokKhusPostTd,
		ObsanslokKhusPostNadi: req.ObsanslokKhusPostNadi, ObsanslokKhusPostSuhu: req.ObsanslokKhusPostSuhu,
		ObsanslokKhusPostRr: req.ObsanslokKhusPostRr,
	}).Scan(&res).Error
	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) UpdateTriasRepositoryV2(kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error) {

	times := time.Now()

	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.NoReg}).Updates(&soap.TriaseModel{
		IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseAkral: req.IgdTriaseAkral,
		//  IgdTriaseCahaya: req.IgdTriaseCahaya,
		IgdTriaseGangguanDetail: req.IgdTriaseGangguanDetail, IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), KdBagian: kdBagian, Noreg: req.NoReg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) InsertTriaseRepositoryV2(noReg string, kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, err error) {
	times := time.Now()

	data := soap.TriaseModel{
		KdBagian: kdBagian, Noreg: noReg,
		InsertDttm: times.Format("2006-01-02 15:04:05"), IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseAkral: req.IgdTriaseAkral,
		//  IgdTriaseCahaya: req.IgdTriaseCahaya,
		IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseGangguanDetail: req.IgdTriaseAlergiDetail, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), IgdTriaseFingerUser: req.IgdTriaseFingerUser, IgdTriaseFingerJam: times.Format("15:04:05"),
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, errors.New(result.Error.Error())
	}

	return data, nil
}

func (sr *soapRepository) InsertPemeriksaanFisikRepositoryV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, err error) {
	times := time.Now()

	data := soap.PemeriksaanFisikModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		AsesmedPemfisKepala: req.AsesmedPemfisKepala, AsesmedPemfisKepalaDetail: req.AsesmedPemfisKepalaDetail, AsesmedPemfisLeher: req.AsesmedPemfisLeher, AsesmedPemfisLeherDetail: req.AsesmedPemfisLeherDetail, AsesmedPemfisGenetalia: req.AsesmedPemfisGenetalia, AsesmedPemfisGenetaliaDetail: req.AsesmedPemfisGenetaliaDetail, AsesmedPemfisAbdomen: req.AsesmedPemfisAbdomen, AsesmedPemfisAbdomenDetail: req.AsesmedPemfisAbdomenDetail, AsesmedPemfisEkstremitas: req.AsesmedPemfisEkstremitas, AsesmedPemfisEkstremitasDetail: req.AsesmedPemfisEkstremitasDetail, AsesmedPemfisDada: req.AsesmedPemfisDada, AsesmedPemfisDadaDetail: req.AsesmedPemfisDadaDetail, AsesmedPemfisPunggung: req.AsesmedPemfisPunggung, AsesmedPemfisPunggungDetail: req.AsesmedPemfisPunggungDetail, AsesmedPemfisLain: req.AsesmedPemfisLain,

		// PENAMBAHAN INPUT DATA
		KeteranganPerson: req.KeteranganPerson, InsertUserId: req.InsertUserId, InsertPc: req.DeviceID,
		Pelayanan: req.Pelayanan,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) UpdatePemeriksaanFisikRepositoryV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, err error) {
	times := time.Now()

	if req.AsesmedPemfisLain == "" {
		errs := sr.DB.Where(&soap.PemeriksaanFisikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.PemeriksaanFisikModel{InsertDttm: times.Format("2006-01-02 15:04:05"), AsesmedPemfisKepala: req.AsesmedPemfisKepala, AsesmedPemfisKepalaDetail: req.AsesmedPemfisKepalaDetail, AsesmedPemfisLeher: req.AsesmedPemfisLeher, AsesmedPemfisLeherDetail: req.AsesmedPemfisLeherDetail, AsesmedPemfisGenetalia: req.AsesmedPemfisGenetalia, AsesmedPemfisGenetaliaDetail: req.AsesmedPemfisGenetaliaDetail, AsesmedPemfisAbdomen: req.AsesmedPemfisAbdomen, AsesmedPemfisAbdomenDetail: req.AsesmedPemfisAbdomenDetail, AsesmedPemfisEkstremitas: req.AsesmedPemfisEkstremitas, AsesmedPemfisEkstremitasDetail: req.AsesmedPemfisEkstremitasDetail, AsesmedPemfisDada: req.AsesmedPemfisDada, AsesmedPemfisDadaDetail: req.AsesmedPemfisDadaDetail, AsesmedPemfisPunggung: req.AsesmedPemfisPunggung,
			AsesmedPemfisPunggungDetail: req.AsesmedPemfisPunggungDetail, AsesmedPemfisLain: " ",
		}).Scan(&res).Error

		if errs != nil {
			return res, errors.New(errs.Error())
		}

		return res, nil
	} else {
		errs := sr.DB.Where(&soap.PemeriksaanFisikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.PemeriksaanFisikModel{InsertDttm: times.Format("2006-01-02 15:04:05"), AsesmedPemfisKepala: req.AsesmedPemfisKepala, AsesmedPemfisKepalaDetail: req.AsesmedPemfisKepalaDetail, AsesmedPemfisLeher: req.AsesmedPemfisLeher, AsesmedPemfisLeherDetail: req.AsesmedPemfisLeherDetail, AsesmedPemfisGenetalia: req.AsesmedPemfisGenetalia, AsesmedPemfisGenetaliaDetail: req.AsesmedPemfisGenetaliaDetail, AsesmedPemfisAbdomen: req.AsesmedPemfisAbdomen, AsesmedPemfisAbdomenDetail: req.AsesmedPemfisAbdomenDetail, AsesmedPemfisEkstremitas: req.AsesmedPemfisEkstremitas, AsesmedPemfisEkstremitasDetail: req.AsesmedPemfisEkstremitasDetail, AsesmedPemfisDada: req.AsesmedPemfisDada, AsesmedPemfisDadaDetail: req.AsesmedPemfisDadaDetail, AsesmedPemfisPunggung: req.AsesmedPemfisPunggung,
			AsesmedPemfisPunggungDetail: req.AsesmedPemfisPunggungDetail, AsesmedPemfisLain: req.AsesmedPemfisLain,
		}).Scan(&res).Error

		if errs != nil {
			return res, errors.New(errs.Error())
		}

		return res, nil
	}

}

// INSERT RENCANA TINDAK LAJUT REPOSITORY
func (sr *soapRepository) InsertRencanaTindakLanjutRepositoryV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string, tglMasuk string, jamMasuk string, KdDpjp string) (res soap.RencanaTindakLanjutModel, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.RencanaTindakLanjutModel{Noreg: req.NoReg, KdBagian: kdBagian}).Save(soap.RencanaTindakLanjutModel{
		AsesmedDokterTtd: req.InsertUserId, AsesmedDokterTtdTgl: times.Format("2006-01-02"), AsesmedDokterTtdJam: times.Format("15:04:05"),
		KdBagian: kdBagian, Noreg: req.NoReg, AsesmedTerapi: req.RencanaAnjuranTerapi, AsesmedAlasanOpname: req.AlasanOpname, AsesmedKonsulKe: req.KonsulKe, InsertDttm: times.Format("2006-01-02 15:04:05"), KeteranganPerson: req.KeteranganPerson, InsertUserId: req.InsertUserId, TglMasuk: tglMasuk, JamMasuk: jamMasuk, KdDpjp: KdDpjp, AsesmedKonsulKeAlasan: req.AlasanKonsul, InsertPc: req.DeviceID, Pelayanan: req.Pelayanan, AsesmenPrognosis: req.Prognosis,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateRencanaTindakLanjutRepositoryV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string) (res soap.RencanaTindakLanjutModel, err error) {

	errs := sr.DB.Where(&soap.RencanaTindakLanjutModel{Noreg: req.NoReg, KdBagian: kdBagian}).Updates(soap.RencanaTindakLanjutModel{
		AsesmedKonsulKeAlasan: req.AlasanKonsul, KdBagian: kdBagian, Noreg: req.NoReg, AsesmedTerapi: req.RencanaAnjuranTerapi,
		AsesmenPrognosis: req.Prognosis, AsesmedAlasanOpname: req.AlasanOpname, AsesmedKonsulKe: req.KonsulKe}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, err error) {
	times := time.Now()

	data := soap.AsesmedKeperawatanBidan{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: req.Noreg, AseskepPerolehanInfo: req.AseskepPerolehanInfo, AseskepCaraMasuk: req.AseskepAsalMasuk, AseskepCaraMasukDetail: req.AseskepAsalMasukDetail, AseskepAsalMasuk: req.AseskepAsalMasuk, AseskepAsalMasukDetail: req.AseskepAsalMasukDetail, AseskepBb: req.AseskepBb,
		AseskepTb: req.AseskepTb, AseskepRwytPnykt: req.AseskepRwytPnykt, AseskepRwytObatDetail: req.AseskepRwytObatDetail, AseskepAsesFungsional: req.AseskepAsesFungsional, AseskepRj1: req.AseskepRj1, AseskepRj2: req.AseskepRj2, AseskepHslKajiRj: req.AseskepHslKajiRj, AseskepHslKajiRjTind: req.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.AseskepLamaNyeri, AseskepNyeriMenjalar: req.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.AseskepKualitasNyeri, AseskepNyeriPemicu: req.AseskepNyeriPemicu, AseskepNyeriPengurang: req.AseskepNyeriPengurang, AseskepKehamilan: req.AseskepKehamilan, AseskepKehamilanGravida: req.AseskepKehamilanGravida, AseskepKehamilanPara: req.AseskepKehamilanPara, AseskepKehamilanAbortus: req.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.AseskepKehamilanHpht, AseskepKehamilanTtp: req.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.AseskepKehamilanDjj, AseskepKehamilanVt: req.AseskepKehamilanVt, AseskepDekubitus1: req.AseskepDekubitus1, AseskepDekubitus2: req.AseskepDekubitus2, AseskepDekubitus3: req.AseskepDekubitus3, AseskepDekubitus4: req.AseskepDekubitus4, AseskepDekubitusAnak: req.AseskepDekubitusAnak, AseskepPulangKondisi: req.AseskepPulangKondisi, AseskepPulangTransportasi: req.AseskepPulangTransportasi, AseskepPulangPendidikan: req.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.AseskepPulangPendidikanDetail,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, err error) {

	errs := sr.DB.Where(soap.AsesmedKeperawatanBidan{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(
		&soap.AsesmedKeperawatanBidan{
			KdBagian: kdBagian, Noreg: req.Noreg, AseskepPerolehanInfo: req.AseskepPerolehanInfo, AseskepCaraMasuk: req.AseskepCaraMasuk, AseskepCaraMasukDetail: req.AseskepAsalMasukDetail, AseskepAsalMasuk: req.AseskepAsalMasuk, AseskepAsalMasukDetail: req.AseskepAsalMasukDetail, AseskepBb: req.AseskepBb, AseskepTb: req.AseskepTb, AseskepRwytPnykt: req.AseskepRwytPnykt, AseskepRwytObatDetail: req.AseskepRwytObatDetail, AseskepAsesFungsional: req.AseskepAsesFungsional, AseskepRj1: req.AseskepRj1, AseskepRj2: req.AseskepRj2, AseskepHslKajiRj: req.AseskepHslKajiRj, AseskepHslKajiRjTind: req.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.AseskepLamaNyeri, AseskepNyeriMenjalar: req.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.AseskepKualitasNyeri, AseskepNyeriPemicu: req.AseskepNyeriPemicu, AseskepNyeriPengurang: req.AseskepNyeriPengurang, AseskepKehamilan: req.AseskepKehamilan, AseskepKehamilanGravida: req.AseskepKehamilanGravida, AseskepKehamilanPara: req.AseskepKehamilanPara, AseskepKehamilanAbortus: req.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.AseskepKehamilanHpht, AseskepKehamilanTtp: req.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.AseskepKehamilanDjj, AseskepKehamilanVt: req.AseskepKehamilanVt, AseskepDekubitus1: req.AseskepDekubitus1, AseskepDekubitus2: req.AseskepDekubitus2, AseskepDekubitus3: req.AseskepDekubitus3, AseskepDekubitus4: req.AseskepDekubitus4, AseskepDekubitusAnak: req.AseskepDekubitusAnak, AseskepPulangKondisi: req.AseskepPulangKondisi, AseskepPulangTransportasi: req.AseskepPulangTransportasi, AseskepPulangPendidikan: req.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.AseskepPulangPendidikanDetail,
		}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) OnGetAsesmenIGDRepository(kdBagian string, noReg string) (res soap.AsesemenIGD, err error) {
	results := sr.DB.Where(&soap.AsesemenIGD{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

// GET INFORMASI DAN KELUHAN PADA TABLE SOAP PASIEN
func (sr *soapRepository) GetDcpptSoapPasien(kdBagian string, noReg string) (res soap.DcpptSoapPasienModel, err error) {
	results := sr.DB.Where(&soap.DcpptSoapPasienModel{KdBagian: kdBagian, Noreg: noReg}).First(&res)

	if results.Error != nil {
		sr.Logging.Info("DATA SOAP PASIEN TIDAK DITEMUKAN")
		return res, results.Error
	}

	return res, nil
}

func (sr *soapRepository) GetRiwayatPenyakitSebelumnya(noRm string) (res []soap.RiwayatPenyakit, err error) {

	query := `select * from vicore_rme.view_riwayat_penyakit WHERE no_rm=?;`
	result := sr.DB.Raw(query, noRm).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, result.AddError(errors.New(message))
	}

	return res, nil
}

func (sr *soapRepository) GetDccptSoapDokterRepository(kdBagian string, noReg string) (res soap.DcpptSoapDokter, err error) {

	results := sr.DB.Where(&soap.DcpptSoapDokter{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		sr.Logging.Info("DATA SOAP PASIEN TIDAK DITEMUKAN")
		return res, results.Error
	}

	sr.Logging.Info("DATA SOAP PASIEN DITEMUKAN")
	return res, nil
}

func (sr *soapRepository) GetDcpptSoapPasienRepository(kdBagian string, noReg string, userID string) (res soap.DcpptSoapPasienModel, err error) {

	results := sr.DB.Where(&soap.DcpptSoapPasienModel{KdBagian: kdBagian, Noreg: noReg, InsertUserId: userID}).Find(&res)

	if results.Error != nil {
		sr.Logging.Info("DATA SOAP PASIEN TIDAK DITEMUKAN")
		return res, results.Error
	}

	sr.Logging.Info("DATA SOAP PASIEN DITEMUKAN")
	return res, nil
}

func (sr *soapRepository) GetDccptSoapDokterAsesmenRepository(kdBagian string, userID string, noReg string) (res soap.DcpptSoapDokter, err error) {

	results := sr.DB.Where(&soap.DcpptSoapDokter{KdBagian: kdBagian, InsertUserId: userID, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		sr.Logging.Info("DATA SOAP PASIEN TIDAK DITEMUKAN")
		return res, results.Error
	}

	sr.Logging.Info("DATA SOAP PASIEN DITEMUKAN")
	return res, nil
}

func (sr *soapRepository) GetDccptSoapDokterAsesmenForPerawatRepository(kdBagian string, noReg string) (res soap.DcpptSoapDokter, err error) {

	results := sr.DB.Where(&soap.DcpptSoapDokter{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		sr.Logging.Info("DATA SOAP PASIEN TIDAK DITEMUKAN")
		return res, results.Error
	}

	sr.Logging.Info("DATA SOAP PASIEN DITEMUKAN")
	return res, nil
}

func (sr *soapRepository) GetTindakLanjutRepository(kdBagian string, noReg string) (res soap.TindakLanjutModel, err error) {
	results := sr.DB.Where(&soap.TindakLanjutModel{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}
	return res, nil
}

// ========================================= GET SKRINING RESIKO DIKUBITUS  =============== //
func (sr *soapRepository) GetPemeriksaanFisikRepository(kdBagian string, noReg string) (res rme.PemeriksaanFisikModel, err error) {
	results := sr.DB.Where(&soap.TindakLanjutModel{KdBagian: kdBagian, Noreg: noReg}).Find(&res)

	if results.Error != nil {
		return res, results.Error
	}

	return res, nil
}

func (sr *soapRepository) OnGetReportPemeriksaanFisikDokterAntonio(kdBagian string, noReg string) (res rme.PemeriksaanFisikDokterAntonio, err error) {
	result := sr.DB.Where(&rme.PemeriksaanFisikDokterAntonio{
		KdBagian:  kdBagian,
		Noreg:     noReg,
		KetPerson: "Dokter",
	}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *soapRepository) OnDeleteDoubleCheckRepository(idDoubleCheck int) (res soap.DDoubleCheck, err error) {
	sr.Logging.Info("ID DOUBLE CHECK")
	sr.Logging.Info(idDoubleCheck)
	result := sr.DB.Where("id_double_check=?", idDoubleCheck).Delete(&soap.DDoubleCheck{})

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}
