package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
)

func (sr *soapRepository) GetTandaVitalIGDRepository(userID string, kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where insert_user_id=? AND kd_bagian=? AND ket_person=? AND pelayanan=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, pelayanan, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetTandaVitalIGDPerawatRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND pelayanan=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, pelayanan, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) InsertTandaVitalIGDRepository(data soap.DVitalSignIGDDokter) (res soap.DVitalSignIGDDokter, err error) {
	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) GetPemFisikIGDDokterRepository(userID string, kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, akral, pupil, kesadaran  FROM vicore_rme.dpem_fisik where insert_user_id=? AND kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
func (sr *soapRepository) GetPemFisikIGDPerawatRepository(kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, akral, pupil, kesadaran  FROM vicore_rme.dpem_fisik where kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) UpdatePemFisikIGDDokterRespository(kdBagian string, req dto.ReqSaveTandaVitalIGDDokter) (res soap.DPemFisikIGDDokterRepository, err error) {
	errs := sr.DB.Where(&soap.DPemFisikIGDDokterRepository{
		KdBagian: kdBagian, KetPerson: req.Person, Noreg: req.Noreg,
	}).Updates(soap.DPemFisikIGDDokterRepository{
		GcsE:      req.GCSE,
		GcsV:      req.GCSV,
		GcsM:      req.GCSM,
		Akral:     req.Akral,
		Pupil:     req.Pupil,
		Kesadaran: req.Kesadaran,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateTandaVitalIGDDokterRespository(userID string, kdBagian string, req dto.ReqSaveTandaVitalIGDDokter) (res soap.DVitalSignIGDDokter, err error) {
	errs := sr.DB.Where(&soap.DVitalSignIGDDokter{
		InsertUserId: userID, KdBagian: kdBagian, KetPerson: req.Person, Noreg: req.Noreg,
	}).Updates(soap.DVitalSignIGDDokter{
		Td:         req.Td,
		Suhu:       req.Suhu,
		Nadi:       req.Nadi,
		Pernafasan: req.Pernafasan,
		Tb:         req.Tb,
		Bb:         req.Bb,
		Spo2:       req.Spo2,
	}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertPemFisikIGDDokterIGDRepository(data soap.DPemFisikIGDDokterRepository) (res soap.DPemFisikIGDDokterRepository, err error) {
	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) GetRencanaTindakLanjutRepository(noReg string, kdBagian string, kdDpjp string) (res soap.RencanaTindakLanjutModel, err error) {

	errs := sr.DB.Select("asesmed_terapi", "asesmed_alasan_opname", "asesmed_konsul_ke", "kd_bagian", "noreg", "asesmen_prognosis").Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: noReg, KdDpjp: kdDpjp}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetResikoJatuhGoUpGoTestRepository(noReg string, kdBagian string) (res soap.ResikoJatuhGetUpGoTest, err error) {

	errs := sr.DB.Select("insert_dttm", "upd_dttm", "keterangan_person", "insert_user_id", "insert_pc", "tgl_masuk", "kd_bagian", "pelayanan", "aseskep_rj1", "aseskep_rj2", "aseskep_hsl_kaji_rj", "aseskep_hsl_kaji_rj_tind").Where(&soap.ResikoJatuhGetUpGoTest{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetRencanaTindakLanjutReportRepository(noReg string) (res soap.RencanaTindakLanjutModel, err error) {

	errs := sr.DB.Select("asesmed_terapi", "asesmed_alasan_opname", "asesmed_konsul_ke", "kd_bagian", "noreg", "asesmen_prognosis").Where(&soap.AsesmedAnamnesa{KdBagian: "IGD001", Noreg: noReg}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetDiagnosaRepositoryReportIGD(noreg string) (res []soap.DiagnosaResponse, err error) {
	var diag []soap.DiagnosaResponse

	type Diagnoses struct {
		P    string `json:"p"`
		Desp string `json:"desp"`
		S1   string `json:"s1"`
		Des1 string `json:"des1"`
		S2   string `json:"s2"`
		Des2 string `json:"des2"`
		S3   string `json:"s3"`
		Des3 string `json:"des3"`
		S4   string `json:"s4"`
		Des4 string `json:"des4"`
		S5   string `json:"s5"`
		Des5 string `json:"des5"`
		S6   string `json:"s6"`
		Des6 string `json:"des6"`
	}

	data := Diagnoses{}

	query := `
			SELECT asesmed_diagP AS p, b.description as desp, 
			asesmed_diagS1 AS s1, b1.description as des1, 
			asesmed_diagS2 AS s2, b2.description as des2,
			asesmed_diagS3 AS s3, b3.description as des3,
			asesmed_diagS4 AS s4, b4.description as des4,
			asesmed_diagS5 AS s5, b5.description as des5,
			asesmed_diagS6 AS s6, b6.description as des6
			FROM vicore_rme.dcppt_soap_dokter AS a 
			LEFT JOIN vicore_lib.k_icd10 AS b ON a.asesmed_diagP=b.code2 
			LEFT JOIN vicore_lib.k_icd10 AS b1 ON a.asesmed_diagS1=b1.code2
			LEFT JOIN vicore_lib.k_icd10 AS b2 ON a.asesmed_diagS2=b2.code2
			LEFT JOIN vicore_lib.k_icd10 AS b3 ON a.asesmed_diagS3=b3.code2
			LEFT JOIN vicore_lib.k_icd10 AS b4 ON a.asesmed_diagS4=b4.code2
			LEFT JOIN vicore_lib.k_icd10 AS b5 ON a.asesmed_diagS5=b5.code2
			LEFT JOIN vicore_lib.k_icd10 AS b6 ON a.asesmed_diagS6=b6.code2
			WHERE a.noreg=? AND a.kd_bagian=? LIMIT 1;
		`

	result := sr.DB.Raw(query, noreg, "IGD001").Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	if len(data.P) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.P,
			Description: data.Desp,
			Type:        "primer",
			Table:       "P",
		})
	}

	if len(data.S1) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S1,
			Description: data.Des1,
			Type:        "sekunder",
			Table:       "S1",
		})
	}

	if len(data.S2) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S2,
			Description: data.Des2,
			Type:        "sekunder",
			Table:       "S2",
		})
	}

	if len(data.S3) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S3,
			Description: data.Des3,
			Type:        "sekunder",
			Table:       "S3",
		})
	}
	if len(data.S4) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S4,
			Description: data.Des4,
			Type:        "sekunder",
			Table:       "S4",
		})
	}

	if len(data.Des5) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S5,
			Description: data.Des5,
			Type:        "sekunder",
			Table:       "S5",
		})
	}

	if len(data.S6) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S6,
			Description: data.Des6,
			Type:        "sekunder",
			Table:       "S6",
		})
	}

	sr.Logging.Info("GET DATA DIAGNOSA")
	sr.Logging.Info(diag)

	return diag, nil
}

func (sr *soapRepository) GetDiagnosaRepositoryReportBangsal(noreg string, bagian string) (res []soap.DiagnosaResponse, err error) {
	var diag []soap.DiagnosaResponse

	type Diagnoses struct {
		P    string `json:"p"`
		Desp string `json:"desp"`
		S1   string `json:"s1"`
		Des1 string `json:"des1"`
		S2   string `json:"s2"`
		Des2 string `json:"des2"`
		S3   string `json:"s3"`
		Des3 string `json:"des3"`
		S4   string `json:"s4"`
		Des4 string `json:"des4"`
		S5   string `json:"s5"`
		Des5 string `json:"des5"`
		S6   string `json:"s6"`
		Des6 string `json:"des6"`
	}

	data := Diagnoses{}

	query := `
			SELECT asesmed_diagP AS p, b.description as desp, 
			asesmed_diagS1 AS s1, b1.description as des1, 
			asesmed_diagS2 AS s2, b2.description as des2,
			asesmed_diagS3 AS s3, b3.description as des3,
			asesmed_diagS4 AS s4, b4.description as des4,
			asesmed_diagS5 AS s5, b5.description as des5,
			asesmed_diagS6 AS s6, b6.description as des6
			FROM vicore_rme.dcppt_soap_dokter AS a 
			LEFT JOIN vicore_lib.k_icd10 AS b ON a.asesmed_diagP=b.code2 
			LEFT JOIN vicore_lib.k_icd10 AS b1 ON a.asesmed_diagS1=b1.code2
			LEFT JOIN vicore_lib.k_icd10 AS b2 ON a.asesmed_diagS2=b2.code2
			LEFT JOIN vicore_lib.k_icd10 AS b3 ON a.asesmed_diagS3=b3.code2
			LEFT JOIN vicore_lib.k_icd10 AS b4 ON a.asesmed_diagS4=b4.code2
			LEFT JOIN vicore_lib.k_icd10 AS b5 ON a.asesmed_diagS5=b5.code2
			LEFT JOIN vicore_lib.k_icd10 AS b6 ON a.asesmed_diagS6=b6.code2
			WHERE a.noreg=? AND a.kd_bagian=? AND a.pelayanan=? LIMIT 1;
		`

	result := sr.DB.Raw(query, noreg, bagian, "ranap").Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	if len(data.P) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.P,
			Description: data.Desp,
			Type:        "primer",
			Table:       "P",
		})
	}

	if len(data.S1) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S1,
			Description: data.Des1,
			Type:        "sekunder",
			Table:       "S1",
		})
	}

	if len(data.S2) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S2,
			Description: data.Des2,
			Type:        "sekunder",
			Table:       "S2",
		})
	}

	if len(data.S3) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S3,
			Description: data.Des3,
			Type:        "sekunder",
			Table:       "S3",
		})
	}
	if len(data.S4) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S4,
			Description: data.Des4,
			Type:        "sekunder",
			Table:       "S4",
		})
	}

	if len(data.Des5) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S5,
			Description: data.Des5,
			Type:        "sekunder",
			Table:       "S5",
		})
	}

	if len(data.S6) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S6,
			Description: data.Des6,
			Type:        "sekunder",
			Table:       "S6",
		})
	}

	sr.Logging.Info("GET DATA DIAGNOSA")
	sr.Logging.Info(diag)

	return diag, nil
}

func (sr *soapRepository) GetTandaVitalIGDReportdDokterRepository(noreg string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, td, nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, "IGD001", "Dokter", noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetTandaVitalBangsalRepository(noreg string, kdBagian string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, td, nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, kdBagian, "Dokter", noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetTandaVitalReportIGDDokterRepository(kdBagian string, person string, noreg string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data di dapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetReportPemFisikIGDDokterRepository(kdBagian string, person string, noreg string) (res soap.DPemFisikIGDDokterRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, kesadaran  FROM vicore_rme.dpem_fisik where  kd_bagian=? AND ket_person=? AND noreg=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data di dapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetTandaVitalReportReportDokterRepository(kdBagian string, person string, noreg string, pelayanan string) (res soap.DVitalSignIGDDokter, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, tb, td, bb,  nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where kd_bagian=? AND ket_person=? AND noreg=? AND pelayanan=? LIMIT 1`

	result := sr.DB.Raw(query, kdBagian, person, noreg, pelayanan).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data di dapat", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
