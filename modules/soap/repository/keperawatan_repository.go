package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
)

func (sr *soapRepository) OnGetPengkajianKeperawatanDewasaRepository(noReg string, kdBagian string, userID string) (res soap.PengkajianAwalKeperawatan, err error) {
	errs := sr.DB.Where(&soap.PengkajianAwalKeperawatan{Noreg: noReg, KdBagian: kdBagian, KeteranganPerson: "Dewasa"}).First(&res).Error

	if errs != nil {
		sr.Logging.Info(errs)
		return res, errors.New(errs.Error())
	}

	return res, nil
}

// ==============
func (sr *soapRepository) OnGetReportPengkajianKeperawatanDewasaRepository(noReg string, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error) {
	errs := sr.DB.Where(&soap.PengkajianAwalKeperawatan{Noreg: noReg, KdBagian: kdBagian, KeteranganPerson: "Dewasa"}).First(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) OnGetPengkajianAnakRepository(noReg string, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error) {

	errs := sr.DB.Where(&soap.PengkajianAwalKeperawatan{Noreg: noReg, KdBagian: kdBagian, KeteranganPerson: "Anak"}).Find(&res).Error

	if errs != nil {
		sr.Logging.Info(errs)
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) OnSavePengkajianKeperawatanRepository(data soap.PengkajianAwalKeperawatan) (res soap.PengkajianAwalKeperawatan, err error) {

	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

// OnSavePengkajianKeperawatanRepository
func (sr *soapRepository) OnUpdatePengkajianKeperawatanRepository(req dto.ReqSavePengkajianKeperawatan, kdBagian string, person string) (res soap.PengkajianAwalKeperawatan, err error) {

	result := sr.DB.Model(&soap.PengkajianAwalKeperawatan{}).Where("noreg = ? AND kd_bagian=? AND keterangan_person=?", req.Noreg, kdBagian, person).Updates(&soap.PengkajianAwalKeperawatan{
		AseskepPerolehanInfo:       req.AseskepJenpel,
		AseskepPerolehanInfoDetail: req.AseskepJenpelDetail,
		AseskepReaksiAlergi:        req.AseskepReaksiAlergi,
		AseskepKel:                 req.AseskepKel,
		AseskepRwytPnykt:           req.AseskepRwytPnykt,
		AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil

}

// OnSavePengkajianKeperawatanRepository
func (sr *soapRepository) OnUpdatePengkajianKeperawatanDewasaRepository(req dto.ReqSavePengkajianKeperawatan, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error) {

	result := sr.DB.Model(&soap.PengkajianAwalKeperawatan{}).Where("noreg = ? AND kd_bagian=? AND keterangan_person=?", req.Noreg, kdBagian, "Dewasa").Updates(&soap.PengkajianAwalKeperawatan{
		AseskepPerolehanInfo:       req.AseskepJenpel,
		AseskepPerolehanInfoDetail: req.AseskepJenpelDetail,
		AseskepReaksiAlergi:        req.AseskepReaksiAlergi,
		AseskepKel:                 req.AseskepKel,
		AseskepRwytPnykt:           req.AseskepRwytPnykt,
		AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil

}

func (sr *soapRepository) OnUpdatePengkajianAnakRepository(req dto.ReqSavePengkajianAnak, kdBagian string) (res soap.PengkajianAwalKeperawatan, err error) {

	result := sr.DB.Model(&soap.PengkajianAwalKeperawatan{}).Where("noreg = ? AND kd_bagian=? AND pelayanan=?", req.Noreg, kdBagian, req.Pelayanan).Updates(&soap.PengkajianAwalKeperawatan{
		AseskepPerolehanInfo:       req.AseskepJenpel,
		AseskepPerolehanInfoDetail: req.AseskepJenpelDetail,
		AseskepReaksiAlergi:        req.AseskepReaksiAlergi,
		AseskepKel:                 req.AseskepKel,
		AseskepRwytPnykt:           req.AseskepRwytPnykt,
		AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
		AseskepRwtImunisasi:        req.RwtImunisasi,
		AseskepRwtKelahiran:        req.RwtKelahiran,
	}).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil

}

func (sr *soapRepository) GetVitalSignKeperawatanBangsalRepository(userID string, kdBagian string, person string, noreg string) (res soap.DVitalSignKeperawatanBangsal, err error) {
	query := `SELECT insert_dttm, upd_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, noreg, td, tb, bb, nadi, suhu, spo2, pernafasan FROM vicore_rme.dvital_sign where insert_user_id=? AND kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) InsertVitalSignKeperawatanBangsalRepository(data soap.DVitalSignKeperawatanBangsal) (res soap.DVitalSignKeperawatanBangsal, err error) {
	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) InsertPemFisikKeperawatanBangsalRepository(data soap.DPemFisikKeperawatanBangsalRepository) (res soap.DPemFisikKeperawatanBangsalRepository, err error) {
	result := sr.DB.Create(&data).Scan(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil

}

// UpdatePemFisikIGDDokterRespository
func (sr *soapRepository) UpdatePemFisikKeperawatanBangsalRespository(userID string, kdBagian string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res soap.DPemFisikKeperawatanBangsalRepository, err error) {
	errs := sr.DB.Where(&soap.DPemFisikIGDDokterRepository{
		InsertUserId: userID, KdBagian: kdBagian, KetPerson: req.Person, Noreg: req.Noreg,
	}).Updates(soap.DPemFisikKeperawatanBangsalRepository{
		GcsE:      req.GCSE,
		GcsV:      req.GCSV,
		GcsM:      req.GCSM,
		Kesadaran: req.Kesadaran,
		Akral:     req.Akral,
		Pupil:     req.Pupil,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateTandaVitalIKeperawatanBangsalRespository(userID string, kdBagian string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res soap.DVitalSignKeperawatanBangsal, err error) {
	errs := sr.DB.Where(&soap.DVitalSignKeperawatanBangsal{
		InsertUserId: userID, KdBagian: kdBagian, KetPerson: req.Person, Noreg: req.Noreg,
	}).Updates(soap.DVitalSignKeperawatanBangsal{
		Bb:         req.BeratBadan,
		Tb:         req.TinggiBadan,
		Td:         req.Td,
		Suhu:       req.Suhu,
		Nadi:       req.Nadi,
		Spo2:       req.Spo2,
		Pernafasan: req.Pernafasan,
	}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetPemFisikKeperawatanRepository(userID string, kdBagian string, person string, noreg string) (res soap.DPemFisikKeperawatanBangsalRepository, err error) {
	query := `SELECT insert_dttm, update_dttm, insert_user_id, kd_bagian, insert_device,  kategori, ket_person, gcs_e, gcs_v, gcs_m, kesadaran, akral, pupil FROM vicore_rme.dpem_fisik where insert_user_id=? AND kd_bagian=? AND ket_person=? AND noreg=?`

	result := sr.DB.Raw(query, userID, kdBagian, person, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}
