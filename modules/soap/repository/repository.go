package repository

import (
	"errors"
	"fmt"
	rmeDto "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/modules/soap/entity"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type soapRepository struct {
	DB         *gorm.DB
	Logging    *logrus.Logger
	soapMapper entity.SoapMapper
}

func NewSoapRepository(db *gorm.DB, logging *logrus.Logger, soapMapper entity.SoapMapper) entity.SoapRepository {
	return &soapRepository{
		DB:         db,
		Logging:    logging,
		soapMapper: soapMapper,
	}
}

func (sr *soapRepository) UpdateTriaseRiwayatAlergiRepository(noReg string, kdBagian string,
	req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, err error) {

	query := `UPDATE vicore_rme.dcppt_soap_dokter  SET	asesmed_keluh_utama=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.KeluhanUtama, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	errs := sr.DB.Where(&soap.TriaseRiwayatAlergi{Noreg: noReg, KdBagian: kdBagian}).Updates(soap.TriaseRiwayatAlergi{
		AsesmedKeluhUtama: req.KeluhanUtama}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

// ON GET DOUBLE CHECK HIGH ALERT
func (sr *soapRepository) OnGetDoubleCheckHightReporitory(noReg string, kdBagian string) (ress []soap.DDoubleCheck, err error) {
	query := `SELECT * From vicore_rme.ddoble_check WHERE noreg=? AND kd_bagian=?`

	result := sr.DB.Raw(query, noReg, kdBagian).Scan(&ress)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return ress, errors.New(message)
	}

	return ress, nil
}

func (sr *soapRepository) OnVerfyDoubleCheckHighRepository(req dto.ReqOnVerifyDoubleCheck, userID string) (res soap.DDoubleCheck, err error) {

	times := time.Now()

	data := soap.DDoubleCheck{
		VerifyDttm:        times.Format("2006-01-02 15:04:05"),
		VerifyNama:        req.NamaVerify,
		PasienVerify:      req.Pasien,
		ObatVerify:        req.Obat,
		DosisVerify:       req.Dosis,
		CaraVerify:        req.Cara,
		WaktuVerify:       req.Waktu,
		InformasiVerify:   req.Informasi,
		DokumentasiVerify: req.Dokumentasi,
	}

	errs := sr.DB.Where(&soap.DDoubleCheck{IdDoubleCheck: req.IDCheck, Noreg: req.Noreg}).Updates(&data).Find(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) OnGetViewDoubleCheckRepository(noReg string) (res []soap.DDoubleCheck, err error) {
	errs := sr.DB.Where(soap.DDoubleCheck{
		Noreg: noReg}).Preload("Perawat").Preload("PerawatVerify").Find(&res)

	if errs != nil {
		return res, errs.Error
	}

	return res, nil
}

// ON SAVE
func (sr *soapRepository) OnSaveDobleCheckHighReporitory(userID string, kdBagian string, devicesID string, req dto.ReqSaveDoubleCheck) (res soap.DDoubleCheck, err error) {

	times := time.Now()
	//===//
	data := soap.DDoubleCheck{
		InsertDttm:         times.Format("2006-01-02 15:04:05"),
		UserId:             userID,
		KdBagian:           kdBagian,
		InsertPc:           devicesID,
		Noreg:              req.Noreg,
		PasienPemberi:      req.Pasien,
		ObatPemberi:        req.Obat,
		DosisPemberi:       req.Dosis,
		CaraPemberi:        req.Cara,
		WaktuPemberi:       req.Waktu,
		InformasiPemberi:   req.Informasi,
		DokumentasiPemberi: req.Dokumentasi,
		Keterangan:         req.Keterangan,
		NamaObat:           req.NamaObat,
		PerawatPemberi:     req.PemberiObat,
		KodeObat:           req.KodeObat,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return res, nil
}

// UPDATE DATA KELUHAN UTAMA IGD
func (sr *soapRepository) UpdateKeluhanUtamaIGDRepository(noReg string, kdBagian string, data dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET aseskep_kel=? WHERE noreg=? AND kd_bagian=?`

	result := sr.DB.Raw(query, data.KeluhanUtama, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	sr.Logging.Info("UPDATE DATA BERHASIL")
	return res, nil
}

func (sr *soapRepository) SaveTriaseRiwayatAlergiRepository(noReg string, kdBagian string, tglMasuk string, jamMasuk string, req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, err error) {

	times := time.Now()

	data := soap.TriaseRiwayatAlergi{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		UpdDttm:           times.Format("2006-01-02 15:04:05"),
		TglMasuk:          tglMasuk[0:10],
		JamMasuk:          jamMasuk,
		Noreg:             noReg,
		AsesmedKeluhUtama: req.KeluhanUtama,
		KdBagian:          kdBagian,
		KeteranganPerson:  req.KeteranganPerson,
		InsertUserId:      req.InsertUserId,
		InsertPc:          req.DeviceID,
		Pelayanan:         req.Pelayanan,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) GetJamMasukPasienRepository(noReg string) (res soap.DRegister, err error) {
	query := `SELECT tanggal, jam FROM rekam.dregister WHERE noreg=?  LIMIT 1`

	result := sr.DB.Raw(query, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) InsertOndontogramRepository(req dto.InsertOdontogram) (res soap.Odontograms, err error) {
	data := soap.Odontograms{Number: req.Number, Keterangan: req.Keterangan}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, err
	}

	return data, nil
}

func (sr *soapRepository) UpdateAssesRawatJalanDokterRepository(req dto.RequestSaveAssementRawatJalanDokter) (res soap.DcpptOdontogram, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET asesmed_jenpel=?, asesmed_keluh_utama=?, asesmed_rwyt_skrg=?,
	asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?,
	asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=?, asesmed_gol_darah=?,
	asesmed_td=?, asesmed_td_detail=?, asesmed_peny_jantung=?, asesmed_peny_jantung_detail=?, asesmed_diabet=?, asesmed_diabet_detail=?, asesmed_haemop=?,
	asesmed_haemop_detail=?, asesmed_hepat=?, asesmed_hepat_detail=?, asesmed_peny_lain=?, asesmed_peny_lain_detail=?, asesmed_alergi_obat=?, asesmed_alergi_obat_detail=?, 
	asesmed_alergi_mknan=?, asesmed_alergi_mknan_detail=?, asesmed_kebiasaan_buruk=?, asesmed_khus_ok_anterior=?, asesmed_khus_ok_posterior=?, asesmed_khus_ok_molar=?, 
	asesmed_khus_palatum=?, asesmed_khus_torus_P=?, asesmed_khus_torus_M=?, asesmed_khus_torus_M_detail=?, asesmed_khus_super_teeth=?, asesmed_khus_super_teeth_detail=?, 
	asesmed_khus_diastema=?, asesmed_khus_diastema_detail=?, asesmed_khus_gigi_anomali=?, asesmed_khus_gigi_anomali_detail=?, asesmed_khus_oral_lain=?
	WHERE noreg=?;`

	result := sr.DB.Raw(query, req.Pelayanan, req.KeluhanUtama, req.AsesmedRwytSkrg, req.AsesmedRwytDulu,
		req.AssesRiwayat.AsesmedRwytObat, req.AssesRiwayat.AsesmedRwytPenyKlrg, req.AssesRiwayat.AsesmedRwytAlergi, req.AssesRiwayat.AsesmedRwytAlergiDetail,
		req.AsesmedKhusPemGigi1, req.AsesmedKhusPemGigi2, req.AsesmedKhusPemGigi3, req.AsesmedKhusPemGigi4, req.AsesmedKhusPemGigi5, req.AsesmedKhusPemGigi5Detail,
		req.AsesmedGolDarah, req.AsesmedTd, req.AsesmedTdDetail, req.AsesmedPenyJantung, req.AsesmedPenyJantungDetail, req.AsesmedDiabet, req.AsesmedDiabetDetail, req.AsesmedHaemop, req.AsesmedHaemopDetail, req.AsesmedHepat, req.AsesmedHepatDetail, req.AsesmedPenyLain, req.AsesmedPenyLainDetail, req.AsesmedAlergiObat, req.AsesmedAlergiObatDetail,
		req.AsesmedAlergiMknan, req.AsesmedAlergiMknanDetail, req.AsesmedKebiasaanBuruk, req.AsesmedKhusOkAnterior, req.AsesmedKhusOkPosterior, req.AsesmedKhusOkMolar, req.AsesmedKhusPalatum,
		req.AsesmedKhusTorusP, req.AsesmedKhusTorusM, req.AsesmedKhusTorusMDetail, req.AsesmedKhusSuperTeeth, req.AsesmedKhusSuperTeethDetail,
		req.AsesmedKhusDiastema, req.AsesmedDiabetDetail, req.AsesmedKhusGigiAnomali, req.AsesmedKhusGigiAnomaliDetail, req.AsesmedKhusOralLain,
		req.NoReg).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) InsertAssesRawatJalanDokterRepository(userID string, req dto.RequestSaveAssementRawatJalanDokter) (res soap.DcpptOdontogram, err error) {
	times := time.Now()
	data := soap.DcpptOdontogram{
		InsertDttm:                   times.Format("2006-01-02 15:04:05"),
		InsertUserID:                 userID,
		KdBagian:                     req.KdBagian,
		Noreg:                        req.NoReg,
		AsesmedJenpel:                req.Pelayanan,
		AsesmedKeluhUtama:            req.KeluhanUtama,
		AsesmedRwytSkrg:              req.AssesRiwayat.AsesmedRwytSkrg,
		AsesmedRwytDulu:              req.AssesRiwayat.AsesmedRwytDulu,
		AsesmedRwytObat:              req.AssesRiwayat.AsesmedRwytObat,
		AsesmedRwytPenyKlrg:          req.AssesRiwayat.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergi:            req.AssesRiwayat.AsesmedRwytAlergi,
		AsesmedRwytAlergiDetail:      req.AssesRiwayat.AsesmedRwytAlergiDetail,
		AsesmedKhusPemGigi1:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:          req.AssesPemeriksaanGigi.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail:    req.AssesPemeriksaanGigi.AsesmedKhusPemGigi5Detail,
		AsesmedGolDarah:              req.AsesmedGolDarah,
		AsesmedTd:                    req.AsesmedTd,
		AsesmedTdDetail:              req.AsesmedTdDetail,
		AsesmedPenyJantung:           req.AsesmedPenyJantung,
		AsesmedPenyJantungDetail:     req.AsesmedPenyJantungDetail,
		AsesmedDiabet:                req.AsesmedDiabet,
		AsesmedDiabetDetail:          req.AsesmedDiabetDetail,
		AsesmedHaemop:                req.AsesmedHaemop,
		AsesmedHaemopDetail:          req.AsesmedAlergiMknanDetail,
		AsesmedHepat:                 req.AsesmedHepat,
		AsesmedHepatDetail:           req.AsesmedHepatDetail,
		AsesmedPenyLain:              req.AsesmedPenyLain,
		AsesmedPenyLainDetail:        req.AsesmedPenyLainDetail,
		AsesmedAlergiObat:            req.AsesmedAlergiObat,
		AsesmedAlergiObatDetail:      req.AsesmedAlergiObatDetail,
		AsesmedAlergiMknan:           req.AsesmedAlergiMknan,
		AsesmedAlergiMknanDetail:     req.AsesmedAlergiMknanDetail,
		AsesmedKebiasaanBuruk:        req.AsesmedKebiasaanBuruk,
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusMDetail:      req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) InsertImageOdontogramRepository(kdBagian string, noReg string, imgFile string) (res soap.DcpptOdontogram, err error) {
	times := time.Now()
	data := soap.DcpptOdontogram{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: noReg, AsesmedKhusGigiOdontPng: imgFile,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) InsertDataOdontogramRepository(kdBagian string, NoReg string, imgFile string) (res soap.DcpptOdontogram, err error) {
	times := time.Now()
	data := soap.DcpptOdontogram{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: NoReg, AsesmedKhusGigiOdontPng: imgFile,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) UpdateImageOdontogramRepository(noReg string, imgFile string) (res soap.DcpptOdontogram, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_khus_gigi_odont_png=? WHERE noreg=?;`

	result := sr.DB.Raw(query, imgFile, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) UpdateDataOdontogramRepository(noReg string, imgFile string) (res soap.DcpptOdontogram, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_khus_gigi_odont_png=? WHERE noreg=?;`

	result := sr.DB.Raw(query, imgFile, noReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetRecordOdontogramRepository(noReg string) (res soap.DcpptOdontogram, err error) {
	results := sr.DB.Where("noreg=?", noReg).Find(&res)

	if results.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", results.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetOdontogramRepository(noReg string) (res []soap.Odontograms, err error) {

	results := sr.DB.Where("noreg=?", noReg).Find(&res)

	if results.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", results.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) SearchDcpptSoapPasienRepository(noReg string, kdBagian string) (res dto.ResDcpptSoap, err error) {
	var data soap.DcpptSoap

	results := sr.DB.Select("kd_bagian, noreg").Where(&soap.DcpptSoap{KdBagian: kdBagian, Noreg: noReg}).Find(&data)

	if results.Error != nil {
		return res, results.Error
	}

	mapper := sr.soapMapper.ToResDcpptSoapMapper(data.KdBagian, data.Noreg)

	return mapper, nil
}

func (sr *soapRepository) SearchDcpptSoapPasienBangsalRepository(noReg string, userID string, kdBagian string) (res dto.ResDcpptSoap, err error) {
	var data soap.DcpptSoapPasien

	results := sr.DB.Select("kd_bagian, noreg").Where(&soap.DcpptSoapPasien{KdBagian: kdBagian, Noreg: noReg, InsertUserId: userID}).Find(&data)

	if results.Error != nil {
		return res, results.Error
	}

	mapper := sr.soapMapper.ToResDcpptSoapMapper(data.KdBagian, data.Noreg)

	return mapper, nil
}

func (sr *soapRepository) SearchDcpptSoapDokterRepository(noReg string, kdBagian string) (res dto.ResDcpptSoap, err error) {
	var data soap.DcpptSoapDokter

	results := sr.DB.Select("kd_bagian, noreg").Where(&soap.DcpptSoapDokter{KdBagian: kdBagian, Noreg: noReg}).Find(&data)

	if results.Error != nil {
		return res, results.Error
	}

	sr.Logging.Info("Data ditemukan")
	sr.Logging.Info(data)

	mapper := sr.soapMapper.ToResDcpptSoapMapper(data.KdBagian, data.Noreg)

	return mapper, nil
}

func (sr *soapRepository) SearchDcpptSoapDokterForAsesmentRepository(userID string, noReg string, kdBagian string) (res dto.ResDcpptSoap, err error) {
	var data soap.DcpptSoapDokter

	results := sr.DB.Select("insert_user_id, kd_bagian, noreg").Where(&soap.DcpptSoapDokter{InsertUserId: userID, KdBagian: kdBagian, Noreg: noReg}).Find(&data)

	if results.Error != nil {
		return res, results.Error
	}

	sr.Logging.Info("Data ditemukan")
	sr.Logging.Info(data)

	mapper := sr.soapMapper.ToResDcpptSoapMapper(data.KdBagian, data.Noreg)

	return mapper, nil
}

func (sr *soapRepository) UpdateDcpptSoapPasien(req dto.RequestSaveAssementRawatJalanPerawat) (res soap.AssementRawatJalanPerawat, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_kel=?, aseskep_rwyt_pnykt=?, aseskep_rwyt_obat=?, aseskep_rwyt_obat_detail=?, aseskep_td=?, aseskep_nadi=?, aseskep_suhu=?, aseskep_rr=?, aseskep_bb=?, aseskep_tb=?, aseskep_ases_fungsional=?, aseskep_ases_fungsional_detail=?, aseskep_rj1=?, aseskep_rj2=?, aseskep_hsl_kaji_rj=?, aseskep_hsl_kaji_rj_tind=?,aseskep_ases_nyeri=?, aseskep_psiko=?, aseskep_psiko_detail=?, aseskep_pulang1=?, aseskep_pulang1_detail=?, aseskep_pulang2=?, aseskep_pulang2_detail=?, aseskep_pulang3=?, aseskep_pulang3_detail=?, aseskep_mslh_kprwtan=?, aseskep_renc_kprwtan=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.KelUtama, req.RiwayatPenyakit, req.RiwayatObat, req.RiwayatObatDetail, req.TekananDarah, req.Nadi, req.Suhu, req.Pernapasan, req.BeratBadan, req.TinggiBadan, req.Fungsional, req.FungsionalDetail, req.AseskepRj1, req.AseskepRj2, req.AseskepHasilKajiRj, req.AseskepHslKajiRjTind, req.AseskepNyeri, req.Psikologis, req.PsikologisDetail, req.AseskepPulang1, req.AseskepPulang1Detail, req.AseskepPulang2, req.AseskepPulang2Detail, req.AseskepPulang3, req.AseskepPulang3Detail, req.MasalahKeperawatan, req.RencanaKeperawatan, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) InsertDcpptSoapPasien(req dto.RequestSaveAssementRawatJalanPerawat) (res soap.AssementRawatJalanPerawat, err error) {
	times := time.Now()
	data := soap.AssementRawatJalanPerawat{
		InsertDttm: times.Format("2006-01-02 15:04:05"), AseskepKel: req.KelUtama, AseskepRwytPnykt: req.RiwayatPenyakit,
		AseskepTd: req.TekananDarah, AseskepNadi: req.Nadi, AseskepSuhu: req.Suhu, AseskepRr: req.Pernapasan, AseskepBb: req.BeratBadan, AseskepTb: req.TinggiBadan, AseskepAsesFungsional: req.Fungsional, AseskepMslhKprwtan: req.MasalahKeperawatan, AseskepRencKprwtan: req.RencanaKeperawatan, Noreg: req.NoReg, KdBagian: req.KodeBagian,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil

}

func (sr *soapRepository) SaveAnatomiRepository(req dto.RequestSaveAnatomi, urlImage string, dpjp string) (res soap.Anatomi, err error) {
	anatomi := soap.Anatomi{Norm: req.Norm, Nama: req.Nama, Keterangan: req.Keterangan, UrlImage: urlImage, Dpjp: dpjp}
	result := sr.DB.Create(&anatomi)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return res, errors.New(message)
	}

	return anatomi, nil
}

func (sr *soapRepository) SaveSkriningRepository(req dto.RequestSaveSkrining) (res soap.DcpptSoapPasien, err error) {

	var datetime = time.Now().UTC()
	dt := datetime.Format("2006-01-02 15:04:05")

	skrining := soap.DcpptSoapPasien{InsertDttm: dt, Noreg: req.NoReg, KdBagian: req.KodeBagian, SkriningK1: req.K1, SkriningK2: req.K2,
		SkriningK3: req.K3, SkriningK4: req.K4, SkriningK5: req.K5, SkriningK6: req.K6, SkriningF1: req.KF1, SkriningF2: req.KF2, SkriningF3: req.KF3,
		SkriningF4: req.KF4, SkriningB1: req.B1, SkriningB2: req.B2, SkriningRj: req.RJ, SkriningR1: req.IV1, SkriningR2: req.IV2, SkriningR3: req.IV3,
		SkriningR4: req.IV4, SkriningTgl: dt, AseskepTgl: dt,
	}

	result := sr.DB.Create(&skrining)

	if result.Error != nil {

		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error.Error())
		return skrining, errors.New(message)
	}

	return skrining, nil
}

func (sr *soapRepository) UpdateSkriningRepository(req dto.RequestSaveSkrining) (res soap.DcpptSoapPasien, err error) {

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET skrining_k1=?, skrining_k2=?, skrining_k3=?,   skrining_k4=? , skrining_k5=?, skrining_k6=?, skrining_f1=?, skrining_f2=?, skrining_f3=?, skrining_f4=?, skrining_b1=?, skrining_b2=?, skrining_rj=?, skrining_r1=?,
	skrining_r2=?, skrining_r3=?, skrining_r4=?, skrining_user=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.K1, req.K2, req.K3, req.K4, req.K5, req.K6, req.KF1, req.KF2, req.KF3, req.KF4, req.B1, req.B2, req.RJ,
		req.IV1, req.IV2, req.IV3, req.IV4, req.User, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *soapRepository) GetSkriningRepository(noreg string) (res soap.DcpptSoapPasien, err error) {
	results := sr.DB.Where("noreg=?", noreg).Find(&res)

	if results.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", results.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetKebutuhanEdukasiRespository(noReg string) (res soap.KebEdukasi, err error) {
	results := sr.DB.Where("noreg=?", noReg).Find(&res)

	if results.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", results.Error.Error())
		return res, errors.New(message)
	}

	return res, nil

}

func (sr *soapRepository) GetAssementRawatJalanPerawatRepository(noReg string) (res soap.AssementRawatJalanPerawat, err error) {
	results := sr.DB.Where("noreg=?", noReg).Find(&res)

	if results.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", results.Error.Error())
		return res, results.AddError(errors.New(message))
	}

	return res, nil
}

func (sr *soapRepository) DeleteOdontogramRepository(req dto.RequestDeleteOdontogram) (res soap.DcpptSoapPasien, err error) {
	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =""  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, result.AddError(errors.New(message))
	}

	return res, nil
}

func (sr *soapRepository) DeleteDiagnosa(req dto.ReqDeleteDiagnosa, kdBagian string) (err error) {

	var datas soap.DiagnosaModelOne
	value := "asesmed_diag" + req.Table

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =""  WHERE noreg=? AND kd_bagian=?;`
	result := sr.DB.Raw(query, req.Noreg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return errors.New(message)
	}

	return nil
}

func (sr *soapRepository) GetDiagnosaRepository(noreg string, kdBagian string, kdDpjp string) (res []soap.DiagnosaResponse, err error) {

	var diag []soap.DiagnosaResponse

	type Diagnoses struct {
		P    string `json:"p"`
		Desp string `json:"desp"`
		S1   string `json:"s1"`
		Des1 string `json:"des1"`
		S2   string `json:"s2"`
		Des2 string `json:"des2"`
		S3   string `json:"s3"`
		Des3 string `json:"des3"`
		S4   string `json:"s4"`
		Des4 string `json:"des4"`
		S5   string `json:"s5"`
		Des5 string `json:"des5"`
		S6   string `json:"s6"`
		Des6 string `json:"des6"`
	}

	data := Diagnoses{}

	query := `
			SELECT asesmed_diagP AS p, b.description as desp, 
			asesmed_diagS1 AS s1, b1.description as des1, 
			asesmed_diagS2 AS s2, b2.description as des2,
			asesmed_diagS3 AS s3, b3.description as des3,
			asesmed_diagS4 AS s4, b4.description as des4,
			asesmed_diagS5 AS s5, b5.description as des5,
			asesmed_diagS6 AS s6, b6.description as des6
			FROM vicore_rme.dcppt_soap_dokter AS a 
			LEFT JOIN vicore_lib.k_icd10 AS b ON a.asesmed_diagP=b.code2 
			LEFT JOIN vicore_lib.k_icd10 AS b1 ON a.asesmed_diagS1=b1.code2
			LEFT JOIN vicore_lib.k_icd10 AS b2 ON a.asesmed_diagS2=b2.code2
			LEFT JOIN vicore_lib.k_icd10 AS b3 ON a.asesmed_diagS3=b3.code2
			LEFT JOIN vicore_lib.k_icd10 AS b4 ON a.asesmed_diagS4=b4.code2
			LEFT JOIN vicore_lib.k_icd10 AS b5 ON a.asesmed_diagS5=b5.code2
			LEFT JOIN vicore_lib.k_icd10 AS b6 ON a.asesmed_diagS6=b6.code2
			WHERE a.noreg=? AND a.kd_bagian=? AND a.kd_dpjp=? LIMIT 1;
		`

	result := sr.DB.Raw(query, noreg, kdBagian, kdDpjp).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	if len(data.P) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.P,
			Description: data.Desp,
			Type:        "primer",
			Table:       "P",
		})
	}

	if len(data.S1) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S1,
			Description: data.Des1,
			Type:        "sekunder",
			Table:       "S1",
		})
	}

	if len(data.S2) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S2,
			Description: data.Des2,
			Type:        "sekunder",
			Table:       "S2",
		})
	}

	if len(data.S3) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S3,
			Description: data.Des3,
			Type:        "sekunder",
			Table:       "S3",
		})
	}

	if len(data.S4) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S4,
			Description: data.Des4,
			Type:        "sekunder",
			Table:       "S4",
		})
	}

	if len(data.Des5) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S5,
			Description: data.Des5,
			Type:        "sekunder",
			Table:       "S5",
		})
	}

	if len(data.S6) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S6,
			Description: data.Des6,
			Type:        "sekunder",
			Table:       "S6",
		})
	}

	sr.Logging.Info("GET DATA DIAGNOSA")
	sr.Logging.Info(diag)

	return diag, nil
}

func (sr *soapRepository) GetDiagnosaRepositoryBangsal(noreg string, kdBagian string) (res []soap.DiagnosaResponse, err error) {
	var diag []soap.DiagnosaResponse

	type Diagnoses struct {
		P    string `json:"p"`
		Desp string `json:"desp"`
		S1   string `json:"s1"`
		Des1 string `json:"des1"`
		S2   string `json:"s2"`
		Des2 string `json:"des2"`
		S3   string `json:"s3"`
		Des3 string `json:"des3"`
		S4   string `json:"s4"`
		Des4 string `json:"des4"`
		S5   string `json:"s5"`
		Des5 string `json:"des5"`
		S6   string `json:"s6"`
		Des6 string `json:"des6"`
	}

	data := Diagnoses{}

	query := `
			SELECT asesmed_diagP AS p, b.description as desp, 
			asesmed_diagS1 AS s1, b1.description as des1, 
			asesmed_diagS2 AS s2, b2.description as des2,
			asesmed_diagS3 AS s3, b3.description as des3,
			asesmed_diagS4 AS s4, b4.description as des4,
			asesmed_diagS5 AS s5, b5.description as des5,
			asesmed_diagS6 AS s6, b6.description as des6
			FROM vicore_rme.dcppt_soap_dokter AS a 
			LEFT JOIN vicore_lib.k_icd10 AS b ON a.asesmed_diagP=b.code2 
			LEFT JOIN vicore_lib.k_icd10 AS b1 ON a.asesmed_diagS1=b1.code2
			LEFT JOIN vicore_lib.k_icd10 AS b2 ON a.asesmed_diagS2=b2.code2
			LEFT JOIN vicore_lib.k_icd10 AS b3 ON a.asesmed_diagS3=b3.code2
			LEFT JOIN vicore_lib.k_icd10 AS b4 ON a.asesmed_diagS4=b4.code2
			LEFT JOIN vicore_lib.k_icd10 AS b5 ON a.asesmed_diagS5=b5.code2
			LEFT JOIN vicore_lib.k_icd10 AS b6 ON a.asesmed_diagS6=b6.code2
			WHERE a.noreg=? AND a.kd_bagian=? LIMIT 1;
		`

	result := sr.DB.Raw(query, noreg, kdBagian).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	if len(data.P) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.P,
			Description: data.Desp,
			Type:        "primer",
			Table:       "P",
		})
	}

	if len(data.S1) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S1,
			Description: data.Des1,
			Type:        "sekunder",
			Table:       "S1",
		})
	}

	if len(data.S2) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S2,
			Description: data.Des2,
			Type:        "sekunder",
			Table:       "S2",
		})
	}

	if len(data.S3) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S3,
			Description: data.Des3,
			Type:        "sekunder",
			Table:       "S3",
		})
	}
	if len(data.S4) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S4,
			Description: data.Des4,
			Type:        "sekunder",
			Table:       "S4",
		})
	}

	if len(data.Des5) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S5,
			Description: data.Des5,
			Type:        "sekunder",
			Table:       "S5",
		})
	}

	if len(data.S6) > 1 {
		diag = append(diag, soap.DiagnosaResponse{
			Diagnosa:    data.S6,
			Description: data.Des6,
			Type:        "sekunder",
			Table:       "S6",
		})
	}

	sr.Logging.Info("GET DATA DIAGNOSA")
	sr.Logging.Info(diag)

	return diag, nil
}

func (sr *soapRepository) OnGetTindakanICD9RepositoryBangsalDokter(noReg string, kdBagian string) (res []soap.TindakanResponse, err error) {

	var listTindakan []soap.TindakanResponse

	type Tindakan struct {
		Noreg        string
		KdBagian     string
		AsesmenPros1 string `gorm:"column:asesmed_pros1" json:"asesmen_pros1"`
		AsesmenPros2 string `gorm:"column:asesmed_pros2" json:"asesmen_pros2"`
		Des1         string `gorm:"column:des1" json:"des1"`
		Des2         string `gorm:"column:des2" json:"des2"`
	}

	data := Tindakan{}

	query := `
			SELECT noreg, kd_bagian, asesmed_pros1,asesmed_pros2 , lib.Description AS des1 , lib1.Description AS des2 FROM vicore_rme.dcppt_soap_dokter AS dd
			LEFT JOIN vicore_lib.k_icd9 AS lib ON lib.Code2 = dd.asesmed_pros1
			LEFT JOIN vicore_lib.k_icd9 AS lib1 ON lib1.Code2 = dd.asesmed_pros2
			WHERE noreg=? AND kd_bagian=? LIMIT 1;
			`
	// SELECT noreg, kd_bagian, asesmed_pros1,  asesmed_pros2 , lib.Description AS des1, lib1.Description AS des2
	// FROM vicore_rme.dcppt_soap_dokter AS dd
	// LEFT JOIN vicore_lib.k_icd9 AS lib ON lib.Code = dd.asesmed_pros1
	// LEFT JOIN vicore_lib.k_icd9 AS lib1 ON lib1.Code = dd.asesmed_pros2

	result := sr.DB.Raw(query, noReg, kdBagian).Scan(&data)

	sr.Logging.Info(data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	if len(data.AsesmenPros1) > 1 {
		listTindakan = append(listTindakan, soap.TindakanResponse{
			Description: data.Des1,
			Code2:       data.AsesmenPros1,
		})
	}

	if len(data.AsesmenPros2) > 1 {
		listTindakan = append(listTindakan, soap.TindakanResponse{
			Description: data.Des2,
			Code2:       data.AsesmenPros2,
		})
	}

	return listTindakan, nil
}

func (sr *soapRepository) GetDiagnosaBandingRepositoryBangsal(noreg string, kdBagian string) (res soap.DiagnosaBandingResponse, err error) {
	var diag soap.DiagnosaBandingResponse

	query := `
			SELECT asesmed_diag_banding AS diagnosa, b.description AS description
			FROM vicore_rme.dcppt_soap_dokter AS a LEFT JOIN vicore_lib.k_icd10 AS b ON a.asesmed_diag_banding=b.code2
			WHERE a.noreg=? AND a.kd_bagian=? LIMIT 1;
		`

	result := sr.DB.Raw(query, noreg, kdBagian).Scan(&diag)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error.Error())
		return res, errors.New(message)
	}

	return diag, nil
}

func (sr *soapRepository) InsertSingleDiagnosaFirst(req dto.ReqInputSingleDiagnosa, kdBagian string) (res soap.DiagnosaModelOne, err error) {
	errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Save(soap.DiagnosaModelOne{
		AsesmedDiagp: req.Code}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InsertRencanaTindakLanjutRepository(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.RencanaTindakLanjutModel{Noreg: req.NoReg, KdBagian: kdBagian}).Save(soap.RencanaTindakLanjutModel{
		KdBagian: kdBagian, Noreg: req.NoReg, AsesmedTerapi: req.RencanaAnjuranTerapi, AsesmedAlasanOpname: req.AlasanOpname, AsesmedKonsulKe: req.KonsulKe, InsertDttm: times.Format("2006-01-02 15:04:05")}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateRencanaTindakLanjutRepository(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.RencanaTindakLanjutModel{Noreg: req.NoReg, KdBagian: kdBagian}).Updates(soap.RencanaTindakLanjutModel{
		KdBagian: kdBagian, Noreg: req.NoReg, AsesmedTerapi: req.RencanaAnjuranTerapi, AsesmedAlasanOpname: req.AlasanOpname, AsesmedKonsulKe: req.KonsulKe, InsertDttm: times.Format("2006-01-02 15:04:05")}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) InputSingleDiagnosa(req dto.ReqInputSingleDiagnosa, kdBagian string) (res soap.DiagnosaModelOne, err error) {

	if req.Type == "primer" {
		errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
			AsesmedDiagp: req.Code}).Scan(&res).Error
		if errs != nil {
			return res, errs
		}
		return res, nil
	}

	if req.Type == "sekunder" {

		switch req.Table {
		case "S1":

			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags1: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		case "S2":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags2: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S3":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags3: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S4":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags4: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil

		case "S5":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags5: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		case "S6":
			errs := sr.DB.Where(&soap.DiagnosaModelOne{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(soap.DiagnosaModelOne{
				AsesmedDiags6: req.Code}).Scan(&res).Error
			if errs != nil {
				return res, errs
			}
			return res, nil
		}
	}

	return res, nil
}

func (sr *soapRepository) InsertDiagnosa(req dto.ReqInputDiagnosa) (res soap.DiagnosaModelOne, err error) {
	for i := 0; i <= len(req.Diagnosa)-1; i++ {
		if req.Diagnosa[i].Type == "primary" {
			query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_diagP =? WHERE noreg=? AND kd_bagian=?;`
			sr.DB.Raw(query, req.Diagnosa[i].Code, req.Noreg, req.KdBagian).Scan(res)

		}

		if req.Diagnosa[i].Type == "sekunder" {
			value := "asesmed_diagS" + strconv.Itoa(i)
			query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =? WHERE noreg=? AND kd_bagian=?;`
			sr.DB.Raw(query, req.Diagnosa[i].Code, req.Noreg, req.KdBagian).Scan(&res)

		}

	}

	return res, nil
}

func (sr *soapRepository) UpdateOdontogramRepository(req dto.RequestSaveOdontogram) (res soap.DcpptSoapPasien, err error) {

	value := "asesmed_khus_odont_" + req.NoGigi

	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET ` + value + ` =?  WHERE noreg=?;`
	result := sr.DB.Raw(query, req.Keterangan, req.NoReg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) DetailSkriningRepository(noreg string) (res soap.DcpptSoapPasien, err error) {
	query := `SELECT * from vicore_rme.dcppt_soap_pasien where noreg=?`
	result := sr.DB.Raw(query, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) SaveAnamnesaRepository(req dto.RequestSaveAnamnesa) (err error) {
	data := soap.Anamnesa{}
	query := `UPDATE vicore_rme.dcppt_soap_pasien  SET asesmed_keluh_utama=?, asesmed_jenpel=?, asesmed_rwyt_skrg=?, asesmed_rwyt_dulu=?, asesmed_rwyt_obat=?, asesmed_rwyt_peny_klrg=?, asesmed_rwyt_alergi=?, asesmed_rwyt_alergi_detail=?, asesmed_khus_pem_gigi1=?, asesmed_khus_pem_gigi2=?, asesmed_khus_pem_gigi3=?, asesmed_khus_pem_gigi4=?, asesmed_khus_pem_gigi5=?, asesmed_khus_pem_gigi5_detail=? WHERE noreg=?;`

	result := sr.DB.Raw(query, req.AsesmedKeluhUtama, req.AsesmedJenpel,
		req.AsesmenRwtSkrg, req.AsesmenRwtDulu, req.AsesmenRwtObat, req.AsesmenRwtPenyKlrg, req.AsesmenRwtPenyAlergi,
		req.AsesmenRwtPenyAlergiDetail, req.AsesmenKhusPemGigi1, req.AsesmenKhusPemGigi2, req.AsesmenKhusPemGigi3,
		req.AsesmenKhusPemGigi4, req.AsesmenKhusPemGigi5, req.AsesmenKhusPemGigi5Detail, req.NoReg).Scan(&data)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", result.Error)
		return errors.New(message)
	}

	return nil
}

func (sr *soapRepository) GetAnamnesaRepository(noreg string) (res soap.Anamnesa, err error) {
	query := `SELECT asesmed_keluh_utama AS keluhan,asesmed_jenpel AS jenpel,asesmed_rwyt_skrg AS riwayat_sekarang,asesmed_rwyt_dulu AS riwayat_dulu,asesmed_rwyt_obat AS riwayat_obat,asesmed_rwyt_peny_klrg AS penyakit_keluarga,asesmed_rwyt_alergi AS alergi,asesmed_rwyt_alergi_detail AS alergi_detail,
	asesmed_khus_pem_gigi1 AS gigi1,asesmed_khus_pem_gigi2 AS gigi2,asesmed_khus_pem_gigi3 AS gigi3,asesmed_khus_pem_gigi4 AS gigi4,asesmed_khus_pem_gigi5 AS gigi5,asesmed_khus_pem_gigi5_detail AS gigi_detail  from vicore_rme.dcppt_soap_pasien where noreg=?`
	result := sr.DB.Raw(query, noreg).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetHasilPenunjangMedikRepository(noreg string) (res []soap.HasilPenunjangMedik, err error) {
	ersr := sr.DB.Where("noreg=?", noreg).Preload("KPelayanan").Find(&res).Error
	if ersr != nil {
		return res, ersr
	}
	return res, nil
}

// GET MASALAH MEDIS
func (sr *soapRepository) SaveMasalahMedis(req dto.ReqDataPasien) (res soap.InformasiMedis, err error) {
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// UPDATE INFORMASI MEDIS
func (sr *soapRepository) UpdateInformasiMedis(req dto.ReqInformasiMedis) (res soap.InformasiMedis, err error) {
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).Updates(soap.InformasiMedis{
		AsesmedMslhMedis: req.MasalahMedis,
		AsesmedTerapi:    req.Terapi,
		AsesmedPemFisik:  req.PemeriksaanFisik,
		AsesmedAnjuran:   req.Anjuran,
	}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetInformasiMedis(req dto.ReqDataPasien) (res soap.InformasiMedis, err error) {
	errs := sr.DB.Select("insert_dttm", "kd_bagian", "noreg", "asesmed_mslh_medis", "asesmed_terapi", "asesmed_pem_fisik", "asesmed_anjuran").Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).First(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// INSERT DATA INFORMASI MEDIS
func (sr *soapRepository) InsertInformasiMedisRepository(req dto.ReqInformasiMedis) (res soap.InformasiMedis, err error) {
	times := time.Now()
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: req.Noreg, KdBagian: req.KdBagian}).Save(soap.InformasiMedis{
		AsesmedMslhMedis: req.MasalahMedis,
		AsesmedTerapi:    req.Terapi,
		AsesmedPemFisik:  req.PemeriksaanFisik,
		AsesmedAnjuran:   req.Anjuran,
		InsertDttm:       times.Format("2006-01-02 15:04:05"),
	}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// CARI SOAP PASIEN
func (sr *soapRepository) SearchSoapPasien(noReg string, kdBagian string) (res soap.SoapPasien, err error) {
	errs := sr.DB.Where(&soap.InformasiMedis{Noreg: noReg, KdBagian: kdBagian}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// GET DATA
func (sr *soapRepository) GetAsesmedAnamnesaRepository(noReg string, kdBagian string) (res soap.AsesmedAnamnesa, err error) {

	errs := sr.DB.Select("asesmed_keluh_utama", "asesmed_rwyt_skrg", "asesmed_rwyt_peny_klrg", "asesmed_rwyt_alergi",
		"asesmed_rwyt_alergi_detail", "asesmed_jenpel", "asesmed_khus_pem_gigi1", "asesmed_khus_pem_gigi2", "asesmed_khus_pem_gigi3", "asesmed_khus_pem_gigi4", "asesmed_khus_pem_gigi5", "asesmed_khus_pem_gigi5_detail", "kd_bagian", "noreg").Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: noReg}).Find(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// GET DATA MEDIK YANG DIPERLUKAN
func (sr *soapRepository) GetDataMedikRepository(noReg string, kdBagian string) (res soap.DataMedikModel, err error) {
	errs := sr.DB.Select("asesmed_gol_darah", "asesmed_td", "asesmed_td_detail", "asesmed_peny_jantung",
		"asesmed_peny_jantung_detail", "asesmed_diabet", "asesmed_diabet_detail", "asesmed_haemop",
		"asesmed_haemop_detail", "asesmed_hepat", "asesmed_hepat_detail", "asesmed_peny_lain",
		"asesmed_peny_lain_detail", "asesmed_alergi_obat", "asesmed_alergi_obat_detail",
		"asesmed_alergi_mknan", "asesmed_alergi_mknan_detail", "asesmed_kebiasaan_buruk", "kd_bagian", "noreg").Where(&soap.DataMedikModel{KdBagian: kdBagian, Noreg: noReg}).Find(&res).Error
	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) GetDataIntraOralRepository(noReg string, kdBagian string) (res soap.DataIntraOralModel, err error) {
	errs := sr.DB.Select("asesmed_khus_ok_anterior", "asesmed_khus_ok_posterior", "asesmed_khus_ok_molar", "asesmed_khus_palatum", "asesmed_khus_torus_P", "asesmed_khus_torus_M", "asesmed_khus_torus_M_detail", "asesmed_khus_super_teeth", "asesmed_khus_super_teeth_detail", "asesmed_khus_diastema", "asesmed_khus_diastema_detail", "asesmed_khus_gigi_anomali", "asesmed_khus_gigi_anomali_detail", "asesmed_khus_oral_lain", "kd_bagian", "noreg").Where(&soap.DataIntraOralModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil

}

func (sr *soapRepository) InsertDataIntraOral(req dto.ReqDataIntraOral, kdBagian string) (res soap.DataIntraOralModel, err error) {
	times := time.Now()
	errs := sr.DB.Where(&soap.DataIntraOralModel{
		Noreg: req.Noreg, KdBagian: kdBagian}).Save(&soap.DataIntraOralModel{
		InsertDttm:                   times.Format("2006-01-02 15:04:05"),
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusDetail:       req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
		Noreg:                        req.Noreg,
		KdBagian:                     kdBagian,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}

	return res, nil
}

func (sr *soapRepository) UpdateDataIntraOral(req dto.ReqDataIntraOral, kdBagian string) (res soap.DataIntraOralModel, err error) {

	errs := sr.DB.Where(&soap.DataIntraOralModel{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(&soap.DataIntraOralModel{
		AsesmedKhusOkAnterior:        req.AsesmedKhusOkAnterior,
		AsesmedKhusOkPosterior:       req.AsesmedKhusOkPosterior,
		AsesmedKhusOkMolar:           req.AsesmedKhusOkMolar,
		AsesmedKhusPalatum:           req.AsesmedKhusPalatum,
		AsesmedKhusTorusP:            req.AsesmedKhusTorusP,
		AsesmedKhusTorusM:            req.AsesmedKhusTorusM,
		AsesmedKhusTorusDetail:       req.AsesmedKhusTorusMDetail,
		AsesmedKhusSuperTeeth:        req.AsesmedKhusSuperTeeth,
		AsesmedKhusSuperTeethDetail:  req.AsesmedKhusSuperTeethDetail,
		AsesmedKhusDiastema:          req.AsesmedKhusDiastema,
		AsesmedKhusDiastemaDetail:    req.AsesmedKhusDiastemaDetail,
		AsesmedKhusGigiAnomali:       req.AsesmedKhusGigiAnomali,
		AsesmedKhusGigiAnomaliDetail: req.AsesmedKhusGigiAnomaliDetail,
		AsesmedKhusOralLain:          req.AsesmedKhusOralLain,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateDataMedik(req dto.ReqDataMedik, kdBagian string) (res soap.DataMedikModel, err error) {
	errs := sr.DB.Select(
		"asesmed_gol_darah", "asesmed_td", "asesmed_td_detail", "asesmed_peny_jantung", "asesmed_peny_jantung_detail",
		"asesmed_diabet", "asesmed_diabet_detail", "asesmed_haemop", "asesmed_haemop_detail", "asesmed_hepat", "asesmed_hepat_detail", "asesmed_peny_lain", "asesmed_peny_lain_detail", "asesmed_alergi_obat",
		"asesmed_alergi_obat_detail", "asesmed_alergi_mknan", "asesmed_alergi_mknan_detail",
		"asesmed_kebiasaan_buruk").Where(&soap.DataMedikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.DataMedikModel{AsesmedGolDarah: req.AsesmedGolDarah, AsesmedTd: req.AsesmedTd, AsesmedTdDetail: req.AsesmedTdDetail, AsesmedPenyJantung: req.AsesmedPenyJantung, AsesmedPenyJantungDetail: req.AsesmedPenyJantungDetail, AsesmedDiabet: req.AsesmedDiabet, AsesmedDiabetDetail: req.AsesmedDiabetDetail, AsesmedHaemop: req.AsesmedHaemop, AsesmedHaemopDetail: req.AsesmedHaemopDetail, AsesmedHepat: req.AsesmedHepat,
		AsesmedHepatDetail: req.AsesmedHepatDetail, AsesmedPenyLain: req.AsesmedPenyLain, AsesmedPenyLainDetail: req.AsesmedPenyLainDetail, AsesmedAlergiObat: req.AsesmedAlergiObat, AsesmedAlergiObatDetail: req.AsesmedAlergiObatDetail, AsesmedAlergiMknan: req.AsesmedAlergiMknan, AsesmedAlergiMknanDetail: req.AsesmedAlergiMknanDetail, AsesmedKebiasaanBuruk: req.AsesmedKebiasaanBuruk,
	}).Scan(&res).Error
	if errs != nil {
		return res, errs
	}
	return res, nil
}

// SAVE DATA MEDIK YANG DIPERLUKAN
func (sr *soapRepository) SaveDataMedik(req dto.ReqDataMedik, kdBagian string) (res soap.DataMedikModel, err error) {
	errs := sr.DB.Select("asesmed_gol_darah", "asesmed_td", "asesmed_td_detail", "asesmed_peny_jantung",
		"asesmed_peny_jantung_detail", "asesmed_diabet", "asesmed_diabet_detail", "asesmed_haemop", "asesmed_haemop_detail", "asesmed_hepat", "asesmed_hepat_detail", "asesmed_peny_lain",
		"asesmed_peny_lain_detail", "asesmed_alergi_obat", "asesmed_alergi_obat_detail", "asesmed_alergi_mknan", "asesmed_alergi_mknan_detail", "asesmed_kebiasaan_buruk").Where(&soap.DataMedikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Save(&soap.DataMedikModel{AsesmedGolDarah: req.AsesmedGolDarah, AsesmedTd: req.AsesmedTd, AsesmedTdDetail: req.AsesmedTdDetail, AsesmedPenyJantung: req.AsesmedPenyJantung, AsesmedPenyJantungDetail: req.AsesmedPenyJantungDetail, AsesmedDiabet: req.AsesmedDiabet, AsesmedDiabetDetail: req.AsesmedDiabetDetail, AsesmedHaemop: req.AsesmedHaemop, AsesmedHaemopDetail: req.AsesmedHaemopDetail, AsesmedHepat: req.AsesmedHepat,
		AsesmedHepatDetail: req.AsesmedHepatDetail, AsesmedPenyLain: req.AsesmedPenyLain, AsesmedPenyLainDetail: req.AsesmedPenyLainDetail, AsesmedAlergiObat: req.AsesmedAlergiObat, AsesmedAlergiObatDetail: req.AsesmedAlergiObatDetail, AsesmedAlergiMknan: req.AsesmedAlergiMknan, AsesmedAlergiMknanDetail: req.AsesmedAlergiMknanDetail, AsesmedKebiasaanBuruk: req.AsesmedKebiasaanBuruk,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) UpdateDataAnamnesa(req dto.ReqSaveAsesmedAnamnesa, kdBagian string) (res soap.AsesmedAnamnesa, err error) {
	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.AsesmedAnamnesa{
		AsesmedKeluhUtama:         req.AsesmedKeluhUtama,
		AsesmedRwytSkrg:           req.AsesmedRwytSkrg,
		AsesmedRwytPenyKlrg:       req.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergiDetail:   req.AsesmedRwytAlergiDetail,
		AsesmedJenpel:             req.AsesmedJenpel,
		AsesmedKhusPemGigi1:       req.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:       req.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:       req.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:       req.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:       req.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail: req.AsesmedKhusPemGigi5Detail,
		AsesmedRwytAlergi:         req.AsesmedRwytAlergi,
		KdBagian:                  kdBagian,
		Noreg:                     req.Noreg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) SaveAsesmedAnamnesa(req dto.ReqSaveAsesmedAnamnesa, kdBagian string) (res soap.AsesmedAnamnesa, err error) {
	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.Noreg}).Save(&soap.AsesmedAnamnesa{
		AsesmedKeluhUtama:         req.AsesmedKeluhUtama,
		AsesmedRwytSkrg:           req.AsesmedRwytSkrg,
		AsesmedRwytPenyKlrg:       req.AsesmedRwytPenyKlrg,
		AsesmedRwytAlergiDetail:   req.AsesmedRwytAlergiDetail,
		AsesmedJenpel:             req.AsesmedJenpel,
		AsesmedKhusPemGigi1:       req.AsesmedKhusPemGigi1,
		AsesmedKhusPemGigi2:       req.AsesmedKhusPemGigi2,
		AsesmedKhusPemGigi3:       req.AsesmedKhusPemGigi3,
		AsesmedKhusPemGigi4:       req.AsesmedKhusPemGigi4,
		AsesmedKhusPemGigi5:       req.AsesmedKhusPemGigi5,
		AsesmedKhusPemGigi5Detail: req.AsesmedKhusPemGigi5Detail,
		AsesmedRwytAlergi:         req.AsesmedRwytAlergi,
		KdBagian:                  kdBagian,
		Noreg:                     req.Noreg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

// Get INTRA OPERASI
func (sr *soapRepository) GetIntraOperasiRepository(noReg string, kdBagian string) (res soap.IntraOperasiModel, err error) {
	errs := sr.DB.Select("obsanslok_khus_post_td", "obsanslok_khus_post_nadi", "obsanslok_khus_post_suhu", "obsanslok_khus_post_rr", "kd_bagian", "noreg").Where(&soap.DataIntraOralModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

// Save Pasca Operasi
func (sr *soapRepository) InsertPascaOperasiRepository(req dto.ReqPascaOperasi, kdBagian string) (res soap.PascaOperasiModel, err error) {
	errs := sr.DB.Where(&soap.PascaOperasiModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
	}).Save(&soap.PascaOperasiModel{
		ObsanslokKhusPostKeadUmum:   req.ObsanslokKhusPostKeadUmum,
		ObsanslokKhusPostKeluhUtama: req.ObsanslokKhusPostKeluhUtama, ObsanslokKhusPostTd: req.ObsanslokKhusPostTd,
		ObsanslokKhusPostNadi: req.ObsanslokKhusPostNadi, ObsanslokKhusPostSuhu: req.ObsanslokKhusPostSuhu,
		ObsanslokKhusPostRr: req.ObsanslokKhusPostRr,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

// UPDATE PASCA OPERASI
func (sr *soapRepository) SavePascaOperasiRepository(req dto.ReqPascaOperasi, kdBagian string) (res soap.PascaOperasiModel, err error) {
	errs := sr.DB.Select("obsanslok_khus_post_kead_umum", "obsanslok_khus_post_keluh_utama", "obsanslok_khus_post_td", "obsanslok_khus_post_nadi", "obsanslok_khus_post_suhu", "obsanslok_khus_post_rr", "kd_bagian", "noreg").Where(&soap.PascaOperasiModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
	}).Updates(&soap.PascaOperasiModel{
		ObsanslokKhusPostKeadUmum:   req.ObsanslokKhusPostKeadUmum,
		ObsanslokKhusPostKeluhUtama: req.ObsanslokKhusPostKeluhUtama, ObsanslokKhusPostTd: req.ObsanslokKhusPostTd,
		ObsanslokKhusPostNadi: req.ObsanslokKhusPostNadi, ObsanslokKhusPostSuhu: req.ObsanslokKhusPostSuhu,
		ObsanslokKhusPostRr: req.ObsanslokKhusPostRr,
	}).Scan(&res).Error
	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) GetPascaOperasi(noReg string, kdBagian string) (res soap.PascaOperasiModel, err error) {
	errs := sr.DB.Select("obsanslok_khus_post_kead_umum", "obsanslok_khus_post_keluh_utama", "obsanslok_khus_post_td", "obsanslok_khus_post_nadi", "obsanslok_khus_post_suhu", "obsanslok_khus_post_rr", "kd_bagian", "noreg").Where(&soap.DataIntraOralModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) UpdateTriasRepository(kdBagian string, req dto.RegTriase) (res soap.TriaseModel, err error) {

	times := time.Now()

	errs := sr.DB.Where(&soap.AsesmedAnamnesa{KdBagian: kdBagian, Noreg: req.NoReg}).Updates(&soap.TriaseModel{
		IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseAkral: req.IgdTriaseAkral,
		// IgdTriaseCahaya: req.IgdTriaseCahaya,
		IgdTriaseGangguanDetail: req.IgdTriaseGangguanDetail, IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), KdBagian: kdBagian, Noreg: req.NoReg,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) InsertTriaseRepository(noReg string, kdBagian string, req dto.RegTriase) (res soap.TriaseModel, err error) {
	times := time.Now()

	data := soap.TriaseModel{
		KdBagian: kdBagian, Noreg: noReg,
		InsertDttm: times.Format("2006-01-02 15:04:05"), IgdTriaseKeluh: req.IgdTriaseKeluh, IgdTriaseAlergi: req.IgdTriaseAlergi, IgdTriaseAlergiDetail: req.IgdTriaseAlergiDetail, IgdTriaseNafas: req.IgdTriaseNafas, IgdTriaseTd: req.IgdTriaseTd, IgdTriaseRr: req.IgdTriaseRr, IgdTriasePupil: req.IgdTriasePupil, IgdTriaseNadi: req.IgdTriaseNadi, IgdTriaseSpo2: req.IgdTriaseSpo2, IgdTriaseSuhu: req.IgdTriaseSuhu, IgdTriaseAkral: req.IgdTriaseAkral,
		IgdTriaseGangguan: req.IgdTriaseGangguan, IgdTriaseGangguanDetail: req.IgdTriaseAlergiDetail, IgdTriaseSkalaNyeri: req.IgdTriaseSkalaNyeri, IgdTriaseSkalaNyeriP: req.IgdTriaseSkalaNyeriP, IgdTriaseSkalaNyeriQ: req.IgdTriaseSkalaNyeriQ, IgdTriaseSkalaNyeriR: req.IgdTriaseSkalaNyeriR, IgdTriaseSkalaNyeriS: req.IgdTriaseSkalaNyeriS, IgdTriaseSkalaNyeriT: req.IgdTriaseSkalaNyeriT, IgdTriaseFlaccWajah: req.IgdTriaseFlaccWajah, IgdTriaseFlaccKaki: req.IgdTriaseFlaccKaki, IgdTriaseFlaccAktifitas: req.IgdTriaseFlaccAktifitas, IgdTriaseFlaccMenangis: req.IgdTriaseFlaccMenangis, IgdTriaseFlaccBersuara: req.IgdTriaseFlaccBersuara, IgdTriaseFlaccTotal: req.IgdTriaseFlaccTotal, IgdTriaseSkalaTriase: req.IgdTriaseSkalaTriase, IgdTriaseFingerTgl: times.Format("2006-01-02"), IgdTriaseFingerUser: req.IgdTriaseFingerUser, IgdTriaseFingerJam: times.Format("15:04:05"),
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, errors.New(result.Error.Error())
	}

	return data, nil
}

func (sr *soapRepository) GetTriaseRepository(noReg string, kdBagian string) (res soap.TriaseModel, err error) {
	errs := sr.DB.Select("igd_triase_alergi", "igd_triase_alergi_detail", "kd_bagian", "noreg").Where(&soap.TriaseModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errs
	}
	return res, nil
}

func (sr *soapRepository) GetAnamnesaIGDRepository(noReg string, kdBagian string) (res soap.AsesmedAwalPasienIGDModel, err error) {
	errs := sr.DB.Select("asesmed_keluh_utama", "asesmed_telaah", "asesmed_jenpel", "kd_bagian", "noreg").Where(&soap.AsesmedAwalPasienIGDModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) GetImageLokalisRepository(noReg string, kdBagian string) (res soap.ImageLokalisModel, err error) {
	errs := sr.DB.Select("asesmed_lokalis_image", "kd_bagian", "noreg").Where(&soap.AsesmedAwalPasienIGDModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) GetImageLokalisMataRepository(noReg string, kdBagian string) (res soap.ImageLokalisModel, err error) {
	errs := sr.DB.Select("asesmed_lokalis_image", "kd_bagian", "noreg").Where(&soap.AsesmedAwalPasienIGDModel{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil
}

func (sr *soapRepository) SaveAnamnesaIGD(kdBagian string, req dto.AnamnesaIGD) (res soap.AsesmedAwalPasienIGDModel, err error) {
	times := time.Now()

	data := soap.AsesmedAwalPasienIGDModel{
		InsertDttm: times.Format("2006-01-02 15:04:05"),
		KdBagian:   kdBagian, Noreg: req.NoReg, AsesmedKeluhUtama: req.AsesmedKeluhUtama, AsesmedTelaah: req.AsesmendTelaah, AsesmedMslhMedis: req.AsesmendMslhMedis, AsesmedRwytSkrg: req.AsesmedRwytSkrg, AsesmedRwytObat: req.AsesmedRwytObat, AsesmedJenpel: req.AsesmedJenpel,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) UpdateAnamnesaIGD(kdBagian string, req dto.AnamnesaIGD) (res soap.AsesmedAwalPasienIGDModel, err error) {

	times := time.Now()

	errs := sr.DB.Where(&soap.AsesmedAwalPasienIGDModel{KdBagian: kdBagian, Noreg: req.NoReg}).Updates(&soap.AsesmedAwalPasienIGDModel{
		InsertDttm:        times.Format("2006-01-02 15:04:05"),
		AsesmedKeluhUtama: req.AsesmedKeluhUtama, AsesmedTelaah: req.AsesmendTelaah, AsesmedMslhMedis: req.AsesmendMslhMedis, AsesmedRwytSkrg: req.AsesmedRwytSkrg, AsesmedRwytObat: req.AsesmedRwytDulu, AsesmedJenpel: req.AsesmedJenpel,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateImageLokalisRepository(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, err error) {

	errs := sr.DB.Where(&soap.ImageLokalisModel{KdBagian: kdBagian, Noreg: noReg}).Updates(&soap.ImageLokalisModel{
		AsesmedLokalisImage: path,
	}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}
	return res, nil

}

func (sr *soapRepository) InsertImageLokalisRepository(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, err error) {
	times := time.Now()

	data := soap.ImageLokalisModel{
		KdBagian: kdBagian, Noreg: noReg,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		AsesmedLokalisImage: path,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) InsertImageToLokasisRepositoryV2(kdBagian string, noReg string, path string, keteranganPerson string, insertUserId string, devicesID string, pelayanan string, tglMasuk string, jamMasuk string) (res soap.ImageLokalisModel, err error) {
	times := time.Now()

	data := soap.ImageLokalisModel{
		KdDpjp:              insertUserId,
		KdBagian:            kdBagian,
		Noreg:               noReg,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		AsesmedLokalisImage: path,
		TglMasuk:            tglMasuk,
		JamMasuk:            jamMasuk,
		KeteranganPerson:    keteranganPerson,
		InsertUserId:        insertUserId, InsertPc: devicesID,
		Pelayanan:           pelayanan,
		AsesmedDokterTtd:    insertUserId,
		AsesmedDokterTtdTgl: times.Format("2006-01-02"),
		AsesmedDokterTtdJam: times.Format("15:04:05"),
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) InsertPemeriksaanFisikRepository(kdBagian string, req dto.RequestSavePemeriksaanFisik) (res soap.PemeriksaanFisikModel, err error) {
	times := time.Now()

	data := soap.PemeriksaanFisikModel{
		KdBagian: kdBagian, Noreg: req.Noreg,
		InsertDttm:          times.Format("2006-01-02 15:04:05"),
		AsesmedPemfisKepala: req.AsesmedPemfisKepala, AsesmedPemfisKepalaDetail: req.AsesmedPemfisKepalaDetail, AsesmedPemfisLeher: req.AsesmedPemfisLeher, AsesmedPemfisLeherDetail: req.AsesmedPemfisLeherDetail, AsesmedPemfisGenetalia: req.AsesmedPemfisGenetalia, AsesmedPemfisGenetaliaDetail: req.AsesmedPemfisGenetaliaDetail, AsesmedPemfisAbdomen: req.AsesmedPemfisAbdomen, AsesmedPemfisAbdomenDetail: req.AsesmedPemfisAbdomenDetail, AsesmedPemfisEkstremitas: req.AsesmedPemfisEkstremitas, AsesmedPemfisEkstremitasDetail: req.AsesmedPemfisEkstremitasDetail, AsesmedPemfisDada: req.AsesmedPemfisDada, AsesmedPemfisDadaDetail: req.AsesmedPemfisDadaDetail, AsesmedPemfisPunggung: req.AsesmedPemfisPunggung, AsesmedPemfisPunggungDetail: req.AsesmedPemfisPunggungDetail,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) InsertAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, err error) {
	times := time.Now()

	data := soap.AsesmedKeperawatanBidan{
		InsertDttm: times.Format("2006-01-02 15:04:05"), KdBagian: kdBagian, Noreg: req.Noreg, AseskepPerolehanInfo: req.AseskepPerolehanInfo, AseskepCaraMasuk: req.AseskepAsalMasuk, AseskepCaraMasukDetail: req.AseskepAsalMasukDetail, AseskepAsalMasuk: req.AseskepAsalMasuk, AseskepAsalMasukDetail: req.AseskepAsalMasukDetail, AseskepBb: req.AseskepBb,
		AseskepTb: req.AseskepTb, AseskepRwytPnykt: req.AseskepRwytPnykt, AseskepRwytObatDetail: req.AseskepRwytObatDetail, AseskepAsesFungsional: req.AseskepAsesFungsional, AseskepRj1: req.AseskepRj1, AseskepRj2: req.AseskepRj2, AseskepHslKajiRj: req.AseskepHslKajiRj, AseskepHslKajiRjTind: req.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.AseskepLamaNyeri, AseskepNyeriMenjalar: req.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.AseskepKualitasNyeri, AseskepNyeriPemicu: req.AseskepNyeriPemicu, AseskepNyeriPengurang: req.AseskepNyeriPengurang, AseskepKehamilan: req.AseskepKehamilan, AseskepKehamilanGravida: req.AseskepKehamilanGravida, AseskepKehamilanPara: req.AseskepKehamilanPara, AseskepKehamilanAbortus: req.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.AseskepKehamilanHpht, AseskepKehamilanTtp: req.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.AseskepKehamilanDjj, AseskepKehamilanVt: req.AseskepKehamilanVt, AseskepDekubitus1: req.AseskepDekubitus1, AseskepDekubitus2: req.AseskepDekubitus2, AseskepDekubitus3: req.AseskepDekubitus3, AseskepDekubitus4: req.AseskepDekubitus4, AseskepDekubitusAnak: req.AseskepDekubitusAnak, AseskepPulangKondisi: req.AseskepPulangKondisi, AseskepPulangTransportasi: req.AseskepPulangTransportasi, AseskepPulangPendidikan: req.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.AseskepPulangPendidikanDetail,
	}

	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}
	return data, nil
}

func (sr *soapRepository) UpdateAsesmenKeperawatanBidanRepository(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, err error) {

	errs := sr.DB.Where(soap.AsesmedKeperawatanBidan{Noreg: req.Noreg, KdBagian: kdBagian}).Updates(
		&soap.AsesmedKeperawatanBidan{
			KdBagian: kdBagian, Noreg: req.Noreg, AseskepPerolehanInfo: req.AseskepPerolehanInfo, AseskepCaraMasuk: req.AseskepCaraMasuk, AseskepCaraMasukDetail: req.AseskepAsalMasukDetail, AseskepAsalMasuk: req.AseskepAsalMasuk, AseskepAsalMasukDetail: req.AseskepAsalMasukDetail, AseskepBb: req.AseskepBb, AseskepTb: req.AseskepTb, AseskepRwytPnykt: req.AseskepRwytPnykt, AseskepRwytObatDetail: req.AseskepRwytObatDetail, AseskepAsesFungsional: req.AseskepAsesFungsional, AseskepRj1: req.AseskepRj1, AseskepRj2: req.AseskepRj2, AseskepHslKajiRj: req.AseskepHslKajiRj, AseskepHslKajiRjTind: req.AseskepHslKajiRjTind, AseskepSkalaNyeri: req.AseskepSkalaNyeri, AseskepFrekuensiNyeri: req.AseskepFrekuensiNyeri, AseskepLamaNyeri: req.AseskepLamaNyeri, AseskepNyeriMenjalar: req.AseskepNyeriMenjalar, AseskepNyeriMenjalarDetail: req.AseskepNyeriMenjalarDetail, AseskepKualitasNyeri: req.AseskepKualitasNyeri, AseskepNyeriPemicu: req.AseskepNyeriPemicu, AseskepNyeriPengurang: req.AseskepNyeriPengurang, AseskepKehamilan: req.AseskepKehamilan, AseskepKehamilanGravida: req.AseskepKehamilanGravida, AseskepKehamilanPara: req.AseskepKehamilanPara, AseskepKehamilanAbortus: req.AseskepKehamilanAbortus, AseskepKehamilanHpht: req.AseskepKehamilanHpht, AseskepKehamilanTtp: req.AseskepKehamilanTtp, AseskepKehamilanLeopold1: req.AseskepKehamilanLeopold1, AseskepKehamilanLeopold2: req.AseskepKehamilanLeopold2, AseskepKehamilanLeopold3: req.AseskepKehamilanLeopold3, AseskepKehamilanLeopold4: req.AseskepKehamilanLeopold4, AseskepKehamilanDjj: req.AseskepKehamilanDjj, AseskepKehamilanVt: req.AseskepKehamilanVt, AseskepDekubitus1: req.AseskepDekubitus1, AseskepDekubitus2: req.AseskepDekubitus2, AseskepDekubitus3: req.AseskepDekubitus3, AseskepDekubitus4: req.AseskepDekubitus4, AseskepDekubitusAnak: req.AseskepDekubitusAnak, AseskepPulangKondisi: req.AseskepPulangKondisi, AseskepPulangTransportasi: req.AseskepPulangTransportasi, AseskepPulangPendidikan: req.AseskepPulangPendidikan, AseskepPulangPendidikanDetail: req.AseskepPulangPendidikanDetail,
		}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdatePemeriksaanFisikRepository(kdBagian string, req dto.RequestSavePemeriksaanFisik) (res soap.PemeriksaanFisikModel, err error) {
	times := time.Now()

	errs := sr.DB.Where(&soap.PemeriksaanFisikModel{KdBagian: kdBagian, Noreg: req.Noreg}).Updates(&soap.PemeriksaanFisikModel{InsertDttm: times.Format("2006-01-02 15:04:05"), AsesmedPemfisKepala: req.AsesmedPemfisKepala, AsesmedPemfisKepalaDetail: req.AsesmedPemfisKepalaDetail, AsesmedPemfisLeher: req.AsesmedPemfisLeher, AsesmedPemfisLeherDetail: req.AsesmedPemfisLeherDetail, AsesmedPemfisGenetalia: req.AsesmedPemfisGenetalia, AsesmedPemfisGenetaliaDetail: req.AsesmedPemfisGenetaliaDetail, AsesmedPemfisAbdomen: req.AsesmedPemfisAbdomen, AsesmedPemfisAbdomenDetail: req.AsesmedPemfisAbdomenDetail, AsesmedPemfisEkstremitas: req.AsesmedPemfisEkstremitas, AsesmedPemfisEkstremitasDetail: req.AsesmedPemfisEkstremitasDetail, AsesmedPemfisDada: req.AsesmedPemfisDada, AsesmedPemfisDadaDetail: req.AsesmedPemfisDadaDetail, AsesmedPemfisPunggung: req.AsesmedPemfisPunggung, AsesmedPemfisPunggungDetail: req.AsesmedPemfisPunggungDetail}).Scan(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) GetTotalKTaripRepository(namaGrup string) (res soap.KtaripTotal, err error) {
	query := `SELECT  a.name_grup , SUM(b.tarip) AS tarip_kelas FROM vicore_lib.kprocedure AS a LEFT JOIN vicore_lib.ktaripsimrs AS b ON a.kode=b.kode AND a.kd_bag=b.kd_bagian WHERE a.kd_bag='LAB001' AND b.kd_debitur='PRIBADI' AND b.kd_kelas='00' AND a.name_grup=? ORDER BY a.urut ASC`

	result := sr.DB.Raw(query, namaGrup).Scan(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diupdate", result.Error.Error())
		return res, errors.New(message)
	}

	return res, nil
}

func (sr *soapRepository) GetPemeriksaanLaborRepository(namaGrup string) (res []soap.PemeriksaanLaborModel, err error) {
	query := `SELECT kelompok, satuan, normal, urut, name_grup, deskripsi AS pemeriksaan, kode  FROM vicore_lib.kprocedure WHERE name_grup=? ORDER BY urut asc`

	result := sr.DB.Raw(query, namaGrup).Scan(&res)

	if result.Error != nil {
		return res, result.Error
	}

	return res, nil
}

func (sr *soapRepository) GetAsesmedKeperawatanBidanRepository(noReg string, kdBagian string) (res soap.AsesmedKeperawatanBidan, err error) {

	errs := sr.DB.Select("insert_dttm", "kd_bagian", "noreg", "aseskep_perolehan_info", "aseskep_cara_masuk", "aseskep_cara_masuk_detail", "aseskep_asal_masuk", "aseskep_asal_masuk_detail", "aseskep_bb", "aseskep_tb", "aseskep_rwyt_pnykt", "aseskep_rwyt_obat_detail", "aseskep_ases_fungsional", "aseskep_rj1", "aseskep_rj2", "aseskep_hsl_kaji_rj", "aseskep_hsl_kaji_rj_tind", "aseskep_skala_nyeri", "aseskep_frekuensi_nyeri", "aseskep_lama_nyeri", "aseskep_nyeri_menjalar", "aseskep_nyeri_menjalar_detail", "aseskep_kualitas_nyeri", "aseskep_nyeri_pemicu", "aseskep_nyeri_pengurang", "aseskep_kehamilan", "aseskep_kehamilan_gravida", "aseskep_kehamilan_para", "aseskep_kehamilan_abortus", "aseskep_kehamilan_hpht", "aseskep_kehamilan_ttp", "aseskep_kehamilan_leopold1", "aseskep_kehamilan_leopold2", "aseskep_kehamilan_leopold3", "aseskep_kehamilan_leopold4", "aseskep_kehamilan_djj", "aseskep_kehamilan_vt", "aseskep_dekubitus1", "aseskep_dekubitus2", "aseskep_dekubitus3", "aseskep_dekubitus4", "aseskep_dekubitus_anak", "aseskep_pulang_kondisi", "aseskep_pulang_transportasi", "aseskep_pulang_pendidikan", "aseskep_pulang_pendidikan_detail").Where(&soap.AsesmedKeperawatanBidan{
		Noreg: noReg, KdBagian: kdBagian,
	}).Find(&res).Error

	if errs != nil {
		return res, errors.New(errs.Error())
	}

	return res, nil
}

func (sr *soapRepository) SaveDcpptSoapPasien(data soap.DcpptSoapPasienModel) (res soap.DcpptSoapPasienModel, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) OnUpdateDcpptSoapPasien(data soap.DcpptSoapPasienModel, kdBagian string, req dto.ReqOnSaveAsesmenIGD) (res soap.DcpptSoapPasienModel, err error) {

	err12 := sr.DB.Where(&soap.DcpptSoapPasienModel{
		KdBagian: kdBagian,
		Noreg:    req.Noreg,
	}).Updates(&data).Scan(&data).Error

	if err12 != nil {
		return res, errors.New(err12.Error())
	}

	return data, nil
}

func (sr *soapRepository) SaveDcpptSoapDokter(data soap.DcpptSoapDokter) (res soap.DcpptSoapDokter, err error) {
	result := sr.DB.Create(&data)

	if result.Error != nil {
		return data, result.Error
	}

	return data, nil
}

func (sr *soapRepository) UpdateInformasiKeluhan(noReg string, kdBagian string, req dto.ReqAsesmenInformasiKeluhanIGD) (res soap.DcpptSoapPasienModel, err error) {
	var hasilKaji []string

	if len(req.AseskepHslKajiRj) > 3 {
		sr.Logging.Info("HASIL KAJI DI ISI")
		hasilKaji = strings.SplitAfter(req.AseskepHslKajiRj, "-")
	} else {
		hasilKaji = append(hasilKaji, "")
		hasilKaji = append(hasilKaji, "")
		sr.Logging.Info("HASIL KAJI KOSONG")
	}

	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_perolehan_info=?, aseskep_perolehan_info_detail=?, aseskep_cara_masuk=?, aseskep_cara_masuk_detail=?,aseskep_asal_masuk=?, aseskep_asal_masuk_detail=?, aseskep_bb=?, aseskep_tb=?, aseskep_rwyt_pnykt=?, aseskep_rwyt_obat=?, aseskep_ases_fungsional=?, aseskep_rj1=?, aseskep_rj2=?, aseskep_hsl_kaji_rj=?, aseskep_hsl_kaji_rj_tind=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.AseskepPerolehanInfo, req.AseskepPerolehanInfoDetail, req.AseskepCaraMasuk, req.AseskepCaraMasukDetail,
		req.AseskepAsalMasuk, req.AseskepAsalMasukDetail, req.Aseskepbb, req.AseskepTb, req.AseskepRwytPnykt, req.AseskepRwytObat,
		req.AseskepAsesFungsional, req.AseskepRj1, req.AseskepRj2, hasilKaji[0], hasilKaji[1], noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateResikoJatuhRepository(noReg string, kdBagian string, req dto.ReqResikoJatuhGoUpAndGoTest) (res soap.ResikoJatuhGetUpGoTest, err error) {
	var hasilKaji []string

	if len(req.Tindakan) > 3 {
		sr.Logging.Info("HASIL KAJI DI ISI")
		hasilKaji = strings.SplitAfter(req.Tindakan, "-")
	} else {
		hasilKaji = append(hasilKaji, "")
		hasilKaji = append(hasilKaji, "")
		sr.Logging.Info("HASIL KAJI KOSONG")
	}

	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_rj1=?, aseskep_rj2=?, aseskep_hsl_kaji_rj=?, aseskep_hsl_kaji_rj_tind=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.ResikoJatuh1, req.ResikoJatuh2, hasilKaji[0], hasilKaji[1], noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateSkriningResikoDekubitus(noReg string, kdBagian string, req dto.ReqSkriningDekubitusIGD) (res soap.DcpptSoapPasienModel, err error) {
	// AMBIL HASIL KAJI // PARSING HASIL TINDAKAN

	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_dekubitus1=?,aseskep_dekubitus2=?,aseskep_dekubitus3=?,aseskep_dekubitus4=?,aseskep_dekubitus_anak=?  WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.Dekubitus1, req.Dekubitus2, req.Dekubitus3, req.Dekubitus4, req.DekubitusAnak, noReg, kdBagian).Scan(&res)

	sr.Logging.Info(query)
	sr.Logging.Info(result)
	sr.Logging.Info("noreg " + noReg)
	sr.Logging.Info("kdbagian " + kdBagian)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateRiwayatKehamilanRepository(noReg string, kdBagian string, req dto.RequestKehamilanIGD) (res soap.DcpptSoapPasienModel, err error) {

	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_kehamilan=?, aseskep_kehamilan_gravida=?,  aseskep_kehamilan_para=?, aseskep_kehamilan_abortus=?,  aseskep_kehamilan_hpht=?, aseskep_kehamilan_ttp=?, aseskep_kehamilan_leopold1=?,aseskep_kehamilan_leopold2=?, aseskep_kehamilan_leopold3=?, aseskep_kehamilan_leopold4=?,	aseskep_kehamilan_djj=?, aseskep_kehamilan_vt=?  WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.Kehamilan, req.KehamilanGravida, req.KehamilanPara, req.KehamilanAbortus, req.KehamilanHpht,
		req.KehamilanTtp, req.KehamilanLeopold1, req.KehamilanLeopold2, req.KehamilanLeopold3, req.KehamilanLeopold4, req.KehamilanDjj, req.KehamilanVt, noReg, kdBagian).Scan(&res)

	sr.Logging.Info(query)
	sr.Logging.Info(result)
	sr.Logging.Info("noreg " + noReg)
	sr.Logging.Info("kdbagian " + kdBagian)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil

}

func (sr *soapRepository) UpdateSkriningNyeriRepository(noReg string, kdBagian string, req dto.RequestSkriningNyeriIGD) (res soap.DcpptSoapPasienModel, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_skala_nyeri=?, aseskep_frekuensi_nyeri=?, aseskep_lama_nyeri=?, aseskep_nyeri_menjalar=?, aseskep_nyeri_menjalar_detail=?, aseskep_kualitas_nyeri=?,aseskep_nyeri_pemicu=?, aseskep_nyeri_pengurang=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.SkalaNyeri, req.FrekuensiNyeri, req.LamaNyeri, req.NyeriMenjalar, req.NyeriMenjalarDetail,
		req.Kualitasnyeri, req.NyeriPemicu, req.NyeriPengurang, noReg, kdBagian).Scan(&res)

	sr.Logging.Info(query)
	sr.Logging.Info(result)
	sr.Logging.Info("noreg " + noReg)
	sr.Logging.Info("kdbagian " + kdBagian)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateTindakLanjutRepository(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD) (res soap.DcpptSoapPasienModel, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_pulang1=?, aseskep_pulang1_detail=?, aseskep_pulang2=?, aseskep_pulang2_detail=?, aseskep_pulang3=?, aseskep_pulang3_detail=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.AseskepPulang1, req.AseskepPulang1Detail, req.AseskepPulang2, req.AseskepPulang2Detail, req.AseskepPulang3, req.AseskepPulang3Detail, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateTindakLanjutRepositoryDipulangkan(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD, jam string) (res soap.DcpptSoapPasienModel, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_pulang1=?, aseskep_pulang1_detail=?, aseskep_pulang2=?, aseskep_pulang2_detail=?, aseskep_pulang3=?, aseskep_pulang3_detail=?, jam_check_out=?, jam_check_in_ranap=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.AseskepPulang1, req.AseskepPulang1Detail, req.AseskepPulang2, req.AseskepPulang2Detail, req.AseskepPulang3, req.AseskepPulang3Detail, jam, jam, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}
func (sr *soapRepository) UpdateTindakLanjutRepositoryDirawatInap(noReg string, kdBagian string, req dto.RequestInputTindakLanjutIGD, jam string) (res soap.DcpptSoapPasienModel, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_pulang1=?, aseskep_pulang1_detail=?, aseskep_pulang2=?, aseskep_pulang2_detail=?, aseskep_pulang3=?, aseskep_pulang3_detail=?, jam_check_in_ranap=? WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, req.AseskepPulang1, req.AseskepPulang1Detail, req.AseskepPulang2, req.AseskepPulang2Detail, req.AseskepPulang3, req.AseskepPulang3Detail, jam, noReg, kdBagian).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateAsesmenDokterRepository(noReg string, kdBagian string, userID string, req dto.RequestAsesmenDokter) (res soap.DcpptSoapDokter, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_dokter SET asesmed_jenpel=?, asesmed_keluh_utama=?, asesmed_telaah=?, asesmed_mslh_medis=?, asesmed_rwyt_skrg=?,  asesmed_rwyt_obat=?, asesmed_anamnesa=? WHERE noreg=? AND kd_bagian=? AND insert_user_id=?;`

	result := sr.DB.Raw(query, req.AsesmedJenpel, req.AsesmedKeluhUtama, req.AsesmedTelaah, req.AsesmedMslhMedis, req.AsesmedRwytSkrg,
		req.AsesmedRwytObat, req.AsesmedAnamnesa, noReg, kdBagian, userID).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) UpdateKeadaanUmumBangsalRepository(noReg string, kdBagian string, userID string, req rmeDto.ReqKeadaanUmumBangsal) (res soap.DcpptSoapPasienModel, err error) {
	query := `UPDATE vicore_rme.dcppt_soap_pasien SET aseskep_keadaan_umum=?, aseskep_kesadaran=?, aseskep_kesadaran_detail=? WHERE noreg=? AND kd_bagian=? AND insert_user_id=?;`

	result := sr.DB.Raw(query, req.KeadaanUmum, req.Kesadaran, req.KesadaranDetail, noReg, kdBagian, userID).Scan(&res)

	if result.Error != nil {
		return res, errors.New(result.Error.Error())
	}

	return res, nil
}

func (sr *soapRepository) OnDeleteTindakanICD9Repository(noReg string, index string, kdBagian string) (err error) {

	var datas soap.DiagnosaModelOne

	value := "asesmed_pros" + index

	query := `UPDATE vicore_rme.dcppt_soap_dokter  SET ` + value + ` =""  WHERE noreg=? AND kd_bagian=?;`

	result := sr.DB.Raw(query, noReg, kdBagian).Scan(&datas)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data gagal diubah", result.Error.Error())
		return errors.New(message)
	}

	return nil
}
