package soap

import (
	"hms_api/modules/rme"
	"time"
)

type (
	/*
		Odontogram Struct
	*/
	Odontogram struct {
		KdBagian string `json:"kdBagian"`
		Noreg    string `json:"noreg"`
	}

	ResAssKebEdukasi struct {
		Name string `json:"name"`
	}

	Odontograms struct {
		Number     int    `json:"number"`
		Assets     string `json:"assets"`
		Keterangan string `json:"keterangan"`
	}

	DRegister struct {
		Tanggal string
		Jam     string
	}

	// ======================== TRIASE RIWAYAT ALERGI DARI TABLE SOAP DOKTER
	TriaseRiwayatAlergi struct {
		InsertDttm        string `json:"insert_dttm"`
		UpdDttm           string `json:"update_dttm"`
		AsesmedKeluhUtama string `json:"keluhan_utama"`
		Noreg             string `json:"noreg"`
		KdBagian          string `json:"kd_bagian"`
		TglMasuk          string `json:"tgl_masuk"`
		JamMasuk          string `json:"jam_masuk"`
		// TAMBAHKAN DATA USER ID
		// TAMBAHKAN DATA PERSON
		KeteranganPerson string `json:"person"`
		InsertUserId     string `json:"user_id"`
		InsertPc         string `json:"device_id"`
		Pelayanan        string `json:"pelayanan"`
	}

	KeluhanUtamaPerawatIGD struct {
		InsertDttm string `json:"insert_dttm"`
		UpdDttm    string `json:"update_dttm"`
		AseskepKel string `json:"keluhan_utama"`
		Noreg      string `json:"noreg"`
		KdBagian   string `json:"kd_bagian"`
		TglMasuk   string `json:"tgl_masuk"`
		// TAMBAHKAN DATA USER ID
		// TAMBAHKAN DATA PERSON
		KeteranganPerson string `json:"person"`
		InsertUserId     string `json:"user_id"`
		InsertPc         string `json:"device_id"`
		Pelayanan        string `json:"pelayanan"`
	}

	Odontograms2 struct {
		Number     int    `json:"number"`
		Keterangan string `json:"keterangan"`
	}

	KebEdukasi struct {
		AseseduKe1            string
		AseseduKe2            string
		AseseduKe3            string
		AseseduKe4            string
		AseseduKe5            string
		AseseduKe6            string
		AseseduKe7            string
		AseseduKe8            string
		AseseduKe9            string
		AseseduKe10           string
		AseseduKe11           string
		AseseduKe11Detail     string
		AseseduTu1            string
		AseseduTu2            string
		AseseduKb1            string
		AseseduKb2            string
		AseseduKb3            string
		AseseduMb1            string
		AseseduMb2            string
		AseseduMb3            string
		AseseduNp1            string
		AseseduNp2            string
		AseseduNp3            string
		AseseduNp4            string
		AseseduNp5            string
		AseseduNp6            string
		AseseduNp6Detail      string
		AseseduHa1            string
		AseseduHa2            string
		AseseduHa3            string
		AseseduHa4            string
		AseseduHa5            string
		AseseduHa6            string
		AseseduHa7            string
		AseseduHa8            string
		AseseduHa9            string
		AseseduHa9Detail      string
		AseseduIn1            string
		AseseduIn2            string
		AseseduIn3            string
		AseseduIn4            string
		AseseduIn5            string
		AseseduMet1           string
		AseseduMet2           string
		AseseduMet3           string
		AseseduMet4           string
		AseseduMet4Detail     string
		AseseduHasil1         string
		AseseduHasil2         string
		AseseduRencanaEdukasi string
		AseseduTgl            time.Time
		AseseduJam            string
		AseseduUser           string
	}

	AssesPemeriksaanGigi struct {
		AsesmedKhusPemGigi1       string `json:"pemGigi1"`
		AsesmedKhusPemGigi2       string `json:"pemGigi2"`
		AsesmedKhusPemGigi3       string `json:"pemGigi3"`
		AsesmedKhusPemGigi4       string `json:"pemGigi4"`
		AsesmedKhusPemGigi5       string `json:"pemGigi5"`
		AsesmedKhusPemGigi5Detail string `json:"pemGigi5Detail"`
	}

	AssesRiwayat struct {
		AsesmedRwytSkrg         string `json:"riwayatSekarang"`
		AsesmedRwytDulu         string `json:"riwayatDulu"`
		AsesmedRwytObat         string `json:"riwayatObat"`
		AsesmedRwytPenyKlrg     string `json:"riwayatPenyakitKlrg"`
		AsesmedRwytAlergi       string `json:"riwayatAlergi"`
		AsesmedRwytAlergiDetail string `json:"riwayatAlergiDetail"`
	}

	DataMedik struct {
		AsesmedGolDarah          string `json:"golDarah"`
		AsesmedTd                string `json:"tdDarah"`
		AsesmedTdDetail          string `json:"tdDarahDetail"`
		AsesmedPenyJantung       string `json:"penyJantung"`
		AsesmedPenyJantungDetail string `json:"penyJantungDetail"`
		AsesmedDiabet            string `json:"diabet"`
		AsesmedDiabetDetail      string `json:"diabetDetail"`
		AsesmedHaemop            string `json:"haemop"`
		AsesmedHaemopDetail      string `json:"haemopDetail"`
		AsesmedHepat             string `json:"hepatitis"`
		AsesmedHepatDetail       string `json:"hepatitisDetail"`
		AsesmedPenyLain          string `json:"penyLain"`
		AsesmedPenyLainDetail    string `json:"penyLainDetail"`
		AsesmedAlergiObat        string `json:"alergiObat"`
		AsesmedAlergiObatDetail  string `json:"alergiObatDetail"`
		AsesmedAlergiMknan       string `json:"alergiMakanan"`
		AsesmedAlergiMknanDetail string `json:"alergiMakananDetail"`
		AsesmedKebiasaanBuruk    string `json:"kebiasaanBuruk"`
	}

	IntraOral struct {
		// Data Intra Oral
		AsesmedKhusOkAnterior        string `json:"aterior"`
		AsesmedKhusOkPosterior       string `json:"posterior"`
		AsesmedKhusOkMolar           string `json:"hubMoral"`
		AsesmedKhusPalatum           string `json:"palatum"`
		AsesmedKhusTorusP            string `json:"torusPalatinus"`
		AsesmedKhusTorusM            string `json:"torusMandibulari"`
		AsesmedKhusTorusMDetail      string `json:"torusMandibulariDetail"`
		AsesmedKhusSuperTeeth        string `json:"superTeeth"`
		AsesmedKhusSuperTeethDetail  string `json:"superTeethDetail"`
		AsesmedKhusDiastema          string `json:"diastema"`
		AsesmedKhusDiastemaDetail    string `json:"diastemaDetail"`
		AsesmedKhusGigiAnomali       string `json:"gigiAnomali"`
		AsesmedKhusGigiAnomaliDetail string `json:"gigiAnomaliDetail"`
		AsesmedKhusOralLain          string `json:"lainnya"`
	}

	DcpptOdontogram struct {
		InsertDttm        string
		Noreg             string
		AsesmedJenpel     string
		AsesmedKeluhUtama string
		InsertUserID      string
		KdBagian          string

		// RIWAYAT
		AsesmedRwytSkrg         string
		AsesmedRwytDulu         string
		AsesmedRwytObat         string
		AsesmedRwytPenyKlrg     string
		AsesmedRwytAlergi       string
		AsesmedRwytAlergiDetail string

		// Pemeriksaan Gigi
		AsesmedKhusPemGigi1       string
		AsesmedKhusPemGigi2       string
		AsesmedKhusPemGigi3       string
		AsesmedKhusPemGigi4       string
		AsesmedKhusPemGigi5       string
		AsesmedKhusPemGigi5Detail string

		// Data medik yang perlu diperhatikan
		AsesmedGolDarah          string
		AsesmedTd                string
		AsesmedTdDetail          string
		AsesmedPenyJantung       string
		AsesmedPenyJantungDetail string
		AsesmedDiabet            string
		AsesmedDiabetDetail      string
		AsesmedHaemop            string
		AsesmedHaemopDetail      string
		AsesmedHepat             string
		AsesmedHepatDetail       string
		AsesmedPenyLain          string
		AsesmedPenyLainDetail    string
		AsesmedAlergiObat        string
		AsesmedAlergiObatDetail  string
		AsesmedAlergiMknan       string
		AsesmedAlergiMknanDetail string
		AsesmedKebiasaanBuruk    string

		// Data Intra Oral
		AsesmedKhusOkAnterior        string
		AsesmedKhusOkPosterior       string
		AsesmedKhusOkMolar           string
		AsesmedKhusPalatum           string
		AsesmedKhusTorusP            string
		AsesmedKhusTorusM            string
		AsesmedKhusTorusMDetail      string
		AsesmedKhusSuperTeeth        string
		AsesmedKhusSuperTeethDetail  string
		AsesmedKhusDiastema          string
		AsesmedKhusDiastemaDetail    string
		AsesmedKhusGigiAnomali       string
		AsesmedKhusGigiAnomaliDetail string
		AsesmedKhusOralLain          string

		// ODONTOGRAM
		// AsesmedKhusOdontImages string
		AsesmedKhusOdont_11     string
		AsesmedKhusOdont_12     string
		AsesmedKhusOdont_13     string
		AsesmedKhusOdont_14     string
		AsesmedKhusOdont_15     string
		AsesmedKhusOdont_16     string
		AsesmedKhusOdont_17     string
		AsesmedKhusOdont_18     string
		AsesmedKhusOdont_21     string
		AsesmedKhusOdont_22     string
		AsesmedKhusOdont_23     string
		AsesmedKhusOdont_24     string
		AsesmedKhusOdont_25     string
		AsesmedKhusOdont_26     string
		AsesmedKhusOdont_27     string
		AsesmedKhusOdont_28     string
		AsesmedKhusOdont_31     string
		AsesmedKhusOdont_32     string
		AsesmedKhusOdont_33     string
		AsesmedKhusOdont_34     string
		AsesmedKhusOdont_35     string
		AsesmedKhusOdont_36     string
		AsesmedKhusOdont_37     string
		AsesmedKhusOdont_38     string
		AsesmedKhusOdont_41     string
		AsesmedKhusOdont_42     string
		AsesmedKhusOdont_43     string
		AsesmedKhusOdont_44     string
		AsesmedKhusOdont_45     string
		AsesmedKhusOdont_46     string
		AsesmedKhusOdont_47     string
		AsesmedKhusOdont_48     string
		AsesmedKhusOdont_51     string
		AsesmedKhusOdont_52     string
		AsesmedKhusOdont_53     string
		AsesmedKhusOdont_54     string
		AsesmedKhusOdont_55     string
		AsesmedKhusOdont_61     string
		AsesmedKhusOdont_62     string
		AsesmedKhusOdont_63     string
		AsesmedKhusOdont_64     string
		AsesmedKhusOdont_65     string
		AsesmedKhusOdont_81     string
		AsesmedKhusOdont_82     string
		AsesmedKhusOdont_83     string
		AsesmedKhusOdont_84     string
		AsesmedKhusOdont_85     string
		AsesmedKhusOdont_71     string
		AsesmedKhusOdont_72     string
		AsesmedKhusOdont_73     string
		AsesmedKhusOdont_74     string
		AsesmedKhusOdont_75     string
		AsesmedKhusGigiOdontPng string
	}

	DcpptSoap struct {
		KdBagian string `json:"kdBagian"`
		Noreg    string `json:"noreg"`
	}

	// ================================ DCPPT SOAP DOKTER  ================================= //
	DcpptSoapDokter struct {
		InsertDttm       string
		UpdDttm          string
		KeteranganPerson string
		InsertUserId     string
		InsertPc         string
		TglMasuk         string
		JamMasuk         string

		// TglKeluar        string
		Pelayanan     string
		KdBagian      string `json:"kdBagian"`
		Noreg         string `json:"noreg"`
		KdDpjp        string
		Subjectif     string
		Asesmen       string
		Plan          string
		PpaPascaBedah string

		// ======================================== SOAP DOKTER
		// IgdTriaseSkalaNyeri     int    `json:"nyeri"`
		// IgdTriaseSkalaNyeriP    string `json:"nyeri_p"`
		// IgdTriaseSkalaNyeriQ    string `json:"nyeri_q"`
		// IgdTriaseSkalaNyeriR    string `json:"nyeri_r"`
		// IgdTriaseSkalaNyeriS    string `json:"nyeri_s"`
		// IgdTriaseSkalaNyeriT    string `json:"nyeri_t"`
		// IgdTriaseFlaccWajah     int    `json:"wajah"`
		// IgdTriaseFlaccKaki      int    `json:"kaki"`
		// IgdTriaseFlaccAktifitas int    `json:"aktifitas"`
		// IgdTriaseFlaccMenangis  int    `json:"menangis"`
		// IgdTriaseFlaccBersuara  int    `json:"bersuara"`
		// IgdTriaseFlaccTotal     int    `json:"total"`
		// IgdTriaseSkalaTriase    int    `json:"triase"`

		// ============================================= TAMBAHKAN DATA SESUAI KEBUTUHAN
		// AsesmedDokterTtd        string
		AsesmedDokterTtdTgl string
		AsesmedDokterTtdJam string
		AsesmedJenpel       string
		AsesmedKeluhUtama   string
		// AsesmedKeluhTambahan    string
		AsesmedTelaah    string
		AsesmedMslhMedis string
		// AsesmedRwytSkrg         string
		// AsesmedRwytDulu         string
		// AsesmedRwytObat         string
		// AsesmedRwytPenyKlrg     string
		// AsesmedRwytAlergi       string
		// AsesmedRwytAlergiDetail string
		// AsesmedAnamnesa         string
		// ============================================= TAMBAHKAN DATA SESUAI KEBUTUHAN
	}
	// ================================ DCPPT SOAP DOKTER  ================================= //

	Anatomi struct {
		AnatomiId  int    `json:"id"`
		Dpjp       string `json:"dpjp"`
		Norm       string `json:"norm"`
		Nama       string `json:"nama"`
		Keterangan string `json:"keterangan"`
		UrlImage   string `json:"urlImage"`
	}

	AssementRawatJalanPerawat struct {
		InsertDttm                  string
		KdBagian                    string `json:"kdBagian"`
		Noreg                       string `json:"noreg"`
		AseskepKel                  string `json:"asseskepKel"`
		AseskepRwytPnykt            string `json:"AssesKepRwytPenyakit"`
		AseskepRwytObat             string `json:"AssesKepRwytObat"`
		AseskepRwytObatDetail       string `json:"AssesKepRwytObatDetail"`
		AseskepTd                   string `json:"AssesKepTd"`
		AseskepNadi                 string `json:"AssesNadi"`
		AseskepSuhu                 string `json:"AssesSuhu"`
		AseskepRr                   string `json:"AssesRr"`
		AseskepBb                   string `json:"AssesBb"`
		AseskepTb                   string `json:"AssesTb"`
		AseskepAsesFungsional       string `json:"AssesAsesFungsional"`
		AseskepAsesFungsionalDetail string `json:"AssesAsesFungsionalDetail"`
		AseskepRj1                  string `json:"AseskepRJ1"`
		AseskepRj2                  string `json:"AseskepRJ2"`
		AseskepHslKajiRj            string `json:"AseskepHslKajiRj"`
		AseskepHslKajiRjTind        string `json:"AseskepHslKajiRjTind"`
		AseskepAsesNyeri            string `json:"AseskepAsesNyeri"`
		AseskepPsiko                string `json:"AseskepPsiko"`
		AseskepPsikoDetail          string `json:"AseskepPsikoDetail"`
		AseskepPulang1              string `json:"AseskepPulang1"`
		AseskepPulang1Detail        string `json:"AseskepPulang1Detail"`
		AseskepPulang2              string `json:"AseskepPulang2"`
		AseskepPulang2Detail        string `json:"AseskepPulang2Detail"`
		AseskepPulang3              string `json:"AseskepPulang3"`
		AseskepPulang3Detail        string `json:"AseskepPulang3Detail"`
		AseskepMslhKprwtan          string `json:"AseskepMslhKprwtan"`
		AseskepRencKprwtan          string `json:"AseskepRencKprwtan"`
	}

	Anamnesa struct {
		Keluhan          string `json:"keluhUtama"`
		Jenpel           string `json:"jenisPelayanan"`
		RiwayatSekarang  string `json:"rwtSekarang"`
		RiwayatDulu      string `json:"rwtDulu"`
		RiwayatObat      string `json:"rwtObat"`
		PenyakitKeluarga string `json:"rwtPenyKeluarga"`
		Alergi           string `json:"rwtPenyAlergi"`
		AlergiDetail     string `json:"rwtPenyAlergiDetail"`
		Gigi1            string `json:"khusPemGigi1"`
		Gigi2            string `json:"khusPemGigi2"`
		Gigi3            string `json:"khusPemGigi3"`
		Gigi4            string `json:"khusPemGigi4"`
		Gigi5            string `json:"khusPemGigi5"`
		GigiDetail       string `json:"khusPemGigi5Detail"`
	}

	DiagnosaResponse struct {
		Diagnosa    string `json:"diagnosa"`
		Description string `json:"description"`
		Type        string `json:"type"`
		Table       string `json:"table"`
	}

	TindakanResponse struct {
		// Code        string `json:"code"`
		Code2       string `json:"kode2"`
		Description string `json:"description"`
		// Class       string `json:"class"`
	}

	DiagnosaBandingResponse struct {
		Diagnosa    string `json:"diagnosa"`
		Description string `json:"description"`
	}

	DiagnosaModelOne struct {
		TglMasuk            string `json:"tgl_masuk"`
		JamMasuk            string `json:"jam_masuk"`
		InsertDttm          string `json:"insert_dttm"`
		AsesmedDiagp        string `json:"P"`
		AsesmedDiags1       string `json:"S1"`
		AsesmedDiags2       string `json:"S2"`
		AsesmedDiags3       string `json:"S3"`
		AsesmedDiags4       string `json:"S4"`
		AsesmedDiags5       string `json:"S5"`
		AsesmedDiags6       string `json:"S6"`
		KdBagian            string `json:"kdBagian"`
		Noreg               string `json:"noReg"`
		KdDpjp              string `json:"kd_dpjp"`
		KeteranganPerson    string `json:"person"`
		InsertUserId        string `json:"user_id"`
		InsertPc            string `json:"device_id"`
		Pelayanan           string `json:"pelayanan"`
		AsesmedPros1        string `json:"asesmed_pros1"`
		AsesmedPros2        string `json:"asesmed_pros2"`
		AsesmedDokterTtd    string `json:"dokter_ttd"`
		AsesmedDokterTtdTgl string `json:"dokter_ttd_tgl"`
		AsesmedDokterTtdJam string `json:"dokter_ttd_jam"`
	}

	DiagnosaModel struct {
		P  string `json:"primary"`
		S1 string `json:"sekunder1"`
		S2 string `json:"sekunder2"`
		S3 string `json:"sekunder3"`
		S4 string `json:"sekunder4"`
		S5 string `json:"sekunder5"`
		S6 string `json:"sekunder6"`
	}

	DcpptSoapPasien struct {
		InsertDttm                   string `json:"insertDttm"`
		InsertUserId                 string `json:"insertUserID"`
		InsertPc                     string `json:"insertPC"`
		JamCheckOut                  string `json:"jamCheckOut"`
		KdBagian                     string `json:"kdBagian"`
		Noreg                        string `json:"noreg"`
		KdDpjp                       string `json:"kdDpjp"`
		Subjectif                    string `json:"subjectif"`
		Objectif                     string `json:"objectif"`
		Asesmen                      string `json:"asesmen"`
		Plan                         string `json:"plan"`
		PpaPascaBedah                string `json:"ppaPascaBedah"`
		IcdKerja                     string `json:"ICDKerja"`
		IcdDeferensia                string `json:"icdDeferensia"`
		Icd_9                        string `json:"icd9"`
		Note                         string `json:"note"`
		SkriningK1                   string `json:"k1"`
		SkriningK2                   string `json:"k2"`
		SkriningK3                   string `json:"k3"`
		SkriningK4                   string `json:"k4"`
		SkriningK5                   string `json:"k5"`
		SkriningK6                   string `json:"k6"`
		SkriningF1                   string `json:"F1"`
		SkriningF2                   string `json:"F2"`
		SkriningF3                   string `json:"F3"`
		SkriningF4                   string `json:"F4"`
		SkriningB1                   string `json:"B1"`
		SkriningB2                   string `json:"B2"`
		SkriningRj                   string `json:"RJ"`
		SkriningR1                   string `json:"R1"`
		SkriningR2                   string `json:"R2"`
		SkriningR3                   string `json:"R3"`
		SkriningR4                   string `json:"R4"`
		SkriningUser                 string `json:"user"`
		SkriningTgl                  string `json:"tglSkrining"`
		SkriningJam                  string `json:"jamSkrining"`
		AseskepKel                   string `json:"asseskepKel"`
		AseskepRwytPnykt             string `json:"AssesKepRwytPenyakit"`
		AseskepRwytObat              string `json:"AssesKepRwytObat"`
		AseskepRwytObatDetail        string `json:"AssesKepRwytObatDetail"`
		AseskepTd                    string `json:"AssesKepTd"`
		AseskepNadi                  string `json:"AssesNadi"`
		AseskepSuhu                  string `json:"AssesSuhu"`
		AseskepRr                    string `json:"AssesRr"`
		AseskepBb                    string `json:"AssesBb"`
		AseskepTb                    string `json:"AssesTb"`
		AseskepAsesFungsional        string `json:"AssesAsesFungsional"`
		AseskepAsesFungsionalDetail  string `json:"AssesAsesFungsionalDetail"`
		AseskepRj1                   string `json:"AseskepRJ1"`
		AseskepRj2                   string `json:"AseskepRJ2"`
		AseskepHslKajiRj             string `json:"AseskepHslKajiRj"`
		AseskepHslKajiRjTind         string `json:"AseskepHslKajiRjTind"`
		AseskepAsesNyeri             string `json:"AseskepAsesNyeri"`
		AseskepPsiko                 string `json:"AseskepPsiko"`
		AseskepPsikoDetail           string `json:"AseskepPsikoDetail"`
		AseskepPulang1               string `json:"AseskepPulang1"`
		AseskepPulang1Detail         string `json:"AseskepPulang1Detail"`
		AseskepPulang2               string `json:"AseskepPulang2"`
		AseskepPulang2Detail         string `json:"AseskepPulang2Detail"`
		AseskepPulang3               string `json:"AseskepPulang3"`
		AseskepPulang3Detail         string `json:"AseskepPulang3Detail"`
		AseskepMslhKprwtan           string `json:"AseskepMslhKprwtan"`
		AseskepRencKprwtan           string `json:"AseskepRencKprwtan"`
		AseskepUser                  string `json:"AseskepUser"`
		AseskepTgl                   string `json:"AsesKepTgl"`
		AseskepJam                   string `json:"AseskepJam"`
		AsesmedJenpel                string `json:"AsesmedJenpel"`
		AsesmedKeluhUtama            string `json:"AsesmedKeluhUtama"`
		AsesmedRwytSkrg              string `json:"AsesmedRwtSkrg"`
		AsesmedRwytDulu              string `json:"AsesmedRwtDulu"`
		AsesmedRwytObat              string `json:"AsesmedRwtObat"`
		AsesmedRwytPenyKlrg          string `json:"AsesmedRwtPenyKlrg"`
		AsesmedRwytAlergi            string `json:"AsesmedRwtAlergi"`
		AsesmedRwytAlergiDetail      string `json:"AsesmedRwtAlergiDetail"`
		AsesmedKhusPemGigi1          string `json:"AsesmedKhusPemGigi1"`
		AsesmedKhusPemGigi2          string `json:"AsesmedKhusPemGigi2"`
		AsesmedKhusPemGigi3          string `json:"AsesmedKhusPemGigi3"`
		AsesmedKhusPemGigi4          string `json:"AsesmedKhusPemGigi4"`
		AsesmedKhusPemGigi5          string `json:"AsesmedKhusPemGigi5"`
		AsesmedKhusPemGigi5Detail    string `json:"AsesmedKhusPemGigi5Detail"`
		AsesmedGolDarah              string `json:"AsesmedGolDarah"`
		AsesmedTdDetail              string `json:"AsesmedTdDetail"`
		AsesmedPenyJantung           string `json:"AsesmedPenyJantung"`
		AsesmedPenyJantungDetail     string `json:"AsesmedPenyJantungDetail"`
		AsesmedDiabet                string `json:"AsesmedDiabet"`
		AsesmedDiabetDetail          string `json:"AsesmedDiabetDetail"`
		AsesmedHaemop                string `json:"AsesmedHaemop"`
		AsesmedHaemopDetail          string `json:"AsesmedHaemopDetail"`
		AsesmedHepat                 string `json:"AsesmedHepat"`
		AsesmedHepatDetail           string `json:"AsesmedHepatDetail"`
		AsesmedPenyLain              string `json:"AsesmedPenyLain"`
		AsesmedPenyLainDetail        string `json:"AsesmedPenyLainDetail"`
		AsesmedAlergiObat            string `json:"AsesmedAlergiObat"`
		AsesmedAlergiObatDetail      string `json:"AsesmedAlergiObatDetail"`
		AsesmedAlergiMknan           string `json:"AsesmedAlergiMknan"`
		AsesmedAlergiMknanDetail     string `json:"AsesmedAlergiMknanDetail"`
		AsesmedKebiasaanBuruk        string `json:"AsesmedKebiasaanBuruk"`
		AsesmedKhusOkAnterior        string `json:"AsesmedKhusOkAnterior"`
		AsesmedKhusOkPosterior       string `json:"AsesmedKhusOkPosterior"`
		AsesmedKhusOkMolar           string `json:"AsesmedKhusOkMolar"`
		AsesmedKhusPalatum           string `json:"AsesmedKhusPalatum"`
		AsesmedKhusTorusP            string `json:"AsesmedKhusTorusP"`
		AsesmedKhusTorusM            string `json:"AsesmedKhusTorusM"`
		AsesmedKhusTorusMDetail      string `json:"AsesmedKhusTorusMDetail"`
		AsesmedKhusSuperTeeth        string `json:"AsesmedKhusSuperTeeth"`
		AsesmedKhusSuperTeethDetail  string `json:"AsesmedKhusSuperTeethDetail"`
		AsesmedKhusDiastema          string `json:"AsesmedKhusDiastema"`
		AsesmedKhusDiastemaDetail    string `json:"AsesmedKhusDiastemaDetail"`
		AsesmedKhusGigiAnomali       string `json:"AsesmedKhusGigiAnomali"`
		AsesmedKhusGigiAnomaliDetail string `json:"AsesmedKhusGigiAnomaliDetail"`
		AsesmedKhusOralLain          string `json:"AsesmedKhusOralLain"`
	}

	HasilPenunjangMedik struct {
		KetAsalPelayanan string     `json:"asal_pelayanan"`
		NoPenmed         string     `json:"penmed"`
		Uraian           string     `json:"uraian"`
		Hasil            string     `json:"hasil"`
		Catatan          string     `json:"catatan"`
		KodePenunjang    string     `gorm:"primaryKey:KodePenunjang" json:"kode_penunjang"`
		KPelayanan       KPelayanan `gorm:"foreignKey:KdBag" json:"kd_pelayanan"`
	}

	KPelayanan struct {
		KdBag     string `json:"kd_bagian"`
		Bagian    string `json:"bagian"`
		Pelayanan string `json:"pelayanan"`
	}

	InformasiMedis struct {
		InsertDttm       string `json:"insert_dttm"`
		KdBagian         string `json:"kd_bagian"`
		Noreg            string `json:"noreg"`
		AsesmedMslhMedis string `json:"masalah_medis"`
		AsesmedTerapi    string `json:"terapi"`
		AsesmedPemFisik  string `json:"fisik"`
		AsesmedAnjuran   string `json:"anjuran"`
	}

	// DATA SOAP PASIEN
	SoapPasien struct {
		KdBagian string `json:"kd_bagian"`
		Noreg    string `json:"noreg"`
	}

	// GET ASESMED
	AsesmedAnamnesa struct {
		AsesmedKeluhUtama         string `json:"kelUtama"`
		AsesmedRwytSkrg           string `json:"riwayatSekarang"`
		AsesmedRwytPenyKlrg       string `json:"penyakitKeluarga"`
		AsesmedRwytAlergi         string `json:"riwayatAlergi"`
		AsesmedRwytAlergiDetail   string `json:"riwayatAlergiDetail"`
		AsesmedJenpel             string `json:"jenisPelayanan"`
		AsesmedKhusPemGigi1       string `json:"gigi1"`
		AsesmedKhusPemGigi2       string `json:"gigi2"`
		AsesmedKhusPemGigi3       string `json:"gigi3"`
		AsesmedKhusPemGigi4       string `json:"gigi4"`
		AsesmedKhusPemGigi5       string `json:"gigi5"`
		AsesmedKhusPemGigi5Detail string `json:"gigi5Detail"`
		KdBagian                  string `json:"kdBagian"`
		Noreg                     string `json:"noReg"`
		KdDpjp                    string `json:"kd_dpjp"`
	}

	DataMedikModel struct {
		AsesmedGolDarah          string `json:"golDarah"`
		AsesmedTd                string `json:"tekananDarah"`
		AsesmedTdDetail          string `json:"tekananDarahDetail"`
		AsesmedPenyJantung       string `json:"penyJantung"`
		AsesmedPenyJantungDetail string `json:"penyJantungDetail"`
		AsesmedDiabet            string `json:"diabet"`
		AsesmedDiabetDetail      string `json:"diabetDetail"`
		AsesmedHaemop            string `json:"haemop"`
		AsesmedHaemopDetail      string `json:"haemopDetail"`
		AsesmedHepat             string `json:"hepat"`
		AsesmedHepatDetail       string `json:"hepatDetail"`
		AsesmedPenyLain          string `json:"penyLain"`
		AsesmedPenyLainDetail    string `json:"penyLainDetail"`
		AsesmedAlergiObat        string `json:"alergiObat"`
		AsesmedAlergiObatDetail  string `json:"alergiObatDetail"`
		AsesmedAlergiMknan       string `json:"alergiMakanan"`
		AsesmedAlergiMknanDetail string `json:"alergiMakananDetail"`
		AsesmedKebiasaanBuruk    string `json:"kebiasaanBuruk"`
		KdBagian                 string `json:"kdBagian"`
		Noreg                    string `json:"noReg"`
	}

	// Data Intra Oral
	DataIntraOralModel struct {
		InsertDttm                   string `json:"insertDttm"`
		AsesmedKhusOkAnterior        string `json:"anterior"`
		AsesmedKhusOkPosterior       string `json:"posterior"`
		AsesmedKhusOkMolar           string `json:"molar"`
		AsesmedKhusPalatum           string `json:"palatum"`
		AsesmedKhusTorusP            string `gorm:"column:asesmed_khus_torus_P" json:"torusP"`
		AsesmedKhusTorusM            string `gorm:"column:asesmed_khus_torus_M" json:"torusM"`
		AsesmedKhusTorusDetail       string `gorm:"column:asesmed_khus_torus_M_detail" json:"torusMDetail"`
		AsesmedKhusSuperTeeth        string `json:"superTeeth"`
		AsesmedKhusSuperTeethDetail  string `json:"superTeethDetail"`
		AsesmedKhusDiastema          string `json:"diastema"`
		AsesmedKhusDiastemaDetail    string `json:"diastemaDetail"`
		AsesmedKhusGigiAnomali       string `json:"gigiAnomali"`
		AsesmedKhusGigiAnomaliDetail string `json:"gigiAnomaliDetail"`
		AsesmedKhusOralLain          string `json:"khusOralLain"`
		KdBagian                     string `json:"kdBagian"`
		Noreg                        string `json:"noReg"`
	}

	// Pasca Operasi Model
	PascaOperasiModel struct {
		InsertDttm                  string `json:"insertDttm"`
		KdBagian                    string `json:"kdBagian"`
		Noreg                       string `json:"noreg"`
		ObsanslokKhusPostKeadUmum   string `json:"keadaanUmum"`
		ObsanslokKhusPostKeluhUtama string `json:"keluhUmum"`
		ObsanslokKhusPostTd         string `json:"tekananDarah"`
		ObsanslokKhusPostNadi       string `json:"nadi"`
		ObsanslokKhusPostSuhu       string `json:"suhu"`
		ObsanslokKhusPostRr         string `json:"pernapasan"`
	}

	// INTRA OPERASI
	IntraOperasiModel struct {
		InsertDttm         string `json:"insertDttm"`
		KdBagian           string `json:"kdBagian"`
		Noreg              string `json:"noreg"`
		ObsansloKhusInTd   string `json:"tekananDarah"`
		ObsansloKhusInNadi string `json:"nadi"`
		ObsansloKhusInSuhu string `json:"suhu"`
		ObsansloKhusInRr   string `json:"pernapasan"`
	}

	// ====================== TRIASE ======================TRIASE
	TriaseModel struct {
		InsertDttm              string `json:"insertDttm"`
		KdBagian                string `json:"kdBagian"`
		Noreg                   string `json:"noreg"`
		IgdTriaseKeluh          string `json:"keluhan"`
		IgdTriaseAlergi         string `json:"alergi"`
		IgdTriaseAlergiDetail   string `json:"alergi_detail"`
		IgdTriaseNafas          string `json:"nafas"`
		IgdTriaseTd             string `json:"tekanan_darah"`
		IgdTriaseRr             string `json:"pernapasan"`
		IgdTriasePupil          string `json:"pupil"`
		IgdTriaseNadi           string `json:"nadi"`
		IgdTriaseSpo2           string `json:"spo"`
		IgdTriaseSuhu           string `json:"suhu"`
		IgdTriaseAkral          string `json:"akral"`
		IgdTriaseGangguan       string `json:"gangguan"`
		IgdTriaseGangguanDetail string `json:"gangguan_detail"`
		IgdTriaseSkalaNyeri     int    `json:"nyeri"`
		IgdTriaseSkalaNyeriP    string `json:"nyeri_p"`
		IgdTriaseSkalaNyeriQ    string `json:"nyeri_q"`
		IgdTriaseSkalaNyeriR    string `json:"nyeri_r"`
		IgdTriaseSkalaNyeriS    string `json:"nyeri_s"`
		IgdTriaseSkalaNyeriT    string `json:"nyeri_t"`
		IgdTriaseFlaccWajah     int    `json:"wajah"`
		IgdTriaseFlaccKaki      int    `json:"kaki"`
		IgdTriaseFlaccAktifitas int    `json:"aktifitas"`
		IgdTriaseFlaccMenangis  int    `json:"menangis"`
		IgdTriaseFlaccBersuara  int    `json:"bersuara"`
		IgdTriaseFlaccTotal     int    `json:"total"`
		IgdTriaseSkalaTriase    int    `json:"triase"`
		IgdTriaseFingerUser     string `json:"user"`
		IgdTriaseFingerTgl      string `json:"tanggal"`
		IgdTriaseFingerJam      string `json:"jam"`
	}

	// ASESMEN AWAL MEDIS PASIEN IGD
	AsesmedAwalPasienIGDModel struct {
		InsertDttm        string `json:"insertDttm"`
		KdBagian          string `json:"kdBagian"`
		TglMasuk          string `json:"tgl_masuk"`
		JamMasuk          string `json:"jam_masuk"`
		Noreg             string `json:"noreg"`
		AsesmedKeluhUtama string `json:"keluhan_utama"`
		AsesmedTelaah     string `json:"telaah"`
		AsesmedMslhMedis  string `json:"mslh_medis"`
		AsesmedRwytSkrg   string `json:"rwyt_skrg"`
		AsesmedRwytObat   string `json:"rwyt_obat"`
		AsesmedJenpel     string `json:"jenpel"`
		KeteranganPerson  string `json:"person"`
		InsertUserId      string `json:"user_id"`
		InsertPc          string `json:"device_id"`
		Pelayanan         string `json:"pelayanan"`
	}

	ImageLokalisModel struct {
		InsertDttm          string `json:"insertDttm"`
		KdBagian            string `json:"kdBagian"`
		Noreg               string `json:"noreg"`
		AsesmedLokalisImage string `json:"lokalis_image"`
		TglMasuk            string `json:"tgl_masuk"`
		JamMasuk            string `json:"jam_masuk"`
		KdDpjp              string `json:"kd_dpjp"`
		KeteranganPerson    string `json:"person"`
		InsertUserId        string `json:"user_id"`
		InsertPc            string `json:"device_id"`
		Pelayanan           string `json:"pelayanan"`
		AsesmedDokterTtd    string `json:"dokter_ttd"`
		AsesmedDokterTtdTgl string `json:"dokter_ttd_tgl"`
		AsesmedDokterTtdJam string `json:"dokter_ttd_jam"`
	}

	//  PEMERIKSAAN FISIK MODEL
	PemeriksaanFisikModel struct {
		InsertDttm                     string `json:"insertDttm"`
		KdBagian                       string `json:"kdBagian"`
		Noreg                          string `json:"noreg"`
		AsesmedPemfisKepala            string `json:"kepala"`
		AsesmedPemfisKepalaDetail      string `json:"kepala_detail"`
		AsesmedPemfisLeher             string `json:"leher"`
		AsesmedPemfisLeherDetail       string `json:"leher_detail"`
		AsesmedPemfisGenetalia         string `json:"genetalia"`
		AsesmedPemfisGenetaliaDetail   string `json:"genetalia_detail"`
		AsesmedPemfisAbdomen           string `json:"abdomen"`
		AsesmedPemfisAbdomenDetail     string `json:"abdomen_detail"`
		AsesmedPemfisEkstremitas       string `json:"ekstremitas"`
		AsesmedPemfisEkstremitasDetail string `json:"ekstremitas_detail"`
		AsesmedPemfisDada              string `json:"data"`
		AsesmedPemfisDadaDetail        string `json:"data_detail"`
		AsesmedPemfisPunggung          string `json:"punggung"`
		AsesmedPemfisPunggungDetail    string `json:"punggung_detail"`
		AsesmedPemfisLain              string `json:"lain_lain"`
		KeteranganPerson               string `json:"person"`
		InsertUserId                   string `json:"user_id"`
		InsertPc                       string `json:"device_id"`
		Pelayanan                      string `json:"pelayanan"`
	}

	KtaripTotal struct {
		TaripKelas float64 `json:"tarip_kelas"`
		NameGrup   string  `json:"nama_grup"`
	}

	// MODEL MENAMPUNG DATA PILIH LABOR
	PilihPemeriksaanLaborModel struct {
		Num         int     `json:"num"`
		NameGrup    string  `json:"name_grup"`
		Kode        string  `json:"kode"`
		Pemeriksaan string  `json:"pemeriksaan"`
		TaripKelas  float64 `json:"tarip_kelas"`
	}

	DetailPemeriksaanLaborModel struct {
		KtaripTotal KtaripTotal             `json:"total"`
		Pemeriksaan []PemeriksaanLaborModel `json:"pemeriksaan"`
	}

	PemeriksaanLaborModel struct {
		Kelompok    string `json:"kelompok"`
		Satuan      string `json:"satuan"`
		Normal      string `json:"normal"`
		Urut        int    `json:"urut"`
		NameGrup    string `json:"name_grup"`
		Pemeriksaan string `json:"pemeriksaan"`
		Kode        string `json:"kode"`
	}

	// asesmed_konsul_ke_alasan, asesmed_konsul_ke, asesmed_anjuran, asesmed_anjuran, asesmed_alasan_opname, asesmed_terapi

	// =======    ====== //
	RencanaTindakLanjutModel struct {
		InsertDttm            string `json:"insertDttm"`
		KdBagian              string `json:"kdBagian"`
		Noreg                 string `json:"noreg"`
		AsesmedKonsulKeAlasan string `json:"alasan_konsul"`
		AsesmedTerapi         string `json:"terapi"`
		AsesmedAlasanOpname   string `json:"alasan_opname"`
		AsesmedKonsulKe       string `json:"konsul_ke"`
		AsesmenPrognosis      string `json:"prognosis"`
		TglMasuk              string `json:"tgl_masuk"`
		JamMasuk              string `json:"jam_masuk"`
		KdDpjp                string `json:"kd_dpjp"`
		KeteranganPerson      string `json:"person"`
		InsertUserId          string `json:"user_id"`
		InsertPc              string `json:"device_id"`
		Pelayanan             string `json:"pelayanan"`
		AsesmedDokterTtd      string `json:"dokter_ttd"`
		AsesmedDokterTtdTgl   string `json:"dokter_ttd_tgl"`
		AsesmedDokterTtdJam   string `json:"dokter_ttd_jam"`
	}

	// ========= MODEL ASESMED_KEPERAWATAN_BIDAN

	AsesmedKeperawatanBidan struct {
		InsertDttm                 string `json:"insertDttm"`
		KdBagian                   string `json:"kdBagian"`
		Noreg                      string `json:"noreg"`
		AseskepPerolehanInfo       string `json:"info"`
		AseskepCaraMasuk           string `json:"cara_masuk"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail"`
		AseskepAsalMasuk           string `json:"asal_masuk"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail"`
		AseskepBb                  string `json:"bb"`
		AseskepTb                  string `json:"tb"`
		AseskepRwytPnykt           string `json:"rwt_penyakit"`
		AseskepRwytObatDetail      string `json:"obat_detail"`
		AseskepAsesFungsional      string `json:"fungsional"`
		AseskepRj1                 string `json:"rj1"`
		AseskepRj2                 string `json:"rj2"`
		AseskepHslKajiRj           string `json:"kaji_rj"`
		AseskepHslKajiRjTind       string `json:"kaji_rj_tindakan"`
		AseskepSkalaNyeri          int    `json:"skala_nyeri"`
		AseskepFrekuensiNyeri      string `json:"frekuensi_nyeri"`
		AseskepLamaNyeri           string `json:"lama_nyeri"`
		AseskepNyeriMenjalar       string `json:"nyeri_menjalar"`
		AseskepNyeriMenjalarDetail string `json:"menjalar_detail"`
		AseskepKualitasNyeri       string `json:"kualitas_nyeri"`
		AseskepNyeriPemicu         string `json:"nyeri_pemicu"`
		AseskepNyeriPengurang      string `json:"nyeri_pengurang"`
		// =================== RIWAYAT KEHAMILAN
		AseskepKehamilan         string `json:"kehamilan"`
		AseskepKehamilanGravida  string `json:"kehamilan_gravida"`
		AseskepKehamilanPara     string `json:"kehamilan_para"`
		AseskepKehamilanAbortus  string `json:"kehamilan_abortus"`
		AseskepKehamilanHpht     string `json:"kehamilan_hpht"`
		AseskepKehamilanTtp      string `json:"kehamilan_ttp"`
		AseskepKehamilanLeopold1 string `json:"kehamilan_leopol1"`
		AseskepKehamilanLeopold2 string `json:"kehamilan_leopol2"`
		AseskepKehamilanLeopold3 string `json:"kehamilan_leopol3"`
		AseskepKehamilanLeopold4 string `json:"kehamilan_leopol4"`
		AseskepKehamilanDjj      string `json:"kehamilan_djj"`
		AseskepKehamilanVt       string `json:"kehamilan_vt"`
		// ==============================
		AseskepDekubitus1             string `json:"dekubitus1"`
		AseskepDekubitus2             string `json:"dekubitus2"`
		AseskepDekubitus3             string `json:"dekubitus3"`
		AseskepDekubitus4             string `json:"dekubitus4"`
		AseskepDekubitusAnak          string `json:"dekubitus_anak"`
		AseskepPulangKondisi          string `json:"pulang_kondisi"`
		AseskepPulangTransportasi     string `json:"pulang_transportasi"`
		AseskepPulangPendidikan       string `json:"pendidikan"`
		AseskepPulangPendidikanDetail string `json:"pendidikan_detail"`
	}

	RiwayatPenyakit struct {
		Noreg    string `json:"	noreg"`
		Penyakit string `json:"penyakit"`
		NoRm     string `json:"no_rm"`
	}

	//===//
	ResikoJatuhGetUpGoTest struct {
		InsertDttm           string `json:"insert_dttm"`
		UpdDttm              string `json:"update"`
		KeteranganPerson     string `json:"person"`
		InsertUserId         string `json:"user_id"`
		InsertPc             string `json:"insert_pc"`
		TglMasuk             string `json:"tgl_masuk"`
		KdBagian             string `json:"kd_bagian"`
		Noreg                string `json:"noreg"`
		Pelayanan            string `json:"pelayanan"`
		AseskepRj1           string `json:"rj1"`
		AseskepRj2           string `json:"rj2"`
		AseskepHslKajiRj     string `json:"hasil_rj"`
		AseskepHslKajiRjTind string `json:"rj_tind"`
	}

	AsesemenIGD struct {
		InsertDttm                 string
		UpdDttm                    string
		KeteranganPerson           string
		InsertUserId               string
		InsertPc                   string
		TglMasuk                   string
		KdBagian                   string
		Noreg                      string
		Pelayanan                  string
		AseskepKel                 string `json:"keluhan_utama"`
		JamCheckOut                string `json:"jam_check_out"`
		JamCheckInRanap            string `json:"jam_check_in_ranap"`
		CaraKeluar                 string `json:"cara_keluar"`
		AseskepPerolehanInfo       string `json:"info"`
		AseskepPerolehanInfoDetail string `json:"info_detail"`
		AseskepCaraMasuk           string `json:"cara_masuk"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail"`
		AseskepAsalMasuk           string `json:"asal_masuk"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail"`
		AseskepRwytObat            string `json:"rwt_obat"`
		AseskepAsesFungsional      string `json:"fungsional"`
		AseskepReaksiAlergi        string `json:"reaksi_alergi"`
		AseskepRwytPnykt           string `json:"rwt_sekarang"`
		AseskepRwytPnyktDahulu     string `json:"rwt_penyakit"`
		AseskepPulang1             string
	}

	ResponseAsesmenIGD struct {
		CaraKeluar                 string `json:"cara_keluar"`
		AseskepPerolehanInfo       string `json:"info"`
		AseskepPerolehanInfoDetail string `json:"info_detail"`
		AseskepCaraMasuk           string `json:"cara_masuk"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail"`
		AseskepAsalMasuk           string `json:"asal_masuk"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail"`
		AseskepRwytPnykt           string `json:"rwt_penyakit"`
		AseskepRwytObat            string `json:"rwt_obat"`
		AseskepAsesFungsional      string `json:"fungsional"`
	}

	// TABLE SOAP PASIEN
	DcpptSoapPasienModel struct {
		InsertDttm       string
		UpdDttm          string
		KeteranganPerson string
		InsertUserId     string
		InsertPc         string
		TglMasuk         string
		KdBagian         string
		Noreg            string
		Pelayanan        string

		AseskepKel                 string
		AseskepReaksiAlergi        string
		AseskepRwytPnyktDahulu     string
		JamCheckOut                string `json:"jam_check_out"`
		JamCheckInRanap            string `json:"jam_check_in_ranap"`
		CaraKeluar                 string `json:"cara_keluar"`
		AseskepPerolehanInfo       string `json:"info"`
		AseskepPerolehanInfoDetail string `json:"info_detail"`
		AseskepCaraMasuk           string `json:"cara_masuk"`
		AseskepCaraMasukDetail     string `json:"cara_masuk_detail"`
		AseskepAsalMasuk           string `json:"asal_masuk"`
		AseskepAsalMasukDetail     string `json:"asal_masuk_detail"`
		AseskepTd                  string `json:"td"`
		AseskepBb                  string `json:"bb"`
		AseskepTb                  string `json:"tb"`
		AseskepRwytPnykt           string `json:"rwt_penyakit"`
		AseskepRwytObat            string `json:"rwt_obat"`
		AseskepAsesFungsional      string `json:"fungsional"`
		AseskepRj1                 string `json:"rj1"`
		AseskepRj2                 string `json:"rj2"`
		AseskepHslKajiRj           string `json:"hasil_rj"`
		AseskepHslKajiRjTind       string `json:"rj_tind"`
		AseskepDekubitus1          string `json:"dekubitus1"`
		AseskepDekubitus2          string `json:"dekubitus2"`
		AseskepDekubitus3          string `json:"dekubitus3"`
		AseskepDekubitus4          string `json:"dekubitus4"`
		AseskepDekubitusAnak       string `json:"dekubitus_anak"`
		// ============================ END SKRINING DEKUBITUS
		AseskepKehamilan         string `json:"kehamilan"`
		AseskepKehamilanGravida  string `json:"gravida"`
		AseskepKehamilanPara     string `json:"para"`
		AseskepKehamilanAbortus  string `json:"abortus"`
		AseskepKehamilanHpht     string `json:"hpht"`
		AseskepKehamilanTtp      string `json:"ttp"`
		AseskepKehamilanLeopold1 string `json:"leopol1"`
		AseskepKehamilanLeopold2 string `json:"leopol2"`
		AseskepKehamilanLeopold3 string `json:"leopol3"`
		AseskepKehamilanLeopold4 string `json:"leopol4"`
		AseskepKehamilanDjj      string `json:"djj"`
		AseskepKehamilanVt       string `json:"vt"`
		// ================================= SKRINING NYERI
		AseskepSkalaNyeri          int    `json:"nyeri"`
		AseskepFrekuensiNyeri      string `json:"frekuensi_nyeri"`
		AseskepLamaNyeri           string `json:"lama_nyeri"`
		AseskepNyeriMenjalar       string `json:"menjalar"`
		AseskepNyeriMenjalarDetail string `json:"menjalar_detail"`
		AseskepKualitasNyeri       string `json:"kualitas_nyeri"`
		AseskepNyeriPemicu         string `json:"pemicu"`
		AseskepNyeriPengurang      string `json:"pengurang"`
		// ============================== TINDAK LANJUT IGD
		AseskepPulang1       string `json:"pulang1"`
		AseskepPulang1Detail string `json:"pulang1_detail"`
		AseskepPulang2       string `json:"pulang2"`
		AseskepPulang2Detail string `json:"pulang2_detail"`
		AseskepPulang3       string `json:"pulang3"`
		AseskepPulang3Detail string `json:"pulang3_detail"`
		// ASESESMEN KEBIDANAN
		AseskepKeadaanUmum     string `json:"keadaan_umum"`
		AseskepKesadaran       string `json:"kesadaran"`
		AseskepKesadaranDetail string `json:"kesadaran_detail"`
	}

	TindakLanjutModel struct {
		KdBagian             string
		Noreg                string
		AseskepPulang1       string
		AseskepPulang1Detail string
		AseskepPulang2       string
		AseskepPulang2Detail string
		AseskepPulang3       string
		AseskepPulang3Detail string
	}

	// PENKAJIAN AWAL KEPERAWATAN
	PengkajianAwalKeperawatan struct {
		InsertDttm                 string `json:"insert_dttm"`
		KdBagian                   string `json:"kd_bagian"`
		KeteranganPerson           string `json:"keterangan_person"`
		InsertUserId               string `json:"user_id"`
		InsertPc                   string `json:"insert_pc"`
		Pelayanan                  string `json:"pelayanan"`
		Noreg                      string `json:"noreg"`
		TglMasuk                   string `json:"tgl_masuk"`
		KdDpjp                     string `json:"kd_dpjp"`
		AseskepPerolehanInfo       string `json:"perolehan_info"`
		AseskepPerolehanInfoDetail string `json:"perolehan_info_detail"`
		AseskepKel                 string `json:"keluhan_utama"`
		AseskepRwytPnykt           string `json:"rwt_penyakit"`
		AseskepReaksiAlergi        string `json:"reaksi_alergi"`
		AseskepRwytPnyktDahulu     string `json:"rwt_penyakit_dahulu"`
		AseskepRwtImunisasi        string `json:"rwt_imunisasi"`
		AseskepRwtKelahiran        string `json:"rwt_kelahiran"`
	}

	DDoubleCheck struct {
		InsertDttm         string               `json:"insert_dttm"`
		VerifyDttm         string               `json:"verify_dttm"`
		IdDoubleCheck      int                  `json:"id_double_check"`
		Noreg              string               `json:"no_reg"`
		UserId             string               `json:"user_id"`
		InsertPc           string               `json:"insert_pc"`
		KdBagian           string               `json:"kd_bagian"`
		PasienPemberi      bool                 `json:"pasien_pemberi"`
		PasienVerify       bool                 `json:"pasien_verify"`
		ObatPemberi        bool                 `json:"obat_pemberi"`
		ObatVerify         bool                 `json:"obat_verify"`
		DosisPemberi       bool                 `json:"dosis_pemberi"`
		DosisVerify        bool                 `json:"dosis_verify"`
		CaraPemberi        bool                 `json:"cara_pemberi"`
		CaraVerify         bool                 `json:"cara_verify"`
		WaktuPemberi       bool                 `json:"waktu_pemberi"`
		WaktuVerify        bool                 `json:"waktu_verify"`
		InformasiPemberi   bool                 `json:"informasi_pemberi"`
		InformasiVerify    bool                 `json:"informasi_verify"`
		DokumentasiPemberi bool                 `json:"dokumentasi_pemberi"`
		DokumentasiVerify  bool                 `json:"dokumentasi_verify"`
		Keterangan         string               `json:"keterangan"`
		NamaObat           string               `json:"nama_obat"`
		KodeObat           string               `json:"kode_obat"`
		PerawatPemberi     string               `json:"perawat_pemberi"`
		Perawat            rme.UserPerawatModel `gorm:"foreignKey:UserId" json:"user"`
		VerifyNama         string               `json:"verify_nama"`
		// PerawatVerify      rme.UserPerawatModel `gorm:"foreignKey:VerifyId" json:"user_verify"`
	}
)

func (PemeriksaanFisikModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DDoubleCheck) TableName() string {
	return "vicore_rme.ddoble_check"
}

func (PengkajianAwalKeperawatan) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesmedKeperawatanBidan) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (TindakLanjutModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DcpptSoapPasienModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesemenIGD) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (ResikoJatuhGetUpGoTest) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (RencanaTindakLanjutModel) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (SoapPasien) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (ImageLokalisModel) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (AsesmedAwalPasienIGDModel) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (IntraOperasiModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (TriaseModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (PascaOperasiModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DataIntraOralModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
func (DataMedikModel) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AsesmedAnamnesa) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DiagnosaModelOne) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (InformasiMedis) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (HasilPenunjangMedik) TableName() string {
	return "vicore_his.dhasil_penunjang"
}

func (TriaseRiwayatAlergi) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (KeluhanUtamaPerawatIGD) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (KPelayanan) TableName() string {
	return "vicore_lib.kpelayanan"
}
func (Anatomi) TableName() string {
	return "vicore_soap.anatomi"
}

func (DcpptSoapPasien) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (AssementRawatJalanPerawat) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DcpptSoap) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (DcpptSoapDokter) TableName() string {
	return "vicore_rme.dcppt_soap_dokter"
}

func (Odontograms) TableName() string {
	return "vicore_rme.odontogram"
}

func (DcpptOdontogram) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}

func (KebEdukasi) TableName() string {
	return "vicore_rme.dcppt_soap_pasien"
}
