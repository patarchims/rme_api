package soap

type (
	DVitalSignIGDDokter struct {
		InsertDttm   string `json:"insert_dttm"`
		InsertUserId string `json:"user_id"`
		Pelayanan    string `json:"pelayanan"`
		KdBagian     string `json:"kd_bagian"`
		InsertDevice string `json:"device_id"`
		Kategori     string `json:"kategori"`
		KetPerson    string `json:"person"`
		Noreg        string `json:"noreg"`
		Td           string `json:"td"`
		Nadi         string `json:"nadi"`
		Suhu         string `json:"suhu"`
		Spo2         string `json:"spo2"`
		Pernafasan   string `json:"pernafasan"`
		Tb           string `json:"tinggi_badan"`
		Bb           string `json:"berat_badan"`
	}

	DPemFisikIGDDokterRepository struct {
		InsertDttm   string
		Noreg        string
		Pelayanan    string
		InsertUserId string
		KdBagian     string
		InsertDevice string
		Kategori     string
		KetPerson    string
		GcsE         string
		GcsV         string
		GcsM         string
		Kesadaran    string
		Akral        string
		Pupil        string
	}
)

func (DPemFisikIGDDokterRepository) TableName() string {
	return "vicore_rme.dpem_fisik"
}
func (DVitalSignIGDDokter) TableName() string {
	return "vicore_rme.dvital_sign"
}
