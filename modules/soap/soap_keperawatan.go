package soap

type (
	DVitalSignKeperawatanBangsal struct {
		InsertDttm   string
		InsertUserId string
		Pelayanan    string
		KdBagian     string
		InsertDevice string
		Kategori     string
		KetPerson    string
		Noreg        string
		Td           string
		Tb           string
		Bb           string
		Nadi         string
		Suhu         string
		Spo2         string
		Pernafasan   string
	}

	DPemFisikKeperawatanBangsalRepository struct {
		InsertDttm   string
		Noreg        string
		Pelayanan    string
		InsertUserId string
		KdBagian     string
		InsertDevice string
		Kategori     string
		KetPerson    string
		GcsE         string
		GcsV         string
		GcsM         string
		Kesadaran    string
		Akral        string
		Pupil        string
	}
)

func (DVitalSignKeperawatanBangsal) TableName() string {
	return "vicore_rme.dvital_sign"
}

func (DPemFisikKeperawatanBangsalRepository) TableName() string {
	return "vicore_rme.dpem_fisik"
}
