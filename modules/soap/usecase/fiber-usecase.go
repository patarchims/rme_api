package usecase

import (
	"encoding/json"
	"errors"
	"fmt"
	rmeDto "hms_api/modules/rme/dto"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

func (su *soapUseCase) PublishImageOdontogramUseCaseV2(noReg string, imgFile string, kdBagian string) (res soap.DcpptOdontogram, err error) {

	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(noReg, kdBagian)

	if err != nil || soap.Noreg == "" {
		res, errs := su.soapRepository.InsertImageOdontogramRepository(kdBagian, noReg, imgFile)
		if errs != nil {
			log.Info().Msg(err.Error())
			return res, errs
		}

		return res, nil
	} else {
		// DELETE IMAGE SEBELUMNYA

		data, err := su.soapRepository.UpdateImageOdontogramRepository(noReg, imgFile)
		if err != nil {
			log.Info().Msg(err.Error())
			return res, err
		}
		return data, nil
	}
}

func (su *soapUseCase) DeleteOdontogramV2(req dto.RequestDeleteOdontogramV2) (err error) {
	_, errs := su.soapRepository.DeleteOdontogramRepositoryV2(req)

	if errs != nil {
		return errs
	}
	return nil
}

func (su *soapUseCase) DeleteOdontogramUsecaseV2(req dto.RequestDeleteOdontogramV2) (err error) {
	_, errs := su.soapRepository.DeleteOdontogramRepositoryV2(req)

	if errs != nil {
		return errs
	}
	return nil
}

func (su *soapUseCase) InsertOdontogramUseCaseV2(reg dto.RequestSaveOdontogramV2) (err error) {

	_, errs := su.soapRepository.UpdateOdontogramRepositoryV2(reg)

	if errs != nil {
		return errs
	}

	return nil
}

func (su *soapUseCase) SaveAssesRawatJalanDokterUsecaseV2(userID string, req dto.RequestSaveAssementRawatJalanDokterV2) (data soap.DcpptOdontogram, err error) {
	// CARI APAKAH SUDAH ADA DATA SEBELUMNYA ?
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, req.KdBagian)
	if err != nil || soap.Noreg == "" {
		// SIMPAN DATA YANG BARU
		fmt.Println("INSERT SOAP PASIEN")
		value, err := su.soapRepository.InsertAssesRawatJalanDokterRepositoryV2(userID, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return value, errors.New(message)
		}

		return value, nil
	} else {
		// UPDATE DATA DENGAN
		// YANG SUDAH ADA
		log.Info().Msg("UPDATE ASESMEN RAWAT JALAN DOKTER")
		request, _ := json.Marshal(req)
		log.Info().Msg(string(request))
		save, errs := su.soapRepository.UpdateAssesRawatJalanDokterRepositoryV2(req)

		if errs != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errs.Error())
			return save, errors.New(message)
		}

		return save, nil
	}

}

func (su *soapUseCase) SaveAssementRawatJalanPerawatUseCaseV2(req dto.RequestSaveAssementRawatJalanPerawatV2) (res soap.AssementRawatJalanPerawat, err error) {

	// CARI APAKAH SUDAH ADA DATA SEBELUMNYA ?
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, req.KodeBagian)

	if err != nil || soap.Noreg == "" {
		// SIMPAN DATA YANG BARU
		log.Info().Msg("INSERT ASESMEN RAWAT JALAN PERAWAT")
		request, _ := json.Marshal(req)
		log.Info().Msg(string(request))
		repos, ersr := su.soapRepository.InsertDcpptSoapPasienRepositoryV2(req)
		if ersr != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return res, errors.New(message)
		}
		return repos, err
	}

	// UPDATE DATA RAWAT JALAN PERAWAT
	repo, ersr := su.soapRepository.UpdateDcpptSoapPasienRepositoryV2(req)
	log.Info().Msg("UPDATE ASESMEN RAWAT JALAN PERAWAT")
	log.Info().Msg("Request Data")
	data, _ := json.Marshal(req)
	log.Info().Msg(string(data))
	if ersr != nil {
		message := fmt.Sprintf("Error %s, Data gagal disimpan", ersr.Error())
		return repo, errors.New(message)
	}

	return repo, nil
}

func (su *soapUseCase) SimpanAnamnesaIGDUsecaseV2(kdBagian string, req dto.AnamnesaIGDV2) (res soap.AsesmedAwalPasienIGDModel, message string, err error) {

	su.logging.Info("CARI DATA PASIEN REPOSITORY")

	soap, errs := su.SearchDcpptSoapPasienDokterUsecase(req.NoReg, kdBagian, req.KeteranganPerson)
	su.logging.Info(soap)

	if errs != nil || soap.Noreg == "" {
		// LAKUKAN PENCARIAN DATA PASIEN DI REKAM REGISTER
		// AMBIL DATA PASIEN TERDAHULU
		// GET TANGGAL MASUK PASIEN
		su.logging.Info("CARI TANGGAL")
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.NoReg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		su.logging.Info("INSERT DATA Anamnesa IGD")
		insert, err1 := su.soapRepository.SaveAnamnesaIGDRepositoryV2(kdBagian, req, tanggal.Tanggal[0:10], tanggal.Jam)

		if err1 != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err1.Error())
			return insert, message, errors.New(message)
		}

		return insert, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE DATA Anamnesa IGD")
		update, err2 := su.soapRepository.UpdateAnamnesaIGDRepositoryV2(kdBagian, req)

		if err2 != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err2.Error())
			return update, message, errors.New(message)
		}

		return update, "Data berhasil di update", nil

	}
}

func (su *soapUseCase) SaveAssesRawatJalanDokterUseCaseV2(userID string, req dto.RequestSaveAssementRawatJalanDokterV2) (data soap.DcpptOdontogram, err error) {
	// CARI APAKAH SUDAH ADA DATA SEBELUMNYA ?
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, req.KdBagian)
	if err != nil || soap.Noreg == "" {
		// SIMPAN DATA YANG BARU
		fmt.Println("INSERT SOAP PASIEN")
		value, err := su.soapRepository.InsertAssesRawatJalanDokterRepositoryV2(userID, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return value, errors.New(message)
		}

		return value, nil
	} else {
		// UPDATE DATA DENGAN
		// YANG SUDAH ADA
		log.Info().Msg("UPDATE ASESMEN RAWAT JALAN DOKTER")
		request, _ := json.Marshal(req)
		log.Info().Msg(string(request))
		save, errs := su.soapRepository.UpdateAssesRawatJalanDokterRepositoryV2(req)

		if errs != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errs.Error())
			return save, errors.New(message)
		}

		return save, nil
	}
}

func (su *soapUseCase) OnGetAsesmenIGDUseCase(userID string, kdBagian string, req dto.ReqAsesmenIGD) (res dto.ResponseAsesmenIGD) {
	pengkajian, _ := su.soapRepository.OnGetAsesmenIGDRepository(kdBagian, req.Noreg)
	alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

	maper := su.soapMapper.ToMappingResponseAsesmenIGD(alergi, riwayatPerawat, pengkajian)

	return maper
}

func (su *soapUseCase) OnSaveAsesmenIGDUseCase(userID string, kdBagian string, req dto.ReqOnSaveAsesmenIGD) (res dto.ResponseAsesmenIGD, message string, err error) {
	soaps, err12 := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	if err12 != nil || soaps.Noreg == "" {
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		times := time.Now()

		data1 := soap.DcpptSoapPasienModel{
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			UpdDttm:                    times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:           req.Person,
			InsertUserId:               userID,
			InsertPc:                   req.DevicesID,
			TglMasuk:                   tanggal.Tanggal[0:10],
			KdBagian:                   kdBagian,
			Noreg:                      req.Noreg,
			Pelayanan:                  req.Pelayanan,
			AseskepPerolehanInfo:       req.PerolehanInfo,
			AseskepPerolehanInfoDetail: req.PerolehanInfoDetail,
			AseskepCaraMasuk:           req.CaraMasuk,
			AseskepAsalMasuk:           req.AsalMasuk,
			AseskepAsesFungsional:      req.FungsiAktivitas,
			AseskepReaksiAlergi:        req.ReaksiAlergi,
			AseskepKel:                 req.KeluhanUtama,
			AseskepRwytPnykt:           req.RiwayatPenyakitSekarang,
			AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
		}

		_, err := su.soapRepository.SaveDcpptSoapPasien(data1)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return res, message, errors.New(message)
		}

		return res, "Data berhasil disimpan", nil

	} else {

		data1 := soap.DcpptSoapPasienModel{
			AseskepPerolehanInfo:       req.PerolehanInfo,
			AseskepPerolehanInfoDetail: req.PerolehanInfoDetail,
			AseskepCaraMasuk:           req.CaraMasuk,
			AseskepAsalMasuk:           req.AsalMasuk,
			AseskepAsesFungsional:      req.FungsiAktivitas,
			AseskepReaksiAlergi:        req.ReaksiAlergi,
			AseskepKel:                 req.KeluhanUtama,
			AseskepRwytPnykt:           req.RiwayatPenyakitSekarang,
			AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
		}

		_, err := su.soapRepository.OnUpdateDcpptSoapPasien(data1, kdBagian, req)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return res, message, errors.New(message)
		}

		return res, "Data berhasil diupate", nil
	}

}

func (su *soapUseCase) OnSaveResikoJatuhGoUpAndGoTestIGDUseCase(kdBagian string, userID string, req dto.ReqResikoJatuhGoUpAndGoTest) (res dto.ResponseResikoJatuhGoUpGoTest, message string, err error) {
	// GET DATA TERLEBIH DAHULU
	// CARI APAKAH ADA DATA DI SOAP PASIEN
	soaps, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	if err != nil || soaps.Noreg == "" {
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		var hasilKaji []string

		if len(req.Tindakan) > 3 {
			su.logging.Info("HASIL KAJI DI ISI")
			hasilKaji = strings.SplitAfter(req.Tindakan, "-")
		} else {
			hasilKaji = append(hasilKaji, "")
			hasilKaji = append(hasilKaji, "")
			su.logging.Info("HASIL KAJI KOSONG")
		}

		data1 := soap.DcpptSoapPasienModel{
			InsertDttm:           times.Format("2006-01-02 15:04:05"),
			UpdDttm:              times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:     req.Person,
			InsertUserId:         userID,
			InsertPc:             req.DeviceID,
			TglMasuk:             tanggal.Tanggal[0:10],
			KdBagian:             kdBagian,
			Noreg:                req.Noreg,
			AseskepRj1:           req.ResikoJatuh1,
			AseskepRj2:           req.ResikoJatuh2,
			Pelayanan:            req.Pelayanan,
			AseskepHslKajiRj:     hasilKaji[0],
			AseskepHslKajiRjTind: hasilKaji[1],
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(data1)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return res, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		mapper := su.soapMapper.ToResikoJatuhMapper(simpan)

		return mapper, "Data berhasil disimpan", nil
	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		update, err := su.soapRepository.UpdateResikoJatuhRepository(req.Noreg, kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Data gagal diubah %s", err.Error())
			return res, message, errors.New(message)
		}
		mapper := su.soapMapper.ToMappingResikoJatuhGoUpAndGoTest(update)
		return mapper, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) DoubleCheckHighUseCase(userID string, kdBagian string, req dto.ReqSaveDoubleCheck) (message string, err error) {
	_, er121 := su.soapRepository.OnSaveDobleCheckHighReporitory(userID, kdBagian, req.Devices, req)

	if er121 != nil {
		messages := fmt.Sprintf("Error %s, Data gagal disimpan", er121.Error())
		return messages, errors.New(message)
	}

	return "Data berhasil disimpan", nil
}

func (su *soapUseCase) SimpanTriaseUseCaseV2(kdBagian string, req dto.RegTriaseV2) (res soap.TriaseModel, message string, err error) {
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, kdBagian)
	if err != nil || soap.Noreg == "" {
		// SIMPAN DATA YANG BARU
		su.logging.Info("SIMPAN DATA TRISE")
		su.logging.Info(req)
		insert, err := su.soapRepository.InsertTriaseRepositoryV2(req.NoReg, kdBagian, req)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return insert, message, errors.New(message)
		}

		return insert, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE DATA TRISE")
		su.logging.Info(req)
		update, err := su.soapRepository.UpdateTriasRepositoryV2(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Update data gagal", err.Error())
			return update, message, errors.New(message)
		}
		return update, "Data berhasil di update", nil
	}
}

func (su *soapUseCase) SearchDcpptSoapPasienDokterUsecase(noReg string, kdBagian string, person string) (res dto.ResDcpptSoap, err error) {
	su.logging.Info("Keterangan person")
	su.logging.Info(person)
	if person == "Dokter" {
		soap, errs := su.soapRepository.SearchDcpptSoapDokterRepository(noReg, kdBagian)
		return soap, errs
	} else {
		soap, errs := su.soapRepository.SearchDcpptSoapPasienRepository(noReg, kdBagian)
		return soap, errs
	}
}

func (su *soapUseCase) SaveTriaseRiwayatAlergiUsecase(kdBagian string, req dto.ReqSaveTriaseRiwayatAlergi) (res soap.TriaseRiwayatAlergi, message string, err error) {

	soap, err1 := su.SearchDcpptSoapPasienDokterUsecase(req.Noreg, kdBagian, req.KeteranganPerson)

	su.logging.Info("DATA SOAP")
	su.logging.Info(soap)

	if err1 != nil || soap.Noreg == "" {
		// GET TANGGAL MASUK PASIEN
		su.logging.Info("CARI TANGGAL")
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		// SIMPAN DATA YANG BARU
		su.logging.Info("SIMPAN DATA TRISE")
		insert, err := su.soapRepository.SaveTriaseRiwayatAlergiRepository(req.Noreg, kdBagian, tanggal.Tanggal, tanggal.Jam, req)

		if err != nil {
			su.logging.Info("DATA TRIASE GAGAL DI DISIMPAN")
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return insert, message, errors.New(message)
		}

		su.logging.Info("Data berhasil disimpan")
		return insert, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE DATA TRISE")
		su.logging.Info(req)
		insert, err := su.soapRepository.UpdateTriaseRiwayatAlergiRepository(req.Noreg, kdBagian, req)

		if err != nil {
			su.logging.Info("DATA TRIASE GAGAL DI UBAH")
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return insert, message, errors.New(message)
		}

		return insert, "Data berhasil diupdate", nil
	}
}

func (su *soapUseCase) OnGetDoubleCheckUseCase(kdBagian string, noReg string) (err error) {
	return nil
}

func (su *soapUseCase) SaveKeluhanUtamaIGDUseCase(kdBagian string, data dto.RegSaveKeluhanUtamaIGD) (res soap.KeluhanUtamaPerawatIGD, message string, err error) {
	soap, err2 := su.soapRepository.GetDcpptSoapPasien(kdBagian, data.Noreg)
	su.logging.Info("DATA SOAP")
	su.logging.Info(soap)

	if err2 != nil || soap.Noreg == "" {
		su.logging.Info("CARI TANGGAL")
		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(data.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		// INSERT DATA KELUHAN UTAMA IGD
		save, err1 := su.soapRepository.SaveKeluhanUtamaIGDRepository(kdBagian, tanggal.Tanggal[0:10], data)

		if err1 != nil {
			su.logging.Info("DATA TIDAK DAPAT DI SIMPAN")
			message := fmt.Sprintf("%s,", errors.New(err1.Error()))
			return res, message, errors.New(message)
		}

		return save, "Data berhasil disimpan", nil

	} else {
		su.logging.Info("UPDATE DATA KELUHAN UTAMA IGD ")
		update, errrn := su.soapRepository.UpdateKeluhanUtamaIGDRepository(data.Noreg, kdBagian, data)

		if errrn != nil {
			su.logging.Info("DATA  GAGAL DI UBAH")
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errrn.Error())
			return update, message, errors.New(message)
		}

		return update, "Data berhasil diupdate", nil
	}
}

func (su *soapUseCase) SavePemeriksaanFisikUseCaseV2(kdBagian string, req dto.RequestSavePemeriksaanFisikV2) (res soap.PemeriksaanFisikModel, message string, err error) {
	// CEK APAKAH ADA DATANYA
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA ")
		data, err := su.soapRepository.InsertPemeriksaanFisikRepositoryV2(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return data, message, errors.New(message)
		}
		su.logging.Info(req)

		return data, "Data berhasil di simpan", nil
	} else {
		su.logging.Info("UPDATE DATA")
		data, err := su.soapRepository.UpdatePemeriksaanFisikRepositoryV2(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", err.Error())
			return data, message, errors.New(message)
		}
		su.logging.Info(req)

		return data, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) InputRencanaTindakLanjutUseCaseV2(req dto.RequestRencanaTindakLanjutV2, kdBagian string, userID string) (res soap.RencanaTindakLanjutModel, message string, err error) {
	// CARI DATA, APAKAH ADA ATAU TIDAK
	soap, errs := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, kdBagian)

	su.logging.Info(soap)

	if errs != nil || soap.Noreg == "" {

		// =========================== AMBIL TANGGAL MASUK ======================== //
		su.logging.Info("SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.NoReg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		value, er := su.soapRepository.InsertRencanaTindakLanjutRepositoryV2(req, kdBagian, tanggal.Tanggal[0:10], tanggal.Jam, userID)

		if er != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", er.Error())
			return value, message, errors.New(message)
		}

		return value, "Data berhasil disimpan", nil

	} else {

		su.logging.Info("UPATE DATA RENCANA TINDAK LANJUT")
		value, er := su.soapRepository.UpdateRencanaTindakLanjutRepositoryV2(req, kdBagian)

		if er != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", er.Error())
			return value, message, errors.New(message)
		}

		return value, "Data berhasil diubah", nil
	}
}

// SAVE ASESMEN KEPERAWATAN BIDAN
func (su *soapUseCase) SaveKeperawatanBidanUseCaseV2(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidanV2) (res soap.AsesmedKeperawatanBidan, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	// JIKA TIDAK ADA DATA
	if err != nil || soap.Noreg == "" {
		// LAKUKAN  SAVE DATA
		su.logging.Info("SIMPAN DATA ")
		data, err := su.soapRepository.InsertAsesmenKeperawatanBidanRepositoryV2(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Data berhasil di simpan", nil
	} else {
		data, err := su.soapRepository.UpdateAsesmenKeperawatanBidanRepositoryV2(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Data berhasil diubah", nil
	}
}

// SAVE ASESMEN INFO KELUAR IGD HANDLER USECASE
func (su *soapUseCase) SaveAsesmenInfoKeluarUsecase(kdBagian string, userID string, req dto.ReqAsesmenInformasiKeluhanIGD) (
	data soap.DcpptSoapPasienModel, message string, err error) {

	// CARI APAKAH ADA DATA DI SOAP PASIEN
	soaps, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	if err != nil || soaps.Noreg == "" {

		su.logging.Info("SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		// BUAT
		su.logging.Info("BUAT MODEL INFORMASI KELUHAN")
		var hasilKaji []string

		if len(req.AseskepHslKajiRj) > 3 {
			su.logging.Info("HASIL KAJI DI ISI")
			hasilKaji = strings.SplitAfter(req.AseskepHslKajiRj, "-")
		} else {
			hasilKaji = append(hasilKaji, "")
			hasilKaji = append(hasilKaji, "")
			su.logging.Info("HASIL KAJI KOSONG")
		}

		su.logging.Info(hasilKaji)

		su.logging.Info("LAKUKAN PARSING RESIKO JATUH")

		var resikoJatuh1 = ""
		var resikoJatuh2 = ""

		if len(req.AseskepRj1) > 1 {
			switch req.AseskepRj1 {
			case "Ya":
				resikoJatuh1 = "true"
			case "Tidak":
				resikoJatuh1 = "false"
			default:
				resikoJatuh1 = ""
			}

		}

		if len(req.AseskepRj2) > 1 {
			switch req.AseskepRj2 {
			case "Ya":
				resikoJatuh2 = "true"
			case "Tidak":
				resikoJatuh2 = "false"
			default:
				resikoJatuh2 = ""
			}
		}

		data = soap.DcpptSoapPasienModel{
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			UpdDttm:                    times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:           req.Person,
			InsertUserId:               userID,
			InsertPc:                   req.DeviceID,
			TglMasuk:                   tanggal.Tanggal[0:10],
			KdBagian:                   kdBagian,
			Noreg:                      req.Noreg,
			AseskepPerolehanInfo:       req.AseskepPerolehanInfo,
			AseskepPerolehanInfoDetail: req.AseskepPerolehanInfoDetail,
			AseskepCaraMasuk:           req.AseskepCaraMasuk,
			AseskepCaraMasukDetail:     req.AseskepAsalMasukDetail,
			AseskepAsalMasuk:           req.AseskepAsalMasuk,
			AseskepAsalMasukDetail:     req.AseskepAsalMasukDetail,
			AseskepBb:                  req.Aseskepbb,
			AseskepTb:                  req.AseskepTb,
			AseskepRwytPnykt:           req.AseskepRwytPnykt,
			AseskepRwytObat:            req.AseskepRwytObat,
			AseskepAsesFungsional:      req.AseskepAsesFungsional,
			AseskepRj1:                 resikoJatuh1,
			AseskepRj2:                 resikoJatuh2,
			Pelayanan:                  req.Pelayanan,
			AseskepHslKajiRj:           hasilKaji[0],
			AseskepHslKajiRjTind:       hasilKaji[1],
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(data)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil

	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		// ================ LAKUKAN UPDATE DATA  ========================= //
		update, err := su.soapRepository.UpdateInformasiKeluhan(req.Noreg, kdBagian, req)

		if err != nil {
			su.logging.Info("DATA GAGAL DI UBAH")
			message := fmt.Sprintf("Data gagal diubah %s", err.Error())
			return data, message, errors.New(message)
		}

		return update, "Data berhasil diubah", nil
	}
}

// ======================= //
func (su *soapUseCase) SaveSkriningResikoDekubitusUsecase(kdBagian string, userID string, req dto.ReqSkriningDekubitusIGD) (data soap.DcpptSoapPasienModel, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap1, err1 := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	su.logging.Info(soap1)
	su.logging.Info(err1)
	// JIKA TIDAK ADA DATA
	if err1 != nil || soap1.Noreg == "" {
		// LAKUKAN SIMPAN DATA
		// JIKA DATA TIDAK ADA LAKUKAN SAVE
		// LAKUKAN  SAVE DATA
		// AMBIL TANGGAL MASUK PASIEN

		su.logging.Info("SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		datas := soap.DcpptSoapPasienModel{
			InsertDttm:           times.Format("2006-01-02 15:04:05"),
			UpdDttm:              times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:     req.Person,
			InsertUserId:         userID,
			InsertPc:             req.DeviceID,
			TglMasuk:             tanggal.Tanggal[0:10],
			KdBagian:             kdBagian,
			Noreg:                req.Noreg,
			AseskepDekubitus1:    req.Dekubitus1,
			AseskepDekubitus2:    req.Dekubitus2,
			AseskepDekubitus3:    req.Dekubitus3,
			AseskepDekubitus4:    req.Dekubitus4,
			AseskepDekubitusAnak: req.DekubitusAnak,
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(datas)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil
	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		// ================ LAKUKAN UPDATE DATA
		update, err := su.soapRepository.UpdateSkriningResikoDekubitus(req.Noreg, kdBagian, req)

		if err != nil {
			su.logging.Info("DATA GAGAL DI UBAH")
			message := fmt.Sprintf("Data gagal diubah %s", err.Error())
			return data, message, errors.New(message)
		}

		return update, "Data berhasil diubah", nil
	}

}

func (su *soapUseCase) SaveRiwayatKehamilanUsecase(kdBagian string, userID string, req dto.RequestKehamilanIGD) (data soap.DcpptSoapPasienModel, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap1, err1 := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)
	// JIKA TIDAK ADA DATA
	if err1 != nil || soap1.Noreg == "" {
		// LAKUKAN SIMPAN DATA
		// JIKA DATA TIDAK ADA LAKUKAN SAVE
		// LAKUKAN  SAVE DATA
		// AMBIL TANGGAL MASUK PASIEN

		su.logging.Info("SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		datas := soap.DcpptSoapPasienModel{
			InsertDttm:       times.Format("2006-01-02 15:04:05"),
			UpdDttm:          times.Format("2006-01-02 15:04:05"),
			KeteranganPerson: req.Person,
			InsertUserId:     userID,
			InsertPc:         req.DeviceID,
			TglMasuk:         tanggal.Tanggal[0:10],
			KdBagian:         kdBagian,
			Noreg:            req.Noreg,
			// ============================
			// ============================ SIMPAN RIWAYAT KEHAMILAN
			Pelayanan:                req.Pelayanan,
			AseskepKehamilan:         req.Kehamilan,
			AseskepKehamilanGravida:  req.KehamilanGravida,
			AseskepKehamilanPara:     req.KehamilanPara,
			AseskepKehamilanAbortus:  req.KehamilanAbortus,
			AseskepKehamilanHpht:     req.KehamilanHpht,
			AseskepKehamilanTtp:      req.KehamilanTtp,
			AseskepKehamilanLeopold1: req.KehamilanLeopold1,
			AseskepKehamilanLeopold2: req.KehamilanLeopold2,
			AseskepKehamilanLeopold3: req.KehamilanLeopold3,
			AseskepKehamilanLeopold4: req.KehamilanLeopold4,
			AseskepKehamilanDjj:      req.KehamilanDjj,
			AseskepKehamilanVt:       req.KehamilanVt,
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(datas)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil
	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		// ================ LAKUKAN UPDATE DATA
		update, err1 := su.soapRepository.UpdateRiwayatKehamilanRepository(req.Noreg, kdBagian, req)

		if err1 != nil {
			su.logging.Info("DATA GAGAL DI UBAH")
			message := fmt.Sprintf("Data gagal diubah %s", err1.Error())
			return data, message, errors.New(message)
		}

		return update, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) GetVitalSignBangsalUsecase(kdBagian string, noReg string) (res dto.ResponseVitalSignBangsal) {
	/// GET DATA DARI SOAP PASIEN
	su.logging.Info("GET VITAL SIGN BANGSAL USECASE")
	su.logging.Info(kdBagian)
	su.logging.Info(noReg)
	soapPasien, err := su.soapRepository.GetDcpptSoapPasien(kdBagian, noReg)
	su.logging.Info(soapPasien)
	/// GET DATA DARI PEMERIKSAAN FISIK

	if err != nil {
		su.logging.Info("DATA GAGAL DI UBAH")
		message := fmt.Sprintf("Data gagal didapat %s", err.Error())
		su.logging.Info(message)
	}

	su.logging.Info("GET DATA SOAP PASIEN")
	su.logging.Info(soapPasien)

	fisik, _ := su.soapRepository.GetPemeriksaanFisikRepository(kdBagian, noReg)
	su.logging.Info("GET DATA PEMERIKSAAN FISIK")
	su.logging.Info(fisik)

	// MAPPING DATA KE VITA SIGN BANGSAL

	maper := su.soapMapper.ToMapperVitalSignBangsal(soapPasien, fisik)
	su.logging.Info("MAPING DATA")
	su.logging.Info(maper)

	return maper
}

func (su *soapUseCase) SaveKeadaanUmumUseCase(kdBagian string, noReg string, userID string, req rmeDto.ReqKeadaanUmumBangsal) (datas soap.DcpptSoapPasienModel, message string, err error) {
	// CHEK APAKAH DATA SUDAH ADA PADA HARI INI
	// CHECK PEMERIKSAAN FISIK
	fisik, _ := su.soapRepository.SearchDcpptSoapPasienBangsalRepository(noReg, userID, kdBagian)
	su.logging.Info("CARI DATA VITAL SIGN")

	// GET
	if len(fisik.Noreg) > 1 || fisik.Noreg != "" {
		// UPDATA DATA KE YANG BARU
		// LAKUKAN UPDATE PADA KESADARAN UMUM DI TABLE SOAP PASIEN
		su.logging.Info("LAKUKAN UPDATE DATA ")
		su.logging.Info("UPDATE DATA")

		data, err1 := su.soapRepository.UpdateKeadaanUmumBangsalRepository(noReg, kdBagian, userID, req)

		if err1 != nil {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA GAGAL DIUBAH"))
			return datas, message, errors.New(message)
		}

		return data, "Data berhasil disimpan", nil

	} else {
		// SIMPAN DATA KE YANG BARU
		su.logging.Info("SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return datas, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		data := soap.DcpptSoapPasienModel{
			InsertDttm:             times.Format("2006-01-02 15:04:05"),
			UpdDttm:                times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:       req.Person,
			InsertUserId:           userID,
			InsertPc:               req.DeviceID,
			TglMasuk:               tanggal.Tanggal[0:10],
			KdBagian:               kdBagian,
			Noreg:                  req.Noreg,
			AseskepKeadaanUmum:     req.KeadaanUmum,
			AseskepKesadaran:       req.Kesadaran,
			AseskepKesadaranDetail: req.KesadaranDetail,
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(data)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil
	}

}

func (su *soapUseCase) OnGetTandaVitalIGDDokterUserCase(userID string, kdBagian string, person string, noReg string, pelayanan string) (res dto.TandaVitalIGDResponse, err error) {

	vital, errs := su.soapRepository.GetTandaVitalIGDRepository(userID, kdBagian, person, noReg, pelayanan)

	if errs != nil {
		message := fmt.Sprintf("Data gagal didapat, %s,", errs.Error())
		return res, errors.New(message)
	}

	// GET PEMFISIK IGD DOKTER
	fisik, _ := su.soapRepository.GetPemFisikIGDDokterRepository(userID, kdBagian, person, noReg)
	mapper := su.soapMapper.ToMapperResponseIGDDokter(vital, fisik)

	return mapper, nil
}

func (su *soapUseCase) OnGetTandaVitalIGDPerawatUserCase(kdBagian string, person string, noReg string, pelayanan string) (res dto.TandaVitalIGDResponse, err error) {
	vital, errs := su.soapRepository.GetTandaVitalIGDPerawatRepository(kdBagian, person, noReg, pelayanan)

	if errs != nil {
		message := fmt.Sprintf("Data gagal didapat, %s,", errs.Error())
		return res, errors.New(message)
	}

	// GET PEMFISIK IGD DOKTER
	fisik, _ := su.soapRepository.GetPemFisikIGDPerawatRepository(kdBagian, person, noReg)

	mapper := su.soapMapper.ToMapperResponseIGDDokter(vital, fisik)

	return mapper, nil
}

func (su *soapUseCase) OnSaveTandaVitalIGDDokterUseCase(userID string, kdBagian string, person string, noReg string, deviceID string, req dto.ReqSaveTandaVitalIGDDokter) (res dto.TandaVitalIGDResponse, message string, err error) {

	vital, err := su.soapRepository.GetTandaVitalIGDRepository(userID, kdBagian, person, noReg, req.Pelayanan)

	times := time.Now()

	su.logging.Info("TANGGAL")
	su.logging.Info(times.Format("2006-01-02 15:04:05"))

	if err != nil || vital.Noreg == "" {
		// SIMPAN DATA
		su.logging.Info("INSERT DATA TANDA VITAL")

		datas1 := soap.DVitalSignIGDDokter{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertUserId: userID,
			KdBagian:     kdBagian,
			InsertDevice: deviceID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Noreg:        req.Noreg,
			Pelayanan:    req.Pelayanan,
			Td:           req.Td,
			Nadi:         req.Nadi,
			Suhu:         req.Suhu,
			Spo2:         req.Spo2,
			Pernafasan:   req.Pernafasan,
			Tb:           req.Tb,
			Bb:           req.Bb,
		}

		tandaVital, _ := su.soapRepository.InsertTandaVitalIGDRepository(datas1)

		datas2 := soap.DPemFisikIGDDokterRepository{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertUserId: userID,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Pelayanan:    req.Pelayanan,
			InsertDevice: deviceID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			GcsE:         req.GCSE,
			GcsV:         req.GCSV,
			GcsM:         req.GCSM,
			Kesadaran:    req.Kesadaran,
			Akral:        req.Akral,
			Pupil:        req.Pupil,
		}

		pemfisik, _ := su.soapRepository.InsertPemFisikIGDDokterIGDRepository(datas2)

		mapper := su.soapMapper.ToMapperResponseIGDDokter(tandaVital, pemfisik)

		return mapper, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE  DATA TANDA VITAL")

		// UPDATE
		pemFisik, e12 := su.soapRepository.UpdatePemFisikIGDDokterRespository(kdBagian, req)

		if e12 != nil {
			su.logging.Info(e12)
		}

		updateTandaVital, errw := su.soapRepository.UpdateTandaVitalIGDDokterRespository(userID, kdBagian, req)

		if errw != nil {
			su.logging.Info(e12)
		}

		mapper := su.soapMapper.ToMapperResponseIGDDokter(updateTandaVital, pemFisik)

		return mapper, "Data berhasil diupdate", nil

	}

}

func (su *soapUseCase) OnGetReportPengkajianAwalKeperawatanRawatInapUseCase(req dto.ReqReportPengkajianAwalKeperawatan, kdBagian string, userID string) (res dto.ResponsePengkajianKeperawatanRawatInap, err error) {
	// GET DATA PENGKAJIAN KEPERWATAN RAWAT INAP
	// pengkajian, _ := su.soapRepository.OnGetPengkajianKeperawatanDewasaRepository(req.Noreg, kdBagian, userID)
	// perawat, _ := su.soapRepository.OnGetPengkajianKeperawatanDewasaRepository(req.Noreg, kdBagian, userID)
	// alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	// riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

	// mapepr := su.soapMapper.TOMappingResponsePengkajianKeperawatan(perawat, alergi, riwayatPerawat)

	// ToMappingResponsePengkajiaKeperawatanRawatInap(pengkajian soap.PengkajianAwalKeperawatan) (res dto.ResponsePengkajianKeperawatanRawatInap)
	// mapper := su.soapMapper.ToMappingResponsePengkajiaKeperawatanRawatInap(pengkajian)

	return res, nil
}

func (su *soapUseCase) OnGetTandaReportVitalIGDDokterUserCase(kdBagian string, person string, noReg string) (res dto.TandaVitalIGDResponse, err error) {

	vital, errs := su.soapRepository.GetTandaVitalReportIGDDokterRepository(kdBagian, person, noReg)

	su.logging.Info("VITAL DI DAPAT")
	su.logging.Info(vital)
	su.logging.Info(vital)

	if errs != nil {
		message := fmt.Sprintf("Data gagal didapat, %s,", errs.Error())
		return res, errors.New(message)
	}

	// GET PEMFISIK IGD DOKTER
	fisik, _ := su.soapRepository.GetReportPemFisikIGDDokterRepository(kdBagian, person, noReg)

	su.logging.Info("FISIK DI DAPAT")
	su.logging.Info(fisik)

	mapper := su.soapMapper.ToMapperResponseIGDDokter(vital, fisik)

	return mapper, nil
}
