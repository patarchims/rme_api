package usecase

import (
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"time"
)

func (su *soapUseCase) GetPengkajianAwalUsecase(userID string, kdBagian string, req dto.ReqAsesmenKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error) {
	perawat, _ := su.soapRepository.OnGetPengkajianKeperawatanDewasaRepository(req.Noreg, kdBagian, userID)
	alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

	mapepr := su.soapMapper.TOMappingResponsePengkajianKeperawatan(perawat, alergi, riwayatPerawat)

	return mapepr, message, nil
}

func (su *soapUseCase) OnGetAsesmenAnakUseCase(userID string, kdBagian string, req dto.ReqAsesmenAnak) (res dto.ResponseAsesmenAnak, err error) {
	alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	perawat, _ := su.soapRepository.OnGetPengkajianAnakRepository(req.Noreg, kdBagian)
	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)
	mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, perawat, riwayatPerawat)
	return mapper, nil
}

// GET PENGKAJIAN AWAL KEPERAWATAN ANAK USECASE
func (su *soapUseCase) OnGetPengkajianAwalKeperawatanAnakUseCase(userID string, kdBagian string, req dto.ReqAsesmenKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error) {
	perawat, _ := su.soapRepository.OnGetPengkajianKeperawatanDewasaRepository(req.Noreg, kdBagian, userID)
	alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
	riwayatPerawat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

	mapepr := su.soapMapper.TOMappingResponsePengkajianKeperawatan(perawat, alergi, riwayatPerawat)
	return mapepr, message, nil
}

func (su *soapUseCase) OnSavePengkajianAwalKeperawatanUseCase(userID string, kdBagian string, req dto.ReqSavePengkajianKeperawatan) (res dto.ResponseAsesmenKeperawatan, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA

	times := time.Now()

	// GET PENGKAJIAN AWAL DEWASA
	penkajian, err1 := su.soapRepository.OnGetPengkajianKeperawatanDewasaRepository(req.Noreg, kdBagian, userID)

	if err1 != nil || penkajian.Noreg == "" {
		// GET JAM MASUK DARI IGD // PENAMBAHAN RIWAYAT PENYAKIT DAHULU
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		kaper := soap.PengkajianAwalKeperawatan{
			InsertUserId:               userID,
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			TglMasuk:                   tanggal.Tanggal[0:10],
			KdBagian:                   kdBagian,
			KeteranganPerson:           "Dewasa",
			Pelayanan:                  req.Pelayanan,
			Noreg:                      req.Noreg,
			KdDpjp:                     req.Dpjp,
			AseskepPerolehanInfo:       req.AseskepJenpel,
			InsertPc:                   req.DeviceID,
			AseskepPerolehanInfoDetail: req.AseskepJenpel,
			AseskepRwytPnykt:           req.AseskepRwytPnykt,
			AseskepKel:                 req.AseskepKel,
			AseskepReaksiAlergi:        req.AseskepReaksiAlergi,
			AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
		}

		perawat, errs := su.soapRepository.OnSavePengkajianKeperawatanRepository(kaper)

		alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

		if errs != nil {
			mapper := su.soapMapper.TOMappingResponsePengkajianKeperawatan(kaper, alergi, riwayat)
			return mapper, "Data gagal disimpan", errs
		}

		mapper := su.soapMapper.TOMappingResponsePengkajianKeperawatan(perawat, alergi, riwayat)

		return mapper, "Data berhasil disimpan", errs
	} else {
		// UPDATE DATA
		su.logging.Info("UPDATE DATA PENGKAJIAN KEPERAWATAN")

		alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
		update, err1 := su.soapRepository.OnUpdatePengkajianKeperawatanDewasaRepository(req, kdBagian)
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

		if err1 != nil {
			mapper := su.soapMapper.TOMappingResponsePengkajianKeperawatan(penkajian, alergi, riwayat)
			return mapper, "Data gagal disimpan", err1
		}

		mapper := su.soapMapper.TOMappingResponsePengkajianKeperawatan(update, alergi, riwayat)

		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *soapUseCase) OnSavePengkajianAwalAnakUseCase(userID string, kdBagian string, req dto.ReqSavePengkajianAnak) (res dto.ResponseAsesmenAnak, message string, err error) {
	times := time.Now()

	penkajian, err1 := su.soapRepository.OnGetPengkajianAnakRepository(req.Noreg, kdBagian)

	if err1 != nil || penkajian.Noreg == "" {
		tanggal, _ := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		kaper := soap.PengkajianAwalKeperawatan{
			InsertUserId:               userID,
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			TglMasuk:                   tanggal.Tanggal[0:10],
			KdBagian:                   kdBagian,
			KeteranganPerson:           "Anak",
			Pelayanan:                  req.Pelayanan,
			Noreg:                      req.Noreg,
			KdDpjp:                     req.Dpjp,
			AseskepPerolehanInfo:       req.AseskepJenpel,
			InsertPc:                   req.DeviceID,
			AseskepPerolehanInfoDetail: req.AseskepJenpel,
			AseskepRwytPnykt:           req.AseskepRwytPnykt,
			AseskepKel:                 req.AseskepKel,
			AseskepReaksiAlergi:        req.AseskepReaksiAlergi,
			AseskepRwytPnyktDahulu:     req.RiwayatPenyakitDahulu,
			AseskepRwtImunisasi:        req.RwtImunisasi,
			AseskepRwtKelahiran:        req.RwtKelahiran,
		}

		perawat, errs := su.soapRepository.OnSavePengkajianKeperawatanRepository(kaper)

		alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

		if errs != nil {
			mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, perawat, riwayat)
			return mapper, "Data gagal disimpan", errs
		}

		mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, perawat, riwayat)

		return mapper, "Data berhasil disimpan", nil
	} else {
		// UPDATE DATA
		su.logging.Info("UPDATE DATA PENGKAJIAN KEPERAWATAN")

		alergi, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(req.NoRm)
		update, err1 := su.soapRepository.OnUpdatePengkajianAnakRepository(req, kdBagian)
		riwayat, _ := su.rmeRepository.OnGetRiwayatPenyakitDahuluPerawat(req.NoRm, req.Tanggal)

		if err1 != nil {
			mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, penkajian, riwayat)
			return mapper, "Data gagal disimpan", err1
		}

		mapper := su.soapMapper.ToMapperResponseAsesmenAnak(alergi, update, riwayat)

		return mapper, "Data berhasil diupdate", nil
	}
}

func (su *soapUseCase) OnSaveTandaVitalKeperawatanUseCase(userID string, kdBagian string, person string, noReg string, deviceID string, req dto.ReqSaveVitalSignKeperawatanBangsal) (res dto.TandaVitalBangsalKeperawatanResponse, message string, err error) {

	vital, err := su.soapRepository.GetVitalSignKeperawatanBangsalRepository(userID, kdBagian, person, noReg)

	times := time.Now()
	if err != nil || vital.Noreg == "" {
		// SIMPAN DATA
		su.logging.Info("INSERT DATA TANDA VITAL")

		datas1 := soap.DVitalSignKeperawatanBangsal{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertUserId: userID,
			KdBagian:     kdBagian,
			InsertDevice: deviceID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			Bb:           req.BeratBadan,
			Tb:           req.TinggiBadan,
			Td:           req.Td,
			Noreg:        req.Noreg,
			Pelayanan:    req.Pelayanan,
			Pernafasan:   req.Pernafasan,
			Nadi:         req.Nadi,
			Suhu:         req.Suhu,
			Spo2:         req.Spo2,
		}

		tandaVital, _ := su.soapRepository.InsertVitalSignKeperawatanBangsalRepository(datas1)

		datas2 := soap.DPemFisikKeperawatanBangsalRepository{
			InsertDttm:   times.Format("2006-01-02 15:04:05"),
			InsertUserId: userID,
			KdBagian:     kdBagian,
			Noreg:        req.Noreg,
			Pelayanan:    req.Pelayanan,
			InsertDevice: deviceID,
			Kategori:     req.Pelayanan,
			KetPerson:    req.Person,
			GcsE:         req.GCSE,
			GcsV:         req.GCSV,
			GcsM:         req.GCSM,
			Kesadaran:    req.Kesadaran,
			Akral:        req.Akral,
			Pupil:        req.Pupil,
		}

		pemfisik, _ := su.soapRepository.InsertPemFisikKeperawatanBangsalRepository(datas2)

		mapper := su.soapMapper.ToMapperResponseKeperawatanBangsal(tandaVital, pemfisik)

		return mapper, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE  DATA TANDA VITAL")

		// UPDATE
		// UpdatePemFisikKeperawatanBangsalRespository
		pemFisik, e12 := su.soapRepository.UpdatePemFisikKeperawatanBangsalRespository(userID, kdBagian, req)

		if e12 != nil {
			su.logging.Info(e12)
		}

		updateTandaVital, errw := su.soapRepository.UpdateTandaVitalIKeperawatanBangsalRespository(userID, kdBagian, req)

		if errw != nil {
			su.logging.Info(e12)
		}

		mapper := su.soapMapper.ToMapperResponseKeperawatanBangsal(updateTandaVital, pemFisik)

		return mapper, "Data berhasil diupdate", nil
	}

}

func (su *soapUseCase) OnGetTandaVitalKeperawatanBangsalUseCase(userID string, kdBagian string, person string, noReg string) (res dto.TandaVitalBangsalKeperawatanResponse, err error) {

	vital, _ := su.soapRepository.GetVitalSignKeperawatanBangsalRepository(userID, kdBagian, person, noReg)
	fisik, _ := su.soapRepository.GetPemFisikKeperawatanRepository(userID, kdBagian, person, noReg)

	mapper := su.soapMapper.ToMapperResponseKeperawatanBangsal(vital, fisik)

	return mapper, nil
}

func (su *soapUseCase) OnUpdateImplementasiKeperawatanUseCase() (err error) {
	// GET ALL DIMPLEMTANASI KEPERAWATAN

	return nil
}
