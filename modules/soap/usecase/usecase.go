package usecase

import (
	"errors"
	"fmt"
	hisEntity "hms_api/modules/his/entity"
	kebidananDTO "hms_api/modules/kebidanan/dto"
	kebidananEntity "hms_api/modules/kebidanan/entity"
	rmeRepository "hms_api/modules/rme/entity"
	"hms_api/modules/soap"
	"hms_api/modules/soap/dto"
	"hms_api/modules/soap/entity"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
	"github.com/sirupsen/logrus"
)

type soapUseCase struct {
	soapRepository  entity.SoapRepository
	soapMapper      entity.SoapMapper
	kebidananMapepr kebidananEntity.KebidananMapper
	logging         *logrus.Logger
	rmeRepository   rmeRepository.RMERepository
	kebidananRepo   kebidananEntity.KebidananRepository
	hisUsesCase     hisEntity.HisUsecase
}

func NewAntreanUseCase(ur entity.SoapRepository, sm entity.SoapMapper, logging *logrus.Logger, rmeRepository rmeRepository.RMERepository, kebidananRepo kebidananEntity.KebidananRepository, kebidananMapepr kebidananEntity.KebidananMapper, hisUsesCase hisEntity.HisUsecase) entity.SoapUseCase {
	return &soapUseCase{
		soapRepository:  ur,
		soapMapper:      sm,
		logging:         logging,
		rmeRepository:   rmeRepository,
		kebidananRepo:   kebidananRepo,
		kebidananMapepr: kebidananMapepr,
		hisUsesCase:     hisUsesCase,
	}
}

func (su *soapUseCase) PublishImageOdontogramUseCase(noReg string, imgFile string, kdBagian string) (res soap.DcpptOdontogram, err error) {

	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(noReg, kdBagian)

	if err != nil || soap.Noreg == "" {
		res, errs := su.soapRepository.InsertImageOdontogramRepository(kdBagian, noReg, imgFile)
		if errs != nil {
			log.Info().Msg(err.Error())
			return res, errs
		}

		return res, nil
	} else {
		// DELETE IMAGE SEBELUMNYA

		data, err := su.soapRepository.UpdateImageOdontogramRepository(noReg, imgFile)
		if err != nil {
			log.Info().Msg(err.Error())
			return res, err
		}
		return data, nil
	}
}

func (su *soapUseCase) UploadOdontogramUseCase(kdBagian string, noReg string, imgFile string) (res soap.DcpptOdontogram, err error) {

	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(noReg, kdBagian)
	if err != nil || soap.Noreg == "" {
		res, errs := su.soapRepository.InsertDataOdontogramRepository(kdBagian, noReg, imgFile)
		if errs != nil {
			log.Info().Msg(err.Error())
			return res, errs
		}

		return res, nil
	} else {
		data, err := su.soapRepository.UpdateDataOdontogramRepository(noReg, imgFile)
		if err != nil {
			log.Info().Msg(err.Error())
			return res, err
		}
		return data, nil
	}

}

func (su *soapUseCase) GetAssessRawatJalanUseCase(noRm string) (res map[string]interface{}, err error) {
	// GET DATA ODONTOGRAM
	data, err := su.soapRepository.GetRecordOdontogramRepository(noRm)

	m := map[string]any{}
	if err != nil {
		log.Error().Msg(err.Error())
		return res, errors.New(err.Error())
	}
	values := reflect.ValueOf(data)
	typesOf := values.Type()

	// TANGKAP ODONTOGRAM
	pemGigi := su.soapMapper.ToPemeriksaanGigi(data)
	odont := su.soapMapper.ToOdontogramModel(values, typesOf)
	riwayat := su.soapMapper.ToRiwayat(data)
	dataMedik := su.soapMapper.ToDataMedik(data)
	dataOral := su.soapMapper.ToIntraOral(data)

	for i := 0; i < values.NumField(); i++ {
		fmt.Println(values)
	}

	// fmt.Println(values.NumField())
	m["pelayanan"] = data.AsesmedJenpel
	m["kelUtama"] = data.AsesmedKeluhUtama
	m["riwayat"] = riwayat
	m["pemeriksaanGigi"] = pemGigi
	m["dataMedik"] = dataMedik
	m["intraOral"] = dataOral

	if odont != nil {
		m["odontogram"] = odont
	}

	if odont == nil {
		m["odontogram"] = []any{}
	}

	return m, nil
}

func (su *soapUseCase) DeleteOdontogramUsecase(req dto.RequestDeleteOdontogram) (err error) {
	_, errs := su.soapRepository.DeleteOdontogramRepository(req)

	if errs != nil {
		return errs
	}
	return nil
}

func (su *soapUseCase) InputRencanaTindakLanjutUseCase(req dto.RequestRencanaTindakLanjut, kdBagian string) (res soap.RencanaTindakLanjutModel, message string, err error) {
	// CARI DATA,
	// SearchDcpptSoapPasienDokterUsecase
	// su.SearchDcpptSoapPasienDokterUsecase()
	// APAKAH ADA ATAU TIDAK
	soap, errs := su.SearchDcpptSoapPasienDokterUsecase(req.NoReg, kdBagian, req.KetPerson)

	if errs != nil || soap.Noreg == "" {
		su.logging.Info("INSERT DATA RENCANA TINDAK LANJUT")
		value, er := su.soapRepository.InsertRencanaTindakLanjutRepository(req, kdBagian)

		if er != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", errs.Error())
			return value, "Data gagal disimpan", errors.New(message)
		}

		return value, "Data berhasil disimpan", nil

	} else {

		su.logging.Info("UPATE DATA RENCANA TINDAK LANJUT")
		value, er := su.soapRepository.UpdateRencanaTindakLanjutRepository(req, kdBagian)

		if er != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", er.Error())
			return value, "Data gagal diubah", errors.New(message)
		}

		return value, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) InsertOdontogramUseCase(reg dto.RequestSaveOdontogram) (err error) {

	_, errs := su.soapRepository.UpdateOdontogramRepository(reg)

	if errs != nil {
		return errs
	}

	return nil
}

func (su *soapUseCase) OnGetReportPengkajianAwalMedisDokterUseCase(noReg string, kdBagian string, person string, pelayanan string, tanggal string, noRM string) (res kebidananDTO.ReportPengkajianAwalDokterResponse, err error) {
	pemFisik, _ := su.kebidananRepo.OnGetPemeriksaanFisikMedisDokter(noReg)
	vital, _ := su.kebidananRepo.GetTandaVitalIGDDokterRepository(kdBagian, person, noReg, pelayanan)
	asesmen, _ := su.kebidananRepo.GetAsesmenAwalMedisRawatInapReportDokter(noReg, kdBagian, pelayanan)
	dahulu, _ := su.rmeRepository.OnGetRiwayatPenyakitDahulu(noReg, tanggal)
	keluarga, _ := su.rmeRepository.GetRiwayatAlergiKeluargaRepository(noRM)
	diagnosa, _ := su.soapRepository.GetDiagnosaRepositoryReportBangsal(noReg, kdBagian)

	labor, _ := su.hisUsesCase.HistoryLaboratoriumUsecaseV2(noReg)
	radiologi, _ := su.hisUsesCase.HistoryRadiologiUsecaseV2(noReg)

	mapper := su.kebidananMapepr.ToResponseMapperPengkajianAwalDokter(pemFisik, vital, asesmen, dahulu, keluarga, labor, radiologi, diagnosa)
	return mapper, nil
}

func (su *soapUseCase) GetKebutuhanEdukasiUseCase(noreg string) (res map[string]interface{}, err error) {
	data, errs := su.soapRepository.GetKebutuhanEdukasiRespository(noreg)

	if errs != nil {
		return res, errs
	}

	m := map[string]any{}

	mapper := su.soapMapper.KebEdukasi(data)
	tujuan := su.soapMapper.TujuanEdukasi(data)
	kemBelajar := su.soapMapper.KemBelajar(data)
	motBelajar := su.soapMapper.MotivasiBelajar(data)
	nilaiPasien := su.soapMapper.PilihanPasien(data)
	hambatan := su.soapMapper.ToHambatan(data)
	intervensi := su.soapMapper.ToIntervensi(data)
	hasil := su.soapMapper.ToHasil(data)

	m["edukasi"] = mapper
	m["tujuan"] = tujuan
	m["kemampuanBelajar"] = kemBelajar
	m["motivasiBelajar"] = motBelajar
	m["nilaiPasien"] = nilaiPasien
	m["hambatan"] = hambatan
	m["intervensi"] = intervensi
	m["rencanaEdukasi"] = data.AseseduRencanaEdukasi
	m["aseseduTgl"] = data.AseseduTgl.Format("2006-01-02")
	m["aseseduJam"] = data.AseseduJam
	m["aseseduUser"] = data.AseseduUser
	m["hasil"] = hasil

	return m, nil
}

func (su *soapUseCase) SimpanTriaseUseCase(kdBagian string, req dto.RegTriase) (res soap.TriaseModel, message string, err error) {
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.NoReg, kdBagian)
	if err != nil || soap.Noreg == "" {
		// SIMPAN DATA YANG BARU
		su.logging.Info("SIMPAN DATA TRISE")
		su.logging.Info(req)
		insert, err := su.soapRepository.InsertTriaseRepository(req.NoReg, kdBagian, req)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return insert, message, errors.New(message)
		}

		return insert, "Data berhasil disimpan", nil
	} else {
		su.logging.Info("UPDATE DATA TRISE")
		su.logging.Info(req)
		update, err := su.soapRepository.UpdateTriasRepository(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Update data gagal", err.Error())
			return update, message, errors.New(message)
		}
		return update, "Data berhasil di update", nil
	}
}

func (su *soapUseCase) SaveImageLokalisUseCase(kdBagian string, path string, noReg string) (res soap.ImageLokalisModel, message string, err error) {
	soap, err := su.soapRepository.GetImageLokalisRepository(noReg, kdBagian)

	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA LOKALIS")
		data, err := su.soapRepository.InsertImageLokalisRepository(kdBagian, noReg, path)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}
		return data, "Gambar berhasil disimpan", nil
	} else {
		// UPDATE DATA
		// DELETE FILE SEBELUMNYA
		e := os.Remove("images/lokalis/" + soap.AsesmedLokalisImage)

		if e != nil {
			su.logging.Error(e.Error())
		}

		su.logging.Info("UPDATE DATA IMAGE LOKALIS")
		data, err := su.soapRepository.UpdateImageLokalisRepository(kdBagian, noReg, path)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}

		su.logging.Info(noReg)

		return data, "Gambar berhasil diubah", nil
	}
}

func tryRemoveFile(filePath string) error {
	logrus.Info("Attempt to open the file with write permissions to ensure it's not in use")
	file, err := os.OpenFile(filePath, os.O_WRONLY, 0)
	if err != nil {
		return err
	}

	defer file.Close()

	logrus.Info("Now that the file is open, attempt to remove it")
	err = os.Remove(filePath)
	if err != nil {
		return err
	}

	return nil
}

func (su *soapUseCase) SaveImageLokalisUsecase(kdBagian string, noReg string, context *fiber.Ctx) (res soap.ImageLokalisModel, message string, err error) {
	soap, err := su.soapRepository.GetImageLokalisRepository(noReg, kdBagian)

	if soap.Noreg == "" {
		if err != nil {
			message := fmt.Sprintf("Error %s, Data tidak ditemukan", err.Error())
			return res, message, errors.New(message)
		}

		return res, "Lakunan asesmen terlebih dahulu", errors.New(message)

	} else {
		files, err := context.FormFile("imageUrl")

		if err != nil {
			return res, "Gambar gagal di upload", errors.New(message)
		}

		tipe := strings.Split(files.Filename, ".")

		times := time.Now()

		su.logging.Info("LAKUKAN UPLOAD IMAGE")

		path := fmt.Sprintf("%s-%s-%s.%s", times.Format("20060102150405"), noReg, kdBagian, tipe[1])

		_ = context.SaveFile(files, "images/lokalis/"+path)

		// UPDATE DATA // DELETE FILE SEBELUMNYA

		// Attempt to remove the file
		errs := tryRemoveFile("images/lokalis/" + soap.AsesmedLokalisImage)

		if errs != nil {
			fmt.Printf("Error removing file: %v\n", err)
		}

		su.logging.Info("UPDATE DATA IMAGE LOKALIS")
		data, err := su.soapRepository.UpdateImageLokalisRepository(kdBagian, noReg, path)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Gambar berhasil diubah", nil
	}
}

func (su *soapUseCase) SaveImageLokalisPublicUsecase(kdBagian string, noReg string, path string,
	keteranganPerson string, insertUserId string, deviceId string, pelayanan string,
) (res soap.ImageLokalisModel, message string, err error) {
	soap, err := su.soapRepository.GetImageLokalisRepository(noReg, kdBagian)

	if err != nil || soap.Noreg == "" {

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(noReg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return res, message, errors.New(message)
		}

		data, err := su.soapRepository.InsertImageToLokasisRepositoryV2(kdBagian, noReg, path,
			keteranganPerson, insertUserId, deviceId, pelayanan, tanggal.Tanggal[0:10], tanggal.Jam)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}
		return data, "Gambar berhasil disimpan", nil
	} else {
		// UPDATE DATA
		// DELETE FILE SEBELUMNYA
		// Attempt to remove the file
		errs := tryRemoveFile("images/lokalis/" + soap.AsesmedLokalisImage)

		if errs != nil {
			fmt.Printf("Error removing file: %v\n", err)
			su.logging.Error(errs.Error())
		}

		data, err := su.soapRepository.UpdateImageLokalisRepository(kdBagian, noReg, path)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Gambar berhasil diubah", nil
	}
}

func (su *soapUseCase) SaveImageLokalisToPublicUsecase(kdBagian string, noReg string, path string) (res soap.ImageLokalisModel, message string, err error) {
	soap, err := su.soapRepository.GetImageLokalisRepository(noReg, kdBagian)

	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA LOKALIS")
		data, err := su.soapRepository.InsertImageLokalisRepository(kdBagian, noReg, path)

		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}
		return data, "Gambar berhasil disimpan", nil
	} else {
		// UPDATE DATA
		// DELETE FILE SEBELUMNYA
		e := os.Remove("images/lokalis/" + soap.AsesmedLokalisImage)

		if e != nil {
			su.logging.Error(e.Error())
		}

		su.logging.Info("UPDATE DATA IMAGE LOKALIS")
		data, err := su.soapRepository.UpdateImageLokalisRepository(kdBagian, noReg, path)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal di update", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Gambar berhasil diubah", nil
	}
}

func (su *soapUseCase) SavePemeriksaanFisikUseCase(kdBagian string, req dto.RequestSavePemeriksaanFisik) (res soap.PemeriksaanFisikModel, message string, err error) {
	// CEK APAKAH ADA DATANYA
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)
	if err != nil || soap.Noreg == "" {
		su.logging.Info("SIMPAN DATA ")
		data, err := su.soapRepository.InsertPemeriksaanFisikRepository(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return data, message, errors.New(message)
		}
		su.logging.Info(req)

		return data, "Data berhasil di simpan", nil
	} else {
		su.logging.Info("UPDATE DATA ")
		data, err := su.soapRepository.UpdatePemeriksaanFisikRepository(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", err.Error())
			return data, message, errors.New(message)
		}
		su.logging.Info(req)

		return data, "Data berhasil diubah", nil
	}
}

// PILIH PEMERIKSAAN LABOR
func (su *soapUseCase) PilihPemeriksaanLaborUsecase(namaGrup string) (res []soap.PilihPemeriksaanLaborModel, err error) {
	// GET TARIP DARI NAMA GRUP
	tarip, err := su.soapRepository.GetTotalKTaripRepository(namaGrup)

	su.logging.Info(tarip)

	if err != nil {
		return res, nil
	}

	pemeriksaan, err := su.soapRepository.GetPemeriksaanLaborRepository(namaGrup)
	su.logging.Info(pemeriksaan)
	if err != nil {
		return res, nil
	}

	var labor []soap.PilihPemeriksaanLaborModel

	labor = append(labor, soap.PilihPemeriksaanLaborModel{
		TaripKelas: tarip.TaripKelas,
		NameGrup:   tarip.NameGrup,
	})

	for i := 0; i <= len(pemeriksaan)-1; i++ {
		labor = append(labor, soap.PilihPemeriksaanLaborModel{
			Num:         pemeriksaan[i].Urut,
			NameGrup:    pemeriksaan[i].NameGrup,
			Kode:        pemeriksaan[i].Kode,
			Pemeriksaan: pemeriksaan[i].Pemeriksaan,
			TaripKelas:  0,
		})

	}

	return labor, nil
}

func (su *soapUseCase) DetailPemeriksaanLaborUseCase(namaGrup string) (res soap.DetailPemeriksaanLaborModel, err error) {
	// GET TARIP DARI NAMA GRUP
	tarip, err := su.soapRepository.GetTotalKTaripRepository(namaGrup)

	su.logging.Info(tarip)

	if err != nil {
		return res, nil
	}
	var labor soap.DetailPemeriksaanLaborModel

	pemeriksaan, err := su.soapRepository.GetPemeriksaanLaborRepository(namaGrup)

	su.logging.Info(pemeriksaan)

	if err != nil {
		return res, nil
	}

	labor.Pemeriksaan = pemeriksaan
	labor.KtaripTotal.NameGrup = tarip.NameGrup
	labor.KtaripTotal.TaripKelas = tarip.TaripKelas

	return labor, nil
}

// SAVE ASESMEN
// KEPERAWATAN BIDAN
func (su *soapUseCase) SaveKeperawatanBidanUseCase(kdBagian string, req dto.ReqSaveAsesmenKeperawatanBidan) (res soap.AsesmedKeperawatanBidan, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap, err := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)

	// JIKA TIDAK ADA DATA
	if err != nil || soap.Noreg == "" {
		data, err := su.soapRepository.InsertAsesmenKeperawatanBidanRepository(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal disimpan", err.Error())
			return data, message, errors.New(message)
		}

		return data, "Data berhasil di simpan", nil

	} else {
		data, err := su.soapRepository.UpdateAsesmenKeperawatanBidanRepository(kdBagian, req)
		if err != nil {
			message := fmt.Sprintf("Error %s, Data gagal diubah", err.Error())
			return data, message, errors.New(message)
		}
		su.logging.Info(req)

		return data, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) SaveSkriningNyeriUseCase(userID string, kdBagian string, req dto.RequestSkriningNyeriIGD) (data soap.DcpptSoapPasienModel, message string, err error) {

	// CEK APAKAH ADA DATA SEBELUMNYA
	soap1, err1 := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)
	// JIKA TIDAK ADA DATA
	if err1 != nil || soap1.Noreg == "" {

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		datas := soap.DcpptSoapPasienModel{
			InsertDttm:                 times.Format("2006-01-02 15:04:05"),
			UpdDttm:                    times.Format("2006-01-02 15:04:05"),
			KeteranganPerson:           req.Person,
			InsertUserId:               userID,
			InsertPc:                   req.DeviceID,
			TglMasuk:                   tanggal.Tanggal[0:10],
			KdBagian:                   kdBagian,
			Noreg:                      req.Noreg,
			Pelayanan:                  req.Pelayanan,
			AseskepSkalaNyeri:          req.SkalaNyeri,
			AseskepFrekuensiNyeri:      req.FrekuensiNyeri,
			AseskepLamaNyeri:           req.LamaNyeri,
			AseskepNyeriMenjalar:       req.NyeriMenjalar,
			AseskepNyeriMenjalarDetail: req.NyeriMenjalarDetail,
			AseskepKualitasNyeri:       req.Kualitasnyeri,
			AseskepNyeriPemicu:         req.NyeriPemicu,
			AseskepNyeriPengurang:      req.NyeriPengurang,
		}

		su.logging.Info("Lakukan SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapPasien(datas)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil
	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		// ================ LAKUKAN UPDATE DATA
		update, err1 := su.soapRepository.UpdateSkriningNyeriRepository(req.Noreg, kdBagian, req)

		if err1 != nil {
			su.logging.Info("DATA GAGAL DI UBAH")
			message := fmt.Sprintf("Data gagal diubah %s", err1.Error())
			return data, message, errors.New(message)
		}

		return update, "Data berhasil diubah", nil
	}
}

func (su *soapUseCase) SaveTindakLanjutUseCase(userID string, kdBagian string, req dto.RequestInputTindakLanjutIGD) (data soap.DcpptSoapPasienModel, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	soap1, err1 := su.soapRepository.SearchDcpptSoapPasienRepository(req.Noreg, kdBagian)
	// JIKA TIDAK ADA DATA
	if err1 != nil || soap1.Noreg == "" {

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)

		if errs != nil || tanggal.Tanggal == "" {
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// var jam = "";
		times := time.Now()

		if req.CaraKeluar == "Dipulangkan" {
			// BUAT MODEL
			inputTime := req.Jam + ":" + req.Menit
			su.logging.Info(inputTime)
			su.logging.Info(req.Jam)

			datas := soap.DcpptSoapPasienModel{
				InsertDttm:           times.Format("2006-01-02 15:04:05"),
				UpdDttm:              times.Format("2006-01-02 15:04:05"),
				KeteranganPerson:     req.Person,
				InsertUserId:         userID,
				InsertPc:             req.DeviceID,
				TglMasuk:             tanggal.Tanggal[0:10],
				KdBagian:             kdBagian,
				Noreg:                req.Noreg,
				Pelayanan:            req.Pelayanan,
				JamCheckOut:          inputTime,
				CaraKeluar:           req.CaraKeluar,
				AseskepPulang1:       req.AseskepPulang1,
				AseskepPulang1Detail: req.AseskepPulang1Detail,
				AseskepPulang2:       req.AseskepPulang2,
				AseskepPulang2Detail: req.AseskepPulang2Detail,
				AseskepPulang3:       req.AseskepPulang3,
				AseskepPulang3Detail: req.AseskepPulang3Detail,
			}

			su.logging.Info("Lakukan SIMPAN DATA")

			simpan, err := su.soapRepository.SaveDcpptSoapPasien(datas)

			if err != nil {
				su.logging.Info("DATA GAGAL DISIMPAN")
				message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
				return simpan, message, errors.New(message)
			}

			su.logging.Info("SIMPAN DATA BERHASIL")

			return simpan, "Data berhasil disimpan", nil
		} else {
			// BUAT MODEL

			inputTime := req.Jam + ":" + req.Menit
			su.logging.Info(inputTime)
			su.logging.Info(req.Jam)

			datas := soap.DcpptSoapPasienModel{
				InsertDttm:           times.Format("2006-01-02 15:04:05"),
				UpdDttm:              times.Format("2006-01-02 15:04:05"),
				KeteranganPerson:     req.Person,
				InsertUserId:         userID,
				InsertPc:             req.DeviceID,
				TglMasuk:             tanggal.Tanggal[0:10],
				KdBagian:             kdBagian,
				Noreg:                req.Noreg,
				Pelayanan:            req.Pelayanan,
				JamCheckInRanap:      inputTime,
				CaraKeluar:           req.CaraKeluar,
				AseskepPulang1:       req.AseskepPulang1,
				AseskepPulang1Detail: req.AseskepPulang1Detail,
				AseskepPulang2:       req.AseskepPulang2,
				AseskepPulang2Detail: req.AseskepPulang2Detail,
				AseskepPulang3:       req.AseskepPulang3,
				AseskepPulang3Detail: req.AseskepPulang3Detail,
			}

			su.logging.Info("Lakukan SIMPAN DATA")

			simpan, err := su.soapRepository.SaveDcpptSoapPasien(datas)

			if err != nil {
				su.logging.Info("DATA GAGAL DISIMPAN")
				message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
				return simpan, message, errors.New(message)
			}

			su.logging.Info("SIMPAN DATA BERHASIL")

			return simpan, "Data berhasil disimpan", nil
		}

	} else {
		// LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		// ================ LAKUKAN UPDATE DATA

		if req.CaraKeluar == "Dipulangkan" {
			inputTime := req.Jam + ":" + req.Menit + ":00"

			update, err1 := su.soapRepository.UpdateTindakLanjutRepositoryDipulangkan(req.Noreg, kdBagian, req, inputTime)

			if err1 != nil {
				su.logging.Info("DATA GAGAL DI UBAH")
				message := fmt.Sprintf("Data gagal diubah %s", err1.Error())
				return data, message, errors.New(message)
			}

			return update, "Data berhasil diubah", nil

		} else {
			inputTime := req.Jam + ":" + req.Menit + ":00"

			update, err1 := su.soapRepository.UpdateTindakLanjutRepositoryDirawatInap(req.Noreg, kdBagian, req, inputTime)
			if err1 != nil {
				su.logging.Info("DATA GAGAL DI UBAH")
				message := fmt.Sprintf("Data gagal diubah %s", err1.Error())
				return data, message, errors.New(message)
			}

			return update, "Data berhasil diubah", nil
		}

	}
}

func (su *soapUseCase) SaveAsesmenDokterUseCase(userID string, kdBagian string, req dto.RequestAsesmenDokter) (data soap.DcpptSoapDokter, message string, err error) {
	// CEK APAKAH ADA DATA SEBELUMNYA
	// CARI SOAP DOKTER
	dokter, err1 := su.soapRepository.SearchDcpptSoapDokterForAsesmentRepository(userID, req.Noreg, kdBagian)

	if err1 != nil || dokter.Noreg == "" {
		// JIKA DATA DOKTER KOSONG
		su.logging.Info("DATA KOSONG LAKUKAN SIMPAN DATA ")
		su.logging.Info("CARI TANGGAL")

		tanggal, errs := su.soapRepository.GetJamMasukPasienRepository(req.Noreg)
		su.logging.Info(tanggal)

		if errs != nil || tanggal.Tanggal == "" {
			su.logging.Info("DATA TIDAK ADA DI REGISTER")
			message := fmt.Sprintf("%s,", errors.New("DATA PASIEN TIDAK ADA DI REGISTER"))
			return data, message, errors.New(message)
		}

		// BUAT MODEL
		times := time.Now()

		datas := soap.DcpptSoapDokter{
			InsertDttm:       times.Format("2006-01-02 15:04:05"),
			UpdDttm:          times.Format("2006-01-02 15:04:05"),
			KeteranganPerson: req.Person,
			InsertUserId:     userID,
			InsertPc:         req.DeviceID,
			TglMasuk:         tanggal.Tanggal[0:10],
			JamMasuk:         tanggal.Jam,
			KdBagian:         kdBagian,
			Noreg:            req.Noreg,
			Pelayanan:        req.Pelayanan,
			KdDpjp:           userID,
			// ============================ SIMPAN TINDAK LANJUT
			AsesmedJenpel:     req.AsesmedJenpel,
			AsesmedKeluhUtama: req.AsesmedKeluhUtama,
			// AsesmedKeluhTambahan:    req.AsesmedKeluhTambahan,
			AsesmedTelaah:    req.AsesmedTelaah,
			AsesmedMslhMedis: req.AsesmedMslhMedis,

			AsesmedDokterTtdTgl: times.Format("2006-01-02 15:04:05"),
			AsesmedDokterTtdJam: times.Format("15:04:05"),
		}

		su.logging.Info("LAKUKAN SIMPAN DATA")

		simpan, err := su.soapRepository.SaveDcpptSoapDokter(datas)

		if err != nil {
			su.logging.Info("DATA GAGAL DISIMPAN")
			message := fmt.Sprintf("Data gagal disimpan, %s,", err.Error())
			return simpan, message, errors.New(message)
		}

		su.logging.Info("SIMPAN DATA BERHASIL")

		return simpan, "Data berhasil disimpan", nil
	} else {
		// LAKUKAN UPDATE DATA // ================ LAKUKAN UPDATE DATA
		su.logging.Info("UPDATE DATA")
		update, err1 := su.soapRepository.UpdateAsesmenDokterRepository(req.Noreg, kdBagian, userID, req)

		if err1 != nil {
			su.logging.Info("DATA GAGAL DI UBAH")
			message := fmt.Sprintf("Data gagal diubah %s", err1.Error())
			return data, message, errors.New(message)
		}

		return update, "Data berhasil diubah", nil
	}
}
