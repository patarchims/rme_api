package dto

type (
	RegisterUser struct {
		NIK   string `json:"nik"  binding:"required" bson:"nik"`
		IDK   string `json:"idKaryawan"  binding:"required" bson:"idKaryawan"`
		Email string `json:"email"  binding:"required" bson:"email"`
	}

	RegisterUserV2 struct {
		NIK   string `json:"nik" validate:"required" bson:"nik"`
		IDK   string `json:"idKaryawan" validate:"required" bson:"idKaryawan"`
		Email string `json:"email" validate:"required,email" bson:"email"`
	}

	CreateUserAccount struct {
		KodeModul      string `json:"kodeModul"  binding:"required" bson:"kodeModul"`
		Email          string `json:"email"  binding:"required" bson:"email"`
		NIK            string `json:"nik"  binding:"required" bson:"nik"`
		Password       string `json:"password"  binding:"required" bson:"password"`
		RepeatPassword string `json:"repeatPassword"  binding:"required" bson:"repeatPassword"`
	}

	CreateAccountUser struct {
		NIK       string `json:"nik"  binding:"required" bson:"nik"`
		KodeModul string `json:"kodeModul"  binding:"required" bson:"kodeModul"`
		Password  string `json:"password"  binding:"required" bson:"password"`
	}

	CreateAccountUserV2 struct {
		NIK       string `json:"nik"  validate:"required" bson:"nik"`
		KodeModul string `json:"kodeModul"  validate:"required" bson:"kodeModul"`
		Password  string `json:"password"  validate:"required" bson:"password"`
	}

	UserOTPStatus struct {
		Nik     string `json:"nik" bson:"nik"`
		IDK     string `json:"idKaryawan" bson:"idKaryawan"`
		Email   string `json:"email" bson:"email"`
		KodeOtp string `json:"kodeOtp" bson:"kodeOtp"`
		Exp     int    `json:"exp" bson:"exp"`
	}

	UsersResponse struct {
		Nama         string `json:"nama" bson:"nama"`
		Email        string `json:"email" bson:"email"`
		Nik          string `json:"nik" bson:"nik"`
		NoHp         string `json:"no_hp" bson:"no_hp"`
		Bagian       string `json:"bagian" bson:"bagian"`
		KodePoli     string `json:"kodePoli" bson:"kodePoli"`
		Pelayanan    string `json:"pelayanan" bson:"pelayanan"`
		UserStatus   string `json:"user_status" bson:"user_status"`
		Spesialisasi string `json:"spesialisasi"`
		UserId       string `json:"user_id" bson:"user_id"`
		Token        string `json:"token" bson:"token"`
		RefreshToken string `json:"refresh_token" bson:"refresh_token"`
		Person       string `json:"person" bson:"person"`
		Alamat       string `json:"alamat" bson:"alamat"`
		Photo        string `json:"photo" bson:"photo"`
		JenisKelamin string `json:"jenisKelamin" bson:"jenisKelamin"`
		TglLahir     string `json:"tglLahir" bson:"tglLahir"`
		Kota         string `json:"kota" bson:"kota"`
		Usia         int    `json:"usia" bson:"usia"`
		Agama        string `json:"agama" bson:"agama"`
		TglMasuk     string `json:"tglMasuk" bson:"tglMasuk"`
		StatusKawin  string `json:"statusKawin" bson:"statusKawin"`
	}

	VerifyOTP struct {
		Email   string `json:"email" bson:"email"`
		KodeOtp string `json:"kodeOtp" bson:"kodeOtp"`
	}

	VerifyOTPV2 struct {
		Email   string `json:"email" validate:"required, email"  bson:"email"`
		KodeOtp string `json:"kodeOtp" validate:"required" bson:"kodeOtp"`
	}

	SignInRequest struct {
		Email    string `json:"email" bson:"email"`
		Password string `json:"password" bson:"password"`
	}

	RequestLoginEmailAndPassword struct {
		KodeModul string `json:"kodeModul" binding:"required" bson:"kodeModul"`
		Email     string `json:"email" binding:"required,email" bson:"email"`
		Password  string `json:"password" binding:"required" bson:"password"`
	}

	RequestLoginEmailAndPasswordV2 struct {
		KodeModul string `json:"kodeModul" validate:"required" bson:"kodeModul"`
		Email     string `json:"email" validate:"required,email" bson:"email"`
		Password  string `json:"password" validate:"required" bson:"password"`
	}

	RequestLoginIDAndPassword struct {
		KodeModul string `json:"kodeModul" validate:"required" bson:"kodeModul"`
		UserID    string `json:"user_id" validate:"required," bson:"user_id"`
		Password  string `json:"password" validate:"required" bson:"password"`
	}

	RequesLoginWithDevices struct {
		Email       string `json:"email" binding:"required,email" bson:"email"`
		Password    string `json:"password" binding:"required" bson:"password"`
		IDDevice    string `json:"iddevice" binding:"omitempty" bson:"iddevice"`
		NamaDevice  string `json:"namadevice" binding:"omitempty" bson:"namadevice"`
		TokenDevice string `json:"tokendevice" binding:"omitempty" bson:"tokendevice"`
	}

	RequestCheckToken struct {
		Token string `header:"x-token" binding:"required" bson:"token"`
	}

	RequestSearchKaryawan struct {
		Params string `json:"params" binding:"required" bson:"params"`
	}

	RequestSearchKaryawanV2 struct {
		Params string `json:"params" validate:"required" bson:"params"`
	}

	RequestCariDokter struct {
		ID string `json:"id_dokter" validate:"required" bson:"params"`
	}

	RequestActivatedCode struct {
		ActivatedCode string `json:"activated_code"  binding:"required" bson:"activated_code"`
	}

	RequestActivatedCodeV2 struct {
		ActivatedCode string `json:"activated_code"  validate:"required" bson:"activated_code"`
	}

	RequestDecrypPasswordUser struct {
		Password string `json:"password" validate:"required"`
	}

	ResponseDataPemberiInformasi struct {
		Nama         string `json:"nama"`
		JenisKelamin string `json:"jenis_kelamin"`
	}
)
