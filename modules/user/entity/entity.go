package entity

import (
	"hms_api/modules/lib"
	"hms_api/modules/user"
	"hms_api/modules/user/dto"

	"github.com/gofiber/fiber/v2"
)

// USER USECASE
type UserUseCase interface {
	RegisterUser(req dto.RegisterUser) (kodeOPT user.OTPUser, err error)
	RegisterUserV2(ctx *fiber.Ctx, req dto.RegisterUserV2) (kodeOPT user.OTPUser, err error)

	CreateAccountUser(req dto.CreateUserAccount) (valueUser user.Users, err error)
	SignInUserWithDevices(req dto.RequesLoginWithDevices) (res user.Users, err error)
	SignInEmailWithPassword(req dto.RequestLoginEmailAndPassword) (res user.Users, err error)
	SignInEmailWithPasswordV2(ctx *fiber.Ctx, req dto.RequestLoginEmailAndPasswordV2) (res user.Users, err error)
	ActivatedCodeUser(req dto.RequestActivatedCode) (res user.Users, err error)
	ActivatedCodeUserV2(ctx *fiber.Ctx, req dto.RequestActivatedCodeV2) (res user.Users, err error)
	CreateUserAndSendOTP(req dto.CreateAccountUser) (valueUser user.DataUsers, err error)
	CreateUserAndSendOTPV2(ctx *fiber.Ctx, req dto.CreateAccountUserV2) (valueUser user.DataUsers, err error)

	// USER PONEK
	OnChangedIgdToPonekUseCase(ctx *fiber.Ctx) (err error)
}

// USER REPOSITORY
type UserRepository interface {
	// GET ALL DATA PENGAJAR
	OnGetDProfilePasienRepository(noRm string) (res user.ProfilePasien, err error)
	OnGetProfilePasienRepository(noRM string) (res user.Pasien, err error)
	OnGetAllMutiaraPengajarRepository() (res []user.MutiaraPengajar, err error)
	// ==
	GetDataPengajarRepository(ID string) (res user.MutiaraPengajar, err error)
	GetAllKTaripDokter() (res []user.HisKtaripDokter, err error)
	InsertMutiaraPengajarRepository(pengajar user.MutiaraPengajar) (res user.MutiaraPengajar, err error)
	GetKaryawanRepository(id string) (res user.Karyawan, err error)
	GetKemployeByKTPAndID(ktp string, idKaryawan string) (user user.Kemploye, err error)
	GetKemployeByKTPAndIDV2(ctx *fiber.Ctx, ktp string, idKaryawan string) (user user.Kemploye, err error)
	GetKemployeByKTP(ktp string) (user user.Kemploye, err error)
	SaveOPT(email string, kodeOPT string, exp int) (opt user.OTPUser, err error)
	SaveOPTV2(ctx *fiber.Ctx, email string, kodeOPT string, exp int) (opt user.OTPUser, err error)

	SearchUsers(email string) (users user.Users, err error)
	SearchUsersV2(ctx *fiber.Ctx, email string) (users user.Users, err error)
	GetUsersByIDAndKodeModul(userID string, kodeModul string) (users user.Users, err error)
	GetUsersByIDAndKodeModulV2(ctx *fiber.Ctx, userID string, kodeModul string) (users user.Users, err error)

	OTPVerify(req dto.VerifyOTP) (res user.OTPUser, err error)
	OTPVerifyV2(ctx *fiber.Ctx, req dto.VerifyOTPV2) (res user.OTPUser, err error)
	SaveUsers(user user.Users) (res user.Users, err error)
	// KARYAWAN
	GetKemploye(params string) (user user.Kemploye, err error)
	GetKemployeV2(ctx *fiber.Ctx, params string) (user user.Kemploye, err error)
	GetKemployeByKTPV2(ctx *fiber.Ctx, ktp string) (user user.Kemploye, err error)
	CariDokterRepository(idDokter string) (user user.DokterModel, err error)

	//
	SearchUserByActivatedCode(activatedCode string) (users user.Users, err error)
	SearchUserByActivatedCodeV2(ctx *fiber.Ctx, activatedCode string) (users user.Users, err error)
	GetKPelayananByKode(kode string) (res lib.KPelayanan, err error)
	GetKPelayananByKodeV2(ctx *fiber.Ctx, kode string) (res lib.KPelayanan, err error)
	UpdateUserActive(activatedCode string)
	SaveDataUsers(user user.DataUsers) (res user.DataUsers, err error)
	SaveDataUsersV2(ctx *fiber.Ctx, user user.DataUsers) (res user.DataUsers, err error)
	SearchUsersByEmailAndModul(email string, modul string) (users user.Users, err error)
	SearchUsersByEmailAndModulV2(ctx *fiber.Ctx, email string, modul string) (users user.Users, err error)

	// ========== OLD DATABASE
	CariUserRepository(modul string, id string) (users user.UserKlinik, err error)
	CariUserPerawatRepository(id string) (users user.UserPerawat, err error)
	CariKtaripDokterRepository(idDokter string) (user user.DokterModel, err error)

	// FIND USER
	FindUserKlinikIGDRepository() (res []user.UserKlinik, err error)
	SaveUserKlinikPonekReporitory(user user.UserKlinik) (res user.UserKlinik, err error)
}

// USER MAPPER
type IUserMapper interface {
	ToMappingProfilePasien(data user.Pasien) (res user.ProfilePasien)

	ToMappingResponseDataPengajar(data []user.MutiaraPengajar) (res []dto.ResponseDataPemberiInformasi)
	ToSendOTP(data user.OTPUser, email string, req dto.RegisterUser) (value dto.UserOTPStatus)
	ToSendOTPV2(data user.OTPUser, email string, req dto.RegisterUserV2) (value dto.UserOTPStatus)
	ToUserResponse(data user.Users, token string, refreshToken string, pelayanan lib.KPelayanan) (value dto.UsersResponse)
}
