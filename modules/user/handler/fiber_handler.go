package handler

import (
	"hms_api/modules/user"
	"hms_api/modules/user/dto"
	"hms_api/pkg/helper"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func (uh *UserHandler) MoveDokterToPengajar(c *fiber.Ctx) error {

	kdokter, _ := uh.UserRepository.GetAllKTaripDokter()

	for _, V := range kdokter {
		pengajar, er12 := uh.UserRepository.GetDataPengajarRepository(V.Iddokter)

		if pengajar.Id == "" || er12 != nil {
			var pengajar = user.MutiaraPengajar{
				Id:           V.Iddokter,
				Keterangan:   V.Spesialisasi,
				Nama:         V.Namadokter,
				Jeniskelamin: V.Jeniskelamin,
				Status:       V.Statusdokter,
				Pendidikan:   V.Pendidikan,
				Tgllahir:     "0000-00-00",
				Anak:         "-",
				Telp:         "-",
				Hp:           "-",
				Fax:          "-",
				Email:        "-",
			}

			// insert data
			uh.UserRepository.InsertMutiaraPengajarRepository(pengajar)
		}
	}

	response := helper.APIResponse("Password", http.StatusOK, "OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) OnGetDataPengawaiFiberHandler(c *fiber.Ctx) error {
	pegawai, er1 := uh.UserRepository.OnGetAllMutiaraPengajarRepository()

	if er1 != nil {
		uh.Logging.Error(er1)
		response := helper.APIResponseFailure(er1.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if len(pegawai) == 0 {
		response := helper.APIResponse("Data Kosong", http.StatusOK, make([]dto.ResponseDataPemberiInformasi, 0))
		return c.Status(fiber.StatusOK).JSON(response)
	}

	mapper := uh.UserMapperImpl.ToMappingResponseDataPengajar(pegawai)
	response := helper.APIResponse("OK", http.StatusOK, mapper)
	return c.Status(fiber.StatusOK).JSON(response)
}
