package handler

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"hms_api/app/rest"
	"hms_api/modules/user"
	"hms_api/modules/user/dto"
	"hms_api/modules/user/entity"
	"hms_api/pkg/constant"
	"hms_api/pkg/helper"
	"io"
	"net/http"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

type UserHandler struct {
	UserUseCase    entity.UserUseCase
	UserMapperImpl entity.IUserMapper
	UserRepository entity.UserRepository
	Logging        *logrus.Logger
	Store          *session.Store
}

func (uh *UserHandler) SignInWithUsernameAndPassword(c *gin.Context) {
	payload := new(dto.RequestLoginEmailAndPassword)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error("errorMessage")
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Login gagal", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	mail := isEmailValid(payload.Email)

	if !mail {
		uh.Logging.Error("Validasi Email Gagal")
		response := helper.APIResponse("Email tidak valid", http.StatusAccepted, "email tidak valid")
		c.JSON(http.StatusAccepted, response)
		return
	}

	if payload.Password == "" {
		uh.Logging.Error("Email tidak boleh kosong")
		response := helper.APIResponse("Email tidak valid", http.StatusAccepted, "email tidak valid")
		c.JSON(http.StatusAccepted, response)
		return
	}

	//  FIND EMAIL USER
	email, _ := uh.UserRepository.SearchUsers(payload.Email)

	// ERROR EMAIL TIDAK DITEMUKAN
	if email.Email == "" {
		uh.Logging.Info(constant.EmailNotFound)
		response := helper.APIResponseFailure("Email tidak ditemukan", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	login, err := uh.UserUseCase.SignInEmailWithPassword(*payload)

	if err != nil || login.Email == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if login.UserStatus == "deactivated" {
		message := fmt.Sprintf("User tidak aktif%s, \nharap memasukkan kode verifikasi yang telah \ndikirimkan ke email Anda", "")
		uh.Logging.Error(message)
		response := helper.APIResponseFailure(message, http.StatusNonAuthoritativeInfo)
		c.JSON(http.StatusNonAuthoritativeInfo, response)
		return
	}

	pelayanan, _ := uh.UserRepository.GetKPelayananByKode(login.KodeModul)
	token, _ := rest.GenerateTokenUser(login)
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(login, token["token"], token["refresh_token"], pelayanan)

	response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("Login berhasil")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) SignInWithUsernameAndPasswordV2(c *fiber.Ctx) error {
	payload := new(dto.RequestLoginEmailAndPasswordV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Gagal Login", http.StatusAccepted, errorMessage)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	//  CARI USER DENGAN EMAIL
	//  DAN KODE MODUL FIND EMAIL USER

	email, _ := uh.UserRepository.SearchUsersV2(c, payload.Email)

	if email.Email == "" {
		// ERROR EMAIL TIDAK DITEMUKAN
		uh.Logging.Info(constant.EmailNotFound)
		response := helper.APIResponseFailure("Email tidak ditemukan", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	login, err := uh.UserUseCase.SignInEmailWithPasswordV2(c, *payload)

	if err != nil || login.Email == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if login.UserStatus == "deactivated" {
		message := fmt.Sprintf("User tidak aktif %s, \nharap memasukkan kode verifikasi yang telah \ndikirimkan ke email Anda", "")
		uh.Logging.Error(message)
		response := helper.APIResponseFailure(message, http.StatusNonAuthoritativeInfo)
		return c.Status(http.StatusNonAuthoritativeInfo).JSON(response)
	}

	pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, login.KodeModul)

	token, _ := rest.GenerateTokenUser(login)
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(login, token["token"], token["refresh_token"], pelayanan)

	response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("LOGIN BERHASIL")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) SignInWithIDAndPassword(c *fiber.Ctx) error {
	payload := new(dto.RequestLoginIDAndPassword)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	uh.Logging.Info("Receive Request Body")
	userData, err := uh.UserRepository.CariUserRepository(payload.KodeModul, payload.UserID)

	uh.Logging.Info("Get User Data")

	if err != nil {
		message := fmt.Sprintf("Data user dengan id %s tidak ditemukan", payload.UserID)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if userData.Id == "" {
		message := fmt.Sprintf("Data user dengan id %s tidak ditemukan", payload.UserID)
		response := helper.APIResponseFailure(message, http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// CARI APAKAH USER PERAWAT
	perawat, _ := uh.UserRepository.CariUserPerawatRepository(userData.Id)

	// JIKA PERAWAT DI TEMUKAN
	if perawat.Idperawat != "" {
		var sha = sha1.New()
		sha.Write([]byte(payload.Password))
		var encrypted = sha.Sum(nil)
		var passStr = fmt.Sprintf("%x", encrypted)

		if userData.Password == passStr {
			// ==== //
			var newUser = user.Users{
				Nama: perawat.Namaperawat, ReqStatus: "", NotifStatus: "", Email: "", Nik: "", NoHp: "", Kota: "",
				Agama: "", Jeniskelamin: "", KodeModul: userData.MappingKpelayanan, KeteranganPerson: "perawat", UserId: perawat.Idperawat,
				Hp: "", Alamat: perawat.Alamat,
			}

			pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, newUser.KodeModul)

			token, _ := rest.GenerateTokenUser(newUser)
			mapperSendOTP := uh.UserMapperImpl.ToUserResponse(newUser, token["token"], token["refresh_token"], pelayanan)

			uh.Logging.Info("perawat")

			response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
			uh.Logging.Info("LOGIN BERHASIL")
			return c.Status(fiber.StatusOK).JSON(response)

		} else {
			message := fmt.Sprintf("Password Salah %s", "")
			response := helper.APIResponseFailure(message, http.StatusCreated)
			uh.Logging.Debug(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}
	}

	dokter, _ := uh.UserRepository.CariKtaripDokterRepository(userData.Id)

	if dokter.Iddokter != "" {
		var sha = sha1.New()
		sha.Write([]byte(payload.Password))
		var encrypted = sha.Sum(nil)
		var passStr = fmt.Sprintf("%x", encrypted)

		if userData.Password == passStr {
			var newUser = user.Users{
				Photo: dokter.Photo, Nama: dokter.Namadokter, ReqStatus: "", NotifStatus: "", Email: "", Nik: "", NoHp: "", Kota: "", Agama: "", Jeniskelamin: dokter.Jeniskelamin, KodeModul: userData.MappingKpelayanan, KeteranganPerson: "dokter", UserId: dokter.Iddokter, Spesialisasi: dokter.Spesialisasi, Hp: "", Alamat: dokter.Alamat,
			}

			pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, newUser.KodeModul)

			token, _ := rest.GenerateTokenUser(newUser)
			mapperSendOTP := uh.UserMapperImpl.ToUserResponse(newUser, token["token"], token["refresh_token"], pelayanan)

			uh.Logging.Info("dokter")
			response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
			uh.Logging.Info("LOGIN BERHASIL")
			return c.Status(fiber.StatusOK).JSON(response)

		} else {
			message := fmt.Sprintf("Password Salah %s", "")
			response := helper.APIResponseFailure(message, http.StatusCreated)
			uh.Logging.Error(response)
			return c.Status(fiber.StatusCreated).JSON(response)
		}
	}

	message := fmt.Sprintf("Data user dengan id %s tidak diizinkan", payload.UserID)
	response := helper.APIResponseFailure(message, http.StatusCreated)
	uh.Logging.Error(response)
	return c.Status(fiber.StatusCreated).JSON(response)

}

func (uh *UserHandler) SignInWithDevices(c *gin.Context) {
	payload := new(dto.RequesLoginWithDevices)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	signIn, errs := uh.UserUseCase.SignInUserWithDevices(*payload)

	if errs != nil || signIn.Nik == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// mapper user
	pelayanan, _ := uh.UserRepository.GetKPelayananByKode(signIn.KodeModul)
	token, _ := rest.GenerateTokenUser(signIn)
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(signIn, token["token"], token["refresh_token"], pelayanan)
	response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("Login berhasil")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) SendOTP(c *gin.Context) {
	payload := new(dto.RegisterUser)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	kodeOtp, errs := uh.UserUseCase.RegisterUser(*payload)

	if errs != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	mapperSendOTP := uh.UserMapperImpl.ToSendOTP(kodeOtp, payload.Email, *payload)
	message := fmt.Sprintf("Kode OTP Berhasil dikirim ke %s", payload.Email)
	uh.Logging.Info(message)
	response := helper.APIResponse(message, http.StatusOK, mapperSendOTP)
	c.JSON(http.StatusOK, response)
}

// @Summary		Create Account User
// @Description	Mencari detail karyawan
// @Tags			User
// @Accept			json
// @Produce		json
// @Param			createAccount		body		dto.CreateAccountUser		true	"Create Account User Request"
// @Success		200		{object}	user.DataUsers
// @Router			/register-user/ [get]
func (uh *UserHandler) CreateAccountUser(c *gin.Context) {
	payload := new(dto.CreateAccountUser)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	data, err := uh.UserUseCase.CreateUserAndSendOTP(*payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	message := fmt.Sprintf("Kode verifikasi berhasil dikirim ke email %s", data.Email)

	response := helper.APIResponse(message, http.StatusOK, data)
	uh.Logging.Info(response)
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) CreateAccountUserV2(c *fiber.Ctx) error {
	payload := new(dto.CreateAccountUserV2)

	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	data, err := uh.UserUseCase.CreateUserAndSendOTPV2(c, *payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	message := fmt.Sprintf("Kode verifikasi berhasil dikirim ke email %s", data.Email)
	response := helper.APIResponse(message, http.StatusOK, data)
	uh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) ChangedIGDToPonekFiberHandler(c *fiber.Ctx) error {
	err12 := uh.UserUseCase.OnChangedIgdToPonekUseCase(c)

	if err12 != nil {
		response := helper.APIResponseFailure(err12.Error(), http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponseFailure("OK", http.StatusOK)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) CreateAccount(c *gin.Context) {
	payload := new(dto.CreateUserAccount)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	value, errs := uh.UserUseCase.CreateAccountUser(*payload)
	if errs != nil || value.Nik == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponseFailure("User berhasil diregistrasi", http.StatusOK)
	uh.Logging.Info("User berhasil diregistrasi")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) RegisterUser(c *gin.Context) {
	payload := new(dto.RegisterUser)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	kodeOtp, errs := uh.UserUseCase.RegisterUser(*payload)

	if errs != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	mapperSendOTP := uh.UserMapperImpl.ToSendOTP(kodeOtp, payload.Email, *payload)
	message := fmt.Sprintf("Kode OTP Berhasil dikirim ke %s", payload.Email)
	response := helper.APIResponse(message, http.StatusOK, mapperSendOTP)
	uh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) RegisterUserV2(c *fiber.Ctx) error {
	payload := new(dto.RegisterUserV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	kodeOtp, errs := uh.UserUseCase.RegisterUserV2(c, *payload)

	if errs != nil {
		uh.Logging.Error(errs)
		response := helper.APIResponseFailure(errs.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	mapperSendOTP := uh.UserMapperImpl.ToSendOTPV2(kodeOtp, payload.Email, *payload)
	message := fmt.Sprintf("Kode OTP Berhasil dikirim ke %s", payload.Email)
	response := helper.APIResponse(message, http.StatusOK, mapperSendOTP)
	uh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Verifikasi Kode OTP
// @Description	User yang didaftarkan dalam keadaan tidak aktif /nlalu dapat diverifikasi dengan endpoint berikut ini
// @Tags			User
// @Accept			json
// @Produce		json
// @Param			kode_otp		path		string		true	"kode_otp"
// @Param			email		path		string		true	"email"
// @Success		200		{object}	user.OTPUser
// @Router			/verify-otp/ [get]
func (uh *UserHandler) VerifyOTP(c *gin.Context) {
	payload := new(dto.VerifyOTP)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// VERIFIKASI OTP
	otpUser, errs := uh.UserRepository.OTPVerify(*payload)
	if errs != nil || len(otpUser.Email) == 0 {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Verifikasi OTP Gagal", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	now := time.Now().Unix()

	if int64(otpUser.Exp) < int64(now) {
		uh.Logging.Info("Kode OTP \ntelah expired")
		response := helper.APIResponseFailure("Kode OTP \ntelah expired", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	response := helper.APIResponse("Verifikasi kode OTP\nberhasil", http.StatusOK, otpUser)
	uh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) VerifyOTPV2(c *fiber.Ctx) error {
	payload := new(dto.VerifyOTPV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Error", http.StatusCreated, errors)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// VERIFIKASI OTP
	otpUser, errs := uh.UserRepository.OTPVerifyV2(c, *payload)

	if errs != nil || len(otpUser.Email) == 0 {
		uh.Logging.Error(errs)
		response := helper.APIResponseFailure("Verifikasi OTP Gagal", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	now := time.Now().Unix()

	if int64(otpUser.Exp) < int64(now) {
		uh.Logging.Info("Kode OTP \ntelah expired")
		response := helper.APIResponseFailure("Kode OTP \ntelah expired", http.StatusCreated)
		uh.Logging.Error(response)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	response := helper.APIResponse("Verifikasi kode OTP\nberhasil", http.StatusOK, otpUser)
	uh.Logging.Info("OK")
	return c.Status(fiber.StatusOK).JSON(response)
}

// @Summary		Session User
// @Description	User memiliki session dalam masa aktif token user
// @Tags			User
// @Produce		json
// @Accept			*/*
// @Param			x-token		path		string		true	"x-token"
// @securityDefinitions.apikey ApiKeyAuth
// @Security ApiKeyAuth
// @Security BasicAuth
// @in header
// @name X-Token
// @Success		200		{object}	dto.UsersResponse
// @Router			/session/ [get]
func (uh *UserHandler) Session(c *gin.Context) {
	payload := new(dto.RequestCheckToken)
	err := c.ShouldBindHeader(&payload)

	if err != nil {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure("Isian tidak lengkap", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	userID := c.MustGet("userID")
	modulID := c.MustGet("modulID").(string)

	valueUser, err := uh.UserRepository.GetUsersByIDAndKodeModul(userID.(string), modulID)
	if err != nil || valueUser.Nik == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// mapper user
	pelayanan, _ := uh.UserRepository.GetKPelayananByKode(valueUser.KodeModul)
	token, _ := rest.GenerateTokenUser(valueUser)
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(valueUser, token["token"], token["refresh_token"], pelayanan)
	response := helper.APIResponse("OK", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)

}

func (uh *UserHandler) SessionV2(c *fiber.Ctx) error {

	userID := c.Locals("userID")
	modulID := c.Locals("modulID")

	valueUser, err := uh.UserRepository.GetUsersByIDAndKodeModulV2(c, userID.(string), modulID.(string))

	if err != nil || valueUser.Nik == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// mapper user
	// Get Current User
	pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, valueUser.KodeModul)
	token, _ := rest.GenerateTokenUser(valueUser)
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(valueUser, token["token"], token["refresh_token"], pelayanan)

	response := helper.APIResponse("OK", http.StatusOK, mapperSendOTP)
	uh.Logging.Info(response)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) SessionV3(c *fiber.Ctx) error {
	userID := c.Locals("userID").(string)
	modulID := c.Locals("modulID").(string)
	person := c.Locals("person").(string)

	uh.Logging.Info("CEK SESSION")

	uh.Logging.Info(person)
	uh.Logging.Info(userID)

	userData, err := uh.UserRepository.CariUserRepository(modulID, userID)

	if err != nil || userData.Id == "" {
		uh.Logging.Error(err)
		response := helper.APIResponseFailure(err.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// mapper user
	perawat, _ := uh.UserRepository.CariUserPerawatRepository(userData.Id)

	// JIKA PERAWAT DI TEMUKAN
	uh.Logging.Info(perawat)

	uh.Logging.Info("KD BAGIAN")
	uh.Logging.Info(modulID)

	if perawat.Idperawat != "" {
		uh.Logging.Info("PERAWAT")
		uh.Logging.Info(perawat.Namaperawat)

		var newUser = user.Users{
			Nama: perawat.Namaperawat, ReqStatus: "", NotifStatus: "", Email: "", Nik: "", NoHp: "", Kota: "",
			Agama: "", Jeniskelamin: "", KodeModul: userData.MappingKpelayanan, KeteranganPerson: "perawat", UserId: perawat.Idperawat,
			Hp: "", Alamat: perawat.Alamat,
		}

		pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, newUser.KodeModul)

		token, _ := rest.GenerateTokenUser(newUser)
		mapperSendOTP := uh.UserMapperImpl.ToUserResponse(newUser, token["token"], token["refresh_token"], pelayanan)

		response := helper.APIResponse("OK", http.StatusOK, mapperSendOTP)
		uh.Logging.Info("OK")
		return c.Status(fiber.StatusOK).JSON(response)

	}

	dokter, _ := uh.UserRepository.CariKtaripDokterRepository(userData.Id)

	if dokter.Iddokter != "" {
		uh.Logging.Info("DOOKTER")
		uh.Logging.Info(dokter.Namadokter)

		var newUser = user.Users{
			Photo: dokter.Photo, Nama: dokter.Namadokter, ReqStatus: "", NotifStatus: "", Email: "", Nik: "", NoHp: "", Kota: "",
			Agama: "", Jeniskelamin: dokter.Jeniskelamin, KodeModul: userData.MappingKpelayanan, KeteranganPerson: "dokter", UserId: dokter.Iddokter, Hp: "", Alamat: dokter.Alamat, Spesialisasi: dokter.Spesialisasi,
		}

		pelayanan, _ := uh.UserRepository.GetKPelayananByKodeV2(c, newUser.KodeModul)

		token, _ := rest.GenerateTokenUser(newUser)
		mapperSendOTP := uh.UserMapperImpl.ToUserResponse(newUser, token["token"], token["refresh_token"], pelayanan)

		response := helper.APIResponse("Login berhasil", http.StatusOK, mapperSendOTP)
		uh.Logging.Info("LOGIN BERHASIL")
		return c.Status(fiber.StatusOK).JSON(response)
	}

	return nil

}

// @Summary		Cari Karyawan
// @Description	Mencari detail karyawan
// @Tags			Karyawan
// @Accept			json
// @Produce		json
// @Param			params		path		string		true	"Params"
// @Success		200		{object}	user.Kemploye
// @Router			/cari-karyawan/ [get]
func (uh *UserHandler) CariKaryawan(c *gin.Context) {
	payload := new(dto.RequestSearchKaryawan)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Data gagal diproses", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	keploye, errs := uh.UserRepository.GetKemploye(payload.Params)

	if errs != nil || keploye.Idk == "" {
		uh.Logging.Info("Karyawan tidak ditemukan")
		response := helper.APIResponseFailure("Karyawan tidak ditemukan", http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	if keploye.Filephoto == "" || keploye.Filephoto == "-" {
		uh.Logging.Info("Foto Kosong")
		keploye.Filephoto = "images/default_image.png"
	}

	response := helper.APIResponse("OK", http.StatusOK, keploye)
	uh.Logging.Info("OK")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) CariKaryawanV2(c *fiber.Ctx) error {
	payload := new(dto.RequestSearchKaryawanV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	keploye, errs := uh.UserRepository.GetKemployeV2(c, payload.Params)

	if errs != nil || keploye.Idk == "" {
		uh.Logging.Info("Karyawan tidak ditemukan")
		response := helper.APIResponseFailure("Karyawan tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if keploye.Filephoto == "" || keploye.Filephoto == "-" {
		uh.Logging.Info("Foto Kosong")
		keploye.Filephoto = "images/default_image.png"
	}

	response := helper.APIResponse("OK", http.StatusOK, keploye)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) CariDokterV3(c *fiber.Ctx) error {
	payload := new(dto.RequestCariDokter)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	data, err := uh.UserRepository.CariDokterRepository(payload.ID)

	if err != nil {
		uh.Logging.Info("Data Dokter tidak ditemukan")
		response := helper.APIResponseFailure("Data dokter tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	if data.Iddokter == "" {
		uh.Logging.Info("Data Dokter tidak ada")
		response := helper.APIResponseFailure("Data dokter tidak ditemukan", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("OK", http.StatusOK, data)
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) CariKaryawanV3(c *fiber.Ctx) error {
	// CARI DATA DOKTER DARI HIS KTARIPDOKTER

	// CARI DATA KARYWAWAN DARI MUTIARA.PENGAJAR

	return nil
}

// @Summary			Verifikasi User
// @Description		User yang didaftarkan dalam keadaan tidak aktif /nlalu dapat diverifikasi dengan endpoint berikut ini
// @Tags			User
// @Accept			json
// @Produce			json
// @Param			activated_code		path		string		true	"activated_code"
// @Success			200		{object}	dto.UsersResponse
// @Router			/verifikasi-user/ [get]
func (uh *UserHandler) ActivatedCode(c *gin.Context) {
	payload := new(dto.RequestActivatedCode)
	err := c.ShouldBindJSON(&payload)

	if err != nil {
		uh.Logging.Error(err)
		errors := helper.FormatValidationError(err)
		errorMessage := gin.H{"errors": errors}
		response := helper.APIResponse("Active code", http.StatusAccepted, errorMessage)
		c.JSON(http.StatusAccepted, response)
		return
	}

	// CARI USER DENGAN ACTIVATED CODE
	dataUser, er := uh.UserUseCase.ActivatedCodeUser(*payload)

	if er != nil {
		uh.Logging.Error(er)
		response := helper.APIResponseFailure(er.Error(), http.StatusCreated)
		c.JSON(http.StatusCreated, response)
		return
	}

	// cari pelayanan
	uh.UserRepository.UpdateUserActive(payload.ActivatedCode)
	pelayanan, _ := uh.UserRepository.GetKPelayananByKode(dataUser.KodeModul)

	token, _ := rest.GenerateTokenUser(dataUser)
	dataUser.UserStatus = "activated"
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(dataUser, token["token"], token["refresh_token"], pelayanan)
	response := helper.APIResponse("User berhasil diaktivasi", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("User berhasil diaktivasi")
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) ActivatedCodeV2(c *fiber.Ctx) error {
	payload := new(dto.RequestActivatedCodeV2)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// CARI USER DENGAN ACTIVATED CODE
	dataUser, er := uh.UserUseCase.ActivatedCodeUserV2(c, *payload)

	if er != nil {
		uh.Logging.Error(er)
		response := helper.APIResponseFailure(er.Error(), http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)

	}

	// cari pelayanan
	uh.UserRepository.UpdateUserActive(payload.ActivatedCode)
	pelayanan, _ := uh.UserRepository.GetKPelayananByKode(dataUser.KodeModul)

	token, _ := rest.GenerateTokenUser(dataUser)
	dataUser.UserStatus = "activated"
	mapperSendOTP := uh.UserMapperImpl.ToUserResponse(dataUser, token["token"], token["refresh_token"], pelayanan)
	response := helper.APIResponse("User berhasil diaktivasi", http.StatusOK, mapperSendOTP)
	uh.Logging.Info("User berhasil diaktivasi")
	return c.Status(fiber.StatusOK).JSON(response)
}

func encrypt(text string, key []byte) (string, error) {
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext[aes.BlockSize:], plaintext)

	return base64.StdEncoding.EncodeToString(ciphertext), nil
}

func decrypt(ciphertext string, key []byte) (string, error) {
	ciphertextBytes, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	if len(ciphertextBytes) < aes.BlockSize {
		return "", fmt.Errorf("ciphertext is too short")
	}

	iv := ciphertextBytes[:aes.BlockSize]
	ciphertextBytes = ciphertextBytes[aes.BlockSize:]

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertextBytes, ciphertextBytes)

	return string(ciphertextBytes), nil
}

func sha1Hash(input string) string {
	hasher := sha1.New()
	hasher.Write([]byte(input))
	hashBytes := hasher.Sum(nil)
	hashString := hex.EncodeToString(hashBytes)
	return hashString
}

func hashPassword(password string) (string, error) {
	hasher := sha1.New()
	hasher.Write([]byte(password))
	hashBytes := hasher.Sum(nil)
	hashString := hex.EncodeToString(hashBytes)
	return hashString, nil
}

func (uh *UserHandler) DecryptPasswordUserHandler(c *fiber.Ctx) error {
	payload := new(dto.RequestDecrypPasswordUser)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if err := validate.Struct(payload); err != nil {
		errors := helper.FormatValidationError(err)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errors)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	// key := []byte("16-byte-key-1234") // Replace with a secure key

	decryptedText, errs := hashPassword(payload.Password)

	if errs != nil {
		fmt.Println("Error decrypting:", errs)
		response := helper.APIResponse("Data tidak dapat diproses", http.StatusCreated, errs)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	response := helper.APIResponse("Password", http.StatusOK, decryptedText)
	uh.Logging.Info("Password")
	return c.Status(fiber.StatusOK).JSON(response)
}

func (uh *UserHandler) RootHandler(c *gin.Context) {
	message := gin.H{"hello": "Hello Word"}
	response := helper.APIResponse("User berhasil diaktivasi", http.StatusOK, message)
	c.JSON(http.StatusOK, response)
}

func (uh *UserHandler) RootHandlerV2(c *fiber.Ctx) error {
	message := gin.H{"hello": "Hello Word"}
	response := helper.APIResponse("User berhasil diaktivasi", http.StatusOK, message)
	uh.Logging.Info(message)
	return c.Status(fiber.StatusCreated).JSON(response)
}

func isEmailValid(e string) bool {
	emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return emailRegex.MatchString(e)
}
