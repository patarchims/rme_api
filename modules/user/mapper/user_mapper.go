package mapper

import (
	"hms_api/modules/lib"
	"hms_api/modules/user"
	"hms_api/modules/user/dto"
	"hms_api/modules/user/entity"
)

type UserMapperImpl struct{}

func NewUserMapperImple() entity.IUserMapper { return &UserMapperImpl{} }

func (a *UserMapperImpl) ToMappingProfilePasien(data user.Pasien) (res user.ProfilePasien) {
	return user.ProfilePasien{
		Id:           data.Id,
		Nik:          data.Nik,
		Firstname:    data.Firstname,
		Agama:        data.Agama,
		Jeniskelamin: data.Jeniskelamin,
		Email:        data.Email,
		Hp:           data.Hp,
		Alamat:       data.Alamat,
		TglLahir:     data.Tgllahir,
	}
}

func (a *UserMapperImpl) ToMappingResponseDataPengajar(data []user.MutiaraPengajar) (res []dto.ResponseDataPemberiInformasi) {
	var dat []dto.ResponseDataPemberiInformasi
	for _, V := range data {
		dat = append(dat, dto.ResponseDataPemberiInformasi{
			Nama:         V.Nama,
			JenisKelamin: V.Jeniskelamin,
		})
	}

	return dat
}

func (a *UserMapperImpl) ToSendOTP(data user.OTPUser, email string, req dto.RegisterUser) (value dto.UserOTPStatus) {
	return dto.UserOTPStatus{
		Nik:     req.NIK,
		IDK:     req.IDK,
		KodeOtp: data.KodeOtp,
		Exp:     data.Exp,
		Email:   email,
	}

}
func (a *UserMapperImpl) ToSendOTPV2(data user.OTPUser, email string, req dto.RegisterUserV2) (value dto.UserOTPStatus) {
	return dto.UserOTPStatus{
		Nik:     req.NIK,
		IDK:     req.IDK,
		KodeOtp: data.KodeOtp,
		Exp:     data.Exp,
		Email:   email,
	}

}

func (a *UserMapperImpl) ToUserResponse(data user.Users, token string, refreshToken string, pelayanan lib.KPelayanan) (value dto.UsersResponse) {
	// if data.Photo == "" || data.Photo == "-" {
	// 	data.Photo = "images/default_image.png"
	// }

	return dto.UsersResponse{
		Nama:         data.Nama,
		Person:       data.KeteranganPerson,
		KodePoli:     pelayanan.KdBag,
		Email:        data.Email,
		Nik:          data.Nik,
		NoHp:         data.NoHp,
		Spesialisasi: data.Spesialisasi,
		UserStatus:   data.UserStatus,
		UserId:       data.UserId,
		Token:        token,
		Bagian:       pelayanan.Bagian,
		Pelayanan:    pelayanan.Pelayanan,
		RefreshToken: refreshToken,
		Alamat:       data.Alamat,
		JenisKelamin: data.Jeniskelamin,
		Photo:        data.Photo,
		TglLahir:     data.Tgllahir.Format("2006-01-02"),
		Usia:         data.Usia,
		StatusKawin:  data.Status,
		TglMasuk:     (data.Tglmasuk.String())[0:10],
		Agama:        data.Agama,
		Kota:         data.Kota,
	}
}
