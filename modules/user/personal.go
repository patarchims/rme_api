package user

type (

	// DATA MUTIARA.PENGAJAR
	MutiaraPengajar struct {
		Keterangan   string
		Id           string
		Nama         string
		Jeniskelamin string
		Pendidikan   string
		Tgllahir     string
		Status       string
		Anak         string
		Telp         string
		Hp           string
		Fax          string
		Email        string
	}

	// DATA KATARIP DOKTER
	HisKtaripDokter struct {
		Spesialisasi string
		Iddokter     string
		Namadokter   string
		Alamat       string
		Jeniskelamin string
		Pendidikan   string
		Statusdokter string
		Drahli       string
	}
)

func (MutiaraPengajar) TableName() string {
	return "mutiara.pengajar"
}

func (HisKtaripDokter) TableName() string {
	return "his.ktaripdokter"
}
