package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/user"
)

func (ur *userRepository) InsertMutiaraPengajarRepository(pengajar user.MutiaraPengajar) (res user.MutiaraPengajar, err error) {
	result := ur.DB.Create(&pengajar)
	if result.Error != nil {
		return pengajar, err
	}
	return pengajar, nil
}

func (ur *userRepository) OnGetAllMutiaraPengajarRepository() (res []user.MutiaraPengajar, err error) {

	result := ur.DB.Model(user.MutiaraPengajar{}).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}
