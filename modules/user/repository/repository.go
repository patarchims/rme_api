package repository

import (
	"errors"
	"fmt"
	"hms_api/modules/lib"
	"hms_api/modules/user"
	"hms_api/modules/user/dto"
	"hms_api/modules/user/entity"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type userRepository struct {
	DB *gorm.DB
}

func NewUserRepository(db *gorm.DB) entity.UserRepository {
	return &userRepository{
		DB: db,
	}
}

func (ur *userRepository) GetKemployeByKTPAndID(ktp string, idKaryawan string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE ktp=? AND idk=?  LIMIT 1"
	result := ur.DB.Raw(query, ktp, idKaryawan).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) GetKemployeByKTPAndIDV2(ctx *fiber.Ctx, ktp string, idKaryawan string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE ktp=? AND idk=?  LIMIT 1"
	result := ur.DB.Raw(query, ktp, idKaryawan).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) SearchUsers(email string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.email=? LIMIT 1"
	result := ur.DB.Raw(query, email).Scan(&users)
	logrus.Println(query)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) SearchUsersV2(ctx *fiber.Ctx, email string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.email=? LIMIT 1"
	result := ur.DB.Raw(query, email).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) CariUserRepository(modul string, id string) (users user.UserKlinik, err error) {
	query := "SELECT app, id, user, mapping_kpelayanan, password FROM klinik.users  WHERE mapping_kpelayanan=? AND id =? LIMIT 1"
	result := ur.DB.Raw(query, modul, id).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) CariUserPerawatRepository(id string) (users user.UserPerawat, err error) {
	query := "SELECT idperawat, namaperawat, alamat, jeniskelamin, statusperawat FROM his.kperawat WHERE idperawat=? LIMIT 1"
	result := ur.DB.Raw(query, id).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) CariKtaripDokterRepository(idDokter string) (users user.DokterModel, err error) {
	query := "SELECT iddokter, namadokter, alamat, jeniskelamin, pendidikan, photo, spesialisasi FROM his.ktaripdokter  WHERE iddokter=? LIMIT 1"

	result := ur.DB.Raw(query, idDokter).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) SearchUsersByEmailAndModul(email string, modul string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.email=? AND a.kode_modul=? LIMIT 1"
	result := ur.DB.Raw(query, email, modul).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) SearchUsersByEmailAndModulV2(ctx *fiber.Ctx, email string, modul string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.email=? AND a.kode_modul=? LIMIT 1"

	result := ur.DB.Raw(query, email, modul).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) SaveUsers(user user.Users) (res user.Users, err error) {
	result := ur.DB.Create(&user)
	if result.Error != nil {
		return user, err
	}
	return user, nil
}
func (ur *userRepository) SaveDataUsers(user user.DataUsers) (res user.DataUsers, err error) {
	result := ur.DB.Create(&user)
	if result.Error != nil {
		return user, err
	}
	return user, nil
}
func (ur *userRepository) SaveDataUsersV2(ctx *fiber.Ctx, user user.DataUsers) (res user.DataUsers, err error) {
	result := ur.DB.Create(&user)
	if result.Error != nil {
		return user, err
	}
	return user, nil
}

func (ur *userRepository) SaveOPT(email string, kodeOPT string, exp int) (opt user.OTPUser, err error) {
	data := user.OTPUser{Email: email, KodeOtp: kodeOPT, Exp: exp}

	result := ur.DB.Create(&data)

	if result.Error != nil {
		return data, err
	}

	return data, nil
}

func (ur *userRepository) SaveOPTV2(ctx *fiber.Ctx, email string, kodeOPT string, exp int) (opt user.OTPUser, err error) {
	data := user.OTPUser{Email: email, KodeOtp: kodeOPT, Exp: exp}

	result := ur.DB.Create(&data)

	if result.Error != nil {
		return data, err
	}

	return data, nil
}

func (ur *userRepository) OTPVerify(req dto.VerifyOTP) (res user.OTPUser, err error) {
	query := "SELECT * FROM vicore_usr.users_otp  WHERE email = ? AND kode_otp=?"
	result := ur.DB.Raw(query, req.Email, req.KodeOtp).Scan(&res)

	if result.Error != nil {
		return res, err
	}

	return res, nil
}

func (ur *userRepository) OTPVerifyV2(ctx *fiber.Ctx, req dto.VerifyOTPV2) (res user.OTPUser, err error) {
	query := "SELECT * FROM vicore_usr.users_otp  WHERE email = ? AND kode_otp=?"
	result := ur.DB.Raw(query, req.Email, req.KodeOtp).Scan(&res)

	if result.Error != nil {
		return res, err
	}

	return res, nil
}

func (ur *userRepository) GetKemployeByKTP(ktp string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE ktp=?  LIMIT 1"
	result := ur.DB.Raw(query, ktp).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) GetKemployeByKTPV2(ctx *fiber.Ctx, ktp string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE ktp=?  LIMIT 1"
	result := ur.DB.Raw(query, ktp).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) GetKemploye(params string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE  email=? OR hp=? OR ktp=? OR idk=?   LIMIT 1"
	result := ur.DB.Raw(query, params, params, params, params).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) CariDokterRepository(idDokter string) (user user.DokterModel, err error) {
	query := "SELECT iddokter,namadokter,alamat,jeniskelamin,pendidikan, statusdokter FROM his.ktaripdokter WHERE  iddokter=?  LIMIT 1"
	result := ur.DB.Raw(query, idDokter).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) GetKemployeV2(ctx *fiber.Ctx, params string) (user user.Kemploye, err error) {
	query := "SELECT * FROM vicore_hrd.kemployee WHERE  email=? OR hp=? OR ktp=? OR idk=?   LIMIT 1"
	result := ur.DB.Raw(query, params, params, params, params).Scan(&user)

	if result.Error != nil {
		return user, err
	}

	return user, nil
}

func (ur *userRepository) GetUsersByIDAndKodeModul(userID string, kodeModul string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.user_id=? AND a.kode_modul=? LIMIT 1"
	result := ur.DB.Raw(query, userID, kodeModul).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) GetUsersByIDAndKodeModulV2(ctx *fiber.Ctx, userID string, kodeModul string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.user_id=? AND a.kode_modul=? LIMIT 1"
	result := ur.DB.Raw(query, userID, kodeModul).Scan(&users)

	if result.Error != nil {
		return users, err
	}

	return users, nil
}

func (ur *userRepository) SearchUserByActivatedCode(activatedCode string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.activated_code=? LIMIT 1"
	result := ur.DB.Raw(query, activatedCode).Scan(&users)
	if result.Error != nil {
		return users, errors.New("kode activasi tidak ditemukan")
	}

	return users, nil

}

func (ur *userRepository) SearchUserByActivatedCodeV2(ctx *fiber.Ctx, activatedCode string) (users user.Users, err error) {
	query := "SELECT * FROM vicore_usr.users AS a INNER JOIN vicore_hrd.kemployee AS b ON a.user_id = b.idk WHERE a.activated_code=? LIMIT 1"
	result := ur.DB.Raw(query, activatedCode).Scan(&users)
	if result.Error != nil {
		return users, errors.New("kode activasi tidak ditemukan")
	}

	return users, nil

}

func (ur *userRepository) GetKPelayananByKode(kode string) (res lib.KPelayanan, err error) {
	result := ur.DB.Where("kd_bag = ?", kode).First(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}
func (ur *userRepository) GetKaryawanRepository(id string) (res user.Karyawan, err error) {
	result := ur.DB.Where("id = ?", id).First(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (ur *userRepository) FindUserKlinikIGDRepository() (res []user.UserKlinik, err error) {
	result := ur.DB.Model(user.UserKlinik{}).Where("mapping_kpelayanan= ?", "IGD001").Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}
func (ur *userRepository) GetAllKTaripDokter() (res []user.HisKtaripDokter, err error) {
	result := ur.DB.Model(user.HisKtaripDokter{}).Scan(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (ur *userRepository) SaveUserKlinikPonekReporitory(user user.UserKlinik) (res user.UserKlinik, err error) {
	result := ur.DB.Create(&user)
	if result.Error != nil {
		return user, err
	}
	return user, nil
}

func (ur *userRepository) GetKPelayananByKodeV2(ctx *fiber.Ctx, kode string) (res lib.KPelayanan, err error) {
	result := ur.DB.Where("kd_bag = ?", kode).First(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (ur *userRepository) GetDataPengajarRepository(ID string) (res user.MutiaraPengajar, err error) {
	result := ur.DB.Where("id=?", ID).First(&res)
	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}
func (ur *userRepository) UpdateUserActive(activatedCode string) {
	ur.DB.Model(user.Users{}).Where("activated_code  = ?", activatedCode).Updates(user.Users{UserStatus: "activated"})
}

func (ur *userRepository) OnGetDProfilePasienRepository(noRm string) (res user.ProfilePasien, err error) {
	result := ur.DB.Where(user.ProfilePasien{Id: noRm}).First(&res)

	if result.Error != nil {
		message := fmt.Sprintf(" Data tidak ditemukan, Error %s", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}

func (ur *userRepository) OnGetProfilePasienRepository(noRM string) (res user.Pasien, err error) {
	result := ur.DB.Where(user.Pasien{Id: noRM}).Find(&res)

	if result.Error != nil {
		message := fmt.Sprintf("Error %s, Data tidak ditemukan", result.Error)
		return res, errors.New(message)
	}

	return res, nil
}
