package usecase

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"hms_api/modules/user"
	"hms_api/modules/user/dto"
	"hms_api/modules/user/entity"
	"hms_api/pkg/helper"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/xlzd/gotp"
)

type userUseCase struct {
	userRepository entity.UserRepository
	logging        *logrus.Logger
}

func NewUserUseCase(ur entity.UserRepository, loging *logrus.Logger) entity.UserUseCase {
	return &userUseCase{
		userRepository: ur,
		logging:        loging,
	}
}

func (us *userUseCase) CreateUserAndSendOTP(req dto.CreateAccountUser) (valueUser user.DataUsers, err error) {
	// cari data user terlebih dahulu
	data, err := us.userRepository.GetKemployeByKTP(req.NIK)

	if err != nil || data.Ktp == "" {
		str := fmt.Sprintf("Karyawan tidak dapat didaftarkan %s", "")
		return valueUser, errors.New(str)
	}

	if len(req.Password) < 5 {
		str := fmt.Sprintf("Password harus lebih dari 5 karakter %s", "")
		return valueUser, errors.New(str)
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	var otp = gotp.RandomSecret(5)

	// KIRIMKAN OTP KE EMAIL
	helper.SendMail(data.Email, data.Email, otp)

	dataUser := user.DataUsers{ReqStatus: "0", NotifStatus: "0", DelStatus: "false", Email: data.Email, Nik: data.Ktp, NoHp: data.Hp, KodeModul: req.KodeModul, UserStatus: "deactivated", UserId: data.Idk, Password: passStr, CreateUser: "Create From Tablet", CreatePc: "None", ActivatedCode: otp}

	res, err := us.userRepository.SaveDataUsers(dataUser)
	if err != nil {
		str := fmt.Sprintf("Data gagal diregistrasi %s", "")
		return res, errors.New(str)
	}

	return res, nil
}

func (us *userUseCase) CreateUserAndSendOTPV2(ctx *fiber.Ctx, req dto.CreateAccountUserV2) (valueUser user.DataUsers, err error) {
	// cari data user terlebih dahulu
	data, err := us.userRepository.GetKemployeByKTPV2(ctx, req.NIK)
	if err != nil || data.Ktp == "" {
		str := fmt.Sprintf("Karyawan tidak dapat didaftarkan %s", "")
		return valueUser, errors.New(str)
	}

	if len(req.Password) < 5 {
		str := fmt.Sprintf("Password harus lebih dari 5 karakter %s", "")
		return valueUser, errors.New(str)
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	var otp = gotp.RandomSecret(5)

	// KIRIMKAN OTP KE EMAIL
	helper.SendMail(data.Email, data.Email, otp)

	dataUser := user.DataUsers{ReqStatus: "0", NotifStatus: "0", DelStatus: "false", Email: data.Email, Nik: data.Ktp, NoHp: data.Hp, KodeModul: req.KodeModul, UserStatus: "deactivated", UserId: data.Idk, Password: passStr, CreateUser: "Create From Tablet", CreatePc: "None", ActivatedCode: otp}

	res, err := us.userRepository.SaveDataUsersV2(ctx, dataUser)

	if err != nil {
		str := fmt.Sprintf("Data gagal diregistrasi %s", "")
		return res, errors.New(str)
	}

	return res, nil
}

func (us *userUseCase) CreateAccountUser(req dto.CreateUserAccount) (valueUser user.Users, err error) {
	// GET DATA USER TERLEBIH DAHULU

	if req.RepeatPassword != req.Password {
		str := fmt.Sprintf("Password tidak sama %s", "")
		return valueUser, errors.New(str)
	}

	value, errs := us.userRepository.GetKemployeByKTP(req.NIK)

	if errs != nil {
		return valueUser, errors.New(err.Error())
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	// SIMPAN DATA USER
	dataUser := user.Users{ReqStatus: "0", NotifStatus: "0", DelStatus: "false", Email: req.Email, Nik: value.Ktp, NoHp: value.Hp, KodeModul: req.KodeModul, UserStatus: "activated", UserId: value.Idk, Password: passStr, CreateUser: "Create From Tablet"}

	res, err := us.userRepository.SaveUsers(dataUser)
	if err != nil {
		str := fmt.Sprintf("Data gagal diregistrasi %s", "")
		return res, errors.New(str)
	}

	return res, nil

}

func (us *userUseCase) RegisterUser(req dto.RegisterUser) (kodeOPT user.OTPUser, err error) {
	// CHECK USER APAKAH ADA DI KEMPLOYEE
	karyawan, err := us.userRepository.GetKemployeByKTPAndID(req.NIK, req.IDK)

	if err != nil {
		message := fmt.Sprintf("Karyawan dengan nik %s, \ntidak dapat ditemukan", req.NIK)
		return kodeOPT, errors.New(message)
	}

	if karyawan.Idk == "" {
		message := fmt.Sprintf("Karyawan dengan nik %s dan ID %s , \ntidak dapat ditemukan", req.NIK, req.IDK)
		return kodeOPT, errors.New(message)
	}

	// CARI USER DENGAN DIK TERSEBUT
	user, _ := us.userRepository.SearchUsers(req.Email)

	if user.Email != "" {
		message := fmt.Sprintf("Karyawan dengan email %s\ntersebut, sudah terdaftar", req.Email)
		return kodeOPT, errors.New(message)
	}

	exp := time.Now().Add(time.Minute + 5).Unix()
	otpKode := strconv.Itoa(int(exp))
	otpStr := otpKode[4:10]

	// SIMPAN KODE OTP
	kodeOPT, errs := us.userRepository.SaveOPT(req.Email, otpStr, int(exp))
	if errs != nil {
		message := fmt.Sprintf("Kode OPT gagal dikirim %s", "")
		return kodeOPT, errors.New(message)
	}

	helper.SendMail(req.Email, req.Email, otpStr)

	return kodeOPT, nil

}

func (us *userUseCase) RegisterUserV2(ctx *fiber.Ctx, req dto.RegisterUserV2) (kodeOPT user.OTPUser, err error) {
	// CHECK USER APAKAH ADA DI KEMPLOYEE
	karyawan, err := us.userRepository.GetKemployeByKTPAndIDV2(ctx, req.NIK, req.IDK)

	us.logging.Info(karyawan)

	if err != nil {
		message := fmt.Sprintf("Karyawan dengan nik %s, \ntidak dapat ditemukan", req.NIK)
		us.logging.Error(message)
		return kodeOPT, errors.New(message)
	}

	if karyawan.Idk == "" {
		message := fmt.Sprintf("Karyawan dengan nik %s dan ID %s , \ntidak dapat ditemukan", req.NIK, req.IDK)
		us.logging.Error(message)
		return kodeOPT, errors.New(message)
	}

	// CARI USER DENGAN DIK TERSEBUT
	user, _ := us.userRepository.SearchUsersV2(ctx, req.Email)

	if user.Email != "" {
		message := fmt.Sprintf("Karyawan dengan email %s\ntersebut, sudah terdaftar", req.Email)
		us.logging.Error(message)
		return kodeOPT, errors.New(message)
	}

	exp := time.Now().Add(time.Minute + 5).Unix()
	otpKode := strconv.Itoa(int(exp))
	otpStr := otpKode[4:10]

	// SIMPAN KODE OTP
	kodeOPT, errs := us.userRepository.SaveOPTV2(ctx, req.Email, otpStr, int(exp))
	if errs != nil {
		message := fmt.Sprintf("Kode OPT gagal dikirim %s", "")
		us.logging.Error(message)
		return kodeOPT, errors.New(message)
	}

	helper.SendMail(req.Email, req.Email, otpStr)

	return kodeOPT, nil

}

func (us *userUseCase) SignInEmailWithPassword(req dto.RequestLoginEmailAndPassword) (res user.Users, err error) {
	user, err := us.userRepository.SearchUsersByEmailAndModul(req.Email, req.KodeModul)

	if err != nil || user.Email == "" {
		message := fmt.Sprintf("User dengan email %s, \ntidak dapat ditemukan \ndengan kode modul %s", req.Email, req.KodeModul)
		return res, errors.New(message)
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	if user.Password == passStr {
		return user, nil
	} else {
		message := fmt.Sprintf("Password Salah %s", "")
		return res, errors.New(message)
	}

}

func (us *userUseCase) SignInEmailWithPasswordV2(ctx *fiber.Ctx, req dto.RequestLoginEmailAndPasswordV2) (res user.Users, err error) {
	user, err := us.userRepository.SearchUsersByEmailAndModulV2(ctx, req.Email, req.KodeModul)

	if err != nil || user.Email == "" {
		message := fmt.Sprintf("User dengan email %s, \ntidak dapat ditemukan \ndengan kode modul %s", req.Email, req.KodeModul)
		return res, errors.New(message)
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	if user.Password == passStr {
		return user, nil
	} else {
		message := fmt.Sprintf("Password Salah %s", "")
		return res, errors.New(message)
	}

}

func (us *userUseCase) SignInUserWithIDAndPasswordUsecase(req dto.RequestLoginIDAndPassword) (err error) {
	// CEK APAKAH DATA USER ADA
	userData, err := us.userRepository.CariUserRepository(req.KodeModul, req.UserID)

	if err != nil {
		message := fmt.Sprintf("Data user dengan id %s tidak ditemukan", req.UserID)
		return errors.New(message)
	}

	if userData.Id == "" {
		message := fmt.Sprintf("Data user dengan id %s tidak ditemukan", req.UserID)
		return errors.New(message)
	}

	// CARI APAKAH USER PERAWAT
	perawat, _ := us.userRepository.CariUserPerawatRepository(userData.Id)

	// JIKA PERAWAT DI TEMUKAN
	if perawat.Idperawat != "" {
		var sha = sha1.New()
		sha.Write([]byte(req.Password))
		var encrypted = sha.Sum(nil)
		var passStr = fmt.Sprintf("%x", encrypted)

		if userData.Password == passStr {
			return nil
		} else {
			message := fmt.Sprintf("Password Salah %s", "")
			return errors.New(message)
		}
	}

	dokter, _ := us.userRepository.CariKtaripDokterRepository(userData.Id)

	if dokter.Iddokter != "" {
		var sha = sha1.New()
		sha.Write([]byte(req.Password))
		var encrypted = sha.Sum(nil)
		var passStr = fmt.Sprintf("%x", encrypted)

		if userData.Password == passStr {
			return nil
		} else {
			message := fmt.Sprintf("Password Salah %s", "")
			return errors.New(message)
		}
	}

	return nil
}

func (us *userUseCase) SignInUserWithDevices(req dto.RequesLoginWithDevices) (res user.Users, err error) {
	user, err := us.userRepository.SearchUsers(req.Email)
	if err != nil || user.Email == "" {
		message := fmt.Sprintf("User dengan email %s, \ntidak dapat ditemukan", req.Email)
		return res, errors.New(message)
	}

	var sha = sha1.New()
	sha.Write([]byte(req.Password))
	var encrypted = sha.Sum(nil)
	var passStr = fmt.Sprintf("%x", encrypted)

	if user.Password == passStr {
		return user, nil
	} else {
		message := fmt.Sprintf("Password Salah %s", "")
		return res, errors.New(message)
	}

}

func (us *userUseCase) ActivatedCodeUser(req dto.RequestActivatedCode) (res user.Users, err error) {
	users, errs := us.userRepository.SearchUserByActivatedCode(req.ActivatedCode)

	if errs != nil || len(users.Nik) <= 2 {
		message := fmt.Sprintf("Kode tidak ditemuakan %s", "")
		return res, errors.New(message)
	}

	if users.UserStatus == "activated" {
		message := fmt.Sprintf("Kode sudah diaktivasi %s", "")
		return res, errors.New(message)
	}

	return users, nil
}

func (us *userUseCase) ActivatedCodeUserV2(ctx *fiber.Ctx, req dto.RequestActivatedCodeV2) (res user.Users, err error) {
	users, errs := us.userRepository.SearchUserByActivatedCodeV2(ctx, req.ActivatedCode)

	if errs != nil || len(users.Nik) <= 2 {
		message := fmt.Sprintf("Kode tidak ditemuakan %s", "")
		return res, errors.New(message)
	}

	if users.UserStatus == "activated" {
		message := fmt.Sprintf("Kode sudah diaktivasi %s", "")
		return res, errors.New(message)
	}

	return users, nil
}

func (us *userUseCase) OnChangedIgdToPonekUseCase(ctx *fiber.Ctx) (err error) {
	// GET DATA USER PONEK
	users, err1 := us.userRepository.FindUserKlinikIGDRepository()

	if err1 != nil {
		return err1
	}

	for _, V := range users {
		// INSERT DATA USER DENGAN PONEK
		var dataUser = user.UserKlinik{
			App:               "PONEK",
			Id:                V.Id,
			MappingKpelayanan: "PONEK",
			User:              V.User,
			Password:          V.Password,
		}
		us.userRepository.SaveUserKlinikPonekReporitory(dataUser)
	}

	return nil
}
