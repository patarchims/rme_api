package user

import (
	"time"
)

type (
	UserStatus struct {
		Status string `json:"status"`
	}

	AlamatUser struct {
		Id         int     `json:"id"`
		UserNik    string  `json:"nikUser"`
		Alamat     string  `json:"alamat"`
		Keterangan string  `json:"keterangan"`
		Latt       float64 `json:"latt"`
		Long       float64 `json:"long"`
	}

	ProfilePasien struct {
		Id           string    `json:"id"`
		Nik          string    `json:"nik"`
		Firstname    string    `db:"firstname" json:"nama"`
		Agama        string    `json:"agama"`
		Jeniskelamin string    `json:"jeniskelamin"`
		Email        string    `json:"email"`
		Hp           string    `json:"hp"`
		TglLahir     time.Time `json:"tgl_lahir"`
		Alamat       string    `db:"alamat" json:"alamat"`
	}

	Pasien struct {
		Id           string    `json:"id"`
		Nik          string    `db:"nik" json:"nik"`
		Firstname    string    `db:"firstname" json:"nama"`
		Hp           string    `json:"hp"`
		Agama        string    `json:"agama"`
		Jeniskelamin string    `json:"jenisKelamin"`
		Tempatlahir  string    `json:"tempatLahir"`
		Tgllahir     time.Time `json:"tglLahir"`
		Alamat       string    `json:"alamat"`
		Rtrw         string    `json:"rtRW"`
		Propinsi     string    `json:"provinsi"`
		Kelurahan    string    `json:"kelurahan"`
		Kecamatan    string    `json:"kecamatan"`
		Kabupaten    string    `json:"kabupaten"`
		Negara       string    `json:"negara"`
		Pekerjaan    string    `json:"pekerjaan"`
		Status       string    `json:"statusKawin"`
		Email        string    `json:"email"`
		LinkFoto     string    `json:"photo"`
		Token        string    `json:"token"`
		RefreshToken string    `json:"refresh_token"`
	}

	UserKlinik struct {
		App               string `json:"app"`
		Id                string `json:"id"`
		MappingKpelayanan string `json:"kpelayanan"`
		User              string `json:"user"`
		Password          string `json:"password"`
	}

	UserPerawat struct {
		Idperawat     string `json:"id"`
		Namaperawat   string `json:"nama"`
		Alamat        string `json:"alamat"`
		Jeniskelamin  string `json:"jenis_kelamin"`
		Statusperawat string `json:"status"`
	}

	// ========================
	UserDokter struct {
		Iddokter     string `json:"id_dokter"`
		Namadokter   string `json:"nama_dokter"`
		Alamat       string `json:"alamat"`
		Jeniskelamin string `json:"jenis_kelamin"`
		Pendidikan   string `json:"pendidikan"`
		Spesialisasi string `json:"spesialisasi"`
	}

	Users struct {
		Nama             string    `json:"nama"`
		ReqStatus        string    `json:"req_status"`
		NotifStatus      string    `json:"notif_status"`
		DelStatus        string    `json:"del_status"`
		CreatePc         string    `json:"create_pc"`
		CreateUser       string    `json:"create_user"`
		Email            string    `json:"email"`
		Nik              string    `json:"nik"`
		NoHp             string    `json:"no_hp"`
		KodeModul        string    `json:"kode_modul"`
		UserStatus       string    `json:"user_status"`
		UserId           string    `json:"user_id"`
		Password         string    `json:"password"`
		ActivatedCode    string    `json:"activated_code"`
		KeteranganPerson string    `json:"person"`
		KodeSpesialisai  string    `json:"kode_spesialisasi"`
		Spesialisasi     string    `json:"spesialisasi"`
		Photo            string    `json:"photo"`
		Tglmasuk         time.Time `json:"tglMasuk"`
		Jeniskelamin     string    `json:"jenisKelamin"`
		Tgllahir         time.Time `json:"tglLahir"`
		Tempatlahir      string    `json:"tempatLahir"`
		Hp               string    `json:"hp"`
		Status           string    `json:"statusKawin"`
		Usia             int       `json:"usia"`
		Alamat           string    `json:"alamat"`
		Agama            string    `json:"agama"`
		Kota             string    `json:"kota"`
	}

	DataUsers struct {
		ReqStatus     string `json:"req_status" bson:"req_status"`
		NotifStatus   string `json:"notif_status" bson:"notif_status"`
		DelStatus     string `json:"del_status" bson:"del_status"`
		CreatePc      string `json:"create_pc" bson:"create_pc"`
		CreateUser    string `json:"create_user" bson:"create_user"`
		Email         string `json:"email" bson:"email"`
		Nik           string `json:"nik" bson:"nik"`
		NoHp          string `json:"no_hp" bson:"no_hp"`
		KodeModul     string `json:"kode_modul" bson:"kode_modul"`
		UserStatus    string `json:"user_status" bson:"user_status"`
		UserId        string `json:"user_id" bson:"user_id"`
		Password      string `json:"password" bson:"password"`
		ActivatedCode string `json:"activated_code" bson:"activated_code"`
	}

	Kemploye struct {
		KeteranganPerson string `json:"person" bson:"person"`
		Ktp              string `json:"nik" bson:"ktp"`
		Idk              string `json:"idKaryawan" bson:"idk"`
		Hp               string `json:"hp" bson:"hp"`
		AlamatSekarang   string `json:"alamat" bson:"alamat"`
		Filephoto        string `json:"photo" bson:"photo"`
		Bagian           string `json:"bagian" bson:"bagian"`
		Nama             string `json:"nama" bson:"nama"`
		Jeniskelamin     string `json:"jenisKelamin" bson:"jenisKelamin"`
		Tgllahir         string `json:"tglLahir" bson:"tglLahir"`
		Tempatlahir      string `json:"tempatLahir" bson:"tempatLahir"`
		Kota             string `json:"kota" bson:"kota"`
		Usia             string `json:"usia" bson:"usia"`
		Agama            string `json:"agama" bson:"agama"`
		Status           string `json:"statusKawin" bson:"statusKawin"`
		Email            string `json:"email" bson:"email"`
		Tglmasuk         string `json:"tgl_masuk" bson:"tgl_masuk"`
	}

	// iddokter,namadokter,alamat,jeniskelamin,pendidikan, statusdokter
	DokterModel struct {
		Iddokter     string `json:"id_dokter"`
		Namadokter   string `json:"nama_dokter"`
		Alamat       string `json:"alamat"`
		Photo        string `json:"photo"`
		Jeniskelamin string `json:"jenis_kelamin"`
		Pendidikan   string `json:"pendidikan"`
		Statusdokter string `json:"status_dokter"`
		Spesialisasi string `json:"spesialisasi"`
	}

	ApiUser struct {
		ID         int    `json:"id"`
		Username   string `json:"username"`
		Password   string `json:"password"`
		Ket        string `json:"ket"`
		Nama       string `json:"nama"`
		Status     string `json:"status"`
		Photo      string `json:"photo"`
		Norm       string `json:"norm"`
		Nik        string `json:"nik"`
		Bpjs       string `json:"bpjs"`
		Noka       string `json:"noka"`
		Jk         string `json:"jk"`
		Alamat     string `json:"alamat"`
		TglLahir   string `json:"tgl_lahir"`
		Hp         string `json:"hp"`
		Prov       string `json:"prov"`
		Kota       string `json:"kota"`
		Kel        string `json:"kel"`
		Rt         string `json:"rt"`
		Rw         string `json:"rw"`
		NamaBagian string `json:"bagian"`
		Shift      string `json:"shift"`
	}

	User struct {
		UserName     string `json:"username"`
		Photo        string `json:"photo"`
		Nama         string `json:"nama"`
		Status       string `json:"status"`
		Token        string `json:"token"`
		RefreshToken string `json:"refresh_token"`
		Norm         string `json:"norm"`
		Nik          string `json:"nik"`
		Bpjs         string `json:"bpjs"`
		Noka         string `json:"noka"`
		Jk           string `json:"jk"`
		Alamat       string `json:"alamat"`
		TglLahir     string `json:"tgl_lahir"`
		Hp           string `json:"hp"`
		Prov         string `json:"prov"`
		Kota         string `json:"kota"`
		Kel          string `json:"kel"`
		Rt           string `json:"rt"`
		Rw           string `json:"rw"`
		NamaBagian   string `json:"bagian"`
		Shift        string `json:"shift"`
	}

	DebiturModel struct {
		Kode    string `json:"kode"`
		Debitur string `json:"debitur"`
		Alamat  string `json:"alamat"`
	}

	OTPUser struct {
		IdOtp   int    `json:"id_otp"`
		Email   string `json:"email"`
		KodeOtp string `json:"kode_otp"`
		Exp     int    `json:"exp"`
	}

	// MUTIARA PENGAJAR
	Karyawan struct {
		Id           string `gorm:"primaryKey:Id" json:"id"`
		Nama         string `json:"nama"`
		Jeniskelamin string `json:"jenis_kelamin"`
		Tgllahir     string `json:"tgl_lahir"`
	}
)

func (ApiUser) TableName() string {
	return "vicore_api.users"
}

func (Karyawan) TableName() string {
	return "mutiara.pengajar"
}

func (OTPUser) TableName() string {
	return "vicore_usr.users_otp"
}

func (ApiUser) DebiturModel() string {
	return "vicore_lib.kdebitur"
}

func (ProfilePasien) TableName() string {
	return "his.dprofilpasien"
}

func (Users) TableName() string {
	return "vicore_usr.users"
}

func (DataUsers) TableName() string {
	return "vicore_usr.users"
}

func (UserKlinik) TableName() string {
	return "klinik.users"
}

func (Kemploye) TableName() string {
	return "vicore_hrd.kemployee"
}

func (Pasien) TableName() string {
	return "his.dprofilpasien"
}

func (AlamatUser) TableName() string {
	return "vicore_api.alamat"
}
