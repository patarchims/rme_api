package dto

type (
	ReqAddVersion struct {
		AddVersion float32 `json:"add_version" validate:"required"`
	}
)
