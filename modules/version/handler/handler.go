package handler

import (
	"hms_api/modules/version/entity"
	"hms_api/pkg/helper"
	"net/http"

	"hms_api/modules/version/dto"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/session"
	"github.com/sirupsen/logrus"
)

type VersionHandler struct {
	VersionUseCase    entity.VersionUsecase
	VersionRepository entity.VersionRepository
	VersionMapper     entity.VersionMapper
	Logging           *logrus.Logger
	Store             *session.Store
}

func (vh VersionHandler) OnAddVersionFiberHandler(c *fiber.Ctx) error {
	payload := new(dto.ReqAddVersion)
	errs := c.BodyParser(&payload)

	if errs != nil {
		response := helper.APIResponseFailure("Data tidak dapat diproses", http.StatusCreated)
		return c.Status(fiber.StatusCreated).JSON(response)
	}

	validate := validator.New()

	if errs := validate.Struct(payload); errs != nil {
		errors := helper.FormatValidationError(errs)
		response := helper.APIResponse("Error", http.StatusAccepted, errors)
		return c.Status(fiber.StatusAccepted).JSON(response)
	}

	vh.Logging.Info("ADD VERSION")

	return nil
}
