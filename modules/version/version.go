package version

type (
	// VERSION
	// MODEL
	VersionModel struct {
		AndroidVersion float32 `json:"android_version"`
		IosVersion     float32 `json:"ios_version"`
		WindowsVersion float32 `json:"windows_version"`
	}
)

func (VersionModel) TableName() string {
	return "vicore_absensi.tb_version"
}
